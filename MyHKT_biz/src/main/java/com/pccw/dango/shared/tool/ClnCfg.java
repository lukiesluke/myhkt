/*
    Class for Client Configuration

    Keywords
    --------
    $URL: $
    $Rev: $
    $Date: $
    $Author: $
*/

package com.pccw.dango.shared.tool;

import java.io.Serializable;

import com.pccw.wheat.shared.tool.MiniProp;
import com.pccw.wheat.shared.tool.MiniRtException;


public class ClnCfg implements Serializable
{
    private static final long serialVersionUID = -7552635642543239831L;
    
    private MiniProp                prop;
    
    public static final String      DANGO_HOME              = "CLN_DANGO_HOME";           /* DANGO Home URL                       */
    public static final String      DANGO_CTX               = "CLN_DANGO_CTX";            /* Context of DANGO Home URL            */


    /* Chat URL */
    public static final String      CHAT_PREM_URL           = "CLN_CHAT_PREM_URL";        /* Chat for Premier Customer            */
    public static final String      CHAT_MOB_URL            = "CLN_CHAT_MOB_URL";         /* Chat for Mobile Subscribers          */
    public static final String      CHAT_URL                = "CLN_CHAT_URL";             /* Chat for Typical Customer            */
    public static final String      CHAT_COMM_URL           = "CLN_CHAT_COMM_URL";        /* Chat for Commercial Customer         */
    public static final String      CHAT_BA_MRT_URL         = "CLN_CHAT_BA_MRT_URL";      /* Chat for Bill Agent MRT              */
    public static final String      CHAT_MA                 = "CLN_CHAT_MA";              /* Chat Maintenance?                    */
    
    
    /* Directory Number */
    public static final String      MOB_PFX                 = "CLN_MOB_PFX";              /* Mobile Prefix                        */
    public static final String      LTS_PFX                 = "CLN_LTS_PFX";              /* Landline Prefix                      */


    /* BINQ */
    public static final String      BINQ_MAX_RTRY           = "CLN_BINQ_MAX_RTRY";        /* Check Bill Retry Max Count           */
    public static final String      BINQ_SLP_MS             = "CLN_BINQ_SLP_MS";          /* Check Bill Sleep Interval (MS)       */

    
    /* LNTT */
    public static final String      LNTT_MAX_RTRY           = "CLN_LNTT_MAX_RTRY";        /* Line Test Retry Max Count            */
    public static final String      LNTT_SLP_MS             = "CLN_LNTT_SLP_MS";          /* Line Test Sleep Interval (MS)        */

    
    /* Show Bill */
    public static final String      SHOWBILL_URL            = "CLN_SHOWBILL_URL";         /* URL for Show Bill (PDF)              */
    
    
    /* Mobile Pay Bill thru BEB */
    public static final String      MOBPAY_URL              = "CLN_MOBPAY_URL";           /* URL to Pay Mob Bill by BEB           */

    
    /* TNG Pay */
    public static final String      TNGPAY_URL              = "CLN_TNGPAY_URL";           /* URL for TNG Pay                      */

        
    /* CARE */
    public static final String      IGUARD_URL              = "CLN_IGUARD_URL";           /* URL for i-Guard                      */
    
    
    /* Transform Scale */
    public static final String      TRANSFORM               = "CLN_TRANSFORM";            /* Transform Scale                     */
        
    
    public static void main(String rArg[])
    {
        System.out.println(getVer());
        return;
    }
    
    
    public static String getVer()
    {
        return ("$URL: $, $Rev: $");
    }


    public ClnCfg()
    {
        initAndClear();
    }


    public ClnCfg(MiniProp rProp)
    {
        initAndClear();
        setProp(rProp);
    }
    
    
    final void initAndClear()
    {
        init();
        clear();
    }

    
    protected void init()
    {
    }
    
    
    public void clear()
    {
        clearProp();
    }
    

    public ClnCfg copyFrom(ClnCfg rSrc)
    {
        setProp(rSrc.getProp());
        
        return (this);
    }
    
    
    public ClnCfg copyTo(ClnCfg rDes)
    {
        rDes.copyFrom(this);
        return (rDes);
    }
    
    
    public ClnCfg copyMe()
    {
        ClnCfg                      rDes;
        
        rDes = new ClnCfg();
        rDes.copyFrom(this);
        
        return (rDes);
    }

    
    public void clearProp()
    {
        setProp(new MiniProp());
    }
    
    
    public void setProp(MiniProp rProp)
    {
        prop = rProp;
    }
    
    
    public MiniProp getProp()
    {
        return (prop);
    }
    
    
    public String get(String rKey)
    {
        return (getProp().get(rKey));
    }
    
    
    public int getInt(String rKey)
    {
        return (getProp().getInt(rKey));
    }
    
    
    public double getDouble(String rKey)
    {
        return (getProp().getDouble(rKey));
    }

    
    public boolean isOn(String rKey)
    {
        return (getProp().isOn(rKey));
    }
    
    
    public String getWithLang(String rKey)
    {
        String                      rKeyWL;
        
        /* rKey should be ended "_" */
        
        if (!rKey.endsWith("_")) {
            throw new MiniRtException("rKey is not proper ended!");
        }
		 rKeyWL = rKey + "EN";
        // TODO: Temp remove Code
//        if (ClnEnv.getBiTxMap().isEn()) rKeyWL = rKey + "EN";
//        else if (ClnEnv.getBiTxMap().isZh()) rKeyWL = rKey + "ZH";
//        else throw new MiniRtException("Unexpected Lang("+ClnEnv.getBiTxMap().getLang()+")!");
        
        return (get(rKeyWL));
    }
}
