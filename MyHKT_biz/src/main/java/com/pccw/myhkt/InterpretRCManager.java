package com.pccw.myhkt;

import android.content.Context;
import android.util.Log;
import com.pccw.biz.myhkt.R;
import com.pccw.dango.shared.entity.CustRec;
import com.pccw.dango.shared.entity.SubnRec;
import com.pccw.dango.shared.tool.RC;
import com.pccw.wheat.shared.tool.Reply;



public class InterpretRCManager {



	/**
	 * Used in
	 * ChangePwdFragment 
	 * MyProfLoginFragment
	 * 
	 */
	// Translate Reply getCode() to screen-specific string
	public final static String interpretRC_AcMainMdu(Context cxt, Reply rRC) {
		//TODO first 2 row are smae. need to combine
//		if (rRC.getCode().equals(RC.SVEE_NLPWD)) { return Utils.getString(cxt, R.string.PAMM_NAPWD); }
		if (rRC.getCode().equals(RC.SVEE_NLPWD)) { return Utils.getString(cxt, R.string.REGM_ILPWD); }
//		if (rRC.getCode().equals(RC.SVEE_IVPWDCBN)) { return Utils.getString(cxt, R.string.PAMM_NAPWD); }
		if (rRC.getCode().equals(RC.SVEE_IVPWDCBN)) { return Utils.getString(cxt, R.string.PAMM_PWD_NO_CHANGE); }
		if (rRC.getCode().equals(RC.SVEE_ILNICKNAME)) { return Utils.getString(cxt, R.string.PAMM_ILNICKNAME); }
		if (rRC.getCode().equals(RC.SVEE_RESV_NCKNM)) { return Utils.getString(cxt, R.string.PAMM_RESV_NCKNM); }
		if (rRC.getCode().equals(RC.SVEE_ICCTMAIL)) { return Utils.getString(cxt, R.string.PAMM_NACTMAIL); }
		if (rRC.getCode().equals(RC.SVEE_NLCTMAIL)) { return Utils.getString(cxt, R.string.PAMM_ILCTMAIL); }
		if (rRC.getCode().equals(RC.SVEE_IVCTMAIL)) { return Utils.getString(cxt, R.string.PAMM_IVCTMAIL); }
		if (rRC.getCode().equals(RC.SVEE_IVCTMOB)) { return Utils.getString(cxt, R.string.PAMM_IVMOB); }
		if (rRC.getCode().equals(RC.SVEE_IVMOBALRT)) { return Utils.getString(cxt, R.string.PAMM_IVMOBALRT); }
		if (rRC.getCode().equals(RC.SVEE_IVLANG)) { return Utils.getString(cxt, R.string.PAMM_IVLANG); }
		//			if (rRC.getCode().equals(RC._RCUS_IVSECQUS)) { return Utils.getString(cxt, R.string.PAMM_IVSECQUS); }
		//			if (rRC.getCode().equals(RC._RCUS_IVSECANS)) { return Utils.getString(cxt, R.string.SFRM_IVSECANS); }
		//			if (rRC.getCode().equals(RC._RCUS_ILSECANS)) { return Utils.getString(cxt, R.string.SFRM_ILSECANS); }
		if (rRC.getCode().equals(RC.NOT_AUTH)) { return Utils.getString(cxt, R.string.PAMM_NOT_AUTH); }
		if (rRC.getCode().equals(RC.INACTIVE_SVEE)) { return Utils.getString(cxt, R.string.PAMM_INACT_RCUS); }
		//			if (rRC.getCode().equals(RC._CTMAIL_EXIST)) { return Utils.getString(cxt, R.string.PAMM_CTMAIL_EXIST); }
		if (rRC.getCode().equals(RC.PWD_MISMATCH)) { return Utils.getString(cxt, R.string.PAMM_PWD_MISMATCH); }
		if (rRC.getCode().equals(RC.PWD_NO_CHANGE)) { return Utils.getString(cxt, R.string.PAMM_PWD_NO_CHANGE); }
		
		//Commercial App
		if (rRC.getCode().equals(RC.BCIF_ILCUSTNM)) { return Utils.getString(cxt, R.string.M_IV_CONAME); }
        else if (rRC.getCode().equals(RC.BCIF_NLINDUST)) { return Utils.getString(cxt, R.string.M_IV_INDUST); }
        else if (rRC.getCode().equals(RC.BCIF_NLNUSTFF)) { return Utils.getString(cxt, R.string.M_IV_NUSTFF); }
        else if (rRC.getCode().equals(RC.BCIF_NLNUPRSN)) { return Utils.getString(cxt, R.string.M_IV_NUPRSN); }
        else if (rRC.getCode().equals(RC.BCIF_IVXBRDR)) { return Utils.getString(cxt, R.string.M_IV_XBRDR);  }
        else if (rRC.getCode().equals(RC.BCIF_ILPRICTNM)) { return Utils.getString(cxt, R.string.M_IV_PRICTNM); }
        else if (rRC.getCode().equals(RC.BCIF_IVPRICTTL)) { return Utils.getString(cxt, R.string.M_IV_PRICTTL); }
        else if (rRC.getCode().equals(RC.BCIF_NLPRICTEM) || rRC.getCode().equals(RC.BCIF_IVPRICTEM)) { 
        	return Utils.getString(cxt, R.string.M_IV_PRICTEM); }
        else if (rRC.getCode().equals(RC.BCIF_ILPRICTJT)) { return Utils.getString(cxt, R.string.M_IV_PRICTJT); }
        else if (rRC.getCode().equals(RC.BCIF_NLPRICTTEL) || rRC.getCode().equals(RC.BCIF_IVPRICTTEL)) { 
        	return Utils.getString(cxt, R.string.M_IV_PRICTTEL); } 
        else if (rRC.getCode().equals(RC.BCIF_NLPRICTMOB) || rRC.getCode().equals(RC.BCIF_IVPRICTMOB)) {
            return Utils.getString(cxt, R.string.M_IV_PRICTMOB); } 
        else if (rRC.getCode().equals(RC.BCIF_ILSECCTNM)) { return Utils.getString(cxt, R.string.M_IV_SECCTNM); }
        else if (rRC.getCode().equals(RC.BCIF_IVSECCTTL)) { return Utils.getString(cxt, R.string.M_IV_SECCTTL); } 
        else if (rRC.getCode().equals(RC.BCIF_IVSECCTEM)) {
            return Utils.getString(cxt, R.string.M_IV_SECCTEM); } 
        else if (rRC.getCode().equals(RC.BCIF_ILSECCTJT)) { return Utils.getString(cxt, R.string.M_IV_SECCTJT); }
        else if (rRC.getCode().equals(RC.BCIF_NLSECCTTEL) || rRC.getCode().equals(RC.BCIF_IVSECCTTEL)) {
        	return Utils.getString(cxt, R.string.M_IV_SECCTTEL); }
        else if (rRC.getCode().equals(RC.BCIF_NLSECCTMOB) || rRC.getCode().equals(RC.BCIF_IVSECCTMOB)) {
            return Utils.getString(cxt, R.string.M_IV_SECCTMOB); }
        //else if(rRC.getCode().equals(RC.SUBN_NONEED_SRVNUM)) { return Utils.getString(cxt, R.string.M_IV_SECCTJT); }
		
		return ClnEnv.getRPCErrMsg(cxt, rRC.getCode());
	}
	
//	public final static String interpretRC_ResetPassword(Context cxt, Reply rRC) {
//		if (rRC.getCode().equals(RC.NO_CUSTOMER)) { return Utils.getString(cxt, R.string.FGCM_NO_RELV_RCUS); }
//		if (rRC.getCode().equals(RC.NOT_YET_RCUS)) { return Utils.getString(cxt, R.string.FGCM_NO_RELV_RCUS); }
//		if (rRC.getCode().equals(RC.FILL_SVEE4CLBL)) { return Utils.getString(cxt, R.string.LOGINSTATE_ERR1); }
//		if (rRC.getCode().equals(RC.FILL_SVEE4CLUB)) { return Utils.getString(cxt, R.string.LOGINSTATE_ERR1); }
//		if (rRC.getCode().equals(RC.FILL_SVEE_NOTNC)) { return Utils.getString(cxt, R.string.LOGINSTATE_ERR1); }
//		if (rRC.getCode().equals(RC.FILL_SVEE4CLBL_NOTNC)) { return Utils.getString(cxt, R.string.LOGINSTATE_ERR1); }
//		if (rRC.getCode().equals(RC.FILL_SVEE4CLAV)) { return Utils.getString(cxt, R.string.LOGINSTATE_ERR1); }
//		
//		
//		//TODO first 2 row are smae. need to combine
////		if (rRC.getCode().equals(RC.RC_NO_CTMAIL)) { return Utils.getString(cxt, R.string.FGCM_NO_CTMAIL); }
//
////		if (rRC.getCode().equals(RC.RC_RCUS_NACTMAIL)) { return Utils.getString(cxt, R.string.FGCM_NACTMAIL); }
//
////		if (rRC.getCode().equals(RC.RC_RCUS_ILCTMAIL)) { return Utils.getString(cxt, R.string.FGCM_ILCTMAIL); }
//
////		if (rRC.getCode().equals(RC.SVEE_IVCTMAIL)) { return Utils.getString(cxt, R.string.FGCM_IVCTMAIL); }
////
//		if (rRC.getCode().equals(RC.ACTV_SVEE) || rRC.getCode().equals(RC.RESET_SVEE)) { return Utils.getString(cxt, R.string.FGCM_IVSTATE); }
////
////		if (rRC.getCode().equals(RC.RC_RCUS_IVSECANS)) { return Utils.getString(cxt, R.string.FGCM_IVSECANS); }
////
////		if (rRC.getCode().equals(RC.RC_RCUS_ILSECANS)) { return Utils.getString(cxt, R.string.FGCM_ILSECANS); }
////
////		if (rRC.getCode().equals(RC.RC_SECANS_MISMATCH)) { return Utils.getString(cxt, R.string.FGCM_SECANS_MM); }
////		
////		if (rRC.getCode().equals(RC.RC_CUST_ILDOCNUM)) {return Utils.getString(cxt, R.string.SFRM_ILDOCNUM);}
//		
//		return ClnEnv.getRPCErrMsg(cxt, rRC.getCode());
//	}
//	
	public final static String interpretRC_ForgetPassword(Context cxt, Reply rRC) {
		if (rRC.getCode().equals(RC.NO_CUSTOMER)) { return Utils.getString(cxt, R.string.FGCM_NO_RELV_RCUS); }
		if (rRC.getCode().equals(RC.NOT_YET_RCUS)) { return Utils.getString(cxt, R.string.FGCM_NO_RELV_RCUS); }
		if (rRC.getCode().equals(RC.FILL_SVEE4CLBL)) { return Utils.getString(cxt, R.string.LOGINSTATE_ERR1); }
		if (rRC.getCode().equals(RC.FILL_SVEE4CLUB)) { return Utils.getString(cxt, R.string.LOGINSTATE_ERR1); }
		if (rRC.getCode().equals(RC.FILL_SVEE_NOTNC)) { return Utils.getString(cxt, R.string.LOGINSTATE_ERR1); }
		if (rRC.getCode().equals(RC.FILL_SVEE4CLBL_NOTNC)) { return Utils.getString(cxt, R.string.LOGINSTATE_ERR1); }
		if (rRC.getCode().equals(RC.FILL_SVEE4CLAV)) { return Utils.getString(cxt, R.string.LOGINSTATE_ERR1); }
		if (rRC.getCode().equals(RC.SVEE_IVSTATE)) { return Utils.getString(cxt, R.string.FILL_SVEE_IVSTATE); }

		//TODO first 2 row are smae. need to combine
//		if (rRC.getCode().equals(RC.RC_NO_CTMAIL)) { return Utils.getString(cxt, R.string.FGCM_NO_CTMAIL); }
//		if (rRC.getCode().equals(RC.RC_RCUS_NACTMAIL)) { return Utils.getString(cxt, R.string.FGCM_NACTMAIL); }
		if (rRC.getCode().equals(RC.SVEE_ICCTMAIL)) { return Utils.getString(cxt, R.string.FGCM_ILCTMAIL); }
		if (rRC.getCode().equals(RC.SVEE_IVCTMAIL)) { return Utils.getString(cxt, R.string.FGCM_IVCTMAIL); }
		if (rRC.getCode().equals(RC.ACTV_SVEE) || rRC.getCode().equals(RC.RESET_SVEE)) { return Utils.getString(cxt, R.string.FGCM_IVSTATE); }
//		if (rRC.getCode().equals(RC.SVEE_IVSECANS)) { return Utils.getString(cxt, R.string.FGCM_IVSECANS); }
//		if (rRC.getCode().equals(RC.RC_RCUS_ILSECANS)) { return Utils.getString(cxt, R.string.FGCM_ILSECANS); }
//		if (rRC.getCode().equals(RC.RC_SECANS_MISMATCH)) { return Utils.getString(cxt, R.string.FGCM_SECANS_MM); }		
		if (rRC.getCode().equals(RC.CUST_NLDOCNUM)) {return Utils.getString(cxt, R.string.SFRM_ILDOCNUM);}
		
		return ClnEnv.getRPCErrMsg(cxt, rRC.getCode());
	}

	public final static String interpretDQ_Mdu(Context me, String errCode, String replyErrCode) {
//		Log.i("", "Code"+errCode);
		if ("0".equalsIgnoreCase(errCode)) { return Utils.getString(me, R.string.MYHKT_DQ_ERR_0); }
		if ("1".equalsIgnoreCase(errCode)) { return Utils.getString(me, R.string.MYHKT_DQ_ERR_1); }
		if ("2".equalsIgnoreCase(errCode)) { return Utils.getString(me, R.string.MYHKT_DQ_ERR_2); }
		if ("3".equalsIgnoreCase(errCode)) { return Utils.getString(me, R.string.MYHKT_DQ_ERR_3); }
		if ("4".equalsIgnoreCase(errCode)) { return Utils.getString(me, R.string.MYHKT_DQ_ERR_4); }
		if ("5".equalsIgnoreCase(errCode)) { return Utils.getString(me, R.string.MYHKT_DQ_ERR_5); }
		if ("6".equalsIgnoreCase(errCode)) { return Utils.getString(me, R.string.MYHKT_DQ_ERR_6); }
		if ("7".equalsIgnoreCase(errCode)) { return Utils.getString(me, R.string.MYHKT_DQ_ERR_7); }
		if ("8".equalsIgnoreCase(errCode)) { return Utils.getString(me, R.string.MYHKT_DQ_ERR_8); }
		if ("9".equalsIgnoreCase(errCode)) { return Utils.getString(me, R.string.MYHKT_DQ_ERR_9); }
		if ("10".equalsIgnoreCase(errCode)) { return Utils.getString(me, R.string.MYHKT_DQ_ERR_10); }
		if ("11".equalsIgnoreCase(errCode)) { return Utils.getString(me, R.string.MYHKT_DQ_ERR_11); }

		return ClnEnv.getRPCErrMsg(me, replyErrCode);
	}

	public final static String interpretMM_Mdu(Context cxt, Reply rRC) {
		//		if (rRC.isEqual(Reply.RC_RCUS_IVMOB)) { return Utils.getString(me, R.string.REGM_IVMOB); }
		//		if (rRC.isEqual(Reply.RC_MM_EAI_IVPWD) || rRC.isEqual(Reply.RC_MM_MMP_IVPWD)) { return Utils.getString(me, R.string.PAMM_IVCURRPWD); }
		//		if (rRC.isEqual(Reply.RC_MM_H_INDIV)) { return Utils.getString(me, R.string.MYMOB_RC_MM_H_INDIV); }
		//		if (rRC.isEqual(Reply.RC_MM_C_INDIV)) { return Utils.getString(me, R.string.MYMOB_RC_MM_C_INDIV); }
		//		if (rRC.isEqual(Reply.RC_MM_TOO_MANY)) { return Utils.getString(me, R.string.MYMOB_RC_MM_TOO_MANY); }
		//		if (rRC.isEqual(Reply.RC_MM_EAI_IVPWD)) { return Utils.getString(me, R.string.MYMOB_RC_MM_EAI_IVPWD); }
		//		if (rRC.isEqual(Reply.RC_MM_MMP_IVPWD)) { return Utils.getString(me, R.string.MYMOB_RC_MM_MMP_IVPWD); }
		//		if (rRC.isEqual(Reply.RC_MM_SUBN_FND)) { return Utils.getString(me, R.string.MYMOB_RC_MM_SUBN_FND); }
		//		if (rRC.isEqual(Reply.RC_MM_NO_SUBN)) { return Utils.getString(me, R.string.MYMOB_RC_MM_NO_SUBN); }
		//		if (rRC.isEqual(Reply.RC_API_NOTAUTH)) {return Utils.getString(me, R.string.MYMOB_RC_CVI_UNSUCC);}
		//		if (rRC.isEqual(Reply.RC_CVI_UNSUCC)) {return Utils.getString(me, R.string.MYMOB_RC_CVI_UNSUCC);}
		//		if (rRC.isEqual(Reply.RC_MM_MMP_OA)) {return Utils.getString(me, R.string.MYMOB_RC_MM_MMP_OA);}

		if (rRC.getCode().equals(RC.SVEE_IVMOBALRT)) { return Utils.getString(cxt, R.string.REGM_IVMOB); }
		if (rRC.getCode().equals(RC.AO_EAI_IVPWD)) { return Utils.getString(cxt, R.string.MYMOB_RC_MM_MMP_IVPWD); }

		if (rRC.getCode().equals(RC.AO_MMP_NO_SUBN)) { return Utils.getString(cxt, R.string.MYMOB_RC_MM_NO_SUBN); }
		if (rRC.getCode().equals(RC.AO_MMP_IVPWD)) { return Utils.getString(cxt, R.string.MYMOB_RC_MM_MMP_IVPWD); }
		if (rRC.getCode().equals(RC.AO_MMP_UNSUCC)) { return Utils.getString(cxt, R.string.MYMOB_RC_CVI_UNSUCC); }
		if (rRC.getCode().equals(RC.AO_MMP_OA)) { return Utils.getString(cxt, R.string.MYMOB_RC_MM_MMP_OA); }
		//		if (rRC.getCode().equals(RC.AO_MMP_UKNLOB)) { return Utils.getString(cxt, R.string.); }
		if (rRC.getCode().equals(RC.AO_EAI_IVPWD)) { return Utils.getString(cxt, R.string.MYMOB_RC_MM_EAI_IVPWD); }

		return ClnEnv.getRPCErrMsg(cxt, rRC.getCode());
	}
	
	// Translate Reply getCode() to screen-specific string
	public final static String interpretRC_CtacMdu(Context cxt, Reply rRC) {
//		if (rRC.isEqual(Reply.RC_CTAC_NACTMAIL)) { return Utils.getString(me.getActivity(), R.string.CTAM_NAEMAIL); }
		if (rRC.getCode().equals(RC.CTAC_IVCTMAIL)) { return Utils.getString(cxt, R.string.CTAM_IVEMAIL); }
		if (rRC.getCode().equals(RC.CTAC_ILCTMAIL)) { return Utils.getString(cxt, R.string.CTAM_ILEMAIL); }
		if (rRC.getCode().equals(RC.CTAC_REQ_MOB)) { return Utils.getString(cxt, R.string.CTAM_REQ_MOB); }
		if (rRC.getCode().equals(RC.CTAC_REQ_HMNUM)) { return Utils.getString(cxt, R.string.CTAM_REQ_HMNUM); }
		if (rRC.getCode().equals(RC.CTAC_REQ_OFCNUM)) { return Utils.getString(cxt, R.string.CTAM_REQ_OFCNUM); }
		if (rRC.getCode().equals(RC.CTAC_REQ_EMAIL)) { return Utils.getString(cxt, R.string.CTAM_REQ_EMAIL); }
		if (rRC.getCode().equals(RC.CTAC_IVMOB)) { return Utils.getString(cxt, R.string.CTAM_IVMOB); }
		if (rRC.getCode().equals(RC.CTAC_IVHMNUM)) { return Utils.getString(cxt, R.string.CTAM_IVHMNUM); }
		if (rRC.getCode().equals(RC.CTAC_IVOFCNUM)) { return Utils.getString(cxt, R.string.CTAM_IVOFCNUM); }
		if (rRC.getCode().equals(RC.CTAC_REQ_INPUT)) { return Utils.getString(cxt, R.string.CTAM_REQ_INPUT); }

		return ClnEnv.getRPCErrMsg(cxt, rRC.getCode());
	}
	
	public final static String interpretRC_PlanLtsMdu(Context cxt, Reply rRC) {
		if (rRC.getCode().equals(RC.SIP)) { return Utils.getString(cxt, R.string.SHLM_SIP); }
		if (rRC.getCode().equals(RC.WIP)) { return Utils.getString(cxt, R.string.SHLM_WIP); }

		return ClnEnv.getRPCErrMsg(cxt, rRC.getCode());
	}

	//	public final static String interpretMM_Mdu(Context cxt, Reply rRC, String lob) { 
	//		if (lob.equalsIgnoreCase(SubnRec.LOB_101)) {
	//			if (rRC.isEqual(Reply.RC_API_NOTAUTH)) { 
	//				return Utils.getString(cxt, R.string.MYMOB_RC_CVI_UNSUCC_101); 
	//			} else if (rRC.isEqual(Reply.RC_CVI_UNSUCC)) { 
	//				return Utils.getString(cxt, R.string.MYMOB_RC_CVI_UNSUCC_101); 
	//			} //TODO
	//		} else if (lob.equalsIgnoreCase(SubnRec.LOB_O2F)) {
	//			if (rRC.isEqual(Reply.RC_API_NOTAUTH)) { 
	//				return Utils.getString(cxt, R.string.MYMOB_RC_CVI_UNSUCC_O2F); 
	//			} else if (rRC.isEqual(Reply.RC_CVI_UNSUCC)) { 
	//				return Utils.getString(cxt, R.string.MYMOB_RC_CVI_UNSUCC_O2F); 
	//			} //TODO
	//		}
	//		return interpretRC(rRC);//TODO
	//	}

	public final static String interpretRC(Context me, Reply rRC) {
		
		//		Log.i(TAG, "interpretRC" + rRC.getCode());
		//		if (rRC.equals(Reply.RC_SVEE_NALOGINID)) { return Utils.getString(me, R.string.SFRM_NALOGINID); }
		//		if (rRC.equals(Reply.RC_SVEE_IVLOGINID)) { return Utils.getString(me, R.string.SFRM_IVLOGINID); }
		//		if (rRC.equals(Reply.RC_SVEE_ILLOGINID)) { return Utils.getString(me, R.string.SFRM_ILLOGINID); }
		//		if (rRC.equals(Reply.RC_SVEE_RESV_LOGIN)) { return Utils.getString(me, R.string.SFRM_RESV_LOGIN); }
		//		if (rRC.equals(Reply.RC_SVEE_NAPWD)) { return Utils.getString(me, R.string.SFRM_NAPWD); }
		//		if (rRC.equals(Reply.RC_SVEE_ILPWD)) { return Utils.getString(me, R.string.SFRM_ILPWD); }
		//		if (rRC.equals(Reply.RC_SVEE_IVPWDCBN)) { return Utils.getString(me, R.string.SFRM_IVPWDCBN); }
		//		if (rRC.equals(Reply.RC_SVEE_ILNICKNAME)) { return Utils.getString(me, R.string.SFRM_ILNICKNAME); }
		//		if (rRC.equals(Reply.RC_SVEE_RESV_NCKNM)) { return Utils.getString(me, R.string.SFRM_RESV_NCKNM); }
		//		if (rRC.equals(Reply.RC_SVEE_NACTMAIL)) { return Utils.getString(me, R.string.SFRM_NACTMAIL); }
		//		if (rRC.equals(Reply.RC_SVEE_ILCTMAIL)) { return Utils.getString(me, R.string.SFRM_ILCTMAIL); }
		//		if (rRC.equals(Reply.RC_SVEE_IVCTMAIL)) { return Utils.getString(me, R.string.SFRM_IVCTMAIL); }
		//		if (rRC.equals(Reply.RC_SVEE_IVMOB)) { return Utils.getString(me, R.string.SFRM_IVMOB); }
		//		if (rRC.equals(Reply.RC_SVEE_IVMOBALRT)) { return Utils.getString(me, R.string.SFRM_IVMOBALRT); }
		//		if (rRC.equals(Reply.RC_SVEE_IVLANG)) { return Utils.getString(me, R.string.SFRM_IVLANG); }
		//		if (rRC.equals(Reply.RC_SVEE_IVSECQUS)) { return Utils.getString(me, R.string.SFRM_IVSECQUS); }
		//		if (rRC.equals(Reply.RC_SVEE_IVSECANS)) { return Utils.getString(me, R.string.SFRM_IVSECANS); }
		//		if (rRC.equals(Reply.RC_SVEE_ILSECANS)) { return Utils.getString(me, R.string.SFRM_ILSECANS); }
		//		if (rRC.equals(Reply.RC_CUST_IVDOCTY)) { return Utils.getString(me, R.string.SFRM_IVDOCTY); }
		//		if (rRC.equals(Reply.RC_CUST_ILDOCNUM)) { return Utils.getString(me, R.string.SFRM_ILDOCNUM); }
		//		if (rRC.equals(Reply.RC_NO_CUSTOMER)) { return Utils.getString(me, R.string.SFRM_NO_CUSTOMER); }
		//		if (rRC.equals(Reply.RC_NO_SUBN)) { return Utils.getString(me, R.string.SFRM_NO_SUBN); }
		//		if (rRC.equals(Reply.RC_CUS_ALDY_REG)) { return Utils.getString(me, R.string.SFRM_CUS_ALDY_REG); }
		//		if (rRC.equals(Reply.RC_LOGIN_ID_EXIST)) { return Utils.getString(me, R.string.SFRM_LOGIN_ID_EXIST); }
		//		if (rRC.equals(Reply.RC_CTMAIL_EXIST)) { return Utils.getString(me, R.string.SFRM_CTMAIL_EXIST); }
		//		if (rRC.equals(Reply.RC_CIK_MISMATCH)) { return Utils.getString(me, R.string.SFRM_DOC_TY_NUM_MM); }
		//		if (rRC.equals(Reply.RC_LOST_RGNS)) { return Utils.getString(me, R.string.SFRM_LOST_RGNS); }
		// Undefined error mapping
		return ClnEnv.getRPCErrMsg(me, rRC.getCode());
	}

	//Use in reg acc info activity
	public final static String interpretRC_RegnMdu(Context me, Reply rRC) {
		if (rRC.equals(RC.NO_LOGIN_ID)) { return Utils.getString(me, R.string.FGCM_NO_RELV_RCUS); }
		if (rRC.equals(RC.CUS_MISMATCH)) { return Utils.getString(me, R.string.SFRM_NO_CUSTOMER); }

		if (rRC.equals(RC.SVEE_NLLOGINID)) { return Utils.getString(me, R.string.REGM_NALOGINID); }
		if (rRC.equals(RC.SVEE_IVLOGINID)) { return Utils.getString(me, R.string.REGM_IVLOGINID); }
		if (rRC.equals(RC.SVEE_IVLOGINID)) { return Utils.getString(me, R.string.REGM_ILLOGINID); }
		//				if (rRC.equals(RC.SVEE_RESV_LOGIN)) { return Utils.getString(me, R.string.REGM_RESV_LOGIN); }
//		if (rRC.equals(RC.SVEE_NLPWD)) { return Utils.getString(me, R.string.REGM_NAPWD); }
		if (rRC.equals(RC.SVEE_NLPWD)) { return Utils.getString(me, R.string.REGM_ILPWD); }
		if (rRC.equals(RC.SVEE_IVPWDCBN)) { return Utils.getString(me, R.string.REGM_IVPWDCBN); }
		if (rRC.equals(RC.SVEE_ILNICKNAME)) { return Utils.getString(me, R.string.REGM_ILNICKNAME); }
		if (rRC.equals(RC.SVEE_RESV_NCKNM)) { return Utils.getString(me, R.string.REGM_RESV_NCKNM); }
		if (rRC.equals(RC.SVEE_NLCTMAIL)) { return Utils.getString(me, R.string.REGM_NACTMAIL); }
		if (rRC.equals(RC.SVEE_IVCTMAIL)) { return Utils.getString(me, R.string.REGM_IVCTMAIL); }
		if (rRC.equals(RC.SVEE_IVCTMOB)) { return Utils.getString(me, R.string.REGM_IVMOB); }
		if (rRC.equals(RC.SVEE_IVMOBALRT)) { return Utils.getString(me, R.string.REGM_IVMOBALRT); }
		if (rRC.equals(RC.SVEE_IVLANG)) { return Utils.getString(me, R.string.REGM_IVLANG); }
		if (rRC.equals(RC.SVEE_IVSECQUS)) { return Utils.getString(me, R.string.REGM_IVSECQUS); }
		if (rRC.equals(RC.SVEE_IVSECANS)) { return Utils.getString(me, R.string.REGM_IVSECANS); }
		//		if (rRC.equals(RC.SVEE_ILSECANS)) { return Utils.getString(me, R.string.REGM_ILSECANS); }
		if (rRC.equals(RC.CUST_IVDOCTY)) { return Utils.getString(me, R.string.REGM_IVDOCTY); }
		if (rRC.equals(RC.CUST_ILDOCNUM)) { return Utils.getString(me, R.string.REGM_ILDOCNUM); }
		if (rRC.equals(RC.CUST_NLDOCNUM)) { return Utils.getString(me, R.string.REGM_ILDOCNUM); }

		if (rRC.equals(RC.SUBN_IVLOB)) { return Utils.getString(me, R.string.REGM_IVLOB); }
		//		if (rRC.equals(RC.SUBN_ILACCTNUM)) { return Utils.getString(me, R.string.REGM_ILACCTNUM); }
		if (rRC.equals(RC.SUBN_IVACCTNUM)) { return Utils.getString(me, R.string.REGM_IVACCTNUM); }
		if (rRC.equals(RC.SUBN_IVSN_LTS)) { return Utils.getString(me, R.string.REGM_ILSRVNUM_LTS); }
		if (rRC.equals(RC.SUBN_IVSN_MOB)) { return Utils.getString(me, R.string.REGM_ILSRVNUM_MOB); }
//				if (rRC.equals(RC.SUBN_IVAN_101)) { return Utils.getString(me, R.string.REGM_IVAN_101); }
//				if (rRC.equals(RC.SUBN_IVAN_NE)) { return Utils.getString(me, R.string.REGM_IVAN_NE); }
		if (rRC.equals(RC.SUBN_IVSN_PCD)) { return Utils.getString(me, R.string.REGM_ILSRVNUM_PCD); }
		if (rRC.equals(RC.SUBN_IVAN_TV)) { return Utils.getString(me, R.string.REGM_IVAN_TV); }
		if (rRC.equals(RC.SUBN_IVSN_LTS)) { return Utils.getString(me, R.string.REGM_IVSN_LTS); }
		if (rRC.equals(RC.SUBN_IVSN_MOB)) { return Utils.getString(me, R.string.REGM_IVSN_MOB); }
		if (rRC.equals(RC.NO_CUSTOMER)) { return Utils.getString(me, R.string.REGM_NO_CUSTOMER); }
		if (rRC.equals(RC.NO_SUBN)) { return Utils.getString(me, R.string.REGM_NO_SUBN); }
		if (rRC.equals(RC.CUS_ALDY_REG)) { return Utils.getString(me, R.string.REGM_CUS_ALDY_REG); }
		if (rRC.equals(RC.LOGIN_ID_EXIST)) { return Utils.getString(me, R.string.REGM_LOGIN_ID_EXIST); }
		//		if (rRC.equals(RC.CTMAIL_EXIST)) { return Utils.getString(me, R.string.REGM_CTMAIL_EXIST); }
		//		if (rRC.equals(RC.NO_CUSTOMER_NOPP)) { return Utils.getString(me, R.string.REGM_NO_CUSTOMER_NOPP); }
		//		if (rRC.equals(RC.NO_SUBN_NOPP)) { return Utils.getString(me, R.string.REGM_NO_SUBN_NOPP); }

		// Undefined error mapping
		return ClnEnv.getRPCErrMsg(me.getApplicationContext(), rRC.getCode());
	}
	
	//Use in reg baic activity
	public final static String interpretRC_RegnMdu(Context me, Reply rRC, SubnRec subnRec, CustRec custRec) {
		//		debugLog(TAG, "interpretRC_RegnMdu" + rRC.getCode());
		if (isReqSpecMsg4DocTy(subnRec, custRec)) { 
			if (rRC.equals(RC.NO_CUSTOMER)) {
				return Utils.getString(me, R.string.REGM_NO_CUSTOMER_NOPP);
			}
			else if (rRC.equals(RC.NO_SUBN)) {
				return Utils.getString(me, R.string.REGM_NO_SUBN_NOPP);
			}
		}

		if (rRC.equals(RC.SVEE_NLLOGINID)) { return Utils.getString(me, R.string.REGM_NALOGINID); }
		if (rRC.equals(RC.SVEE_IVLOGINID)) { return Utils.getString(me, R.string.REGM_IVLOGINID); }
		if (rRC.equals(RC.SVEE_IVLOGINID)) { return Utils.getString(me, R.string.REGM_ILLOGINID); }
		//				if (rRC.equals(RC.SVEE_RESV_LOGIN)) { return Utils.getString(me, R.string.REGM_RESV_LOGIN); }
//		if (rRC.equals(RC.SVEE_NLPWD)) { return Utils.getString(me, R.string.REGM_NAPWD); }
		if (rRC.equals(RC.SVEE_NLPWD)) { return Utils.getString(me, R.string.REGM_ILPWD); }
		if (rRC.equals(RC.SVEE_IVPWDCBN)) { return Utils.getString(me, R.string.REGM_IVPWDCBN); }
		if (rRC.equals(RC.SVEE_ILNICKNAME)) { return Utils.getString(me, R.string.REGM_ILNICKNAME); }
		if (rRC.equals(RC.SVEE_RESV_NCKNM)) { return Utils.getString(me, R.string.REGM_RESV_NCKNM); }
		if (rRC.equals(RC.SVEE_NLCTMAIL)) { return Utils.getString(me, R.string.REGM_NACTMAIL); }
//		if (rRC.equals(RC.SVEE_IVCTMAIL)) { return Utils.getString(me, R.string.REGM_ILCTMAIL); }
		if (rRC.equals(RC.SVEE_IVCTMAIL)) { return Utils.getString(me, R.string.REGM_IVCTMAIL); }
		if (rRC.equals(RC.SVEE_IVCTMOB)) { return Utils.getString(me, R.string.REGM_IVMOB); }
		if (rRC.equals(RC.SVEE_IVMOBALRT)) { return Utils.getString(me, R.string.REGM_IVMOBALRT); }
		if (rRC.equals(RC.SVEE_IVLANG)) { return Utils.getString(me, R.string.REGM_IVLANG); }
		if (rRC.equals(RC.SVEE_IVSECQUS)) { return Utils.getString(me, R.string.REGM_IVSECQUS); }
		if (rRC.equals(RC.SVEE_IVSECANS)) { return Utils.getString(me, R.string.REGM_IVSECANS); }
		//		if (rRC.equals(RC.SVEE_ILSECANS)) { return Utils.getString(me, R.string.REGM_ILSECANS); }
		if (rRC.equals(RC.CUST_IVDOCTY)) { return Utils.getString(me, R.string.REGM_IVDOCTY); }
		if (rRC.equals(RC.CUST_ILDOCNUM)) { return Utils.getString(me, R.string.REGM_ILDOCNUM); }
		if (rRC.equals(RC.CUST_NLDOCNUM)) { return Utils.getString(me, R.string.REGM_ILDOCNUM); }

		if (rRC.equals(RC.SUBN_IVLOB)) { return Utils.getString(me, R.string.REGM_IVLOB); }
		//		if (rRC.equals(RC.SUBN_ILACCTNUM)) { return Utils.getString(me, R.string.REGM_ILACCTNUM); }
		if (rRC.equals(RC.SUBN_IVACCTNUM)) { return Utils.getString(me, R.string.REGM_IVACCTNUM); }
		if (rRC.equals(RC.SUBN_IVSN_LTS)) { return Utils.getString(me, R.string.REGM_ILSRVNUM_LTS); }
		if (rRC.equals(RC.SUBN_IVSN_MOB)) { return Utils.getString(me, R.string.REGM_ILSRVNUM_MOB); }
//				if (rRC.equals(RC.SUBN_IVAN_101)) { return Utils.getString(me, R.string.REGM_IVAN_101); }
//				if (rRC.equals(RC.SUBN_IVAN_NE)) { return Utils.getString(me, R.string.REGM_IVAN_NE); }
		if (rRC.equals(RC.SUBN_IVSN_PCD)) { return Utils.getString(me, R.string.REGM_ILSRVNUM_PCD); }
		if (rRC.equals(RC.SUBN_IVAN_TV)) { return Utils.getString(me, R.string.REGM_IVAN_TV); }


		String lob = subnRec.lob;
		if (rRC.equals(RC.SUBN_ILSRVNUM)) {
			if (lob.equals(SubnRec.LOB_LTS)) {
				return Utils.getString(me, R.string.REGM_ILSRVNUM_LTS);
			} else if (lob.equals(SubnRec.LOB_MOB) || lob.equals(SubnRec.LOB_IOI)) {
				return Utils.getString(me, R.string.REGM_ILSRVNUM_MOB);
			} else if (lob.equals(SubnRec.LOB_PCD)) {
				return Utils.getString(me, R.string.REGM_ILSRVNUM_PCD);
			} else if (lob.equals(SubnRec.LOB_TV)) {
				return Utils.getString(me, R.string.REGM_NO_SUBN);
			} else {
				return Utils.getString(me, R.string.REGM_ILSRVNUM);
			}
		}

		if (rRC.equals(RC.SUBN_IVSN_LTS)) { return Utils.getString(me, R.string.REGM_IVSN_LTS); }
		if (rRC.equals(RC.SUBN_IVSN_MOB)) { return Utils.getString(me, R.string.REGM_IVSN_MOB); }
		if (rRC.equals(RC.NO_CUSTOMER)) { return Utils.getString(me, R.string.REGM_NO_CUSTOMER); }
		if (rRC.equals(RC.NO_SUBN)) { return Utils.getString(me, R.string.REGM_NO_SUBN); }
		if (rRC.equals(RC.CUS_ALDY_REG)) { return Utils.getString(me, R.string.REGM_CUS_ALDY_REG); }
		if (rRC.equals(RC.LOGIN_ID_EXIST)) { return Utils.getString(me, R.string.REGM_LOGIN_ID_EXIST); }
		//		if (rRC.equals(RC.CTMAIL_EXIST)) { return Utils.getString(me, R.string.REGM_CTMAIL_EXIST); }
		//		if (rRC.equals(RC.NO_CUSTOMER_NOPP)) { return Utils.getString(me, R.string.REGM_NO_CUSTOMER_NOPP); }
		//		if (rRC.equals(RC.NO_SUBN_NOPP)) { return Utils.getString(me, R.string.REGM_NO_SUBN_NOPP); }

		// Undefined error mapping
		return ClnEnv.getRPCErrMsg(me.getApplicationContext(), rRC.getCode());
	}



	private static boolean  isReqSpecMsg4DocTy(SubnRec subnRec, CustRec custRec) {
		/* TODO - Remove when Not Necessary */
		/* Special Handling for 101 and O2F */

		String  rLob;
		if (subnRec == null || custRec == null) {
			return false;
		}

		rLob = subnRec.lob;
		if (rLob.equals(SubnRec.LOB_101)) {
            return custRec.docTy == CustRec.TY_PASSPORT;
		}
		else if (rLob.equals(SubnRec.LOB_O2F)) {
            return custRec.docTy == CustRec.TY_PASSPORT;
		}
		else if (rLob.equals(SubnRec.LOB_MOB) || rLob.equals(SubnRec.LOB_IOI)) {
			if (subnRec.acctNum.trim().length() == SubnRec.CSL_ACCTNUM_LEN) {
                return custRec.docTy == CustRec.TY_PASSPORT;
			}
		}
		return (false);
	}

	// Translate Reply getCode() to screen-specific string
	public final static String interpretRC_SmsActnMdu(Context me, Reply rRC) {
		if (rRC.equals(RC.ALNK_FAIL)) { return Utils.getString(me, R.string.SACM_ALNK_NOTFND); }
		if (rRC.equals(RC.ACTCODE_MISMATCH)) { return Utils.getString(me, R.string.SACM_ACTCODE_MM); }
		if (rRC.equals(RC.ACTCODE_REGEN_EXCEED)) { return Utils.getString(me, R.string.SACM_TOOMANY_RG); }

		// Undefined error mapping
		return ClnEnv.getRPCErrMsg(me.getApplicationContext(), rRC.getCode());
	}

	// Temporary return Line Test Message
	public final static String interpretRC_LttLtsMdu(Context me, String rRC) {
		if (rRC.equals("LTT_NORMAL")) { return Utils.getString(me, R.string.LTT_NORMAL); }
		if (rRC.equals("LTT_OSY")) { return Utils.getString(me, R.string.LTT_OSY); }
		if (rRC.equals("LTT_NP_BB_NTWK_NGD")) { return Utils.getString(me, R.string.LTT_NP_BB_NTWK_NGD); }
		if (rRC.equals("LTT_NP_EYE_MODEM_NGD")) { return Utils.getString(me, R.string.LTT_NP_EYE_MODEM_NGD); }
		if (rRC.equals("LTT_NP_EYE_NORMAL")) { return Utils.getString(me, R.string.LTT_NP_EYE_NORMAL); }
		if (rRC.equals("LTT_NP_EYE2_MODEM_NGD")) { return Utils.getString(me, R.string.LTT_NP_EYE2_MODEM_NGD); }
		if (rRC.equals("LTT_NP_EYE2_NORMAL")) { return Utils.getString(me, R.string.LTT_NP_EYE2_NORMAL); }
		if (rRC.equals("LTT_NP_IMS_MODEN_NGD")) { return Utils.getString(me, R.string.LTT_NP_IMS_MODEN_NGD); }
		if (rRC.equals("LTT_NP_IMS_NORMAL")) { return Utils.getString(me, R.string.LTT_NP_IMS_NORMAL); }
		if (rRC.equals("LTT_NP_MODEM_OSY")) { return Utils.getString(me, R.string.LTT_NP_MODEM_OSY); }
		if (rRC.equals("LTT_NP_TV_MODEM_NGD")) { return Utils.getString(me, R.string.LTT_NP_TV_MODEM_NGD); }
		if (rRC.equals("LTT_NP_TV_NORMAL")) { return Utils.getString(me, R.string.LTT_NP_TV_NORMAL); }
		if (rRC.equals("LTT_PE_BIC")) { return Utils.getString(me, R.string.LTT_PE_BIC); }
		if (rRC.equals("LTT_PE_BOH")) { return Utils.getString(me, R.string.LTT_PE_BOH); }
		if (rRC.equals("LTT_PE_FAU")) { return Utils.getString(me, R.string.LTT_PE_FAU); }
		if (rRC.equals("LTT_PE_GOD")) { return Utils.getString(me, R.string.LTT_PE_GOD); }
		if (rRC.equals("LTT_PON_BB_NTWK_NGD")) { return Utils.getString(me, R.string.LTT_PON_BB_NTWK_NGD); }
		if (rRC.equals("LTT_PON_EYE_MODEM_NGD")) { return Utils.getString(me, R.string.LTT_PON_EYE_MODEM_NGD); }
		if (rRC.equals("LTT_PON_EYE_NORMAL")) { return Utils.getString(me, R.string.LTT_PON_EYE_NORMAL); }
		if (rRC.equals("LTT_PON_MODEM_OSY")) { return Utils.getString(me, R.string.LTT_PON_MODEM_OSY); }
		if (rRC.equals("LTT_PON_TV_MODEM_NGD")) { return Utils.getString(me, R.string.LTT_PON_TV_MODEM_NGD); }
		if (rRC.equals("LTT_PON_TV_NORMAL")) { return Utils.getString(me, R.string.LTT_PON_TV_NORMAL); }

		if (rRC.equals("LTT_PON_EYE2_MODEM_NGD")) { return Utils.getString(me, R.string.LTT_PON_EYE2_MODEM_NGD); }
		if (rRC.equals("LTT_PON_EYE2_NORMAL")) { return Utils.getString(me, R.string.LTT_PON_EYE2_NORMAL); }
		if (rRC.equals("LTT_PON_IMS_MODEM_NGD")) { return Utils.getString(me, R.string.LTT_PON_IMS_MODEM_NGD); }
		if (rRC.equals("LTT_PON_IMS_NORMAL")) { return Utils.getString(me, R.string.LTT_PON_IMS_NORMAL); }

		if (rRC.equals("LTT_VOICE_BIC")) { return Utils.getString(me, R.string.LTT_VOICE_BIC); }
		if (rRC.equals("LTT_VOICE_BOH")) { return Utils.getString(me, R.string.LTT_VOICE_BOH); }
		if (rRC.equals("LTT_VOICE_FAU")) { return Utils.getString(me, R.string.LTT_VOICE_FAU); }
		if (rRC.equals("LTT_VOICE_GOD")) { return Utils.getString(me, R.string.LTT_VOICE_GOD); }
		if (rRC.equals("LTTF_ACCTNUM")) { return Utils.getString(me, R.string.LTTF_ACCTNUM); }
		if (rRC.equals("LTTF_BBNW")) { return Utils.getString(me, R.string.LTTF_BBNW); }
		if (rRC.equals("LTTF_CLOSE")) { return Utils.getString(me, R.string.LTTF_CLOSE); }
		if (rRC.equals("LTTF_DEV")) { return Utils.getString(me, R.string.LTTF_DEV); }
		if (rRC.equals("LTTF_HDR")) { return Utils.getString(me, R.string.LTTF_HDR); }
		if (rRC.equals("LTTF_ITO")) { return Utils.getString(me, R.string.LTTF_ITO); }
		if (rRC.equals("LTTF_LOGIN")) { return Utils.getString(me, R.string.LTTF_LOGIN); }
		if (rRC.equals("LTTF_MDM")) { return Utils.getString(me, R.string.LTTF_MDM); }
		if (rRC.equals("LTTF_TELNO")) { return Utils.getString(me, R.string.LTTF_TELNO); }
		if (rRC.equals("LTTF_TEST")) { return Utils.getString(me, R.string.LTTF_TEST); }
		if (rRC.equals("LTTM_ERR")) { return Utils.getString(me, R.string.LTTM_ERR); }
		if (rRC.equals("LTTM_RES_NF")) { return Utils.getString(me, R.string.LTTM_RES_NF); }
		if (rRC.equals("LTTM_TIMEOUT")) { return Utils.getString(me, R.string.LTTM_TIMEOUT); }
		// SR
		if (rRC.equals("LTTM_AUTOSR")) { return Utils.getString(me, R.string.LTTM_AUTOSR); }
		if (rRC.equals("LTTM_PORT_RESET")) { return Utils.getString(me, R.string.LTTM_PORT_RESET); }
		if (rRC.equals("LTTM_PSR")) { return Utils.getString(me, R.string.LTTM_PSR); }
		if (rRC.equals("LTTM_PSR_NG")) { return Utils.getString(me, R.string.LTTM_PSR_NG); }

		return ClnEnv.getRPCErrMsg(me.getApplicationContext(), rRC);
	}
	
	public final static String interpretRC_PlanMobMdu(Context me, Reply rRC) {
//		if (rRC.isEqual(Reply.RC_PLNMB_NOSUBN)) { return Utils.getString(me, R.string.PLNMBM_NOSUBN); }

//		if (rRC.isEqual(Reply.RC_PLNMB_IVSUBN)) { return Utils.getString(me.getActivity(), R.string.PLNMBM_IVSUBN); }
//
//		if (rRC.isEqual(Reply.RC_SIP)) { return Utils.getString(me.getActivity(), R.string.SHLM_SIP); }
//
//		if (rRC.isEqual(Reply.RC_WIP)) { return Utils.getString(me.getActivity(), R.string.SHLM_WIP); }

		return ClnEnv.getRPCErrMsg(me, rRC.getCode());
	}
	
	
    public final static String interpretRC_SRMdu(Context cxt, String rRC) {
		if (rRC.equals(RC.SR_IVCTNAME)) {return Utils.getString(cxt, R.string.MYHKT_SR_ERR_INVALID_NAME);}
		if (rRC.equals(RC.SR_NONEN_NAME)) {return Utils.getString(cxt, R.string.MYHKT_SR_ERR_NOT_ENGLISH_NAME);}
		if (rRC.equals(RC.SR_IVCTNUM)) {return Utils.getString(cxt, R.string.MYHKT_SR_ERR_INVALID_TEL);}
		if (rRC.equals(RC.SR_IVAPPTTS)) {return Utils.getString(cxt, R.string.MYHKT_SR_ERR_INVALID_TIMESLOT);}
		if (rRC.equals(RC.SR_IVUPDTY)) {return Utils.getString(cxt, R.string.MYHKT_SR_ERR_NO_UPDATE_CHANGE);}
		return ClnEnv.getRPCErrMsg(cxt, rRC);
	}
	
	
	// LTS - Translate Reply getCode() to screen-specific string
	public final static String interpretRC_BinqLtsMdu(Context ctx, String rRC) {
			if (rRC.equals(RC.LTSA_UNSUCC)) { return Utils.getString(ctx, R.string.BILTM_GW_UNSUCC); }

//			if (rRC.equals(RC.LTSA_UXPACTN)) { return Utils.getString(getActivity(), R.string.BILTM_GW_UXPACTN); }

			if (rRC.equals(RC.BINQ_BIERR)) { return Utils.getString(ctx, R.string.BILTM_BIERR); }

//			if (rRC.equals(RC.BUPD_ADR_MAXLEN)) { return Utils.getString(getActivity(), R.string.BUPLTM_IVADRLEN); }

//			if (rRC.equals(RC.BUPD_ADR_MAXTLEN)) { return Utils.getString(getActivity(), R.string.BUPLTM_IVADRTLEN); }

			if (rRC.equals(RC.BIIF_NIL_ADR)) { return Utils.getString(ctx, R.string.BUPLTM_ADR_IS_NIL); }
			if (rRC.equals(RC.BIIF_IL_ADR)) { return Utils.getString(ctx, R.string.BILLINFO_MAXLENEX); }
			if (rRC.equals(RC.BIIF_NA_ADR)) { return Utils.getString(ctx, R.string.BUPLTM_ADR_NOTASC); }
//			if (rRC.equals(RC.BUPD_ADR_NOTASC)) { return Utils.getString(getActivity(), R.string.BUPLTM_ADR_NOTASC); }

			if (rRC.equals(RC.BIIF_IV_LANG)) { return Utils.getString(ctx, R.string.BUPLTM_IVLANG); }

			if (rRC.equals(RC.BIIF_NIL_EMAIL)) { return Utils.getString(ctx, R.string.BUPLTM_EMAIL_IS_NIL); }
			
//			if (rRC.equals(RC.BUPD_NACTMAIL) || rRC.equals(RC.BUPD_IVCTMAIL) || rRC.equals(RC.BUPD_ILCTMAIL) || rRC.equals(RC.LTSA_REQ_EMAIL)) { return Utils.getString(getActivity(), R.string.BUPLTF_IVEMAIL); }
			if (rRC.equals(RC.BIIF_NA_EMAIL) || rRC.equals(RC.BIIF_IL_EMAIL) || rRC.equals(RC.BIIF_IV_EMAIL)) { return Utils.getString(ctx, R.string.BUPIMM_IVEMAIL); }
			
			if (rRC.equals(RC.BIIF_IV_SMSNTF)) return (Utils.getString(ctx, R.string.BUPLTF_IVMOB));
			if (rRC.equals(RC.NOT_AUTH)) {			
				return Utils.getString(ctx, R.string.PLNMBM_NOTAUT_GNL); 
			}
			// NRF
			if (rRC.equals(RC.NRF)) { return Utils.getString(ctx, R.string.BILTM_NOBILL); }

			return ClnEnv.getRPCErrMsg(ctx.getApplicationContext(), rRC);
		}

		// Translate Reply getCode() to screen-specific string
		// PCD / TV
		public final static String interpretRC_BinqImsMdu(Context ctx, String rRC) {
			if (rRC.equals(RC.IMSC_UNSUCC)) { return Utils.getString(ctx, R.string.BIIMM_GW_UNSUCC); }

//			if (rRC.equals(RC.IMSC_UXPACTN)) { return Utils.getString(me.getActivity(), R.string.BIIMM_GW_UXPACTN); }

			if (rRC.equals(RC.LTSA_UNSUCC)) { return Utils.getString(ctx, R.string.BIIMM_GW_UNSUCC); }

//			if (rRC.equals(RC.LTSA_UXPACTN)) { return Utils.getString(me.getActivity(), R.string.BIIMM_GW_UXPACTN); }

			if (rRC.equals(RC.BINQ_BIERR)) { return Utils.getString(ctx, R.string.BIIMM_BIERR); }

//			if (rRC.equals(RC.IMSB_RTOS_NOT_SERVED)) { return Utils.getString(me.getActivity(), R.string.BIIMM_RTOS_NOT_SERVED); }

//			if (rRC.equals(RC.IMSB_RTOS_NNOR)) { return Utils.getString(me.getActivity(), R.string.BIIMM_RTOS_NNOR); }

//			if (rRC.equals(RC.IMSB_RTOS_FAIL)) { return Utils.getString(me.getActivity(), R.string.BIIMM_RTOS_FAIL); }
	//
//			if (rRC.equals(RC.IMSB_UXPACTN)) { return Utils.getString(me.getActivity(), R.string.BIIMM_GW_UXPACTN); }
	//
//			if (rRC.equals(RC.BUPD_ADR_MAXLEN)) { return Utils.getString(me.getActivity(), R.string.BUPIMM_IVADRLEN); }
	//
//			if (rRC.equals(RC.BUPD_ADR_MAXTLEN)) { return Utils.getString(me.getActivity(), R.string.BUPIMM_IVADRTLEN); }
	//
			if (rRC.equals(RC.BIIF_NIL_ADR)) { return Utils.getString(ctx, R.string.BUPIMM_ADR_IS_NIL); }
			if (rRC.equals(RC.BIIF_IL_ADR)) { return Utils.getString(ctx, R.string.BILLINFO_MAXLENEX); }
			if (rRC.equals(RC.BIIF_NA_ADR)) { return Utils.getString(ctx, R.string.BUPIMM_ADR_NOTASC); }
//			if (rRC.equals(RC.BUPD_ADR_NOTASC)) { return Utils.getString(me.getActivity(), R.string.BUPIMM_ADR_NOTASC); }

//			if (rRC.equals(RC.BUPD_NACTMAIL) || rRC.equals(RC.BUPD_IVCTMAIL) || rRC.equals(RC.BUPD_ILCTMAIL) || rRC.equals(RC.LTSA_REQ_EMAIL)) { return Utils.getString(me.getActivity(), R.string.BUPIMM_IVEMAIL); }
			if (rRC.equals(RC.BIIF_NA_EMAIL) || rRC.equals(RC.BIIF_IL_EMAIL) || rRC.equals(RC.BIIF_IV_EMAIL)) { return Utils.getString(ctx, R.string.BUPIMM_IVEMAIL); }
			if (rRC.equals(RC.NOT_AUTH)) {			
				return Utils.getString(ctx, R.string.PLNMBM_NOTAUT_GNL); 
			}
			// NRF
			if (rRC.equals(RC.NRF)) { return Utils.getString(ctx, R.string.BIIMM_NOBILL); }

			return ClnEnv.getRPCErrMsg(ctx.getApplicationContext(), rRC);
		}

		// MOB - Translate Reply getCode() to screen-specific string
		public final static String interpretRC_BinqMobMdu(Context ctx, String rRC, int lob) {
			if (rRC.equals(RC.MOBA_UNSUCC)) { return Utils.getString(ctx, R.string.BIMBM_GW_UNSUCC); }

			if (rRC.equals(RC.MOBB_UNSUCC)) { return Utils.getString(ctx, R.string.BIMBM_GW_UNSUCC); }

//			if (rRC.equals(RC.MOBB_UXPACTN)) { return Utils.getString(me.getActivity(), R.string.BIMBM_GW_UXPACTN); }

			if (rRC.equals(RC.LTSA_UNSUCC)) { return Utils.getString(ctx, R.string.BIMBM_GW_UNSUCC); }

//			if (rRC.equals(RC.LTSA_UXPACTN)) { return Utils.getString(me.getActivity(), R.string.BIMBM_GW_UXPACTN); }

			if (rRC.equals(RC.BINQ_BIERR)) { return Utils.getString(ctx, R.string.BIMBM_BIERR); }

//			if (rRC.equals(RC.MOBA_HDUN_ALDY)) { return Utils.getString(me.getActivity(), R.string.BIMBM_HDUN_ALDY); }
	//
//			if (rRC.equals(RC.MOBA_NCBAR)) { return Utils.getString(me.getActivity(), R.string.BIMBM_NCBAR); }
	//
//			if (rRC.equals(RC.MOBA_RTOS_FAIL)) { return Utils.getString(me.getActivity(), R.string.BIMBM_RTOS_FAIL); }
	//
//			if (rRC.equals(RC.BUPD_ADR_MAXLEN)) { return Utils.getString(me.getActivity(), R.string.BUPMBM_IVADRLEN); }
	//
//			if (rRC.equals(RC.BUPD_ADR_MAXTLEN)) { return Utils.getString(me.getActivity(), R.string.BUPMBM_IVADRTLEN); }
	//
			if (rRC.equals(RC.BIIF_NIL_ADR)) { return Utils.getString(ctx, R.string.BUPMBM_ADR_IS_NIL); }
			if (rRC.equals(RC.BIIF_IL_ADR)) { return Utils.getString(ctx, R.string.BILLINFO_MAXLENEX); }
			if (rRC.equals(RC.BIIF_NA_ADR)) { return Utils.getString(ctx, R.string.BUPMBM_ADR_NOTASC); }
			if (rRC.equals(RC.BIIF_NIL_EMAIL)) { return Utils.getString(ctx, R.string.BUPLTM_EMAIL_IS_NIL); }
	//
//			if (rRC.equals(RC.BUPD_ADR_NOTASC)) { return Utils.getString(me.getActivity(), R.string.BUPMBM_ADR_NOTASC); }

//			if (rRC.equals(RC.BUPD_NACTMAIL) || rRC.equals(RC.BUPD_IVCTMAIL) || rRC.equals(RC.BUPD_ILCTMAIL) || rRC.equals(RC.LTSA_REQ_EMAIL)) { return Utils.getString(me.getActivity(), R.string.BUPMBM_IVEMAIL); }
			
			if (rRC.equals(RC.BIIF_IV_LANG)) { return Utils.getString(ctx, R.string.BUPMBM_IVLANG); }
			if (rRC.equals(RC.BIIF_NA_EMAIL) || rRC.equals(RC.BIIF_IL_EMAIL) || rRC.equals(RC.BIIF_IV_EMAIL)) { return Utils.getString(ctx, R.string.BUPIMM_IVEMAIL); }
			if (lob == R.string.CONST_LOB_1010 || lob == R.string.CONST_LOB_IOI) {
				if (rRC.equals(RC.API_NOTAUTH)) { 
					return Utils.getString(ctx, R.string.MYMOB_RC_CVI_UNSUCC_101); 
				} else if (rRC.equals(RC.CVI_UNSUCC)) { 
					return Utils.getString(ctx, R.string.MYMOB_RC_CVI_UNSUCC_101); 
				}
			} else if (lob == R.string.CONST_LOB_O2F) {
				if (rRC.equals(RC.API_NOTAUTH)) { 
					return Utils.getString(ctx, R.string.MYMOB_RC_CVI_UNSUCC_O2F); 
				} else if (rRC.equals(RC.CVI_UNSUCC)) { 
					return Utils.getString(ctx, R.string.MYMOB_RC_CVI_UNSUCC_O2F); 
				}
			}
			
			if (rRC.equals(RC.NOT_AUTH)) {
				if (lob == R.string.CONST_LOB_1010 || lob == R.string.CONST_LOB_IOI) {
					return Utils.getString(ctx, R.string.PLNMBM_NOTAUT_101); 
				} else {
					return Utils.getString(ctx, R.string.PLNMBM_NOTAUT_GNL); 
				}
			}
			
			// RC_NRF
			if (rRC.equals(RC.NRF)) { return Utils.getString(ctx, R.string.BIMBM_NOBILL); }
			
			return ClnEnv.getRPCErrMsg(ctx.getApplicationContext(), rRC);
			
		
		}

}
