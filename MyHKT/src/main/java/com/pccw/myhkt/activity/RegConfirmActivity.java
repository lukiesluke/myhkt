package com.pccw.myhkt.activity;

import android.content.Intent;
import android.graphics.Point;
import android.os.Bundle;
import android.text.TextPaint;
import android.text.style.ClickableSpan;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.androidquery.AQuery;
import com.pccw.dango.shared.cra.AcMainCra;
import com.pccw.dango.shared.entity.CustRec;
import com.pccw.dango.shared.entity.SubnRec;
import com.pccw.dango.shared.entity.SveeRec;
import com.pccw.dango.shared.tool.RC;
import com.pccw.myhkt.APIsManager;
import com.pccw.myhkt.APIsResponse;
import com.pccw.myhkt.ClnEnv;
import com.pccw.myhkt.DialogHelper;
import com.pccw.myhkt.R;
import com.pccw.myhkt.Utils;
import com.pccw.myhkt.lib.ui.AAQuery;
import com.pccw.myhkt.lib.ui.HKTButton;
import com.pccw.myhkt.lib.ui.RegConfirmView;
import com.pccw.myhkt.lib.ui.RegImageButton;
import com.pccw.myhkt.model.SubService;
import com.pccw.wheat.shared.tool.Reply;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/************************************************************************
 * File : RegConfirmActivity.java 
 * Desc : Reg Confirm 
 * Name : RegConfirmActivity
 * by : Andy Wong 
 * Date : 30/10/2015
 * Change History:
 * Date       Modified By		Description
 * ---------- ----------------	-------------------------------
 * 30/10/2015 Andy Wong 		-First draft
 *************************************************************************/
public class RegConfirmActivity extends BaseActivity {
    // Common Components
    private boolean debug = false;
    private static RegConfirmActivity me;
    private String TAG = this.getClass().getName();
    private AQuery aq;
    private final int colMaxNum = 3;
    private int deviceWidth = 0;
    private int colWidth = 0;
    private int buttonPadding = 0;
    private int extralinespace = 0;
    private int regConfirmViewHeight = 0;
    private int greylineHeight = 0;
    private int basePadding = 0;
    private List<RegConfirmItem> regConfirmItemList;
    private RegImageButton regImageButon;
    private TextView textViewTc, textViewPri;

    private Button canceBtn;
    private Button confirmBtn;
    private HashMap<String, String> serviceMap;

    private CustRec custRec;
    private SveeRec sveeRec;
    private SubnRec subnRec;
    private Boolean isHKID;

    private String domainURL = APIsManager.PRD_FTP;

    @Override
    public final void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        me = this;
        debug = getResources().getBoolean(R.bool.DEBUG);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            custRec = (CustRec) extras.getSerializable(RegBasicActivity.BUNDLE_CUSTREC);
            sveeRec = (SveeRec) extras.getSerializable(RegBasicActivity.BUNDLE_SVEEREC);
            subnRec = (SubnRec) extras.getSerializable(RegBasicActivity.BUNDLE_SUBNREC);
            ClnEnv.updateUILocale(getBaseContext(), ClnEnv.getAppLocale(getBaseContext()));
        }
        domainURL = ClnEnv.getPref(me, me.getString(R.string.CONST_PREF_FTP), APIsManager.PRD_FTP);
        setContentView(R.layout.activity_regconfirm);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        ClnEnv.updateUILocale(getBaseContext(), ClnEnv.getAppLocale(getBaseContext()));
    }

    @Override
    protected final void onStart() {
        initData();
        super.onStart();

        setClickListener();
    }

    @Override
    protected final void onResume() {
        super.onResume();
        moduleId = getResources().getString(R.string.MODULE_REG);

        // Update Locale
        ClnEnv.updateUILocale(getBaseContext(), ClnEnv.getAppLocale(getBaseContext()));
    }

    @Override
    protected final void onPause() {
        super.onPause();
        if (debug) Log.d("MainMenuActivity", "");
        //		Log.d("MainMenuActivity", "getLayout Width: "+aq.id(R.id.mainmenu_topbanner).getImageView().getWidth());
    }

    @Override
    protected final void onStop() {
        super.onStop();
    }

    private void initData() {
        //init the service map
        serviceMap = new HashMap<String, String>();
        serviceMap.put(SubnRec.LOB_PCD, Utils.getString(this, R.string.SHLM_LB_LOB_PCD));
        serviceMap.put(SubnRec.LOB_LTS, Utils.getString(this, R.string.SHLM_LB_LOB_LTS));
        serviceMap.put(SubnRec.LOB_TV, Utils.getString(this, R.string.SHLM_LB_LOB_TV));
        serviceMap.put(SubnRec.WLOB_X101, Utils.getString(this, R.string.SHLM_LB_LOB_1010));
        serviceMap.put(SubnRec.WLOB_CSL, Utils.getString(this, R.string.SHLM_LB_LOB_MOB));
        serviceMap.put(SubnRec.LOB_CSP, Utils.getString(this, R.string.SHLM_LB_LOB_CSP));

        //* init the regconfirm list
        regConfirmItemList = new ArrayList<RegConfirmItem>();

        if (subnRec.lob.equals(SubnRec.LOB_TV)) {
            /* For TV, make sure srv_num is empty and acct_num has value*/
            regConfirmItemList.add(new RegConfirmItem(R.id.regconfirm_view_service, getResString(R.string.myhkt_confirm_head1), serviceMap.get(subnRec.lob) + "/" + subnRec.acctNum));
        } else {
            regConfirmItemList.add(new RegConfirmItem(R.id.regconfirm_view_service, getResString(R.string.myhkt_confirm_head1), serviceMap.get(subnRec.lob) + "/" + subnRec.srvNum));
        }

        regConfirmItemList.add(new RegConfirmItem(R.id.regconfirm_view_hkid, custRec.docTy.equals(CustRec.TY_HKID) ? getResString(R.string.myhkt_confirm_hkid) : getResString(R.string.myhkt_confirm_passport), custRec.docNum));
        regConfirmItemList.add(new RegConfirmItem(R.id.regconfirm_view_loginid, getResString(R.string.myhkt_confirm_loginid), sveeRec.loginId));
        regConfirmItemList.add(new RegConfirmItem(R.id.regconfirm_view_nickname, getResString(R.string.myhkt_confirm_nickname), sveeRec.nickname));
        regConfirmItemList.add(new RegConfirmItem(R.id.regconfirm_view_mobile_number_reset, getResString(R.string.myhkt_confirm_reset), sveeRec.ctMob));

        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        deviceWidth = size.x;
        colWidth = (deviceWidth - (int) getResources().getDimension(R.dimen.extralinespace) * 2) / colMaxNum;
        buttonPadding = (int) getResources().getDimension(R.dimen.reg_logo_padding);
        extralinespace = (int) getResources().getDimension(R.dimen.extralinespace);
        basePadding = (int) getResources().getDimension(R.dimen.basePadding);
        regConfirmViewHeight = (int) getResources().getDimension(R.dimen.reg_confirm_view_height);
        greylineHeight = (int) getResources().getDimension(R.dimen.greyline_height);

    }

    protected final void initUI2() {
        aq = new AQuery(this);
        AAQuery aaq = new AAQuery(this);
        aaq.navBarButton(R.id.navbar_button_left, R.drawable.btn_back);
        aaq.navBarTitle(R.id.navbar_title, getResString(R.string.myhkt_account_reg));

        aq.id(R.id.navbar_button_left).clicked(this, "onBackPressed");
        aq.id(R.id.regconfirm_heading).text(getResString(R.string.myhkt_confirm_head2)).height(40);
        aq.id(R.id.regconfirm_heading).getTextView().setPadding(basePadding, extralinespace, basePadding, 0);

        regImageButon = (RegImageButton) aq.id(R.id.regconfirm_icon).getView();
        regImageButon.initViews(this, SubService.getResId(subnRec.lob), subnRec.lob, 0, 0, regConfirmViewHeight);
        regImageButon.setPadding(basePadding, extralinespace / 2, basePadding, extralinespace / 2);
        regImageButon.setBg(R.drawable.logo_container_gray);

        setLine(R.id.regconfirm_line1);
        setLine(R.id.regconfirm_line2);
        setLine(R.id.regconfirm_line3);
        setLine(R.id.regconfirm_line4);
        setLine(R.id.regconfirm_line5);

        //init info list
        for (int itemCount = 0; itemCount < regConfirmItemList.size(); itemCount++) {
            RegConfirmView regConfirmView = (RegConfirmView) aq.id(regConfirmItemList.get(itemCount).res).getView();
            regConfirmView.initViews(this, regConfirmViewHeight, regConfirmItemList.get(itemCount).title, regConfirmItemList.get(itemCount).content);
            regConfirmView.setPadding(itemCount == 0 ? 0 : basePadding, 0, basePadding, 0);
        }

        aq.id(R.id.regconfirm_txt_agree).text(getResString(R.string.REGF_TNC_BTN));
        aq.id(R.id.regconfirm_txt_agree).height(LinearLayout.LayoutParams.WRAP_CONTENT, false);
        aq.id(R.id.regconfirm_txt_agree).clicked(this, "onClick");

        aq.id(R.id.regconfirm_option_txt_agree).text(getResString(R.string.REGF_TNC_BTN_OPT));
        aq.id(R.id.regconfirm_option_txt_agree).height(LinearLayout.LayoutParams.WRAP_CONTENT, false);
        aq.id(R.id.regconfirm_option_txt_agree).clicked(this, "onClick");

        //Set the general conditions and privacy statement clickable
        Utils.setTextViewClickableSpan(aq.id(R.id.regconfirm_txt_agree).getTextView(),
                new String[]{
                        getResString(R.string.REGF_TNC_GC_LINK),
                        getResString(R.string.REGF_TNC_PS_LINK),
                        getResString(R.string.REGF_TNC_PICS_LINK)
                },
                new ClickableSpan[]{
                        GENERAL_CONDITION_CLICK,
                        HKT_PRIVACY_STATEMENT_CLICK,
                        HKT_PIC_CLICK
                });

        aq.id(R.id.regconfirm_relative_layout).getView().setPadding(basePadding, extralinespace, basePadding, 0);
        aq.id(R.id.regconfirm_option_in_out).getView().setPadding(basePadding, 0, basePadding, 0);

        canceBtn = (HKTButton) aq.id(R.id.regconfirm_btn_cancel).getView();
        canceBtn.setText(getResString(R.string.myhkt_confirm_cancel));
        LinearLayout.LayoutParams params_cancel = (LayoutParams) canceBtn.getLayoutParams();
        params_cancel.width = ViewGroup.LayoutParams.MATCH_PARENT;
        params_cancel.rightMargin = buttonPadding;
        canceBtn.setLayoutParams(params_cancel);
        confirmBtn = (HKTButton) aq.id(R.id.regconfirm_btn_confirm).getView();
        confirmBtn.setText(getResString(R.string.myhkt_confirm_submit));
        LinearLayout.LayoutParams params_confirm = (LayoutParams) confirmBtn.getLayoutParams();
        params_confirm.leftMargin = buttonPadding;
        params_confirm.width = ViewGroup.LayoutParams.MATCH_PARENT;
        confirmBtn.setLayoutParams(params_confirm);
        confirmBtn.setEnabled(false);
        confirmBtn.setAlpha(0.2f);
        aq.id(R.id.regconfirm_checkbox).getCheckBox().setOnCheckedChangeListener(new OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (!isChecked) {
                    confirmBtn.setEnabled(false);
                    confirmBtn.setAlpha(0.2f);
                } else {
                    confirmBtn.setEnabled(true);
                    confirmBtn.setAlpha(1.0f);
                }
            }
        });
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.regconfirm_btn_confirm:
                if (aq.id(R.id.regconfirm_checkbox).isChecked()) {

                    Toast.makeText(v.getContext(), "Reg Success", Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(getApplicationContext(), MainMenuActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    startActivity(intent);
                    overridePendingTransition(R.anim.left_slide_in, R.anim.right_slide_out);
                    finish();
                } else {
                    DialogHelper.createSimpleDialog(RegConfirmActivity.this, getResString(R.string.myhkt_confirm_tick));
                }
                break;

            case R.id.regconfirm_txt_agree:
                aq.id(R.id.regconfirm_checkbox).getCheckBox().performClick();
                break;
            case R.id.regconfirm_option_txt_agree:
                aq.id(R.id.regconfirm_option_checkbox).getCheckBox().performClick();
                break;
        }
    }

    /**
     * Description: Local clickable span for 'General Conditions of "My HKT" Portal'
     * Created: 02/26/2018 - AMoiz Esmail
     */
    ClickableSpan GENERAL_CONDITION_CLICK = new ClickableSpan() {
        @Override
        public void onClick(View view) {
            navigateToBrowserActivity(isZh ? domainURL + "/mba/html/tnc_zh.htm" : domainURL + "/mba/html/tnc_en.htm",
                    getResString(R.string.myhkt_confirm_ack2));
        }

        @Override
        public void updateDrawState(TextPaint ds) {
            super.updateDrawState(ds);
            ds.setColor(getResources().getColor(R.color.hkt_buttonblue));
        }
    };

    /**
     * Description: Local clickable span for 'HKT Privacy Statement'
     * Created: 02/26/2018 - AMoiz Esmail
     */
    ClickableSpan HKT_PRIVACY_STATEMENT_CLICK = new ClickableSpan() {
        @Override
        public void onClick(@NonNull View view) {
            navigateToBrowserActivity(isZh ? "http://www.hkt.com/privacy-statement/for-customers?locale=zh" : "http://www.hkt.com/privacy-statement/for-customers?locale=en",
                    getResString(R.string.myhkt_confirm_ack3));
        }

        @Override
        public void updateDrawState(@NonNull TextPaint ds) {
            super.updateDrawState(ds);
            ds.setColor(getResources().getColor(R.color.hkt_buttonblue));
        }
    };

    /**
     * Description: Local clickable span for 'HKT PIC Statement'
     * Created: 05/38/2018 - AMoiz Esmail
     */
    ClickableSpan HKT_PIC_CLICK = new ClickableSpan() {
        @Override
        public void onClick(@NonNull View view) {
            navigateToBrowserActivity(isZh ? domainURL + "/mba/html/pics_zh.htm" : domainURL + "/mba/html/pics_en.htm",
                    getResString(R.string.REGF_PIC_TITLE));
        }

        @Override
        public void updateDrawState(@NonNull TextPaint ds) {
            super.updateDrawState(ds);
            ds.setColor(getResources().getColor(R.color.hkt_buttonblue));
        }
    };

    private void navigateToBrowserActivity(String url, String title) {
        Intent intent = new Intent(getApplicationContext(), BrowserActivity.class);
        Bundle bundle = new Bundle();
        bundle.putString(BrowserActivity.INTENTMODULEID, moduleId);
        bundle.putString(BrowserActivity.INTENTLINK, url);
        bundle.putString(BrowserActivity.INTENTTITLE, title);
        intent.putExtras(bundle);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivity(intent);
        overridePendingTransition(R.anim.right_slide_in, R.anim.left_slide_out);
    }

    //Set onClickListener for the views
    public void setClickListener() {
        confirmBtn.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                if (aq.id(R.id.regconfirm_checkbox).isChecked()) {
                    //set the sveerec promo option
                    if (aq.id(R.id.regconfirm_option_checkbox).getCheckBox().isChecked()) {
                        sveeRec.promoOpt = SveeRec.OPT_IN;
                    } else {
                        sveeRec.promoOpt = SveeRec.OPT_OUT;
                    }
                    doRegister();
                } else {
                    DialogHelper.createSimpleDialog(RegConfirmActivity.this, getResString(R.string.myhkt_confirm_tick));
                }
            }
        });

        canceBtn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    public void setLine(int resId) {
        aq.id(resId).image(R.drawable.greyline).height(greylineHeight, false);
    }

    // Android Device Back Button Handling
    public final void onBackPressed() {
        //
        //		Intent intent = new Intent(getApplicationContext(), RegBasicActivity.class);
        //		intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        //		startActivity(intent);
        finish();
        overridePendingTransition(R.anim.left_slide_in, R.anim.right_slide_out);
    }

    private void doRegister() {
        AcMainCra acMainCra = new AcMainCra();
        acMainCra.setISveeRec(sveeRec);
        acMainCra.setICustRec(custRec);
        SubnRec newSRec = subnRec.copyMe();
        if (newSRec.lob.equals(SubnRec.WLOB_CSL)) {
            newSRec.lob = SubnRec.LOB_MOB;
        }
        if (newSRec.lob.equals(SubnRec.LOB_101) || newSRec.lob.equals(SubnRec.WLOB_X101)) {
            newSRec.lob = SubnRec.LOB_IOI;
        }

        acMainCra.setISubnRec(newSRec);
        APIsManager.doRegister(this, acMainCra, false);
    }

    public void debugLog(String tag, String msg) {
        if (debug) Log.i(tag, msg);
    }

    // Translate Reply getCode() to screen-specific string
    private static String interpretRC_RegnMdu(Reply rRC) {
        if (isReqSpecMsg4DocTy()) {

            if (rRC.getCode().equals(RC.NO_CUSTOMER)) {
                return Utils.getString(me, R.string.REGM_NO_CUSTOMER_NOPP);
            } else if (rRC.getCode().equals(RC.NO_SUBN)) {
                return Utils.getString(me, R.string.REGM_NO_SUBN_NOPP);
            }
        }

        if (rRC.getCode().equals(RC.INACTIVE_CUST)) {
            return Utils.getString(me, R.string.REGM_INACTIVE_CUST);
        }
        if (rRC.getCode().equals(RC.CUS_ALDY_REG)) {
            return Utils.getString(me, R.string.REGM_CUS_ALDY_REG);
        }

//		if (rRC.getCode().equals(RC.RCUS_NALOGINID)) { return Utils.getString(me, R.string.REGM_NALOGINID); }

        if (rRC.getCode().equals(RC.RCUS_IVLOGINID)) {
            return Utils.getString(me, R.string.REGM_IVLOGINID);
        }

        if (rRC.getCode().equals(RC.RCUS_ILLOGINID)) {
            return Utils.getString(me, R.string.REGM_ILLOGINID);
        }

//		if (rRC.getCode().equals(RC.RCUS_RESV_LOGIN)) { return Utils.getString(me, R.string.REGM_RESV_LOGIN); }

//		if (rRC.getCode().equals(RC.RCUS_NAPWD)) { return Utils.getString(me, R.string.REGM_NAPWD); }

        if (rRC.getCode().equals(RC.SVEE_NLPWD)) {
            return Utils.getString(me, R.string.REGM_ILPWD);
        }

        if (rRC.getCode().equals(RC.SVEE_IVPWDCBN)) {
            return Utils.getString(me, R.string.REGM_IVPWDCBN);
        }

        if (rRC.getCode().equals(RC.SVEE_ILNICKNAME)) {
            return Utils.getString(me, R.string.REGM_ILNICKNAME);
        }

        if (rRC.getCode().equals(RC.SVEE_RESV_NCKNM)) {
            return Utils.getString(me, R.string.REGM_RESV_NCKNM);
        }

//		if (rRC.getCode().equals(RC.RCUS_NACTMAIL)) { return Utils.getString(me, R.string.REGM_NACTMAIL); }

        if (rRC.getCode().equals(RC.SVEE_NLCTMAIL)) {
            return Utils.getString(me, R.string.REGM_ILCTMAIL);
        }

        if (rRC.getCode().equals(RC.SVEE_IVCTMAIL)) {
            return Utils.getString(me, R.string.REGM_IVCTMAIL);
        }

        if (rRC.getCode().equals(RC.SVEE_IVCTMOB)) {
            return Utils.getString(me, R.string.REGM_IVMOB);
        }

        if (rRC.getCode().equals(RC.SVEE_IVMOBALRT)) {
            return Utils.getString(me, R.string.REGM_IVMOBALRT);
        }

        if (rRC.getCode().equals(RC.SVEE_IVLANG)) {
            return Utils.getString(me, R.string.REGM_IVLANG);
        }

        if (rRC.getCode().equals(RC.SVEE_IVSECQUS)) {
            return Utils.getString(me, R.string.REGM_IVSECQUS);
        }

        if (rRC.getCode().equals(RC.SVEE_IVSECANS)) {
            return Utils.getString(me, R.string.REGM_IVSECANS);
        }

        if (rRC.getCode().equals(RC.SVEE_NLSECANS)) {
            return Utils.getString(me, R.string.REGM_ILSECANS);
        }

        if (rRC.getCode().equals(RC.CUST_IVDOCTY)) {
            return Utils.getString(me, R.string.REGM_IVDOCTY);
        }

        if (rRC.getCode().equals(RC.CUST_ILDOCNUM)) {
            return Utils.getString(me, R.string.REGM_ILDOCNUM);
        }

        if (rRC.getCode().equals(RC.SUBN_IVLOB)) {
            return Utils.getString(me, R.string.REGM_IVLOB);
        }

        if (rRC.getCode().equals(RC.SUBN_NLACCTNUM)) {
            return Utils.getString(me, R.string.REGM_ILACCTNUM);
        }

        if (rRC.getCode().equals(RC.SUBN_IVACCTNUM)) {
            return Utils.getString(me, R.string.REGM_IVACCTNUM);
        }

        if (RC.SUBN_IVSN_LTS.equals(rRC.getCode())) {
            return Utils.getString(me, R.string.REGM_IVAN_LTS);
        }

        if (RC.SUBN_IVSN_MOB.equals(rRC.getCode())) {
            return Utils.getString(me, R.string.REGM_IVAN_MOB);
        }

//		if (rRC.getCode().equals(RC.SUBN_IVAN_101)) { return Utils.getString(me, R.string.REGM_IVAN_101); }

//		if (rRC.getCode().equals(RC.SUBN_IVAN_NE)) { return Utils.getString(me, R.string.REGM_IVAN_NE); }

        if (rRC.getCode().equals(RC.SUBN_IVSN_PCD)) {
            return Utils.getString(me, R.string.REGM_IVAN_PCD);
        }

        if (rRC.getCode().equals(RC.SUBN_IVAN_TV)) {
            return Utils.getString(me, R.string.REGM_IVAN_TV);
        }

        if (rRC.getCode().equals(RC.SUBN_ILSRVNUM)) {
//			if (me.equals(SubnRec.LOB_LTS)) {
//				return Utils.getString(me, R.string.REGM_ILSRVNUM_LTS);
//			} else if (me.lob.equals(SubnRec.LOB_MOB)) {
//				return Utils.getString(me, R.string.REGM_ILSRVNUM_MOB);
//			} else if (me.lob.equals(SubnRec.LOB_NE)) {
//				return Utils.getString(me, R.string.REGM_ILSRVNUM_MOB);
//			} else if (me.lob.equals(SubnRec.LOB_PCD)) {
//				return Utils.getString(me, R.string.REGM_ILSRVNUM_PCD);
//			} else if (me.lob.equals(SubnRec.LOB_TV)) {
//				return Utils.getString(me, R.string.REGM_NO_SUBN);
//			} else {
            return Utils.getString(me, R.string.REGM_ILSRVNUM);
//			}
        }

        if (RC.SUBN_IVSN_LTS.equals(rRC.getCode())) {
            return Utils.getString(me, R.string.REGM_IVSN_LTS);
        }

        if (RC.SUBN_IVSN_MOB.equals(rRC.getCode())) {
            return Utils.getString(me, R.string.REGM_IVSN_MOB);
        }

        if (rRC.getCode().equals(RC.NO_CUSTOMER)) {
            return Utils.getString(me, R.string.REGM_NO_CUSTOMER);
        }

        if (rRC.getCode().equals(RC.NO_SUBN)) {
            return Utils.getString(me, R.string.REGM_NO_SUBN);
        }

        if (RC.CUS_ALDY_REG.equals(rRC.getCode())) {
            return Utils.getString(me, R.string.REGM_CUS_ALDY_REG);
        }

        if (rRC.getCode().equals(RC.LOGIN_ID_EXIST)) {
            return Utils.getString(me, R.string.REGM_LOGIN_ID_EXIST);
        }

//		if (rRC.getCode().equals(RC.CTMAIL_EXIST)) { return Utils.getString(me, R.string.REGM_CTMAIL_EXIST); }

//		if (rRC.getCode().equals(RC.NO_CUSTOMER_NOPP)) { return Utils.getString(me, R.string.REGM_NO_CUSTOMER_NOPP); }

//		if (rRC.getCode().equals(RC.NO_SUBN_NOPP)) { return Utils.getString(me, R.string.REGM_NO_SUBN_NOPP); }

        // Undefined error mapping
        return ClnEnv.getRPCErrMsg(me, rRC.getCode());
    }

    private static boolean isReqSpecMsg4DocTy() {
        /* Special Handling for 101 and O2F */

        String rLob;
        if (me.subnRec == null || me.custRec == null) {
            return false;
        }

        rLob = me.subnRec.lob;
        if (rLob.equals(SubnRec.LOB_101)) {
            return me.custRec.docTy.equals(CustRec.TY_PASSPORT);
        } else if (rLob.equals(SubnRec.LOB_O2F)) {
            return me.custRec.docTy.equals(CustRec.TY_PASSPORT);
        } else if (rLob.equals(SubnRec.LOB_MOB) || rLob.equals(SubnRec.LOB_IOI)) {
            if (me.subnRec.acctNum.trim().length() == SubnRec.CSL_ACCTNUM_LEN) {
                return me.custRec.docTy.equals(CustRec.TY_PASSPORT);
            }
        }
        return (false);
    }

    public void onSuccess(APIsResponse response) {
        super.onSuccess(response);
        if (response != null) {
            if (APIsManager.HELO.equals(response.getActionTy())) {
                debugLog(TAG, "doHelo complete!!");
            } else if (APIsManager.SIGNUP.equals(response.getActionTy())) {
                DialogHelper.createRedirectActivationDialog(this, sveeRec.loginId, sveeRec.pwd);
            }
        }
    }

    public void onFail(APIsResponse response) {
        super.onFail(response);
        if (response != null) {
            if (APIsManager.SIGNUP.equals(response.getActionTy())) {
            }

            // General Error Message
            if (!"".equals(response.getMessage()) && response.getMessage() != null) {
                displayDialog(this, response.getMessage());
            } else if (RC.TCSESS_MM.equalsIgnoreCase(response.getReply().getCode())) {
                ivSessDialog();
            } else {
                displayDialog(this, interpretRC_RegnMdu(response.getReply()));
            }
        }
    }

    public static class RegConfirmItem {
        String title;
        String content;
        int res;

        RegConfirmItem(int res, String title, String content) {
            this.res = res;
            this.title = title;
            this.content = content;
        }
    }
}
