package com.pccw.myhkt;

import java.io.Serializable;

public class APIsRequest<T> implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5091367163071796176L;
	/**
	 * 
	 */
	private String path;
	private String actionTy;
	private T iCra;
	
	public APIsRequest(String path, String actionTy) {
		super();
		this.path = path;
		this.actionTy = actionTy;
	}
	
	public String getPath() {
		return path;
	}
	public void setPath(String path) {
		this.path = path;
	}
	public String getActionTy() {
		return actionTy;
	}
	public void setActionTy(String actionTy) {
		this.actionTy = actionTy;
	}

	public T getiCra() {
		return iCra;
	}

	public void setiCra(T iCra) {
		this.iCra = iCra;
	}
}