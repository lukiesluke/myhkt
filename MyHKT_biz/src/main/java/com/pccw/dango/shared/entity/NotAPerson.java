/*
    Unidentified User (likely for Web/API) in Dango
    
    Keywords
    --------
    $URL: $
    $Rev: $
    $Date: $
    $Author: $
*/

package com.pccw.dango.shared.entity;

import java.io.Serializable;


public class NotAPerson extends BaseUserEx implements Serializable
{
    private static final long serialVersionUID = -6952557558466779388L;


    public static void main(String rArg[])
    {
        System.out.println(getVer());
        return;
    }
    
    
    public static String getVer()
    {
        return ("$URL: $, $Rev: $");
    }
    
    
    public NotAPerson()
    {
        super();
    }
    
    
    public String getId()
    {
        return (ID_UNKN);
    }
    
    
    public String getType()
    {
        return (TY_NPSN);
    }
    
    
    public String getRole()
    {
        return ("");
    }
    
    
    public String[] getRghtAry()
    {
        return (new String[0]);
    }
    
    
    public boolean isAuth(String rRght)
    {
        return (false);
    }
}
