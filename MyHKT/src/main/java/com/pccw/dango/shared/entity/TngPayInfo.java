/*
    TNG Payment Information

    Keywords
    --------
    $URL: svn://10.87.120.207/dango/rel/v16.0/src/com/pccw/dango/shared/entity/TngPayInfo.java $
    $Rev: 948 $
    $Date: 2016-12-19 15:37:20 +0800 (¶g¤@, 19 ¤Q¤G¤ë 2016) $
    $Author: alpha.lau $
*/

package com.pccw.dango.shared.entity;

import java.io.Serializable;


public class TngPayInfo implements Serializable
{
    private static final long serialVersionUID = -1762191310011695148L;
    
    public String                   totalPrice;
    public String                   currency;
    public String                   merTradeNo;
    public String                   notifyUrl;
    public String                   returnUrl;
    public String                   remark;
    public String                   lang;
    
    
    public static void main(String rArg[])
    {
        System.out.println(getVer());
        return;
    }
    
    
    public static String getVer()
    {
        return ("$URL: svn://10.87.120.207/dango/rel/v16.0/src/com/pccw/dango/shared/entity/TngPayInfo.java $, $Rev: 948 $");
    }
    
    
    public TngPayInfo()
    {
        initAndClear();
    }
    
    
    final void initAndClear()
    {
        init();
        clear();
    }
    
    
    protected void init()
    {
    }
    
    
    public void clear()
    {
        totalPrice = "";
        currency   = "";
        merTradeNo = "";
        notifyUrl  = "";
        returnUrl  = "";
        remark     = "";
        lang       = "";
    }
    
    
    public TngPayInfo copyFrom(TngPayInfo rSrc)
    {
        totalPrice = rSrc.totalPrice;
        currency   = rSrc.currency;
        merTradeNo = rSrc.merTradeNo;
        notifyUrl  = rSrc.notifyUrl;
        returnUrl  = rSrc.returnUrl;
        remark     = rSrc.remark;
        lang       = rSrc.lang;
    
        return (this);
    }
    
    
    public TngPayInfo copyTo(TngPayInfo rDes)
    {
        rDes.copyFrom(this);
        return (rDes);
    }
    
    
    public TngPayInfo copyMe()
    {
        TngPayInfo                  rDes;
        
        rDes = new TngPayInfo();
        rDes.copyFrom(this);
        
        return (rDes);
    }
}
