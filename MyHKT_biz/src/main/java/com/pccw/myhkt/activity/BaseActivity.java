package com.pccw.myhkt.activity;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.Point;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.LinearLayout.LayoutParams;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AlertDialog.Builder;
import androidx.fragment.app.FragmentActivity;

import com.pccw.biz.myhkt.R;
import com.pccw.dango.shared.tool.DirNum;
import com.pccw.myhkt.APIsManager;
import com.pccw.myhkt.APIsManager.OnAPIsListener;
import com.pccw.myhkt.APIsResponse;
import com.pccw.myhkt.ClnEnv;
import com.pccw.myhkt.DialogHelper;
import com.pccw.myhkt.LiveChatHelper;
import com.pccw.myhkt.Utils;
import com.pccw.myhkt.fragment.BaseServiceFragment.OnLiveChatListener;
import com.pccw.myhkt.lib.ui.AAQuery;
import com.pccw.myhkt.model.HomeButtonItem.MAINMENU;
import com.pccw.myhkt.utils.FirebaseSetting;

/************************************************************************
 * File : BaseActivity.java 
 * Desc : Base Activity containing basic settings for all activity
 * Name : BaseActivity
 * by : Derek Tsui 
 * Date : 24/11/2015
 *
 * Change History:
 * Date       Modified By		Description
 * ---------- ----------------	-------------------------------
 * 24/11/2015 Derek Tsui 		-First draft
 * 23/06/2017 Abdulmoiz Esmail  -Moved the checking of language on the onCreate method and on the setContentView method,
 *  							 checked if the locale  has changed to default English (due to webviews in Android 
 *  							 higher or equal 24), if does switch to Chinese. 
 *************************************************************************/

public class BaseActivity extends FragmentActivity implements OnAPIsListener, OnLiveChatListener {
    private boolean debug = false;
    private String clientVersion = "";
    public static BaseActivity me;
    private AAQuery aq;
    protected int basePadding;
    protected int extralinespace;
    protected int titleWdith;
    protected int detailBtnPadding;
    private static final int DEFAULT_TITLE_SIZE = 14;
    private static final int DEFAULT_TITLE_TXT_COLOR = R.color.hkt_navbarblue;
    protected Boolean isLiveChatShown = false;
    private boolean doHeloFromChatMA = false;
    // FrameLayout as Container for activity
    protected FrameLayout frameContainer;
    protected AlertDialog alertDialog = null;
    protected String moduleId;

    public String getModuleId() {
        return moduleId;
    }

    protected Boolean isZh = false;
    protected FirebaseSetting firebaseSetting;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        debug = getResources().getBoolean(R.bool.DEBUG);
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        extralinespace = (int) getResources().getDimension(R.dimen.extralinespace);
        basePadding = (int) getResources().getDimension(R.dimen.basePadding);
        titleWdith = (int) getResources().getDimension(R.dimen.dq_title_width);
        detailBtnPadding = (int) getResources().getDimension(R.dimen.detail_btn_padding);
        ClnEnv.updateUILocale(getBaseContext(), ClnEnv.getAppLocale(getBaseContext()));
        me = this;

        //Language Indicator
        isZh = "zh".equalsIgnoreCase(getString(R.string.myhkt_lang));

        //For validation
        String mobPfx = ClnEnv.getPref(this, "mobPfx", "51,52,53,54,55,56,57,59,6,9,84,85,86,87,89");
        String ltsPfx = ClnEnv.getPref(this, "ltsPfx", "2,3,81,82,83");
        DirNum.getInstance(mobPfx, ltsPfx);

        //Status Bar Color
        if (android.os.Build.VERSION.SDK_INT >= 21) {
            Window window = getWindow();
            window.getDecorView().setFitsSystemWindows(false);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            if (ClnEnv.isMyMobFlag()) {
                window.setStatusBarColor(getResources().getColor(R.color.hkt_edittextbgorange));
            } else {
                window.setStatusBarColor(getResources().getColor(ClnEnv.getSessionPremierFlag() ? R.color.hkt_headingpremier : R.color.hkt_textcolor));
            }
        }
        firebaseSetting = new FirebaseSetting(this);
    }

    @Override
    public void setContentView(int layoutResID) {
        if (frameContainer != null) {
            frameContainer.removeAllViews();
        }
        frameContainer = new FrameLayout(this);
        LayoutParams layoutParam = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
        frameContainer.addView(getLayoutInflater().inflate(layoutResID, null), layoutParam);

        if (Build.VERSION.SDK_INT >= 24 && isZh && "en".equalsIgnoreCase(getString(R.string.myhkt_lang))) {
            Utils.switchToZhLocale(getApplicationContext());
        }

        super.setContentView(frameContainer, layoutParam);
    }

    protected Boolean callOnStart = true;

    @Override
    protected void onRestart() {
        if (debug) Log.i(this.getClass().toString(), "onRestart");
        callOnStart = false;
        super.onRestart();

    }

    @Override
    protected void onStart() {
        if (debug) Log.i(this.getClass().toString(), "onStart");
        super.onStart();

        isZh = "zh".equalsIgnoreCase(getResString(R.string.myhkt_lang));

        if (callOnStart) {
            initNavBar();
            //		initUI();
            initUI2();
        }

    }


    public final void initNavBar() {
        aq = new AAQuery(this);
        aq.navBarBaseLayout(R.id.navbar_base_layout);
        LiveChatHelper.getInstance(me, me);
        if (LiveChatHelper.isPause || isLiveChatShown) {
            aq.id(R.id.navbar_button_right).visibility(View.VISIBLE);
        } else {
            aq.id(R.id.navbar_button_right).visibility(View.INVISIBLE);
        }

        aq.id(R.id.navbar_button_right).clicked(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                //login required
                if (LiveChatHelper.isPause) {
                    openLiveChat();
                } else if (moduleId == getResources().getString(R.string.MODULE_CONTACT_US) && !ClnEnv.isLoggedIn()) {
                    loginFirstDialog(MAINMENU.CONTACTUS);
                } else if (moduleId == getResources().getString(R.string.MODULE_LTS_IDD) && !ClnEnv.isLoggedIn()) {
                    loginFirstDialog(MAINMENU.IDD);
                } else if (moduleId == getResources().getString(R.string.MODULE_MM_MAIN) && !ClnEnv.isLoggedIn()) {
                    loginFirstDialog(MAINMENU.MYMOB);
                } else if (moduleId == getResources().getString(R.string.MODULE_SETTING) && !ClnEnv.isLoggedIn()) {
                    loginFirstDialog(MAINMENU.SETTING);
                } else {
                    openLiveChat();
                }
            }
        });
    }

    public void openLiveChat() {
        // Event Tracker

        //open live chat
        if (ClnEnv.getChatIsMaintenance() != null) {
            if (!"Y".equalsIgnoreCase(ClnEnv.getChatIsMaintenance())) {
                LiveChatHelper.getInstance(me, me).showWebView(me, moduleId);
            } else {
                DialogHelper.createSimpleDialog(me, getString(R.string.LIVECHAT_UNAVAILABLE));
            }
        } else {
            doHeloFromChatMA = true;
            APIsManager.doHelo(me);
        }

    }

    public void openLiveChat(String tempModuleId) {
        // Event Tracker

        LiveChatHelper.getInstance(me, me).showWebView(me, tempModuleId);
    }

    public void setModuleId(String moduleId) {
        this.moduleId = moduleId;
    }

    private void initUI() {

    }

    protected void initUI2() {

    }

    @Override
    protected void onNewIntent(Intent intent) {
        if (debug) Log.i(this.getClass().toString(), "onNewIntent");
        super.onNewIntent(intent);
        isZh = "zh".equalsIgnoreCase(getResString(R.string.myhkt_lang));
        ClnEnv.updateUILocale(getBaseContext(), ClnEnv.getAppLocale(getBaseContext()));
    }

    @Override
    protected void onResume() {
        super.onResume();
        me = this;
        isZh = "zh".equalsIgnoreCase(getResString(R.string.myhkt_lang));
        setcustomNavBarRight();
    }


    private void setcustomNavBarRight() {
        Boolean isPause = LiveChatHelper.getInstance(me, me).isPause;
        setLiveChangeIcon(isPause);
    }

    public void setLiveChangeIcon(Boolean isPause) {
        if (isPause) {
            if (aq != null) {
                aq.id(R.id.navbar_button_right).visibility(View.VISIBLE);
                aq.navBarButton(R.id.navbar_button_right, R.drawable.chat);
                ColorFilter filter = new PorterDuffColorFilter(Color.parseColor("#DB9200"), PorterDuff.Mode.MULTIPLY);
                aq.id(R.id.navbar_button_right).getImageView().setColorFilter(filter);
            }
        } else {
            if (aq != null) {
                //				aq.navBarButton(R.id.navbar_button_right,R.drawable.livechat_small);
                //				aq.id(R.id.navbar_button_right).getImageView().setColorFilter(null);
                if (isLiveChatShown) {
                    aq.id(R.id.navbar_button_right).visibility(View.VISIBLE);
                } else {
                    aq.id(R.id.navbar_button_right).visibility(View.INVISIBLE);
                }
                aq.navBarButton(R.id.navbar_button_right, R.drawable.chat);
                aq.id(R.id.navbar_button_right).getImageView().clearColorFilter();
                //Premier: white , Non-Premier : blue
                //Prtimrt :
                if (debug) Log.i("TAG", ClnEnv.getSessionPremierFlag() + "");
                if (!ClnEnv.getSessionPremierFlag()) {
                    ColorFilter filter = new PorterDuffColorFilter(Color.parseColor("#217EC0"), PorterDuff.Mode.MULTIPLY);
                    aq.id(R.id.navbar_button_right).getImageView().setColorFilter(filter);
                } else {
                    ColorFilter filter = new PorterDuffColorFilter(Color.parseColor("#FFFFFF"), PorterDuff.Mode.MULTIPLY);
                    aq.id(R.id.navbar_button_right).getImageView().setColorFilter(filter);
                }
            }
        }
    }

    protected void onStop() {
        super.onStop();
        cleanupUI();
    }

    // Free all references to the UI elements
    protected void cleanupUI() {

    }

    public final void displayDialog(Context cxt, String message) {
        if (alertDialog != null) {
            alertDialog.dismiss();
        }

        AlertDialog.Builder builder = new Builder(cxt);
        builder.setMessage(message);
        builder.setPositiveButton(Utils.getString(cxt, R.string.btn_ok), new OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                alertDialog = null;
            }
        });

        alertDialog = builder.create();
        alertDialog.show();
    }

    // Login First Dialog
    protected final void loginFirstDialog(final MAINMENU clickItem) {
        OnClickListener onClickListener = new OnClickListener() {
            Bundle rbundle;

            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent intent;
                dialog.dismiss();
                intent = new Intent(getApplicationContext(), LoginActivity.class);
                rbundle = new Bundle();
                rbundle.putSerializable("CLICKBUTTON", clickItem);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                intent.putExtras(rbundle);
                finish();
                startActivity(intent);
                overridePendingTransition(R.anim.right_slide_in, R.anim.left_slide_out);
            }
        };
        DialogHelper.createSimpleDialog(this,
                Utils.getString(this, R.string.myhkt_plslogin),
                Utils.getString(this, R.string.btn_ok),
                onClickListener);
    }


    public static void ivSessDialog() {
        DialogInterface.OnClickListener onPositiveClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(me.getApplicationContext(), LoginActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                me.startActivity(intent);
                me.overridePendingTransition(R.anim.left_slide_in, R.anim.right_slide_out);
            }
        };

        DialogHelper.createSimpleDialog(me, Utils.getString(me, R.string.DLGM_ABORT_IVSESS), Utils.getString(me, R.string.btn_ok), onPositiveClickListener);
    }

    public String getResString(int res) {
        return Utils.getString(this, res);
    }


    public float getResDimen(int res) {
        return getResources().getDimension(res);
    }

    public Integer getResInt(int res) {
        return getResources().getInteger(res);
    }

    public void onSuccess(APIsResponse response) {
        if (response != null) {
            if (APIsManager.HELO.equals(response.getActionTy()) && doHeloFromChatMA) {
                if (debug) debugLog("DOHELO", "CHAT_MA dohelo sucess");
                doHeloFromChatMA = false;
                if (!"Y".equalsIgnoreCase(ClnEnv.getChatIsMaintenance())) {
                    LiveChatHelper.getInstance(me, me).showWebView(me, moduleId);
                } else {
                    DialogHelper.createSimpleDialog(me, getString(R.string.LIVECHAT_UNAVAILABLE));
                }
            }
        }

    }

    public void onFail(APIsResponse response) {
        if (response != null) {
            if (APIsManager.HELO.equals(response.getActionTy()) && doHeloFromChatMA) {
                doHeloFromChatMA = false;
                // General Error Message
                if (!"".equals(response.getMessage()) && response.getMessage() != null) {
                    DialogHelper.createSimpleDialog(this, response.getMessage());
                } else {
                    DialogHelper.createSimpleDialog(this, ClnEnv.getRPCErrMsg(this, response.getReply().getCode()));
                }
            }
        }
    }

    public void debugLog(String tag, String msg) {
        if (debug) Log.i(tag, msg);
    }

    protected void onDestory() {
        // TODO Auto-generated method stub
    }

    @Override
    public void onActivityResult(final int requestCode, final int resultCode, final Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        if (requestCode == LiveChatHelper.mRequestCodeFilePicker) {
            if (resultCode == Activity.RESULT_OK) {
                if (intent != null) {
                    if (LiveChatHelper.mFileUploadCallbackFirst != null) {
                        LiveChatHelper.mFileUploadCallbackFirst.onReceiveValue(intent.getData());
                        LiveChatHelper.mFileUploadCallbackFirst = null;
                    } else if (LiveChatHelper.mFileUploadCallbackSecond != null) {
                        Uri[] dataUris;
                        try {
                            dataUris = new Uri[]{Uri.parse(intent.getDataString())};
                        } catch (Exception e) {
                            dataUris = null;
                        }

                        LiveChatHelper.mFileUploadCallbackSecond.onReceiveValue(dataUris);
                        LiveChatHelper.mFileUploadCallbackSecond = null;
                    }
                }
            } else {
                if (LiveChatHelper.mFileUploadCallbackFirst != null) {
                    LiveChatHelper.mFileUploadCallbackFirst.onReceiveValue(null);
                    LiveChatHelper.mFileUploadCallbackFirst = null;
                } else if (LiveChatHelper.mFileUploadCallbackSecond != null) {
                    LiveChatHelper.mFileUploadCallbackSecond.onReceiveValue(null);
                    LiveChatHelper.mFileUploadCallbackSecond = null;
                }
            }
        }
    }
}
