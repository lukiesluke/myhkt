package com.pccw.dango.shared.g3entity;

import java.io.Serializable;

public class G3GetUnbilledDailyRoamWifiResultDTO implements Serializable 
{
    private static final long serialVersionUID = 3832615927055276152L;
    
    private String rtnCd;
	private String rtnMsg;
	private String resultRefCode;
	private G3UnbilledDailyRoamWifiDTO[] g3UnbilledDailyRoamWifiDTOArr;

	public String getRtnCd() {
		return rtnCd;
	}
	public void setRtnCd(String rtnCd) {
		this.rtnCd = rtnCd;
	}
	public String getRtnMsg() {
		return rtnMsg;
	}
	public void setRtnMsg(String rtnMsg) {
		this.rtnMsg = rtnMsg;
	}
	public G3UnbilledDailyRoamWifiDTO[] getG3UnbilledDailyRoamWifiDTOArr() {
		return g3UnbilledDailyRoamWifiDTOArr;
	}
	public void setG3UnbilledDailyRoamWifiDTOArr(
			G3UnbilledDailyRoamWifiDTO[] g3UnbilledDailyRoamWifiDTOArr) {
		this.g3UnbilledDailyRoamWifiDTOArr = g3UnbilledDailyRoamWifiDTOArr;
	}
	public String getResultRefCode() {
		return resultRefCode;
	}
	public void setResultRefCode(String resultRefCode) {
		this.resultRefCode = resultRefCode;
	}
}
