package com.pccw.myhkt;

import android.content.Context;

import androidx.multidex.MultiDex;
import androidx.multidex.MultiDexApplication;

public class MyHKTApplication extends MultiDexApplication {
    @Override
    public void onCreate() {
        super.onCreate();
//        FontsOverride.setDefaultFont(this, "DEFAULT", "Roboto-Regular.ttf");
//        FontsOverride.setDefaultFont(this, "DEFAULT_BOLD", "Roboto-Bold.ttf");
        FontsOverride.setDefaultFont(this, "MONOSPACE", "Roboto-Regular.ttf");
//        FontsOverride.setDefaultFont(this, "SERIF", "Roboto-Regular.ttf");
//        FontsOverride.setDefaultFont(this, "SANS_SERIF", "Roboto-Regular.ttf");
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(LocaleHelper.onAttach(base, ClnEnv.getAppLocaleStatic(base)));
        MultiDex.install(this);
    }
}
