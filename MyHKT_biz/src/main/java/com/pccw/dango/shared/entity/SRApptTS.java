/*
    Service Request Appointment Timeslot
    
    Keywords
    --------
    $URL: $
    $Rev: $
    $Date: $
    $Author: $
*/

package com.pccw.dango.shared.entity;

import java.io.Serializable;

public class SRApptTS implements Serializable
{
    private static final long serialVersionUID = 9211917161737021508L;
    
    private String                  apptDate;           /* Appointment Date                              */
    private String                  apptTmslot;         /* Appointment Timeslot                          */
    private String                  apptDT;             /* Appointment Datetime                          */

    
    public static void main(String rArg[])
    {
        System.out.println(getVer());
        return;
    }
    
    
    public static String getVer()
    {
        return ("$URL: $, $Rev: $");
    }

    
    public SRApptTS()
    {
        initAndClear();
    }

    
    final void initAndClear()
    {
        init();
        clear();
    }


    protected void init()
    {
    }


    public void clear()
    {
        clearApptDate();
        clearApptTmslot();
        clearApptDT();
    }


    public SRApptTS copyFrom(SRApptTS rSrc)
    {
        setApptDate(rSrc.getApptDate());
        setApptTmslot(rSrc.getApptTmslot());
        setApptDT(rSrc.getApptDT());

        return (this);
    }


    public SRApptTS copyTo(SRApptTS rDes)
    {
        rDes.copyFrom(this);
        return (rDes);
    }
    
    
    public SRApptTS copyMe()
    {
        SRApptTS                    rDes;

        rDes = new SRApptTS();
        rDes.copyFrom(this);
        return (rDes);
    }


    public void clearApptDate()
    {
        apptDate = "";
    }


    public void setApptDate(String rArg)
    {
        apptDate = rArg;
    }


    public String getApptDate()
    {
        return (apptDate);
    }


    public void clearApptTmslot()
    {
        apptTmslot = "";
    }


    public void setApptTmslot(String rArg)
    {
        apptTmslot = rArg;
    }


    public String getApptTmslot()
    {
        return (apptTmslot);
    }


    public void clearApptDT()
    {
        apptDT = "";
    }


    public void setApptDT(String rArg)
    {
        apptDT = rArg;
    }


    public String getApptDT()
    {
        return (apptDT);
    }
}
