package com.pccw.myhkt.fragment;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ListView;

import androidx.annotation.NonNull;

import com.pccw.dango.shared.cra.AcMainCra;
import com.pccw.dango.shared.entity.SubnRec;
import com.pccw.dango.shared.entity.SveeRec;
import com.pccw.dango.shared.tool.RC;
import com.pccw.myhkt.APIsManager;
import com.pccw.myhkt.APIsResponse;
import com.pccw.myhkt.ClnEnv;
import com.pccw.myhkt.DialogHelper;
import com.pccw.myhkt.R;
import com.pccw.myhkt.Utils;
import com.pccw.myhkt.activity.BaseActivity;
import com.pccw.myhkt.adapter.ServiceListViewAdapter;
import com.pccw.myhkt.adapter.ServiceListViewAdapter.OnServiceListViewAdapterListener;
import com.pccw.myhkt.dialogs.ChangeDisplayNameDialog;
import com.pccw.myhkt.fragment.IDDRateServiceFragment.OnIDDRateServiceListener;
import com.pccw.myhkt.lib.swipelistview.BaseSwipeListViewListener;
import com.pccw.myhkt.lib.swipelistview.SwipeListView;
import com.pccw.myhkt.lib.ui.AAQuery;
import com.pccw.myhkt.model.AcctAgent;

import java.util.Objects;

public class ServListFragment extends BaseFragment implements OnServiceListViewAdapterListener, ChangeDisplayNameDialog.OnChangeDisplayName{
	private ServListFragment me;
	private AAQuery aq;
	private View thisView;
	private SveeRec sveeRec = null;

	// CallBack
	private OnServListListener callback;
	private OnIDDRateServiceListener callbackidd;
	// List View
	private ServiceListViewAdapter serviceListViewAdapter;

	private SwipeListView fragment_servlist_listview;

	private boolean isChangePwdMode = false;
	private final String TAG = "ServListFragment";
	private int padding_twocol = 0;
	private int deviceWidth = 0;
	private int extralinespace;
	private int basePadding;

	private AcMainCra acMainCra = null;

	public interface OnServListListener {
		int getType();
	}

	public static final int IDDRate = 0;
	public static final int MYPROF = 1;


	@Override
	public void onAttach(@NonNull Activity activity) {
		super.onAttach(activity);

		try {
			callback = (OnServListListener) activity;
			callbackidd = (OnIDDRateServiceListener) activity;
		} catch (ClassCastException e) {
			throw new ClassCastException(activity.toString() + " must implement OnMyProfServListListener");
		}
	}

	public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		me = this;
		if (debug) Log.i(TAG, "onCreateView");
		deviceWidth = getDeviceWidth();
		padding_twocol = (int) getResources().getDimension(R.dimen.padding_twocol);
		extralinespace = (int) getResources().getDimension(R.dimen.extralinespace);
		basePadding = (int) getResources().getDimension(R.dimen.basePadding);

		View fragmentLayout = inflater.inflate(R.layout.fragment_servlist, container, false);
		thisView = fragmentLayout;
		fragmentLayout.findViewById(R.id.fragment_servlist_btn).setVisibility(View.GONE);
		return fragmentLayout;
	}

	@Override
	public void onStart() {
		super.onStart();
		if (debug) Log.i(TAG, "ServListFragment start");
		// sveeRec = callback.getSveeRec();
		init();
	}

	@Override
	public void onResume(){
		if (debug) Log.i(TAG, "ServListFragment resume");
		super.onResume();

	}

	private void init() {
		if (debug) Log.i(TAG, "ServListFragment init");
		aq = new AAQuery(thisView);

		aq.id(R.id.servicelist_empty).textSize(getResources().getInteger(R.integer.textsize_default_int));
		aq.normTxtBtn(R.id.fragment_servlist_btn, getResString(R.string.myhkt_btn_alias), LinearLayout.LayoutParams.MATCH_PARENT);
		aq.id(R.id.fragment_servlist_btn).clicked(this, "onClick");
		aq.padding(R.id.fragment_servlist_btn_layout, basePadding, basePadding, basePadding, basePadding);

		if (callback.getType() == IDDRate) {
			if (debug) Log.i(TAG, "ServListFragment init1");
			initIDDRate();
		} else if (callback.getType() == MYPROF) {
			// initMyProf();
		}
	}

	/*************************************
	 * 
	 * Mode
	 * 
	 ***************************************************/
	public void initIDDRate(){
		// aq.id(R.id.fragment_servlist_myprof_header_layout).visibility(View.GONE);
		aq.id(R.id.myprofservlist_btns_layout1).visibility(View.GONE);
		aq.id(R.id.myprofservlist_btns_layout2).visibility(View.GONE);
	}

	protected final void refreshData() {
		super.refreshData();
		aq = new AAQuery(thisView);

		//Swipe List View
		int deviceWidth = getResources().getDisplayMetrics().widthPixels;

		fragment_servlist_listview = (SwipeListView) getActivity().findViewById(R.id.fragment_servlist_listview);

		try {
			serviceListViewAdapter = new ServiceListViewAdapter(me, ServiceListViewAdapter.IDDRATE, fragment_servlist_listview, false);
			thisView.findViewById(R.id.fragment_servlist_btn).setVisibility(serviceListViewAdapter.getCount() > 0 ? View.VISIBLE : View.GONE);
			aq.id(R.id.fragment_servlist_listview).getListView().setAdapter(serviceListViewAdapter);
			fragment_servlist_listview.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
			fragment_servlist_listview.setDeviceWidth(deviceWidth - basePadding * 2);
			fragment_servlist_listview.setSwipeListViewListener(new ServiceListSwipeListViewListener());
			fragment_servlist_listview.setEmptyView(aq.id(R.id.fragment_servlist_empty).getView());
			fragment_servlist_listview.setSwipeMode(SwipeListView.SWIPE_MODE_LEFT);
			fragment_servlist_listview.setSwipeActionLeft(SwipeListView.SWIPE_ACTION_REVEAL);
			fragment_servlist_listview.setSwipeActionRight(SwipeListView.SWIPE_ACTION_NONE);
			fragment_servlist_listview.setOffsetLeft((deviceWidth - basePadding * 2) * 5 / 6);
			fragment_servlist_listview.setAnimationTime(0);
			fragment_servlist_listview.setSwipeOpenOnLongPress(false);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void refreshSwipeMode() {
		//		viewholder.servicelist_listView.setSwipeMode(SwipeListView.SWIPE_MODE_LEFT);
		//		viewholder.servicelist_listView.setSwipeActionLeft(SwipeListView.SWIPE_ACTION_REVEAL);
		//		viewholder.servicelist_listView.setSwipeActionRight(SwipeListView.SWIPE_ACTION_NONE);
		//		viewholder.servicelist_listView.setOffsetLeft(deviceWidth * 5 / 6);
		//		viewholder.servicelist_listView.setAnimationTime(0);
		//		viewholder.servicelist_listView.setSwipeOpenOnLongPress(false);
		//	}
	}
	class ServiceListSwipeListViewListener extends BaseSwipeListViewListener{
		@Override
		public int onChangeSwipeMode(int position) {
			if (serviceListViewAdapter.isZombie(position)) {
				return SwipeListView.SWIPE_MODE_NONE;
			} else {
				me.refreshSwipeMode();
				return SwipeListView.SWIPE_MODE_LEFT;
			}
		}

		//		AdapterView<?> adapterView, View view, int position, long arg3
		@Override
		public void onClickFrontView(int position, View view) {
			if (debug) Log.i(TAG, "Front Click");
			super.onClickFrontView(position, view);
			Intent intent = null;
			Bundle bundle = new Bundle();
			callbackidd.setAcctAgent((AcctAgent)serviceListViewAdapter.getItem(position));
			callbackidd.goToIddRateFragment();
			//			//pass button click
			//			bundle.putInt("CLICKBUTTON", parentOnClickId);
			//			
			//			// We pass different object according to the source of the array element
			//			if (serviceListViewAdapter.isItemAssocSubnAry(position)) {
			//				bundle.putBoolean("ISASSOCARY", true);
			//				bundle.putSerializable("SUBNREC", (SubscriptionRec) serviceListViewAdapter.getItem(position));
			//			} else {
			//				bundle.putBoolean("ISASSOCARY", false);
			//				bundle.putSerializable("ACCOUNT", (Account) serviceListViewAdapter.getItem(position));
			//			}
			//
			//			String selectedLob = serviceListViewAdapter.getLOB(position);
			//
			//			if (selectedLob.equals(SubscriptionRec.LOB_LTS)) {
			//				intent = new Intent(getApplicationContext(), ServiceLTSActivity.class);
			//			} else if (selectedLob.equals(SubscriptionRec.LOB_MOB)) {
			//				intent = new Intent(getApplicationContext(), ServiceMOBActivity.class);
			//			} else if (selectedLob.equals(SubscriptionRec.LOB_NE)) {
			//				intent = new Intent(getApplicationContext(), ServiceMOBActivity.class);
			//			} else if (selectedLob.equals(SubscriptionRec.LOB_PCD)) {
			//				intent = new Intent(getApplicationContext(), ServicePCDActivity.class);
			//			} else if (selectedLob.equals(SubscriptionRec.LOB_TV)) {
			//				intent = new Intent(getApplicationContext(), ServiceTVActivity.class);
			//			} else if (selectedLob.equals(SubscriptionRec.LOB_101)) {
			//				intent = new Intent(getApplicationContext(), Service1010Activity.class);
			//			} else if (selectedLob.equals(SubscriptionRec.LOB_O2F)) {
			//				intent = new Intent(getApplicationContext(), Service1010Activity.class);
			//			}
			//			
			//			if (intent != null) {
			//				intent.putExtras(bundle);
			//				startActivity(intent);
			//				overridePendingTransition(R.anim.right_slide_in, R.anim.left_slide_out);
			//			}
		}

		@Override
		public void onClickBackView(int position) {
			//			viewholder.servicelist_listView.closeOpenedItems();
		}

		@Override
		public void onDismiss(int[] reverseSortedPositions) {
			//			for (int position : reverseSortedPositions) {
			//				testData.remove(position);
			//			}
			//			mAdapter.notifyDataSetChanged();
			//		}
		}
	}

	public void closeOpenedListItems() {
		if (fragment_servlist_listview != null) {
			if (fragment_servlist_listview.isOpened()) {
				fragment_servlist_listview.closeOpenedItems();
			}
		}
	}

	public void onClick(View v) {
		if (v.getId() == R.id.fragment_servlist_btn) {
			if (fragment_servlist_listview.isOpened()) {
				fragment_servlist_listview.closeOpenedItems();
			} else {
				for (int i = fragment_servlist_listview.getFirstVisiblePosition(); i <= fragment_servlist_listview.getLastVisiblePosition(); i++) {
					if (!serviceListViewAdapter.isZombie(i))
						fragment_servlist_listview.openAnimate(i);
				}
			}
		}
	}

	@Override
	public void onSuccess(APIsResponse response) {
		AcMainCra acMainCra = (AcMainCra) response.getCra();
		DialogHelper.createSimpleDialog(me.getActivity(), getString(R.string.RSVM_DONE));
		ClnEnv.getQualSvee().setSveeRec(acMainCra.getOSveeRec());
		ClnEnv.getQualSvee().setSubnRecAry(acMainCra.getOSubnRecAry());
//		refreshList();
		refreshData();
	}

	@Override
	public void onFail(APIsResponse response) {
		// TODO Auto-generated method stub
		AcMainCra acMainCra;
		try {
			acMainCra = (AcMainCra)response.getCra();
		} catch (Exception e) {
			acMainCra = null;
		}
//		if (acMainCra != null) {
//			//			if (!logincra.getReply().isEqual(Reply.RC_SUCC)) {
//			//				if (logincra.getReply().isEqual(Reply.RC_IVSESS)) {
//			//					// RC_IVSESS
//			//					me.redirectDialog(ClnEnv.getRPCErrMsg(me.getActivity().getApplicationContext(), logincra.getReply().getCode()));
//			//				} else if (logincra.getReply().isEqual(Reply.RC_ALT)) {
//			//					// RC_ALT
//			//					if (!me.errALT2nd) {
//			//						// RC_ALT 1st time
//			//						if (!ClnEnv.getSessionPassword().equalsIgnoreCase("") && !ClnEnv.getSessionLoginID().equalsIgnoreCase("")) {
//			//							// try to login once, if have LoginID/Password
//			//							// doHelo
//			//							me.progressDialogHelper.showProgressDialog(me.getActivity(), me.onstopHandler, true, ProgressDialogHelper.dohello);
//			//							HeloGrq helogrq = new HeloGrq();
//			//							helogrq.actionTy = DoHeloAsyncTask.actionTy;
//			//							helogrq.smphCra = new SmphCra();
//			//							helogrq.smphCra.getISmphReq().chkSum = Utils.sha256(ClnEnv.getPref(me.getActivity().getApplicationContext(), me.getString(R.string.CONST_PREF_GCM_REGID), "") + "A", Utils
//			//									.getString(me.getActivity(), R.string.CONST_SALT));
//			//							helogrq.smphCra.getISmphReq().devId = ClnEnv.getPref(me.getActivity().getApplicationContext(), me.getString(R.string.CONST_PREF_GCM_REGID), "");  // Device ID
//			//							helogrq.smphCra.getISmphReq().devTy = "A"; 																							// Device Type: A for android, I for IOS
//			//							helogrq.smphCra.getISmphReq().gni = ClnEnv.getPref(me.getActivity().getApplicationContext(), me.getString(R.string.CONST_PREF_GENNOTI_FLAG), "Y"); // General Notification
//			//																																												// ON/OFF
//			//							if (ClnEnv.getPref(me.getActivity().getApplicationContext(), me.getString(R.string.CONST_PREF_SAVEPASSWORD), false) && ClnEnv.isLoggedIn()) {							// Bill Notificatiion "Y" if
//			//								// password is saved
//			//								helogrq.smphCra.getISmphReq().bni = "Y";
//			//							} else {
//			//								helogrq.smphCra.getISmphReq().bni = "N";
//			//							}
//			//							helogrq.smphCra.getISmphReq().lang = ClnEnv.getAppLocale(me.getActivity().getBaseContext());
//			//							doHeloAsyncTask = new DoHeloAsyncTask(me.getActivity().getApplicationContext(), callbackHandler).execute(helogrq);
//			//
//			//						} else {
//			//							// redirect to Login, error message IVSESS, if no LoginID/Password
//			//							me.redirectDialog(Utils.getString(me.getActivity(), R.string.DLGM_ABORT_IVSESS));
//			//						}
//			//					} else {
//			//						// redirect to Login, error message IVSESS
//			//						me.redirectDialog(Utils.getString(me.getActivity(), R.string.DLGM_ABORT_IVSESS));
//			//					}
//			//					me.errALT2nd = true;
//			//				} else if (logincra.getReply().isEqual(Reply.RC_NOT_AUTH)) {
//			//					// RC_NOT_AUTH
//			//					me.displayDialog(Utils.getString(me.getActivity(), R.string.RSVM_NOT_AUTH));
//			//					// Reset check RC_ALT error time
//			//					me.errALT2nd = false;
//			//					me.regnGrq = null;
//			//				} else if (logincra.getReply().isEqual(Reply.RC_INACTIVE_RCUS)) {
//			//					// RC_INACTIVE_RCUS
//			//					me.displayDialog(Utils.getString(me.getActivity(), R.string.RSVM_INACT_RCUS));
//			//					// Reset check RC_ALT error time
//			//					me.errALT2nd = false;
//			//					me.regnGrq = null;
//			//				} else {
//			//					// RC_IVDATA
//			//					// RC_UXSVLTERR
//			//					me.displayDialog(ClnEnv.getRPCErrMsg(me.getActivity().getApplicationContext(), logincra.getReply().getCode()));
//			//					// Reset check RC_ALT error time
//			//					me.errALT2nd = false;
//			//					me.regnGrq = null;
//			//				}
//			//			}
//		}

		// General Error Message
		if (!"".equals(response.getMessage()) && response.getMessage() != null) {
			DialogHelper.createSimpleDialog(getActivity(), response.getMessage());
		} else if (RC.TCSESS_MM.equalsIgnoreCase(response.getReply().getCode())) {
			BaseActivity.ivSessDialog();
		} else {
			DialogHelper.createSimpleDialog(getActivity(), ClnEnv.getRPCErrMsg(getActivity(), response.getReply().getCode()));
		}
	}

	@Override
	public void displayEditDialog(String message, final int position) {
		if (alertDialog != null) {
			alertDialog.dismiss();
		}
		closeOpenedListItems();

		ChangeDisplayNameDialog changeDisplayNameDialog = ChangeDisplayNameDialog.newInstance(serviceListViewAdapter.getAlias(position), position);
		changeDisplayNameDialog.setCallback(this);
		changeDisplayNameDialog.show(getActivity().getSupportFragmentManager(), "dialog");
	}

	public void updateAlias(String alias, SubnRec subnRec) {
		// doSvcAso
		// Prepare subnRecAry which are selected
		SubnRec[] subnRecAry = new SubnRec[Objects.requireNonNull(ClnEnv.getQualSvee()).getSubnRecAry().length];
		SubnRec selectedSubnRec = new SubnRec();
		for (int i = 0; i < ClnEnv.getQualSvee().getSubnRecAry().length; i++) {
			subnRecAry[i] = ClnEnv.getQualSvee().getSubnRecAry()[i].copyMe();

			if (Utils.matchSubnRec(subnRecAry[i], subnRec)) {
				subnRecAry[i].alias = alias;
				selectedSubnRec = subnRecAry[i];
			}
		}

		AcMainCra acMainCra = new AcMainCra();
		acMainCra.setIChgPwd(false);
		acMainCra.getISveeRec().pwd = ClnEnv.getSessionPassword();
		acMainCra.setISubnRecAry(subnRecAry);
		acMainCra.setISveeRec(ClnEnv.getQualSvee().getSveeRec().copyMe());
		acMainCra.setILoginId(ClnEnv.getQualSvee().getSveeRec().loginId);
		acMainCra.setISubnRec(selectedSubnRec);

		APIsManager.doSvcAso4Subn(null, me, acMainCra);
	}

	//refresh listview to display new changes
	private void refreshList() {
		try {
			// List View re-init
			serviceListViewAdapter =new ServiceListViewAdapter(me, ServiceListViewAdapter.IDDRATE, fragment_servlist_listview, false);
			aq.id(R.id.fragment_servlist_listview).getListView().setAdapter(serviceListViewAdapter);
			fragment_servlist_listview.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
			fragment_servlist_listview.setDeviceWidth(deviceWidth - extralinespace*2);
			fragment_servlist_listview.setSwipeListViewListener(new ServiceListSwipeListViewListener());
			fragment_servlist_listview.setEmptyView(aq.id(R.id.servicelist_empty).getView());
			fragment_servlist_listview.setSwipeMode(SwipeListView.SWIPE_MODE_LEFT);
			fragment_servlist_listview.setSwipeActionLeft(SwipeListView.SWIPE_ACTION_REVEAL);
			fragment_servlist_listview.setSwipeActionRight(SwipeListView.SWIPE_ACTION_NONE);
			fragment_servlist_listview.setOffsetLeft((deviceWidth - extralinespace*2) * 5 / 6);
			fragment_servlist_listview.setAnimationTime(0);
			fragment_servlist_listview.setSwipeOpenOnLongPress(false);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void showRemark(boolean show) {
		// TODO Auto-generated method stub
	}

	private int getDeviceWidth() {
		return getResources().getDisplayMetrics().widthPixels;
	}

	@Override
	public void onChangeNickname(String name, int position) {
		SubnRec subnRec = (SubnRec) serviceListViewAdapter.getSubnRecItem(position);
		updateAlias(name, subnRec);
	}
}
