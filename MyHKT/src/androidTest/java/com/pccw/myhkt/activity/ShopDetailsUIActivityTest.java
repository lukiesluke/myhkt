package com.pccw.myhkt.activity;

import android.Manifest;
import android.content.Context;
import androidx.test.rule.ActivityTestRule;
import androidx.test.rule.GrantPermissionRule;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import com.pccw.myhkt.R;
import com.pccw.myhkt.base.BaseTest;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static androidx.test.platform.app.InstrumentationRegistry.getInstrumentation;

@Ignore
@RunWith(AndroidJUnit4.class)
public class ShopDetailsUIActivityTest extends BaseTest {
    private Context context;

    @Rule
    public ActivityTestRule<MainMenuActivity> activityTestRule = new ActivityTestRule<>(
            MainMenuActivity.class);
    @Rule public GrantPermissionRule permissionRule = GrantPermissionRule.grant(
            Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION);

    @Before
    public void setUp(){
        context = getInstrumentation().getTargetContext();
        gotoMainMenuItem(10);
        clickViewInAdapter(R.id.shoptype_listview,1);
        waitForShopListFragment();
        clickString("Wu Pak Street csl Shop");
    }

    @Test
    public void testDisplayUI() {
        checkStringIsDisplayed(context, R.string.myhkt_shop_title);
        backBtnExist();
        checkViewIsDisplayed(R.id.shopdetail_shopname);
        checkViewIsDisplayed(R.id.shopdetail_mapview);
        checkViewIsDisplayed(R.id.shopdetail_address);
        checkViewIsDisplayed(R.id.shopdetail_tel);
        checkViewIsDisplayed(R.id.shopdetail_bushrs);
    }

    @Test
    public void testBackClick() {
        clickLeftNavBar();
        waitForShopListFragment();
    }

    public void waitForShopListFragment(){
        delay(TestTimeouts.API_TIMEOUTS,waitForText("Kwong On Building csl Shop"));

    }

}