/*
    Class for Directory Number (i.e. Telephone#)

    Keywords
    --------
    $URL: $
    $Rev: $
    $Date: $
    $Author: $
*/

package com.pccw.dango.shared.tool;

import java.io.Serializable;

import com.pccw.wheat.shared.tool.Tool;


public class DirNum implements Serializable, DynamicCfg
{
    private static final long serialVersionUID = 6894105466479289933L;
    
    private String                  mobPfxAry[];
    private String                  ltsPfxAry[];

    private static DirNum           me = null;
    

    public static void main(String rArg[])
    {
        System.out.println(getVer());
        return;
    }
    
    
    public static String getVer()
    {
        return ("$URL: $, $Rev: $");
    }


    protected DirNum()
    {
        /*
            Constuctor.
        */
        initAndClear();
    }
    
    
    public synchronized static DirNum getInstance(String rMobPfx, String rLtsPfx)
    {
        String                      rAry[];
        
        if (me == null) {
            me = new DirNum();
            
            rAry = Tool.tokenAry(rMobPfx, ',');
            me.setMobPfxAry(rAry);

            rAry = Tool.tokenAry(rLtsPfx, ',');
            me.setLtsPfxAry(rAry);
        }
        
        return (me);
    }
    
    
    public synchronized static DirNum getInstance()
    {
        if (me == null) {
            throw new RuntimeException("DirNum Not Yet Instantisated!");
        }
        
        return (me);
    }
    
    
    private void initAndClear()
    {
        init();
        clear();
    }
    
    
    final void init()
    {
    }
    
    
    protected void clear()
    {
        clearMobPfxAry();
        clearLtsPfxAry();
    }
    
    
    protected void clearMobPfxAry()
    {
        setMobPfxAry(new String[0]);
    }
    
    
    protected void setMobPfxAry(String rMobPfxAry[])
    {
        mobPfxAry = rMobPfxAry;
    }
    
    
    public String[] getMobPfxAry()
    {
        return (mobPfxAry);
    }
    
    
    protected void clearLtsPfxAry()
    {
        setLtsPfxAry(new String[0]);
    }
    
    
    protected void setLtsPfxAry(String rLtsPfxAry[])
    {
        ltsPfxAry = rLtsPfxAry;
    }
    
    
    public String[] getLtsPfxAry()
    {
        return (ltsPfxAry);
    }
    
    
    public static boolean isValidMob(String rStr)
    {
        String                      rDpAry[];
        int                         rx, ri, rl;

        if (rStr.length() == 8 && Tool.isDig(rStr)) {
            rDpAry = getInstance().getMobPfxAry();
            
            for (rx=0; rx<rDpAry.length; rx++) {
                if (rStr.startsWith(rDpAry[rx])) {
                    return (true);
                }
            }
        }
        
        return (false);
    }
    
    
    public static boolean isValidTel(String rStr)
    {
        String                      rDpAry[];
        int                         rx, ri, rl;

        if (rStr.length() == 8 && Tool.isDig(rStr)) {
            rDpAry = getInstance().getLtsPfxAry();
            
            for (rx=0; rx<rDpAry.length; rx++) {
                if (rStr.startsWith(rDpAry[rx])) {
                    return (true);
                }
            }
        }
        
        return (false);
    }
    
    
    public static boolean isVaDirNum(String rStr)
    {
        int                         rx, ri, rl;
        
        return (isValidMob(rStr) || isValidTel(rStr));
    }
}    
