package com.pccw.myhkt.activity;

import android.content.Context;
import android.content.SharedPreferences;
import androidx.test.espresso.intent.Intents;
import androidx.test.rule.ActivityTestRule;
import androidx.test.rule.GrantPermissionRule;
import androidx.test.ext.junit.runners.AndroidJUnit4;
import android.view.View;

import com.pccw.myhkt.AccountHelper;
import com.pccw.myhkt.ClnEnv;
import com.pccw.myhkt.R;
import com.pccw.myhkt.base.BaseTest;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static androidx.test.platform.app.InstrumentationRegistry.getInstrumentation;

@Ignore
@RunWith(AndroidJUnit4.class)
public class MyMobileActivityAddMobileTest extends BaseTest {
    private Context context;
    SharedPreferences shared;

    @Rule
    public ActivityTestRule<MainMenuActivity> activityTestRule = new ActivityTestRule<>(MainMenuActivity.class);
    @Rule
    public GrantPermissionRule permissionRule = GrantPermissionRule.grant(android.Manifest.permission.WRITE_EXTERNAL_STORAGE);

    @Before
    public void setUp() {
        context = getInstrumentation().getTargetContext();
        shared = context.getSharedPreferences(
                "myhkt",
                Context.MODE_PRIVATE);
        Intents.init();
        loginUAT();
    }

    @After
    public void tearDown() {
        if(ClnEnv.isLoggedIn()){
            executeLogout(context);
        }
        Intents.release();
    }

    public void loginUAT(){
        clickLeftNavBar();
        clickString(context, R.string.myhkt_LGIF_LOGIN);
        loginMyCredentialsUAT(AccountHelper.getUATAccount8().getUsername(),
                AccountHelper.getUATAccount8().getPassword(), shared);
        gotoMainMenuItem(4);
        clickView(R.id.mymob_servicelist_add_btn);
    }

    @Test
    public void testDisplayUI(){
        checkStringIsDisplayed(context,R.string.MYMOB_NAV_TITLE);
        backBtnExist();
        checkLiveChatBtnIsHidden();
        checkStringIsDisplayed(context,R.string.myhkt_mymob_desc_noacc);
        checkStringIsDisplayed(context,R.string.MYMOB_ADD_TITLE);
        checkStringIsDisplayed(context,R.string.MYMOB_DISPLAY_NAME);
        checkViewIsDisplayed(R.id.mymob_displayname);
        checkStringHint(context,R.id.mymob_displayname,R.string.myhkt_mymob_displayname_hint);
        checkStringIsDisplayed(context,R.string.MYMOB_MOBILE_NUM);
        checkViewIsDisplayed(R.id.mymob_mobilenum);
        checkStringHint(context,R.id.mymob_mobilenum,R.string.myhkt_mymob_mobilenum_hint);
        checkStringIsDisplayed(context,R.string.MYMOB_PASSWORD);
        checkViewIsDisplayed(R.id.mymob_password);
        checkStringHint(context,R.id.mymob_password,R.string.myhkt_mymob_password_hint);
        checkViewIsDisplayed(R.id.mymob_forget_pw_txt);
        checkStringIsDisplayed(context,R.string.myhkt_rememberpassword);
        checkViewIsChecked(R.id.mymob_remember_pw_sb);
        checkViewIsDisplayed(R.id.mymob_login_button);
    }

    @Test
    public void testEmpty(){
        checkViewString(R.id.mymob_displayname,"");
        checkViewString(R.id.mymob_mobilenum,"");
        checkViewString(R.id.mymob_password,"");
        clickView(R.id.mymob_login_button);
        checkDialogStringIsDisplayed(context,R.string.REGM_IVMOB);
        clickOK();
    }

    @Test
    public void testSave(){
        clearTextView(R.id.mymob_displayname);
        typeTextView(R.id.mymob_password,"Aa12345");
        clickView(R.id.mymob_login_button);
        checkDialogStringIsDisplayed(context,R.string.PAMM_IVMOB);
        clickOK();
        typeTextView(R.id.mymob_mobilenum,"70720066");
        clickView(R.id.mymob_login_button);
        checkDialogStringIsDisplayed(context,R.string.MYMOB_SAVE_CONFIRM);
        clickCANCEL();
    }

    @Test
    public void testRememberPassword(){
        clickView(R.id.mymob_remember_pw_sb);
        checkViewIsNotChecked(R.id.mymob_remember_pw_sb);
        clickView(R.id.mymob_remember_pw_sb);
        checkViewIsChecked(R.id.mymob_remember_pw_sb);
    }

    @Test
    public void testForgotPassword(){
        clickView(R.id.mymob_forget_pw_txt);
        checkDialogStringIsDisplayed(context,R.string.MYMOB_HINT_PASSWORD);
        clickOK();
    }

    @Test
    public void testBackBtn(){
        checkViewVisibility(R.id.mymob_navbar, View.VISIBLE);
        clickLeftNavBar();
        checkStringIsDisplayed(context,R.string.MYMOB_LIST_TITLE);
        clickView(R.id.mymob_servicelist_add_btn);
    }
}