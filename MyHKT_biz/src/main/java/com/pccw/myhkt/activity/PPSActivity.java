package com.pccw.myhkt.activity;

import java.lang.Thread.UncaughtExceptionHandler;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.text.ClipboardManager;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.pccw.myhkt.ClnEnv;
import com.pccw.myhkt.DialogHelper;
import com.pccw.biz.myhkt.R;
import com.pccw.myhkt.Utils;
import com.pccw.myhkt.lib.ui.AAQuery;
import com.pccw.myhkt.model.AcctAgent;

/************************************************************************
 * File : BrowserActivity.java
 * Desc : Web Page
 * Name : BrowserActivity
 * by 	: Andy Wong
 * Date : 22/12/2015
 * 
 * Change History:
 * Date       Modified By		Description
 * ---------- ----------------	-------------------------------
 * 22/12/2015 Andy Wong 		-First draft
   01/02/2016 Derek Tsui		-add logic
   22/02/2016 Derek Tsui		-usable condition, but need more polishing and throw away junks
 *************************************************************************/
public class PPSActivity extends BaseActivity{
	private String 		TAG = this.getClass().getName();
	public final static String BUNDLEURL = "bundleurl";
	public final static String BUNDLETITLE = "bundletitle";
	private boolean				debug			= false;		
	private String 				ppsURL = null;
	private String 				title = null;

	private AAQuery aq;
	private WebView 	webView;

	private AcctAgent 	acctAgent;
	private String		acctnum;
	//	private String 		lob 			= "";
	private int 		lob 			= 0;
	private int 		ltsType			= 0;
	private String[] 	billSumDetail	= new String[4];
	private String 		navbarText 		= "";
	private int			index			= 0;
	private boolean		isLOBPCDTV		= false;

	@Override
	public final void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);	

		debug = getResources().getBoolean(R.bool.DEBUG);
		ppsURL = Utils.getString(me, R.string.myhkt_pps_url);
		ClnEnv.updateUILocale(getBaseContext(),	ClnEnv.getAppLocale(getBaseContext()));		
		setContentView(R.layout.activity_pps);

		//		Bundle bundle = getIntent().getExtras();
		//		if (bundle != null) {
		//		    acctAgent = (AcctAgent) bundle.getSerializable("ACCTAGENT");
		//			lob = acctAgent.getLob();
		//			navbarText = acctAgent.getSrvNum();

		//		}

		Bundle bundle = getIntent().getExtras();
		if (bundle != null) {
			acctnum = bundle.getString("ACCTNUM") != null ? bundle.getString("ACCTNUM") : "";
			String amount = bundle.getString("AMOUNT") != null ? bundle.getString("AMOUNT") : "";
			lob = bundle.getInt("LOB");
			//			ltsType = bundle.getInt("LTSTYPE") != 0 ? bundle.getInt("LTSTYPE") : 0;
			loadData(acctnum, lob, amount);
		} else {
			loadData("", 0, "");
		}
		setModuleID();
	}


	@Override
	protected final void onStart() {
		super.onStart();
		//Screen Tracker
	}

	public void onBackPressed() {
		displayConfirmExitDialog();
	}

	// Restore the Bundle data when re-built Activity
	protected void onRestoreInstanceState(Bundle savedInstanceState) {
		index = savedInstanceState.getInt("INDEX");
		billSumDetail = savedInstanceState.getStringArray("STRINGARY");
		super.onRestoreInstanceState(savedInstanceState);
	}

	// Saved data before Destroy Activity
	protected void onSaveInstanceState(Bundle outState) {
		outState.putInt("INDEX", index);
		outState.putStringArray("STRINGARY", billSumDetail);
		super.onSaveInstanceState(outState);
	}

	protected void initUI2() {
		aq = new AAQuery(this);
		//navbar
		//		aq.navBarTitle(R.id.navbar_title, R.id.navbar_leftdraw, 0, getLobIcon(), 0, acctAgent.getSubnRec().srvNum);
		aq.navBarTitle(R.id.navbar_title, R.id.navbar_leftdraw, 0, R.drawable.lob_pps, 0, getString(R.string.myhkt_pps_paybypps));
		//		R.drawable.lob_pps
		//		getString(R.string.myhkt_pps_paybypps)
		aq.navBarButton(R.id.navbar_button_left, R.drawable.btn_back);
		aq.id(R.id.navbar_button_left).clicked(this, "onClick");
		aq.id(R.id.pps_footbar_doublearrow_right).clicked(this, "onClick");
		aq.id(R.id.pps_footbar_doublearrow_left).clicked(this, "onClick");
		aq.id(R.id.pps_footbar_Txt).clicked(this, "onClick");
		//		webView = aq.id(R.id.activity_browser_webview).getWebView();
		//		webView.loadUrl(ppsURL);		

		updatePPSBarText();
	}

	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.navbar_button_left:
			onBackPressed();
			break;
			// Navigation Buttons
			//				case R.id.pps_navbar_back:
			//				case R.id.pps_navbar_logo:
			//					displayConfirmExitDialog();
			//					break;
		case R.id.pps_footbar_doublearrow_left:
			index = (index - 1 + 4) % 4;

			if (index == 2 && !isLOBPCDTV) {
				// Show bill type for PCD & TV Only
				index = 1;
			}
			updatePPSBarText();
			break;
		case R.id.pps_footbar_doublearrow_right:
			index = (index + 1) % 4;

			if (index == 2 && !isLOBPCDTV) {
				// Show bill type for PCD & TV Only
				index = 3;
			}
			updatePPSBarText();
			break;
		case R.id.pps_footbar_Txt:
			if (index == 1) {
				// Copy to clipboard only supports Account No.
				ClipboardManager cm = (ClipboardManager) getSystemService(CLIPBOARD_SERVICE);
				cm.setText(acctnum);
				DialogHelper.createSimpleDialog(this, getString(R.string.myhkt_pps_copyacctnum));
			}
			break;
		}
	}

	//	// UI on-click events handling
	//	public final void onClickHandler(View selectedView) {
	//		switch (selectedView.getId()) {
	//		// Navigation Buttons
	////			case R.id.pps_navbar_back:
	////			case R.id.pps_navbar_logo:
	////				displayConfirmExitDialog();
	////				break;
	//			case R.id.pps_footbar_doublearrow_left:
	//				index = (index - 1 + 4) % 4;
	//
	//				if (index == 2 && !isLOBPCDTV) {
	//					// Show bill type for PCD & TV Only
	//					index = 1;
	//				}
	//				updatePPSBarText();
	//				break;
	//			case R.id.pps_footbar_doublearrow_right:
	//				index = (index + 1) % 4;
	//
	//				if (index == 2 && !isLOBPCDTV) {
	//					// Show bill type for PCD & TV Only
	//					index = 3;
	//				}
	//				updatePPSBarText();
	//				break;
	//			case R.id.pps_footbar_Txt:
	//				if (index == 1) {
	//					// Copy to clipboard only supports Account No.
	//					ClipboardManager cm = (ClipboardManager) getSystemService(CLIPBOARD_SERVICE);
	//					cm.setText(acctnum);
	//					DialogHelper.createSimpleDialog(this, getString(R.string.myhkt_pps_copyacctnum));
	//				}
	//				break;
	//		}
	//	}

	private final void loadData(String mAcctNum, int mLOB, String mAmount) {
		aq = new AAQuery(this);
		String billtype;
		String mechCode;
		// Prepare ProgressDialog
		//		progressDialogHelper = ProgressDialogHelper.getInstance(me);

		if (mLOB == R.string.CONST_LOB_LTS) {
			billtype = "";
			mechCode = Utils.getString(me, R.string.myhkt_pps_merchantcode_lts);
			isLOBPCDTV = false;
		} else if (mLOB == R.string.CONST_LOB_MOB || mLOB == R.string.CONST_LOB_IOI) {
			billtype = "";
			mechCode = Utils.getString(me, R.string.myhkt_pps_merchantcode_mob);
			isLOBPCDTV = false;
		} else if (mLOB == R.string.CONST_LOB_NE) {
			billtype = "";
			mechCode = Utils.getString(me, R.string.myhkt_pps_merchantcode_mob);
			isLOBPCDTV = false;
		} else if (mLOB == R.string.CONST_LOB_PCD || mLOB == R.string.CONST_LOB_IMS) {
			billtype = Utils.getString(me, R.string.myhkt_pps_billtype_pcd);
			mechCode = Utils.getString(me, R.string.myhkt_pps_merchantcode_pcd);
			isLOBPCDTV = true;
		} else if (mLOB == R.string.CONST_LOB_TV) {
			billtype = Utils.getString(me, R.string.myhkt_pps_billtype_pcd);
			mechCode = Utils.getString(me, R.string.myhkt_pps_merchantcode_pcd);
			isLOBPCDTV = true;
		} else if (mLOB == R.string.CONST_LOB_1010) {
			billtype = "";
			mechCode = Utils.getString(me, R.string.myhkt_pps_merchantcode_1010);
			isLOBPCDTV = false;
		} else if (mLOB == R.string.CONST_LOB_O2F) {
			billtype = "";
			mechCode = Utils.getString(me, R.string.myhkt_pps_merchantcode_o2f);
			isLOBPCDTV = false;
		} else {
			// Unknown Case
			billtype = "";
			mechCode = "";
			isLOBPCDTV = false;
		}

		billSumDetail[0] = String.format("%s %s", Utils.getString(me, R.string.myhkt_pps_merchantcode), mechCode);
		billSumDetail[1] = String.format("%s %s", Utils.getString(me, R.string.qrcode_acc_no_txt), mAcctNum);
		billSumDetail[2] = ("".equalsIgnoreCase(billtype)) ? "" : String.format("%s %s", Utils.getString(me, R.string.qrcode_bill_type_mobile_txt), billtype);
		billSumDetail[3] = String.format("%s %s", Utils.getString(me, R.string.qrcode_amt_txt), mAmount);

		// Loading WebView once OnCreate
		webView = aq.id(R.id.pps_webview).getWebView();
		//		webView.loadUrl(ppsURL);
		//		viewholder.pps_webview = (WebView) findViewById(R.id.pps_webview);

		WebSettings websettings = webView.getSettings();
		websettings.setSupportZoom(true);
		websettings.setBuiltInZoomControls(true);
		websettings.setJavaScriptEnabled(true);
		webView.setWebViewClient(new WebViewClient() {
			@Override
			public void onPageFinished(WebView view, String url) {
				super.onPageFinished(view, url);
				//				progressDialogHelper.dismissProgressDialog(true);
			}

			@Override
			public void onPageStarted(WebView view, String url, Bitmap favicon) {
				super.onPageStarted(view, url, favicon);
				//				progressDialogHelper.showProgressDialog(me, me.onstopHandler, true);
			}

			@Override
			public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
				super.onReceivedError(view, errorCode, description, failingUrl);
				//				progressDialogHelper.dismissProgressDialog(true);
			}
		});
		//		progressDialogHelper.showProgressDialog(me, me.onstopHandler, true);
		webView.loadData("LOADING.....", "", "");

		DialogHelper.createSimpleDialog(this, getString(R.string.myhkt_pps_enterweb), getString(R.string.btn_ok), new OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();
				webView.loadUrl(ppsURL);
			}
		});
	}

	private final void updatePPSBarText() {
		aq = new AAQuery(this);
		if (index == 1) {
			aq.id(R.id.pps_footbar_Txt).getTextView().setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.pps_btn_copy, 0);
			//			viewholder.pps_footbar_Txt.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.pps_btn_copy, 0);

		} else {
			aq.id(R.id.pps_footbar_Txt).getTextView().setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
			//			viewholder.pps_footbar_Txt.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
		}

		if (index < billSumDetail.length) {
			aq.id(R.id.pps_footbar_Txt).text(billSumDetail[index]);
			//			viewholder.pps_footbar_Txt.setText(billSumDetail[index]);
		}
	}

	protected void setModuleID() {
		//Module id
		switch (lob) {
		case R.string.CONST_LOB_LTS:
			moduleId = getResString(R.string.MODULE_LTS_BILL);
			break;
		case R.string.CONST_LOB_PCD:
			moduleId = getResString(R.string.MODULE_PCD_BILL);
			break;
		case R.string.CONST_LOB_TV:
			moduleId = getResString(R.string.MODULE_TV_BILL);
			break;
		case R.string.CONST_LOB_O2F:
			if (ClnEnv.isMyMobFlag()) {
				moduleId = getResString(R.string.MODULE_O2F_MM_BILL);
			} else {
				moduleId = getResString(R.string.MODULE_O2F_BILL);
			}
			break;
		case R.string.CONST_LOB_MOB:
			if (ClnEnv.isMyMobFlag()) {
				moduleId = getResString(R.string.MODULE_MOB_MM_BILL);
			} else {
				moduleId = getResString(R.string.MODULE_MOB_BILL);
			}
			break;
		case R.string.CONST_LOB_IOI:
			if (ClnEnv.isMyMobFlag()) {
				moduleId = getResString(R.string.MODULE_IOI_MM_BILL);
			} else {
				moduleId = getResString(R.string.MODULE_IOI_BILL);
			}
			break;
		case R.string.CONST_LOB_1010:
			if (ClnEnv.isMyMobFlag()) {
				moduleId = getResString(R.string.MODULE_101_MM_BILL);
			} else {
				moduleId = getResString(R.string.MODULE_101_BILL);
			}
			break;	
		}			
	}

	private final int getLobIcon() {
		int lobType = acctAgent.getLobType();
		if (lobType == R.string.CONST_LOB_1010 || lobType == R.string.CONST_LOB_IOI) {
			return R.drawable.logo_1010;
		} else if (lobType == R.string.CONST_LOB_MOB || lobType == R.string.CONST_LOB_O2F) {
			return R.drawable.logo_csl;
		} else if (lobType == R.string.CONST_LOB_LTS) {
			switch (ltsType) {
			case R.string.CONST_LTS_FIXEDLINE:
				return R.drawable.logo_fixedline_residential;
			case R.string.CONST_LTS_EYE:
				return R.drawable.logo_fixedline_eye;
			case R.string.CONST_LTS_IDD0060:
				return R.drawable.logo_fixedline_0060;
			case R.string.CONST_LTS_CALLINGCARD:
				return R.drawable.logo_fixedline_global;
			case R.string.CONST_LTS_ONECALL:
				return R.drawable.logo_fixedline_onecall;
			case R.string.CONST_LTS_ICFS:
				return R.drawable.logo_fixedline_icfs;
			default:
				return R.drawable.logo_fixedline;
			}
		} else if (lobType == R.string.CONST_LOB_PCD) {
			return R.drawable.logo_netvigator;
		} else if (lobType == R.string.CONST_LOB_TV) {
			return R.drawable.logo_now;
		}
		return 0;
	}

	// Confirm Exit
	protected final void displayConfirmExitDialog() {
		AlertDialog.Builder builder = new Builder(this);
		builder.setMessage(Utils.getString(me, R.string.myhkt_pps_exitweb));
		builder.setPositiveButton(Utils.getString(me, R.string.myhkt_btn_no), new OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();
			}
		});
		builder.setNegativeButton(Utils.getString(me, R.string.myhkt_btn_yes), new OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();
				finish();
				overridePendingTransition(R.anim.left_slide_in, R.anim.right_slide_out);
			}
		});	
		builder.create().show();
	}

	//	private void displayWebView(String url) {
	//		webView.setWebViewClient(new WebViewClient());
	//		webView.setVisibility(View.VISIBLE);
	//		webView.getSettings().setJavaScriptEnabled(true);
	//		webView.getSettings().setBuiltInZoomControls(true);
	//		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
	//			webView.getSettings().setDisplayZoomControls(false);
	//		}
	//		webView.getSettings().setLoadWithOverviewMode(true);
	//		webView.getSettings().setUseWideViewPort(true);
	////		browser_webview.setWebViewClient(webview);
	//		if (!"".equalsIgnoreCase(url)) webView.loadUrl(url);
	//	}
}
