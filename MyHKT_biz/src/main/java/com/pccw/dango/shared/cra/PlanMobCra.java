/*
    Crate for Plan (MOB)
    
    Keywords
    --------
    $URL: $
    $Rev: $
    $Date: $
    $Author: $
*/

package com.pccw.dango.shared.cra;

import java.io.Serializable;

import com.pccw.dango.shared.entity.MobUsage;
import com.pccw.dango.shared.entity.MupMrt;
import com.pccw.dango.shared.entity.SubnRec;
import com.pccw.dango.shared.g3entity.G3BoostUpOffer1DTO;
import com.pccw.dango.shared.g3entity.G3DisplayServiceItemDTO;
import com.pccw.dango.shared.g3entity.G3RechargeHistDTO;
import com.pccw.wheat.shared.tool.Tool;

public class PlanMobCra extends BaseCraEx implements Serializable
{
    private static final long serialVersionUID = -7411571296393209273L;
    
    private String                  iLoginId;               /* Login ID                                      */
    private SubnRec                 iSubnRec;               /* Subscription Record                           */
    private G3DisplayServiceItemDTO iTopupItem;             /* Top up item                                   */
    private G3BoostUpOffer1DTO      iBstUpOff;              /* Selected boost up offer                       */
    private String                  iTopupActn;             /* Top up Action                                 */
    private String                  iBstUpHistStDt;         /* Boost up Hist Start Date                      */
    private String                  iBstUpHistEnDt;         /* Boost up Hist End Date                        */

    private MobUsage                oMobUsage;              /* Mobile Usage                                  */
    private String                  oTtlEntMB;              /* Total Entitlement In MB                       */
    private String                  oMthlyPkgMB;            /* Monthly Package In MB                         */
    private String                  oTtlEntKB;              /* Total Entitlement In KB                       */
    private String                  oMthlyPkgKB;            /* Monthly Package In KB                         */
    private String                  oEntExpDT;              /* Entitlement Expiry Datetime                   */
    private String                  oVbpCustTy;             /* Vbp Customer Type (for C use)                 */
    private String                  oContrEndDt;            /* Contract End Date (for C use                  */
    private G3BoostUpOffer1DTO[]    oG3BoostUpOffer1DTOAry; /* Array of Boost-up Offer                       */
    private G3RechargeHistDTO[]     oG3RechargeHistDTOAry;  /* Array of Recharge History                     */
                                                            /* Sub Group Code                                */
    private MupMrt[]                oMupMrtAry;             /* MUP MRT List                                  */
    
    public static String            TP_ACTN_I = "I";		/* Topup Action - Initial                        */
    public static String            TP_ACTN_R = "R";		/* Topup Action - Review                         */
    public static String            TP_ACTN_C = "C";		/* Topup Action - Confirm                        */

    
    public static void main(String rArg[])
    {
        System.out.println(getVer());
        return;
    }
    
    
    public static String getVer()
    {
        return ("$URL: $, $Rev: $");
    }


    public PlanMobCra()
    {
        initAndClear();
    }


    protected void init()
    {
        super.init();
    }


    public void clear()
    {
        super.clear();
        
        clearILoginId();
        clearISubnRec();
        clearITopupItem();
        clearITopupActn();
        clearIBstUpOff();
        clearIBstUpHistStDt();
        clearIBstUpHistEnDt();
        clearOMobUsage();
        clearOTtlEntMB();
        clearOMthlyPkgMB();
        clearOTtlEntKB();
        clearOMthlyPkgKB();
        clearOEntExpDT();
        clearOContrEndDt();
        clearOVbpCustTy();
        clearOG3BoostUpOffer1DTOAry();
        clearOG3RechargeHistDTOAry();
        clearOMupMrtAry();
    }


    public PlanMobCra copyFrom(PlanMobCra rSrc)
    {
        super.copyFrom(rSrc);
        
        setILoginId(rSrc.getILoginId());
        setISubnRec(rSrc.getISubnRec());
        setITopupItem(rSrc.getITopupItem());
        setIBstUpOff(rSrc.getIBstUpOff());
        setIBstUpHistStDt(rSrc.getIBstUpHistStDt());
        setIBstUpHistEnDt(rSrc.getIBstUpHistEnDt());
        setITopupActn(rSrc.getITopupActn());
        setOMobUsage(rSrc.getOMobUsage());
        setOTtlEntMB(rSrc.getOTtlEntMB());
        setOMthlyPkgMB(rSrc.getOMthlyPkgMB());
        setOTtlEntKB(rSrc.getOTtlEntKB());
        setOMthlyPkgKB(rSrc.getOMthlyPkgKB());
        setOEntExpDT(rSrc.getOEntExpDT());
        setOContrEndDt(rSrc.getOContrEndDt());
        setOVbpCustTy(rSrc.getOVbpCustTy());
        setOG3BoostUpOffer1DTOAry(rSrc.getOG3BoostUpOffer1DTOAry());
        setOG3RechargeHistDTOAry(rSrc.getOG3RechargeHistDTOAry());
        setOMupMrtAry(rSrc.getOMupMrtAry());
        
        
        return (this);
    }


    public PlanMobCra copyTo(PlanMobCra rDes)
    {
        rDes.copyFrom(this);
        return (rDes);
    }
    
    
    public PlanMobCra copyMe()
    {
        PlanMobCra                  rDes;

        rDes = new PlanMobCra();
        rDes.copyFrom(this);
        return (rDes);
    }


    public void clearILoginId()
    {
        iLoginId = "";
    }


    public void setILoginId(String rArg)
    {
        iLoginId = rArg;
    }


    public String getILoginId()
    {
        return (iLoginId);
    }


    public void clearISubnRec()
    {
        iSubnRec = new SubnRec();
    }


    public void setISubnRec(SubnRec rArg)
    {
        iSubnRec = rArg;
    }


    public SubnRec getISubnRec()
    {
        return (iSubnRec);
    }


    
    public void setITopupItem(G3DisplayServiceItemDTO rArg) 
    {
        iTopupItem = rArg;
    }

    
    public G3DisplayServiceItemDTO getITopupItem() 
    {
        return iTopupItem;
    }

    
    public void clearITopupItem()
    {
        setITopupItem(new G3DisplayServiceItemDTO());
    }

    
    public void setITopupActn(String rArg) 
    {
        iTopupActn = rArg;
    }

    
    public String getITopupActn() 
    {
        return iTopupActn;
    }

    
    public void clearITopupActn()
    {
        setITopupActn("");
    }
    
    
    public void setIBstUpOff(G3BoostUpOffer1DTO rArg) 
    {
        iBstUpOff = rArg;
    }

    
    public void setIBstUpHistStDt(String rArg) 
    {
        iBstUpHistStDt = rArg;
    }

    
    public String getIBstUpHistStDt() 
    {
        return iBstUpHistStDt;
    }

    
    public void clearIBstUpHistStDt()
    {
        iBstUpHistStDt = "";
    }

    
    public void setIBstUpHistEnDt(String rArg) 
    {
        iBstUpHistEnDt = rArg;
    }

    
    public String getIBstUpHistEnDt() 
    {
        return iBstUpHistEnDt;
    }

    
    public void clearIBstUpHistEnDt()
    {
        iBstUpHistEnDt = "";
    }
    
    public G3BoostUpOffer1DTO getIBstUpOff() 
    {
        return iBstUpOff;
    }

    
    public void clearIBstUpOff()
    {
        setIBstUpOff(new G3BoostUpOffer1DTO());
    }

    
    public void clearOMobUsage()
    {
        oMobUsage = new MobUsage();
    }


    public void setOMobUsage(MobUsage rArg)
    {
        oMobUsage = rArg;
    }


    public MobUsage getOMobUsage()
    {
        return (oMobUsage);
    }


    public void clearOTtlEntMB()
    {
        oTtlEntMB = "";
    }


    public void setOTtlEntMB(String rArg)
    {
        oTtlEntMB = rArg;
    }


    public String getOTtlEntMB()
    {
        return (oTtlEntMB);
    }


    public void clearOMthlyPkgMB()
    {
        oMthlyPkgMB = "";
    }


    public void setOMthlyPkgMB(String rArg)
    {
        oMthlyPkgMB = rArg;
    }


    public String getOMthlyPkgMB()
    {
        return (oMthlyPkgMB);
    }

    
    public void clearOTtlEntKB()
    {
        oTtlEntKB = "";
    }


    public void setOTtlEntKB(String rArg)
    {
        oTtlEntKB = rArg;
    }


    public String getOTtlEntKB()
    {
        return (oTtlEntKB);
    }


    public void clearOMthlyPkgKB()
    {
        oMthlyPkgKB = "";
    }


    public void setOMthlyPkgKB(String rArg)
    {
        oMthlyPkgKB = rArg;
    }


    public String getOMthlyPkgKB()
    {
        return (oMthlyPkgKB);
    }
    
    
    public void clearOEntExpDT()
    {
        oEntExpDT = "";
    }


    public void setOEntExpDT(String rArg)
    {
        oEntExpDT = rArg;
    }


    public String getOEntExpDT()
    {
        return (oEntExpDT);
    }

    
    public void clearOContrEndDt()
    {
        oContrEndDt = "";
    }


    public void setOContrEndDt(String rArg)
    {
        oContrEndDt = rArg;
    }


    public String getOContrEndDt()
    {
        return (oContrEndDt);
    }
    

    public void clearOVbpCustTy()
    {
        oVbpCustTy = "";
    }


    public void setOVbpCustTy(String rArg)
    {
        oVbpCustTy = rArg;
    }


    public String getOVbpCustTy()
    {
        return (oVbpCustTy);
    }


    public void clearOG3BoostUpOffer1DTOAry()
    {
        oG3BoostUpOffer1DTOAry = new G3BoostUpOffer1DTO[0];
    }


    public void setOG3BoostUpOffer1DTOAry(G3BoostUpOffer1DTO[] rArg)
    {
        oG3BoostUpOffer1DTOAry = rArg;
    }


    public G3BoostUpOffer1DTO[] getOG3BoostUpOffer1DTOAry()
    {
        return (oG3BoostUpOffer1DTOAry);
    }


    public void clearOG3RechargeHistDTOAry()
    {
        oG3RechargeHistDTOAry = new G3RechargeHistDTO[0];
    }


    public void setOG3RechargeHistDTOAry(G3RechargeHistDTO[] rArg)
    {
        oG3RechargeHistDTOAry = rArg;
    }


    public G3RechargeHistDTO[] getOG3RechargeHistDTOAry()
    {
        return (oG3RechargeHistDTOAry);
    }
    
    
    public void clearOMupMrtAry()
    {
        oMupMrtAry = new MupMrt[0];
    }


    public void setOMupMrtAry(MupMrt[] rArg)
    {
        oMupMrtAry = rArg;
    }


    public MupMrt[] getOMupMrtAry()
    {
        return (oMupMrtAry);
    }
    

    public boolean isMaster() 
    {
        return (isMaster(getISubnRec().srvNum));
    }
    
    
    public boolean isMaster(String rDn) 
    {
        for (int r1 =0; r1 < getOMupMrtAry().length; r1++) {
            
            if (rDn.equals(getOMupMrtAry()[r1].getDn()) && Tool.isOn(getOMupMrtAry()[r1].getMaster()))
                return (true);
        }
        
        return (false);
    }

    
    public String getMasterMrt() 
    {
        for (int r1 =0; r1 < getOMupMrtAry().length; r1++) {
            
            if (Tool.isOn(getOMupMrtAry()[r1].getMaster()))
                return (getOMupMrtAry()[r1].getDn());
        }
        
        return ("");
    }

    
    public boolean isMup() 
    {
        return (isMup(getISubnRec().srvNum));
    }
    
    
    public boolean isMup(String rDn) 
    {
        for (int r1 =0; r1 < getOMupMrtAry().length; r1++) {
            
            if (!Tool.isOn(getOMupMrtAry()[r1].getMaster()))
                return (true);
        }
        
        return (false);
    }
}
