package com.pccw.dango.shared.g3entity;

import java.io.Serializable;

public class G3PaymentRejectStatusDTO implements Serializable 
{
    private static final long serialVersionUID = 913797205536786349L;
    
    private String acctNum;
    private String paymentRejSts;
    private String errCode;
    private String errMsg;
    private String resultRefCode;
    
    public String getAcctNum() {
        return acctNum;
    }
    public void setAcctNum(String acctNum) {
        this.acctNum = acctNum;
    }
    public String getPaymentRejSts() {
        return paymentRejSts;
    }
    public void setPaymentRejSts(String paymentRejSts) {
        this.paymentRejSts = paymentRejSts;
    }
    public String getErrCode() {
        return errCode;
    }
    public void setErrCode(String errCode) {
        this.errCode = errCode;
    }
    public String getErrMsg() {
        return errMsg;
    }
    public void setErrMsg(String errMsg) {
        this.errMsg = errMsg;
    }
    public String getResultRefCode() {
        return resultRefCode;
    }
    public void setResultRefCode(String resultRefCode) {
        this.resultRefCode = resultRefCode;
    }
}
