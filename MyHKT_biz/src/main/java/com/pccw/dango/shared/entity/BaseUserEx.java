/*
    Base User template in DANGO
    
    This is implemented as a Class instead of Abstract
    to cope with JSON limitation.
    
    Keywords
    --------
    $URL: $
    $Rev: $
    $Date: $
    $Author: $
*/

package com.pccw.dango.shared.entity;

import java.io.Serializable;

import com.pccw.wheat.shared.entity.BaseUser;
import com.pccw.wheat.shared.tool.MiniRtException;


public class BaseUserEx implements BaseUser, Serializable
{
    /**
	 * 
	 */
	private static final long serialVersionUID = 6542956586204167093L;
	public static final String      TY_NPSN     = "NPSN";   /* Not a Person     */
    public static final String      TY_SVEE     = "SVEE";   /* Servee           */ 
    public static final String      TY_STFF     = "STFF";   /* HKT/PCCW Staff   */
    public static final String      TY_OSUR     = "OSUR";   /* OS User          */

    
    public BaseUserEx()
    {
    }
    
    
    public boolean isNotAPerson()
    {
        return (getType().equals(TY_NPSN));
    }
    
    
    public boolean isSvee()
    {
        return (getType().equals(TY_SVEE));
    }
    
    
    public boolean isStaff()
    {
        return (getType().equals(TY_STFF));
    }
    
    
    public boolean isOsUser()
    {
        return (getType().equals(TY_OSUR));
    }


    public String getId()
    {
        throw new MiniRtException("Non-Implemented Function!");
    }


    public String getType()
    {
        throw new MiniRtException("Non-Implemented Function!");
    }


    public String getRole()
    {
        throw new MiniRtException("Non-Implemented Function!");
    }


    public String[] getRghtAry()
    {
        throw new MiniRtException("Non-Implemented Function!");
    }


    public boolean isAuth(String rRght)
    {
        throw new MiniRtException("Non-Implemented Function!");
    }
}