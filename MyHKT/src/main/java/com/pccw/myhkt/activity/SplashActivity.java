package com.pccw.myhkt.activity;

import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Point;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.SystemClock;
import android.view.Display;
import android.widget.ImageView;

import androidx.fragment.app.FragmentActivity;

import com.pccw.myhkt.APIsManager;
import com.pccw.myhkt.APIsResponse;
import com.pccw.myhkt.ClnEnv;
import com.pccw.myhkt.R;
import com.pccw.myhkt.Utils;
import com.pccw.myhkt.lib.ui.AAQuery;
import com.pccw.myhkt.model.PushData;
import com.pccw.myhkt.service.ConnectivityChangeReceiver;
import com.pccw.myhkt.service.VersionCheckService;
import com.pccw.myhkt.util.Constant;

/************************************************************************
 File       : SplashActivity.java
 Desc       : Splash Screen (opening screen)
 Name       : SplashActivity
 Created by : Derek Tsui
 Date       : 30/12/2015

 Change History:
 Date       Modified By			Description
 ---------- ----------------	-------------------------------
 30/12/2015 Derek Tsui			- First draft
 06/06/2018 Abdulmoiz Esmail     - 74064: Force Update > Tap Update/Exit buttons (multiple) > Skip force update
 06/07/2018 Abdulmoiz Esmail     - Navigate to next screen when version checker is failed (e.i: Device is in Airplane mode)
 *************************************************************************/

public class SplashActivity extends FragmentActivity implements APIsManager.OnAPIsListener {
    private AAQuery aq;
    private Long startTime;
    private boolean bRunning;
    private static SplashActivity me;
    public static boolean isNextScreenReady = false;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        APIsManager.requestAppConfigJSON(this, this);

        ClnEnv.isForceCloseApp = false;
        ClnEnv.isVCFinish = false;
        me = this;
        aq = new AAQuery(this);
        setContentView(R.layout.activity_splash);

        // Init splash images
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int deviceWidth = size.x;
        int extralinespace = (int) getResources().getDimension(R.dimen.extralinespace);

        ImageView hktlogoView = aq.id(R.id.splash_hktlogo_img).getImageView();
        hktlogoView.getLayoutParams().width = deviceWidth * 2 / 3;
        hktlogoView.requestLayout();

        ImageView pccwlogoView = aq.id(R.id.splash_pccwlogo_img).getImageView();
        pccwlogoView.getLayoutParams().width = deviceWidth * 2 / 3;
        pccwlogoView.requestLayout();
        aq.marginpx(R.id.splash_pccwlogo_img, 0, 0, 0, extralinespace);

        startTime = System.currentTimeMillis();
        bRunning = false;

        // Set Flag for Bill Message Result redirected Activity from Splash > MainMenu > ServiceList > TargetService
        if (getIntent().getBooleanExtra("BILLMSG_READNOW", false)) {
            ClnEnv.clear(me);
            ClnEnv.setPref(me.getApplicationContext(), me.getString(R.string.CONST_PREF_BILLMSG_NOTICEFLAG), getIntent().getBooleanExtra("BILLMSG_READNOW", false));
            if (getIntent().getExtras() != null && getIntent().getExtras().getSerializable("SERIAL_BILLMSG") != null) {
                ClnEnv.setPushDataBill((PushData) getIntent().getExtras().getSerializable("SERIAL_BILLMSG"));
            }
            getIntent().removeExtra("BILLMSG_READNOW");
            getIntent().removeExtra("SERIAL_BILLMSG");
            clearNotificationList();
        } else {
            ClnEnv.setPref(me.getApplicationContext(), me.getString(R.string.CONST_PREF_BILLMSG_NOTICEFLAG), getIntent().getBooleanExtra("BILLMSG_READNOW", false));
        }
        // Set Flag for General Message Result redirected Activity from Splash > MainMenu
        if (getIntent().getBooleanExtra("GENMSG_READNOW", false)) {
            ClnEnv.clear(me);
            ClnEnv.setPref(me.getApplicationContext(), me.getString(R.string.CONST_PREF_GENMSG_NOTICEFLAG), getIntent().getBooleanExtra("GENMSG_READNOW", false));
            if (getIntent().getExtras() != null && getIntent().getExtras().getSerializable("SERIAL_GENMSG") != null) {
                ClnEnv.setPushDataGen((PushData) getIntent().getExtras().getSerializable("SERIAL_GENMSG"));
            }
            getIntent().removeExtra("GENMSG_READNOW");
            getIntent().removeExtra("SERIAL_GENMSG");
            clearNotificationList();
        }
        new TimerAsyncTask().execute();

//        FirebaseDynamicLinks.getInstance().getDynamicLink(getIntent()).addOnSuccessListener(this, new OnSuccessListener<PendingDynamicLinkData>() {
//            @Override
//            public void onSuccess(PendingDynamicLinkData pendingDynamicLinkData) {
//                Log.i("lwg", "We have a dynamic link!");
//                Uri deepLink = null;
//                if (pendingDynamicLinkData !=null) {
//                    deepLink = pendingDynamicLinkData.getLink();
//                    assert deepLink != null;
//                    Log.i("lwg", deepLink.toString());
//                }
//            }
//        });

        // ATTENTION: This was auto-generated to handle app links.
        Intent appLinkIntent = getIntent();
        String appLinkAction = appLinkIntent.getAction();
        Uri appLinkData = appLinkIntent.getData();
    }

    private void clearNotificationList() {
        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.cancelAll();
    }

    @Override
    protected final void onResume() {
        super.onResume();
        try {
            // Start Version Check first
            Intent mVersionCheckerService = new Intent(this, VersionCheckService.class);
            mVersionCheckerService.setPackage(getPackageName());
            startService(mVersionCheckerService);

            IntentFilter filter = new IntentFilter(Constant.ACTION_APP_VERSION_IS_UPDATED);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
                registerReceiver(mVersionCheckBReceiver, filter, Context.RECEIVER_NOT_EXPORTED);
            } else {
                registerReceiver(mVersionCheckBReceiver, filter);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public final void onBackPressed() {
        finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        try {
            if (mVersionCheckBReceiver != null) {
                this.unregisterReceiver(mVersionCheckBReceiver);
            }
        } catch (Exception e) {
            // mVersionCheckBReceiver is already unregistered
            mVersionCheckBReceiver = null;
        }
    }

    @Override
    public void onSuccess(APIsResponse response) throws Exception {

    }

    @Override
    public void onFail(APIsResponse response) throws Exception {

    }

    // 20121212 Vincent, Create a AsyncTask for Timer
    class TimerAsyncTask extends AsyncTask<Void, String, Void> {
        @Override
        protected void onPreExecute() {
            bRunning = true;
        }

        @Override
        protected Void doInBackground(Void... params) {
            while (bRunning) {
                // Update Timer
                long spentTime = System.currentTimeMillis() - startTime;
                long seconds = (spentTime / 1000) % 60;
                if (seconds >= 1 && ClnEnv.isVCFinish) bRunning = false;
                SystemClock.sleep(1000);
            }

            // Fetch LoginID and Password for auto-login, if available
            if (ClnEnv.getPref(getApplicationContext(), getString(R.string.CONST_PREF_SAVELOGINID), false) && ClnEnv.getPref(getApplicationContext(), getString(R.string.CONST_PREF_SAVEPASSWORD), false)) {
                ClnEnv.setSessionLoginID(ClnEnv.getPref(getApplicationContext(), getString(R.string.CONST_PREF_LOGINID), ""));
                ClnEnv.setSessionPassword(ClnEnv.getEncPref(getApplicationContext(), ClnEnv.getSessionLoginID(), getString(R.string.CONST_PREF_PASSWORD), ""));
                ClnEnv.setSessionPremierFlag(ClnEnv.getPref(getApplicationContext(), getString(R.string.CONST_PREF_PREMIER_FLAG), false));

                if (Build.VERSION.SDK_INT >= 23 && Utils.isTouchIDAvailableOnSystem(SplashActivity.this) && !Utils.isTouchIDLoginActivated(SplashActivity.this)) {
                    Utils.saveTempUserCredentials(getApplicationContext(), ClnEnv.getPref(getApplicationContext(), getString(R.string.CONST_PREF_LOGINID), ""), ClnEnv.getEncPref(getApplicationContext(), ClnEnv.getSessionLoginID(), getString(R.string.CONST_PREF_PASSWORD), ""));
                }
            }
            return null;
        }

        protected void onPostExecute(Void result) {
            // For development, we need to check the size of log file
            if (getResources().getBoolean(R.bool.DEBUGGRQ) || getResources().getBoolean(R.bool.DEBUGCRA)) {
                ClnEnv.cleanupLogFile();
            }
            //Time ends, ready to show the next screen, regardless of the result the version checker
            nextScreenRouting();
        }
    }

    private void tacsOrMainMenu() {
        Intent intent;
        finish();
        // NOTE: MUST SHOW TACS WHEN DOING DEMO, RESTORE BEFORE RELEASING
        try {
            if (ClnEnv.getAppVersion(getApplicationContext()) != ClnEnv.getPref(getApplicationContext(), getString(R.string.CONST_PREF_TACSVERSION), 0)) {
                intent = new Intent(getApplicationContext(), TACSActivity.class);
            } else {
                intent = new Intent(getApplicationContext(), MainMenuActivity.class);
            }
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);

            if (!ClnEnv.isForceCloseApp && !isForcedUpdate()) {
                startActivity(intent);
            }
        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }

    private boolean isForcedUpdate() {
        String clientVersion = "";
        try {
            clientVersion = me.getApplicationContext().getPackageManager().getPackageInfo(me.getApplicationContext().getPackageName(), 0).versionName;
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (!ClnEnv.getPref(this, Constant.BLOCK_VERSION, "--").equalsIgnoreCase(clientVersion)) {
            //The device has internet connection but network fail : tested through proxy network
            //Shared pref BLOCK_VERSION is empty
            return false;
        } else {
            //Force update if:
            //-BLOCK_VERSION == installed device and internet is available (Version checker service is running)
            //-isNextScreenReady == false (Waiting for version check service to fail (true)) and it internet is available
            return (ClnEnv.getPref(this, Constant.BLOCK_VERSION, "--").equalsIgnoreCase(clientVersion) || isNextScreenReady) && ConnectivityChangeReceiver.isConnected(this);
        }
    }

    public final void nextScreenRouting() {
        tacsOrMainMenu();
    }

    BroadcastReceiver mVersionCheckBReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (Constant.ACTION_APP_VERSION_IS_UPDATED.equals(intent.getAction())) {
                //show next screen if the timer is done
                isNextScreenReady = true;
            }
        }
    };
}
