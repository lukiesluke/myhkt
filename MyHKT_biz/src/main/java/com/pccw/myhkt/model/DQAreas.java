package com.pccw.myhkt.model;

import java.io.Serializable;

import com.pccw.dango.shared.cra.BaseCraEx;

public class DQAreas extends BaseCraEx implements Serializable {

	/**
	 * 
	 */
	private static final long	serialVersionUID	= 44082999378198380L;
	public DQArea[]				areas;
}