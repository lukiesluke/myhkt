package com.pccw.myhkt.fragment;

import java.lang.Thread.UncaughtExceptionHandler;
import java.util.ArrayList;
import java.util.List;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.graphics.Point;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.pccw.dango.shared.cra.PlanMobCra;
import com.pccw.dango.shared.entity.MobUsage;
import com.pccw.dango.shared.entity.SubnRec;
import com.pccw.dango.shared.g3entity.G3BoostUpOffer1DTO;
import com.pccw.dango.shared.g3entity.G3DisplayServiceItemDTO;
import com.pccw.dango.shared.g3entity.G3UsageQuotaDTO;
import com.pccw.myhkt.APIsResponse;
import com.pccw.myhkt.ClnEnv;
import com.pccw.biz.myhkt.R;
import com.pccw.myhkt.Utils;
import com.pccw.myhkt.activity.BaseActivity;
import com.pccw.myhkt.cell.model.ArrowTextCell;
import com.pccw.myhkt.cell.model.Cell;
import com.pccw.myhkt.cell.model.CircleViewCell;
import com.pccw.myhkt.cell.model.IconTextCell;
import com.pccw.myhkt.cell.model.SingleBtnCell;
import com.pccw.myhkt.cell.model.SmallTextCell;
import com.pccw.myhkt.cell.model.WebViewCell;
import com.pccw.myhkt.cell.view.CellViewAdapter;
import com.pccw.myhkt.lib.ui.AAQuery;
import com.pccw.myhkt.model.AcctAgent;
/************************************************************************
File       : UsageDataChild1Fragment.java
Desc       : View for showing csl/1010/o2f Usage/Roaming
			 isMob: true for csl , false for 1010/o2f case
			 isLocal: true for local data, false for roaming
Name       : UsageDataChild1Fragment
Created by : Andy Wong
Date       : 15/03/2016

Change History:
Date       Modified By			Description
---------- ----------------	-------------------------------
15/03/2016 Andy Wong			- First draft
14/06/2017 Abdulmoiz Esmail     - Update showing of remaining data usage, use the KB or MB form depending on the size. 
15/06/2017 Abdulmoiz Esmail		- 59645: Change HSim Normal Sim header as Secondary Sim in Usage
 *************************************************************************/
@SuppressLint("ResourceAsColor")
public class UsageDataChild1Fragment extends BaseServiceFragment {
	private UsageDataChild1Fragment me;
	private View 		myView;
	private AAQuery		aq;
	private List<Cell> 	cellList;
	private int 		col2Width;
	private int 		col3Width;
	private int 		btnWidth;

	private Boolean 	isMob = false;
	private Boolean 	isCSimPrim = false;
	private Boolean 	isLocal = true;
	
	private Boolean 	isMup = false;
	private Boolean 	isMaster = false;

	private OnUsageDataListener callback;
	private OnUsageListener 	callback_usage;

	private CellViewAdapter cellViewAdapter;
	private LinearLayout 	frame;

	List<G3DisplayServiceItemDTO> serviceList;

	//Use in UsageFragment
	public interface OnUsageListener {
		void setPlanMobCra(PlanMobCra mplanMobcra);
		PlanMobCra getPlanMobCra();
		//		public int getCurrentUsagePage();
        void setSelectedServiceItem(G3DisplayServiceItemDTO g3DisplayServiceItemDTO);
		G3DisplayServiceItemDTO getSelectedServiceItem();
		G3BoostUpOffer1DTO getSelectedG3BoostUpOfferDTO();
		void getSelectedG3BoostUpOfferDTO(G3BoostUpOffer1DTO g3BoostUpOffer1DTO);
		void 	setActiveChildview(int index);
		int 	getActiveChildview();
		void 	displayChildview(Boolean isBack);
		Boolean getIsLocal();
		int getLob();
	}
	public interface OnUsageDataListener {
		PlanMobCra getPlanMobCra();
		void setPlanMobCra(PlanMobCra mplanMobcra);
		List<G3DisplayServiceItemDTO> getLocalServiceList();
		List<G3DisplayServiceItemDTO> getRommaServiceList();
		List<G3DisplayServiceItemDTO> getOtherLocalServiceList();
		List<G3DisplayServiceItemDTO> getOtherRommaServiceList();
		AcctAgent getAcctAgent();
	}


	private PlanMobCra planMobCra;
	private MobUsage mobUsage;

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		try {
			callback = (OnUsageDataListener)getParentFragment();
			callback_usage = (OnUsageListener)getParentFragment().getParentFragment();
		} catch (ClassCastException e) {
			throw new ClassCastException(getParentFragment().toString() + " must implement OnUsageListener");
		}		
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {	
		super.onCreate(savedInstanceState);
        isMob = SubnRec.LOB_MOB.equals(callback_main.getAcctAgent().getSubnRec().lob) || SubnRec.LOB_IOI.equals(callback_main.getAcctAgent().getSubnRec().lob) || SubnRec.WLOB_XMOB.equals(callback_main.getAcctAgent().getSubnRec().lob);
		//		isMob = true;//derek testing remove after
		isLocal = callback_usage.getIsLocal();
		if (callback.getPlanMobCra() != null && callback.getPlanMobCra().getOMobUsage()!= null) {
			G3UsageQuotaDTO g3OtherUsageDTO = callback.getPlanMobCra().getOMobUsage().getG3UsageQuotaDTO();
			if (g3OtherUsageDTO != null) {			
				isCSimPrim = "P".equalsIgnoreCase(g3OtherUsageDTO.getPrimaryFlag());
			}			
		}
	}

	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		me = this;
		View fragmentLayout = inflater.inflate(R.layout.fragment_usagedata_child1, container, false);
		myView = fragmentLayout;
		initData();
		initUI();
		return fragmentLayout;
	}

	protected void initData() {    		
		super.initData();
		aq = new AAQuery(myView);
		btnWidth = deviceWidth/2 - basePadding; 
		col2Width = (deviceWidth - basePadding *2 )/4;
		col3Width = (deviceWidth - basePadding *2 ) * 2/9;
		planMobCra = callback.getPlanMobCra();
		cellViewAdapter = new CellViewAdapter(me.getActivity());
	}

	protected void initUI() {
		super.initUI();

		if (planMobCra == null) {
			if (isMob) {	
				initMobUI();
			} else {
				init1010UI();	
			}	
		} else {
			if (isMob) {
				setMobUI();
			} else {
				set1010UI();	
			}			
		}		
	}

	protected void initMobUI() {
		aq.id(R.id.fragment_usagedata_layout).backgroundColorId(R.color.white);
		aq.marginpx(R.id.fragment_usagedata_layout, 0, 0, 0, 0);
		frame = (LinearLayout) aq.id(R.id.fragment_usagedata_listview).getView();

		cellList = new ArrayList<Cell>();
		Cell cell;
		int mobusageImg = Utils.theme(R.drawable.icon_mobusage, callback_usage.getLob()); //themed icon
		cell = new Cell(Cell.ICONTEXT, mobusageImg, getResString(isLocal ? R.string.usage_mobile_data : R.string.usage_roam_mobile_data));
		cellList.add(cell);
		cellViewAdapter.setView(frame, cellList);
	}

	protected void init1010UI() {
		if (debug) Log.i(TAG, callback.getAcctAgent().getLob());
		aq.id(R.id.fragment_usagedata_layout).backgroundColorId(R.color.white);
		aq.marginpx(R.id.fragment_usagedata_layout, 0, 0, 0, 0);
		frame = (LinearLayout) aq.id(R.id.fragment_usagedata_listview).getView();

		cellList = new ArrayList<Cell>();
		Cell cell;
		if (SubnRec.LOB_MOB.equalsIgnoreCase(callback.getAcctAgent().getLob()) || SubnRec.LOB_IOI.equalsIgnoreCase(callback.getAcctAgent().getLob())) {
			int mobusageImg = Utils.theme(R.drawable.icon_mobusage, callback_usage.getLob()); //themed icon
			cell = new Cell(Cell.ICONTEXT, mobusageImg, getResString(isLocal ? R.string.usage_mobile_data : R.string.roam_mobile_data));
			cell.setLeftPadding(basePadding);
			cell.setRightPadding(basePadding);
			cellList.add(cell);
		} else if (SubnRec.LOB_O2F.equalsIgnoreCase(callback.getAcctAgent().getLob()) || SubnRec.LOB_101.equalsIgnoreCase(callback.getAcctAgent().getLob())) {
			int mobusageImg = Utils.theme(R.drawable.icon_mobusage, callback_usage.getLob()); //themed icon
			cell = new Cell(Cell.ICONTEXT, mobusageImg, getResString(isLocal ? R.string.csl_usage_mobile_data : R.string.roam_mobile_data));
			cell.setLeftPadding(basePadding);
			cell.setRightPadding(basePadding);
			cellList.add(cell);
		}
		cellViewAdapter.setView(frame, cellList);
	}

	protected void setMobUI(){
		aq.id(R.id.fragment_usagedata_layout).backgroundColorId(R.color.white);
		aq.marginpx(R.id.fragment_usagedata_layout, 0, 0, 0, 0);
		frame = (LinearLayout) aq.id(R.id.fragment_usagedata_listview).getView();
		
		isMup = planMobCra.isMup();
		isMaster = planMobCra.isMaster();
		
		//get service list processed by usagedatafragment		
		serviceList = isLocal ? callback.getLocalServiceList() : callback.getRommaServiceList();
		cellList = new ArrayList<Cell>();

		if (isLocal) {

			int[] widthText2Col = {col2Width, col2Width};
			int[] widthText3Col = {col3Width, col3Width, col3Width};
			mobUsage = planMobCra.getOMobUsage();
//			planMobCra namama = planMobCra.isMup;
//			isMup()

			G3UsageQuotaDTO g3UsageQuotaDTO = mobUsage.getG3UsageQuotaDTO();
		
			

			if (isMup) {
				G3DisplayServiceItemDTO mupItem = null;
				String masterMrt = planMobCra.getMasterMrt();
				if (serviceList !=null && serviceList.size() > 0) {
					for (int i=0;i < serviceList.size() ; i++){
						if ("MUP Individual Usage".equals(serviceList.get(i).getServiceType())) {
							mupItem = serviceList.get(i);
							break;
						}
					}
					for (int i=0;i < serviceList.size() ; i++){
						if (!"MUP Individual Usage".equals(serviceList.get(i).getServiceType())) {
							this.addCircleView_MUP(serviceList.get(i), mupItem, masterMrt);			
						}
					}
				}	
			} else {
				if (serviceList !=null && serviceList.size() > 0) {
					for (int i=0;i < serviceList.size() ; i++){
						addCircleView(serviceList.get(i));				
					}
				}	
			}
			//Local Voice part
			String subTitle ="";
			if (g3UsageQuotaDTO.getAsOfDate() != null) {
				String outStr = "";
				try {
					outStr = Utils.toDateString(g3UsageQuotaDTO.getAsOfDate(), "yyyy-MM-dd", Utils.getString(me.getActivity(), R.string.usage_date_format));
				} catch (Exception e) {
					outStr = g3UsageQuotaDTO.getAsOfDate();
				}
				subTitle = String.format("%s%s", getResString(R.string.as_of), outStr);
			}		
			if (!isMup) {
				String[] title = {};
				String[] unitVoice = {};
				String[] minIntra = {};
				String[] minNormal= {};
				title = new String[] {getResString(R.string.usage_individual_entitled), getResString(R.string.usage_individual)};
				unitVoice = new String[] {"("+getResString(R.string.mins)+")", "("+getResString(R.string.mins)+")" };
	
				String localVoiceIntraEntitlement = convertNum(g3UsageQuotaDTO.getLocalVoiceIntraEntitlement());
				String localVoiceInterEntitlement = convertNum(g3UsageQuotaDTO.getLocalVoiceInterEntitlement());
				if (localVoiceIntraEntitlement.equals(getResString(R.string.usage_unlimited)) || localVoiceInterEntitlement.equals(getResString(R.string.usage_unlimited))) {
					localVoiceIntraEntitlement = getResString(R.string.usage_unlimited);
					localVoiceInterEntitlement = getResString(R.string.usage_unlimited);
				}
				
				minIntra = new String[] {localVoiceIntraEntitlement,convertNum(g3UsageQuotaDTO.getLocalVoiceIntraUsage())};
				minNormal = new String[] {localVoiceInterEntitlement,convertNum(g3UsageQuotaDTO.getLocalVoiceInterUsage())};
	
				int voicecallImg = Utils.theme(R.drawable.icon_voicecall, callback_usage.getLob()); //themed icon
				IconTextCell cell1 = new IconTextCell(voicecallImg, getResString(R.string.usage_voice_call));
				cellList.add(cell1);
	
				ArrowTextCell cell2 = new ArrowTextCell(subTitle ,title, unitVoice, widthText2Col);
				cell2.setNoteSizeDelta(-4);		
				cell2.setContentSizeDelta(-4);
				cellList.add(cell2);
				ArrowTextCell cell3 = new ArrowTextCell(getResString(R.string.usage_normal), minNormal, null, widthText2Col, R.color.cell_bg_grey);
				cell3.setNoteSizeDelta(-4);		
				cell3.setContentSizeDelta(-4);
				cellList.add(cell3);
				ArrowTextCell cell4 = new ArrowTextCell(getResString(R.string.usage_intra_network),minIntra , null,widthText2Col,R.color.white);
				cell4.setNoteSizeDelta(-4);		
				cell4.setContentSizeDelta(-4);
				cellList.add(cell4);
			} else {
				String[] title = {};
				String[] unitVoice = {};
				String[] minIntra = {};
				String[] minNormal= {};
				
				/*Maseter will show one more col: group usage */
				title = isMaster ?	new String[] {getResString(R.string.usage_individual_entitled), getResString(R.string.usage_individual), getResString(R.string.usage_group)}
								  :	new String[] {getResString(R.string.usage_individual_entitled), getResString(R.string.usage_individual)};
				unitVoice = isMaster ? new String[] {"("+getResString(R.string.mins)+")", "("+getResString(R.string.mins)+")", "("+getResString(R.string.mins)+")" }
									  : new String[] {"("+getResString(R.string.mins)+")", "("+getResString(R.string.mins)+")" };
	
				String localVoiceIntraEntitlement = convertNum(g3UsageQuotaDTO.getLocalVoiceIntraEntitlement());
				String localVoiceInterEntitlement = convertNum(g3UsageQuotaDTO.getLocalVoiceInterEntitlement());
				if (localVoiceIntraEntitlement.equals(getResString(R.string.usage_unlimited)) || localVoiceInterEntitlement.equals(getResString(R.string.usage_unlimited))) {
					localVoiceIntraEntitlement = getResString(R.string.usage_unlimited);
					localVoiceInterEntitlement = getResString(R.string.usage_unlimited);
				}
				
				minIntra = isMaster ? new String[] {localVoiceIntraEntitlement,convertNum(g3UsageQuotaDTO.getLocalVoiceIntraUsage()), convertNum(g3UsageQuotaDTO.getLocalVoiceIntraUsageGrp())}	
									 : new String[] {localVoiceIntraEntitlement,convertNum(g3UsageQuotaDTO.getLocalVoiceIntraUsage())};
				minNormal = isMaster ? new String[] {localVoiceInterEntitlement,convertNum(g3UsageQuotaDTO.getLocalVoiceInterUsage()), convertNum(g3UsageQuotaDTO.getLocalVoiceInterUsageGrp())}
									  :	new String[] {localVoiceInterEntitlement,convertNum(g3UsageQuotaDTO.getLocalVoiceInterUsage())};
	
				int voicecallImg = Utils.theme(R.drawable.icon_voicecall, callback_usage.getLob()); //themed icon
				IconTextCell cell1 = new IconTextCell(voicecallImg, getResString(R.string.usage_voice_call));
				cellList.add(cell1);
	
				ArrowTextCell cell2 = new ArrowTextCell(subTitle ,title, unitVoice, isMaster? widthText3Col:widthText2Col);
				cell2.setNoteSizeDelta(-4);		
				cell2.setContentSizeDelta(-4);
				cellList.add(cell2);
				ArrowTextCell cell3 = new ArrowTextCell(getResString(R.string.usage_normal), minNormal, null, isMaster? widthText3Col:widthText2Col, R.color.cell_bg_grey);
				cell3.setNoteSizeDelta(-4);		
				cell3.setContentSizeDelta(-4);
				cellList.add(cell3);
				ArrowTextCell cell4 = new ArrowTextCell(getResString(R.string.usage_intra_network),minIntra , null,isMaster? widthText3Col:widthText2Col,R.color.white);
				cell4.setNoteSizeDelta(-4);		
				cell4.setContentSizeDelta(-4);
				cellList.add(cell4);
			}
		} else {
			//Roaming part
			if (serviceList !=null && serviceList.size() > 0) {
				for (int i=0;i < serviceList.size() ; i++){
					addCircleView(serviceList.get(i));				
				}
			} else {
				
				Cell cell;
				int mobusageImg = Utils.theme(R.drawable.icon_mobusage, callback_usage.getLob()); //themed icon
				cell = new Cell(Cell.ICONTEXT, mobusageImg, getResString(isLocal ? R.string.usage_mobile_data : R.string.usage_roam_mobile_data));
				cellList.add(cell);
				
				SmallTextCell noServiceRemark = new SmallTextCell(getResString(R.string.hsim_roaming_no_ser_remark), "");
				noServiceRemark.setRightPadding(basePadding);
				noServiceRemark.setLeftPadding(basePadding);
				noServiceRemark.setTitleSizeDelta(-2);
				cellList.add(noServiceRemark);
				
				/*CA: Moiz - remove live chat features
				//enquiry
				WebViewCell webViewCell = new WebViewCell(isZh? "file:///android_asset/roaming_enquiry_zh.html" : "file:///android_asset/roaming_enquiry_en.html");
				webViewCell.setTopMargin(extralinespace/3);
				cellList.add(webViewCell);
				SingleBtnCell singleBtnCell = new SingleBtnCell(getResString(R.string.MYHKT_BTN_LIVECHAT), col2Width * 2, new OnClickListener(){

					@Override
					public void onClick(View v) {
						((BaseActivity)me.getActivity()).openLiveChat();
					}			
				});	
				singleBtnCell.setBgcolorId(R.color.cell_bg_grey);
				singleBtnCell.setDraw(R.drawable.livechat_small);
				singleBtnCell.setLeftMargin(basePadding);
				singleBtnCell.setRightMargin(basePadding);
				singleBtnCell.setLeftPadding(basePadding);
				singleBtnCell.setRightPadding(basePadding);
				cellList.add(singleBtnCell); */
			}
						
			SmallTextCell remarkCell = new SmallTextCell(getResString(R.string.usage_reference_remark), "");
			remarkCell.setRightPadding(basePadding);
			remarkCell.setLeftPadding(basePadding);
			remarkCell.setTitleSizeDelta(-2);
			cellList.add(remarkCell);

		}

		cellViewAdapter.setView(frame, cellList);
	}

	protected void set1010UI(){
		aq.id(R.id.fragment_usagedata_layout).backgroundColorId(R.color.white);
		aq.marginpx(R.id.fragment_usagedata_layout, 0, 0, 0, 0);
		frame = (LinearLayout) aq.id(R.id.fragment_usagedata_listview).getView();

		//get service list processed by usagedatafragment		
		serviceList = isLocal ? callback.getLocalServiceList() : callback.getRommaServiceList();
		cellList = new ArrayList<Cell>();		
		//		IconTextCell iconTextCell = new IconTextCell(-1, "");
		//TODO
		//		if (isCSimPrim) {
		//			//TODO Local mobie data change to mobile data
		//			//						iconTextCell = new IconTextCell(R.drawable.mms_small, (isZh ? item.getTitleLevel1Chi() : item.getTitleLevel1Eng()) + "("+ getResString(R.string.csl_usage_total) +")");
		//			int mobusageImg = Utils.theme(R.drawable.icon_mobusage, callback_usage.getLob()); //themed icon
		//			iconTextCell = new IconTextCell(mobusageImg, getResString(R.string.csl_usage_mobile_data));
		//		} else {
		//			//			iconTextCell = new IconTextCell(R.drawable.mms_small, (isZh ? item.getTitleLevel1Chi() : item.getTitleLevel1Eng()) + "("+ callback_main.getAcctAgent().getSrvNum() +")");
		//			int mobusageImg = Utils.theme(R.drawable.icon_mobusage, callback_usage.getLob()); //themed icon
		//			iconTextCell = new IconTextCell(mobusageImg, getResString(isLocal ? R.string.usage_mobile_data : R.string.roam_mobile_data));
		//		}
		//		cellList.add(iconTextCell);

		if (serviceList !=null && serviceList.size() > 0) {
			addCircleView(serviceList.get(0));
		}
		cellViewAdapter.setView(frame, cellList);
	}


	public void refreshData(){
		super.refreshData();

		initUI();
	}

	public void refresh(){
		super.refresh();
		isLocal = callback_usage.getIsLocal();

		if (!Utils.getString(getActivity(), R.string.CONST_SCRN_MOBUSAGE).equals(callback_main.getTrackerId()) && callback_usage.getIsLocal()) {
			callback_main.setTrackerId(Utils.getString(getActivity(), R.string.CONST_SCRN_MOBUSAGE));
			//Screen Tracker
			if (debug) Log.i("GATRACKER", "GATRACKER" + Utils.getString(getActivity(), R.string.CONST_SCRN_MOBUSAGE));
		} else if (!Utils.getString(getActivity(), R.string.CONST_SCRN_MOBUSAGE_ROAMING).equals(callback_main.getTrackerId()) && !callback_usage.getIsLocal()){
				callback_main.setTrackerId(Utils.getString(getActivity(), R.string.CONST_SCRN_MOBUSAGE_ROAMING));
				//Screen Tracker
				if (debug) Log.i("GATRACKER", "GATRACKER" + Utils.getString(getActivity(), R.string.CONST_SCRN_MOBUSAGE_ROAMING));
		}
		initUI();
	}

	protected void cleanupUI(){

	}

	/**
	 * @param item
	 */
	private void addCircleView(final G3DisplayServiceItemDTO item) {
		//Title 
		String title = isZh ? item.getTitleLevel1Chi() : item.getTitleLevel1Eng();
		String subTitle = isZh ? item.getTitleLevel2Chi() : item.getTitleLevel2Eng();
		if (subTitle != null && !subTitle.equals("")) {
			if(callback_usage.getLob() != R.string.CONST_LOB_1010 && callback_usage.getLob() != R.string.CONST_LOB_O2F) { //Csim bypass level2 title
//				title = title + " (" + subTitle + ")";
			} else if (isCSimPrim) {
				title = title + " (" + getString(R.string.csl_usage_total) + ")";
			}
		}
		int mobusageImg = Utils.theme(R.drawable.icon_mobusage, callback_usage.getLob()); //themed icon
		IconTextCell titleCell = new IconTextCell(mobusageImg, title);
		titleCell.setLeftPadding(basePadding);
		titleCell.setRightPadding(basePadding);
		cellList.add(titleCell);

		//As of date 
		String asOfDate = isZh ? item.getAsOfDateChi() : item.getAsOfDateEng();
		if (asOfDate != null && !asOfDate.equals("")) {
			ArrowTextCell arrowTextCell = new ArrowTextCell(asOfDate, null, null, null);
			arrowTextCell.setRightPadding(basePadding);
			arrowTextCell.setLeftPadding(basePadding);
			arrowTextCell.setArrowShown(true);
			cellList.add(arrowTextCell);
		}

		//ProgressBar
		if ("Y".equalsIgnoreCase(item.getUsageBar().getShowBar())) {	
			String unit = ""; 
			
			//get usage remaining
			double remainingMB = 0;
			double entitlementMB = 0;
			String remaining = "---";
			String entitlement = "---";	
			Boolean isRemaining = false;

			//6-14-17-Moiz: Moved the getting of remaining MB and KB in the Utils class
			remaining = Utils.getRemainingDataUsage(item.getUsageDescription().getRemainingInMB(), item.getUsageDescription().getRemainingInKB());
			if(!remaining.equals("---")) {
				isRemaining = true;
			}
			
			
//			if (item.getUsageDescription().getInitialInMB() != null && !"".equals(item.getUsageDescription().getInitialInMB())) {
//				entitlementMB = Double.parseDouble(item.getUsageDescription().getInitialInMB().replace(",",""));
//				entitlement = Utils.mbToGb(entitlementMB, false);					
//			}

			//			CircleViewCell circlecell = new CircleViewCell(isZh?  item.getUsageDescription().getTextChi() : item.getUsageDescription().getTextEng() ,"3.67GB", item.getUsageBar().getBarPercent());			
			CircleViewCell circlecell;
			if (isRemaining) {
				circlecell = new CircleViewCell(isZh?  item.getUsageBar().getBarPercent() + "%" : item.getUsageBar().getBarPercent() + "%", remaining, item.getUsageBar().getBarPercent());
			} else {
				circlecell = new CircleViewCell(isZh?  item.getUsageBar().getBarPercent() + "%" : item.getUsageBar().getBarPercent() + "%", "", item.getUsageBar().getBarPercent());
			}

			circlecell.setLeftPadding(extralinespace*4);
			circlecell.setRightPadding(extralinespace*4);

			//set bar color
			if (item.getUsageBar().getBarPercent() < 10) {
				circlecell.setBarColor(R.color.cprogressbar_red);
				circlecell.setTitleColorId(R.color.cprogressbar_red);
			} else if (item.getUsageBar().getBarPercent() >= 10 && item.getUsageBar().getBarPercent() <= 20) {
				circlecell.setBarColor(R.color.cprogressbar_yellow);
				circlecell.setTitleColorId(R.color.cprogressbar_yellow);
			}

			cellList.add(circlecell);
			
			if (!isRemaining) {
				SmallTextCell smallTextCell = new SmallTextCell(isZh? item.getUsageDescription().getTextChi() : item.getUsageDescription().getTextEng() , "");
				smallTextCell.setTitleGravity(Gravity.CENTER);
				smallTextCell.setBgcolorId(R.color.white);
				cellList.add(smallTextCell);
			}
			
		} else {
			SmallTextCell smallTextCell = new SmallTextCell(isZh? item.getUsageDescription().getTextChi() : item.getUsageDescription().getTextEng() , "");
			smallTextCell.setBgcolorId(R.color.cell_bg_grey);
			cellList.add(smallTextCell);
		}
		//Topup btn
		//CSim always shows, HSim shows where allowTopup is Y 
		if ("Y".equalsIgnoreCase(item.getAllowTopup()) || !isMob) {
			OnClickListener onClickListener = new OnClickListener(){
				@Override
				public void onClick(View v) {
					callback_usage.setSelectedServiceItem(item);		
					callback_usage.setActiveChildview(R.string.CONST_SELECTEDFRAG_TOPUP1);
					callback_usage.displayChildview(false);
				}			
			};
			String btnName = getResString(R.string.usage_boost_up1);
			SingleBtnCell singleBtnCell = new SingleBtnCell(btnName, btnWidth, onClickListener);
			int topupImg = Utils.theme(R.drawable.icon_topup, callback_usage.getLob()); //themed icon
			singleBtnCell.setDraw(topupImg);
			cellList.add(singleBtnCell);		
		}
	}
	
	/**
	 * @param item
	 */
	private void addCircleView_MUP(final G3DisplayServiceItemDTO item, G3DisplayServiceItemDTO mupItem, String masterMrt) {
		
		//Title 
		String title = isZh ? item.getTitleLevel1Chi() : item.getTitleLevel1Eng();
		title = title + getResString(R.string.usage_mobile_data_group);
		int mobusageImg = Utils.theme(R.drawable.icon_mobusage, callback_usage.getLob()); //themed icon
		IconTextCell titleCell = new IconTextCell(mobusageImg, title);
		titleCell.setLeftPadding(basePadding);
		titleCell.setRightPadding(basePadding);
		cellList.add(titleCell);
		
		IconTextCell corrPrimCell = new IconTextCell(R.drawable.icon_mobusage_inv, String.format(getResString(R.string.usage_mobile_data_group_sub),masterMrt));
		corrPrimCell.setLeftPadding(basePadding);
		corrPrimCell.setRightPadding(basePadding);	
		corrPrimCell.setBotPadding(basePadding);	
		corrPrimCell.setCellHeight(LayoutParams.WRAP_CONTENT);
		corrPrimCell.setTitleSizeDelta(-2);
		cellList.add(corrPrimCell);

		//As of date 
		String asOfDate = isZh ? item.getAsOfDateChi() : item.getAsOfDateEng();
		if (asOfDate != null && !asOfDate.equals("")) {
			ArrowTextCell arrowTextCell = new ArrowTextCell(asOfDate, null, null, null);
			arrowTextCell.setRightPadding(basePadding);
			arrowTextCell.setLeftPadding(basePadding);
			arrowTextCell.setArrowShown(true);
			cellList.add(arrowTextCell);
		}

		//ProgressBar
		if ("Y".equalsIgnoreCase(item.getUsageBar().getShowBar())) {	
			String unit = ""; 
			
			//get usage remaining
			double remainingMB = 0;
			double entitlementMB = 0;
			String remaining = "---";
			String entitlement = "---";
			String individual = "---";
			Boolean isRemaining = false;
			
			//6-14-17-Moiz: Moved the getting of remaining MB and KB in the Utils class
			remaining = Utils.getRemainingDataUsage(item.getUsageDescription().getRemainingInMB(), item.getUsageDescription().getRemainingInKB());
			if(!remaining.equals("---")) {
				isRemaining = true;
			}
			
			if (item.getUsageDescription().getInitialInMB() != null && !"".equals(item.getUsageDescription().getInitialInMB())) {
//				entitlementMB = Double.parseDouble(item.getUsageDescription().getInitialInMB().replace(",",""));
//				entitlement = Utils.mbToGb(entitlementMB, false);
				entitlement = item.getUsageDescription().getInitialInMB() + "MB";
			}
						
			if (mupItem !=null && mupItem.getUsageDescription().getConsumedInMB() != null && !"".equals(mupItem.getUsageDescription().getConsumedInMB())) {
				individual = mupItem.getUsageDescription().getConsumedInMB() + "MB";
			}			

			//			CircleViewCell circlecell = new CircleViewCell(isZh?  item.getUsageDescription().getTextChi() : item.getUsageDescription().getTextEng() ,"3.67GB", item.getUsageBar().getBarPercent());			
			CircleViewCell circlecell;
			if (isRemaining) {
				circlecell = new CircleViewCell(isZh?  item.getUsageBar().getBarPercent() + "%" : item.getUsageBar().getBarPercent() + "%", remaining, item.getUsageBar().getBarPercent());
			} else {
				circlecell = new CircleViewCell(isZh?  item.getUsageBar().getBarPercent() + "%" : item.getUsageBar().getBarPercent() + "%", "", item.getUsageBar().getBarPercent());
			}

			circlecell.setLeftPadding(extralinespace*4);
			circlecell.setRightPadding(extralinespace*4);

			//set bar color
			if (item.getUsageBar().getBarPercent() < 10) {
				circlecell.setBarColor(R.color.cprogressbar_red);
				circlecell.setTitleColorId(R.color.cprogressbar_red);
			} else if (item.getUsageBar().getBarPercent() >= 10 && item.getUsageBar().getBarPercent() <= 20) {
				circlecell.setBarColor(R.color.cprogressbar_yellow);
				circlecell.setTitleColorId(R.color.cprogressbar_yellow);
			}

			cellList.add(circlecell);
			
			if (!isRemaining) {
				SmallTextCell smallTextCell = new SmallTextCell(isZh? item.getUsageDescription().getTextChi() : item.getUsageDescription().getTextEng() , "");
				smallTextCell.setTitleGravity(Gravity.CENTER);
				smallTextCell.setBgcolorId(R.color.white);
				cellList.add(smallTextCell);
			}
			
			if (isMaster) {
				SmallTextCell dataDesCell = new SmallTextCell(String.format(getResString(R.string.usage_mobile_data_des_master), entitlement, individual) , "");
				dataDesCell.setTitleGravity(Gravity.CENTER);
				dataDesCell.setTitleSizeDelta(-2);
				cellList.add(dataDesCell);
			} else {
				SmallTextCell dataDesCell = new SmallTextCell(String.format(getResString(R.string.usage_mobile_data_des_slave), individual) , "");
				dataDesCell.setTitleGravity(Gravity.CENTER);
				dataDesCell.setTitleSizeDelta(-2);
				cellList.add(dataDesCell);
			}			
			
		} else {
			SmallTextCell smallTextCell = new SmallTextCell(isZh? item.getUsageDescription().getTextChi() : item.getUsageDescription().getTextEng() , "");
			cellList.add(smallTextCell);
		}
		
		//Topup btn
		//CSim always shows, HSim shows where allowTopup is Y 
		if ("Y".equalsIgnoreCase(item.getAllowTopup()) || !isMob) {
			OnClickListener onClickListener = new OnClickListener(){
				@Override
				public void onClick(View v) {
					callback_usage.setSelectedServiceItem(item);		
					callback_usage.setActiveChildview(R.string.CONST_SELECTEDFRAG_TOPUP1);
					callback_usage.displayChildview(false);
				}			
			};
			String btnName = getResString(R.string.usage_boost_up1);
			SingleBtnCell singleBtnCell = new SingleBtnCell(btnName, btnWidth, onClickListener);
			int topupImg = Utils.theme(R.drawable.icon_topup, callback_usage.getLob()); //themed icon
			singleBtnCell.setDraw(topupImg);
			cellList.add(singleBtnCell);		
		}
	}

	private String convertNum(String num) {
		if (num !=null) num = num.trim();
		if (num != null && num.length() != 0){
			if (num.equalsIgnoreCase("UNLIMIT")) {
				return getResString(R.string.usage_unlimited);
			} else {
				return Utils.formatNumber(Utils.truncateStringDot(num)); 
			}
		} else {
			return "-";
		}
	}

	@Override
	public void onSuccess(APIsResponse response) {
	}

	@Override
	public void onFail(APIsResponse response) {
	}

}
