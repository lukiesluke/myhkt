package com.pccw.myhkt.lib.ui;

import java.util.List;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;
import androidx.viewpager.widget.ViewPager;
import androidx.viewpager.widget.ViewPager.OnPageChangeListener;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.RelativeLayout;

import com.androidquery.AQuery;
import com.pccw.myhkt.R;
import com.pccw.myhkt.adapter.TabBarAdapter;
import com.pccw.myhkt.model.TabItem;

public class HKTIndicator extends RelativeLayout implements HKTPageIndicator{

	public HKTIndicator(Context context) {
		super(context);
		LayoutInflater.from(context).inflate(R.layout.hkt_indicator, this);
		// TODO Auto-generated constructor stub
	}
	public HKTIndicator(Context context, AttributeSet attrs) {
		super(context, attrs);
		LayoutInflater.from(context).inflate(R.layout.hkt_indicator, this);
		// TODO Auto-generated constructor stub
	}
	public HKTIndicator(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		LayoutInflater.from(context).inflate(R.layout.hkt_indicator, this);
		// TODO Auto-generated constructor stub
	}

	private GridView gridView;
	private	AQuery aq;
	private List<TabItem> mTapItemList;
	private TabBarAdapter testAdapter;
	private ViewPager mViewPager;
	private int 	  gridViewPos = 0;
	private int 	  pageNum = 0;
	private int 	  tapPos = 0;
	public  Boolean   smoothScroll = true;

	private com.pccw.myhkt.lib.ui.IndicatorBar indicatorBar;
	public void initView(final Context context, List<TabItem> tapItemList, int textSize, int textColor ,int bgcolorRes, int height) {
		LayoutInflater.from(context).inflate(R.layout.hkt_indicator, null, false);
		this.mTapItemList = tapItemList;
		aq = new AQuery(this);
		gridView = aq.id(R.id.hkt_indicator_gridview1).getGridView();
		gridView.setNumColumns(mTapItemList.size());

		testAdapter = new TabBarAdapter(context, mTapItemList, height, textColor, textSize);
		aq.id(R.id.hkt_indicator_gridview1).itemClicked(this, "onHomeBtnClick");
		gridView.setAdapter(testAdapter);
		gridView.setVerticalSpacing((int) context.getResources().getDimension(R.dimen.indicator_tab_spac));
//		gridView.setHorizontalSpacing((int) context.getResources().getDimension(R.dimen.indicator_tab_spac));
		gridView.setHorizontalSpacing(0);
		indicatorBar = (IndicatorBar) aq.id(R.id.hkt_indicator_bar).getView();
		indicatorBar.initPos(0, tapItemList.size());	
		indicatorBar.setVisibility(View.GONE);
	}

	public void setInitPos(int pos) {
		indicatorBar.initPos(pos, mTapItemList.size());	
	}

	public void onHomeBtnClick(AdapterView<?> parent, View v, int position, long id) {
		tapPos = position;
		TabItem tabItem = mTapItemList.get(position);
		if (tabItem.text !=null) {
			pageNum = tabItem.pageNum;
			setCurrentItem(tabItem.pageNum);
			((TabBarAdapter)gridView.getAdapter()).setCurrentPos(tabItem.pageNum);
		}
	}

	public void setViewPager(ViewPager view) {
		if (mViewPager == view) {
			return;
		}
		if (mViewPager != null) {
			mViewPager.setOnPageChangeListener(null);
		}
		if (view.getAdapter() == null) {
			throw new IllegalStateException("ViewPager does not have adapter instance.");
		}
		mViewPager = view;
		mViewPager.setOnPageChangeListener(this);
		invalidate();
	}

	private int mCurrentPage = -1;
	ViewPager.OnPageChangeListener mListener;
	private int mScrollState;
	private float mPositionOffset;


	@Override
	public void setViewPager(ViewPager view, int initialPosition) {
		setViewPager(view);
		setCurrentItem(initialPosition);		
	}
	@Override
	public void setCurrentItem(int item) {
		if (mViewPager == null) {
			throw new IllegalStateException("ViewPager has not been bound.");
		}
		mViewPager.setCurrentItem(item, smoothScroll);
		mCurrentPage = item;
		invalidate();
	}


	@Override
	public void onPageScrollStateChanged(int arg0) {
//		mScrollState = arg0;
//		if (arg0 ==0) {
//			tapPos = 0;
//			for (TabItem tabItem : mTapItemList) {
//				for (int i = 0; i <mTapItemList.size(); i++) {
//					if (mViewPager.getCurrentItem() >= mTapItemList.get(i).pageNum) {
//						tapPos = mTapItemList.get(i).pageNum;
//						break;
//					}
//				}
//				indicatorPos = (float) (indicatorWidth / mTapItemList.size() * (tapPos + 0.5));
//				indicatorBar.updatePos(indicatorPos);
//				Log.d("MOIZ", "setCurrentPos: 136 - " + tapPos);
//				((TabBarAdapter)gridView.getAdapter()).setCurrentPos(tapPos);
//			}
//			if (mListener != null) {
//				mListener.onPageScrollStateChanged(arg0);
//			}
//
//		}
	}



	float oldPos = 0;
	float indicatorWidth;
	float indicatorPos ;
	@Override
	public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels)  {
		//		int oldPos = mViewPager.getCurrentItem();
		indicatorWidth = indicatorBar.getWidth();
		//		if (!isClickScrolling) {
		mCurrentPage = position;
		mPositionOffset = positionOffset;
		//Log.i("MOIZ ", "Move " +position +"/" +positionOffset +"/" + oldPos +"/" + (positionOffset > oldPos) + (positionOffset < oldPos));

		indicatorPos = 0;
		if (positionOffset == 0.0f) {
			tapPos = 0;	
			for (TabItem tabItem : mTapItemList) {
				for (int i = 0; i <mTapItemList.size(); i++) {
					if (position >= mTapItemList.get(i).pageNum) {
						tapPos = mTapItemList.get(i).pageNum;
					}							
				} 
				indicatorPos = (float) (indicatorWidth / mTapItemList.size() * (tapPos + 0.5));
				indicatorBar.updatePos(indicatorPos);
				((TabBarAdapter)gridView.getAdapter()).setCurrentPos(tapPos);
			}
		} else if( positionOffset > oldPos) {
			//Moving to the right , Page number increase
			if (tapPos < mTapItemList.size() -1 &&position == mTapItemList.get(tapPos+1).pageNum - 1)  {
				indicatorPos = (float) (indicatorWidth / mTapItemList.size() * (tapPos + positionOffset + 0.5));
				//				Log.i("Andy1 ", "scl" + indicatorPos +"/ " +positionOffset);
				indicatorBar.updatePos(indicatorPos);
			} if (tapPos !=0 && position == mTapItemList.get(tapPos).pageNum - 1)  {
				//Moving back case when swip less than half page
				indicatorPos = (float) (indicatorWidth / mTapItemList.size() * (tapPos + (positionOffset -1) + 0.5));
				//				Log.i("Andy1 ", "scl" + indicatorPos +"/ " +positionOffset);
				indicatorBar.updatePos(indicatorPos);
			}



		} else if(positionOffset < oldPos) {
			//Moving to the Left ,Page number decrease	
			if (tapPos !=0 && position == mTapItemList.get(tapPos).pageNum - 1)  {
				indicatorPos = (float) (indicatorWidth / mTapItemList.size() * (tapPos + (positionOffset -1) + 0.5));
				indicatorBar.updatePos(indicatorPos);
			} else if (tapPos < mTapItemList.size() -1)  {
				//Moving back case when swip less than half page
				if (position == mTapItemList.get(tapPos+1).pageNum - 1)  {
					indicatorPos = (float) (indicatorWidth / mTapItemList.size() * (tapPos + positionOffset + 0.5));
					indicatorBar.updatePos(indicatorPos);
				}
			}
		}		


		//		Log.i("Andy", "Indicator Pos = " + tapPos);
		invalidate();

		if (mListener != null) {
			mListener.onPageScrolled(position, positionOffset, positionOffsetPixels);
		}
		oldPos = positionOffset;
		//		}

	}
	@Override
	public void onPageSelected(int position) {
		if (mScrollState == ViewPager.SCROLL_STATE_IDLE) {
			mCurrentPage = position;
			mPositionOffset = 0;
			mViewPager.setFocusable(false);
			invalidate();
		}
		if (mListener != null) {
			mListener.onPageSelected(position);
		}		
	}


	@Override
	public void setOnPageChangeListener(OnPageChangeListener listener) {
		mListener = listener;		
	}
	@Override
	public void notifyDataSetChanged() {
		invalidate();		
	}

	@Override
	public void onRestoreInstanceState(Parcelable state) {
		SavedState savedState = (SavedState)state;
		super.onRestoreInstanceState(savedState.getSuperState());
		mCurrentPage = savedState.currentPage;
		requestLayout();
	}

	@Override
	public Parcelable onSaveInstanceState() {
		Parcelable superState = super.onSaveInstanceState();
		SavedState savedState = new SavedState(superState);
		savedState.currentPage = mCurrentPage;
		return savedState;
	}

	static class SavedState extends BaseSavedState {
		int currentPage;

		public SavedState(Parcelable superState) {
			super(superState);
		}

		private SavedState(Parcel in) {
			super(in);
			currentPage = in.readInt();
		}

		@Override
		public void writeToParcel(Parcel dest, int flags) {
			super.writeToParcel(dest, flags);
			dest.writeInt(currentPage);
		}

		@SuppressWarnings("UnusedDeclaration")
		public static final Creator<SavedState> CREATOR = new Creator<SavedState>() {
			@Override
			public SavedState createFromParcel(Parcel in) {
				return new SavedState(in);
			}

			@Override
			public SavedState[] newArray(int size) {
				return new SavedState[size];
			}
		};
	}

}
