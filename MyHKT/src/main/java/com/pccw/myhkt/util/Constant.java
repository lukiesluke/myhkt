package com.pccw.myhkt.util;

/**
 * Created by AMoiz Esmail on 02/11/2017.
 */

public class Constant {
    //Format date
    public static final String FORMAT_YYYYMMDD = "yyyyMMdd";
    public static final String FORMAT_YYYY_MM_DD = "yyyy-MM-dd";
    public static final String FORMAT_DD_MMM_YYYY = "dd-MMM-yyyy";
    public static final String FORMAT_DDMMYYYY = "dd/MM/yyyy";
    public static final String FORMAT_MMM_YYYY = "MMM-yyyy";
    public static final String FORMAT_TIME_STAMP = "yyyy-MM-dd hh:mm:ss.S";
    public static final String FORMAT_MODIFIED_DATE = "EEE, dd MMM yyyy HH:mm:ss zzz";

    //package names
    public static final String HKT_SHOP_PACKAGE_NAME = "com.hkt.android.hktshop";
    public static final String TAP_AND_GO_PACKAGE_NAME = "com.hktpayment.tapngo";
    public static final String THE_CLUB_PACKAGE_NAME = "com.pccw.clublike";
    public static final String MOX_BANK_PACKAGE_NAME = "com.mox.app";
    public static final String AR_LENS_PACKAGE_NAME = "com.hkcsl.csl5glens";
    public static final String DR_GO_PACKAGE_NAME = "com.hkt.nightingale";

    //URL Deeplink
    public static final String URL_DEEP_LINK_GOOGLE = "http://play.google.com/store/apps/details?id=%s";
    public static final String URL_DEEP_LINK_AR_LENS = "https://applink.arlensapp.com/csllens/open?screen=start";
    public static final String URL_DEEP_LINK_MARKET = "market://details?id=%s";
    public static final String URL_DEEP_LINK_CLUB = "https://www.clublike.com.hk/shop/SmartLiving";

    public static final int REQUEST_TOUCH_ID_WELCOME_PAGE = 1111;
    public static final int REQUEST_TOUCH_ID_ACTIVATION_PAGE = 1112;
    public static final int REQUEST_TOUCH_ID_COMPLETE_PAGE = 1113;
    public static final int REQUEST_TOUCH_ID_VERIFICATION_FAILED = 1114;
    public static final int REQUEST_FILE_STORAGE_PERMISSION = 1115;
    public static final int REQUEST_CALL_PHONE_PERMISSION = 1116;
    public static final int REQUEST_LOCATION_PERMISSION = 1117;
    public static final int REQUEST_LOCATION_PERMISSION_ENABLED = 1118;
    public static final int REQUEST_LOCATION_PERMISSION_DISABLED = 1119;
    public static final int REQUEST_LOG_FILE_STORAGE_PERMISSION = 1120;
    public static final int REQUEST_ACCOUNT_DELETE = 8888;

    public static final String DEFAULT_KEY_NAME = "default_key";
    public static final String KEY_NAME_LOB_TYPE = "KEY_NAME_LOB_TYPE";

    public static final String TOUCH_ID_FAILURE_COUNT = "com.pccw.touch_id.count";
    public static final String TOUCH_ID_RETRY_COUNT = "com.pccw.touch_id.retry.count"; //for every 3 failed touch id is equivalent to 1 retry

    public static final String IS_TO_SHOW_LIVE_CHAT = "com.pccw.is_to_show_live_chat";

    public static final String IS_SERVER_TO_SHOW_FP_BANNER = "com.pccw.is_fp_banner_show"; //Y/N

    public static final String TOUCH_ID_LOGIN_ACTIVATED = "com.pccw.touch_id.activated"; //default value is true
    public static final String TOUCH_ID_SETTING_ENABLED = "com.pccw.touch_id.setting.enabled"; //default value is false
    public static final String TOUCH_ID_PWD_DEFAULT = "con.pccw.touch_id.default.password";
    public static final String TOUCH_ID_USER_ID_DEFAULT = "con.pccw.touch_id.default.user_id";
    public static final String TOUCH_ID_TEMP_PWD = "con.pccw.touch_id.default.temp.password";
    public static final String TOUCH_ID_TEMP_USER_ID = "con.pccw.touch_id.default.temp.user_id";
    public static final String TOUCH_ID_WELCOME_TOUCH_ID_COUNTER = "com.pccw.com.touch_id.close.counter.welcome_page";
    public static final String TOUCH_ID_LOGIN_WELCOME_PARENT = "com.pccw.login_activity_parent";


    public static final String ACTION_STORAGE_REQUEST_GRANTED = "com.pccw.com.storage.request_granted";
    public static final String ACTION_LOCATION_REQUEST_GRANTED = "com.pccw.com.location.request_granted";
    public static final String FILE_STORAGE_PERMISSION_DENIED = "com.pccw.com.permission_denied";
    public static final String CALL_PHONE_PERMISSION_DENIED = "com.pccw.com.call_phone.permission_denied";
    public static final String LOCATION_PERMISSION_DENIED = "com.pccw.com.location.permission_denied";

    public static final String ACTION_BROADCAST_IMAGE_FINISH_DOWNLOAD = "com.pccw.com.image_banner_finish_download";
    public static final String ACTION_BROADCAST_CONFIG_FINISH_DOWNLOAD = "com.pccw.com.app_config_finish_download";

    public static final String FILENAME_APP_CONFIG = "app_config.json";
    public static final String KEY_SHARED_APP_CONFIG = "app_config_json";

    public static final String MY_MOBILE_PARENT_ACTIVITY = "com.pccw.com.my_mobile";
    public static final String MY_MOBILE_LOB_STRING = "com.pccw.com.my_mobile.LOB";
    public static final String MY_MOBILE_IS_1010 = "com.pccw.com.my_mobile.is1010";

    public static final String BLOCK_VERSION = "com.pccw.block_version.app";
    public static final String ACTION_CHECK_APP_VERSION = "com.pccw.com.call_api_version_checker";
    public static final String ACTION_APP_VERSION_IS_UPDATED = "com.pccw.com.app.is.updated";
    public static final String ACTION_SESSION_TIMEOUT = "com.pccw.com.app.session.timeout";

    public static final String REGION = "com.pccw.region";

    //My Inbox (Messages) keys
    public static final String INBOX_BUNDLE_CONTENT = "com.pccw.com.email_content";
    public static final String CONST_INBOX_CRA_RESPONSE = "com.pccw.com.inbox_cra_response";
    public static final String ACTION_INBOX_LIST_UPDATED = "com.pccw.com.inbox_is_updated";


    //Broadcast Receivers Action
    public static final String ACTION_LINE_TEST_SUCCESS = "com.pccw.com.action.line_test_success";


    //Broadcast Cached Appointment Result
    public static final String ACTION_CACHED_APPT_RESPONSE = "com.pccw.com.action.cachedappt.success";
    public static final String CONST_PREF_APPTIND_LAST_TS = "apptind_last_ts";
    public static final String CONST_PREF_APPTIND_FLAG = "apptind_flag";
    public static final String CONST_APPT_POPUP_SHOWN_FLAG = "appt_popup_shown_flag";
    public static final String CONST_IS_BADGE_SHOWN = "is_badge_shown";
    public static final String CONST_SHOW_SESS_TIMEOUT = "is_sess_timeout";


    public static final String CONST_IDLE_START_TIME = "idle_start_time";

    public static final String SCREEN_LARGE = "LARGE";
    public static final String SCREEN_NORMAL = "NORMAL";
    public static final String SCREEN_SMALL = "SMALL";
    public static final String SCREEN_UNDEFINED = "UNDEFINED";
    public static final String PersonalInformationCollectionStatement = "Personal Information Collection Statement";

    //Line Test start page - links
    public static final String URL_PROD_CHECK_PC_SETTING = "https://www.appointment.hkt.com/qrcode/sr1.html"; //uat: "https://uat.appointment.hkt.com/qrcode/sr2.html"
    public static final String URL_PROD_CHECK_EQ_CONNECTION = "https://www.appointment.hkt.com/qrcode/sr2.html"; //uat: "https://uat.appointment.hkt.com/qrcode/sr2.html"
    public static final String URL_PROD_CHECK_WIFI_TIPS = "https://www.appointment.hkt.com/qrcode/sr3.html"; //uat: "https://uat.appointment.hkt.com/qrcode/sr3.html"

    //Technical Account keys
    public static final String KEY_IS_MY_LINE_TEST = "com.pccw.myhkt_mylinetest";


    // Notification Timers
    public static final int REMOVE_BADGE_TIMER = 60000;

    //e_wallet
    public static final String URL_E_WALLET_CN_UAT = "https://uatb-coupon-e-wallet-frontend.herokuapp.com/myewallet?lang=zh-HK&timestamp={timestamp}";
    public static final String URL_E_WALLET_EN_UAT = "https://uatb-coupon-e-wallet-frontend.herokuapp.com/myewallet?lang=en&timestamp={timestamp}";
    public static final String URL_E_WALLET_CN_PRD = "https://coupon.hkt.com/myewallet?lang=zh-HK&timestamp={timestamp}";
    public static final String URL_E_WALLET_EN_PRD = "https://coupon.hkt.com/myewallet?lang=en&timestamp={timestamp}";
    public static final String WALLET_TIMESTAMP = "{timestamp}";

    //Browser toolbar title
    public static final String PRIVACY_STATEMENT = "Privacy Statement";
    public static final String PICS = "Personal Information Collection Statement";
    public static final String GENERAL_CONDITION = "General Conditions of “My HKT” Portal";
    public static final String TERMS_CONDITION = "Terms and Conditions";

    //Firebase
    public static final String USER_TYPE_UAT = "UAT";
    public static final String USER_TYPE_PRD = "PRD";
    public static final String USER_TYPE_OS = "Android";
}
