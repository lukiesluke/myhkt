package com.pccw.dango.shared.g3entity;

import java.io.Serializable;


public class G3RechargeHistDTO implements Serializable 
{
    private static final long serialVersionUID = -2395357077770325711L;
    
    private String transactionDateTime;
    private String transactionDate;
    private String quotaName;
    private String boostUpOfferId;
    private String engDescription;
    private String rechargeValue;
    private String rechargeValueInMB;
    private String rechargeAmount;
    private String lastUpdBy;
    private String lastUpdSC;
    
    public String getTransactionDateTime() {
        return transactionDateTime;
    }
    public void setTransactionDateTime(String transactionDateTime) {
        this.transactionDateTime = transactionDateTime;
    }
    public String getQuotaName() {
        return quotaName;
    }
    public void setQuotaName(String quotaName) {
        this.quotaName = quotaName;
    }
    public String getBoostUpOfferId() {
        return boostUpOfferId;
    }
    public void setBoostUpOfferId(String boostUpOfferId) {
        this.boostUpOfferId = boostUpOfferId;
    }
    public String getEngDescription() {
        return engDescription;
    }
    public void setEngDescription(String engDescription) {
        this.engDescription = engDescription;
    }
    public String getRechargeValue() {
        return rechargeValue;
    }
    public void setRechargeValue(String rechargeValue) {
        this.rechargeValue = rechargeValue;
    }
    public String getRechargeAmount() {
        return rechargeAmount;
    }
    public void setRechargeAmount(String rechargeAmount) {
        this.rechargeAmount = rechargeAmount;
    }
    public String getLastUpdBy() {
        return lastUpdBy;
    }
    public void setLastUpdBy(String lastUpdBy) {
        this.lastUpdBy = lastUpdBy;
    }
    public String getLastUpdSC() {
        return lastUpdSC;
    }
    public void setLastUpdSC(String lastUpdSC) {
        this.lastUpdSC = lastUpdSC;
    }
    public String getTransactionDate() {
        return transactionDate;
    }
    public void setTransactionDate(String transactionDate) {
        this.transactionDate = transactionDate;
    }
    public String getRechargeValueInMB() {
        return rechargeValueInMB;
    }
    public void setRechargeValueInMB(String rechargeValueInMB) {
        this.rechargeValueInMB = rechargeValueInMB;
    }

}
