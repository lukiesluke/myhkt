/*
    Crate for Smart Phone Session
    
    Keywords
    --------
    $URL: $
    $Rev: $
    $Date: $
    $Author: $
*/

package com.pccw.dango.shared.cra;

import java.io.Serializable;

import com.pccw.dango.shared.entity.SpssRec;

public class SpssCra extends BaseCraEx implements Serializable
{
    private static final long serialVersionUID = -3050685656674676902L;
    
    private String                  iLoginId;           /* Login ID                                      */
    private String                  iCkSum;             /* Checksum                                      */
    private SpssRec                 iSpssRec;           /* SmartPhone Session Record                     */


    public static void main(String rArg[])
    {
        System.out.println(getVer());
        return;
    }
    
    
    public static String getVer()
    {
        return ("$URL: $, $Rev: $");
    }

    
    public SpssCra()
    {
        initAndClear();
    }


    protected void init()
    {
        super.init();
    }


    public void clear()
    {
        super.clear();
        
        clearILoginId();
        clearICkSum();
        clearISpssRec();
    }


    public SpssCra copyFrom(SpssCra rSrc)
    {
        super.copyFrom(rSrc);
        
        setILoginId(rSrc.getILoginId());
        setICkSum(rSrc.getICkSum());
        setISpssRec(rSrc.getISpssRec());

        return (this);
    }


    public SpssCra copyTo(SpssCra rDes)
    {
        rDes.copyFrom(this);
        return (rDes);
    }
    
    
    public SpssCra copyMe()
    {
        SpssCra                     rDes;

        rDes = new SpssCra();
        rDes.copyFrom(this);
        return (rDes);
    }


    public void clearILoginId()
    {
        iLoginId = "";
    }


    public void setILoginId(String rArg)
    {
        iLoginId = rArg;
    }


    public String getILoginId()
    {
        return (iLoginId);
    }
    
    
    public void clearICkSum()
    {
        iCkSum = "";
    }


    public void setICkSum(String rArg)
    {
        iCkSum = rArg;
    }


    public String getICkSum()
    {
        return (iCkSum);
    }


    public void clearISpssRec()
    {
        iSpssRec = new SpssRec();
    }


    public void setISpssRec(SpssRec rArg)
    {
        iSpssRec = rArg;
    }


    public SpssRec getISpssRec()
    {
        return (iSpssRec);
    }
}
