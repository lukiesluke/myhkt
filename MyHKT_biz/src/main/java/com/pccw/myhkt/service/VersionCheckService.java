package com.pccw.myhkt.service;

/************************************************************************
File       : LineTestIntentService.java
Desc       : Background Service For LineTest
Name       : LineTestIntentService
Created by : Vincent Fung
Date       : 12/02/2014

Change History:
Date       Modified By			Description
---------- ----------------	-------------------------------
12/02/2014  Vincent Fung		- First draft 
13/02/2014  Vincent Fung		1.) Created IntentService for background lineTest
								2.) Allowed to Normal LineTest and Reset Modem
								3.) Added Notification to foreground or push Notification to mobile device
24/02/2014 Vincent Fung			- Code cleanup
 *************************************************************************/

import android.app.IntentService;
import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

import com.pccw.biz.myhkt.R;
import com.pccw.myhkt.APIsManager;
import com.pccw.myhkt.APIsManager.OnAPIsListener;
import com.pccw.myhkt.APIsResponse;
import com.pccw.myhkt.ClnEnv;
import com.pccw.myhkt.GlobalDialog;
import com.pccw.myhkt.model.LnttAgent;
import com.pccw.myhkt.model.VersionCheck;

import java.util.Timer;

public class VersionCheckService extends IntentService implements OnAPIsListener {
	private boolean										debug					= false;

	private static VersionCheckService				me;
	public static int									lnttRtryCnt;
	private Timer										timer					= new Timer();
	private static LnttAgent							lnttAgent				= null;

	private String										errorMsg				= "";
	private String 		TAG = "VersionCheckService";
	private String				clientVersion	= "";
	private boolean		requiredUpdate = false;
	private boolean 	forcedUpdate = false;

	private boolean		debugCra = false;

	public VersionCheckService() {
		super("VersionCheckService");
	}

	@Override
	public void onCreate() {
		// The service is being created
		debug = getResources().getBoolean(R.bool.DEBUG);
		me = this;	}

	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}

	@Override
	protected void onHandleIntent(Intent intent) {
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		try {
			clientVersion = me.getApplicationContext().getPackageManager().getPackageInfo(me.getApplicationContext().getPackageName(), 0).versionName;
		} catch (Exception e) {
			e.printStackTrace();
		}
		APIsManager.doCheckUpdate(this);
		return Service.START_REDELIVER_INTENT;
	}

	@Override
	public void onSuccess(APIsResponse response) {
		VersionCheck versionCheck;
		if (response != null) {
			versionCheck = (VersionCheck) response.getCra();
			// Checks blockedVersion
			String[] blockedVersion = versionCheck.getBlockedVersion();

			for (String version : blockedVersion) {
				if (version.equalsIgnoreCase(clientVersion)) {
					if (debugCra) ClnEnv.toLogFile(String.format("%s %s", this.getClass().getSimpleName(), "version check result"), "blocked version");

					requiredUpdate = true;
					forcedUpdate = true;
					returnResults(requiredUpdate, forcedUpdate);
					return;
				}
			}

			// forced check?
			if (!versionCheck.getForcedCheck()) {
				if (debugCra) ClnEnv.toLogFile(String.format("%s %s", this.getClass().getSimpleName(), "version check result"), "forced check = false. skipped checking");

				requiredUpdate = false;
				forcedUpdate = false;
				returnResults(requiredUpdate, forcedUpdate);
				return;
			} else {
				// Simple string comparison
				String headVersion = versionCheck.getHeadVersion();

				if (headVersion.equalsIgnoreCase(clientVersion)) {
					if (debugCra) ClnEnv.toLogFile(String.format("%s %s", this.getClass().getSimpleName(), "version check result"), "Client Version = " + clientVersion + ", headVersion = " + headVersion + "- no update required");

					// current version equals head version, no update required
					// this is the most common case
					requiredUpdate = false;
					forcedUpdate = false;
					returnResults(requiredUpdate, forcedUpdate);
					return;

				} else {
					// current version either more or less update than the head version
					// (usually less update)
					try {
						if (debug) Log.i(TAG, headVersion + "/" + clientVersion);
						String strHeadVersion[] = headVersion.split("\\.");
						String strClientVersion[] = clientVersion.split("\\.");
						int normalizedHeadVersion = 0;
						int normalizedClientVersion = 0;

						normalizedHeadVersion = Integer.parseInt(strHeadVersion[0]) * 1000000 + Integer.parseInt(strHeadVersion[1]) * 1000 + Integer.parseInt(strHeadVersion[2]);
						normalizedClientVersion = Integer.parseInt(strClientVersion[0]) * 1000000 + Integer.parseInt(strClientVersion[1]) * 1000 + Integer.parseInt(strClientVersion[2]);

						if (normalizedHeadVersion > normalizedClientVersion) {
							// Head version somehow greater than client version
							if (debugCra) ClnEnv.toLogFile(String.format("%s %s", this.getClass().getSimpleName(), "version check result"), "Client Version = " + normalizedClientVersion + ", headVersion = " + normalizedHeadVersion + "- update required");

							requiredUpdate = true;
							forcedUpdate = false;
							returnResults(requiredUpdate, forcedUpdate);
							return;
						} else {
							// Client is more recent. no update required
							if (debugCra) ClnEnv.toLogFile(String.format("%s %s", this.getClass().getSimpleName(), "version check result"), "Client Version = " + normalizedClientVersion + ", headVersion = " + normalizedHeadVersion + "- no update required");

							requiredUpdate = false;
							forcedUpdate = false;
							returnResults(requiredUpdate, forcedUpdate);
							return;
						}

					} catch (Exception e) {
						e.printStackTrace();

						// Unexpected error - skip version check
						if (debugCra) ClnEnv.toLogFile(String.format("%s %s", this.getClass().getSimpleName(), "version check result"), "Client Version = " + clientVersion + ", headVersion = " + headVersion + "- unexpected error");

						requiredUpdate = false;
						forcedUpdate = false;
						returnResults(requiredUpdate, forcedUpdate);
						return;
					}
				}
			}
		}
	}



	@Override
	public void onFail(APIsResponse response) {
		ClnEnv.isVCFinish = true;
	}


	@Override
	public void onLowMemory() {
	}

	@Override
	public void onDestroy() {
		// clear all Timer schedule to avoid restart line test
	}

	private final void returnResults(boolean requiredUpdate, boolean forcedUpdate) {
		if (debug) Log.i(TAG, requiredUpdate + "/" + forcedUpdate);
		if (forcedUpdate) {
			// The current version is blocked. Customer can only upgrade or exit
			displayForcedUpdateDialog();
		} else {
			if (!requiredUpdate) {
				// No update is required, continue the original logic
				ClnEnv.isVCFinish = true;
			} else {
				// Prompt upgrade, or continue the original logic
				displayOptionalUpdateDialog();
			}
		}
	}

	// Dialog: upgrade or exit
	private void displayForcedUpdateDialog() {
		me.stopSelf();
		Intent dialogIntent = new Intent(me.getApplicationContext(), GlobalDialog.class);
		dialogIntent.putExtra("DIALOGTYPE", R.string.CONST_DIALOG_VC_FORCE_UPDATE);
		dialogIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
		me.getApplicationContext().startActivity(dialogIntent);
	}

	// Dialog: continue or exit
	private void displayOptionalUpdateDialog() {
		me.stopSelf();
		Intent dialogIntent = new Intent(me.getApplicationContext(), GlobalDialog.class);
		dialogIntent.putExtra("DIALOGTYPE", R.string.CONST_DIALOG_VC_OPTION_UPDATE);
		dialogIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
		me.getApplicationContext().startActivity(dialogIntent);
	}
}