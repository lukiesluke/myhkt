package com.pccw.myhkt.adapter;


import java.util.List;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;

import com.pccw.biz.myhkt.R;
import com.pccw.myhkt.Utils;
import com.pccw.myhkt.lib.ui.AAQuery;
import com.pccw.myhkt.model.TabItem;

public class TabBarAdapter extends BaseAdapter {

	public 		  Context				context;
	public 		  LayoutInflater		inflater;
	public 		  List<TabItem>			tabItems;
	private final boolean				isZh;
	private 	  int					itemHeight;
	private 	  int 					textColor;
	private 	  int 					textSize;
	private 	  int 					currentPos = 0;
	private 	  int					imagepappding ;
	private 	  int					imageHeight ;
	private 	  int                   textViewHeight ;
	private 	  AAQuery               aq;
	//Filterable
	private 	  BitmapFactory.Options bitmapOptions;
	private int padding_twocol = 0;




	public TabBarAdapter(Context context, List<TabItem>tapItems, int itemHeight, int textColor, int textSize) {
		super();
		this.context = context;
		this.itemHeight = itemHeight;
		this.textColor = textColor;
		this.textSize = textSize;
		this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		this.tabItems = tapItems;
		imagepappding = (int) context.getResources().getDimension(R.dimen.indicator_tab_icon_spac);
		imageHeight = itemHeight /2;
		textViewHeight = itemHeight /2;
		bitmapOptions = new BitmapFactory.Options();
		bitmapOptions.inJustDecodeBounds = true;
		padding_twocol = (int)context.getResources().getDimension(R.dimen.padding_twocol);
		// Language indicator
        isZh = "zh".equalsIgnoreCase(Utils.getString(context, R.string.myhkt_lang));
	}

	@Override
	public int getCount() {
		if (tabItems == null) return 0;
		return tabItems.size();
	}

	@Override
	public Object getItem(int position) {
		if (position >= 0 && position < tabItems.size()) {
			return tabItems.get(position);
		} else {
			return null;
		}
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		Boolean isShadow = tabItems.get(position).imageRes < 0 && tabItems.get(position).text == null && position !=0 && (tabItems.get(position-1).imageRes  > 0 ||  tabItems.get(position-1).text != null); 
		//		if () {
		//			Log.i("TabBarAdapter", "shw" + " " + position);
		//			convertView = inflater.inflate(R.layout.tab_item_shadow, parent, false);
		//			aq = new AAQuery(convertView);
		//			aq.id(R.id.tab_item_imageview).width(5, true).image(R.drawable.hktindicator_shadow);
		//			aq.id(R.id.tab_item_bg).backgroundColorId(R.color.hkt_greybg).enabled(false);
		//		} else {		
		convertView = inflater.inflate(R.layout.tab_item, parent, false);
		aq = new AAQuery(convertView);

		if (!isShadow) {
			//Style TextView
			aq.id(R.id.tab_item_txt).text(tabItems.get(position).text).textColor(textColor).height(textViewHeight , false);
			aq.id(R.id.tab_item_txt).getTextView().setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize);
			aq.id(R.id.tab_item_txt).getTextView().setGravity(Gravity.CENTER);
			aq.id(R.id.tab_item_txt).getTextView().setPadding(padding_twocol, 0, padding_twocol, 0);
		} else {
			aq.id(R.id.tab_item_txt).visibility(View.GONE);
		}

		//Style ImageView
		aq.id(R.id.tab_item_imageview).getImageView().setScaleType(ImageView.ScaleType.FIT_XY);
		if(!isShadow) {
			if (tabItems.get(position).imageRes != -1) {
				BitmapFactory.Options options = new BitmapFactory.Options();
				bitmapOptions.inJustDecodeBounds = true;
				Bitmap bitmap = BitmapFactory.decodeResource(context.getResources(), tabItems.get(position).imageRes, options);
				float bH = bitmap.getHeight();
				float bW = bitmap.getWidth();
				int width = (int)((float) (imageHeight-imagepappding) / bH *bW); 
				aq.id(R.id.tab_item_imageview).height(imageHeight-imagepappding ,false).width(width, false);
				LayoutParams params1 = (LayoutParams) aq.id(R.id.tab_item_imageview).getView().getLayoutParams();
				params1.topMargin = imagepappding;
				params1.addRule(RelativeLayout.CENTER_HORIZONTAL);
				aq.id(R.id.tab_item_imageview).getView().setLayoutParams(params1);
				aq.id(R.id.tab_item_imageview).image(tabItems.get(position).imageRes);
			} else {
				aq.id(R.id.tab_item_imageview).height(imageHeight ,false);
				aq.id(R.id.tab_item_imageview).image(tabItems.get(position).imageRes != -1 ? context.getResources().getDrawable(tabItems.get(position).imageRes) : null);
			}	
		} else {
			aq.id(R.id.tab_item_imageview).getView().setAlpha(0.4f);
			aq.id(R.id.tab_item_imageview).width(5, true).height(ViewGroup.LayoutParams.MATCH_PARENT, true).image(R.drawable.hktindicator_shadow).backgroundColorId(R.color.black);
		}
		//Style Bg
		ViewGroup.LayoutParams params = aq.id(R.id.tab_item_bg).getView().getLayoutParams();
		params.height = itemHeight;
		aq.id(R.id.tab_item_bg).getView().setLayoutParams(params);
		if (tabItems.get(position).imageRes < 0 && tabItems.get(position).text == null) {
			aq.id(R.id.tab_item_bg).backgroundColorId(R.color.hkt_greybg).enabled(false);
		} else {
			aq.id(R.id.tab_item_bg).background(currentPos == position ? R.drawable.hkt_tab_sel : position == 0 ? R.drawable.hkt_tab_bg_selector_firsttab : R.drawable.hkt_tab_bg_selector);
		}		
		//		}
		return convertView;
	}

	public void setCurrentPos(int pos) {
		currentPos = pos;
		this.notifyDataSetChanged();
	}
}
