package com.pccw.myhkt.fragment;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.graphics.Point;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AlertDialog.Builder;
import androidx.fragment.app.Fragment;

import com.pccw.biz.myhkt.R;
import com.pccw.dango.shared.cra.AddOnCra;
import com.pccw.dango.shared.entity.SubnRec;
import com.pccw.myhkt.APIsManager;
import com.pccw.myhkt.APIsManager.OnAPIsListener;
import com.pccw.myhkt.ClnEnv;
import com.pccw.myhkt.Utils;
import com.pccw.myhkt.activity.BaseActivity;
import com.pccw.myhkt.listener.OnAccountDeleteListener;

import java.util.Objects;

/************************************************************************
 File       : BaseFragment.java
 Desc       : BaseFragment for all Fragments
 Name       : BaseFragment
 Created by : Vincent Fung
 Date       : 26/02/2014

 Change History:
 Date       Modified By			Description
 ---------- ----------------	-------------------------------
 26/02/2014 Vincent Fung			- First draft
 08/04/2014 Vincent Fung			- Enhanced ProgressDialog performance
 09/05/2014 Vincent Fung			- Moved initUI to onCreate, cleanupUI to onDestroy
 16/03/2015 Derek Tsui			1. [bugfix] progressdialog set to static by accident fixed.
 2. added redirectdialog onCancel click handling
 *************************************************************************/

public abstract class BaseFragment extends Fragment implements OnAPIsListener, View.OnClickListener {
    protected static String recallAction = null;
    // Common Components
    protected boolean debug = false;
    protected String TAG = "BaseFragment";
    protected AlertDialog alertDialog = null;
    protected Boolean isZh = false;
    protected int basePadding;
    protected int extralinespace;
    protected int deviceWidth;
    protected int padding_twocol;
    // protected ProgressDialogHelper	progressDialogHelper;
    // Use the boolean to control if refreshData should run on resume
    protected Boolean refreshRunOnResume = true;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        debug = activity.getResources().getBoolean(R.bool.DEBUG);
        if (debug)
            Log.i(this.getClass().toString(), "onAttach" + "(" + java.lang.System.identityHashCode(this) + ")");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (debug) Log.i(this.getClass().toString(), "onCreateView");
        return null;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        if (debug)
            Log.i(this.getClass().toString(), "onCreate" + "(" + java.lang.System.identityHashCode(this) + ")");
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        // Language indicator
        isZh = "zh".equalsIgnoreCase(getResString(R.string.myhkt_lang));
    }

    @Override
    public void onStart() {
        if (debug)
            Log.i(this.getClass().toString(), "onStart" + "(" + java.lang.System.identityHashCode(this) + ")");
        super.onStart();
        // Language indicator
        isZh = "zh".equalsIgnoreCase(getResString(R.string.myhkt_lang));
        initUI();
    }

    @Override
    public void onResume() {
        if (debug)
            Log.i(this.getClass().toString(), "onResume" + "(" + java.lang.System.identityHashCode(this) + ")");
        super.onResume();
        if (refreshRunOnResume) {
            refreshData();
        }
    }

    @Override
    public void onPause() {
        if (debug) Log.i(this.getClass().toString(), "onPause");
        super.onPause();
        stopHandlers();
    }

    @Override
    public void onStop() {
        if (debug)
            Log.i(this.getClass().toString(), "onStop" + "(" + java.lang.System.identityHashCode(this) + ")");
        super.onStop();
    }

    /**
     * Please call it on CreateView in fragment
     **/
    protected void initData() {
        extralinespace = (int) getResources().getDimension(R.dimen.extralinespace);
        basePadding = (int) getResources().getDimension(R.dimen.basePadding);
        padding_twocol = (int) getResources().getDimension(R.dimen.padding_twocol);
        Display display = Objects.requireNonNull(getActivity()).getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        deviceWidth = size.x;
    }

    protected void initUI() {
        // Prepare ProgressDialog
        // progressDialogHelper = ProgressDialogHelper.getInstance(getActivity());
    }

    protected void refreshData() {
        if (debug) Log.i(this.getClass().toString(), "refreshData");
    }

    protected void setModuleId() {
        if (debug)
            Log.i(this.getClass().toString(), "setModuleId" + ((BaseActivity) Objects.requireNonNull(this.getActivity())).getModuleId());
    }

    protected final void setOnClickListener(Button... btnList) {
        for (Button Btn : btnList) {
            if (Btn != null) {
                Btn.setOnClickListener(this);
            }
        }
    }

    public void recallDoAuth(Fragment frag, SubnRec subnRec) {

        AddOnCra authAddonCra = new AddOnCra();
        authAddonCra.setISms(false);
        authAddonCra.setILoginId(ClnEnv.isLoggedIn() ? ClnEnv.getSessionLoginID() : "");
        authAddonCra.setISubnRec(subnRec);

        APIsManager.doAuthen(frag, authAddonCra);
    }

	/*	public void recallDoAuthMyMobByAreaAsyncTask(SubscriptionRec subnRec, AcctAgent acctagent) {
		progressDialogHelper.showProgressDialog(getActivity(), onstopHandler, true, ProgressDialogHelper.dologin);

		AuthGrq authgrq = new AuthGrq();
		authgrq.actionTy = DoAuthenMyMobAsyncTask.actionTy;
		authgrq.sessTok = ClnEnv.getSessTok();
		authgrq.authCra = new AuthCra();
		if (ClnEnv.isLoggedIn()) {
			authgrq.authCra.setILoginId(ClnEnv.getSessionLoginID());
		} else {
			authgrq.authCra.setILoginId("");
		}
		authgrq.authCra.setIoSubscriptionRec(subnRec);
		authgrq.authCra.setISms(false);

		doAuthenMyMobAsyncTask = new DoAuthenMyMobAsyncTask(getActivity().getApplicationContext(), getCallBackHandler()).execute(authgrq);
	}*/

    protected Handler getCallBackHandler() {
        return new Handler();
    }

    // Async-task Call-back handler
    protected void stopHandlers() {
        // progressDialogHelper.dismissProgressDialog(true);
    }

    // Simple UI Dialog
    public final void displayDialog(String message) {
        if (alertDialog != null) {
            alertDialog.dismiss();
        }

        AlertDialog.Builder builder = new Builder(Objects.requireNonNull(getActivity()));
        builder.setMessage(message);
        builder.setPositiveButton(Utils.getString(getActivity(), R.string.btn_ok), new OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                alertDialog = null;
            }
        });

        alertDialog = builder.create();
        alertDialog.show();
    }

    // Simple UI Dialog
    public final void displayDialog(String message, OnAccountDeleteListener listener) {
        if (alertDialog != null) {
            alertDialog.dismiss();
        }

        AlertDialog.Builder builder = new Builder(Objects.requireNonNull(getActivity()));
        builder.setCancelable(false);
        builder.setMessage(message);
        builder.setPositiveButton(Utils.getString(getActivity(), R.string.btn_ok), new OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (listener != null) {
                    listener.onFailedDeleted();
                }
                dialog.dismiss();
                alertDialog = null;
            }
        });

        alertDialog = builder.create();
        alertDialog.show();
    }

    // Login First Dialog
	/*	protected final void loginFirstDialog(final int clickbtn) {
		AlertDialog.Builder builder = new Builder(getActivity());
		builder.setMessage(R.string.myhkt_plslogin);
		builder.setPositiveButton(R.string.btn_ok, new OnClickListener() {
			Bundle	rbundle;

			@Override
			public void onClick(DialogInterface dialog, int which) {
				Intent intent;
				dialog.dismiss();
				intent = new Intent(getActivity(), LoginActivity.class);
				rbundle = new Bundle();
				rbundle.putInt("CLICKBUTTON", clickbtn);
				intent.putExtras(rbundle);
				getActivity().startActivity(intent);
				getActivity().overridePendingTransition(R.anim.right_slide_in, R.anim.left_slide_out);
			}
		});
		builder.create().show();
	}

	// redirect to login screen if RC_IVSESS
	protected final void redirectDialog(String message) {
		AlertDialog.Builder builder = new Builder(getActivity());
		builder.setMessage(message);
		builder.setPositiveButton(Utils.getString(getActivity(), R.string.btn_ok), new OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();
				if (ClnEnv.isMyMobFlag() && !ClnEnv.isLoggedIn()) {
					Intent intent = new Intent(getActivity().getApplicationContext(), MyMobileActivity.class);
					intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
					startActivity(intent);
					getActivity().overridePendingTransition(R.anim.right_slide_in, R.anim.left_slide_out);
					getActivity().finish();
				} else if (ClnEnv.isMyMobFlag() && ClnEnv.isLoggedIn()) {
					ClnEnv.clear(getActivity());
					Intent intent = new Intent(getActivity().getApplicationContext(), LoginActivity.class);
					Bundle bundle = new Bundle();
					bundle.putInt("CLICKBUTTON", R.id.adapter_mymob_accountlist_front);
					intent.putExtras(bundle);
					startActivity(intent);
					getActivity().overridePendingTransition(R.anim.right_slide_in, R.anim.left_slide_out);
					getActivity().finish();
				} else {
					ClnEnv.clear(getActivity());
					Intent intent = new Intent(getActivity().getApplicationContext(), LoginActivity.class);
					Bundle bundle = new Bundle();
					bundle.putInt("CLICKBUTTON", R.id.mainmenu_button_loginout);
					intent.putExtras(bundle);
					startActivity(intent);
					getActivity().overridePendingTransition(R.anim.right_slide_in, R.anim.left_slide_out);
					getActivity().finish();
				}
			}
		});
		builder.setOnCancelListener(new Dialog.OnCancelListener() {
			public void onCancel(DialogInterface dialog) {
				dialog.dismiss();
				if (ClnEnv.isMyMobFlag() && !ClnEnv.isLoggedIn()) {
					Intent intent = new Intent(getActivity().getApplicationContext(), MyMobileActivity.class);
					intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
					startActivity(intent);
					getActivity().overridePendingTransition(R.anim.right_slide_in, R.anim.left_slide_out);
					getActivity().finish();
				} else if (ClnEnv.isMyMobFlag() && ClnEnv.isLoggedIn()) {
					ClnEnv.clear(getActivity());
					Intent intent = new Intent(getActivity().getApplicationContext(), LoginActivity.class);
					Bundle bundle = new Bundle();
					bundle.putInt("CLICKBUTTON", R.id.adapter_mymob_accountlist_front);
					intent.putExtras(bundle);
					startActivity(intent);
					getActivity().overridePendingTransition(R.anim.right_slide_in, R.anim.left_slide_out);
					getActivity().finish();
				} else {
					ClnEnv.clear(getActivity());
					Intent intent = new Intent(getActivity().getApplicationContext(), LoginActivity.class);
					Bundle bundle = new Bundle();
					bundle.putInt("CLICKBUTTON", R.id.mainmenu_button_loginout);
					intent.putExtras(bundle);
					startActivity(intent);
					getActivity().overridePendingTransition(R.anim.right_slide_in, R.anim.left_slide_out);
					getActivity().finish();
				}
			}
		});
		builder.create().show();
	}*/

    // UI on-click events handling
    @Override
    public void onClick(View selectedView) {
    }

    protected final void cleanupStack() {
        if (Objects.requireNonNull(getActivity()).getSupportFragmentManager().getBackStackEntryCount() > 0) {
            int popCount = getActivity().getSupportFragmentManager().getBackStackEntryCount();
            for (int i = 0; i < popCount; i++) {
                getActivity().getSupportFragmentManager().popBackStack();
            }
        }
    }

    public String getResString(int res) {
        return Utils.getString(getActivity(), res);
    }

    public float getResDimen(int res) {
        return getActivity().getResources().getDimension(res);
    }

    public Integer getResInt(int res) {
        return getActivity().getResources().getInteger(res);
    }

    // Free all references to the UI elements
    protected void cleanupUI() {
    }

    protected void cleanupFragments() {
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onDestroy() {
        cleanupFragments();
        cleanupUI();
        super.onDestroy();
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    protected static class ViewHolder {
        public Context cxt;
    }
}
