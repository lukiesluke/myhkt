package com.pccw.myhkt.fragment;

import static com.pccw.myhkt.util.Constant.FORMAT_DDMMYYYY;
import static com.pccw.myhkt.util.Constant.FORMAT_DD_MMM_YYYY;
import static com.pccw.myhkt.util.Constant.FORMAT_MMM_YYYY;
import static com.pccw.myhkt.util.Constant.FORMAT_TIME_STAMP;
import static com.pccw.myhkt.util.Constant.FORMAT_YYYYMMDD;
import static com.pccw.myhkt.util.Constant.FORMAT_YYYY_MM_DD;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;

import com.pccw.dango.shared.cra.AddOnCra;
import com.pccw.dango.shared.cra.BinqCra;
import com.pccw.dango.shared.entity.Bill;
import com.pccw.dango.shared.g3entity.G3ServiceCallBarStatusDTO;
import com.pccw.dango.shared.tool.RC;
import com.pccw.myhkt.APIsManager;
import com.pccw.myhkt.APIsResponse;
import com.pccw.myhkt.ClnEnv;
import com.pccw.myhkt.DialogHelper;
import com.pccw.myhkt.FAWrapper;
import com.pccw.myhkt.InterpretRCManager;
import com.pccw.myhkt.R;
import com.pccw.myhkt.SaveAccountHelper;
import com.pccw.myhkt.Tool;
import com.pccw.myhkt.Utils;
import com.pccw.myhkt.activity.BaseActivity;
import com.pccw.myhkt.activity.PPSActivity;
import com.pccw.myhkt.activity.TapNGoActivity;
import com.pccw.myhkt.cell.model.BigTextCell;
import com.pccw.myhkt.cell.model.Cell;
import com.pccw.myhkt.cell.model.DetailBtnCell;
import com.pccw.myhkt.cell.model.FourBtnCell;
import com.pccw.myhkt.cell.model.SmallTextCell;
import com.pccw.myhkt.cell.view.CellViewAdapter;
import com.pccw.myhkt.dialogs.FPSDialog;
import com.pccw.myhkt.lib.ui.AAQuery;
import com.pccw.myhkt.mymob.MyMobileAccountHelper;
import com.pccw.myhkt.util.Constant;
import com.pccw.myhkt.util.RuntimePermissionUtil;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

/************************************************************************
 * File : BillSumFragment.java
 * Desc : Bills and Summary
 * Name : BillSumFragment
 * by 	: 
 * Date :
 * Change History:
 * Date       Modified By		Description
 * ---------- ----------------	-------------------------------
 * 06/7/2017  Abdulmoiz Esmail	-Updated refresh methods, restrict getting of bills
 * 								if LOB is PCD and if bill agent show message, otherwise
 *  					 		get the bill	
 * 06/8/2017  Abdulmoiz Esmail  -Updated refresh methods, remove call GetPlanIMS AP, instead
 * 								get the PCD and billagt from callback
 * 15/6/2017  Abdulmoiz Esmail  -59316: PCD subscription under csl, setModuleId for PCD Plan & PCD Bill.
 * 16/6/2017  Abdulmoiz Esmail  -59684: Change Bill Payment reminder Alert Message Color
 * 04/1/2018  Abdulmoiz Esmail  -Updated startDownloadPdf method, added checking of file storage permission before downloading
 *************************************************************************/

public class BillSumFragment extends BaseServiceFragment {
    private BillSumFragment me;
    //payment method urls
    private final String payMethUrl1010Zh = "http://1010.com.hk/c/payment_method_e";
    private final String payMethUrl1010En = "http://1010.com.hk/e/payment_method_e";
    private final String payMethUrlIOIZh = "http://1010.com.hk/c/payment_method_m";
    private final String payMethUrlIOIEn = "http://1010.com.hk/e/payment_method_m";
    private final String payMethUrlO2FZh = "https://www.hkcsl.com/tc/payment-method/";
    private final String payMethUrlO2FEn = "https://www.hkcsl.com/en/payment-method/";
    private final String payMethUrlMOBZh = "https://www.hkcsl.com/tc/payment-method/";
    private final String payMethUrlMOBEn = "https://www.hkcsl.com/en/payment-method/";
    private final String payMethUrlPCDZh = "https://www.netvigator.com/chi/tips-and-tutorials/general/payment.html";
    private final String payMethUrlPCDEn = "https://www.netvigator.com/eng/tips-and-tutorials/general/payment.html";
    private final String payMethUrlLTSZh = "http://hkt-eye.com/payment/payment_tc.html";
    private final String payMethUrlLTSEn = "http://hkt-eye.com/payment/payment.html";
    private final String payMethUrlTVZh = "https://www.nowtv.now.com/support/making-a-payment/";
    private final String payMethUrlTVEn = "https://www.nowtv.now.com/support/making-a-payment/";
    private final String payMethUrlCSPEn = "https://www.clubsim.com.hk/post-paid/payment-method";
    private final String payMethUrlCSPZh = "https://www.clubsim.com.hk/post-paid/payment-method";
    private View myView;
    private AAQuery aq;
    private LinearLayout frame;
    private List<Cell> cellList;
    private CellViewAdapter cellViewAdapter;
    // Account Database Helper
    private SaveAccountHelper saveAccountHelper;                            // SQLite database for a new bill checkingF
    private MyMobileAccountHelper myMobileAccountHelper;                        // MyMobileAcct SQLite database
    private BinqCra binqCra;
    private AddOnCra addOnCra;
    private String billDate;
    // Thin bill
    private boolean isThinBill1 = false;
    private boolean isThinBill2 = false;
    private boolean isThinBill3 = false;
    private int clickBill;
    private int chkBillCount;
    private String lobString = "";
    private final String TAG = "lwg";

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        me = this;
        View fragmentLayout = inflater.inflate(R.layout.fragment_billsum, container, false);
        myView = fragmentLayout;
        lobString = callback_main.getAcctAgent().getSubnRec().lob;
        initData();
        return fragmentLayout;
    }

    /*
     * Update: Moiz:060817: Check if the LOB is PCD and SubnRec.billagt is true
     * if true, show the message bill by agent, otherwise doGetBill()
     */
    @Override
    protected final void refreshData() {
        if (R.string.CONST_SELECTEDVIEW_BILLSUMMARY == callback_main.getActiveSubview()) {
            doGATracker();
            if ("PCD".equalsIgnoreCase(callback_main.getSubnRec().lob) && callback_main.getSubnRec().isBillByAgent()) {
                //Show message
                showPcdBillByAgent();
            } else {
                if (((BillFragment) requireParentFragment()).isBillSum) {
                    doGetBill();
                }
            }
        }
    }

    /*
     * Update: Moiz:060817: Check if the LOB is PCD and SubnRec.billagt is true
     * if true, show the message bill by agent, otherwise doGetBill()
     */
    @Override
    public void refresh() {
        super.refresh();
        doGATracker();
        if ("PCD".equalsIgnoreCase(callback_main.getSubnRec().lob) && callback_main.getSubnRec().isBillByAgent()) {
            showPcdBillByAgent();
        } else {
            if (((BillFragment) requireParentFragment()).isBillSum) {
                doGetBill();
            }
        }
    }

    protected void initData() {
        super.initData();
    }

    @SuppressLint("NonConstantResourceId")
    @Override
    public void onResume() {
        super.onResume();
        //tracker
        switch (callback_main.getLob()) {
            case R.string.CONST_LOB_MOB:
            case R.string.CONST_LOB_IOI:
            case R.string.CONST_LOB_1010:
            case R.string.CONST_LOB_O2F:
                FAWrapper.getInstance().sendFAScreen(getActivity(), R.string.CONST_SCRN_MOBBILLSUMMARY, false);
                callback_main.setTrackerId(Utils.getString(requireActivity(), R.string.CONST_SCRN_MOBBILLSUMMARY));
                if (debug)
                    Log.i(TAG, "GATRACKER" + Utils.getString(requireActivity(), R.string.CONST_SCRN_MOBBILLSUMMARY));
                break;
            case R.string.CONST_LOB_PCD:
                FAWrapper.getInstance().sendFAScreen(getActivity(), R.string.CONST_SCRN_PCDBILLSUMMARY, false);
                callback_main.setTrackerId(Utils.getString(requireActivity(), R.string.CONST_SCRN_PCDBILLSUMMARY));
                if (debug)
                    Log.i(TAG, "GATRACKER" + Utils.getString(requireActivity(), R.string.CONST_SCRN_PCDBILLSUMMARY));
                break;
            case R.string.CONST_LOB_TV:
                FAWrapper.getInstance().sendFAScreen(getActivity(), R.string.CONST_SCRN_TVBILLSUMMARY, false);
                callback_main.setTrackerId(Utils.getString(requireActivity(), R.string.CONST_SCRN_TVBILLSUMMARY));
                if (debug)
                    Log.i(TAG, "GATRACKER" + Utils.getString(requireActivity(), R.string.CONST_SCRN_TVBILLSUMMARY));
                break;
            case R.string.CONST_LOB_LTS:
                FAWrapper.getInstance().sendFAScreen(getActivity(), R.string.CONST_SCRN_LTSBILLSUMMARY, false);
                callback_main.setTrackerId(Utils.getString(requireActivity(), R.string.CONST_SCRN_LTSBILLSUMMARY));
                if (debug)
                    Log.i(TAG, "GATRACKER" + Utils.getString(requireActivity(), R.string.CONST_SCRN_LTSBILLSUMMARY));
                break;
            case R.string.CONST_LOB_CSP:
                FAWrapper.getInstance().sendFAScreen(getActivity(), R.string.CONST_SCRN_CSPBILLSUMMARY, false);
                callback_main.setTrackerId(Utils.getString(requireActivity(), R.string.CONST_SCRN_CSPBILLSUMMARY));
                if (debug)
                    Log.i(TAG, "GATRACKER" + Utils.getString(requireActivity(), R.string.CONST_SCRN_CSPBILLSUMMARY));
                break;
        }
    }

    protected void initBasicUI() {
        cellList = new ArrayList<>();
        cellViewAdapter = new CellViewAdapter(this);
        aq = new AAQuery(myView);

        aq.id(R.id.fragment_bill_layout).backgroundColorId(R.color.white);
        frame = (LinearLayout) aq.id(R.id.fragment_bill_listview).getView();

        //Header
        BigTextCell cell1;
        String accNo = Tool.formatAcctNum(callback_main.getAcctAgent().getAcctNum());
        cell1 = new BigTextCell(getResString(R.string.myhkt_summary_accountNo) + " ", accNo);
        cell1.setTopMargin(basePadding);
        cell1.setLeftMargin(basePadding);
        cell1.setRightMargin(basePadding);
        cell1.setContentColorId(R.color.hkt_textcolor);
        cellList.add(cell1);
        cellViewAdapter.setView(frame, cellList);
    }

    /***************
     * Listeners
     ******************/
    public void onUpdateClick(View v) {
        ((BillFragment) requireParentFragment()).billInfoLTSSubView = R.string.CONST_SELECTEDVIEW_BILLINFOLTS_CHILD1;
        assert getParentFragment() != null;
        ((BillFragment) getParentFragment()).openBillInfoFrag();
    }

    public void onPayClick(View v) {
        // Event Tracker
        FAWrapper.getInstance().sendFAEvents(me.getActivity(), R.string.CONST_GA_CATEGORY_USERLV,
                R.string.CONST_GA_ACTION_BTN, R.string.CONST_GA_LABEL_PAYMETH,
                " (" + callback_main.getLobString() + ")", false);

        String url = "";
        if (R.string.CONST_LOB_LTS == callback_main.getLob()) {
            url = isZh ? payMethUrlLTSZh : payMethUrlLTSEn;
        } else if (R.string.CONST_LOB_PCD == callback_main.getLob()) {
            url = isZh ? payMethUrlPCDZh : payMethUrlPCDEn;
        } else if (R.string.CONST_LOB_TV == callback_main.getLob()) {
            url = isZh ? payMethUrlTVZh : payMethUrlTVEn;
        } else if (R.string.CONST_LOB_MOB == callback_main.getLob()) {
            url = isZh ? payMethUrlMOBZh : payMethUrlMOBEn;
        } else if (R.string.CONST_LOB_1010 == callback_main.getLob()) {
            url = isZh ? payMethUrl1010Zh : payMethUrl1010En;
        } else if (R.string.CONST_LOB_IOI == callback_main.getLob()) {
            url = isZh ? payMethUrlIOIZh : payMethUrlIOIEn;
        } else if (R.string.CONST_LOB_O2F == callback_main.getLob()) {
            url = isZh ? payMethUrlO2FZh : payMethUrlO2FEn;
        } else if (R.string.CONST_LOB_CSP == callback_main.getLob()) {
            url = isZh ? payMethUrlCSPZh : payMethUrlCSPEn;
        }
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        startActivity(browserIntent);
    }

    public void onUpdatePayMethodClick(View v) {
        ((BillFragment) requireParentFragment()).billInfoLTSSubView = R.string.CONST_SELECTEDVIEW_BILLINFOLTS_CHILD2;
        assert getParentFragment() != null;
        ((BillFragment) getParentFragment()).openBillInfoFrag();
    }

    @SuppressLint("NonConstantResourceId")
    public void onPPSClick(View v) {
        if (debug) Log.e(TAG, "PPS ONCLICK TRACKER");
        // Event Tracker
        FAWrapper.getInstance().sendFAEvents(me.getActivity(), R.string.CONST_GA_CATEGORY_USERLV, R.string.CONST_GA_ACTION_BTN, R.string.CONST_GA_LABEL_PPS, " (" + callback_main.getLobString() + ")", true);

        Intent intent = new Intent(this.getActivity(), PPSActivity.class);
        Bundle bundle = new Bundle();
        bundle.putInt("LOB", callback_main.getLob());

        if (callback_main.getAcctAgent().getLobType() == R.string.CONST_LOB_LTS) {
            bundle.putInt("LTSTYPE", callback_main.getLtsType());
        }
        //insert amount
        switch (callback_main.getLob()) {
            case R.string.CONST_LOB_MOB:
            case R.string.CONST_LOB_IOI:
            case R.string.CONST_LOB_1010:
            case R.string.CONST_LOB_O2F:
            case R.string.CONST_LOB_CSP:
                bundle.putSerializable("ACCTNUM", callback_main.getSubnRec().acctNum);
                if (binqCra.getOG3OutstandingBalanceDTO() != null) {
                    bundle.putString("AMOUNT", Utils.convertStringToPrice(Utils.convertDoubleToString(binqCra.getOG3OutstandingBalanceDTO().getTotalOSBal(), 2)));
                }
                break;
            case R.string.CONST_LOB_PCD:
            case R.string.CONST_LOB_TV:
                if (binqCra.getOBillPreview() != null) {
                    bundle.putString("ACCTNUM", binqCra.getOBillPreview().getReceiptAcctNo());
                    bundle.putString("AMOUNT", String.format("$%s", binqCra.getOBillPreview().getCurrentBalance()));
                }
                break;
            case R.string.CONST_LOB_LTS:
                Bill[] billary = isOBillListNotNull() ? binqCra.getOBillList().getOBillAry() : new Bill[0];
                bundle.putString("ACCTNUM", callback_main.getAcctAgent().getAcctNum());

                if (callback_main.IsZombie()) {
                    bundle.putString("AMOUNT", Utils.convertStringToPrice(billary[0].getInvAmt()));
                } else {
                    bundle.putString("AMOUNT", Utils.convertStringToPrice(Utils.convertDoubleToString(binqCra.getOG3OutstandingBalanceDTO().getTotalOSBal(), 2)));
                }
                break;
        }

        intent.putExtras(bundle);
        startActivity(intent);
        me.requireActivity().overridePendingTransition(R.anim.right_slide_in, R.anim.left_slide_out);
    }

    @SuppressLint("NonConstantResourceId")
    public void onSevenClick(View v) {
        if (debug) Log.e(TAG, "SevenEleven ONCLICK TRACKER");
        FAWrapper.getInstance().sendFAEvents(me.getActivity(), R.string.CONST_GA_CATEGORY_USERLV,
                R.string.CONST_GA_ACTION_BTN, R.string.CONST_GA_LABEL_711,
                " (" + callback_main.getLobString() + ")", true);

        String serviceType;
        String displayDate;
        String amountPCDTV;
        double amtdouble = 0.0;
        if (binqCra != null) {
            switch (callback_main.getAcctAgent().getLobType()) {
                case R.string.CONST_LOB_MOB:
                case R.string.CONST_LOB_IOI:
                    if (binqCra.getOG3OutstandingBalanceDTO().getLastBillDate() != null) {
                        displayDate = Utils.toDateString(binqCra.getOG3OutstandingBalanceDTO().getLastBillDate().toString(), "yyyy-MM-dd hh:mm:ss.S", "dd/MM/yyyy");
                    } else {
                        displayDate = Utils.getString(me.requireActivity(), R.string.summary_not_applicable);
                    }
                    serviceType = (R.string.CONST_LOB_IOI == callback_main.getLob()) ? Utils.getString(me.requireActivity(), R.string.myhkt_qrcode_servicetype_1010) : Utils.getString(me.requireActivity(), R.string.myhkt_qrcode_servicetype_mob);
                    DialogHelper.create7ElevenDialog(me.requireActivity(), callback_main.getLob(), serviceType,
                            displayDate, callback_main.getSubnRec().acctNum, Utils.getString(me.requireActivity(), R.string.qrcode_bill_type_mobile),
                            binqCra.getOG3OutstandingBalanceDTO().getTotalOSBal());
                    break;
                case R.string.CONST_LOB_PCD:
                case R.string.CONST_LOB_TV:
                    serviceType = (R.string.CONST_LOB_PCD == callback_main.getLob()) ? Utils.getString(me.requireActivity(), R.string.myhkt_qrcode_servicetype_pcd) : Utils.getString(me.requireActivity(), R.string.myhkt_qrcode_servicetype_tv);
                    if (binqCra.getOBillPreview().getDisplayBillDate() != null) {
                        SimpleDateFormat sdf = new SimpleDateFormat(FORMAT_DDMMYYYY, Locale.US);

                        displayDate = sdf.format(binqCra.getOBillPreview().getDisplayBillDate());
                    } else {
                        displayDate = Utils.getString(me.getActivity(), R.string.summary_not_applicable);
                    }
                    // PCD Amount is in formatted string, strip , then convert back to double
                    amountPCDTV = binqCra.getOBillPreview().getCurrentBalance().replaceAll(",", "");
                    amtdouble = 0.0;
                    try {
                        amtdouble = Double.parseDouble(amountPCDTV);
                    } catch (NumberFormatException e) {
                        e.printStackTrace();
                    }
                    DialogHelper.create7ElevenDialog(me.getActivity(), callback_main.getLob(), serviceType, displayDate, binqCra.getOBillPreview().getReceiptAcctNo(),
                            Utils.getString(me.getActivity(), R.string.qrcode_bill_type_pcd), amtdouble);
                    break;
                case R.string.CONST_LOB_LTS:
                    Bill[] billAry = isOBillListNotNull() ? binqCra.getOBillList().getOBillAry() : new Bill[0];
                    String billDt;
                    String billAmt = "";
                    String billDrg = "";
                    String billType;
                    if (billAry.length > 0 && !billAry[0].getInvDate().equalsIgnoreCase("")) {
                        // Displaying the first bill info as summary
                        billDt = Tool.formatDate(billAry[0].getInvDate(), FORMAT_DDMMYYYY);
                        billAmt = billAry[0].getInvAmt();
                        if (billAry[0].getDrgBillty() != null) {
                            billDrg = billAry[0].getDrgBillty();
                        }
                    } else {
                        billDt = Utils.getString(me.requireActivity(), R.string.summary_not_applicable);
                    }
                    // Bill List Amount became string in new MyHKT, converting to double for now, NEED CONFIRMATION
                    amountPCDTV = billAmt.replaceAll(",", "");
                    amtdouble = 0.0;
                    try {
                        if (callback_main.IsZombie()) {
                            amtdouble = Double.parseDouble(amountPCDTV);
                        } else {
                            amtdouble = binqCra.getOG3OutstandingBalanceDTO().getTotalOSBal();
                        }
                    } catch (NumberFormatException e) {
                        e.printStackTrace();
                    }

                    billType = Utils.getString(me.requireActivity(), R.string.qrcode_bill_type_lts);
                    if (billDrg != null && billDrg.equalsIgnoreCase("R")) {
                        billType = Utils.getString(me.requireActivity(), R.string.qrcode_bill_type_lts_ruby);
                    }

                    DialogHelper.create7ElevenDialog(me.requireActivity(), callback_main.getLob(), Utils.getString(me.getActivity(), R.string.myhkt_qrcode_servicetype_lts),
                            billDt, callback_main.getAcctAgent().getAcctNum(),
                            billType, amtdouble);
                    break;
                case R.string.CONST_LOB_1010:
                case R.string.CONST_LOB_O2F:
                    serviceType = (R.string.CONST_LOB_1010 == callback_main.getLob()) ? Utils.getString(me.requireActivity(), R.string.myhkt_qrcode_servicetype_1010) : Utils.getString(me.requireActivity(), R.string.myhkt_qrcode_servicetype_mob);
                    if (binqCra.getOG3OutstandingBalanceDTO().getLastBillDate() != null) {
                        displayDate = Utils.toDateString(binqCra.getOG3OutstandingBalanceDTO().getLastBillDate().toString(), "yyyy-MM-dd hh:mm:ss.S", "dd/MM/yyyy");
                    } else {
                        displayDate = Utils.getString(me.getActivity(), R.string.summary_not_applicable);
                    }
                    DialogHelper.create7ElevenDialog(me.getActivity(), callback_main.getLob(), serviceType, displayDate,
                            callback_main.getSubnRec().acctNum, Utils.getString(me.getActivity(), R.string.qrcode_billtype_1010), amtdouble);
                    break;
                case R.string.CONST_LOB_CSP:
                    serviceType = Utils.getString(me.requireActivity(), R.string.myhkt_qrcode_servicetype_csp);
                    String billAmtCSP = "";
                    if (binqCra.getOG3OutstandingBalanceDTO().getLastBillDate() != null) {
                        displayDate = Utils.toDateString(binqCra.getOG3OutstandingBalanceDTO().getLastBillDate().toString(), "yyyy-MM-dd hh:mm:ss.S", "dd/MM/yyyy");
                    } else {
                        displayDate = Utils.getString(me.getActivity(), R.string.summary_not_applicable);
                    }
                    // Bill List Amount became string in new MyHKT, converting to double for now, NEED CONFIRMATION
                    amountPCDTV = billAmtCSP.replaceAll(",", "");
                    amtdouble = 0.0;
                    try {
                        if (callback_main.IsZombie()) {
                            amtdouble = Double.parseDouble(amountPCDTV);
                        } else {
                            amtdouble = binqCra.getOG3OutstandingBalanceDTO().getTotalOSBal();
                        }
                    } catch (NumberFormatException e) {
                        e.printStackTrace();
                    }
                    DialogHelper.create7ElevenDialog(me.getActivity(), callback_main.getLob(), serviceType, displayDate,
                            callback_main.getSubnRec().acctNum, Utils.getString(me.getActivity(), R.string.qrcode_billtype_CSP), amtdouble);
                    break;
            }
        }
    }

    @SuppressLint("NonConstantResourceId")
    public void onFPSClick(View v) {
        if (debug) Log.e(TAG, "FPS ONCLICK TRACKER");

        String serviceType = "";
        String displayDate = "";
        String accNum = "";
        String billType = "";
        String amt;
        double amtdouble = 0.0;

        if (binqCra != null) {
            switch (callback_main.getAcctAgent().getLobType()) {
                case R.string.CONST_LOB_MOB:
                case R.string.CONST_LOB_IOI:
                    if (binqCra.getOG3OutstandingBalanceDTO().getLastBillDate() != null) {
                        displayDate = Utils.toDateString(binqCra.getOG3OutstandingBalanceDTO().getLastBillDate().toString(), "yyyy-MM-dd hh:mm:ss.S", "dd/MM/yyyy");
                    } else {
                        displayDate = Utils.getString(me.requireActivity(), R.string.summary_not_applicable);
                    }

                    serviceType = (R.string.CONST_LOB_IOI == callback_main.getLob()) ? Utils.getString(me.requireActivity(), R.string.myhkt_qrcode_servicetype_1010) : Utils.getString(me.requireActivity(), R.string.myhkt_qrcode_servicetype_mob);
                    accNum = callback_main.getSubnRec().acctNum;
                    billType = Utils.getString(me.getActivity(), R.string.qrcode_bill_type_mobile);
                    amtdouble = binqCra.getOG3OutstandingBalanceDTO().getTotalOSBal();
                    break;
                case R.string.CONST_LOB_PCD:
                case R.string.CONST_LOB_TV:
                    serviceType = (R.string.CONST_LOB_PCD == callback_main.getLob()) ? Utils.getString(me.requireActivity(), R.string.myhkt_qrcode_servicetype_pcd) : Utils.getString(me.requireActivity(), R.string.myhkt_qrcode_servicetype_tv);
                    if (binqCra.getOBillPreview().getDisplayBillDate() != null) {
                        SimpleDateFormat sdf = new SimpleDateFormat(FORMAT_DDMMYYYY, Locale.US);
                        displayDate = sdf.format(binqCra.getOBillPreview().getDisplayBillDate());
                    } else {
                        displayDate = Utils.getString(me.getActivity(), R.string.summary_not_applicable);
                    }
                    // PCD Amount is in formatted string, strip , then convert back to double
                    amt = binqCra.getOBillPreview().getCurrentBalance().replaceAll(",", "");
                    amtdouble = 0.0;
                    try {
                        amtdouble = Double.parseDouble(amt);
                    } catch (NumberFormatException e) {
                        e.printStackTrace();
                    }

                    accNum = binqCra.getOBillPreview().getReceiptAcctNo();
                    billType = Utils.getString(me.getActivity(), R.string.qrcode_bill_type_pcd);
                    break;
                case R.string.CONST_LOB_LTS:
                    serviceType = Utils.getString(me.requireActivity(), R.string.myhkt_qrcode_servicetype_lts);
                    Bill[] billAry = isOBillListNotNull() ? binqCra.getOBillList().getOBillAry() : new Bill[0];
                    String billAmt = "";
                    String billDrg = "";
                    if (billAry.length > 0 && !billAry[0].getInvDate().equalsIgnoreCase("")) {
                        // Displaying the first bill info as summary
                        displayDate = Tool.formatDate(billAry[0].getInvDate(), FORMAT_DDMMYYYY);
                        billAmt = billAry[0].getInvAmt();
                        if (billAry[0].getDrgBillty() != null) {
                            billDrg = billAry[0].getDrgBillty();
                        }
                    } else {
                        displayDate = Utils.getString(me.getActivity(), R.string.summary_not_applicable);
                    }
                    // Bill List Amount became string in new MyHKT, converting to double for now, NEED CONFIRMATION
                    amt = billAmt.replaceAll(",", "");
                    amtdouble = 0.0;

                    try {
                        if (callback_main.IsZombie()) {
                            amtdouble = Double.parseDouble(amt);
                        } else {
                            amtdouble = binqCra.getOG3OutstandingBalanceDTO().getTotalOSBal();
                        }
                    } catch (NumberFormatException e) {
                        e.printStackTrace();
                    }

                    accNum = callback_main.getAcctAgent().getAcctNum();
                    billType = Utils.getString(me.getActivity(), R.string.qrcode_bill_type_lts);
                    if (billDrg != null && billDrg.equalsIgnoreCase("R")) {
                        billType = Utils.getString(me.getActivity(), R.string.qrcode_bill_type_lts_ruby);
                    }
                    break;
                case R.string.CONST_LOB_1010:
                case R.string.CONST_LOB_O2F:
                    serviceType = (R.string.CONST_LOB_1010 == callback_main.getLob()) ? Utils.getString(me.requireActivity(), R.string.myhkt_qrcode_servicetype_1010) : Utils.getString(me.requireActivity(), R.string.myhkt_qrcode_servicetype_mob);
                    if (binqCra.getOG3OutstandingBalanceDTO().getLastBillDate() != null) {
                        displayDate = Utils.toDateString(binqCra.getOG3OutstandingBalanceDTO().getLastBillDate().toString(), "yyyy-MM-dd hh:mm:ss.S", "dd/MM/yyyy");
                    } else {
                        displayDate = Utils.getString(me.getActivity(), R.string.summary_not_applicable);
                    }
                    accNum = callback_main.getSubnRec().acctNum;
                    billType = Utils.getString(me.getActivity(), R.string.qrcode_billtype_1010);
                    break;
                case R.string.CONST_LOB_CSP:
                    serviceType = Utils.getString(me.requireActivity(), R.string.myhkt_qrcode_servicetype_csp);
                    String billAmtCSP = "";
                    if (binqCra.getOG3OutstandingBalanceDTO().getLastBillDate() != null) {
                        displayDate = Utils.toDateString(binqCra.getOG3OutstandingBalanceDTO().getLastBillDate().toString(), "yyyy-MM-dd hh:mm:ss.S", "dd/MM/yyyy");
                    } else {
                        displayDate = Utils.getString(me.getActivity(), R.string.summary_not_applicable);
                    }
                    // Bill List Amount became string in new MyHKT, converting to double for now, NEED CONFIRMATION
                    amt = billAmtCSP.replaceAll(",", "");
                    amtdouble = 0.0;
                    try {
                        if (callback_main.IsZombie()) {
                            amtdouble = Double.parseDouble(amt);
                        } else {
                            amtdouble = binqCra.getOG3OutstandingBalanceDTO().getTotalOSBal();
                        }
                    } catch (NumberFormatException e) {
                        e.printStackTrace();
                    }
                    accNum = callback_main.getSubnRec().acctNum;
                    billType = Utils.getString(me.getActivity(), R.string.qrcode_billtype_CSP);
                    break;
            }
        }

        if (amtdouble > 0) {
            FPSDialog fpsDialog = new FPSDialog();
            Bundle args = new Bundle();
            if (getArguments() != null) {
                args.putBoolean(Constant.MY_MOBILE_PARENT_ACTIVITY, getArguments().getBoolean(Constant.MY_MOBILE_PARENT_ACTIVITY, false));
                args.putBoolean(Constant.MY_MOBILE_IS_1010, getArguments().getBoolean(Constant.MY_MOBILE_IS_1010, false));
                args.putString(Constant.MY_MOBILE_LOB_STRING, getArguments().getString(Constant.MY_MOBILE_LOB_STRING));
                args.putInt("lob", callback_main.getLob());
                args.putString("serviceType", serviceType);
                args.putString("billDt", displayDate);
                args.putString("acctNum", accNum);
                args.putString("billType", billType);
                args.putDouble("amtdouble", amtdouble);
                fpsDialog.setArguments(args);
                fpsDialog.show(requireFragmentManager(), "Sample Fragment");
            }
        } else {
            displayDialog(getString(R.string.myhkt_fps_invalid));
        }
    }

    @SuppressLint("NonConstantResourceId")
    public void onTapNGoClick(View v) {
        if (debug) Log.e(TAG, "TapNGo ONCLICK TRACKER");
        //merch code
        Intent intent = new Intent(this.getActivity(), TapNGoActivity.class);
        Bundle bundle = new Bundle();
        bundle.putBoolean(Constant.MY_MOBILE_PARENT_ACTIVITY, getArguments().getBoolean(Constant.MY_MOBILE_PARENT_ACTIVITY, false));
        bundle.putBoolean(Constant.MY_MOBILE_IS_1010, getArguments().getBoolean(Constant.MY_MOBILE_IS_1010, false));
        bundle.putString(Constant.MY_MOBILE_LOB_STRING, getArguments().getString(Constant.MY_MOBILE_LOB_STRING));

        String currentBalance;
        String displayDate;
        double amtdouble = 0.0;
        if (binqCra != null) {
            switch (callback_main.getAcctAgent().getLobType()) {
                case R.string.CONST_LOB_MOB:
                case R.string.CONST_LOB_IOI:
                    if (binqCra.getOG3OutstandingBalanceDTO().getLastBillDate() != null) {
                        displayDate = Utils.toDateString(binqCra.getOG3OutstandingBalanceDTO().getLastBillDate().toString(), FORMAT_TIME_STAMP, FORMAT_DDMMYYYY);
                    } else {
                        displayDate = Utils.getString(me.requireActivity(), R.string.summary_not_applicable);
                    }
                    amtdouble = binqCra.getOG3OutstandingBalanceDTO().getTotalOSBal();
                    //pass code
                    bundle.putInt("LOB", callback_main.getLob());
                    bundle.putString("SRVNUM", callback_main.getSubnRec().srvNum);
                    bundle.putString("ACCTNUM", callback_main.getSubnRec().acctNum);
                    bundle.putString("BILLDATE", displayDate);
                    bundle.putString("BILLTYPE", Utils.getString(me.requireActivity(), R.string.qrcode_bill_type_mobile));
                    bundle.putDouble("AMOUNT", amtdouble);
                    break;
                case R.string.CONST_LOB_PCD:
                case R.string.CONST_LOB_TV:
                    if (binqCra.getOBillPreview().getDisplayBillDate() != null) {
                        SimpleDateFormat sdf = new SimpleDateFormat(FORMAT_DDMMYYYY, Locale.US);
                        displayDate = sdf.format(binqCra.getOBillPreview().getDisplayBillDate());
                    } else {
                        displayDate = Utils.getString(me.requireActivity(), R.string.summary_not_applicable);
                    }
                    // PCD Amount is in formatted string, strip , then convert back to double
                    amtdouble = 0.0;
                    try {
                        currentBalance = binqCra.getOBillPreview().getCurrentBalance().replaceAll(",", "");
                        amtdouble = Double.parseDouble(currentBalance);
                    } catch (NumberFormatException e) {
                        e.printStackTrace();
                    }
                    //pass code
                    bundle.putInt("LOB", callback_main.getLob());
                    bundle.putString("SRVNUM", callback_main.getSubnRec().srvNum);
                    bundle.putString("ACCTNUM", binqCra.getOBillPreview().getReceiptAcctNo());
                    bundle.putString("BILLDATE", displayDate);
                    bundle.putString("BILLTYPE", Utils.getString(me.requireActivity(), R.string.qrcode_bill_type_pcd));
                    bundle.putDouble("AMOUNT", amtdouble);
                    break;
                case R.string.CONST_LOB_LTS:
                    Bill[] billAry = isOBillListNotNull() ? binqCra.getOBillList().getOBillAry() : new Bill[0];
                    String billDt;
                    String billAmt = "";
                    if (billAry.length > 0 && !billAry[0].getInvDate().equalsIgnoreCase("")) {
                        // Displaying the first bill info as summary
                        billDt = Tool.formatDate(billAry[0].getInvDate(), FORMAT_DDMMYYYY);
                        billAmt = billAry[0].getInvAmt();
                    } else {
                        billDt = Utils.getString(me.requireActivity(), R.string.summary_not_applicable);
                    }
                    // Bill List Amount became string in new MyHKT, converting to double for now, NEED CONFIRMATION
                    currentBalance = billAmt.replaceAll(",", "");
                    amtdouble = 0.0;
                    try {
                        if (callback_main.IsZombie()) {
                            amtdouble = Double.parseDouble(currentBalance);
                        } else {
                            amtdouble = binqCra.getOG3OutstandingBalanceDTO().getTotalOSBal();
                        }
                    } catch (NumberFormatException e) {
                        e.printStackTrace();
                    }

                    int ltsType = Utils.getLtsSrvType(callback_main.getSubnRec().tos, callback_main.getSubnRec().eyeGrp, callback_main.getSubnRec().priMob);
                    //pass code
                    bundle.putInt("LOB", callback_main.getLob());
                    bundle.putString("SRVNUM", callback_main.getSubnRec().srvNum);
                    bundle.putString("ACCTNUM", callback_main.getAcctAgent().getAcctNum());
                    bundle.putString("BILLDATE", billDt);
                    bundle.putString("BILLTYPE", Utils.getString(me.requireActivity(), R.string.qrcode_bill_type_lts));
                    bundle.putDouble("AMOUNT", amtdouble);
                    bundle.putInt("LTS_TYPE", ltsType);
                    break;
                case R.string.CONST_LOB_1010:
                case R.string.CONST_LOB_O2F:
                    if (binqCra.getOG3OutstandingBalanceDTO().getLastBillDate() != null) {
                        displayDate = Utils.toDateString(binqCra.getOG3OutstandingBalanceDTO().getLastBillDate().toString(), "yyyy-MM-dd hh:mm:ss.S", "dd/MM/yyyy");
                    } else {
                        displayDate = Utils.getString(me.requireActivity(), R.string.summary_not_applicable);
                    }

                    amtdouble = binqCra.getOG3OutstandingBalanceDTO().getTotalOSBal();
                    //pass code
                    bundle.putInt("LOB", callback_main.getLob());
                    bundle.putString("SRVNUM", callback_main.getSubnRec().srvNum);
                    bundle.putString("ACCTNUM", callback_main.getSubnRec().acctNum);
                    bundle.putString("BILLDATE", displayDate);
                    bundle.putString("BILLTYPE", Utils.getString(me.requireActivity(), R.string.qrcode_billtype_1010));
                    bundle.putDouble("AMOUNT", amtdouble);
                    break;
                case R.string.CONST_LOB_CSP:
                    if (binqCra.getOBillPreview().getDisplayBillDate() != null) {
                        SimpleDateFormat sdf = new SimpleDateFormat(FORMAT_DDMMYYYY, Locale.US);
                        displayDate = sdf.format(binqCra.getOBillPreview().getDisplayBillDate());
                    } else {
                        displayDate = Utils.getString(me.requireActivity(), R.string.summary_not_applicable);
                    }
                    amtdouble = binqCra.getOG3OutstandingBalanceDTO().getTotalOSBal();
                    //pass code
                    bundle.putInt("LOB", callback_main.getLob());
                    bundle.putString("SRVNUM", callback_main.getSubnRec().srvNum);
                    bundle.putString("ACCTNUM", callback_main.getSubnRec().acctNum);
                    bundle.putString("BILLDATE", displayDate);
                    bundle.putString("BILLTYPE", Utils.getString(me.requireActivity(), R.string.qrcode_billtype_CSP));
                    bundle.putDouble("AMOUNT", amtdouble);
                    break;
            }

            bundle.putBoolean("IS_ZOMBIE", callback_main.getSubnRec().isZombie());
            if (amtdouble > (double) 0) {
                intent.putExtras(bundle);
                startActivity(intent);
                me.getActivity().overridePendingTransition(R.anim.right_slide_in, R.anim.left_slide_out);
            } else {
                displayDialog(getString(R.string.myhkt_tapngo_invalid));
            }
        }
    }

    public boolean isOBillListNotNull() {
        return binqCra != null && binqCra.getOBillList() != null;
    }

    //bill list onClick
    public void onBillClick1(View v) {
        if (debug) Log.e(TAG, "BillClick1 ONCLICK TRACKER");
        if (isThinBill1) {
            DialogHelper.createSimpleDialog(me.getActivity(), getString(R.string.myhkt_holdbill));
        } else {
            clickBill = 0;
            chkBillCount = 0;
            doCheckBillStsLoop(isOBillListNotNull() ? binqCra.getOBillList().getOBillAry()[0].getInvDate() : null, false);
            me.saveAccountHelper.updateFlag(ClnEnv.getSessionLoginID(), binqCra.getIAcct().getAcctNum(), 0);
            billDate = isOBillListNotNull() ? binqCra.getOBillList().getOBillAry()[0].getInvDate() : null;
        }
    }

    public void onBillClick2(View v) {
        if (debug) Log.e(TAG, "BillClick2 ONCLICK TRACKER");
        if (isThinBill2) {
            displayDialog(Utils.getString(me.requireActivity(), R.string.myhkt_holdbill));
        } else {
            clickBill = 1;
            chkBillCount = 0;
            doCheckBillStsLoop(isOBillListNotNull() ? binqCra.getOBillList().getOBillAry()[1].getInvDate() : null, false);
            billDate = isOBillListNotNull() ? binqCra.getOBillList().getOBillAry()[1].getInvDate() : null;
        }
    }

    public void onBillClick3(View v) {
        if (debug) Log.e(TAG, "BillClick3 ONCLICK TRACKER");
        if (isThinBill3) {
            displayDialog(Utils.getString(me.requireActivity(), R.string.myhkt_holdbill));
        } else {
            clickBill = 2;
            chkBillCount = 0;
            doCheckBillStsLoop(isOBillListNotNull() ? binqCra.getOBillList().getOBillAry()[2].getInvDate() : null, false);
            billDate = isOBillListNotNull() ? binqCra.getOBillList().getOBillAry()[2].getInvDate() : null;
        }
    }

    private void doGetBill() {
        String loginId;
        if (!ClnEnv.isLoggedIn()) {
            loginId = "";
        } else {
            loginId = Objects.requireNonNull(ClnEnv.getQualSvee()).getSveeRec().loginId;
        }

        BinqCra binqCra = new BinqCra();
        binqCra.setILoginId(loginId);
        binqCra.setISubnRec(callback_main.getSubnRec().copyMe());

        APIsManager.doGetBill(me, binqCra);
    }

    private void doCheckBillStsLoop(String billDate, Boolean isLastInLoop) {
        BinqCra chkStsBinqCra = new BinqCra();
        chkStsBinqCra.setILoginId(ClnEnv.isLoggedIn() ? Objects.requireNonNull(ClnEnv.getQualSvee()).getSveeRec().loginId : "");
        chkStsBinqCra.setISubnRec(callback_main.getSubnRec().copyMe());
        chkStsBinqCra.setIAcct(isOBillListNotNull() ? binqCra.getOBillList().getIAcct() : null);
        chkStsBinqCra.setIBillDate(billDate);

        APIsManager.doCheckBillSts(me, chkStsBinqCra, isLastInLoop);
    }

    @SuppressLint("NonConstantResourceId")
    private void initMobBillSummary() {
        //tracker
        String totalChg;
        String dueDate;
        String currentBal;

        //load data
        SmallTextCell cell2 = new SmallTextCell(getResString(R.string.myhkt_summary_mostrect), "");
        cell2.setLeftPadding(basePadding);
        cell2.setRightPadding(basePadding);

        if (R.string.CONST_LOB_PCD != callback_main.getLob() && R.string.CONST_LOB_TV != callback_main.getLob()) {
            cellList.add(cell2);
        }

        Cell cell1a = null; // receipt acct no (PCDTV)
        SmallTextCell cell3 = null; //total charge/ amt due(LTS)/ latest bill amt(PCDTV)/ no bill sum remarks
        SmallTextCell cell4 = null; //due date, bill month(LTS)
        SmallTextCell cell5 = null; //current bal, total acct bal(CSIM)
        SmallTextCell cell6 = null; //overdue amt(CSIM)
        SmallTextCell cell6a = null; //Service status (eg. "Suspended")
        SmallTextCell cell6b = null; //Service status remarks
        DetailBtnCell cell7 = null; // payment method button
        SmallTextCell cell7a = null; // Only show "Pay Bills: "
        FourBtnCell cell8 = null; // 7-11, PPS, Tap N Go button
        DetailBtnCell txtRecentBillBtnUpdate = null; // Update bill address button
        SmallTextCell txtRecentBill; // Only show "Recent Bills: "
        SmallTextCell cell9Message = null; // Only show "Recent Bills: "
        DetailBtnCell cell10 = null; //  Update payment method button
        SmallTextCell cell11 = null; // Current Balance

        String[] clickAry8;
        int[] resAry8;

        String[] clickAry8a;
        int[] resAry8a;

        //mandatory cells
        Cell cellLine = new Cell(Cell.LINE); //seperator line
        cellLine.setLeftPadding(basePadding);
        cellLine.setRightPadding(basePadding);
        String[] clickAry7 = {"onPayClick"};
        String[] clickAry9 = {"onUpdateClick"}; //bill info button
        String[] clickAryEmpty = {""};

        txtRecentBill = new SmallTextCell(getResString(R.string.myhkt_summary_recentbill), "", R.color.black);
        if (isThinBill()) {
            cell3 = new SmallTextCell(getResString(R.string.myhkt_holdbill), "");
        } else {
            //prepare cell by LOB
            switch (callback_main.getLob()) {
                case R.string.CONST_LOB_MOB:
                case R.string.CONST_LOB_IOI:
                case R.string.CONST_LOB_CSP:
                    if (R.string.CONST_LOB_IOI == callback_main.getLob()) {
                        cell9Message = new SmallTextCell(getResString(R.string.summary_recent_bills_note_ioi), "");
                        callback_livechat.setModuleId(getResString(ClnEnv.isMyMobFlag() ? R.string.MODULE_IOI_MM_BILL : R.string.MODULE_IOI_BILL));
                    } else if (R.string.CONST_LOB_MOB == callback_main.getLob()) {
                        cell9Message = new SmallTextCell(getResString(R.string.summary_recent_bills_note_mob), "");
                        callback_livechat.setModuleId(getResString(ClnEnv.isMyMobFlag() ? R.string.MODULE_MOB_MM_BILL : R.string.MODULE_MOB_BILL));
                    } else if (R.string.CONST_LOB_CSP == callback_main.getLob()) {
                        cell9Message = new SmallTextCell(getResString(R.string.summary_recent_bills_note_csp), "");
                        callback_livechat.setModuleId(getResString(ClnEnv.isMyMobFlag() ? R.string.MODULE_CLUBSIM_MM_BILL : R.string.MODULE_CLUBSIM_BILL));
                    } else {
                        callback_livechat.setModuleId(getResString(ClnEnv.isMyMobFlag() ? R.string.MODULE_MOB_MM_PLAN : R.string.MODULE_MOB_PLAN));
                    }
                    if (binqCra != null) {
                        if (binqCra.getOG3OutstandingBalanceDTO() != null) {
                            if (binqCra.getOBillList().getOBillAry().length > 0) {
                                totalChg = Utils.convertStringToPrice(binqCra.getOBillList().getOBillAry()[0].getInvAmt());
                                cell3 = new SmallTextCell(getResString(R.string.bill_total_charge), totalChg);
                            }
                            //If due Date is null , dueDate item will not show
                            dueDate = binqCra.getOG3OutstandingBalanceDTO().getPymtDueDate() != null ?
                                    binqCra.getOG3OutstandingBalanceDTO().getPymtDueDate().toString() : "";
                            if (!dueDate.isEmpty()) {
                                try {
                                    SimpleDateFormat sdf = new SimpleDateFormat(FORMAT_TIME_STAMP, Locale.ENGLISH);
                                    Date d = sdf.parse(dueDate);
                                    sdf = new SimpleDateFormat(FORMAT_DDMMYYYY, Locale.ENGLISH);
                                    dueDate = sdf.format(d);
                                } catch (Exception e) {
                                    dueDate = getResString(R.string.summary_not_applicable);
                                }
                                cell4 = new SmallTextCell(getResString(R.string.summary_due_date), dueDate);
                            }

                            currentBal = Utils.convertStringToPrice(Utils.convertDoubleToString(binqCra.getOG3OutstandingBalanceDTO().getTotalOSBal(), 2));
                            cell5 = new SmallTextCell(getResString(R.string.summary_current_balance), currentBal);

                            //Service Status
                            if (binqCra.getOG3ServiceCallBarStatusWS() != null) {
                                G3ServiceCallBarStatusDTO g3ServiceCallBarStatusDTO = binqCra.getOG3ServiceCallBarStatusWS().getG3ServiceCallBarStatusDTO();
                                if ("Y".equalsIgnoreCase(g3ServiceCallBarStatusDTO.getShowServiceStatus())) {
                                    if ("zh".equalsIgnoreCase(ClnEnv.getAppLocale(requireActivity().getBaseContext()))) {
                                        cell6a = new SmallTextCell(getResString(R.string.summary_service_status), g3ServiceCallBarStatusDTO.getServiceStatusChi());
                                    } else {
                                        cell6a = new SmallTextCell(getResString(R.string.summary_service_status), g3ServiceCallBarStatusDTO.getServiceStatusEng());
                                    }
                                }
                                if ("Y".equalsIgnoreCase(g3ServiceCallBarStatusDTO.getHasAlertMessage())) {
                                    boolean isZh = "zh".equalsIgnoreCase(ClnEnv.getAppLocale(requireActivity().getBaseContext()));
                                    if ("1".equalsIgnoreCase(g3ServiceCallBarStatusDTO.getAlertLevel())) {
                                        cell6b = new SmallTextCell(isZh ? g3ServiceCallBarStatusDTO.getAlertMessageChi() : g3ServiceCallBarStatusDTO.getAlertMessageEng(), "", R.color.blue_alert_message);
                                    } else if ("2".equalsIgnoreCase(g3ServiceCallBarStatusDTO.getAlertLevel())) {
                                        cell6b = new SmallTextCell(isZh ? g3ServiceCallBarStatusDTO.getAlertMessageChi() : g3ServiceCallBarStatusDTO.getAlertMessageEng(), "", R.color.red_alert_message);
                                    } else {
                                        cell6b = new SmallTextCell(isZh ? g3ServiceCallBarStatusDTO.getAlertMessageChi() : g3ServiceCallBarStatusDTO.getAlertMessageEng(), "", R.color.black);
                                    }
                                }
                            }
                            //service status end
                            clickAry8 = new String[]{"onPPSClick", "onSevenClick", "onTapNGoClick", "onFPSClick"};
                            resAry8 = new int[]{R.drawable.pps_small, R.drawable.seven_small};
                            cell7 = new DetailBtnCell(getResString(R.string.myhkt_summary_paybill), getResString(R.string.myhkt_summary_paymentmethod), getBtnImage(), clickAry7, false);
                            cell8 = new FourBtnCell("", "", getDrawableArray(binqCra.getISubnRec().lob, binqCra), getClickArray(binqCra.getISubnRec().lob, binqCra));
                        }
                    }
                    break;
                case R.string.CONST_LOB_1010:
                case R.string.CONST_LOB_O2F:
                    if (R.string.CONST_LOB_1010 == callback_main.getAcctAgent().getLobType()) {
                        callback_livechat.setModuleId(getResString(ClnEnv.isMyMobFlag() ? R.string.MODULE_101_MM_BILL : R.string.MODULE_101_BILL));
                    } else {
                        callback_livechat.setModuleId(getResString(ClnEnv.isMyMobFlag() ? R.string.MODULE_O2F_MM_BILL : R.string.MODULE_O2F_BILL));
                    }
                    if (binqCra != null) {
                        if (binqCra.getOG3OutstandingBalanceDTO() != null) {
                            String overDueAmt = Utils.convertStringToPrice(Utils.convertDoubleToString(binqCra.getOG3OutstandingBalanceDTO().getOverdueAmt(), 2));
                            cell6 = new SmallTextCell(getResString(R.string.myhkt_summary_overdue_amount), overDueAmt);

                            totalChg = Utils.convertStringToPrice(Utils.convertDoubleToString(binqCra.getOG3OutstandingBalanceDTO().getCurrOSBal(), 2));
                            cell3 = new SmallTextCell(getResString(R.string.csl_current_amount), totalChg);

                            //If dute Date is null , dueDate item will not show
                            dueDate = binqCra.getOG3OutstandingBalanceDTO().getPymtDueDate() != null ?
                                    binqCra.getOG3OutstandingBalanceDTO().getPymtDueDate().toString() : "";
                            if (!dueDate.isEmpty()) {
                                try {
                                    SimpleDateFormat sdf = new SimpleDateFormat(FORMAT_TIME_STAMP, Locale.ENGLISH);
                                    Date d = sdf.parse(dueDate);
                                    sdf = new SimpleDateFormat(FORMAT_DDMMYYYY, Locale.ENGLISH);
                                    dueDate = sdf.format(d);
                                } catch (Exception e) {
                                    dueDate = getResString(R.string.summary_not_applicable);
                                }
                                cell4 = new SmallTextCell(getResString(R.string.summary_due_date), dueDate);
                            }
                            currentBal = Utils.convertStringToPrice(Utils.convertDoubleToString(binqCra.getOG3OutstandingBalanceDTO().getTotalOSBal(), 2));
                            cell5 = new SmallTextCell(getResString(R.string.csl_total_balance), currentBal);
                            //20170320 Combine Tap&Go button and PPS button on the same row
                            clickAry8 = new String[]{"onPPSClick", "onSevenClick", "onTapNGoClick", "onFPSClick"};
                            resAry8 = new int[]{R.drawable.pps_small};
                            //no pps or update bill info allowed for T / V account type  //postponed
                            cell7 = new DetailBtnCell(getResString(R.string.myhkt_summary_paybill), getResString(R.string.myhkt_summary_paymentmethod), getBtnImage(), clickAry7, false);
                            cell8 = new FourBtnCell("", "", getDrawableArray(binqCra.getISubnRec().lob, binqCra), getClickArray(binqCra.getISubnRec().lob, binqCra));
                        }
                    }
                    break;
                case R.string.CONST_LOB_PCD:
                case R.string.CONST_LOB_TV:
                    if (R.string.CONST_LOB_PCD == callback_main.getAcctAgent().getLobType()) {
                        //IF LOB is PCD and isBillByAgent is true use MODULE_PCD_BILL_4MOBCS otherwise MODULE_PCD_BILL
                        if ("PCD".equalsIgnoreCase(callback_main.getSubnRec().lob) && callback_main.getSubnRec().isBillByAgent()) {
                            callback_livechat.setModuleId(getResString(R.string.MODULE_PCD_BILL_4MOBCS));
                        } else {
                            callback_livechat.setModuleId(getResString(R.string.MODULE_PCD_BILL));
                        }
                    } else {
                        callback_livechat.setModuleId(getResString(R.string.MODULE_TV_BILL));
                    }

                    if (binqCra != null) {
                        String receiptAcctNum;
                        dueDate = binqCra.getOBillPreview().getDueDate();
                        if (binqCra.getOBillPreview().getReceiptAcctNo() != null) {
                            receiptAcctNum = Tool.formatAcctNum(binqCra.getOBillPreview().getReceiptAcctNo());
                            cell1a = new SmallTextCell(getResString(R.string.pcd_summary_recacct_num), receiptAcctNum);
                        }
                        if (dueDate != null) {
                            if (dueDate.toUpperCase().contains("DUE")) {
                                dueDate = Utils.getString(me.requireActivity(), R.string.due_on_immediate);
                            } else {
                                if (dueDate.contains("T")) {
                                    dueDate = dueDate.substring(0, dueDate.indexOf("T"));
                                    dueDate = Utils.toDateString(dueDate, FORMAT_YYYY_MM_DD, FORMAT_DDMMYYYY);
                                } else {
                                    // convert Date format in MyHKT
                                    dueDate = Utils.toDateString(dueDate, FORMAT_DD_MMM_YYYY, FORMAT_DDMMYYYY);
                                }
                            }
                            cell4 = new SmallTextCell(getResString(R.string.summary_due_date), dueDate);
                        }

                        if (binqCra.getOBillList().getOBillAry() != null && binqCra.getOBillList().getOBillAry().length > 0) {
                            totalChg = Utils.convertStringToPrice(binqCra.getOBillList().getOBillAry()[0].getInvAmt());
                            cell3 = new SmallTextCell(getResString(R.string.myhkt_summary_latestbill), totalChg);

                        }
                        if (binqCra.getOBillPreview().getCurrentBalance() != null) {
                            currentBal = Utils.convertStringToPrice(binqCra.getOBillPreview().getCurrentBalance());
                            cell5 = new SmallTextCell(getResString(R.string.pcd_summary_current_balance), currentBal);
                            clickAry8 = new String[]{"onPPSClick", "onSevenClick", "onTapNGoClick", "onFPSClick"};
                            cell7 = new DetailBtnCell(getResString(R.string.myhkt_summary_paybill), getResString(R.string.myhkt_summary_paymentmethod), getBtnImage(), clickAry7, false);
                            cell8 = new FourBtnCell("", "", getDrawableArray(binqCra.getISubnRec().lob, binqCra), getClickArray(binqCra.getISubnRec().lob, binqCra));
                        }
                    }
                    break;
                case R.string.CONST_LOB_LTS:
                    callback_livechat.setModuleId(getResString(R.string.MODULE_LTS_BILL));

                    Bill[] billAry = isOBillListNotNull() ? binqCra.getOBillList().getOBillAry() : new Bill[0];
                    if (billAry.length > 0 && !billAry[0].getDueDate().equalsIgnoreCase("") && !isThinBill()) {
                        totalChg = Utils.convertStringToPrice(billAry[0].getInvAmt());
                        String billMonth = toLocaleDateString(billAry[0].getInvDate(), FORMAT_YYYYMMDD, FORMAT_MMM_YYYY);
                        String currentBalance = Utils.convertStringToPrice(Utils.convertDoubleToString(binqCra.getOG3OutstandingBalanceDTO().getTotalOSBal(), 2));

                        cell4 = new SmallTextCell(getResString(R.string.myhkt_summary_amtdue), totalChg);
                        cell3 = new SmallTextCell(getResString(R.string.myhkt_summary_billmonth), billMonth);

                        String dueDateTxt = billAry[0].getDueDate();
                        if (dueDateTxt != null && !"".equalsIgnoreCase(dueDateTxt) && !"99999999".equalsIgnoreCase(dueDateTxt)) {
                            if (dueDateTxt.toUpperCase().contains("DUE") || "00000000".equalsIgnoreCase(dueDateTxt)) {
                                dueDateTxt = Utils.getString(me.requireActivity(), R.string.due_on_immediate);
                            } else {
                                dueDateTxt = Utils.toDateString(dueDateTxt, FORMAT_YYYYMMDD, FORMAT_DDMMYYYY);
                            }
                            cell5 = new SmallTextCell(getResString(R.string.pcd_summary_due_date), dueDateTxt);
                        }
                        if (!callback_main.IsZombie())
                            cell11 = new SmallTextCell(getResString(R.string.summary_current_balance), currentBalance);

                        cell7 = new DetailBtnCell(getResString(R.string.myhkt_summary_paybill), getResString(R.string.myhkt_summary_paymentmethod), getBtnImage(), clickAry7, false);
                        clickAry8 = new String[]{"onPPSClick", "onSevenClick", "onTapNGoClick", "onFPSClick"};
                        cell8 = new FourBtnCell("", "", getDrawableArray(binqCra.getISubnRec().lob, binqCra), getClickArray(binqCra.getISubnRec().lob, binqCra));
                    }
                    String[] clickAry10 = {"onUpdatePayMethodClick"}; //bill info button
                    cell10 = new DetailBtnCell("", getResString(R.string.myhkt_summary_updatePayMethod), getBtnImage(), clickAry10, false);
                    break;
            }
        }

        if (cell1a != null) {
            cell1a.setLeftPadding(basePadding);
            cell1a.setRightPadding(basePadding);
            cellList.add(cell1a);
        }
        if (cell6 != null) {
            cell6.setLeftPadding(basePadding);
            cell6.setRightPadding(basePadding);
            cellList.add(cell6);
        }
        if (cell3 != null) {
            cell3.setLeftPadding(basePadding);
            cell3.setRightPadding(basePadding);
            cellList.add(cell3);
        }
        if (cell4 != null) {
            cell4.setLeftPadding(basePadding);
            cell4.setRightPadding(basePadding);
            cellList.add(cell4);
        }
        if (cell5 != null) {
            cell5.setLeftPadding(basePadding);
            cell5.setRightPadding(basePadding);
            cellList.add(cell5);
        }
        if (cell11 != null) {
            cell11.setLeftPadding(basePadding);
            cell11.setRightPadding(basePadding);
            cellList.add(cell11);
        }
        if (cell6a != null) {
            cell6a.setLeftPadding(basePadding);
            cell6a.setRightPadding(basePadding);
            cellList.add(cell6a);
        }
        if (cell6b != null) {
            cell6b.setLeftPadding(basePadding);
            cell6b.setRightPadding(basePadding);
            cellList.add(cell6b);
        }
        cellList.add(cellLine);
        if (cell7 != null) {
            cell7.setLeftPadding(basePadding);
            cell7.setRightPadding(basePadding);
            cellList.add(cell7);
        }
        if (cell7a != null) {
            cell7a.setLeftPadding(basePadding);
            cell7a.setRightPadding(basePadding);
            cellList.add(cell7a);
        }
        if (cell8 != null) {
            cell8.setLeftPadding(basePadding);
            cell8.setRightPadding(basePadding);
            cellList.add(cell8);
        }
        cellList.add(cellLine);
        if (txtRecentBillBtnUpdate != null) {
            txtRecentBillBtnUpdate.setLeftPadding(basePadding);
            txtRecentBillBtnUpdate.setRightPadding(basePadding);
            cellList.add(txtRecentBillBtnUpdate);
        }
        if (txtRecentBill != null) {
            txtRecentBill.setLeftPadding(basePadding);
            txtRecentBill.setRightPadding(basePadding);
            cellList.add(txtRecentBill);
        }

        if (cell9Message != null) {
            cell9Message.setLeftPadding(basePadding + basePadding);
            cell9Message.setRightPadding(basePadding);
            cellList.add(cell9Message);
        }

        if (cell10 != null && !callback_main.IsZombie()) {
            cell10.setLeftPadding(basePadding);
            cell10.setRightPadding(basePadding);
            cellList.add(cell10);
        }
        initBillList();
        cellViewAdapter.setView(lobString, frame, cellList);
    }

    private void initBillList() {
        Cell cell11 = null; // bill pdfs
        Cell cell11a = null; //bill remark 1
        Cell cell11b = null; //bill remark 2

        //init bill list
        Bill[] bills = null;
        int emptyBillDtCount = 0;

        if (isOBillListNotNull()) {
            bills = binqCra.getOBillList().getOBillAry();
        }

        if (bills != null && bills.length > 0) {
            String[] textAry10 = new String[bills.length];
            String[] clickAry10;

            List<String> clickList = new ArrayList<>();
            for (int i = 0; i < bills.length; i++) {
                if (!bills[i].getInvDate().equalsIgnoreCase("")) {
                    textAry10[i] = Utils.toDateString(bills[i].getInvDate(), getString(R.string.input_date_format), "MM/yyyy");
                    switch (i) {
                        case 0:
                            clickList.add("onBillClick1");
                            isThinBill1 = !bills[0].getWithheld().trim().isEmpty();
                            break;
                        case 1:
                            clickList.add("onBillClick2");
                            isThinBill2 = !bills[1].getWithheld().trim().isEmpty();
                            break;
                        case 2:
                            clickList.add("onBillClick3");
                            isThinBill3 = !bills[2].getWithheld().trim().isEmpty();
                            break;
                    }
                } else emptyBillDtCount++;
            }
            clickAry10 = clickList.toArray(new String[bills.length]);

            //init bill images
            int[] imageAry;
            imageAry = getBillImages(isNewBill(), bills);
            cell11 = new Cell(Cell.BTNS3, clickAry10, textAry10, R.color.white, imageAry);

            //update new bill icon
            try {
                if (isOBillListNotNull() && binqCra.getOBillList().getOBillAry() != null && binqCra.getOBillList().getOBillAry().length > 0) {
                    if (ClnEnv.isMyMobFlag()) {
                        myMobileAccountHelper = MyMobileAccountHelper.getInstance(getActivity());
                        saveAccountHelper = SaveAccountHelper.getInstance(getActivity());
                        if (callback_main.getAcctAgent().getLatest_bill() == null
                                || "".equals(callback_main.getAcctAgent().getLatest_bill())
                                || callback_main.CompareLastBillDate(callback_main.getAcctAgent().getLatest_bill(), binqCra.getOBillList().getOBillAry()[0].getInvDate())
                                || saveAccountHelper.getFlagByLoginIDAndAcctNum(ClnEnv.getSessionLoginID(), callback_main.getAcctAgent().getAcctNum())) {
                            myMobileAccountHelper.updateLastBillDt(callback_main.getAcctAgent().getSrvNum(), binqCra.getOBillList().getOBillAry()[0].getInvDate());
                            saveAccountHelper.updateLastBillDate(ClnEnv.getSessionLoginID(), callback_main.getAcctAgent().getAcctNum(), binqCra.getOBillList().getOBillAry()[0].getInvDate());
                            callback_main.getAcctAgent().setLatest_bill(binqCra.getOBillList().getOBillAry()[0].getInvDate());
                        }
                    } else {
                        //update bill indicator
                        if (callback_main.getAcctAgent().isLive() && isOBillListNotNull()) {
                            saveAccountHelper = SaveAccountHelper.getInstance(getActivity());
                            if (saveAccountHelper.getFlagByLoginIDAndAcctNum(ClnEnv.getSessionLoginID(), callback_main.getAcctAgent().getAcctNum())) {
                                saveAccountHelper.updateLastBillDate(ClnEnv.getSessionLoginID(), callback_main.getAcctAgent().getAcctNum(), binqCra.getOBillList().getOBillAry()[0].getInvDate());
                            }
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (bills == null || emptyBillDtCount == bills.length) {
            //no bill
            if (R.string.CONST_LOB_LTS == callback_main.getLob() || R.string.CONST_LOB_CSP == callback_main.getLob()) {
                cell11a = new SmallTextCell(getResString(R.string.BILTM_NOBILL), "");
                cell11a.setBgColorId(R.color.white);
            } else if (R.string.CONST_LOB_PCD == callback_main.getLob() || R.string.CONST_LOB_TV == callback_main.getLob()) {
                cell11a = new SmallTextCell(getResString(R.string.BIIMM_NOBILL), "");
                cell11a.setBgColorId(R.color.white);
            } else if (R.string.CONST_LOB_MOB == callback_main.getLob() || R.string.CONST_LOB_IOI == callback_main.getLob()) {
                cell11a = new SmallTextCell(getResString(R.string.BIMBM_NOBILL), "");
                cell11a.setBgColorId(R.color.white);
            } else if (R.string.CONST_LOB_1010 == callback_main.getLob() || R.string.CONST_LOB_O2F == callback_main.getLob()) {
                cell11a = new SmallTextCell(getResString(R.string.BIMBM_NOBILL), "");
                cell11a.setBgColorId(R.color.white);
            }
        }

        if (callback_main.IsZombie()) {
            cell11b = new SmallTextCell(getResString(R.string.zombie_lastbills_housekeep), "", R.color.red);
            cell11b.setBgColorId(R.color.white);
        }

        if (cell11 != null) {
            cell11.setLeftPadding(basePadding);
            cell11.setRightPadding(basePadding);
            cellList.add(cell11);
        }
        if (cell11a != null) {
            cell11a.setLeftPadding(basePadding);
            cell11a.setRightPadding(basePadding);
            cellList.add(cell11a);
        }
        if (cell11b != null) {
            cell11b.setLeftPadding(basePadding);
            cell11b.setRightPadding(basePadding);
            cellList.add(cell11b);
        }
    }

    protected final boolean isThinBill() {
        Bill[] billAry = isOBillListNotNull() ? binqCra.getOBillList().getOBillAry() : null;
        return billAry != null && billAry.length > 0 && !billAry[0].getWithheld().trim().isEmpty();
    }

    private boolean isNewBill() {
        try {
            if (callback_main.isMyMobAcct()) {
                if (isOBillListNotNull() && binqCra.getOBillList().getOBillAry() != null && binqCra.getOBillList().getOBillAry().length > 0) {
                    //check new bill
                    if (callback_main.getAcctAgent().getLatest_bill() == null || "".equalsIgnoreCase(callback_main.getAcctAgent().getLatest_bill())) {
                        return true;
                    } else {
                        return callback_main.CompareLastBillDate(me.callback_main.getAcctAgent().getLatest_bill(), binqCra.getOBillList().getOBillAry()[0].getInvDate());
                    }
                }
            } else {
                if (ClnEnv.getPref(me.requireActivity().getApplicationContext(), me.getString(R.string.CONST_PREF_NEWBILLIND_FLAG), true) && !callback_main.IsZombie()) {
                    // make sure the BillAry Not null and empty Array
                    if (isOBillListNotNull() && binqCra.getOBillList().getOBillAry() != null && binqCra.getOBillList().getOBillAry().length > 0) {
                        try {
                            // Check Database only if the bill date returned
                            if (!"".equalsIgnoreCase(binqCra.getOBillList().getOBillAry()[0].getInvDate())) {
                                me.saveAccountHelper = SaveAccountHelper.getInstance(me.getActivity());
                                // Get lastBillDate from database
                                String lastBillDate = me.saveAccountHelper.getDateByLoginIDAndAcctNum(ClnEnv.getSessionLoginID(), binqCra.getIAcct().getAcctNum());
                                if (lastBillDate == null) {
                                    me.saveAccountHelper.insert(ClnEnv.getSessionLoginID(), binqCra.getIAcct().getAcctNum());
                                } else if (callback_main.CompareLastBillDate(lastBillDate, binqCra.getOBillList().getOBillAry()[0].getInvDate())) {
                                    me.saveAccountHelper.updateFlag(ClnEnv.getSessionLoginID(), binqCra.getIAcct().getAcctNum());
                                }
                                //check new bill
                                saveAccountHelper = SaveAccountHelper.getInstance(me.getActivity());
                                return saveAccountHelper.getFlagByLoginIDAndAcctNum(ClnEnv.getSessionLoginID(), callback_main.getAcctAgent().getAcctNum());
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        return false;
    }

    private int[] getBillImages(boolean isNewBill, Bill[] bills) {
        int[] imageAry = new int[3];
        boolean isBill1Show = bills.length > 0 && !bills[0].getInvDate().equalsIgnoreCase("");
        boolean isBill2Show = bills.length > 1 && !bills[1].getInvDate().equalsIgnoreCase("");
        boolean isBill3Show = bills.length > 2 && !bills[2].getInvDate().equalsIgnoreCase("");

        if (ClnEnv.isMyMobFlag()) {
            if (R.string.CONST_LOB_1010 == callback_main.getLob() || R.string.CONST_LOB_IOI == callback_main.getLob()) {
                imageAry[0] = isNewBill && isBill1Show ? R.drawable.btn_bills_1010_new : R.drawable.btn_bills_1010;
                imageAry[1] = isBill2Show ? R.drawable.btn_bills_1010 : -1;
                imageAry[2] = isBill3Show ? R.drawable.btn_bills_1010 : -1;
            } else if (R.string.CONST_LOB_O2F == callback_main.getLob() || R.string.CONST_LOB_MOB == callback_main.getLob()) {
                imageAry[0] = isNewBill && isBill1Show ? R.drawable.btn_bills_csl_new : R.drawable.btn_bills_csl;
                imageAry[1] = isBill2Show ? R.drawable.btn_bills_csl : -1;
                imageAry[2] = isBill3Show ? R.drawable.btn_bills_csl : -1;
            } else if (R.string.CONST_LOB_CSP == callback_main.getLob()) {
                imageAry[0] = isNewBill && isBill1Show ? R.drawable.btn_bills_clubsim_new : R.drawable.btn_bills_clubsim;
                imageAry[1] = isBill2Show ? R.drawable.btn_bills_clubsim : -1;
                imageAry[2] = isBill3Show ? R.drawable.btn_bills_clubsim : -1;
            } else {
                // Added default bill image in case LOB is not specified
                imageAry[0] = isNewBill && isBill1Show ? R.drawable.btn_bills_new : R.drawable.btn_bills;
                imageAry[1] = isBill2Show ? R.drawable.btn_bills : -1;
                imageAry[2] = isBill3Show ? R.drawable.btn_bills : -1;
            }
        } else {
            imageAry[0] = isNewBill && isBill1Show ? R.drawable.btn_bills_new : R.drawable.btn_bills;
            imageAry[1] = isBill2Show ? R.drawable.btn_bills : -1;
            imageAry[2] = isBill3Show ? R.drawable.btn_bills : -1;
        }
        return imageAry;
    }

    private int getBtnImage() { //button theme for update, bill info, payment..
        int image;
        if (ClnEnv.isMyMobFlag()) {
            if (R.string.CONST_LOB_1010 == callback_main.getLob()) {
                image = R.drawable.btn_details_1010;
            } else if (R.string.CONST_LOB_O2F == callback_main.getLob()) {
                image = R.drawable.btn_details_csl;
            } else if (R.string.CONST_LOB_CSP == callback_main.getLob()) {
                image = R.drawable.btn_details_csp;
            } else {
                image = R.drawable.btn_details_csl;
            }
        } else {
            image = R.drawable.btn_details;
        }
        return image;
    }

    private void startDownloadPdf() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            String lang = isZh ? "zh" : "en";
            APIsManager.doDownloadPDF(me, binqCra, callback_main.getLobString(), callback_main.getSubnRec().cusNum, clickBill, lang);
            return;
        }

        if (RuntimePermissionUtil.isWriteExternalStoragePermissionGranted(getActivity())) {
            if (binqCra != null) {
                String lang = isZh ? "zh" : "en";
                APIsManager.doDownloadPDF(me, binqCra, callback_main.getLobString(), callback_main.getSubnRec().cusNum, clickBill, lang);
            }
        } else {
            if (ClnEnv.getPref(getActivity(), Constant.FILE_STORAGE_PERMISSION_DENIED, false)) {
                displayDialog(requireActivity().getString(R.string.permission_denied_setting_enabled));
            } else {
                RuntimePermissionUtil.requestFileStoragePermission(this);
            }
        }
    }

    // Format BillList Date
    private String toLocaleDateString(String dateString, String patternString, String outPatternString) {
        try {
            SimpleDateFormat sdf = new SimpleDateFormat(patternString, Locale.US);
            Date d = sdf.parse(dateString);
            sdf = new SimpleDateFormat(outPatternString, Locale.US);
            return sdf.format(d);
        } catch (Exception e) {
            if (debug)
                Log.e("ServiceLTSActivity", "toLocaleDateString error: dateString=" + dateString + ", patternString=" + patternString + ", Exception:" + e);
            return dateString;
        }
    }

    @Override
    public void onSuccess(APIsResponse response) {
        if (APIsManager.BILL.equals(response.getActionTy())) {
            binqCra = (BinqCra) response.getCra();
            initBasicUI();
            initMobBillSummary();
        } else if (APIsManager.CHK_BILL.equals(response.getActionTy())) {
            startDownloadPdf();
        } else if (APIsManager.AO_AUTH.equals(response.getActionTy())) {
            addOnCra = (AddOnCra) response.getCra();
            addOnCra.getOSubnRec().ivr_pwd = addOnCra.getISubnRec().ivr_pwd;

            callback_main.setSubscriptionRec(addOnCra.getOSubnRec());
            callback_main.getAcctAgent().setSubnRec(addOnCra.getOSubnRec());
            doGetBill();
        }
    }

    @Override
    public void onFail(APIsResponse response) {
        if (APIsManager.CHK_BILL.equals(response.getActionTy()) && RC.BINQ_BINRDY.equalsIgnoreCase(response.getReply().getCode())) {
            if (me.chkBillCount < 10) {
                me.chkBillCount++;
                try {
                    doCheckBillStsLoop(billDate, me.chkBillCount == 9);
                } catch (Exception e) {
                    // fail to sleep
                    me.chkBillCount = 10;
                }
            } else {
                me.displayDialog(Utils.getString(requireActivity(), R.string.BINQ_BINRDY));
            }
            // General Error Message
        } else if (response.getMessage() != null && !"".equals(response.getMessage())) {
            DialogHelper.createSimpleDialog(getActivity(), response.getMessage());
        } else if (RC.TCSESS_MM.equalsIgnoreCase(response.getReply().getCode())) {
            BaseActivity.ivSessDialog();
        } else if (RC.AO_PSPH_FAIL.equalsIgnoreCase(response.getReply().getCode())) {
            recallDoAuth(me, callback_main.getSubnRec().copyMe());
        } else {
            if (R.string.CONST_LOB_MOB == callback_main.getLob() || R.string.CONST_LOB_IOI == callback_main.getLob() || R.string.CONST_LOB_1010 == callback_main.getLob() || R.string.CONST_LOB_O2F == callback_main.getLob()) {
                DialogHelper.createSimpleDialog(getActivity(), InterpretRCManager.interpretRC_BinqMobMdu(getActivity(), response.getReply().getCode(), callback_main.getLob()));
            } else if (R.string.CONST_LOB_LTS == callback_main.getLob()) {
                DialogHelper.createSimpleDialog(getActivity(), InterpretRCManager.interpretRC_BinqLtsMdu(getActivity(), response.getReply().getCode()));
            } else if (R.string.CONST_LOB_PCD == callback_main.getLob() || R.string.CONST_LOB_TV == callback_main.getLob()) {
                DialogHelper.createSimpleDialog(getActivity(), InterpretRCManager.interpretRC_BinqImsMdu(getActivity(), response.getReply().getCode()));
            } else {
                DialogHelper.createSimpleDialog(getActivity(), ClnEnv.getRPCErrMsg(getActivity(), response.getReply().getCode()));
            }
        }

        if (APIsManager.progressManager != null) {
            APIsManager.forceCloseProgressDialog(getContext());
        }

        if (APIsManager.BILL.equals(response.getActionTy())) {
            initBasicUI();
            initMobBillSummary();
        }
    }

    @SuppressLint("NonConstantResourceId")
    private void doGATracker() {
        if (!(Utils.getString(requireActivity(), R.string.CONST_SCRN_MOBBILLSUMMARY).equals(callback_main.getTrackerId()) ||
                Utils.getString(getActivity(), R.string.CONST_SCRN_PCDBILLSUMMARY).equals(callback_main.getTrackerId()) ||
                Utils.getString(getActivity(), R.string.CONST_SCRN_TVBILLSUMMARY).equals(callback_main.getTrackerId()) ||
                Utils.getString(getActivity(), R.string.CONST_SCRN_LTSBILLSUMMARY).equals(callback_main.getTrackerId()))) {

            //tracker
            switch (callback_main.getLob()) {
                case R.string.CONST_LOB_MOB:
                case R.string.CONST_LOB_IOI:
                case R.string.CONST_LOB_1010:
                case R.string.CONST_LOB_O2F:
                    FAWrapper.getInstance().sendFAScreen(getActivity(),
                            R.string.CONST_SCRN_MOBBILLSUMMARY, false);
                    callback_main.setTrackerId(Utils.getString(getActivity(), R.string.CONST_SCRN_MOBBILLSUMMARY));
                    if (debug)
                        Log.i(TAG, "GATRACKER" + Utils.getString(getActivity(), R.string.CONST_SCRN_MOBBILLSUMMARY));
                    break;
                case R.string.CONST_LOB_PCD:
                    FAWrapper.getInstance().sendFAScreen(getActivity(),
                            R.string.CONST_SCRN_PCDBILLSUMMARY, false);
                    callback_main.setTrackerId(Utils.getString(getActivity(), R.string.CONST_SCRN_PCDBILLSUMMARY));
                    if (debug)
                        Log.i(TAG, "GATRACKER" + Utils.getString(getActivity(), R.string.CONST_SCRN_PCDBILLSUMMARY));
                    break;
                case R.string.CONST_LOB_TV:
                    FAWrapper.getInstance().sendFAScreen(getActivity(),
                            R.string.CONST_SCRN_TVBILLSUMMARY, false);
                    callback_main.setTrackerId(Utils.getString(getActivity(), R.string.CONST_SCRN_TVBILLSUMMARY));
                    if (debug)
                        Log.i(TAG, "GATRACKER" + Utils.getString(getActivity(), R.string.CONST_SCRN_TVBILLSUMMARY));
                    break;
                case R.string.CONST_LOB_LTS:
                    FAWrapper.getInstance().sendFAScreen(getActivity(),
                            R.string.CONST_SCRN_LTSBILLSUMMARY, false);
                    callback_main.setTrackerId(Utils.getString(getActivity(), R.string.CONST_SCRN_LTSBILLSUMMARY));
                    if (debug)
                        Log.i(TAG, "GATRACKER" + Utils.getString(getActivity(), R.string.CONST_SCRN_LTSBILLSUMMARY));
                    break;
                case R.string.CONST_LOB_CSP:
                    FAWrapper.getInstance().sendFAScreen(getActivity(), R.string.CONST_SCRN_CSPBILLSUMMARY, false);
                    callback_main.setTrackerId(Utils.getString(getActivity(), R.string.CONST_SCRN_CSPBILLSUMMARY));
                    if (debug)
                        Log.i(TAG, "GATRACKER" + Utils.getString(getActivity(), R.string.CONST_SCRN_CSPBILLSUMMARY));
                    break;
            }
        }
    }

    /* Update: Moiz: 060717
     * This method will show the message PCD_BILL_UNDER_CSL and will be
     * called if the SubnRec.billagt is true and  the LOB is PCD.
     * 060817: Half the basePadding
     */
    private void showPcdBillByAgent() {
        cellList = new ArrayList<>();
        cellViewAdapter = new CellViewAdapter(this);
        aq = new AAQuery(myView);

        aq.id(R.id.fragment_bill_layout).backgroundColorId(R.color.white);
        frame = (LinearLayout) aq.id(R.id.fragment_bill_listview).getView();

        SmallTextCell messageCell;
        messageCell = new SmallTextCell(getString(R.string.PCD_BILL_UNDER_CSL), "");
        messageCell.setTopMargin(basePadding / 2);
        messageCell.setLeftMargin(basePadding / 2);
        messageCell.setRightMargin(basePadding / 2);
        messageCell.setContentColorId(R.color.hkt_textcolor);
        cellList.add(messageCell);
        cellViewAdapter.setView(frame, cellList);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == Constant.REQUEST_FILE_STORAGE_PERMISSION || requestCode == Constant.REQUEST_LOG_FILE_STORAGE_PERMISSION) {
            //Permission is granted
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                if (requestCode == Constant.REQUEST_FILE_STORAGE_PERMISSION) {
                    startDownloadPdf();
                } else {
                    doGetBill();
                }
            } else if (Build.VERSION.SDK_INT >= 23 && !shouldShowRequestPermissionRationale(permissions[0])) {
                displayDialog(requireActivity().getString(R.string.permission_denied));
                //This will return false if the user tick the Don't ask again checkbox
                if (!RuntimePermissionUtil.shouldShowRequestPermission(getActivity())) {
                    ClnEnv.setPref(getActivity().getApplicationContext(), Constant.FILE_STORAGE_PERMISSION_DENIED, true);
                }
            } else {
                displayDialog(requireActivity().getString(R.string.permission_denied));
                //This will return false if the user tick the Don't ask again checkbox
                if (!RuntimePermissionUtil.shouldShowRequestPermission(getActivity())) {
                    ClnEnv.setPref(getActivity().getApplicationContext(), Constant.FILE_STORAGE_PERMISSION_DENIED, true);
                }
            }
        }
    }

    public boolean isLobCsim(String lob) {
        return "O2F".equals(lob) || "101".equals(lob);
    }

    public int[] getDrawableArray(String lob, BinqCra binqCra) {
        int[] paymentDrawables;
        if (isLobCsim(lob)) {
            paymentDrawables = new int[]{R.drawable.pps_small, R.drawable.tapngo_small, 0, 0};
        } else {
            paymentDrawables = new int[]{R.drawable.pps_small, R.drawable.seven_small, R.drawable.tapngo_small, R.drawable.icon_fps};
        }
        return paymentDrawables;
    }

    public String[] getClickArray(String lob, BinqCra binqCra) {
        String[] clickArray;
        if (isLobCsim(lob)) {
            clickArray = new String[]{"onPPSClick", "onTapNGoClick", "", ""};
        } else {
            clickArray = new String[]{"onPPSClick", "onSevenClick", "onTapNGoClick", "onFPSClick"};
        }
        return clickArray;
    }
}
