package com.pccw.myhkt.fragment;

import android.app.Activity;
import android.os.Bundle;

import com.pccw.dango.shared.entity.ShopRec;
import com.pccw.myhkt.APIsResponse;

public class BaseShopFragment extends BaseFragment{
		// CallBack
		protected OnShopListener		callback_main;
		
		@Override
		public void onAttach(Activity activity) {
			super.onAttach(activity);

			try {
				callback_main = (OnShopListener) activity;
			} catch (ClassCastException e) {
				throw new ClassCastException(activity.toString() + " must implement OnShopListener");
			}
		}
		
		@Override
		public void onActivityCreated(Bundle savedInstanceState) {
			super.onActivityCreated(savedInstanceState);
			
		}

		//Interface
		public interface OnShopListener {
			int getActiveSubview();
			void setActiveSubview(int activeSubview);
			void displaySubview();
			void popBackStack();
			int getShopType();
			void setShopType(int shopType);
			ShopRec[] getShopRecAry();
			void setShopRecAry(ShopRec[] shopRecAry);
			ShopRec getShopRec();
			void setShopRec(ShopRec shopRec);
			boolean isPause();
			void setPause(boolean isPause);
		}
		
		public void initPonBanner() {
			
		}
		
		@Override
		public void onSuccess(APIsResponse response) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void onFail(APIsResponse response) {
			// TODO Auto-generated method stub
			
		}

	}

