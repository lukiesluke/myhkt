package com.pccw.dango.shared.g3entity;

import java.io.Serializable;
import java.util.List;


public class G3ServiceItemDTO implements Serializable
{
    private static final long serialVersionUID = -6377729156762243180L;
    
    private String[] prdCd;
    public String[] getPrdCd() {
        return prdCd;
    }
    public void setPrdCd(String[] prdCd) {
        this.prdCd = prdCd;
    }
    public String getQtaName() {
        return qtaName;
    }
    public void setQtaName(String qtaName) {
        this.qtaName = qtaName;
    }
    public String getSrvName() {
        return srvName;
    }
    public void setSrvName(String srvName) {
        this.srvName = srvName;
    }
    public String getSrvPrior() {
        return srvPrior;
    }
    public void setSrvPrior(String srvPrior) {
        this.srvPrior = srvPrior;
    }
    public String getSrvTy() {
        return srvTy;
    }
    public void setSrvTy(String srvTy) {
        this.srvTy = srvTy;
    }
    public String getDispNameEn() {
        return dispNameEn;
    }
    public void setDispNameEn(String dispNameEn) {
        this.dispNameEn = dispNameEn;
    }
    public String getDispNameZh() {
        return dispNameZh;
    }
    public void setDispNameZh(String dispNameZh) {
        this.dispNameZh = dispNameZh;
    }
    public String getDispOrd() {
        return dispOrd;
    }
    public void setDispOrd(String dispOrd) {
        this.dispOrd = dispOrd;
    }
    public String getDispInd() {
        return dispInd;
    }
    public void setDispInd(String dispInd) {
        this.dispInd = dispInd;
    }
    public String getExtRule() {
        return extRule;
    }
    public void setExtRule(String extRule) {
        this.extRule = extRule;
    }
    public String getResetPrd() {
        return resetPrd;
    }
    public void setResetPrd(String resetPrd) {
        this.resetPrd = resetPrd;
    }
    public String getEnqChl() {
        return enqChl;
    }
    public void setEnqChl(String enqChl) {
        this.enqChl = enqChl;
    }
    public String getQtaVal() {
        return qtaVal;
    }
    public void setQtaVal(String qtaVal) {
        this.qtaVal = qtaVal;
    }
    public String getQtaBal() {
        return qtaBal;
    }
    public void setQtaBal(String qtaBal) {
        this.qtaBal = qtaBal;
    }
    public String getQtaConsumed() {
        return qtaConsumed;
    }
    public void setQtaConsumed(String qtaConsumed) {
        this.qtaConsumed = qtaConsumed;
    }
    private String qtaName;
    private String srvName;
    private String srvPrior;
    private String srvTy;
    private String region;
    private String dispNameEn;
    private String dispNameZh;
    private String dispOrd;
    private String dispInd;
    private String extRule;
    private String resetPrd;
    private String enqChl;
    private String qtaVal;
    private String qtaBal;
    private String qtaConsumed;
    private String qtaInitialVal;
        
    public String getQtaInitialVal() {
        return qtaInitialVal;
    }
    public void setQtaInitialVal(String qtaInitialVal) {
        this.qtaInitialVal = qtaInitialVal;
    }
    private List<G3ServiceItemDTO> listOfSubQta; 
    
    
    public List<G3ServiceItemDTO> getListOfSubQta() {
        return listOfSubQta;
    }
    public void setListOfSubQta(List<G3ServiceItemDTO> listOfSubQta) {
        this.listOfSubQta = listOfSubQta;
    }
    
    public String getRegion() {
        return region;
    }
    public void setRegion(String region) {
        this.region = region;
    }   
}
