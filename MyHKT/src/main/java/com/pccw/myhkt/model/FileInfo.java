package com.pccw.myhkt.model;

import java.util.Date;

public class FileInfo {
    private String filename;
    private Date apiDate;

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public Date getApiDate() {
        return apiDate;
    }

    public void setApiDate(Date apiDate) {
        this.apiDate = apiDate;
    }
}
