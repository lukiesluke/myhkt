package com.pccw.dango.shared.g3entity;

import java.io.Serializable;

public class G3UsageDescriptionDTO extends G3MessageDTO implements Serializable
{
    private static final long serialVersionUID = -8771256059427344399L;
    
    private String showWarningIcon;
    private String textDisplay;
    private String[] wapEntitleDisplay;
    
    public G3UsageDescriptionDTO(String textEng, String textChi,
            int textColorR, int textColorG, int textColorB, String htmlEng,
            String htmlChi, String showWarningIcon, String textDisplay, String[] wapEntitleDisplay) {
        super(textEng, textChi, textColorR, textColorG, textColorB, htmlEng, htmlChi);
        this.showWarningIcon = showWarningIcon;
        this.textDisplay = textDisplay;
        this.wapEntitleDisplay = wapEntitleDisplay;
    }
    public G3UsageDescriptionDTO() {
        super();
    }
    public String getShowWarningIcon() {
        return showWarningIcon;
    }
    public void setShowWarningIcon(String showWarningIcon) {
        this.showWarningIcon = showWarningIcon;
    }
    public String getTextDisplay() {
        return textDisplay;
    }
    public void setTextDisplay(String textDisplay) {
        this.textDisplay = textDisplay;
    }
    public String[] getWapEntitleDisplay() {
        return wapEntitleDisplay;
    }
    public void setWapEntitleDisplay(String[] wapEntitleDisplay) {
        this.wapEntitleDisplay = wapEntitleDisplay;
    }
    
    
}
