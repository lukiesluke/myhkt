package com.pccw.myhkt;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import com.pccw.dango.shared.entity.Ctac;
import com.pccw.dango.shared.entity.SubnRec;
import com.pccw.wheat.shared.tool.Reply;

public class Tool {
	public static final int		MAX_EMAIL			= 40;
	
	
	public Reply validate(Ctac iCtac) {
		Reply rRC;

		rRC = validOfficeNum(iCtac);
		if (rRC.isSucc()) rRC = validHomeNum(iCtac);
		if (rRC.isSucc()) rRC = validMobile(iCtac);
		if (rRC.isSucc()) rRC = validEmail(iCtac);
		if (rRC.isSucc()) rRC = validAllNil(iCtac);

		return (rRC);
	}

	public Reply validAllNil(Ctac iCtac) {
		/*Likely pass this.
		 * Since all fields must be supplied */
		
		if (iCtac.getOfficeNum().trim().length() > 0 || iCtac.getHomeNum().trim().length() > 0 || iCtac.getMobile().trim().length() > 0 || iCtac.getEmail().trim().length() > 0) { return (Reply.getSucc()); }

		return (new Reply("Reply.RC_CTAC_REQ_INPUT"));
	}

	public Reply validOfficeNum(Ctac iCtac) {
		Reply rRC;

		if (iCtac.getOfficeNum().trim().length() == 0) { return (new Reply("Reply.RC_CTAC_REQ_OFCNUM")); }

		if (iCtac.getOfficeNum().length() > 0) {
			if (!Tool.isVaDirNum(iCtac.getOfficeNum())) { return (new Reply("Reply.RC_CTAC_IVOFCNUM")); }
		}

		return (Reply.getSucc());
	}

	public Reply validHomeNum(Ctac iCtac) {
		Reply rRC;

		if (iCtac.getHomeNum().trim().length() == 0) { return (new Reply("Reply.RC_CTAC_REQ_HMNUM")); }

		if (iCtac.getHomeNum().length() > 0) {
			if (!Tool.isVaDirNum(iCtac.getHomeNum())) { return (new Reply("Reply.RC_CTAC_IVHMNUM")); }
		}

		return (Reply.getSucc());
	}

	public Reply validMobile(Ctac iCtac) {
		Reply rRC;

		if (iCtac.getMobile().trim().length() == 0) { return (new Reply("Reply.RC_CTAC_REQ_MOB")); }

		if (iCtac.getMobile().length() > 0) {
			if (!Tool.isVaMob(iCtac.getMobile())) { return (new Reply("Reply.RC_CTAC_IVMOB")); }
		}

		if (iCtac.getBomCust().getLob().equals(SubnRec.LOB_PCD)) {
			if (iCtac.getMobile().length() == 0) { return (new Reply("Reply.RC_CTAC_REQ_MOB")); }
		}

		return (Reply.getSucc());
	}

	public Reply validEmail(Ctac iCtac) {
		Reply rRC;

		if (iCtac.getEmail().trim().length() == 0) { return (new Reply("Reply.RC_CTAC_REQ_EMAIL")); }

		if (iCtac.getEmail().length() > 0) {
			if (!Tool.isASC(iCtac.getEmail())) { return (new Reply("Reply.RC_CTAC_NACTMAIL")); }

			if (iCtac.getEmail().length() > MAX_EMAIL) { return (new Reply("Reply.RC_CTAC_ILCTMAIL")); }

			if (!Tool.isEmailFmt(iCtac.getEmail())) { return (new Reply("Reply.RC_CTAC_IVCTMAIL")); }
		}

		return (Reply.getSucc());
	}
	
	
	// Workaround fixing the DnPrefix issue
	private static String rLtsDp = "2,3,80,81,82,83";
	private static String rMobDp = "51,52,53,54,55,56,57,59,6,9,84,850,851,853,854,855,856,857,858,859,86,87,89";
	
	public static boolean isVaMob(String rStr) {
//		String rDpAry[];
//		int rx, ri, rl;
//
//		if (rStr.length() == 8 && isDig(rStr)) {
//			rDpAry = DnPrefix.getInstance(rMobDp, rLtsDp).getMobPfxAry();
//
//			for (rx = 0; rx < rDpAry.length; rx++) {
//				if (rStr.startsWith(rDpAry[rx])) { return (true); }
//			}
//		}

		return (false);
	}

	public static boolean isVaTel(String rStr) {
//		String rDpAry[];
//		int rx, ri, rl;
//
//		if (rStr.length() == 8 && isDig(rStr)) {
//			rDpAry = DnPrefix.getInstance(rMobDp, rLtsDp).getLtsPfxAry();
//
//			for (rx = 0; rx < rDpAry.length; rx++) {
//				if (rStr.startsWith(rDpAry[rx])) { return (true); }
//			}
//		}
//
		return (false);
	}
	
	public static boolean isDig(String rVar) {
		/*Return true if rVar is filled by digits. */

		return (isNum(rVar, false, false));
	}
	
	public static boolean isNum(String rVar, boolean rSign, boolean rDot) {
		/*Return true if rVar is filled by digits.
		 * if rSign is true, allow +/- at the 1st character. */

		int ri, rsi, rdi;
		int rl, rsl, rdl;
		char rChr;
		int rDotPos = -1;

		rl = rVar.length();
        rdl = 0;

		for (ri = 0; ri < rl; ri++) {
			rChr = rVar.charAt(ri);

            if (rChr >= '0' && rChr <= '9') {
                rdl++;
            }
            else {
				if (rChr == '.') {
					if (rDot && rDotPos < 0) {
						rDotPos = ri;
						continue;
					}
				} else if (rChr == '+' || rChr == '-') {
					if (rSign && ri == 0) {
						continue;
					}
				}

				return (false);
			}
		}
        
        if (rdl == 0) {
            /* No Valid '0' - '9' encountered */
            return (false);
        }
        
		return (true);
	}
	
	public static boolean isVaDirNum(String rStr) {
		int rx, ri, rl;

		return (isVaMob(rStr) || isVaTel(rStr));
	}
	
	public static boolean isEmailFmt(String rStr) {
		String rLocal;
		String rDomain;

		int rx, ri, rl;

		rx = rStr.indexOf('@');
		if (rx < 0) return (false);

		rLocal = rStr.substring(0, rx);
		rDomain = rStr.substring(rx + 1);

		if (isVaEmailLocal(rLocal)) {
			if (isVaEmailDomain(rDomain)) { return (true); }
		}

		return (false);
	}
	
	public static boolean isVaEmailLocal(String rStr) {
		char rC;
		char rNC;

		int rx, ri, rl;

		/*refer to: http://en.wikipedia.org/wiki/Email_address#Valid_email_addresses
		 * See "Local Part" Section
		 * 
		 * However, we don't support the follows:
		 * - Special characters are allowed with restrictions "(),:;<>@[\]
		 * - Comments */

		rl = rStr.length();
		if (rl == 0) return (false);

		for (rx = 0; rx < rl; rx++) {
			rC = rStr.charAt(rx);

			if (isAlpNum(rC)) {
				continue;
			} else if (rC == '.') {
				if (rx == 0) return (false);
				if (rx == (rl - 1)) return (false);

				rNC = rStr.charAt(rx - 1);
				if (rC == rNC) return (false);
			} else if ("!#$%&'*+-/=?^_`{|}~".indexOf(rC) >= 0) {
				continue;
			} else {
				return (false);
			}
		}

		return (true);
	}
	
	public static boolean isVaEmailDomain(String rStr) {
		char rC;
		char rNC;

		int rx, ri, rl;

		/*Refer to: http://en.wikipedia.org/wiki/Domain_name
		 * See "Technical requirements and process" Section
		 * 
		 * Not Supported: Comments */

		rl = rStr.length();
		if (rl == 0) return (false);

		for (rx = 0; rx < rl; rx++) {
			rC = rStr.charAt(rx);

			if (rC == '.') {
				if (rx == 0) return (false);
				if (rx == (rl - 1)) return (false);

				rNC = rStr.charAt(rx - 1);
				if (!isAlpNum(rNC)) return (false);

				rNC = rStr.charAt(rx + 1);
				if (!isAlpNum(rNC)) return (false);

				continue;
			} else if (rC == '-') {
				if (rx == 0) return (false);
				if (rx == (rl - 1)) return (false);

				rNC = rStr.charAt(rx - 1);
				if (!isAlpNum(rNC)) return (false);

				rNC = rStr.charAt(rx + 1);
				if (!isAlpNum(rNC)) return (false);

				continue;
			} else if (isAlpNum(rC)) {
				continue;
			} else {
				return (false);
			}
		}

		return (true);
	}
	
	public static boolean isAlpNum(char rChr) {
		if (rChr >= 'a' && rChr <= 'z') return (true);
		if (rChr >= 'A' && rChr <= 'Z') return (true);
		if (rChr >= '0' && rChr <= '9') return (true);

		return (false);
	}
	
	public static boolean isASC(String rVar) {
		/*Return true if rVar is filled by ASCII characters. */

		int ri, rsi, rdi;
		int rl, rsl, rdl;
		char rChr;

		rl = rVar.length();

		for (ri = 0; ri < rl; ri++) {
			rChr = rVar.charAt(ri);
			if (rChr < 0x20 || rChr > 0x7e) return (false);
		}

		return (true);
	}
	
	public static String formatAcctNum(String rSrc) {
	//derek modified 	
		String rRes;
		if (rSrc.length() == 14) {
			rRes = rSrc.substring(0, 2) + "-" + rSrc.substring(2, 8) + "-" + rSrc.substring(8, 12) + "-" + rSrc.substring(12);
		} else {
		    rRes = rSrc;
		}
		return (rRes);
	}
	
	public static String formatAcctNum14(String rSrc) {
		//derek modified 	
		String rRes;
		if (rSrc.length() == 8) {
			//csim number
			rRes = Utils.convertCsimAccNum(rSrc);
		} else {
		    rRes = rSrc;
		}
		return (rRes);
	}
	
	// Rewritten by Ryan to substitute GWT API by generic Java API
	public static String formatDate(String rStr, String rFmt) {

		Date rVal;

		try {
			if (rStr.equals("00000000")) {
				rStr = new String();
			}

			if (!rStr.isEmpty()) {
				SimpleDateFormat fm1 = new SimpleDateFormat(rFmt, Locale.US);

				rVal = str2Date(rStr + "000000");
				rStr = fm1.format(rVal);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return rStr;
	}
	
	public static Date str2Date(String rVar) {
		/*Convert the rVar to Date.
		 * Using the SimpleDateFormat class. */

		String rYY;
		String rMM;
		String rDD;
		String rHR;
		String rMI;
		String rSE;

		int rNYY;
		int rNMM;
		int rNDD;
		int rNHR;
		int rNMI;
		int rNSE;

		Date rDate;

		try {
			if (rVar.length() == 14 && isDig(rVar)) {
				if (isVaDate(rVar)) {
					rYY = rVar.substring(0, 4);
					rMM = rVar.substring(4, 6);
					rDD = rVar.substring(6, 8);
					rHR = rVar.substring(8, 10);
					rMI = rVar.substring(10, 12);
					rSE = rVar.substring(12, 14);

					rNYY = Integer.parseInt(rYY);
					rNMM = Integer.parseInt(rMM);
					rNDD = Integer.parseInt(rDD);
					rNHR = Integer.parseInt(rHR);
					rNMI = Integer.parseInt(rMI);
					rNSE = Integer.parseInt(rSE);

					rDate = new Date(rNYY - 1900, rNMM - 1, rNDD, rNHR, rNMI, rNSE);

					return (rDate);
				}
			}
		} catch (Exception rEX) {
		}

		return (null);
	}

	
	public static boolean isVaDate(String rVar) {
		/*Validate a String, return true if it is a valid date. */

		String rYY;
		String rMM;
		String rDD;
		String rHR;
		String rMI;
		String rSE;

		int rNYY;
		int rNMM;
		int rNDD;
		int rNHR;
		int rNMI;
		int rNSE;

		try {
			if (rVar.length() == 14 && isDig(rVar)) {
				rYY = rVar.substring(0, 4);
				rMM = rVar.substring(4, 6);
				rDD = rVar.substring(6, 8);
				rHR = rVar.substring(8, 10);
				rMI = rVar.substring(10, 12);
				rSE = rVar.substring(12, 14);

				rNYY = Integer.parseInt(rYY);
				rNMM = Integer.parseInt(rMM);
				rNDD = Integer.parseInt(rDD);
				rNHR = Integer.parseInt(rHR);
				rNMI = Integer.parseInt(rMI);
				rNSE = Integer.parseInt(rSE);

				if (rNYY < 1970) return (false);
				if (rNMM < 1 || rNMM > 12) return (false);
				if (rNDD < 1 || rNDD > dayInMonth(rNYY, rNMM)) return (false);
				if (rNHR < 0 || rNHR > 23) return (false);
				if (rNMI < 0 || rNMI > 59) return (false);
				if (rNSE < 0 || rNSE > 59) return (false);

				return (true);
			}
		} catch (Exception rEX) {
		}

		return (false);
	}
	
	private static final int	DAYINMONTH[]	= { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };
	public static int dayInMonth(int rYY, int rMM) {
		int rDIM;

		if (rMM < 1 || rMM > 12) { throw new RuntimeException("Invalid Month!"); }

		rDIM = DAYINMONTH[rMM - 1];
		if (rMM == 2) {
			if (((rYY % 4) == 0) && (((rYY % 100) != 0) || ((rYY % 400) == 0))) {
				rDIM++;
			}
		}

		return (rDIM);
	}
	
	public static boolean isOddNumer(int num) {
	    return (num & 1) != 0;
	}
}
