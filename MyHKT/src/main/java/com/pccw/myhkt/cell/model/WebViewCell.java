package com.pccw.myhkt.cell.model;

import com.pccw.myhkt.R;

import android.util.Log;

public class WebViewCell extends Cell{

	public WebViewCell(String title) {
		super();
		type = Cell.WEBVIEW;
		this.title = title;
	}
}