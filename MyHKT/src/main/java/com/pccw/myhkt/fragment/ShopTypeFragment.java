package com.pccw.myhkt.fragment;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;

import androidx.annotation.NonNull;

import com.pccw.dango.shared.entity.ShopRec;
import com.pccw.myhkt.APIsResponse;
import com.pccw.myhkt.FAWrapper;
import com.pccw.myhkt.R;
import com.pccw.myhkt.Utils;
import com.pccw.myhkt.adapter.ShopTypeListViewAdapter;
import com.pccw.myhkt.lib.ui.AAQuery;

import java.util.Objects;

public class ShopTypeFragment extends BaseShopFragment {
    private ShopTypeFragment me;
    private AAQuery aq;
    private int SHOP_TYPES = 7;
    private ShopTypeListViewAdapter ShopTypeListViewAdapter;

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        me = this;
        TAG = "ShopTypeFragment";
        return inflater.inflate(R.layout.fragment_shoptype, container, false);
    }

    @Override
    public void onStart() {
        super.onStart();
        init();
    }

    @Override
    public void onResume() {
        super.onResume();
        //Screen Tracker
        FAWrapper.getInstance().sendFAScreen(getActivity(), R.string.CONST_SCRN_SHOPREGION, false);
    }

    private void init() {
        aq = new AAQuery(getActivity());
        aq.id(R.id.shoptype_remark).textSize(getResources().getInteger(R.integer.textsize_default_int));

        String[] shopType = new String[SHOP_TYPES];
        ShopTypeListViewAdapter = new ShopTypeListViewAdapter(requireActivity(), shopType);
        aq.id(R.id.shoptype_listview).adapter(ShopTypeListViewAdapter);
        aq.id(R.id.shoptype_listview).itemClicked(onItemClickListener);
        //set hyperlink for google tnc
        SpannableString content;
        content = new SpannableString(Utils.getString(getActivity(), R.string.shopregion_googletacs));
        content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
        aq.id(R.id.shopregion_googletacs).getTextView().setText(content);
        aq.id(R.id.shopregion_googletacs).clicked(this, "onClick");

        content = new SpannableString(Utils.getString(getActivity(), R.string.shopregion_googleprivacy));
        content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
        aq.id(R.id.shopregion_googleprivacy).getTextView().setText(content);
        aq.id(R.id.shopregion_googleprivacy).clicked(this, "onClick");
    }

    public void onClick(View v) {
        Intent intent;
        switch (v.getId()) {
            case R.id.shopregion_googletacs:
                String gptacsurl = Utils.getString(getActivity(), R.string.shopregion_googletacs_url);
                intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse(gptacsurl));
                startActivity(intent);
                break;
            case R.id.shopregion_googleprivacy:
                String gpurl = Utils.getString(getActivity(), R.string.shopregion_googleprivacy_url);
                intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse(gpurl));
                startActivity(intent);
                break;
        }
    }

    // List View on Item Click Listener
    private final OnItemClickListener onItemClickListener = (adapterView, view, position, arg3) -> {
        if (position != 0) {
            callback_main.setShopType(position);
            callback_main.setActiveSubview(R.string.CONST_SELECTEDFRAG_SHOPDISTRICT);
            callback_main.displaySubview();
        } else {
            callback_main.setActiveSubview(R.string.CONST_SELECTEDFRAG_SHOPMYLOC);
            callback_main.displaySubview();
        }
    };

    @Override
    public void onSuccess(APIsResponse response) {
        // TODO Auto-generated method stub
    }

    @Override
    public void onFail(APIsResponse response) {
        // TODO Auto-generated method stub
    }
}
