/*
    Class for BCSG Record to be used in GWT
    (At the moment of Sep 2016, only WAGASHI will use it)
    
    Keywords
    --------
    $URL: svn://xhkalx08/dango/trk/src/com/pccw/dango/shared/entity/BcsgRec.java $
    $Rev: 844 $
    $Date: 2016-10-17 11:54:01 +0800 (Mon, 17 Oct 2016) $
    $Author: upd $
*/

package com.pccw.dango.shared.entity;

import java.io.Serializable;



public class BcsgRec implements Serializable
{
    private static final long serialVersionUID = 8834749628364661224L;
    
    public int                      cust_rid;           /* CUST rid                                      */
    public String                   seg;                /* Segment Code                                  */
    public String                   create_ts;          /* Record Create TS                              */
    public String                   create_psn;         /* Record Create Person                          */
    public String                   lastupd_ts;         /* Last Update TS                                */
    public String                   lastupd_psn;        /* Last Update Person                            */
    public int                      rev;                /* Record Revision                               */

    public static final int         L_SEG         = 4;
    public static final int         L_CREATE_TS   = 14;
    public static final int         L_CREATE_PSN  = 40;
    public static final int         L_LASTUPD_TS  = 14;
    public static final int         L_LASTUPD_PSN = 40;
    

    public static void main(String rArg[])
    {
        System.out.println(getVer());
        return;
    }
    
    
    public static String getVer()
    {
        return ("$URL: svn://xhkalx08/dango/trk/src/com/pccw/dango/shared/entity/BcsgRec.java $, $Rev: 844 $");
    }

    
    public BcsgRec()
    {
        initAndClear();
    }


    final void initAndClear()
    {
        init();
        clear();
    }

    
    protected void init()
    {
    }
    
    
    public void clear()
    {
        cust_rid        = 0;
        seg             = "";
        create_ts       = "";
        create_psn      = "";
        lastupd_ts      = "";
        lastupd_psn     = "";
        rev             = 0;                
    }


    public BcsgRec copyFrom(BcsgRec rSrc)
    {
        cust_rid                    = rSrc.cust_rid;
        seg                         = rSrc.seg;
        create_ts                   = rSrc.create_ts;
        create_psn                  = rSrc.create_psn;
        lastupd_ts                  = rSrc.lastupd_ts;
        lastupd_psn                 = rSrc.lastupd_psn;
        rev                         = rSrc.rev;

        return (this);
    }


    public BcsgRec copyTo(BcsgRec rDes)
    {
        rDes.copyFrom(this);
        return (rDes);
    }
    
    
    public BcsgRec copyMe()
    {
        BcsgRec                     rDes;

        rDes = new BcsgRec();
        rDes.copyFrom(this);
        return (rDes);
    }
}    
