package com.pccw.myhkt.fragment;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.DialogInterface;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;

import com.pccw.dango.shared.cra.ShopCra;
import com.pccw.dango.shared.entity.ShopRec;
import com.pccw.dango.shared.tool.RC;
import com.pccw.myhkt.APIsManager;
import com.pccw.myhkt.APIsResponse;
import com.pccw.myhkt.ClnEnv;
import com.pccw.myhkt.DialogHelper;
import com.pccw.biz.myhkt.R;
import com.pccw.myhkt.activity.BaseActivity;
import com.pccw.myhkt.adapter.ShopListViewAdapter;
import com.pccw.myhkt.lib.ui.AAQuery;

public class ShopDistrictChildFragment extends BaseShopFragment{
	private ShopDistrictChildFragment me;
	private AAQuery aq;
	private View thisView;
	
	// List View
	private ShopListViewAdapter						shopListViewAdapter;
	private static ShopRec[]							shopRec					= null;
	private static ShopRec[]							filteredRec				= null;
	private ShopCra shopcra = null;
	private ShopCra rShopCra = null;
	private int district = -1;
	
	// Fragment
	private FragmentManager				fragmentManager;
	private Fragment						shopDetailFragment		= null;
	
	
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		me = this;
		
		TAG = "ShopDistrictChildFragment";
		
		View fragmentLayout = inflater.inflate(R.layout.fragment_shopdistrict_child, container, false);
		thisView = fragmentLayout;
		aq = new AAQuery(thisView);
		return fragmentLayout;
	}
	
	
	@Override
	public void onStart() {
		super.onStart();
		init();
	}
	
	@Override
	public void onResume(){
		super.onResume();
	}
	
	@Override
	public void onPause(){
		super.onPause();
//		callback.setPause(true);
//		callback.setActiveSubview(R.string.CONST_SELECTEDFRAG_SHOPDISTRICT);
	}
	
	private void init() {
		fragmentManager = getActivity().getSupportFragmentManager();
			
		int shopType = callback_main.getShopType();
		//header text
		if (shopType > 0) {
			switch (shopType) {
				case 1:
					aq.id(R.id.shopdistrict_shoptype_txt).text(R.string.myhkt_shop_allshop);
					break;
				case 2:
					aq.id(R.id.shopdistrict_shoptype_txt).text(R.string.myhkt_shop_iot);
					break;
				case 3:
					aq.id(R.id.shopdistrict_shoptype_txt).text(R.string.myhkt_shop_hkt);
					break;
				case 4:
					aq.id(R.id.shopdistrict_shoptype_txt).text(R.string.myhkt_shop_biz);
                    break;
				case 5:
					aq.id(R.id.shopdistrict_shoptype_txt).text(R.string.myhkt_shop_1010);
					break;
				case 6:
					aq.id(R.id.shopdistrict_shoptype_txt).text(R.string.myhkt_shop_csl);
					break;
				case 7:
					aq.id(R.id.shopdistrict_shoptype_txt).text(R.string.myhkt_shop_cs);
					break;
				case 8:
					aq.id(R.id.shopdistrict_shoptype_txt).text(R.string.myhkt_shop_smart_living);
					break;
			}
		}
		//listview
		aq.id(R.id.shopdistrict_listview).itemClicked(onItemClickListener);
	}
	
	// List View on Item Click Listener
	private OnItemClickListener	onItemClickListener	= new OnItemClickListener() {
														public void onItemClick(AdapterView<?> adapterView, View view, int position, long arg3) {
															callback_main.setShopRec(shopRec[position]);
															callback_main.setActiveSubview(R.string.CONST_SELECTEDFRAG_SHOPDETAIL);
															callback_main.displaySubview();
														}
													};
	
	protected final void refreshData() {
		super.refreshData();
		
	}
	
	public void getShop(String district){
		aq = new AAQuery(thisView);
		shopcra = new ShopCra();
		shopcra.setIArea(district);
		APIsManager.doGetShop(this, shopcra);
	}
	
	@Override
	public void onSuccess(APIsResponse response) {
		if (APIsManager.SHOP_BYA.equalsIgnoreCase(response.getActionTy())) {
			rShopCra = (ShopCra) response.getCra();
			if ("HK".equalsIgnoreCase(rShopCra.getIArea())) {
				ClnEnv.setShopCraHK(rShopCra);
			} else if ("KLN".equalsIgnoreCase(rShopCra.getIArea())) {
				ClnEnv.setShopCraKLN(rShopCra);
			} else if ("NT".equalsIgnoreCase(rShopCra.getIArea())) {
				ClnEnv.setShopCraNT(rShopCra);
			}
			prepareShopList(rShopCra);
		}
	}

	@Override
	public void onFail(APIsResponse response) {
		if (response != null) {
			//return to shop type page if API error
			DialogInterface.OnClickListener onPositiveClickListener =  new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					callback_main.popBackStack();
				}
			};
			
			
			// General Error Message
			if (!"".equals(response.getMessage()) && response.getMessage() != null) {
				DialogHelper.createSimpleDialog(getActivity(), response.getMessage(), getString(R.string.btn_ok), onPositiveClickListener);
			} else if (RC.TCSESS_MM.equalsIgnoreCase(response.getReply().getCode())) {
				BaseActivity.ivSessDialog();
			} else {
				DialogHelper.createSimpleDialog(getActivity(),  ClnEnv.getRPCErrMsg(getActivity(), response.getReply().getCode()), getString(R.string.btn_ok), onPositiveClickListener);
			}
		}
	}
	
	private void prepareShopList(ShopCra rshopcra) {
		shopRec = callback_main.getShopType() == 1 ? rshopcra.getOShopRecAry() : getFilteredShopRec(rshopcra.getOShopRecAry(), callback_main.getShopType());
		shopListViewAdapter = new ShopListViewAdapter(getActivity(), shopRec);
		aq.id(R.id.shopdistrict_listview).adapter(shopListViewAdapter);
		
		//display message if list is empty
		if (aq.id(R.id.shopdistrict_listview).getListView().getCount() == 0) {
			aq.id(R.id.shopdistrict_empty_txt).getTextView().setVisibility(View.VISIBLE); 
		} else {
			aq.id(R.id.shopdistrict_empty_txt).getTextView().setVisibility(View.GONE); 
		}
	}
	
	public void refresh(int position){
		if (position != -1) {	
			try {
				switch(position) {
					case 0:
						if (ClnEnv.getShopCraHK() != null) {
							prepareShopList(ClnEnv.getShopCraHK());
						} else {
							getShop(getResources().getString(R.string.CONST_SHOP_HK));
						}
						break;
					case 1:
						if (ClnEnv.getShopCraKLN() != null) {
							prepareShopList(ClnEnv.getShopCraKLN());
						} else {
							getShop(getResources().getString(R.string.CONST_SHOP_KLN));
						}
						break;
					case 2:
						if (ClnEnv.getShopCraNT() != null) {
							prepareShopList(ClnEnv.getShopCraNT());
						} else {
							getShop(getResources().getString(R.string.CONST_SHOP_NT));
						}
						break;
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	protected final static ShopRec[] getFilteredShopRec(ShopRec[] shopRec, Integer shopType) {
		List<ShopRec> filteredRecList = new ArrayList<ShopRec>();
		switch (shopType) {
			case 2:
	            for (int i = 0; i < shopRec.length; i++) {
	                if ("I".equalsIgnoreCase(shopRec[i].shop_ty)) {
	                    filteredRecList.add(shopRec[i]);
	                }
	            }
	            break;
			case 3:
				for (int i = 0; i < shopRec.length; i++) {
					if ("S".equalsIgnoreCase(shopRec[i].shop_ty)) {
						filteredRecList.add(shopRec[i]);
					}
				}
				break;
			case 4:
	                for (int i = 0; i < shopRec.length; i++) {
	                    if(!TextUtils.isEmpty(shopRec[i].shop_cat) && shopRec[i].shop_cat.length() >= 2) {
	                        if ("Y".equalsIgnoreCase(String.valueOf(shopRec[i].shop_cat.toCharArray()[1]))) {
	                            filteredRecList.add(shopRec[i]);
	                        }
	                    }
	                }
	                break;
			case 5:
				for (int i = 0; i < shopRec.length; i++) {
					if ("1".equalsIgnoreCase(shopRec[i].shop_ty)) {
						filteredRecList.add(shopRec[i]);
					}
				}
				break;
			case 6:
				for (int i = 0; i < shopRec.length; i++) {
					if ("D".equalsIgnoreCase(shopRec[i].shop_ty)) {
						filteredRecList.add(shopRec[i]);
					}
				}
				break;
			case 7:
				for (int i = 0; i < shopRec.length; i++) {
					if ("C".equalsIgnoreCase(shopRec[i].shop_ty)) {
						filteredRecList.add(shopRec[i]);
					}
				}
				break;
			case 8:
				for (int i = 0; i < shopRec.length; i++) {
					if ("L".equalsIgnoreCase(shopRec[i].shop_ty) || shopRec[i].shop_cat.startsWith("Y")) {
						filteredRecList.add(shopRec[i]);
					}
				}
				break;
		}
	    filteredRec = new ShopRec[filteredRecList.size()];
	    filteredRecList.toArray(filteredRec); // fill the array
	    return filteredRec;
	}

}
