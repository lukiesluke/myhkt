package com.pccw.myhkt.activity;

import android.content.Context;
import android.content.SharedPreferences;
import androidx.test.rule.ActivityTestRule;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import com.pccw.myhkt.AccountHelper;
import com.pccw.myhkt.R;
import com.pccw.myhkt.base.BaseTest;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static androidx.test.platform.app.InstrumentationRegistry.getInstrumentation;

@Ignore
@RunWith(AndroidJUnit4.class)
public class ContactUsActivityTest extends BaseTest {
    private Context context;
    SharedPreferences shared;

    @Rule
    public ActivityTestRule<MainMenuActivity> activityTestRule = new ActivityTestRule<>(MainMenuActivity.class);

    @Before
    public void setUp() {
        context = getInstrumentation().getTargetContext();
        shared = context.getSharedPreferences(
                "myhkt",
                Context.MODE_PRIVATE);

    }

    @Test
    public void testContactUsDisplay() {
        gotoMainMenuItem(11);
        checkStringIsDisplayed(context, R.string.myhkt_cu_title);
        checkStringIsDisplayed(context, R.string.support_consumer);
        checkStringIsDisplayed(context, R.string.SUPPORT_PREMIER);
        checkStringIsDisplayed(context, R.string.support_biz);
        clickLeftNavBar();
    }

    @Test
    public void testConsumerTabDisplay() {
        gotoMainMenuItem(11);
        checkStringIsDisplayed(context, R.string.TITLE_LIVE_CHAT);
        checkStringIsDisplayed(context, R.string.SUB_TITLE_LIVE_CHAT);
        checkStringIsDisplayed(context, R.string.support_liveChat_description_1010);
        checkStringIsDisplayed(context, R.string.support_liveChat_description_csl);
        checkStringIsDisplayed(context, R.string.support_hotlines);
        checkStringIsDisplayed(context, R.string.support_consumer_hotline_title);
        checkStringIsDisplayed(context, R.string.support_consumer_hotline_no);
        checkStringIsDisplayed(context, R.string.support_fixed_line_costumer_service_title);
        checkStringIsDisplayed(context, R.string.support_fixed_line_costumer_service_hotline_info);
        checkStringIsDisplayed(context, R.string.support_1010_customer_hotline_title);
        checkStringIsDisplayed(context, R.string.support_mobile_hotline_no_1010);
        checkStringIsDisplayed(context, R.string.support_csl_customer_hotline_title);
        checkStringIsDisplayed(context, R.string.support_mobile_hotline_no);
        checkStringIsDisplayed(context, R.string.support_consumer_tv_hotline_title);
        checkStringIsDisplayed(context, R.string.support_consumer_tv_hotline_no);
        checkStringIsDisplayed(context, R.string.support_sales_hotline);
        checkStringIsDisplayed(context, R.string.support_sales_hotline_title);
        checkStringIsDisplayed(context, R.string.support_sales_hotline_no);
        checkStringIsDisplayed(context, R.string.support_fixed_line_sales_service_title);
        checkStringIsDisplayed(context, R.string.support_fixed_line_sales_service_hotline_info);
        checkStringIsDisplayed(context, R.string.support_sales_1010_hotline_title);
        checkStringIsDisplayed(context, R.string.support_sales_1010_hotline_no);
        checkStringIsDisplayed(context, R.string.support_sales_csl_hotline_title);
        checkStringIsDisplayed(context, R.string.support_sales_csl_hotline_no);
        checkStringIsDisplayed(context, R.string.support_email_enquiries);
        checkStringIsDisplayed(context, R.string.support_fixed_line_service_title);
        checkStringIsDisplayed(context, R.string.support_fixed_line_service_email_info);
        checkStringIsDisplayed(context, R.string.support_consumer_email);
        checkStringIsDisplayed(context, R.string.support_consumer_email_info);
        checkStringIsDisplayed(context, R.string.myhkt_cu_pcdTitle);
        checkStringIsDisplayed(context, R.string.myhkt_cu_GI);
        checkStringIsDisplayed(context, R.string.myhkt_cu_pcdemail_cs);
        checkStringIsDisplayed(context, R.string.myhkt_cu_TS);
        checkStringIsDisplayed(context, R.string.myhkt_cu_pcdemail_ts);
        checkStringIsDisplayed(context, R.string.myhkt_cu_tvTitle);
        checkStringIsDisplayed(context, R.string.myhkt_cu_GI);
        checkStringIsDisplayed(context, R.string.myhkt_cu_tvemail_cs);
        checkStringIsDisplayed(context, R.string.myhkt_cu_TS);
        checkStringIsDisplayed(context, R.string.myhkt_cu_tvemail_ts);
        checkStringIsDisplayed(context, R.string.support_consumer_others);
        checkStringIsDisplayed(context.getString(R.string.support_consumer_others_fb) + "\n" +
                context.getString(R.string.support_consumer_others_fbinfo));
        checkStringIsDisplayed(context.getString(R.string.support_consumer_others_ig) + "\n" +
                context.getString(R.string.support_consumer_others_iginfo));
        clickView(context,R.id.rv_cu_consumer_live_chat,0,
                R.id.tv_btn_action_text,R.string.BTN_LIVE_CHAT);
        clickLeftNavBar();
        gotoMainMenuItem(11);
        clickView(context,R.id.rv_cu_consumer_live_chat,1,
                R.id.tv_btn_action_text,R.string.BTN_LIVE_CHAT);

        clickLeftNavBar();
        gotoMainMenuItem(11);
        clickView(context,R.id.rv_cu_consumer_live_chat,2,
                R.id.tv_btn_action_text,R.string.BTN_LIVE_CHAT);

        clickLeftNavBar();
        gotoMainMenuItem(11);
    }

    @Test
    public void testHKTPremierTabDisplay() {
        gotoMainMenuItem(11);
        clickString(context, R.string.SUPPORT_PREMIER);
        checkStringIsDisplayed(context, R.string.BTN_LIVE_CHAT);
        checkStringIsDisplayed(context, R.string.MYHKT_BTN_CANCEL);
        checkStringIsDisplayed(context, R.string.BTN_LIVE_CHAT);
        checkStringIsDisplayed(context, R.string.support_liveChat_description_prermier);
        checkStringIsDisplayed(context, R.string.support_liveChat_description_prermier_detail);
        checkStringIsDisplayed(context, R.string.support_hotlines_premier);
        checkStringIsDisplayed(context, R.string.SUPPORT_PREMIER_HOTLINE_TITLE);
        checkStringIsDisplayed(context, R.string.SUPPORT_PREMIER_HOTLINE_NO);
        checkStringIsDisplayed(context, R.string.support_sales_hotline);
        checkStringIsDisplayed(context, R.string.sales_premier_hotline_title);
        checkStringIsDisplayed(context, R.string.sales_premier_hotline_no);
        checkStringIsDisplayed(context, R.string.support_email_enquiries);
        checkStringIsDisplayed(context, R.string.SUPPORT_PREMIER_HOTLINE_TITLE);
        checkStringIsDisplayed(context, R.string.SUPPORT_PREMIER_EMAIL_CS);

        clickView(context,R.id.rv_cu_premier_live_chat,0,
                R.id.tv_btn_action_text,R.string.BTN_LIVE_CHAT);

        clickLeftNavBar();
    }

    @Test
    public void testBusinessTabDisplay() {
        gotoMainMenuItem(11);
        clickString(context, R.string.support_biz);
        checkStringIsDisplayed(context, R.string.support_hotlines);
        checkStringIsDisplayed(context, R.string.support_biz_hotline_title);
        checkStringIsDisplayed(context, R.string.support_biz_hotline_no);
        checkStringIsDisplayed(context, R.string.support_email_enquiries);
        checkStringIsDisplayed(context, R.string.support_biz_email);
        checkStringIsDisplayed(context, R.string.support_biz_email_info);
        checkStringIsDisplayed(context, R.string.support_biz_email_pcd);
        checkStringIsDisplayed(context, R.string.myhkt_cu_SI);
        checkStringIsDisplayed(context, R.string.myhkt_cu_bizpcdemail_sales);
        checkStringIsDisplayed(context, R.string.myhkt_cu_GI);
        checkStringIsDisplayed(context, R.string.myhkt_cu_bizpcdemail_cs);
        checkStringIsDisplayed(context, R.string.myhkt_cu_TS);
        checkStringIsDisplayed(context, R.string.myhkt_cu_bizpcdemail_ts);
    }

    @Test
    public void testNormalContactUsDisplay(){
        loginUAT8();
        checkStringIsDisplayed(context, R.string.myhkt_cu_title);
        checkStringIsDisplayed(context, R.string.support_consumer);
        checkStringIsDisplayed(context, R.string.support_biz);
        executeLogout(context);
    }

    @Test
    public void testPremierContactUsDisplay() {
        loginUAT7();
        checkStringIsDisplayed(context, R.string.myhkt_cu_title);
        checkStringIsDisplayed(context, R.string.SUPPORT_PREMIER);
        checkStringIsDisplayed(context, R.string.support_biz);
        executeLogout(context);
    }

    public void loginUAT7(){
        clickLeftNavBar();
        clickString(context, R.string.myhkt_LGIF_LOGIN);
        loginMyCredentialsUAT(AccountHelper.getUATAccount7().getUsername(),
                AccountHelper.getUATAccount7().getPassword(), shared);
        gotoMainMenuItem(11);
    }

    public void loginUAT8(){
        clickLeftNavBar();
        clickString(context, R.string.myhkt_LGIF_LOGIN);
        loginMyCredentialsUAT(AccountHelper.getUATAccount8().getUsername(),
                AccountHelper.getUATAccount8().getPassword(), shared);
        gotoMainMenuItem(11);
    }

}