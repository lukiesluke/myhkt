 package com.pccw.myhkt.fragment;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;

import androidx.annotation.NonNull;

import com.pccw.dango.shared.cra.ApptCra;
import com.pccw.dango.shared.cra.LnttCra;
import com.pccw.dango.shared.entity.Account;
import com.pccw.dango.shared.entity.SubnRec;
import com.pccw.myhkt.APIsResponse;
import com.pccw.myhkt.ClnEnv;
import com.pccw.myhkt.R;
import com.pccw.myhkt.model.AcctAgent;
import com.pccw.myhkt.model.LnttAgent;
import com.pccw.myhkt.mymob.model.MyMobAcctAgent;

/************************************************************************
 * File : BaseServiceFragment.java
 * Desc : Special base fragment for ServiceActivity related fragments
 * Name : BaseServiceFragment
 * by 	: Derek Tsui
 * Date : 28/01/2016
 * 
 * Change History:
 * Date       Modified By		Description
 * ---------- ----------------	-------------------------------
 * 28/01/2015 Derek Tsui 		-First draft
 *************************************************************************/

public class BaseServiceFragment extends BaseFragment {
	// CallBack
	protected OnServiceListener		callback_main;
	protected OnLiveChatListener 	callback_livechat;

	
	@Override
	public void onAttach(@NonNull Activity activity) {
		super.onAttach(activity);

		try {
			callback_main = (OnServiceListener) activity;
		} catch (ClassCastException e) {
			throw new ClassCastException(activity.toString() + " must implement OnServiceListener");
		}
		
		try {
			callback_livechat = (OnLiveChatListener) activity;
		} catch (ClassCastException e) {
			throw new ClassCastException(activity.toString() + " must implement OnLiveChatListener");
		}

		ClnEnv.updateUILocale(activity.getBaseContext(), ClnEnv.getAppLocale(activity.getBaseContext()));
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);		
		initPonBanner();
	}
	public interface OnLiveChatListener {
		public void 	openLiveChat();
		public void 	openLiveChat(String tempModuleId);
		public void		setModuleId(String moduleId);
		String getModuleId();
	}
	
	//Interface
	public interface OnServiceListener {
		public Account 	getAccount();
		public void 	setAccount(Account account);
		public SubnRec 	getSubnRec();
		public void 	setSubscriptionRec(SubnRec subnRec);
		public void 	setActiveSubview(int index);
		public int 		getActiveSubview();
		public ApptCra  getApptCra();
		public void 	setApptCra(ApptCra mApptCra);
		public void 	displaySubview();
		public int 	getLob();
		public int 	getLtsType();
		public String  getLobString();
		public boolean 	IsZombie();
		public boolean 	CompareLastBillDate(String lastBilldate, String newlastBilldate);
		public boolean 	isMyMobAcct();
		public MyMobAcctAgent getMyMobAcctAgent();
		public AcctAgent getAcctAgent();
		public int getCurrentServicePage();
		//TODO
		public LnttAgent getLnttAgent();
		public void 	 setLnttAgent(LnttAgent lnttAgent);
		public LnttCra   getLnttCra();
		public void 	 setLnttCra(LnttCra lnttCra);
		public boolean  isLnttServiceClearable();
		public void 	setIsLnttServiceClearable(Boolean isLnttServiceClearable);
		public String 	 getNavTitle();
		public String 	 getTrackerId();
		public void 	 setTrackerId(String trackerId);
		void			onShowModemTab(boolean isToAutoReboot);
	}
	
	public void initPonBanner() {
		
	}

	//Refresh Data is called onresume
	//Refresh is called manually
	public void refresh() {
		if (debug) Log.i(this.getClass().toString() , "refresh"+"("+java.lang.System.identityHashCode(this)+")");
//		if (getActivity() != null && getActivity().getBaseContext() != null) {
//			ClnEnv.updateUILocale(getActivity().getBaseContext(), ClnEnv.getAppLocale(getActivity().getBaseContext()));
//		}
	}
	@Override
	public void onSuccess(APIsResponse response) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onFail(APIsResponse response) {
		// TODO Auto-generated method stub
		
	}

}
