package com.pccw.myhkt.activity;

import android.content.Context;
import androidx.test.espresso.intent.Intents;
import androidx.test.rule.ActivityTestRule;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import com.pccw.myhkt.ClnEnv;
import com.pccw.myhkt.R;
import com.pccw.myhkt.base.BaseTest;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static androidx.test.platform.app.InstrumentationRegistry.getInstrumentation;

/**************************************************************************************************
 *
 * This Test class will fail.
 *
 * TODO: Needs to refactor RegBasicActivity LOB for this to work.
 *
 * April 16, 2019 - Jarlou Valenzuela - notes
 **************************************************************************************************/

@Ignore
@RunWith(AndroidJUnit4.class)
public class RegBasicActivityTest extends BaseTest {
    private Context context;

    @Rule
    public ActivityTestRule<RegBasicActivity> activityTestRule = new ActivityTestRule<>(RegBasicActivity.class);

    protected static int NETVIGATOR_POSITION = 0;
    protected static int FIXED_LINE_POSITION = 1;
    protected static int NOW_POSITION = 2;
    protected static int IOIO_POSITION = 0;
    protected static int CSL_POSITION = 1;

    protected static int ROW_ONE_ID = 202020;
    protected static int ROW_TWO_ID = 202021;

    @Before
    public void updateLang() {
        ClnEnv.setPref(activityTestRule.getActivity().getApplicationContext(), activityTestRule.getActivity().getString(R.string.CONST_PREF_LOCALE), activityTestRule.getActivity().getString(R.string.CONST_LOCALE_EN));
        context = getInstrumentation().getTargetContext();
    }

    @Test
    public void testRegistrationUIVisibility() {

        // service containers
        checkViewIsDisplayed(R.id.regacc_service_container);

        executeServiceClick(ROW_ONE_ID, NETVIGATOR_POSITION);

        // passport number
        checkViewIsDisplayed(R.id.regbasic_input_hkid);

        // scroll to mobile number (to make views visible)
        scrollToViewWithDescendant(R.id.reg_input_layoutet, R.id.regbasic_input_mobile_reset);

        // account number
        checkViewIsDisplayed(R.id.regbasic_input_accnum);

        // email address
        checkViewIsDisplayed(R.id.regbasic_input_loginid);

        // password
        checkViewIsDisplayed(R.id.regbasic_input_pw);

        // retype password
        checkViewIsDisplayed(R.id.regbasic_input_repw);

        // nickname
        checkViewIsDisplayed(R.id.regbasic_input_nickname);

        // mobile number
        checkViewIsDisplayed(R.id.regbasic_input_mobile_reset);

        // scroll to language selector
        scrollToView(R.id.regbasic_lang_layout);

        // language selector visible
        checkViewIsDisplayed(R.id.regbasic_lang_layout);

    }

    @Test
    public void testRegistrationInputHints() {
        executeServiceClick(ROW_ONE_ID, NETVIGATOR_POSITION);

        // check hint of HK ID
        checkStringHint(R.id.reg_input_layoutet, R.id.regbasic_input_hkid, "e.g. A123456(7)");

        // scroll to mobile
        scrollToViewWithDescendant(R.id.reg_input_layoutet, R.id.regbasic_input_mobile_reset);

        // check hint of email address
        checkStringHint(R.id.reg_input_layoutet, R.id.regbasic_input_loginid, "Valid email address with 8-40 characters");

        // check hint if password
        checkStringHint(R.id.reg_input_layoutet, R.id.regbasic_input_pw, "6-16 alphanumeric characters(case sensitive)");

        // check hint of nickname
        checkStringHint(R.id.reg_input_layoutet, R.id.regbasic_input_nickname, "Maximum 20 characters");

    }

    @Test
    public void testServicesToggles() {
        checkCorrectTogglePerService(ROW_ONE_ID, NETVIGATOR_POSITION, "NETVIGATOR / LiKE1OO Login ID");

        checkCorrectTogglePerService(ROW_ONE_ID, FIXED_LINE_POSITION, "Telephone Number");

        checkCorrectTogglePerService(ROW_ONE_ID, NOW_POSITION, "Account Number");

        checkCorrectTogglePerService(ROW_TWO_ID, IOIO_POSITION, "Mobile Number");

        checkCorrectTogglePerService(ROW_TWO_ID, CSL_POSITION, "Mobile Number");

    }

    @Test
    public void testChangeDisplayLanguage() {
        executeServiceClick(ROW_ONE_ID, NETVIGATOR_POSITION);

        scrollToView(R.id.regbasic_lang_layout);

        clickView(R.id.regbasic_lang_spin);

        clickString("中文");

        checkViewWithText(R.id.regbasic_lang_txt, "介面語言");

        clickView(R.id.regbasic_lang_spin);

        clickString("English");

        checkViewWithText(R.id.regbasic_lang_txt, "Display Language");

    }


    @Test
    public void testInvalidHkIdInput() {
        executeServiceClick(ROW_ONE_ID, NETVIGATOR_POSITION);

        clickView(R.id.regservice_btn_next);

        checkStringIsDisplayed(context, R.string.REGM_ILDOCNUM);

        delay(TestTimeouts.ACTIVITY_LOAD_TIMEOUTS);

        dismissErrorPopup();
    }

    @Test
    public void testNetvigatorMissingAccountNumberInput() {
        executeServiceClick(ROW_ONE_ID, NETVIGATOR_POSITION);

        executeValidHkInput();

        clickView(R.id.regservice_btn_next);

        checkStringIsDisplayed(context,R.string.REGM_ILSRVNUM_PCD);

        dismissErrorPopup();
    }

    @Test
    public void testFixedLineMissingTelephoneNumberInput() {
        executeServiceClick(ROW_ONE_ID, FIXED_LINE_POSITION);

        executeValidHkInput();

        clickView(R.id.regservice_btn_next);

        checkStringIsDisplayed(context,R.string.REGM_ILSRVNUM_LTS);

        dismissErrorPopup();
    }

    @Test
    public void testNOWMissingAccountNumberInput() {
        executeServiceClick(ROW_ONE_ID, NOW_POSITION);

        executeValidHkInput();

        clickView(R.id.regservice_btn_next);

        delay(TestTimeouts.LIVECHAT_TIMEOUTS);

        checkStringIsDisplayed(context,R.string.REGM_IVAN_PCD);

        dismissErrorPopup();
    }

    @Test
    public void testIOIOMissingMobileNumberInput() {
        executeServiceClick(ROW_TWO_ID, IOIO_POSITION);

        executeValidHkInput();

        clickView(R.id.regservice_btn_next);

        checkStringIsDisplayed(context,R.string.REGM_ILSRVNUM_MOB);

        dismissErrorPopup();
    }

    @Test
    public void testCSLMissingMobileNumberInput() {
        executeServiceClick(ROW_TWO_ID, CSL_POSITION);

        executeValidHkInput();

        clickView(R.id.regservice_btn_next);

        checkStringIsDisplayed(context,R.string.REGM_ILSRVNUM_MOB);

        dismissErrorPopup();
    }

    @Test
    public void testMissingEmailAddress() {
        executeServiceClick(ROW_ONE_ID, FIXED_LINE_POSITION);

        executeValidHkInput();

        executeValidAccountNumber();

        clickView(R.id.regservice_btn_next);

        checkStringIsDisplayed(context,R.string.REGM_NALOGINID);

        dismissErrorPopup();

    }

    @Test
    public void testInvalidEmailAddress() {
        executeServiceClick(ROW_ONE_ID, NETVIGATOR_POSITION);

        executeValidHkInput();

        executeValidAccountNumber();

        typeTextView(R.id.reg_input_layoutet,R.id.regbasic_input_loginid,"greatchina1");

        clickView(R.id.regservice_btn_next);

        checkStringIsDisplayed(context,R.string.REGM_IVLOGINID);

        dismissErrorPopup();

    }

    @Test
    public void testMissingPassword() {
        executeServiceClick(ROW_ONE_ID, NETVIGATOR_POSITION);

        executeValidHkInput();

        executeValidAccountNumber();

        executeValidEmail();

        clickView(R.id.regservice_btn_next);

        checkStringIsDisplayed(context,R.string.REGM_ILPWD);

        dismissErrorPopup();
    }

    @Test
    public void testInvalidPassword() {
        executeServiceClick(ROW_ONE_ID, NETVIGATOR_POSITION);

        executeValidHkInput();

        executeValidAccountNumber();

        executeValidEmail();

        typeTextView(R.id.reg_input_layoutet,R.id.regbasic_input_pw,"12345");

        clickView(R.id.regservice_btn_next);

        checkStringIsDisplayed(context,R.string.REGM_ILPWD);

        dismissErrorPopup();
    }

    @Test
    public void testMissingRetypePassword() {
        executeServiceClick(ROW_ONE_ID, NETVIGATOR_POSITION);

        executeValidHkInput();

        executeValidAccountNumber();

        executeValidEmail();

        executeValidPassword();

        clickView(R.id.regservice_btn_next);

        checkStringIsDisplayed(context, R.string.REGM_IVCFMPWD);

        dismissErrorPopup();
    }

    @Test
    public void testPasswordNotMatched() {
        executeServiceClick(ROW_ONE_ID, NETVIGATOR_POSITION);

        executeValidHkInput();

        executeValidAccountNumber();

        executeValidEmail();

        executeValidPassword();

        typeTextView(R.id.reg_input_layoutet,R.id.regbasic_input_repw,"A12345");

        clickView(R.id.regservice_btn_next);

        checkStringIsDisplayed(context, R.string.REGM_IVCFMPWD);

        dismissErrorPopup();
    }

    @Test
    public void testMissingNickname() {
        executeServiceClick(ROW_ONE_ID, NETVIGATOR_POSITION);

        executeValidHkInput();

        executeValidAccountNumber();

        executeValidEmail();

        executeValidPassword();

        executeValidRetypePassword();

        clickView(R.id.regservice_btn_next);

        checkStringIsDisplayed(context,R.string.REGM_ILNICKNAME);

        dismissErrorPopup();
    }

    @Test
    public void testMissingHkMobileNumber() {
        executeServiceClick(ROW_ONE_ID, NETVIGATOR_POSITION);

        executeValidHkInput();

        executeValidAccountNumber();

        executeValidEmail();

        executeValidPassword();

        executeValidRetypePassword();

        executeValidNickname();

        clickView(R.id.regservice_btn_next);

        checkStringIsDisplayed(context, R.string.REGM_IVMOB);

        dismissErrorPopup();
    }

    @Test
    public void testInvalidHkMobileNumber() {
        executeServiceClick(ROW_ONE_ID, NETVIGATOR_POSITION);

        executeValidHkInput();

        executeValidAccountNumber();

        executeValidEmail();

        executeValidPassword();

        executeValidRetypePassword();

        executeValidNickname();

        typeTextView(R.id.reg_input_layoutet,R.id.regbasic_input_mobile_reset,"aaa)");

        clickView(R.id.regservice_btn_next);

        checkStringIsDisplayed(context, R.string.REGM_IVMOB);

    }

    @Test
    public void testValidRegistrationInputs() {
        Intents.init();

        executeServiceClick(ROW_ONE_ID, NETVIGATOR_POSITION);

        executeValidHkInput();

        executeValidAccountNumber();

        executeValidEmail();

        executeValidPassword();

        executeValidRetypePassword();

        executeValidNickname();

        executeValidMobileNumber();

        clickView(R.id.regservice_btn_next);

        checkIntendedActivity(RegConfirmActivity.class.getName());

        Intents.release();

    }

    @Test
    public void testValidRegistrationInputsForMobileServices() {
        Intents.init();

        clickViewChildView(ROW_TWO_ID, CSL_POSITION);

        executeValidHkInput();

        executeValidMobileAccountNumber();

        executeValidEmail();

        executeValidPassword();

        executeValidRetypePassword();

        executeValidNickname();

        executeValidMobileNumber();

        clickView(R.id.regservice_btn_next);

        checkIntendedActivity(RegConfirmActivity.class.getName());

        Intents.release();

    }

    protected void executeValidHkInput() {
        typeTextView(R.id.reg_input_layoutet,R.id.regbasic_input_hkid,"B209128(9)");
    }

    protected void executeValidAccountNumber() {

        // scroll to mobile
        scrollToViewWithDescendant(R.id.reg_input_layoutet, R.id.regbasic_input_mobile_reset);
        typeTextView(R.id.reg_input_layoutet,R.id.regbasic_input_accnum,"75100175386317");

    }

    protected void executeValidMobileAccountNumber() {

        // scroll to mobile
        scrollToViewWithDescendant(R.id.reg_input_layoutet, R.id.regbasic_input_mobile_reset);
        typeTextView(R.id.reg_input_layoutet,R.id.regbasic_input_accnum,"54728531");
    }

    protected void executeValidEmail() {
        // scroll to mobile
        scrollToViewWithDescendant(R.id.reg_input_layoutet, R.id.regbasic_input_mobile_reset);
        typeTextView(R.id.reg_input_layoutet,R.id.regbasic_input_loginid,"greatchina1@pccw.com");
    }

    protected void executeValidPassword() {
        typeTextView(R.id.reg_input_layoutet,R.id.regbasic_input_pw,"Aa12345");
    }

    protected void executeValidRetypePassword() {
        typeTextView(R.id.reg_input_layoutet,R.id.regbasic_input_repw,"Aa12345");
    }

    protected void executeValidNickname() {
        typeTextView(R.id.reg_input_layoutet,R.id.regbasic_input_nickname,"pccwuser");
    }

    protected void executeValidMobileNumber() {
        typeTextView(R.id.reg_input_layoutet,R.id.regbasic_input_mobile_reset,"54728531");

    }

    protected void dismissErrorPopup() {
        clickOK();
    }

    protected void checkCorrectTogglePerService(int parentId, int servicePosition, String serviceIdLabel) {
        // execute click of the specific service
        clickViewChildView(parentId, servicePosition);

        // check if correct label displayed on the account Id
        checkViewWithText(R.id.reg_input_layouttxt,R.id.regbasic_input_accnum,serviceIdLabel);
    }


    /**
     * NOTE: This will fail due to code change is needed for this test
     * @param rowId
     * @param servicePosition
     */
    protected void executeServiceClick(int rowId, int servicePosition) {
        clickViewChildView(rowId,servicePosition);
    }

}