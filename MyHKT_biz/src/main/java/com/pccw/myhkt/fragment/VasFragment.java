package com.pccw.myhkt.fragment;

import java.lang.Thread.UncaughtExceptionHandler;
import java.util.ArrayList;
import java.util.List;

import android.graphics.Point;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.pccw.dango.shared.cra.AddOnCra;
import com.pccw.dango.shared.cra.PlanMobCra;
import com.pccw.dango.shared.entity.SrvPlan;
import com.pccw.dango.shared.g3entity.G3DisplayServiceItemDTO;
import com.pccw.dango.shared.g3entity.G3DisplayServiceItemResultDTO;
import com.pccw.dango.shared.tool.RC;
import com.pccw.myhkt.APIsManager;
import com.pccw.myhkt.APIsResponse;
import com.pccw.myhkt.ClnEnv;
import com.pccw.myhkt.DialogHelper;
import com.pccw.myhkt.InterpretRCManager;
import com.pccw.biz.myhkt.R;
import com.pccw.myhkt.Utils;
import com.pccw.myhkt.activity.BaseActivity;
import com.pccw.myhkt.cell.model.ArrowTextCell;
import com.pccw.myhkt.cell.model.BigTextCell;
import com.pccw.myhkt.cell.model.Cell;
import com.pccw.myhkt.cell.model.SingleBtnCell;
import com.pccw.myhkt.cell.model.SmallTextCell;
import com.pccw.myhkt.cell.model.WebViewCell;
import com.pccw.myhkt.cell.view.CellViewAdapter;
import com.pccw.myhkt.fragment.UsageDataChild1Fragment.OnUsageDataListener;
import com.pccw.myhkt.lib.ui.AAQuery;
import com.pccw.myhkt.model.AcctAgent;
/************************************************************************
File       : VasFragment.java
Desc       : View for showing Mob VAS
Name       : VasFragment
Created by : Andy Wong
Date       : 08/04/2016

Change History:
Date       Modified By			Description
---------- ----------------	-------------------------------
08/04/2016 Andy Wong			- First draft 
 *************************************************************************/

public class VasFragment extends BaseServiceFragment  implements OnUsageDataListener{
	private VasFragment me;
	private View myView;
	private AAQuery aq;
	private int deviceWidth ;
	private int extralinespace;
	private int colWidth ;

	private ViewPager viewPager;
	private PlanMobCra planMobCra; 
	private AddOnCra addOnCra;
	private List<G3DisplayServiceItemDTO> localServiceList;
	private List<G3DisplayServiceItemDTO> romServiceList;
	private List<G3DisplayServiceItemDTO> otherLocalServiceList;
	private List<G3DisplayServiceItemDTO> otherRommaServiceList;
	private Boolean isMob = true;
	private Boolean isCSimPrim = true;
	private Boolean isCSimProgressShown = true;
	private Boolean isLocal = true;
	private CellViewAdapter			cellViewAdapter;
	ArrayList<Cell> cellList = new ArrayList<Cell>();
	private String TAG = "VasFragment";
	private LinearLayout frame;
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		me = this;		
		View fragmentLayout = inflater.inflate(R.layout.fragment_vas, container, false);
		myView = fragmentLayout;
		initData();
		return fragmentLayout;
	}

	protected void initData() {
		aq = new AAQuery(myView);	
		Display display = me.getActivity().getWindowManager().getDefaultDisplay();
		Point size = new Point();
		display.getSize(size);
		deviceWidth = size.x;
		extralinespace = (int) getResDimen(R.dimen.extralinespace);
		basePadding = (int) getResources().getDimension(R.dimen.basePadding);
		colWidth = (deviceWidth - basePadding *2 )/4;
		cellViewAdapter = new CellViewAdapter(getActivity());
	}

	protected void initUI(){
		
		aq.id(R.id.fragment_vas_sv).backgroundColorId(R.color.white);
		aq.marginpx(R.id.fragment_vas_sv, 0, 0, 0, 0);
		frame = (LinearLayout) aq.id(R.id.fragment_vas_frame).getView();

		cellList = new ArrayList<Cell>();

		//Title
		SmallTextCell smallTextCell = new SmallTextCell(getResString(R.string.PLNMBF_VAS), "");
//		smallTextCell.setTitleTypeface(Typeface.BOLD);
		smallTextCell.setTopMargin(0);
		smallTextCell.setLeftPadding(basePadding);
		smallTextCell.setRightPadding(basePadding);
		cellList.add(smallTextCell);
		
		/*CA: Moiz remove live chat feature
		//enquiry
		WebViewCell webViewCell = new WebViewCell(isZh? "file:///android_asset/plan_lts_enquiry_zh.html" : "file:///android_asset/plan_lts_enquiry_en.html");
		webViewCell.setTopMargin(extralinespace/3);
		cellList.add(webViewCell);
		SingleBtnCell singleBtnCell = new SingleBtnCell(getResString(R.string.MYHKT_BTN_LIVECHAT), colWidth * 2, new OnClickListener(){

			@Override
			public void onClick(View v) {
				((BaseActivity)me.getActivity()).openLiveChat();
			}			
		});	
		singleBtnCell.setBgcolorId(R.color.cell_bg_grey);
		singleBtnCell.setDraw(R.drawable.livechat_small);
		singleBtnCell.setLeftMargin(basePadding);
		singleBtnCell.setRightMargin(basePadding);
		singleBtnCell.setLeftPadding(basePadding);
		singleBtnCell.setRightPadding(basePadding);
		cellList.add(singleBtnCell); */
		
		SmallTextCell remarkCell = new SmallTextCell(getResString(R.string.usage_reference_remark), "");
		remarkCell.setRightPadding(basePadding);
		remarkCell.setLeftPadding(basePadding);
		remarkCell.setTitleSizeDelta(-2);
		cellList.add(remarkCell);
		
		cellViewAdapter.setView(frame, cellList);
	}

	private void setUI(){
		cellList = new ArrayList<Cell>();

		int[] widthAry = { colWidth, colWidth, colWidth};

		SmallTextCell smallTextCell = new SmallTextCell(getResString(R.string.PLNMBF_VAS), "");
//		smallTextCell.setTitleTypeface(Typeface.BOLD);
		smallTextCell.setTitleColorId(R.color.black);
		smallTextCell.setRightPadding(basePadding);
		smallTextCell.setLeftPadding(basePadding);
		smallTextCell.setTopMargin(0);
		cellList.add(smallTextCell);

		SrvPlan[] srvPlans = planMobCra.getOMobUsage().getOptVasAry();
		if (planMobCra != null && srvPlans !=null && srvPlans.length > 0) {
			for (SrvPlan srvPlan : srvPlans) {
				addService(srvPlan);
			}				

			SmallTextCell noteCell = new SmallTextCell(getResString(R.string.PLNMBM_OPTVAS_REM),"");
			cellList.add(noteCell);

//			WebViewCell webViewCell = new WebViewCell(isZh ? "file:///android_asset/plan_novas_zh.html" : "file:///android_asset/plan_novas_zh.html");
//			webViewCell.setBgId(R.drawable.webview_bg_grey_full);
//			cellList.add(webViewCell);
		} else {

			SmallTextCell noteCell = new SmallTextCell(getResString(R.string.PLNMBM_NOVAS),null);
			noteCell.setTitleColorId(R.color.hkt_txtcolor_grey);
			noteCell.setRightPadding(basePadding);
			noteCell.setLeftPadding(basePadding);
			cellList.add(noteCell);
			
			/*CA: Moiz- hide live chat feature
			WebViewCell webViewCell = new WebViewCell(isZh? "file:///android_asset/plan_lts_enquiry_zh.html" : "file:///android_asset/plan_lts_enquiry_en.html");
			webViewCell.setTopMargin(extralinespace/3);
			cellList.add(webViewCell);
			SingleBtnCell singleBtnCell = new SingleBtnCell(getResString(R.string.MYHKT_BTN_LIVECHAT), colWidth * 2 , new OnClickListener(){

				@Override
				public void onClick(View v) {
					((BaseActivity)me.getActivity()).openLiveChat();
				}			
			});	
			singleBtnCell.setBgcolorId(R.color.cell_bg_grey);
			singleBtnCell.setDraw(R.drawable.livechat_small);
			singleBtnCell.setLeftMargin(basePadding);
			singleBtnCell.setRightMargin(basePadding);
			singleBtnCell.setLeftPadding(basePadding);
			singleBtnCell.setRightPadding(basePadding);
			cellList.add(singleBtnCell); */
		}
		
		SmallTextCell remarkCell = new SmallTextCell(getResString(R.string.usage_reference_remark), "");
		remarkCell.setRightPadding(basePadding);
		remarkCell.setLeftPadding(basePadding);
		remarkCell.setTitleSizeDelta(-2);
		cellList.add(remarkCell);
		
		cellViewAdapter.setView(frame, cellList);
	}

	private void addService(SrvPlan srvPlan) {
		int[] widthAry = { colWidth, colWidth, colWidth};
		//Title
		String serviceTitle = isZh ? srvPlan.getDesnZh() : srvPlan.getDesnEn();
		BigTextCell titleCell = new BigTextCell(serviceTitle, "");
		titleCell.setArrowShown(true);
		titleCell.setTitleSizeDelta(0);
		titleCell.setPadding(0);
		titleCell.setRightPadding(basePadding);
		titleCell.setLeftPadding(basePadding);
		titleCell.setTitleColorId(R.color.black);
		cellList.add(titleCell);

		//Commitment Detail
		String[] cols = {getResString(R.string.VAS_PERIOD), getResString(R.string.VAS_MTHLY), getResString(R.string.VAS_EFF_DATE)};
		String[] notes = {getResString(R.string.VAS_MTHS), "", getResString(R.string.VAS_DATE)};

		ArrowTextCell colCell = new ArrowTextCell("", cols , notes, widthAry, true, (int) getActivity().getResources().getDimension(R.dimen.textviewheight2));
//		ArrowTextCell colCell = new ArrowTextCell("", cols , notes, widthAry);
		colCell.setContentSizeDelta(-4);
		colCell.setNoteSizeDelta(-4);
		cellList.add(colCell);

		String effstdt = srvPlan.getStDt();
		effstdt = effstdt == null? "-" : Tool.formatDate(effstdt.replaceAll("-", ""), Tool.DATEFMT_3);
//		String[] data = {cntrStDt,srvPlan.getSrvCmmtPrd(), srvPlan.getFee()};
		String commtPrd = "".equals(srvPlan.getSrvCmmtPrd()) ? "-" : srvPlan.getSrvCmmtPrd();
		String fee = "".equals(srvPlan.getFee()) ? "-" : Utils.convertStringToPrice1dp(srvPlan.getFee());
		String stdt = "".equals(effstdt) ? "-" : effstdt;
		
		String[] data = {"-".equals(commtPrd) ? commtPrd : commtPrd + getResString(R.string.DLGM_MONTH), fee, stdt};
		ArrowTextCell dataCell = new ArrowTextCell(getResString(R.string.VAS_COM), data , null, widthAry);
		dataCell.setContentSizeDelta(-4);
		dataCell.setContentColorId(R.color.hkt_textcolor);
		dataCell.setBgcolorId(R.color.cell_bg_grey);
		cellList.add(dataCell);

		//Month to Month Rate
		String[] m2mCol = {getResString(R.string.VAS_MTHLY_FEE)};
		String[] m2mNote = {""};
		ArrowTextCell m2mColCell = new ArrowTextCell("", m2mCol , m2mNote, widthAry);
		m2mColCell.setContentSizeDelta(-4);
		m2mColCell.setNoteSizeDelta(-4);
		cellList.add(m2mColCell);

		String m2mRate = srvPlan.getM2mRate();
		if (!"".equals(m2mRate)) {
			m2mRate = Utils.convertStringToPrice1dp(m2mRate);
		} else {
			m2mRate = "-";
		}
		
		String[] data1 = {m2mRate};
		ArrowTextCell m2mDataCell = new ArrowTextCell(getResString(R.string.VAS_M2MR), data1 , null, widthAry);
		m2mDataCell.setContentSizeDelta(-4);
		m2mDataCell.setContentColorId(R.color.hkt_textcolor);
		m2mDataCell.setBgcolorId(R.color.cell_bg_grey);
		cellList.add(m2mDataCell);

	}


	//	private void addService(G3DisplayServiceItemDTO g3DisplayServiceItemDTO) {
	//		String serviceTitle = isZh ? g3DisplayServiceItemDTO.getTitleLevel1Chi() :g3DisplayServiceItemDTO.getTitleLevel1Eng(); 
	//	}

	// call on onResume , we have to check if it is on currentPage
	public void refreshData(){
		super.refreshData();
		if (callback_main.getActiveSubview() == R.string.CONST_SELECTEDFRAG_VAS) {			
			if (!Utils.getString(getActivity(), R.string.CONST_SCRN_OPTIONALVAS).equals(callback_main.getTrackerId())) {
				callback_main.setTrackerId(Utils.getString(getActivity(), R.string.CONST_SCRN_OPTIONALVAS));
				//Screen Tracker
				if (debug) Log.i("GATRACKER", "GATRACKER" + Utils.getString(getActivity(), R.string.CONST_SCRN_OPTIONALVAS));
			}
			
			PlanMobCra sMlanMobcra = new PlanMobCra();
			sMlanMobcra.setILoginId(ClnEnv.isLoggedIn() ? ClnEnv.getQualSvee().getSveeRec().loginId : "");
			sMlanMobcra.setISubnRec(callback_main.getAcctAgent().getSubnRec());
			APIsManager.doGetPlanMob(me, sMlanMobcra);		
		} 		
	}
	
	
	// View Pager Class
	private class UsageDataPagerAdapter extends FragmentPagerAdapter {
		private List<Fragment> fragmentList;
		public UsageDataPagerAdapter(FragmentManager fragmentManager ,List<Fragment> fragments) {
			super(fragmentManager);
			fragmentList = fragments;
		}

		@Override
		public int getCount() {
			return fragmentList.size();
		}

		// Returns the fragment to display for that page
		@Override
		public Fragment getItem(int position) {
			switch (position) {
			case 0: // Fragment # 0 - This will show FirstFragment
				return fragmentList.get(position);
			case 1: // Fragment # 1 - This will show FirstFragment different title
				return fragmentList.get(position);
			default:
				return null;				
			}
		}
	}


	protected void cleanupUI(){

	}

	private void processList(G3DisplayServiceItemResultDTO g3DisplayServiceItemResultDTO) {
		List<G3DisplayServiceItemDTO> displayServiceItemList = g3DisplayServiceItemResultDTO.getDisplayServiceItemList();
		localServiceList = new ArrayList<G3DisplayServiceItemDTO>(); 
		romServiceList = new ArrayList<G3DisplayServiceItemDTO>() ;
		otherLocalServiceList = new ArrayList<G3DisplayServiceItemDTO>();
		otherRommaServiceList = new ArrayList<G3DisplayServiceItemDTO>();
		for (G3DisplayServiceItemDTO item : displayServiceItemList) {
			if ("Y".equalsIgnoreCase(item.getShowInMainPage())) {
				if ("LOCAL".equalsIgnoreCase(item.getRegion())) {
					localServiceList.add(item);
				} else if ("ROAMING".equalsIgnoreCase(item.getRegion())) {
					romServiceList.add(item);
				}
			} else {
				if ("LOCAL".equalsIgnoreCase(item.getRegion())) {
					otherLocalServiceList.add(item);
				} else if ("ROAMING".equalsIgnoreCase(item.getRegion())) {
					otherRommaServiceList.add(item);
				}
			}
		}
	}

	@Override
	public void onSuccess(APIsResponse response) {	
		if (debug) Log.i("VAS", "Success");
		if (APIsManager.PLAN_MOB.equals(response.getActionTy())){
			planMobCra = (PlanMobCra) response.getCra();		
//			processList(planMobCra.getOMobUsage().getG3DisplayServiceItemResultDTO());
			setUI();
		} else if (APIsManager.AO_AUTH.equals(response.getActionTy())) {
			addOnCra = (AddOnCra) response.getCra();	
			addOnCra.getOSubnRec().ivr_pwd = addOnCra.getISubnRec().ivr_pwd;
			
			callback_main.setSubscriptionRec(addOnCra.getOSubnRec());
			callback_main.getAcctAgent().setSubnRec(addOnCra.getOSubnRec());
		
			//recall
			PlanMobCra sMlanMobcra = new PlanMobCra();
			sMlanMobcra.setILoginId(ClnEnv.isLoggedIn() ? ClnEnv.getQualSvee().getSveeRec().loginId : "");
			sMlanMobcra.setISubnRec(callback_main.getAcctAgent().getSubnRec());
			APIsManager.doGetPlanMob(me, sMlanMobcra);		
		}

	}

	@Override
	public void onFail(APIsResponse response) {
		if (APIsManager.PLAN_MOB.equals(response.getActionTy())){
			if (!"".equals(response.getMessage()) && response.getMessage() != null) {
				DialogHelper.createSimpleDialog(getActivity(),  response.getMessage());
			} else if (RC.TCSESS_MM.equalsIgnoreCase(response.getReply().getCode())) {
				BaseActivity.ivSessDialog();
			} else if (RC.AO_PSPH_FAIL.equalsIgnoreCase(response.getReply().getCode())) {
				recallDoAuth(me, callback_main.getAcctAgent().getSubnRec());
			} else {
				DialogHelper.createSimpleDialog(getActivity(),  InterpretRCManager.interpretRC_BinqMobMdu(getActivity(), response.getReply().getCode(), callback_main.getLob()));
			}
//			planMobCra = (PlanMobCra) response.getCra();
//			DialogHelper.createSimpleDialog(getActivity(), InterpretRCManager.interpretRC_PlanMobMdu(me.getActivity(), planMobCra.getReply()));
//			planMobCra = ClnEnv.getJSONObject(me.getActivity(), PlanMobCra.class, "json/hsim_mobusage_json.txt");
//			processList(planMobCra.getOMobUsage().getG3DisplayServiceItemResultDTO());
			setUI();
		} else {
			//		planMobCra = ClnEnv.getJSONObject(me.getActivity(), PlanMobCra.class, "mob_plan_primary.json");
			//		processList(planMobCra.getOMobUsage().getG3DisplayServiceItemResultDTO());
			//		callback_usage.setPlanMobCra(planMobCra);
			//		fragmentList = new ArrayList<Fragment>();
			//		if (planMobCra.getOMobUsage().getG3DisplayServiceItemResultDTO() != null && 
			//				planMobCra.getOMobUsage().getG3DisplayServiceItemResultDTO().getDisplayServiceItemList() !=null && 
			//				!planMobCra.getOMobUsage().getG3DisplayServiceItemResultDTO().getDisplayServiceItemList().isEmpty() &&
			//				planMobCra.getOMobUsage().getG3DisplayServiceItemResultDTO().getDisplayServiceItemList().get(0) != null){
			//			// Progress Bar show
			//			isCSimProgressShown = true;
			//		} else {
			//			isCSimProgressShown = false;
			//		}
			//		processList(planMobCra.getOMobUsage().getG3DisplayServiceItemResultDTO());
			//		if (isMob) {		
			//			fragmentList.add(usageDataChild1Fragment);
			//			fragmentList.add(usageDataChild2Fragment);						
			//		} else {
			//			if (isCSimProgressShown ) {
			//				fragmentList.add(usageDataChild1Fragment);	
			//			}				
			//			fragmentList.add(usageDataChild2Fragment);
			//		}
			//		setUI();

			// General Error Message
			if (!"".equals(response.getMessage()) && response.getMessage() != null) {
				DialogHelper.createSimpleDialog(getActivity(),  response.getMessage());
			} else if (RC.TCSESS_MM.equalsIgnoreCase(response.getReply().getCode())) {
				BaseActivity.ivSessDialog();
			} else if (RC.AO_PSPH_FAIL.equalsIgnoreCase(response.getReply().getCode())) {
				recallDoAuth(me, callback_main.getAcctAgent().getSubnRec());
			} else {
				DialogHelper.createSimpleDialog(getActivity(),  InterpretRCManager.interpretRC_BinqMobMdu(getActivity(), response.getReply().getCode(), callback_main.getLob()));
			}	
		}
	}



	@Override
	public PlanMobCra getPlanMobCra() {
		return planMobCra;
	}

	@Override
	public void setPlanMobCra(PlanMobCra mplanMobcra) {
		planMobCra = mplanMobcra;
	}
	@Override
	public List<G3DisplayServiceItemDTO> getLocalServiceList() {
		return localServiceList;
	}

	@Override
	public List<G3DisplayServiceItemDTO> getRommaServiceList() {
		return romServiceList;
	}
	@Override
	public List<G3DisplayServiceItemDTO> getOtherLocalServiceList() {
		return otherLocalServiceList;
	}
	@Override
	public List<G3DisplayServiceItemDTO> getOtherRommaServiceList() {
		return otherRommaServiceList;
	}

	@Override
	public AcctAgent getAcctAgent() {
		return callback_main.getAcctAgent();
	}

	
}
