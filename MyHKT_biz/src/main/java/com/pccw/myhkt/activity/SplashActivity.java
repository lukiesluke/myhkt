package com.pccw.myhkt.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Point;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.SystemClock;
import android.util.Log;
import android.view.Display;
import android.widget.ImageView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.pccw.biz.myhkt.R;
import com.pccw.myhkt.APIsManager.OnAPIsListener;
import com.pccw.myhkt.APIsResponse;
import com.pccw.myhkt.ClnEnv;
import com.pccw.myhkt.lib.ui.AAQuery;
import com.pccw.myhkt.model.PushData;
import com.pccw.myhkt.service.InitGCMAsyncTask;
import com.pccw.myhkt.service.VersionCheckService;

/************************************************************************
File       : SplashActivity.java
Desc       : Splash Screen (opening screen)
Name       : SplashActivity
Created by : Derek Tsui
Date       : 30/12/2015

Change History:
Date       Modified By			Description
---------- ----------------	-------------------------------
30/12/2015 Derek Tsui			- First draft

 *************************************************************************/

public class SplashActivity extends Activity implements OnAPIsListener{
	// Common Components
	private boolean								debug					= false;
	private AAQuery 				aq;

	// 20121212 Vincent, add for timer
	private Long								startTime;
	private boolean								bRunning;

	// Version check
	private static SplashActivity				me;
	//	private AsyncTask<Void, Void, String>			appConfigJsonAsyncTask = null;
	//	private AsyncTask<Void, Void, VersionCheck>	doCheckUpdateAsyncTask	= null;
	private AsyncTask<Context, Void, String>	initGCMAsyncTask		= null;
	public static boolean						timerCompleted			= false;
	public static boolean						versioncheckCompleted	= false;
	public static boolean						requiredUpdate			= false;
	public static boolean						forcedUpdate			= false;

	// GCM
	private static String regId;		//registration ID from GCM

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		debug = getResources().getBoolean(R.bool.DEBUG);
		if (debug) Log.i("MainActivity", "onCreate");
		ClnEnv.isForceCloseApp = false;
		ClnEnv.isVCFinish = false;
		if (debug) Log.i("MainActivity", "onCreate" + ClnEnv.isForceCloseApp);
		me = this;
		aq = new AAQuery(this);
		//Draw status bar
		//		if (android.os.Build.VERSION.SDK_INT >= 21) {
		//			Window window = getWindow();
		//			window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
		//			window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
		//			window.setStatusBarColor(getResources().getColor(R.color.hkt_textcolor));
		//		}	
		setContentView(R.layout.activity_splash);

		// Event Tracker

		//init splash images
		Display display = getWindowManager().getDefaultDisplay();
		Point size = new Point();
		display.getSize(size);
		int deviceWidth = size.x;
		int extralinespace = (int) getResources().getDimension(R.dimen.extralinespace);	


		ImageView hktlogoView = aq.id(R.id.splash_hktlogo_img).getImageView();
		hktlogoView.getLayoutParams().width = deviceWidth * 2/3;
		hktlogoView.requestLayout();

		ImageView pccwlogoView = aq.id(R.id.splash_pccwlogo_img).getImageView();
		pccwlogoView.getLayoutParams().width = deviceWidth * 2/3;
		pccwlogoView.requestLayout();
		aq.marginpx(R.id.splash_pccwlogo_img, 0, 0, 0, extralinespace);


		//		APIsManager.doGetAppConfigJson(me);


		// Start Version Check first
		Intent intent = new Intent(this, VersionCheckService.class);
		startService(intent);

		// Check device for Play Services APK
		if (GooglePlayServicesUtil.isGooglePlayServicesAvailable(this) == ConnectionResult.SUCCESS) {
			//Check if registration ID exists. If not, run InitGCMAsyncTask to register an ID

			//Temp Reg ID, remove after test phase
			//ClnEnv.setPref(me.getApplicationContext(), me.getString(R.string.CONST_PREF_GCM_REGID), "");
			//ClnEnv.setPref(me.getApplicationContext(), me.getString(R.string.CONST_PREF_GCM_REGID), "APA91bG2VHR5BT3p6z8vKHZTjpFxzKWo9LqLS7hZNuqM-CsgoSRPp4LT0Ef5CaAyT2bNrtmxDGMlmx8DoXs43L5QOEkY6AjbIrN3ebpRLKgDBTBwbRabOGnBEGg4zpvq-yhxsmFLgMgcUs-K1gkVyB4mlwUQ5Y8DlQ");
			//Eric nexus 7
			//ClnEnv.setPref(me.getApplicationContext(), me.getString(R.string.CONST_PREF_GCM_REGID), "APA91bEVWnTYg6z5INgQio9XEKMqx7PdeVy4uEXeGVmfaMRbqU6mFMUYOg_BnlgIZJ-tvN7AJTgZiSGKn6fkiUch73I88nkJ7hlw19AI7kBOSg6W-Benr9flIUNIs1fPCZK0oNrePbLm8yWSxAvZ6WNbhfj7kJyaRQ");

			regId = ClnEnv.getPref(me.getApplicationContext(), me.getString(R.string.CONST_PREF_GCM_REGID), "");

			if (regId.isEmpty()) {
				registerInBackground();
			}
		} else {
			//Google Play Service not available.
		}

		startTime = System.currentTimeMillis();
		bRunning = false;

		// Set Flag for Bill Message Result redirected Activity from Splash > MainMenu > ServiceList > TargetService
		if (getIntent().getBooleanExtra("BILLMSG_READNOW", false)) {
			ClnEnv.clear(me);
			if (debug) Log.d("GCM SplashActivity", "BILLMSG_READNOW arrived");
			ClnEnv.setPref(me.getApplicationContext(), me.getString(R.string.CONST_PREF_BILLMSG_NOTICEFLAG), getIntent().getBooleanExtra("BILLMSG_READNOW", false));
			if (debug) Log.d("GCM SplashActivity", "CONST_PREF_BILLMSG_NOTICEFLAG is now true");
			if (getIntent().getExtras().getSerializable("SERIAL_BILLMSG") != null) {
				if (debug) Log.d("GCM SplashActivity", "Saving intent to ClnEnv pushdatabill");
				ClnEnv.setPushDataBill((PushData) getIntent().getExtras().getSerializable("SERIAL_BILLMSG"));
			}
			getIntent().removeExtra("BILLMSG_READNOW");
			getIntent().removeExtra("SERIAL_BILLMSG");
		} else {
			ClnEnv.setPref(me.getApplicationContext(), me.getString(R.string.CONST_PREF_BILLMSG_NOTICEFLAG), getIntent().getBooleanExtra("BILLMSG_READNOW", false));
		}

		// Set Flag for General Message Result redirected Activity from Splash > MainMenu
		if (getIntent().getBooleanExtra("GENMSG_READNOW", false)) {
			ClnEnv.clear(me);
			ClnEnv.setPref(me.getApplicationContext(), me.getString(R.string.CONST_PREF_GENMSG_NOTICEFLAG), getIntent().getBooleanExtra("GENMSG_READNOW", false));
			if (getIntent().getExtras().getSerializable("SERIAL_GENMSG") != null) {
				ClnEnv.setPushDataGen((PushData) getIntent().getExtras().getSerializable("SERIAL_GENMSG"));
			}
			getIntent().removeExtra("GENMSG_READNOW");
			getIntent().removeExtra("SERIAL_GENMSG");
		}

		//derek
		new TimerAsyncTask().execute();

	}

	@Override
	protected final void onResume() {
		super.onResume();
		if (debug) Log.i("MainActivity", "onResume");

	}	

	// Android Device Back Button Handling
	public final void onBackPressed() {
		finish();
	}

	@Override
	protected void onStop() {
		super.onStop();
	}

	private void registerInBackground() {
		InitGCMAsyncTask initGCMAsyncTask = new InitGCMAsyncTask(getApplicationContext(), callbackHandler);
		initGCMAsyncTask.execute(this);
	}

	// 20121212 Vincent, Create a AsyncTask for Timer
	class TimerAsyncTask extends AsyncTask<Void, String, Void> {
		@Override
		protected void onPreExecute() {
			bRunning = true;
		}

		@Override
		protected Void doInBackground(Void... params) {
			while (bRunning) {
				// Update Timer
				Long spentTime = System.currentTimeMillis() - startTime;
				Long seconds = (spentTime / 1000) % 60;
				if (seconds >= 1 && ClnEnv.isVCFinish) bRunning = false;
				SystemClock.sleep(1000);
			}

			// Fetch LoginID and Password for auto-login, if available
			if (ClnEnv.getPref(getApplicationContext(), getString(R.string.CONST_PREF_SAVELOGINID), false) && ClnEnv.getPref(getApplicationContext(), getString(R.string.CONST_PREF_SAVEPASSWORD), false)) {
				ClnEnv.setSessionLoginID(ClnEnv.getPref(getApplicationContext(), getString(R.string.CONST_PREF_LOGINID), ""));
				ClnEnv.setSessionPassword(ClnEnv.getEncPref(getApplicationContext(), ClnEnv.getSessionLoginID(), getString(R.string.CONST_PREF_PASSWORD), ""));
				ClnEnv.setSessionPremierFlag(ClnEnv.getPref(getApplicationContext(), getString(R.string.CONST_PREF_PREMIER_FLAG), false));
			}
			return null;
		}

		protected void onPostExecute(Void result) {
			// For development, we need to check the size of log file
			if (getResources().getBoolean(R.bool.DEBUGGRQ) || getResources().getBoolean(R.bool.DEBUGCRA)) {
				ClnEnv.cleanupLogFile();
			}

			//			if (versioncheckCompleted) {
			// version check already completed. Timer should process the results
			nextScreenRouting();
			//			} else {
			// otherwise the screen just stops here waiting for the version check to take turn
			//				timerCompleted = true;
			//			}
		}
	}

	private final void tacsOrMainMenu() {
		Intent intent = null;
		finish();
		// NOTE: MUST SHOW TACS WHEN DOING DEMO, RESTORE BEFORE RELEASING
		boolean isDemo = false;

		if (isDemo) {
			intent = new Intent(getApplicationContext(), TACSActivity.class);
			startActivity(intent);
		} else {
			if (ClnEnv.getAppVersion(getApplicationContext()) != ClnEnv.getPref(getApplicationContext(), getString(R.string.CONST_PREF_TACSVERSION), 0)) {
				intent = new Intent(getApplicationContext(), TACSActivity.class);
			} else {
				intent = new Intent(getApplicationContext(), MainMenuActivity.class);
			}
//						intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
			if (debug) Log.i("MainActivity", ClnEnv.isForceCloseApp+"");
			if (!ClnEnv.isForceCloseApp) {
				startActivity(intent);
			}
		}
	}

	public final void nextScreenRouting() {

		//		if (forcedUpdate) {
		//			// The current version is blocked. Customer can only upgrade or exit
		//			displayForcedUpdateDialog();
		//		} else {
		//			if (!requiredUpdate) {
		//				// No update is required, continue the original logic
		tacsOrMainMenu();
		//			} else {
		//				// Prompt upgrade, or continue the original logic
		//				displayOptionalUpdateDialog();
		//			}
		//		}
	}

	public static final Handler	callbackHandler	= new Handler() {

		@Override
		public void handleMessage(Message msg) {
			Bundle bundle = msg.getData();


			if (InitGCMAsyncTask.asyncid.equalsIgnoreCase(bundle.getString("CALLER"))) {
				me.initGCMAsyncTask = null;
				regId = bundle.getString("REGID");
				//Save registrationID to shared preference
				ClnEnv.setPref(me.getApplicationContext(), me.getString(R.string.CONST_PREF_GCM_REGID), regId);
			}
		}
	};

	public void onSuccess(APIsResponse response) {
		//		AppConfig appconfigObj = (AppConfig) response.getCra();
		//		String myhkt_consumer_flag = appconfigObj.getMyhkt_consumer_flag();
		//		String myhkt_premier_flag = appconfigObj.getMyhkt_premier_flag();
		//		
		//		if (!"".equalsIgnoreCase(myhkt_consumer_flag)) {
		//			ClnEnv.setPref(me.getApplicationContext(), "myhkt_consumer_flag", "Y".equalsIgnoreCase(myhkt_consumer_flag) ? true : false);
		//			ClnEnv.setPref(me.getApplicationContext(), "myhkt_premier_flag", "Y".equalsIgnoreCase(myhkt_consumer_flag) ? true : false);
		//		}

		new TimerAsyncTask().execute();
	}

	public void onFail(APIsResponse response) {
		new TimerAsyncTask().execute();
	}

}
