package com.pccw.myhkt.listener;

public interface OnAccountDeleteListener {
    void onAccountDeleted();

    void onFailedDeleted();
}
