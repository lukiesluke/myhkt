/*
    Crate for Hello (and Vivi)

    Keywords
    --------
    $URL: $
    $Rev: $
    $Date: $
    $Author: $
*/

package com.pccw.dango.shared.cra;

import java.io.Serializable;

import com.pccw.dango.shared.entity.BaseUserEx;
import com.pccw.dango.shared.entity.NotAPerson;
import com.pccw.dango.shared.tool.BiTxMap;
import com.pccw.dango.shared.tool.ClnCfg;


public class HeloCra extends BaseCraEx implements Serializable 
{
    private static final long serialVersionUID = -5566845720063209827L;
    
    private SpssCra                 iSpssCra;           /* For MyMobile use                              */
    
    private ClnCfg                  oClnCfg;            /* Client Configuration                          */
    private BiTxMap                 oBiTxMap;           /* BiTx Map                                      */
    private String                  oGwtGud;            /* GWT Guard                                     */
    private String                  oBrwGud;            /* Browsing Session Guard                        */
    private BaseUserEx              oPerson;            /* Person                                        */
    private String                  oChatTok;           /* Chat Token (Only for Logged in)               */
    private String                  oMsgOnErr;          /* Msg returned when Vivi Error                  */
    private String                  oUrlOnErr;          /* Redirect to URL when Vivi Error               */
    
    public static void main(String rArg[])
    {
        System.out.println(getVer());
        return;
    }
    
    
    public static String getVer()
    {
        return ("$URL: $, $Rev: $");
    }

    
    public HeloCra()
    {
        initAndClear();
    }
    
    
    protected void init()
    {
        super.init();
    }
    
    
    public void clear()
    {
        super.clear();

        clearISpssCra();
        clearOClnCfg();
        clearOBiTxMap();
        clearOGwtGud();
        clearOBrwGud();
        clearOPerson();
        clearOChatTok();
        clearOMsgOnErr();
        clearOUrlOnErr();
    }
    

    public HeloCra copyFrom(HeloCra rSrc)
    {
        super.copyFrom(rSrc);
        
        setISpssCra(rSrc.getISpssCra());
        setOClnCfg(rSrc.getOClnCfg());
        setOBiTxMap(rSrc.getOBiTxMap());
        setOGwtGud(rSrc.getOGwtGud());
        setOBrwGud(rSrc.getOBrwGud());
        setOPerson(rSrc.getOPerson());
        setOChatTok(rSrc.getOChatTok());
        setODuct(rSrc.getODuct());
        
        return (this);
    }
    
    
    public HeloCra copyTo(HeloCra rDes)
    {
        rDes.copyFrom(this);
        return (rDes);
    }
    
    
    public HeloCra copyMe()
    {
        HeloCra                      rDes;
        
        rDes = new HeloCra();
        rDes.copyFrom(this);
        return (rDes);
    }
    
    
    public void clearISpssCra()
    {
        setISpssCra(new SpssCra());
    }
    
    
    public void setISpssCra(SpssCra rArg)
    {
        iSpssCra = rArg;
    }
    
    
    public SpssCra getISpssCra()
    {
        return (iSpssCra);
    }
    
    
    public void clearOClnCfg()
    {
        setOClnCfg(new ClnCfg());
    }
    
    
    public void setOClnCfg(ClnCfg rArg)
    {
        oClnCfg = rArg;
    }
    
    
    public ClnCfg getOClnCfg()
    {
        return (oClnCfg);
    }
    
    
    public void clearOBiTxMap()
    {
        setOBiTxMap(new BiTxMap());
    }
    
    
    public void setOBiTxMap(BiTxMap rArg)
    {
        oBiTxMap = rArg;
    }
    
    
    public BiTxMap getOBiTxMap()
    {
        return (oBiTxMap);
    }
    
    
    public void clearOGwtGud()
    {
        setOGwtGud("");
    }
    
    
    public void setOGwtGud(String rArg)
    {
        oGwtGud = rArg;
    }
    
    
    public String getOGwtGud()
    {
        return (oGwtGud);
    }
    
    
    public void clearOBrwGud()
    {
        setOBrwGud("");
    }
    
    
    public void setOBrwGud(String rArg)
    {
        oBrwGud = rArg;
    }
    
    
    public String getOBrwGud()
    {
        return (oBrwGud);
    }

    
    public void clearOPerson()
    {
        setOPerson(new NotAPerson());
    }
    
    
    public void setOPerson(BaseUserEx rArg)
    {
        oPerson = rArg;
    }
    
    
    public BaseUserEx getOPerson()
    {
        return (oPerson);
    }
    
    
    public void clearOChatTok()
    {
        setOChatTok("");
    }
    
    
    public void setOChatTok(String rArg)
    {
        oChatTok = rArg;
    }
    
    
    public String getOChatTok()
    {
        return (oChatTok);
    }    
   
    
    public void clearOMsgOnErr()
    {
        setOMsgOnErr("");
    }
    
    
    public void setOMsgOnErr(String rArg)
    {
        oMsgOnErr = rArg;
    }
    
    
    public String getOMsgOnErr()
    {
        return (oMsgOnErr);
    }
    
    
    public void clearOUrlOnErr()
    {
        setOUrlOnErr("");
    }
    
    
    public void setOUrlOnErr(String rArg)
    {
        oUrlOnErr = rArg;
    }
    
    
    public String getOUrlOnErr()
    {
        return (oUrlOnErr);
    }
}
