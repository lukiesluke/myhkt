package com.pccw.dango.shared.g3entity;

import java.io.Serializable;

//import javax.xml.bind.annotation.XmlRootElement;


//@XmlRootElement(name = "G3ServiceCallBarStatusWS")
public class G3ServiceCallBarStatusWS implements Serializable
{
    private static final long serialVersionUID = 2062206852754129256L;
    
    private String rtnCd;
    private String rtnMsg;
    private G3ServiceCallBarStatusDTO g3ServiceCallBarStatusDTO = new G3ServiceCallBarStatusDTO();
    
    public String getRtnCd() {
        return rtnCd;
    }
    public void setRtnCd(String rtnCd) {
        this.rtnCd = rtnCd;
    }
    public String getRtnMsg() {
        return rtnMsg;
    }
    public void setRtnMsg(String rtnMsg) {
        this.rtnMsg = rtnMsg;
    }
    public G3ServiceCallBarStatusDTO getG3ServiceCallBarStatusDTO() {
        return g3ServiceCallBarStatusDTO;
    }
    public void setG3ServiceCallBarStatusDTO(
            G3ServiceCallBarStatusDTO g3ServiceCallBarStatusDTO) {
        this.g3ServiceCallBarStatusDTO = g3ServiceCallBarStatusDTO;
    }

    
}
