package com.pccw.myhkt;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.Transformation;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.LinearLayout.LayoutParams;

public class ViewUtils {
	public static Bitmap scaleToFitHeight(Context ctx, int res , int givenH){

		Bitmap bm = BitmapFactory.decodeResource(ctx.getResources(), res);
		int height = bm.getHeight() ;
		int width = bm.getWidth();
		width = width * givenH / height;
		Bitmap b = Bitmap.createScaledBitmap(bm, width, givenH, false);
		bm = null;
		return b;
	}
	
	public static int heightOfScaleToFitHeight(Context ctx, int res , int givenH){

		Bitmap bm = BitmapFactory.decodeResource(ctx.getResources(), res);
		int height = bm.getHeight() ;
		int width = bm.getWidth();
//		Log.i("ImageH", givenH+"/"+height +"/"+ width);
		width = width * givenH / height;
//		Log.i("ImageW",width+"");
		return width;
	}


	// Animation of Expand Info
	public static void expand(final View v) {
		v.measure(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT);
		final int targtetHeight = v.getMeasuredHeight();

		v.getLayoutParams().height = 0;
		v.setVisibility(View.VISIBLE);
		Animation a = new Animation() {
			@Override
			protected void applyTransformation(float interpolatedTime, Transformation t) {
				v.getLayoutParams().height = interpolatedTime == 1 ? LayoutParams.WRAP_CONTENT : (int) (targtetHeight * interpolatedTime);
				v.requestLayout();
			}

			@Override
			public boolean willChangeBounds() {
				return true;
			}
		};

		// 1dp/ms
		a.setDuration((int) (targtetHeight / v.getContext().getResources().getDisplayMetrics().density) * 3);
		v.startAnimation(a);
	}

	public static void collapse(final View v) {
		final int initialHeight = v.getMeasuredHeight();

		Animation a = new Animation() {
			@Override
			protected void applyTransformation(float interpolatedTime, Transformation t) {
				if (interpolatedTime == 1) {
					v.setVisibility(View.GONE);
				} else {
					v.getLayoutParams().height = initialHeight - (int) (initialHeight * interpolatedTime);
					v.requestLayout();
				}
			}

			@Override
			public boolean willChangeBounds() {
				return true;
			}
		};

		// 1dp/ms
		a.setDuration((int) (initialHeight / v.getContext().getResources().getDisplayMetrics().density) * 3);
		v.startAnimation(a);
	}
	
	//set margin
	public static void marginpx(View view, int left, int top, int right, int bottom) {
		if (view.getLayoutParams() instanceof LinearLayout.LayoutParams) {
			LinearLayout.LayoutParams params = (LinearLayout.LayoutParams)view.getLayoutParams();
			params.leftMargin = left;
			params.topMargin = top;
			params.rightMargin = right;
			params.bottomMargin = bottom;
			view.setLayoutParams(params);
			params = null;

		} else if (view.getLayoutParams() instanceof RelativeLayout.LayoutParams) {
			RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams)view.getLayoutParams();
			params.leftMargin = left;
			params.topMargin = top;
			params.rightMargin = right;
			params.bottomMargin = bottom;
			view.setLayoutParams(params);
			params = null;			
		}	
	}
	
	
	
}
