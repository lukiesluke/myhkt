package com.pccw.myhkt.fragment;

import java.lang.Thread.UncaughtExceptionHandler;
import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.graphics.Point;
import android.graphics.Typeface;
import android.os.Bundle;
import androidx.fragment.app.FragmentTransaction;
import android.text.TextUtils;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.pccw.dango.shared.cra.PlanTvCra;
import com.pccw.dango.shared.entity.SrvPlan;
import com.pccw.dango.shared.tool.RC;
import com.pccw.myhkt.APIsManager;
import com.pccw.myhkt.APIsResponse;
import com.pccw.myhkt.ClnEnv;
import com.pccw.myhkt.DialogHelper;
import com.pccw.biz.myhkt.R;
import com.pccw.myhkt.Tool;
import com.pccw.myhkt.Utils;
import com.pccw.myhkt.activity.BaseActivity;
import com.pccw.myhkt.cell.model.ArrowTextCell;
import com.pccw.myhkt.cell.model.Cell;
import com.pccw.myhkt.cell.model.SmallTextCell;
import com.pccw.myhkt.cell.view.CellViewAdapter;
import com.pccw.myhkt.lib.ui.AAQuery;
import com.pccw.myhkt.lib.ui.ScalableImageView;

/************************************************************************
 * File : PlanTVFragment.java
 * Desc : Plan view for TV service
 * Name : PlanTVFragment
 * by 	: Derek Tsui
 * Date : 18/2/2016
 * 
 * Change History:
 * Date       Modified By		Description
 * ---------- ----------------	-------------------------------
 * 18/2/2016  Derek Tsui 		-First draft
 * 06/6/2017  Abdulmoiz Esmail	-Integrated Array of VAS (Value Added Services)
 *************************************************************************/

public class PlanTVFragment extends BaseServiceFragment {
	private PlanTVFragment me;
	private AAQuery aq;
	private FragmentTransaction ft;
	private View myView;
	private int colWidth ;

	private List<Cell>				cellList;

	private PlanTvCra planTvCra;
	private CellViewAdapter cellViewAdapter;
	private LinearLayout frame;
	private Cell cellLine;
	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
	}

	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		me = this;
		View fragmentLayout = inflater.inflate(R.layout.fragment_plantv, container, false);
		myView = fragmentLayout;
		initData();
		return fragmentLayout;
	}


	@Override
	public void onStart() {
		super.onStart();
	}

	@Override
	public void onResume(){
		super.onResume();
	}

	@Override
	public void setUserVisibleHint(boolean isFragmentVisible) {
		super.setUserVisibleHint(isFragmentVisible);

		if (isFragmentVisible) {
			//Screen Tracker
			setModuleId();
			// we check that the fragment is becoming visible
			initTvPlanUI();
			doGetPlanTv();
		}
	}

	protected void initData() {    		
		super.initData();
		aq = new AAQuery(myView);
		cellViewAdapter = new CellViewAdapter(this);
		colWidth = (deviceWidth - extralinespace *2 )/5;
	}

	protected void initUI(){
	}

	protected void setModuleId(){
		callback_livechat.setModuleId(getResString(R.string.MODULE_TV_PLAN));
		super.setModuleId();
	}

	public void refreshData(){
	}

	private void initTvPlanUI() {
		aq.id(R.id.fragment_plantv_sv).backgroundColorId(R.color.white);
		aq.marginpx(R.id.fragment_plantv_sv, 0, 0, 0, 0);
		frame = (LinearLayout) aq.id(R.id.fragment_plantv_frame).getView();

		int colWidthXL = (deviceWidth - extralinespace *2 )/3;
		int colWidthL = (deviceWidth - extralinespace *2 )/ 5;
		int colWidthS = (deviceWidth - extralinespace *2 )/6;

		Cell cellLine = new Cell(Cell.LINE);
		cellLine.setLeftPadding(basePadding);
		cellLine.setRightPadding(basePadding);

		int[] widthText = {colWidthL, colWidthL};
		cellList = new ArrayList<Cell>();
		String[] nowContent = {getString(R.string.MYHKT_PLN_LBL_EXPIRY_DATE),getString(R.string.MYHKT_PLN_LBL_DOLLAR_AMOUNT)};
		String[] nowDetails = {"--/--/--", "----"};
		ArrowTextCell cell1 = new ArrowTextCell(getResString(R.string.MYHKT_PLN_LBL_BONUS), nowContent, null, widthText);
		cell1.setTitleColorId(R.color.hkt_textcolor_orange);
		cell1.setTitleTypeface(Typeface.BOLD);
		cell1.setContentSizeDelta(-4);
		cellList.add(cell1);
		ArrowTextCell cell2 = new ArrowTextCell(getResString(R.string.MYHKT_PLN_LBL_NOW_DOLLAR), nowDetails, null, widthText);
		cell2.setContentColorId(R.color.hkt_textcolor);
		cell2.setTitleColorId(R.color.hkt_txtcolor_grey);
		cellList.add(cell2);
		cellList.add(cellLine);
		//Header
		SmallTextCell cell3 = new SmallTextCell(getResString(R.string.PLNTVF_SRV_PL_DTL), null);
		cell3.setTitleColorId(R.color.black);
		cell3.setLeftPadding(basePadding);
		cell3.setRightPadding(basePadding);
		cellList.add(cell3);
		SmallTextCell cell4 = new SmallTextCell(getResString(R.string.PLNTVM_NOSVC), null);
		cell4.setTitleColorId(R.color.hkt_txtcolor_grey);
		cell4.setTitleSizeDelta(-2);
		cell4.setLeftPadding(basePadding);
		cell4.setRightPadding(basePadding);
		cellList.add(cell4);
		cellList.add(cellLine);
		
		//Moiz [060117] : Added Value Added Services header
		//VAS header
		SmallTextCell valueAdderSrvcsHeader = new SmallTextCell(getResString(R.string.PLNIMF_VAS), null);
		valueAdderSrvcsHeader.setTitleColorId(R.color.black);
		valueAdderSrvcsHeader.setLeftPadding(basePadding);
		valueAdderSrvcsHeader.setRightPadding(basePadding);
		cellList.add(valueAdderSrvcsHeader);
		//NO VAS message
		SmallTextCell valueAddedSrvcsEmptyMsg = new SmallTextCell(getResString(R.string.PLNIMM_NOVAS), null);
		valueAddedSrvcsEmptyMsg.setTitleColorId(R.color.hkt_txtcolor_grey);
		valueAddedSrvcsEmptyMsg.setTitleSizeDelta(-2);
		valueAddedSrvcsEmptyMsg.setLeftPadding(basePadding);
		valueAddedSrvcsEmptyMsg.setRightPadding(basePadding);
		cellList.add(valueAddedSrvcsEmptyMsg);

		cellList.add(cellLine);
		cellViewAdapter.setView(frame, cellList);
	}

	private void prepareTvPlan() {
		boolean isBillExist = false;
		String totalChg = "";
		String dueDate = null;
		String currentBal = "";
		int colWidthXL = (deviceWidth - extralinespace *2 )/3;
		int colWidthL = (deviceWidth - extralinespace *2 )/ 5;
		int colWidthS = (deviceWidth - extralinespace *2 )/6;
		int[] widthText = {colWidth, colWidth};
		int[] widthText3 = {colWidthXL, colWidthS, colWidthL};
		Cell cellLine = new Cell(Cell.LINE);
		cellLine.setTopPadding(basePadding);
		cellLine.setLeftPadding(basePadding);
		cellLine.setRightPadding(basePadding);
		cellList = new ArrayList<Cell>();
		aq = new AAQuery(myView);

		//Check PON coverage, show PON coverage image if available
		if (planTvCra.getOPonCvg()) {
			aq.id(R.id.fragment_plantv_banner).visibility(View.VISIBLE);
			//			aq.id(R.id.banner_fib_btn).clicked(this, "onClick");
		}

		String nowExpdt = "".equals(planTvCra.getONowDollExp()) ? "--/--/--" : Utils.toDateString(planTvCra.getONowDollExp(), getString(R.string.input_datetime_format), "dd/MM/yy");
		String nowBal = "".equals(planTvCra.getONowDollExp()) ? "----" : Utils.convertStringToPrice0dec(planTvCra.getONowDollBal());
		String[] title = {getResString(R.string.usage_entitled), getResString(R.string.usage_used)};
		String[] nowContent = {getString(R.string.MYHKT_PLN_LBL_EXPIRY_DATE),getString(R.string.MYHKT_PLN_LBL_DOLLAR_AMOUNT)};
		String[] nowDetails = {nowExpdt, nowBal};
		String[] planContent = {getString(R.string.PLNTVF_STDT), getString(R.string.PLNTVF_ENDT), getString(R.string.PLNTVF_COMP)};
		String[] planNote = {getString(R.string.PLNTVF_STDT_FORMAT), getString(R.string.PLNTVF_STDT_FORMAT), getString(R.string.PLNTVF_MONTH)};
		//		String[] minNormal =  {convertNum(g3UsageQuotaDTO.getLocalVoiceInterEntitlement()),convertNum(g3UsageQuotaDTO.getLocalVoiceInterUsage())};

		String[] vasHeaderLabels = {getString(R.string.PLNIMF_STDT), getString(R.string.PLNTVF_ENDT)}; //MOIZ
		String[] vasHeaderFormat = {getString(R.string.PLNTVF_STDT_FORMAT), getString(R.string.PLNTVF_STDT_FORMAT)}; //MOIZ

		
		//Bouns Detail part
		//Header
		ArrowTextCell cell1 = new ArrowTextCell(getResString(R.string.MYHKT_PLN_LBL_BONUS), nowContent, null, widthText);
		cell1.setTitleColorId(R.color.hkt_textcolor_orange);
		cell1.setTitleTypeface(Typeface.BOLD);
		cell1.setContentSizeDelta(-4);
		cellList.add(cell1);
		//Now Dallor
		ArrowTextCell cell2 = new ArrowTextCell(getResString(R.string.MYHKT_PLN_LBL_NOW_DOLLAR), nowDetails, null, widthText);
		cell2.setContentColorId(R.color.hkt_textcolor);
		cell2.setTitleColorId(R.color.hkt_txtcolor_grey);
		cellList.add(cell2);
		//seperator line
		cellList.add(cellLine);
			
		if (planTvCra.getOPlanAry().length > 0) {
			//Services Detail 
			//Header
			SmallTextCell cell4 = new SmallTextCell(getResString(R.string.PLNTVF_SRV_PL_DTL), null);
			cell4.setTitleColorId(R.color.black);
			cell4.setLeftPadding(basePadding);
			cell4.setRightPadding(basePadding);
			cellList.add(cell4);
	
			//Columns Header
			ArrowTextCell cell5 = new ArrowTextCell(null, planContent, planNote , widthText3);
			cell5.setTitleColorId(R.color.hkt_textcolor_orange);
			cell5.setTitleTypeface(Typeface.BOLD);
			cell5.setContentSizeDelta(-6);
			cell5.setNoteSizeDelta(-6);
			cellList.add(cell5);
			//		cellList.add(cellLine);
	
			//prepare plan 
			//		ArrowTextCell cell6 = new ArrowTextCell("Channel Offer", null, null, null);
			//		cellList.add(cell6);
			String stdt = "--";
			String enddt = "--";
			String cmmtprd = "--";
			for (int i = 0; i < planTvCra.getOPlanAry().length; i++) {
				//heading
				if (!"".equalsIgnoreCase(planTvCra.getOPlanAry()[i].getStDt()) && "Y".equalsIgnoreCase(planTvCra.getOPlanAry()[i].getSummInd())) {
					stdt = "".equals(planTvCra.getOPlanAry()[i].getStDt()) ? "--" : Tool.formatDate(planTvCra.getOPlanAry()[i].getStDt(), "dd/MM/yy");
					enddt = "".equals(planTvCra.getOPlanAry()[i].getEnDt()) ? "--" : Tool.formatDate(planTvCra.getOPlanAry()[i].getEnDt(), "dd/MM/yy");
					cmmtprd = "".equals(planTvCra.getOPlanAry()[i].getSrvCmmtPrd()) ? "--" : planTvCra.getOPlanAry()[i].getSrvCmmtPrd()  + " " + Utils.getString(me.getActivity(), R.string.DLGM_MONTH);
	
					Cell cellLine1 = new Cell(Cell.LINE);
					cellLine1.setPadding(basePadding);
					if (i ==0)  {
						cellLine1.setTopPadding(0); 
					} else {
						cellLine1.setTopPadding(basePadding);
					}
					cellList.add(cellLine1);
	
					ArrowTextCell cell = new ArrowTextCell(planTvCra.getOPlanAry()[i].getDesnEn(), null, null, null);
					cell.setArrowShown(true);
					cell.setTitleSizeDelta(0);
					cellList.add(cell);
	
					// 2.0 Start Date, End date / Period
					//				if (!"Y".equalsIgnoreCase(planTvCra.getOPlanAry()[i].getSummInd())) {
					//					int bgColor = Tool.isOddNumer(i) ? R.color.white : R.color.cell_bg_grey;
					//					cell = new ArrowTextCell(planTvCra.getOPlanAry()[i].getDesnEn(), planContent, null, widthText3, bgColor);
					//	 				cellList.add(cell);
					//				}
					
				} else if (!"Y".equalsIgnoreCase(planTvCra.getOPlanAry()[i].getSummInd()) && !"".equalsIgnoreCase(planTvCra.getOPlanAry()[i].getStDt())) {
					Cell cellLine1 = new Cell(Cell.LINE);
					cellLine1.setPadding(basePadding);
					if (i ==0)  {
						cellLine1.setTopPadding(0);
						cellList.add(cellLine1);
					} 
					
					//special case handling like "DTT"
					stdt = "".equals(planTvCra.getOPlanAry()[i].getStDt()) ? "--" : Tool.formatDate(planTvCra.getOPlanAry()[i].getStDt(), "dd/MM/yy");
					enddt = "".equals(planTvCra.getOPlanAry()[i].getEnDt()) ? "--" : Tool.formatDate(planTvCra.getOPlanAry()[i].getEnDt(), "dd/MM/yy");
					cmmtprd = "".equals(planTvCra.getOPlanAry()[i].getSrvCmmtPrd()) ? "--" : planTvCra.getOPlanAry()[i].getSrvCmmtPrd()  + " " + Utils.getString(me.getActivity(), R.string.DLGM_MONTH);
	
					int bgColor = Tool.isOddNumer(i) ? R.color.white : R.color.cell_bg_grey;
					String[] packValues = {stdt, enddt, cmmtprd};
					ArrowTextCell cella = new ArrowTextCell(planTvCra.getOPlanAry()[i].getDesnEn(), packValues, null, widthText3, bgColor);
					cella.setContentColorId(R.color.hkt_textcolor);
					cella.setTitleColorId(R.color.hkt_txtcolor_grey);
					cella.setContentSizeDelta(-5);
					cellList.add(cella);
				} else if (!"Y".equalsIgnoreCase(planTvCra.getOPlanAry()[i].getSummInd()) && "".equalsIgnoreCase(planTvCra.getOPlanAry()[i].getStDt())) {
					Cell cellLine1 = new Cell(Cell.LINE);
					cellLine1.setPadding(basePadding);
					if (i ==0)  {
						cellLine1.setTopPadding(0);
						cellList.add(cellLine1);
					} 
					
					int bgColor = Tool.isOddNumer(i) ? R.color.cell_bg_grey : R.color.white;
					String[] packValues = {stdt, enddt, cmmtprd};
					ArrowTextCell cella = new ArrowTextCell(planTvCra.getOPlanAry()[i].getDesnEn(), packValues, null, widthText3, bgColor);
					cella.setContentColorId(R.color.hkt_textcolor);
					cella.setTitleColorId(R.color.hkt_txtcolor_grey);
					cella.setContentSizeDelta(-5);
					cellList.add(cella);
				}
			}		
	
			cellList.add(cellLine);
//			cellViewAdapter.setView(frame, cellList);
		} else {
			//Header
			SmallTextCell cell3 = new SmallTextCell(getResString(R.string.PLNTVF_SRV_PL_DTL), null);
			cell3.setTitleColorId(R.color.black);
			cell3.setLeftPadding(basePadding);
			cell3.setRightPadding(basePadding);
			cellList.add(cell3);
			SmallTextCell cell4 = new SmallTextCell(getResString(R.string.PLNTVM_NOSVC), null);
			cell4.setTitleColorId(R.color.hkt_txtcolor_grey);
			cell4.setTitleSizeDelta(-2);
			cell4.setLeftPadding(basePadding);
			cell4.setRightPadding(basePadding);
			cellList.add(cell4);
			
			cellList.add(cellLine);
		}
		
		//MOIZ: Value Added Service value
		if(planTvCra.getOVasAry() != null && planTvCra.getOVasAry().length > 0) {
			
			SmallTextCell valueAdderSrvcsHeader = new SmallTextCell(getResString(R.string.PLNIMF_VAS), null);
			valueAdderSrvcsHeader.setTitleColorId(R.color.black);
			valueAdderSrvcsHeader.setLeftPadding(basePadding);
			valueAdderSrvcsHeader.setRightPadding(basePadding);
			cellList.add(valueAdderSrvcsHeader);
			
			//Columns Header
			ArrowTextCell vasColumnHeader = new ArrowTextCell(null, vasHeaderLabels, vasHeaderFormat , widthText);
			vasColumnHeader.setTitleColorId(R.color.hkt_textcolor_orange);
			vasColumnHeader.setTitleTypeface(Typeface.BOLD);
			vasColumnHeader.setContentSizeDelta(-6);
			vasColumnHeader.setNoteSizeDelta(-6);
			cellList.add(vasColumnHeader);
			
			String stdt = "--";
			String enddt = "--";
			

			for (int i = 0; i < planTvCra.getOVasAry().length; i++) {
				
				SrvPlan vasSrvPlan = planTvCra.getOVasAry()[i];
				if(vasSrvPlan != null && !TextUtils.isEmpty(vasSrvPlan.getDesnEn())) {
					
					Cell cellLine1 = new Cell(Cell.LINE);
					cellLine1.setPadding(basePadding);
					if (i ==0)  {
						cellLine1.setTopPadding(0);
						cellList.add(cellLine1);
					} 
					
					stdt = TextUtils.isEmpty(vasSrvPlan.getStDt()) ? "--" : Tool.formatDate(vasSrvPlan.getStDt(), "dd/MM/yy");
					enddt = TextUtils.isEmpty(vasSrvPlan.getEnDt()) ? "--" : Tool.formatDate(vasSrvPlan.getEnDt(), "dd/MM/yy");
	
					int bgColor = Tool.isOddNumer(i) ? R.color.white : R.color.cell_bg_grey;
					String[] packValues = {stdt, enddt};
					ArrowTextCell cella = new ArrowTextCell(vasSrvPlan.getDesnEn(), packValues, null, widthText3, bgColor);
					cella.setContentColorId(R.color.hkt_textcolor);
					cella.setTitleColorId(R.color.hkt_txtcolor_grey);
					cella.setContentSizeDelta(-5);
					cellList.add(cella);
				}
			}
			
		} else {
			
			//VAS header
			SmallTextCell valueAdderSrvcsHeader = new SmallTextCell(getResString(R.string.PLNIMF_VAS), null);
			valueAdderSrvcsHeader.setTitleColorId(R.color.black);
			valueAdderSrvcsHeader.setLeftPadding(basePadding);
			valueAdderSrvcsHeader.setRightPadding(basePadding);
			cellList.add(valueAdderSrvcsHeader);
			//NO VAS message
			SmallTextCell valueAddedSrvcsEmptyMsg = new SmallTextCell(getResString(R.string.PLNIMM_NOVAS), null);
			valueAddedSrvcsEmptyMsg.setTitleColorId(R.color.hkt_txtcolor_grey);
			valueAddedSrvcsEmptyMsg.setTitleSizeDelta(-2);
			valueAddedSrvcsEmptyMsg.setLeftPadding(basePadding);
			valueAddedSrvcsEmptyMsg.setRightPadding(basePadding);
			cellList.add(valueAddedSrvcsEmptyMsg);
			cellList.add(cellLine);
		}
		cellViewAdapter.setView(frame, cellList);
	}

	private void doGetPlanTv() {
		String loginId;
		if (!ClnEnv.isLoggedIn()) {
			loginId = "";
		} else {
			loginId = ClnEnv.getQualSvee().getSveeRec().loginId;
		}

		PlanTvCra planTvCra = new PlanTvCra();
		planTvCra.setILoginId(loginId);
		planTvCra.setISubnRec(callback_main.getSubnRec().copyMe());

		APIsManager.doGetPlanTv(me, planTvCra);
	}

	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.banner_fib_img:
			openLiveChat(Utils.getString(me.getActivity(), R.string.MODULE_TV_PONCHK));	
			break;
		}
	}

	@Override
	public void initPonBanner() {
		RelativeLayout rl = (RelativeLayout) aq.id(R.id.banner_pon_layout).getView();
		ScalableImageView ivFib = rl.findViewById(R.id.banner_fib_img);
		ivFib.setOnClickListener(this);
	}

	protected void openLiveChat(String tempModuled) {
		if(callback_livechat != null) {
			callback_livechat.openLiveChat(tempModuled);
		}
	}

	@Override
	public void onSuccess(APIsResponse response) {
		planTvCra = (PlanTvCra) response.getCra();
		prepareTvPlan();
	}

	@Override
	public void onFail(APIsResponse response) {
		// General Error Message
		if (!"".equals(response.getMessage()) && response.getMessage() != null) {
			DialogHelper.createSimpleDialog(getActivity(),  response.getMessage());
		} else if (RC.TCSESS_MM.equalsIgnoreCase(response.getReply().getCode())) {
			BaseActivity.ivSessDialog();
		} else {
			DialogHelper.createSimpleDialog(getActivity(),  ClnEnv.getRPCErrMsg(getActivity(), response.getReply().getCode()));
		}		
	}

}
