package com.pccw.myhkt.model;


import java.util.List;

public class Email {
    private String monthTitle;
    private List<Object> emailContentList;

    public String getMonthTitle() {
        return monthTitle;
    }

    public void setMonthTitle(String monthTitle) {
        this.monthTitle = monthTitle;
    }


    public List<Object> getChildObjectList() {
        return this.emailContentList;
    }

    public void setChildObjectList(List<Object> list) {
        this.emailContentList = list;
    }
}
