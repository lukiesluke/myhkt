package com.pccw.myhkt.cell.model;

import com.pccw.biz.myhkt.R;

import android.util.Log;

public class CircleViewCell extends Cell{
	
	private int progress;
	private int barColor = R.color.cprogressbar_blue;
	
	
	public CircleViewCell(String title, String content, int progress){
		type = Cell.CIRCLEVIEW;
		titleColorId = R.color.cprogressbar_blue;
		contentColorId = R.color.black;
		this.title = title;
		this.content = content;
		this.progress = progress;
	}
	
	public int getProgress() {
		return progress;
	}
	public void setProgress(int progress) {
		this.progress = progress;
	}	
	public int getBarColor() {
		return barColor;
	}

	public void setBarColor(int barColor) {
		this.barColor = barColor;
	}
}