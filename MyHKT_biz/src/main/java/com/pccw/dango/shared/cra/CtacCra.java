/*
    Crate for Contact
    
    Keywords
    --------
    $URL: $
    $Rev: $
    $Date: $
    $Author: $
*/

package com.pccw.dango.shared.cra;

import java.io.Serializable;

import com.pccw.dango.shared.entity.Ctac;

public class CtacCra extends BaseCraEx implements Serializable
{
    private static final long serialVersionUID = -8527955646478047451L;
    
    private String                  iLoginId;           /* Login ID                                      */
    private Ctac                    iDrgCtac;           /* Drangon - Contact Info                        */
    private Ctac                    iImsCtac;           /* IMS - Contact Info                            */
    private Ctac                    iMobCtac;           /* MOB - Contact Info                            */
    
    private Ctac                    oDrgCtac;           /* Drangon - Contact Info                        */
    private Ctac                    oImsCtac;           /* IMS - Contact Info                            */
    private Ctac                    oMobCtac;           /* MOB - Contact Info                            */


    public static void main(String rArg[])
    {
        System.out.println(getVer());
        return;
    }
    
    
    public static String getVer()
    {
        return ("$URL: $, $Rev: $");
    }

    
    public CtacCra()
    {
        initAndClear();
    }


    protected void init()
    {
        super.init();
    }


    public void clear()
    {
        super.clear();
        
        clearILoginId();
        clearIDrgCtac();
        clearIImsCtac();
        clearIMobCtac();
        clearODrgCtac();
        clearOImsCtac();
        clearOMobCtac();
    }


    public CtacCra copyFrom(CtacCra rSrc)
    {
        super.copyFrom(rSrc);
        
        setILoginId(rSrc.getILoginId());
        setIDrgCtac(rSrc.getIDrgCtac());
        setIImsCtac(rSrc.getIImsCtac());
        setIMobCtac(rSrc.getIMobCtac());
        setODrgCtac(rSrc.getODrgCtac());
        setOImsCtac(rSrc.getOImsCtac());
        setOMobCtac(rSrc.getOMobCtac());

        return (this);
    }


    public CtacCra copyTo(CtacCra rDes)
    {
        rDes.copyFrom(this);
        return (rDes);
    }
    
    
    public CtacCra copyMe()
    {
        CtacCra                     rDes;

        rDes = new CtacCra();
        rDes.copyFrom(this);
        return (rDes);
    }


    public void clearILoginId()
    {
        iLoginId = "";
    }


    public void setILoginId(String rArg)
    {
        iLoginId = rArg;
    }


    public String getILoginId()
    {
        return (iLoginId);
    }


    public void clearIDrgCtac()
    {
        iDrgCtac = new Ctac();
    }


    public void setIDrgCtac(Ctac rArg)
    {
        iDrgCtac = rArg;
    }


    public Ctac getIDrgCtac()
    {
        return (iDrgCtac);
    }


    public void clearIImsCtac()
    {
        iImsCtac = new Ctac();
    }


    public void setIImsCtac(Ctac rArg)
    {
        iImsCtac = rArg;
    }


    public Ctac getIImsCtac()
    {
        return (iImsCtac);
    }


    public void clearIMobCtac()
    {
        iMobCtac = new Ctac();
    }


    public void setIMobCtac(Ctac rArg)
    {
        iMobCtac = rArg;
    }


    public Ctac getIMobCtac()
    {
        return (iMobCtac);
    }


    public void clearODrgCtac()
    {
        oDrgCtac = new Ctac();
    }


    public void setODrgCtac(Ctac rArg)
    {
        oDrgCtac = rArg;
    }


    public Ctac getODrgCtac()
    {
        return (oDrgCtac);
    }


    public void clearOImsCtac()
    {
        oImsCtac = new Ctac();
    }


    public void setOImsCtac(Ctac rArg)
    {
        oImsCtac = rArg;
    }


    public Ctac getOImsCtac()
    {
        return (oImsCtac);
    }


    public void clearOMobCtac()
    {
        oMobCtac = new Ctac();
    }


    public void setOMobCtac(Ctac rArg)
    {
        oMobCtac = rArg;
    }


    public Ctac getOMobCtac()
    {
        return (oMobCtac);
    }
}
