package com.pccw.myhkt.lib.ui;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.drawable.Drawable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.androidquery.AQuery;
import com.pccw.myhkt.ClnEnv;
import com.pccw.biz.myhkt.R;

public class PopOverInputView extends LinearLayout {
	private AQuery aq;
	private TextView textView;

	public PopOverInputView(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
	}

	public PopOverInputView(Context context, AttributeSet attrs) {
		super(context, attrs);
		initViews(context, attrs);
		setOnTouch();
	}

	public PopOverInputView(Context context, AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);
		initViews(context, attrs);
		setOnTouch();
	}

	private void initViews(final Context context, AttributeSet attrs) {
		TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.HomeImageButton, 0, 0);
		aq = new AQuery(this);

		LayoutInflater.from(context).inflate(R.layout.popover_input_view, this);
		try {
			// Style Initialize
			String text = (String) a.getText(R.styleable.RegInputItem_title1);
			textView = aq.id(R.id.popover_input_layouttxt).getTextView();
			aq.id(R.id.popover_input_layouttxt).text(text!=null ? text :"");
			//			aq.id(R.id.home_imageview).image(a.getDrawable(R.styleable.HomeImageButton_android_src));
			Drawable drawable = getContext().getResources().getDrawable(R.drawable.btn_arrowdown);
			drawable.setBounds(0, 0, drawable.getMinimumWidth(), drawable.getMinimumHeight());

			if (ClnEnv.isMyMobFlag()) {
				aq.id(R.id.popover_input_layout).background(R.drawable.hkt_edittextorange_bg);
				ColorFilter filter = new PorterDuffColorFilter(Color.parseColor("#F9A619"), PorterDuff.Mode.MULTIPLY); //hkt_textcolor_orange
				drawable.setColorFilter(filter);
				if (ClnEnv.isIs101Flag()) {
					aq.id(R.id.popover_input_layout).background(R.drawable.hkt_edittextblack_bg);
					filter = new PorterDuffColorFilter(Color.parseColor("#000000"), PorterDuff.Mode.MULTIPLY); //black
					drawable.setColorFilter(filter);
				}
			} else {
				setWhiteBackground();
			}

			aq.id(R.id.popover_input_layouttxt).getTextView().setCompoundDrawables(null, null, drawable, null);
		} finally {
			a.recycle();
		}
	}



	public void initViews(final Context context, String title) {
		initViews(context, (int)this.getResources().getDimension(R.dimen.reg_input_height), title) ;
		setOnTouch();
	}

	public void initViews(final Context context, int height, String title) {
		LayoutInflater.from(context).inflate(R.layout.edittext_reg_input, this, false);

		aq.id(R.id.popover_input_layouttxt).text(title);
		//		aq.id(R.id.popover_input_layouttxt).textColorId(R.color.hkt_buttonblue);
		//		if (title ==null || title.equals("")){ aq.id(R.id.reg_input_layouttxt).gone(); }

		aq.id(R.id.popover_input_layouttxt).height(height/2, false);
	}

	public void setText(String text) {
		aq.id(R.id.popover_input_layouttxt).text(text);
	}

	public String getText() {
		return aq.id(R.id.popover_input_layouttxt).getText().toString();
	}

	public void setWhiteBackground() {
		aq.id(R.id.popover_input_layout).background(R.drawable.hkt_edittextwhite_bg);
	}

	private  void setOnTouch(){
		setOnTouchListener(new OnTouchListener(){
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				switch (event.getAction()) {
				case MotionEvent.ACTION_DOWN: {
//					Log.i("", "Down");
					int		m_color				= Color.argb(128, 0, 0, 0);
					aq.id(R.id.popover_input_layout).getView().getBackground().setColorFilter(m_color, PorterDuff.Mode.SRC_ATOP);

					break;
				}
				case MotionEvent.ACTION_CANCEL:
				case MotionEvent.ACTION_UP: {
//					Log.i("", "UP");
					aq.id(R.id.popover_input_layout).getView().getBackground().clearColorFilter();
				}
				}
				return false;
			}
		});
	}


	public void addTextChangedListener(TextWatcher textWatcher) {
		textView.setTextColor(getResources().getColor(R.color.blue));
	}
}
