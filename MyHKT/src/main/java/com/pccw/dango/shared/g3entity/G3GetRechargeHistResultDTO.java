package com.pccw.dango.shared.g3entity;

import java.io.Serializable;
import java.util.List;

public class G3GetRechargeHistResultDTO implements Serializable 
{
    private static final long serialVersionUID = -8412720231300446934L;
    
    private String rtnCd;
	private String rtnMsg;
	private List<G3RechargeHistDTO> g3RechargeHistDTOList;
	
	public String getRtnCd() {
		return rtnCd;
	}
	public void setRtnCd(String rtnCd) {
		this.rtnCd = rtnCd;
	}
	public String getRtnMsg() {
		return rtnMsg;
	}
	public void setRtnMsg(String rtnMsg) {
		this.rtnMsg = rtnMsg;
	}
	public List<G3RechargeHistDTO> getG3RechargeHistDTOList() {
		return g3RechargeHistDTOList;
	}
	public void setG3RechargeHistDTOList(
			List<G3RechargeHistDTO> g3RechargeHistDTOList) {
		this.g3RechargeHistDTOList = g3RechargeHistDTOList;
	}

}
