package com.pccw.myhkt;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.app.DownloadManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.MutableContextWrapper;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.ValueCallback;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.pccw.dango.shared.cra.LgiCra;
import com.pccw.myhkt.activity.BaseActivity;
import com.pccw.myhkt.activity.MainMenuActivity;
import com.pccw.myhkt.lib.ui.AAQuery;
import com.pccw.myhkt.util.Constant;
import com.pccw.myhkt.util.LiveChatWebViewClient;
import com.pccw.myhkt.util.RuntimePermissionUtil;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

/************************************************************************
 File       : LiveChatHelper.java
 Desc       : Handling All Screens' LiveChat WebView
 Name       : LiveChatHelper
 Created by : Vincent Fung
 Date       : 16/11/2015

 Change History:
 Date       Modified By       Description
 ---------- ----------------    -------------------------------
 16/11/2015 Vincent Fung         - First draft
 15/6/2017  Abdulmoiz Esmail       - Added the SHLF_PA_PLN_PCD_4MOBCS and SHLF_PA_BIL_PCD_4MOBCS in the MOB-CS,
 and revert them to SHLF_PA_PLN_PCD, SHIF_PA_BIL_PCD respectively before passing it to HTTPS
 25/1/2018  Abdulmoiz Esmail        -Updated displaying of live chat disclaimer
 *************************************************************************/

public class LiveChatHelper {
    private boolean debug = false;   // toggle debug logs
    private static LiveChatHelper liveChatHelper = null;
    private static MutableContextWrapper mMutableContext;
    private WebView webview = null;

    public static boolean isPause = false;
    private static boolean isFailed = false;
    private String moduleId = "";
    private static Context cxt;
    private static Activity act;
    private static Dialog lDialog;

    //Attachment function (2016-8-25)
    private BroadcastReceiver downloadCompleteReceiver;
    private DownloadManager downloadManager;
    private long downloadID;
    /**
     * File upload callback for platform versions prior to Android 5.0
     */
    public static ValueCallback<Uri> mFileUploadCallbackFirst;
    /**
     * File upload callback for Android 5.0+
     */
    public static ValueCallback<Uri[]> mFileUploadCallbackSecond;
    protected String mUploadableFileTypes = "*/*";
    protected static final int REQUEST_CODE_FILE_PICKER = 51426;
    public static int mRequestCodeFilePicker = REQUEST_CODE_FILE_PICKER;

    private LiveChatHelper(Context context) {
        debug = context.getResources().getBoolean(R.bool.DEBUG);
        this.cxt = context;
    }

    public static synchronized LiveChatHelper getInstance(Context context, Activity actt) {
        act = actt;
        if (liveChatHelper == null) {
            mMutableContext = new MutableContextWrapper(context);
            liveChatHelper = new LiveChatHelper(mMutableContext);
        } else {
            mMutableContext.setBaseContext(context);
        }
        return liveChatHelper;
    }

    public final WebView getWebView(Activity act) {
        if (debug)
            Log.i("LiveChatHelper", "Module Id : " + moduleId + "\n is Webview Pause: " + isPause);
        if (webview == null) {
            mMutableContext = new MutableContextWrapper(act);
            initWebView(mMutableContext, act);
        } else {
            mMutableContext.setBaseContext(act);
        }
        return webview;
    }

    //**********************************************************************************************
    //								WebView Initialization
    //**********************************************************************************************
    private void initWebView(final Context context, final Activity act) {
        webview = new WebView(context);
        webview.setWebViewClient(new LiveChatWebViewClient(context));
        webview.setVisibility(View.VISIBLE);
        webview.getSettings().setJavaScriptEnabled(true);
        webview.getSettings().setAllowFileAccess(true);
        webview.getSettings().setLoadWithOverviewMode(true);
        webview.setDownloadListener((url, userAgent, contentDisposition, mimetype, contentLength) -> {
            Log.d("lwg", "setDownloadListener: " + url);
            Toast.makeText(context, context.getString(R.string.MYHKT_START_DOWNLOAD), Toast.LENGTH_SHORT).show();
        });

        webview.setWebChromeClient(new WebChromeClient() {
            // file upload callback (Android 2.2 (API level 8) -- Android 2.3 (API level 10)) (hidden method)
            @SuppressWarnings("unused")
            public void openFileChooser(ValueCallback<Uri> uploadMsg) {
                openFileChooser(uploadMsg, null);
            }

            // file upload callback (Android 3.0 (API level 11) -- Android 4.0 (API level 15)) (hidden method)
            public void openFileChooser(ValueCallback<Uri> uploadMsg, String acceptType) {
                openFileChooser(uploadMsg, acceptType, null);
            }

            // file upload callback (Android 4.1 (API level 16) -- Android 4.3 (API level 18)) (hidden method)
            public void openFileChooser(ValueCallback<Uri> uploadMsg, String acceptType, String capture) {
                openFileInput(uploadMsg, null, act);
            }

            // file upload callback (Android 5.0 (API level 21) -- current) (public method)
            @SuppressWarnings("all")
            public boolean onShowFileChooser(WebView webView, ValueCallback<Uri[]> filePathCallback, WebChromeClient.FileChooserParams fileChooserParams) {
                openFileInput(null, filePathCallback, act);
                return true;
            }
        });

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.Q) {
            checkPermission();
        }

//		String livechatURL = Utils.isUAT(context) ? APIsManager.UAT_livechat : APIsManager.PRD_livechat;
        String livechatURL = Utils.isUAT(context) ? APIsManager.UAT_livechat : APIsManager.PRD_livechat;
        Utils.setCurrentModule(Utils.getString(context, R.string.MODULE_REG));
        livechatURL = livechatURL + "?" + genLiveChatUrlParam(context);
        Log.d("LiveChat", "URL:" + livechatURL);
        webview.loadUrl(livechatURL);
    }

    //**********************************************************************************************
    //						Show WebView in Activities (BaseActivity and MainMenuActvity)
    //**********************************************************************************************
    public final void showWebView(final Activity act, final String mModuleId) {

        downloadManager = (DownloadManager) act.getSystemService(Context.DOWNLOAD_SERVICE);
        downloadCompleteReceiver = new BroadcastReceiver() {

            @Override
            public void onReceive(Context context, Intent intent) {
                long id = intent.getLongExtra(DownloadManager.EXTRA_DOWNLOAD_ID, 0L);
                DownloadManager.Query query = new DownloadManager.Query();
                query.setFilterById(id);
                Cursor cursor = downloadManager.query(query);
                if (cursor.moveToFirst()) {
                    if (cursor.getInt(cursor.getColumnIndex(DownloadManager.COLUMN_STATUS)) == DownloadManager.STATUS_SUCCESSFUL) {
                        Toast.makeText(act, act.getResources().getString(R.string.MYHKT_BTN_DONE), Toast.LENGTH_LONG).show();
                    }
                }
            }
        };

        IntentFilter filter = new IntentFilter();
        filter.addAction(DownloadManager.ACTION_DOWNLOAD_COMPLETE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
            act.registerReceiver(downloadCompleteReceiver, filter, Context.RECEIVER_NOT_EXPORTED);
        } else {
            act.registerReceiver(downloadCompleteReceiver, filter);
        }

        if (!isPause && (!ClnEnv.isLoggedIn() || Utils.getLiveChatDisclaimerFlag())) {
            //dialog message
            final TextView message = new TextView(act);
            message.setPadding(10, 10, 10, 10);
            final SpannableString s = new SpannableString(act.getText(R.string.LIVE_CHAT_DISCLAIMER));
            message.setText(s);
            message.setMovementMethod(LinkMovementMethod.getInstance());

            //alert dialog
            AlertDialog alertDialog;
            AlertDialog.Builder builder = new Builder(act);
            builder.setView(message);
            builder.setCancelable(false);
            builder.setNegativeButton(R.string.btn_ok, new Dialog.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                    Utils.setLiveChatDisclaimerFlag(false);
                    displayWebView(act, mModuleId);
                }
            });
            builder.setPositiveButton(R.string.btn_cancel, new Dialog.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });

            alertDialog = builder.create();
            alertDialog.show();
        } else {
            displayWebView(act, mModuleId);
        }
    }

    //Module Id may not be used if the web view is pause only
    public final void displayWebView(final Activity act, String mModuleId) {
        if (mModuleId == null || mModuleId.equals("")) {
            mModuleId = "SHLF_PA_ROOT";
        }

        //If the webview is puase only, use the existing moduleId
        //Else use the new one
        if (!isPause) {
            moduleId = mModuleId;
        }

        //To find the height of notification bar
        Rect rectangle = new Rect();
        Window window = act.getWindow();
        window.getDecorView().getWindowVisibleDisplayFrame(rectangle);
        int statusBarHeight = rectangle.top;
        int contentViewTop = window.findViewById(Window.ID_ANDROID_CONTENT).getTop();
        int titleBarHeight = Math.abs(contentViewTop - statusBarHeight);
        int btnH = act.getResources().getDimensionPixelOffset(R.dimen.livechat_browser_btn);
        //Status Bar Color
        if (android.os.Build.VERSION.SDK_INT >= 21) {
            window.getDecorView().setFitsSystemWindows(false);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
//			if (ClnEnv.isMyMobFlag()) {
//				window.setStatusBarColor(act.getResources().getColor(R.color.hkt_edittextbgorange));
//			} else {
//				window.setStatusBarColor(act.getResources().getColor(R.color.hkt_textcolor));
//			}
        }

        //Dialog setting
        int extralinespace = (int) act.getResources().getDimension(R.dimen.extralinespace);
        int imageHeight = act.getResources().getDimensionPixelOffset(R.dimen.button_height) + act.getResources().getDimensionPixelOffset(R.dimen.padding_screen) * 5 / 2;
        int padding = act.getResources().getDimensionPixelOffset(R.dimen.edittextpadding);
        lDialog = new Dialog(act, android.R.style.Theme_Translucent_NoTitleBar);
        lDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        LayoutInflater inflater = (LayoutInflater) act.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View ctView = inflater.inflate(R.layout.dialog_livechat, null);
        lDialog.setContentView(ctView);
        lDialog.setCanceledOnTouchOutside(false);
        lDialog.setCancelable(false);

        AAQuery aq = new AAQuery(ctView);
        ImageView mini = (ImageView) lDialog.findViewById(R.id.dialog_livechat_mini);
        ImageView close = (ImageView) lDialog.findViewById(R.id.dialog_livechat_close);
        LinearLayout frame = (LinearLayout) lDialog.findViewById(R.id.dialog_livechat_frame);

        aq.id(R.id.dialog_livechat_iv).height(imageHeight, false);
        aq.id(R.id.dialog_livechat_iv).visibility(View.GONE);
        aq.normText(R.id.dialog_livechat_title, act.getResources().getString(R.string.MYHKT_BTN_LIVECHAT));
        aq.id(R.id.dialog_livechat_title).height(ViewGroup.LayoutParams.WRAP_CONTENT, false);
        aq.padding(R.id.dialog_livechat_title, extralinespace, 0, 0, 0);
        aq.id(R.id.dialog_livechat_title).textColor(Color.WHITE).height(act.getResources().getDimensionPixelOffset(R.dimen.livechat_header_height), false);

        Bitmap bmmini = ViewUtils.scaleToFitHeight(act, R.drawable.btn_minimize, btnH);
        boolean isZh;

        if ("zh".equalsIgnoreCase(act.getResources().getString(R.string.myhkt_lang))) {
            isZh = true;
        } else {
            isZh = false;
        }

        Bitmap bmmclose = ViewUtils.scaleToFitHeight(act, isZh ? R.drawable.btn_close2_zh_1 : R.drawable.btn_close2, btnH);
        aq.id(R.id.dialog_livechat_mini).image(bmmini);
        aq.id(R.id.dialog_livechat_close).image(bmmclose);
        aq.marginpx(R.id.dialog_livechat_close, padding * 3 / 2, padding / 2, padding / 2, padding / 2);
        aq.marginpx(R.id.dialog_livechat_main_bg, padding, padding / 2, padding, padding);
        aq.marginpx(R.id.dialog_livechat_frame, padding / 2, 0, padding / 2, padding);

        //Web view part
        frame.removeAllViews();
        final WebView mWebView = getWebView(act);
        if (mWebView.getParent() != null) {
            ((LinearLayout) mWebView.getParent()).removeView(mWebView);
        }
        frame.removeView(mWebView);
        frame.addView(mWebView);
        LinearLayout.LayoutParams para3 = (android.widget.LinearLayout.LayoutParams) mWebView.getLayoutParams();
        para3.height = LinearLayout.LayoutParams.MATCH_PARENT;
        para3.bottomMargin = padding / 2;
        mWebView.setLayoutParams(para3);
        mWebView.setWebViewClient(new LiveChatWebViewClient(act));
        close.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                DialogInterface.OnClickListener onPositiveClickListener = new DialogInterface.OnClickListener() {
                    Bundle rbundle;

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //close livechat
                        closeLiveChat();
                    }
                };

                DialogHelper.createSimpleDialog(act, Utils.getString(act, R.string.LIVE_CHAT_EXIT_ALERT), Utils.getString(act, R.string.btn_ok), onPositiveClickListener, Utils.getString(act, R.string.btn_cancel));
            }
        });
        mini.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                isPause = true;
                if (act instanceof BaseActivity) {
                    ((BaseActivity) act).setLiveChangeIcon(isPause);
                } else if (act instanceof MainMenuActivity) {
                    ((MainMenuActivity) act).setLiveChangeIcon(isPause);
                }
                lDialog.cancel();
            }
        });
        lDialog.show();
    }

    //disclaimer dialog
    protected final void displayDialog(final Activity act, String message, final String mModuleId) {
        AlertDialog alertDialog;
        AlertDialog.Builder builder = new Builder(act);
        builder.setMessage(message);
        builder.setCancelable(false);
        builder.setNegativeButton(R.string.btn_ok, new Dialog.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                Utils.setLiveChatDisclaimerFlag(false);
                displayWebView(act, mModuleId);
            }
        });
        builder.setPositiveButton(R.string.btn_cancel, new Dialog.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        alertDialog = builder.create();
        alertDialog.show();
    }

    public void closeLiveChat() {
        if (webview != null) {
            webview.loadUrl("about:blank");
            webview = null;
            isPause = false;
            if (act instanceof BaseActivity) {
                ((BaseActivity) act).setLiveChangeIcon(isPause);
            } else if (act instanceof MainMenuActivity) {
                ((MainMenuActivity) act).setLiveChangeIcon(isPause);
            }
            lDialog.cancel();
            isFailed = false;
        }
    }

    //Build Live Chat URL param with login
    private String genLiveChatUrlParam(Context context) {
        String str = "";
        String c_id = "";
        String m_n;

        if (moduleId.equals(Utils.getString(mMutableContext, R.string.MODULE_PCD_PLAN_4MOBCS))) {
            m_n = Utils.getString(mMutableContext, R.string.MODULE_PCD_PLAN);
        } else if (moduleId.equals(Utils.getString(mMutableContext, R.string.MODULE_PCD_BILL_4MOBCS))) {
            m_n = Utils.getString(mMutableContext, R.string.MODULE_PCD_BILL);
        } else {
            m_n = moduleId;
        }

        String l = ClnEnv.getAppLocale(context);
        String c_t = "";
        String n_n = "";
        String e_s = "cs.hkt.com (Android)";
        String c_s = "";
        if (ClnEnv.isLoggedIn()) {
            LgiCra loginCra = ClnEnv.getLgiCra();
            c_id = loginCra.getOQualSvee().getSveeRec().loginId;
            c_t = loginCra.getOChatTok();
            n_n = loginCra.getOQualSvee().getSveeRec().nickname;
        }
        if (ClnEnv.getSessionPremierFlag()) {
            //LOB = MOB
            boolean premier_flag = ClnEnv.getPref(mMutableContext.getApplicationContext(), "livechat_premier_flag", false);
            if (isMOBPageByModuleId(moduleId) && premier_flag) {
                c_s = "MOB-CS";
            } else {
                if (ClnEnv.isLoggedIn()) {
                    c_s = "PREM";
                } else { // if user not logged in, it doesnt count as a premier user
                    c_s = "CONS";
                }
            }
        } else {
            boolean consumer_flag = ClnEnv.getPref(mMutableContext.getApplicationContext(), "livechat_consumer_flag", false);
            c_s = isMOBPageByModuleId(moduleId) && consumer_flag ? "MOB-CS" : "CONS";
        }

        //String overrideChatTok = "8838a93cbe8f0941b3fe78f2faab6ee3239da01ee572b424f1f1d4e1a954f08d";
        if (c_id.equals("")) str = str + "c_id=" + urlEncode("Guest");
        else str = str + "c_id=" + urlEncode(c_id);

        if (m_n.equals("")) str = str + "&" + "m_n=" + "";
        else str = str + "&" + "m_n=" + urlEncode(m_n);

        if (l.equals("")) str = str + "&" + "l=" + "";
        else str = str + "&" + "l=" + urlEncode(l);

        if (c_t.equals("")) str = str + "&" + "c_t=" + urlEncode("NTK");
        else str = str + "&" + "c_t=" + urlEncode(c_t);

        if (n_n.equals("")) str = str + "&" + "n_n=" + urlEncode("Guest");
        else str = str + "&" + "n_n=" + urlEncode(n_n);

        if (e_s.equals("")) str = str + "&" + "e_s=" + "";
        else str = str + "&" + "e_s=" + urlEncode(e_s);

        if (!c_s.equals("")) {
            str = str + "&" + "c_s=" + urlEncode(c_s);
        }
        if (debug) Log.d("LiveChat URL: ", str);
        return str;
    }

    private String urlEncode(String url) {
        String encodedurl = "";
        try {
            encodedurl = URLEncoder.encode(url, "UTF-8").replaceAll("\\+", "%20");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return encodedurl;
    }

    private boolean isMOBPageByModuleId(String moduleId) {
        return moduleId.equalsIgnoreCase(Utils.getString(mMutableContext, R.string.MODULE_101_BILL)) || moduleId.equalsIgnoreCase(Utils.getString(mMutableContext, R.string.MODULE_101_PLAN)) || moduleId.equalsIgnoreCase(Utils.getString(mMutableContext, R.string.MODULE_101_MM_BILL)) || moduleId.equalsIgnoreCase(Utils.getString(mMutableContext, R.string.MODULE_101_MM_PLAN)) || moduleId.equalsIgnoreCase(Utils.getString(mMutableContext, R.string.MODULE_O2F_BILL)) || moduleId.equalsIgnoreCase(Utils.getString(mMutableContext, R.string.MODULE_O2F_PLAN)) || moduleId.equalsIgnoreCase(Utils.getString(mMutableContext, R.string.MODULE_O2F_MM_BILL)) || moduleId.equalsIgnoreCase(Utils.getString(mMutableContext, R.string.MODULE_O2F_MM_PLAN)) || moduleId.equalsIgnoreCase(Utils.getString(mMutableContext, R.string.MODULE_MOB_BILL)) || moduleId.equalsIgnoreCase(Utils.getString(mMutableContext, R.string.MODULE_MOB_PLAN)) || moduleId.equalsIgnoreCase(Utils.getString(mMutableContext, R.string.MODULE_MOB_MM_BILL)) || moduleId.equalsIgnoreCase(Utils.getString(mMutableContext, R.string.MODULE_MOB_MM_PLAN)) || moduleId.equalsIgnoreCase(Utils.getString(mMutableContext, R.string.MODULE_MM_MAIN)) || moduleId.equalsIgnoreCase(Utils.getString(mMutableContext, R.string.MODULE_IOI_PLAN)) || moduleId.equalsIgnoreCase(Utils.getString(mMutableContext, R.string.MODULE_IOI_BILL)) || moduleId.equalsIgnoreCase(Utils.getString(mMutableContext, R.string.MODULE_IOI_MM_PLAN)) || moduleId.equalsIgnoreCase(Utils.getString(mMutableContext, R.string.MODULE_IOI_MM_BILL)) || moduleId.equalsIgnoreCase(Utils.getString(mMutableContext, R.string.MODULE_PCD_PLAN_4MOBCS)) || moduleId.equalsIgnoreCase(Utils.getString(mMutableContext, R.string.MODULE_PCD_BILL_4MOBCS)) || moduleId.equalsIgnoreCase(Utils.getString(mMutableContext, R.string.MODULE_CSL_MYMSG)) || moduleId.equalsIgnoreCase(Utils.getString(mMutableContext, R.string.MODULE_CLUBSIM_MM_PLAN)) || moduleId.equalsIgnoreCase(Utils.getString(mMutableContext, R.string.MODULE_CLUBSIM_MM_BILL)) || moduleId.equalsIgnoreCase(Utils.getString(mMutableContext, R.string.MODULE_CLUBSIM_PLAN)) || moduleId.equalsIgnoreCase(Utils.getString(mMutableContext, R.string.MODULE_CLUBSIM_BILL)) || moduleId.equalsIgnoreCase(Utils.getString(mMutableContext, R.string.MODULE_CLUB_SIM)) || moduleId.equalsIgnoreCase(Utils.getString(mMutableContext, R.string.MODULE_CLUBSIM_MYMSG)) || moduleId.equalsIgnoreCase(Utils.getString(mMutableContext, R.string.MODULE_101_MYMSG));
    }

    @SuppressLint("NewApi")
    protected void openFileInput(final ValueCallback<Uri> fileUploadCallbackFirst, final ValueCallback<Uri[]> fileUploadCallbackSecond, Activity act) {
        if (mFileUploadCallbackFirst != null) {
            mFileUploadCallbackFirst.onReceiveValue(null);
        }
        mFileUploadCallbackFirst = fileUploadCallbackFirst;

        if (mFileUploadCallbackSecond != null) {
            mFileUploadCallbackSecond.onReceiveValue(null);
        }
        mFileUploadCallbackSecond = fileUploadCallbackSecond;

        Intent i = new Intent(Intent.ACTION_GET_CONTENT);
        i.addCategory(Intent.CATEGORY_OPENABLE);
        i.setType(mUploadableFileTypes);
        Log.i("BaseActivity", "AAAAA1");
        act.startActivityForResult(Intent.createChooser(i, "File Chooser"), mRequestCodeFilePicker);
    }

    protected boolean checkPermission() {
        if (!RuntimePermissionUtil.isWriteExternalStoragePermissionGranted(act)) {
            if (ClnEnv.getPref(act, Constant.FILE_STORAGE_PERMISSION_DENIED, false)) {
                DialogHelper.createSimpleDialog(act, act.getResources().getString(R.string.permission_denied_setting_enabled), Utils.getString(act, R.string.btn_ok));
            } else {
                RuntimePermissionUtil.requestFileStoragePermissionForLogs(act);
            }
            return false;
        }
        return true;
    }
}