package com.pccw.myhkt.touchId;

import android.annotation.TargetApi;
import android.app.Activity;
import android.hardware.fingerprint.FingerprintManager;
import android.os.Build;
import android.security.keystore.KeyGenParameterSpec;
import android.security.keystore.KeyPermanentlyInvalidatedException;
import android.security.keystore.KeyProperties;
import androidx.annotation.Nullable;
import android.view.View;

import com.pccw.myhkt.DialogHelper;
import com.pccw.myhkt.R;
import com.pccw.myhkt.Utils;
import com.pccw.myhkt.activity.LoginActivity;
import com.pccw.myhkt.activity.TouchIdLoginActivationActivity;

import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.util.UUID;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;

/**
 * Created by AMoiz Esmail on 03/11/2017.
 *
 * 03/21/2018   Abdulmoiz Esmail    - Updates initialization of cipher and keys
 *                                  source:https://github.com/multidots/android-fingerprint-authentication/blob/master/fingerprint-auth/src/main/java/com/multidots/fingerprintauth/FingerPrintAuthHelper.java
 */

public class TouchIDClickListener implements View.OnClickListener {

    private static final String DIALOG_FRAGMENT_TAG = "myFragment";
    private static final String KEY_NAME = UUID.randomUUID().toString();

    Cipher mCipher;

    Activity mContext;

    private KeyStore mKeyStore;
    private KeyGenerator mKeyGenerator;

    public TouchIDClickListener(Activity context) {
        mContext = context;
    }

    @Override
    public void onClick(View view) {

        // Set up the crypto object for later. The object will be authenticated by use
        // of the fingerprint.
    	if(Utils.isTouchIDEnrolledByUser(mContext)) {

            // Show the fingerprint dialog. The user has the option to use the fingerprint with
            // crypto, or you can fall back to using a server-side verified password.
            // Check if Touch ID Activation
            if(mContext instanceof TouchIdLoginActivationActivity) {
                //Check if the saved temp password match with user input
                if(((TouchIdLoginActivationActivity) mContext).getInputPassword().equals(Utils.getSavedPassword(mContext))) {
                    showTouchIdDialogFragment();
                } else {
                    DialogHelper.createSimpleDialog(mContext, mContext.getString(R.string.fp_activation_invalid_pwd));
                }
            } else if(mContext instanceof LoginActivity) {
                //Touch ID LOGIN
                showTouchIdDialogFragment();
            }
	            
    	} else {
    		DialogHelper.createSimpleDialog(mContext, mContext.getString(R.string.fp_welcome_not_enrolled_alert_msg));
    	}
    }

    private void showTouchIdDialogFragment() {
        FingerprintManager.CryptoObject cryptoObject = getCryptoObject();
        if (cryptoObject != null) {
            boolean bool = false;
            if (mContext instanceof LoginActivity) {
                bool = true;
            }
            TouchIdAuthDialogFragment fragment
                    = TouchIdAuthDialogFragment.onInstance(bool);
            fragment.setCryptoObject(new FingerprintManager.CryptoObject(mCipher));
            fragment.show(mContext.getFragmentManager(), DIALOG_FRAGMENT_TAG);
        } else {
            DialogHelper.createSimpleDialog(mContext, mContext.getString(R.string.fp_failure_lock_title));
        }
    }


    /**
     * Generate authentication key.
     *
     * @return true if the key generated successfully.
     */
    @TargetApi(23)
    private boolean generateKey(String keyName, boolean invalidatedByBiometricEnrollment) {
        mKeyStore = null;

        //Get the instance of the key store.
        try {
            mKeyStore = KeyStore.getInstance("AndroidKeyStore");
            mKeyGenerator = KeyGenerator.getInstance(KeyProperties.KEY_ALGORITHM_AES, "AndroidKeyStore");
        } catch (NoSuchAlgorithmException |
                NoSuchProviderException e) {
            return false;
        } catch (KeyStoreException e) {
            return false;
        }

        //generate key.
        try {
            mKeyStore.load(null);
            KeyGenParameterSpec.Builder builder = new KeyGenParameterSpec.Builder(keyName,
                    KeyProperties.PURPOSE_ENCRYPT |
                            KeyProperties.PURPOSE_DECRYPT)
                    .setBlockModes(KeyProperties.BLOCK_MODE_CBC)
                    // Require the user to authenticate with a fingerprint to authorize every use
                    // of the key
                    .setUserAuthenticationRequired(true)
                    .setEncryptionPaddings(KeyProperties.ENCRYPTION_PADDING_PKCS7);

            // This is a workaround to avoid crashes on devices whose API level is < 24
            // because KeyGenParameterSpec.Builder#setInvalidatedByBiometricEnrollment is only
            // visible on API level +24.
            // Ideally there should be a compat library for KeyGenParameterSpec.Builder but
            // which isn't available yet.
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                builder.setInvalidatedByBiometricEnrollment(invalidatedByBiometricEnrollment);
            }
            mKeyGenerator.init(builder.build());
            mKeyGenerator.generateKey();

            return true;
        } catch (NoSuchAlgorithmException
                | InvalidAlgorithmParameterException
                | CertificateException
                | IOException e) {
            return false;
        }
    }

    /**
     * Initialize the cipher.
     *
     * @return true if the initialization is successful.
     */
    @TargetApi(23)
    private boolean cipherInit() {

        if (!generateKey(KEY_NAME, true)) {
            return false;
        }

        try {
            mCipher = Cipher.getInstance(
                    KeyProperties.KEY_ALGORITHM_AES + "/"
                            + KeyProperties.BLOCK_MODE_CBC + "/"
                            + KeyProperties.ENCRYPTION_PADDING_PKCS7);
        } catch (NoSuchAlgorithmException |
                NoSuchPaddingException e) {
            return false;
        }

        try {
            mKeyStore.load(null);
            SecretKey key = (SecretKey) mKeyStore.getKey(KEY_NAME, null);
            mCipher.init(Cipher.ENCRYPT_MODE, key);
            return true;
        } catch (KeyPermanentlyInvalidatedException e) {
            return false;
        } catch (KeyStoreException | CertificateException
                | UnrecoverableKeyException | IOException
                | NoSuchAlgorithmException | InvalidKeyException e) {
            return false;
        }
    }

    @TargetApi(23)
    @Nullable
    private FingerprintManager.CryptoObject getCryptoObject() {
        return cipherInit() ? new FingerprintManager.CryptoObject(mCipher) : null;
    }


}
