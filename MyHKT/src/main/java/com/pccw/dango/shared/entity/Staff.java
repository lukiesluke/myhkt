/*
    Staff (likely for API) in Dango
    
    Staff should also associate with the "Phylum".
    
    Which means she/he can access those Customers (CUST)
    with the Same Phylum.
    
    Keywords
    --------
    $URL: svn://10.87.120.207/dango/rel/v16.0/src/com/pccw/dango/shared/entity/Staff.java $
    $Rev: 844 $
    $Date: 2016-10-17 11:54:01 +0800 (¶g¤@, 17 ¤Q¤ë 2016) $
    $Author: upd $
*/

package com.pccw.dango.shared.entity;

import java.io.Serializable;


public class Staff extends BaseUserEx implements Serializable
{
    private static final long serialVersionUID = -5242508445378007781L;
    
    private String                  staffId;
    private String                  phylum;
    
    
    public static void main(String rArg[])
    {
        System.out.println(getVer());
        return;
    }
    
    
    public static String getVer()
    {
        return ("$URL: svn://10.87.120.207/dango/rel/v16.0/src/com/pccw/dango/shared/entity/Staff.java $, $Rev: 844 $");
    }
    
    
    private Staff()
    {
        /* Constructor Prohibited (I need the StaffId) */
        super();
    }
    
    
    public Staff(String rStaffId, String rPhylum)
    {
        this();
        
        staffId = rStaffId;
        phylum  = rPhylum;
    }
    
    
    public String getId()
    {
        return (staffId);
    }
    
    
    public String getPhylum()
    {
        return (phylum);
    }
    
    
    public String getType()
    {
        return (TY_STFF);
    }
    
    
    public String getRole()
    {
        return ("");
    }
    
    
    public String[] getRghtAry()
    {
        return (new String[0]);
    }
    
    
    public boolean isAuth(String rRght)
    {
        return (false);
    }
}
