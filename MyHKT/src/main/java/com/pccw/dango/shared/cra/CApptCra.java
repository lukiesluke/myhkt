package com.pccw.dango.shared.cra;

import com.pccw.dango.shared.entity.CApptRec;
import com.pccw.dango.shared.entity.CustRec;

import java.io.Serializable;

public class CApptCra extends BaseCraEx implements Serializable
{
    private String                    apiTy;
    private String                    clnVer;

    private String                    sysId;
    private String                    sysPwd;

    private String                    userId;
    private String                    psnTy;

    private String                    iLoginId;  /* Login Id                                     */
    private CustRec                   iCustRec;

    private CApptRec                  oCApptRec;
    private CApptRec                  iCApptRec;

    public static void main(String rArg[])
    {
        System.out.println(getVer());
        return;
    }

    public static String getVer()
    {
        return ("$URL: svn://10.87.120.207/dango/rel/v16.0/src/com/pccw/dango/shared/cra/CApptCra.java $, $Rev: 1934 $");
    }

    public CApptCra copyFrom(CApptCra rSrc)
    {
        setApiTy(rSrc.getApiTy());
        setReply(rSrc.getReply());
        setClnVer(rSrc.getClnVer());

        setSysId(rSrc.getSysId());
        setSysPwd(rSrc.getSysPwd());

        setUserId(rSrc.getUserId());
        setPsnTy(rSrc.getPsnTy());

        setiLoginId(rSrc.getiLoginId());
        setiCustRec(rSrc.getiCustRec());

        setiCApptRec(rSrc.iCApptRec);

        return (this);
    }


    public CApptCra copyTo(CApptCra rDes)
    {
        rDes.copyFrom(this);
        return (rDes);
    }


    public CApptCra copyMe()
    {
        CApptCra                     rDes;

        rDes = new CApptCra();
        rDes.copyFrom(this);
        return (rDes);
    }

    public String getApiTy() {
        return apiTy;
    }

    public void setApiTy(String apiTy) {
        this.apiTy = apiTy;
    }

    public String getClnVer() {
        return clnVer;
    }

    public void setClnVer(String clnVer) {
        this.clnVer = clnVer;
    }

    public String getSysId() {
        return sysId;
    }

    public void setSysId(String sysId) {
        this.sysId = sysId;
    }

    public String getSysPwd() {
        return sysPwd;
    }

    public void setSysPwd(String sysPwd) {
        this.sysPwd = sysPwd;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getPsnTy() {
        return psnTy;
    }

    public void setPsnTy(String psnTy) {
        this.psnTy = psnTy;
    }

    public String getiLoginId() {
        return iLoginId;
    }

    public void setiLoginId(String iLoginId) {
        this.iLoginId = iLoginId;
    }

    public CustRec getiCustRec() {
        return iCustRec;
    }

    public void setiCustRec(CustRec iCustRec) {
        this.iCustRec = iCustRec;
    }

    public CApptRec getiCApptRec() {
        return iCApptRec;
    }

    public void setiCApptRec(CApptRec iCApptRec) {
        this.iCApptRec = iCApptRec;
    }

    public CApptRec getoCApptRec() {
        return oCApptRec;
    }

    public void setoCApptRec(CApptRec oCApptRec) {
        this.oCApptRec = oCApptRec;
    }
}

