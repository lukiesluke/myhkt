package com.pccw.myhkt.model;

import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

import java.util.ArrayList;

@Root(name = "Destinations", strict = false)
public class Destinations {

    public Destinations() {
    }

    public Destinations(ArrayList<Destination> destination) {
        Destination = destination;
    }

    @ElementList(name = "Destination", inline = true, required = false)
    private ArrayList<Destination> Destination;

    public ArrayList<com.pccw.myhkt.model.Destination> getDestination() {
        return Destination;
    }
}
