package com.pccw.myhkt.service;

import android.app.IntentService;
import android.content.Intent;

import androidx.annotation.Nullable;

import com.pccw.dango.shared.cra.ApptCra;
import com.pccw.dango.shared.tool.RC;
import com.pccw.myhkt.APIsManager;
import com.pccw.myhkt.APIsResponse;
import com.pccw.myhkt.ClnEnv;
import com.pccw.myhkt.Utils;
import com.pccw.myhkt.util.Constant;

public class QueryAppointmentService extends IntentService implements APIsManager.OnAPIsListener {

    public QueryAppointmentService() {
        super("QueryAppointmentService");
    }

    public QueryAppointmentService(String name) {
        super(name);
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {

    }

    @Override
    public int onStartCommand(@Nullable Intent intent, int flags, int startId) {
        ApptCra apptCra = new ApptCra();
        apptCra.setILoginId(ClnEnv.getQualSvee().getSveeRec().loginId);
        apptCra.setICustRec(ClnEnv.getQualSvee().getCustRec().copyMe());
        apptCra.setISubnRecAry(ClnEnv.getQualSvee().getSubnRecAry());

        APIsManager.doGetAppointment(this, apptCra);
        ClnEnv.setIsQueryApptServiceRunning(true);
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onSuccess(APIsResponse response) throws Exception {
        if (response != null) {
            if (APIsManager.APPT.equals(response.getActionTy())) {
                ApptCra apptCra = (ApptCra) response.getCra();
                ClnEnv.setApptCra(apptCra);

                Utils.filterApptToShowClock(getApplicationContext(), apptCra);

                sendBroadcast(new Intent().setAction(Constant.ACTION_CACHED_APPT_RESPONSE).setPackage(getPackageName()));
                stopSelf();
            }
        }
    }

    @Override
    public void onFail(APIsResponse response) throws Exception {
        if (RC.TCSESS_MM.equalsIgnoreCase(response.getReply().getCode())) {
            sendBroadcast(new Intent().setAction(Constant.ACTION_SESSION_TIMEOUT).setPackage(getPackageName()));
            stopSelf();
        }
    }
}
