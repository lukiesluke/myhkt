package com.pccw.myhkt.adapter;

/************************************************************************
File       : DirectoryInquiryListAdapter.java
Desc       : Directory Inquiry Result List
Name       : DirectoryInquiryListAdapter
Created by : Andy Wong
Date       : 07/01/2016

Change History:
Date       Modified By        	Description
---------- ----------------   	-------------------------------
07/01/2016 Andy Wong        	- First draft
*************************************************************************/

import java.util.ArrayList;
import java.util.List;

import android.app.ActionBar.LayoutParams;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.Point;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.text.Html;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.pccw.dango.shared.entity.DQSrchDtl;
import com.pccw.myhkt.DialogHelper;
import com.pccw.myhkt.R;
import com.pccw.myhkt.Utils;
import com.pccw.myhkt.lib.ui.AAQuery;
import com.pccw.myhkt.lib.ui.HKTButton;
import com.pccw.myhkt.listeners.OnCallPhoneListener;

public class DirectoryInquiryListViewAdapter extends BaseAdapter {
	// Assume the input array has been processed
	private boolean			debug			= false;
	private DQAgent[]		dQSrchDtlList	= null;
	public Context			context;
	public LayoutInflater	inflater;
	public int				lastItem		= 0;
	public boolean			enableAnimation	= false;
	private int				item_padding;
	private int 			buttonHeight;
	private int 			deviceWidth;
	private int 			btnWidth;
	private int				extralinespace ;
	private OnCallPhoneListener onCallPhoneListener;

	public DirectoryInquiryListViewAdapter(Context context, List<DQSrchDtl> dqSrchDtl, OnCallPhoneListener onCallPhoneListener) {
		super();
		debug = context.getResources().getBoolean(R.bool.DEBUG);
		this.context = context;
		this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		item_padding = context.getResources().getDimensionPixelOffset(R.dimen.item_padding);
		buttonHeight = context.getResources().getDimensionPixelOffset(R.dimen.buttonblue_height);
		extralinespace = context.getResources().getDimensionPixelOffset(R.dimen.extralinespace);
		Display display = ((Activity)context).getWindowManager().getDefaultDisplay();
		Point size = new Point();
		display.getSize(size);
		deviceWidth = size.x;
		btnWidth = (deviceWidth - extralinespace *4)/2;
		prepareDQAgent(dqSrchDtl);
		this.onCallPhoneListener = onCallPhoneListener;

	}

	public int getCount() {
		if (dQSrchDtlList == null) return 0;
		return dQSrchDtlList.length;
	}

	public Object getItem(int position) {
		if (position >= 0 && position < dQSrchDtlList.length) {
			return dQSrchDtlList[position];
		} else {
			return null;
		}
	}

	public final boolean isHeaderItem(int position) {
		if (position >= dQSrchDtlList.length) {
			return false;
		} else {
			return dQSrchDtlList[position].isHeader;
		}
	}

	public long getItemId(int position) {
		return position;
	}

	public View getView(int position, View convertView, ViewGroup parent) {
		try {
			ViewHolder holder;

			if (convertView == null) {
				holder = new ViewHolder();
				convertView = inflater.inflate(R.layout.adapter_directoryinquirylist, null);
				holder.adapter_directoryinquirylist_separator = (TextView) convertView.findViewById(R.id.adapter_directoryinquirylist_separator);
				holder.adapter_directoryinquirylist_layout_content = (LinearLayout) convertView.findViewById(R.id.adapter_directoryinquirylist_layout_content);
				holder.adapter_directoryinquirylist_layout_content1 = (LinearLayout) convertView.findViewById(R.id.adapter_directoryinquirylist_layout_content1);
				holder.adapter_directoryinquirylist_call = (HKTButton) convertView.findViewById(R.id.adapter_directoryinquirylist_call);
				holder.adapter_directoryinquirylist_address = (TextView) convertView.findViewById(R.id.adapter_directoryinquirylist_address);
				holder.adapter_directoryinquirylist_fax = (TextView) convertView.findViewById(R.id.adapter_directoryinquirylist_fax);
//				holder.adapter_directoryinquirylist_call_txt = (TextView) convertView.findViewById(R.id.adapter_directoryinquirylist_call_text);
				holder.adapter_directoryinquirylist_line1 = (ImageView) convertView.findViewById(R.id.adapter_directoryinquirylist_line1);
				holder.adapter_directoryinquirylist_line2 = (ImageView) convertView.findViewById(R.id.adapter_directoryinquirylist_line2);
				holder.adapter_directoryinquirylist_address_icon = (ImageView) convertView.findViewById(R.id.adapter_directoryinquirylist_address_icon);
				holder.adapter_directoryinquirylist_fax_icon = (ImageView) convertView.findViewById(R.id.adapter_directoryinquirylist_fax_icon);
//				holder.adapter_directoryinquirylist_call_icon = (ImageView) convertView.findViewById(R.id.adapter_directoryinquirylist_call_icon);
				convertView.setTag(holder);
			} else {
				holder = (ViewHolder) convertView.getTag();
			}

			AAQuery aq = new AAQuery(convertView);

			// Initial State for each list item
			holder.adapter_directoryinquirylist_separator.setVisibility(View.GONE);
			holder.adapter_directoryinquirylist_layout_content.setVisibility(View.GONE);
			holder.adapter_directoryinquirylist_layout_content1.setVisibility(View.GONE);
			holder.adapter_directoryinquirylist_line1.setVisibility(View.GONE);
			holder.adapter_directoryinquirylist_line2.setVisibility(View.GONE);
			holder.adapter_directoryinquirylist_call.setVisibility(View.INVISIBLE);
			holder.adapter_directoryinquirylist_address.setVisibility(View.INVISIBLE);
			holder.adapter_directoryinquirylist_address_icon.setVisibility(View.INVISIBLE);
			holder.adapter_directoryinquirylist_fax.setVisibility(View.INVISIBLE);
			holder.adapter_directoryinquirylist_fax_icon.setVisibility(View.INVISIBLE);
			convertView.setOnClickListener(null);

			if (dQSrchDtlList[position].isHeader) {
				holder.adapter_directoryinquirylist_separator.setVisibility(View.VISIBLE);
				holder.adapter_directoryinquirylist_line1.setVisibility(View.VISIBLE);
				aq.normTextGrey(R.id.adapter_directoryinquirylist_separator, dQSrchDtlList[position].dQSrchDtl.name);
				holder.adapter_directoryinquirylist_separator.setGravity(Gravity.LEFT|Gravity.CENTER);
			} else {
				holder.adapter_directoryinquirylist_layout_content.setVisibility(View.VISIBLE);
				holder.adapter_directoryinquirylist_layout_content1.setVisibility(View.VISIBLE);
				holder.adapter_directoryinquirylist_line2 .setVisibility(View.VISIBLE);
				if (!"".equalsIgnoreCase(dQSrchDtlList[position].address)) {
					final String title = dQSrchDtlList[position].header;
					final String displayMsg = dQSrchDtlList[position].address;
					holder.adapter_directoryinquirylist_address.setText(displayMsg);
					holder.adapter_directoryinquirylist_address.setVisibility(View.VISIBLE);
					holder.adapter_directoryinquirylist_address_icon.setVisibility(View.VISIBLE);
					convertView.setOnClickListener(new View.OnClickListener() {
						public void onClick(View v) {
							displayDialog(title, displayMsg);
						}
					});
					aq.normTextGrey(R.id.adapter_directoryinquirylist_address, displayMsg);
					holder.adapter_directoryinquirylist_address.setPadding(0, item_padding, item_padding, 0);
					aq.id(R.id.adapter_directoryinquirylist_address).height(LayoutParams.WRAP_CONTENT, false);
					aq.id(R.id.adapter_directoryinquirylist_address_icon).image(R.drawable.di_address_3x);
					aq.marginpx(R.id.adapter_directoryinquirylist_address_icon, item_padding, item_padding, item_padding, item_padding);
				}
				aq.id(R.id.adapter_directoryinquirylist_layout_content1).getView().setPadding(0, 0, 0, item_padding);
				if ("Fax".equalsIgnoreCase(dQSrchDtlList[position].dQSrchDtl.type) || "Fax".equalsIgnoreCase(dQSrchDtlList[position].address)) {
					holder.adapter_directoryinquirylist_fax.setVisibility(View.VISIBLE);
					holder.adapter_directoryinquirylist_fax_icon.setVisibility(View.VISIBLE);
					aq.normTextGrey(R.id.adapter_directoryinquirylist_fax, dQSrchDtlList[position].dQSrchDtl.number);
					holder.adapter_directoryinquirylist_fax.setGravity(Gravity.CENTER_VERTICAL);
					aq.id(R.id.adapter_directoryinquirylist_fax_icon).image(R.drawable.di_fax_3x);
					aq.marginpx(R.id.adapter_directoryinquirylist_fax_icon, item_padding, item_padding, item_padding, item_padding);

				} else if ("Tel".equalsIgnoreCase(dQSrchDtlList[position].dQSrchDtl.type.trim())) {
					aq.gravity(R.id.adapter_directoryinquirylist_call, Gravity.CENTER);
					holder.adapter_directoryinquirylist_call = aq.normTxtBtn(R.id.adapter_directoryinquirylist_call, dQSrchDtlList[position].dQSrchDtl.number, btnWidth , HKTButton.TYPE_BLUE);
					holder.adapter_directoryinquirylist_call.setVisibility(View.VISIBLE);
//					holder.adapter_directoryinquirylist_call_txt.setText(dQSrchDtlList[position].dQSrchDtl.number);
//					holder.adapter_directoryinquirylist_call_txt.setTextColor(context.getResources().getColorStateList(R.color.hkt_btn_blue_txtcolor_selector));
//					aq.id(R.id.adapter_directoryinquirylist_call_text).height(buttonHeight, false);
					final String telNum = dQSrchDtlList[position].dQSrchDtl.number.replaceAll(" ", "");
//					aq.id(R.id.adapter_directoryinquirylist_call).background(R.drawable.hkt_btn_bg_blue_selector);
//					aq.id(R.id.adapter_directoryinquirylist_call).getView().setPadding(item_padding, 0, item_padding, 0);

					aq.gravity(R.id.adapter_directoryinquirylist_call, Gravity.CENTER);

					Drawable drawble = context.getResources().getDrawable(R.drawable.phone_small);

					ColorFilter filter = new PorterDuffColorFilter(Color.parseColor("#187DC3"), PorterDuff.Mode.SRC_IN);

					drawble.setColorFilter(filter);

					int h = drawble.getIntrinsicHeight();
					int w = drawble.getIntrinsicWidth();
					int imageH = buttonHeight - extralinespace *3 /4;
					int imageW = w * (imageH) / h;
					drawble.setBounds( 0, 0, imageW, imageH);


					if (debug) Log.i("DQ" , buttonHeight + "/" + imageH + "/" + imageW);
					holder.adapter_directoryinquirylist_call.setDrawable(drawble);
//					aq.id(R.id.adapter_directoryinquirylist_call_icon).image(drawble);
//					aq.id(R.id.adapter_directoryinquirylist_call_icon).getImageView().setImageBitmap(bm);
//					aq.id(R.id.adapter_directoryinquirylist_call_icon).height((int) context.getResources().getDimension(R.dimen.button_height)/4).width((int) context.getResources().getDimension(R.dimen.button_height)/4);
//					aq.id(R.id.adapter_directoryinquirylist_call_icon).getImageView().setPadding(20/2, 20/2, 20/2, 20/2);
//					aq.marginpx(R.id.adapter_directoryinquirylist_call_icon, 10,10,10,10);
					aq.marginpx(R.id.adapter_directoryinquirylist_call , item_padding , 0 , item_padding, 0);
					holder.adapter_directoryinquirylist_call.setOnClickListener(new View.OnClickListener() {
						public void onClick(View v) {
							onCallPhoneListener.onCallPhoneNumber(String.format(Utils.getString(context, R.string.SUPPORT_PREMIER_TEL_FORMAT), telNum));
//							Intent dialIntent = new Intent(Intent.ACTION_CALL, Uri.parse(String.format(Utils.getString(context, R.string.SUPPORT_PREMIER_TEL_FORMAT), telNum)));
//							context.startActivity(dialIntent);
						}
					});
				}

				if (lastItem <= position && enableAnimation) {
					Animation animation = AnimationUtils.loadAnimation(context, R.anim.list_right_fade_in);
					animation.setDuration(300);
					convertView.startAnimation(animation);
					animation = null;
				}
			}
		} catch (Exception e) {
			if (debug) e.printStackTrace();
		}
		return convertView;
	}

	public final void addDirectoryInquiryRec(List<DQSrchDtl> rDqSrchDtl) {
		DQAgent rDQAgent;
		List<DQAgent> rDQAgentLst;
		int rx, rl;
		rDQAgentLst = new ArrayList<DQAgent>();
		// Original List
		rl = dQSrchDtlList.length;
		for (rx = 0; rx < rl; rx++) {
			rDQAgent = new DQAgent();
			rDQAgent.isHeader = dQSrchDtlList[rx].isHeader;
			rDQAgent.header = dQSrchDtlList[rx].header;
			rDQAgent.dQSrchDtl = dQSrchDtlList[rx].dQSrchDtl;
			rDQAgent.address = dQSrchDtlList[rx].address;

			rDQAgentLst.add(rDQAgent);
		}
		// Added next 10 Records
		rl = rDqSrchDtl.size();
		String previousHeader = new String();
		for (rx = 0; rx < rl; rx++) {
			if (!"".equalsIgnoreCase(rDqSrchDtl.get(rx).name)) {
				rDQAgent = new DQAgent();
				rDQAgent.isHeader = true;
				rDQAgent.header = Html.fromHtml(rDqSrchDtl.get(rx).name).toString().trim();
				rDQAgent.dQSrchDtl = trimDQSrchDtl(rDqSrchDtl.get(rx));
				rDQAgent.address = prepareAddress(rDQAgent.dQSrchDtl);

				rDQAgentLst.add(rDQAgent);

				previousHeader = rDQAgent.header;
			}
			rDQAgent = new DQAgent();
			rDQAgent.isHeader = false;
			rDQAgent.header = previousHeader;
			rDQAgent.dQSrchDtl = trimDQSrchDtl(rDqSrchDtl.get(rx));
			rDQAgent.address = prepareAddress(rDQAgent.dQSrchDtl);

			rDQAgentLst.add(rDQAgent);
		}

		dQSrchDtlList = (DQAgent[]) rDQAgentLst.toArray(new DQAgent[0]);
	}

	public static class ViewHolder {
		TextView		adapter_directoryinquirylist_separator;		// subhead for separating the item
		LinearLayout	adapter_directoryinquirylist_layout_content;
		LinearLayout	adapter_directoryinquirylist_layout_content1;
		HKTButton	adapter_directoryinquirylist_call;
		TextView		adapter_directoryinquirylist_address;
		TextView		adapter_directoryinquirylist_fax;
		TextView		adapter_directoryinquirylist_call_txt;
		ImageView		adapter_directoryinquirylist_line1;
		ImageView		adapter_directoryinquirylist_line2;
		ImageView		adapter_directoryinquirylist_address_icon;
		ImageView		adapter_directoryinquirylist_fax_icon;
		ImageView		adapter_directoryinquirylist_call_icon;
	}

	private final void prepareDQAgent(List<DQSrchDtl> rDqSrchDtl) {
		DQAgent rDQAgent;
		List<DQAgent> rDQAgentLst;
		int rx, rl;
		rDQAgentLst = new ArrayList<DQAgent>();
		rl = rDqSrchDtl.size();
		String previousHeader = new String();
		for (rx = 0; rx < rl; rx++) {
			if (rx == 0 || (!"".equalsIgnoreCase(rDqSrchDtl.get(rx).name))) {
				rDQAgent = new DQAgent();
				rDQAgent.isHeader = true;
				rDQAgent.header = Html.fromHtml(rDqSrchDtl.get(rx).name).toString().trim();
				rDQAgent.dQSrchDtl = trimDQSrchDtl(rDqSrchDtl.get(rx));
				rDQAgent.address = prepareAddress(rDQAgent.dQSrchDtl);

				rDQAgentLst.add(rDQAgent);

				previousHeader = rDQAgent.header;
			}
			rDQAgent = new DQAgent();
			rDQAgent.isHeader = false;
			rDQAgent.header = previousHeader;
			rDQAgent.dQSrchDtl = trimDQSrchDtl(rDqSrchDtl.get(rx));
			rDQAgent.address = prepareAddress(rDQAgent.dQSrchDtl);

			rDQAgentLst.add(rDQAgent);
		}

		dQSrchDtlList = (DQAgent[]) rDQAgentLst.toArray(new DQAgent[0]);
	}

	private final String prepareAddress(DQSrchDtl dQSrchDtl) {
		String addressStr = "";

		if (!"".equalsIgnoreCase(dQSrchDtl.addr)) {
			addressStr = dQSrchDtl.addr;
		}
		if (!"".equalsIgnoreCase(dQSrchDtl.cap1)) {
			addressStr = ("".equalsIgnoreCase(addressStr)) ? dQSrchDtl.cap1 : addressStr + ", " + dQSrchDtl.cap1;
		}
		if (!"".equalsIgnoreCase(dQSrchDtl.cap2)) {
			addressStr = ("".equalsIgnoreCase(addressStr)) ? dQSrchDtl.cap2 : addressStr + ", " + dQSrchDtl.cap2;
		}
		if (!"".equalsIgnoreCase(dQSrchDtl.cap3)) {
			addressStr = ("".equalsIgnoreCase(addressStr)) ? dQSrchDtl.cap3 : addressStr + ", " + dQSrchDtl.cap3;
		}
		if (!"".equalsIgnoreCase(dQSrchDtl.cap4)) {
			addressStr = ("".equalsIgnoreCase(addressStr)) ? dQSrchDtl.cap4 : addressStr + ", " + dQSrchDtl.cap4;
		}

		return addressStr.trim();
	}

	private final DQSrchDtl trimDQSrchDtl(DQSrchDtl dQSrchDtl) {
		DQSrchDtl rDQSrchDtl = dQSrchDtl.copyMe();

		rDQSrchDtl.name = Html.fromHtml(rDQSrchDtl.name).toString().trim();
		rDQSrchDtl.addr = Html.fromHtml(rDQSrchDtl.addr).toString().trim();
		rDQSrchDtl.cap1 = Html.fromHtml(rDQSrchDtl.cap1).toString().trim();
		rDQSrchDtl.cap2 = Html.fromHtml(rDQSrchDtl.cap2).toString().trim();
		rDQSrchDtl.cap3 = Html.fromHtml(rDQSrchDtl.cap3).toString().trim();
		rDQSrchDtl.cap4 = Html.fromHtml(rDQSrchDtl.cap4).toString().trim();
		rDQSrchDtl.type = rDQSrchDtl.type.trim();
		rDQSrchDtl.number = rDQSrchDtl.number.trim();

		return rDQSrchDtl;
	}

	private final void displayDialog(String title, String message) {
		DialogHelper.createTitleDialog(context, title, message);
	}
	
	public void setLeftDrawable(ImageView iv, int resId) {
		iv.setImageResource(resId);
		int item_padding = context.getResources().getDimensionPixelOffset(R.dimen.item_padding);
		LayoutParams params = (LayoutParams) iv.getLayoutParams();
		params.leftMargin = item_padding;
		params.rightMargin = item_padding;
		params.topMargin = item_padding;
		params.bottomMargin = item_padding;
	}

	public final class DQAgent {
		public boolean		isHeader;
		public String		header;
		public String		address;
		public DQSrchDtl	dQSrchDtl;
	}

}
