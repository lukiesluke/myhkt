/*
    Crate for Plan (IMS)
    
    Keywords
    --------
    $URL: $
    $Rev: $
    $Date: $
    $Author: $
*/

package com.pccw.dango.shared.cra;

import java.io.Serializable;

import com.pccw.dango.shared.entity.SrvPlan;
import com.pccw.dango.shared.entity.SubnRec;

public class PlanImsCra extends BaseCraEx implements Serializable
{
    private static final long serialVersionUID = 7220639556884820615L;
    
    private String                  iLoginId;           /* Login ID                                      */
    private SubnRec                 iSubnRec;           /* Subscription Record                           */
    
    private SrvPlan                 oMthFee;            /* Monthly Fee                                   */
    private SrvPlan[]               oVasAry;            /* Array of VAS                                  */
    private SrvPlan[]               oIncAry;            /* Array of Incentive                            */
    private SrvPlan[]               oCommAry;           /* Array of Commitment                           */
    private Boolean                 oPonCvg;            /* Pon Coverage                                  */


    public static void main(String rArg[])
    {
        System.out.println(getVer());
        return;
    }
    
    
    public static String getVer()
    {
        return ("$URL: $, $Rev: $");
    }

    
    public PlanImsCra()
    {
        initAndClear();
    }


    protected void init()
    {
        super.init();
    }


    public void clear()
    {
        super.clear();
        
        clearILoginId();
        clearISubnRec();
        clearOMthFee();
        clearOVasAry();
        clearOIncAry();
        clearOCommAry();
        clearOPonCvg();
    }


    public PlanImsCra copyFrom(PlanImsCra rSrc)
    {
        super.copyFrom(rSrc);
        
        setILoginId(rSrc.getILoginId());
        setISubnRec(rSrc.getISubnRec());
        setOMthFee(rSrc.getOMthFee());
        setOVasAry(rSrc.getOVasAry());
        setOIncAry(rSrc.getOIncAry());
        setOCommAry(rSrc.getOCommAry());
        setOPonCvg(rSrc.getOPonCvg());

        return (this);
    }


    public PlanImsCra copyTo(PlanImsCra rDes)
    {
        rDes.copyFrom(this);
        return (rDes);
    }
    
    
    public PlanImsCra copyMe()
    {
        PlanImsCra                  rDes;

        rDes = new PlanImsCra();
        rDes.copyFrom(this);
        return (rDes);
    }


    public void clearILoginId()
    {
        iLoginId = "";
    }


    public void setILoginId(String rArg)
    {
        iLoginId = rArg;
    }


    public String getILoginId()
    {
        return (iLoginId);
    }


    public void clearISubnRec()
    {
        iSubnRec = new SubnRec();
    }


    public void setISubnRec(SubnRec rArg)
    {
        iSubnRec = rArg;
    }


    public SubnRec getISubnRec()
    {
        return (iSubnRec);
    }


    public void clearOMthFee()
    {
        oMthFee = new SrvPlan();
    }


    public void setOMthFee(SrvPlan rArg)
    {
        oMthFee = rArg;
    }


    public SrvPlan getOMthFee()
    {
        return (oMthFee);
    }


    public void clearOVasAry()
    {
        oVasAry = new SrvPlan[0];
    }


    public void setOVasAry(SrvPlan[] rArg)
    {
        oVasAry = rArg;
    }


    public SrvPlan[] getOVasAry()
    {
        return (oVasAry);
    }


    public void clearOIncAry()
    {
        oIncAry = new SrvPlan[0];
    }


    public void setOIncAry(SrvPlan[] rArg)
    {
        oIncAry = rArg;
    }


    public SrvPlan[] getOIncAry()
    {
        return (oIncAry);
    }


    public void clearOCommAry()
    {
        oCommAry = new SrvPlan[0];
    }


    public void setOCommAry(SrvPlan[] rArg)
    {
        oCommAry = rArg;
    }


    public SrvPlan[] getOCommAry()
    {
        return (oCommAry);
    }


    public void clearOPonCvg()
    {
        oPonCvg = new Boolean(false);
    }


    public void setOPonCvg(Boolean rArg)
    {
        oPonCvg = rArg;
    }


    public Boolean getOPonCvg()
    {
        return (oPonCvg);
    }
}
