package com.pccw.myhkt.activity;

import android.Manifest;
import android.content.Context;
import androidx.test.rule.ActivityTestRule;
import androidx.test.rule.GrantPermissionRule;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import com.pccw.myhkt.R;
import com.pccw.myhkt.base.BaseTest;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static androidx.test.platform.app.InstrumentationRegistry.getInstrumentation;

@Ignore
@RunWith(AndroidJUnit4.class)
public class ShopListUIActivityTest extends BaseTest {
    private Context context;

    @Rule
    public ActivityTestRule<MainMenuActivity> activityTestRule = new ActivityTestRule<>(
            MainMenuActivity.class);
    @Rule public GrantPermissionRule permissionRule = GrantPermissionRule.grant(
            Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION);

    @Before
    public void setUp(){
        context = getInstrumentation().getTargetContext();
        gotoMainMenuItem(10);

        clickViewInAdapter(R.id.shoptype_listview,1);
    }

    @Test
    public void testDisplayUI() {
        delay(TestTimeouts.ACTIVITY_LOAD_TIMEOUTS);
        checkStringIsDisplayed(context, R.string.myhkt_shop_title);
        checkViewIsDisplayed(R.id.navbar_button_left, R.id.navbar_base_layout);
        checkStringIsDisplayed(context, R.string.myhkt_shop_hk);
        checkStringIsDisplayed(context, R.string.myhkt_shop_kln);
        checkStringIsDisplayed(context, R.string.myhkt_shop_nt);
        checkViewIsDisplayed(R.id.shopdistrict_indictaor);
        checkViewIsDisplayed(R.id.shopdistrict_commonview);
    }

    @Test
    public void testHongKongDisplay() {
        delay(TestTimeouts.ACTIVITY_LOAD_TIMEOUTS);
        clickString(context, R.string.myhkt_shop_hk);
    }

    @Test
    public void testKowloonDisplay() {
        delay(TestTimeouts.ACTIVITY_LOAD_TIMEOUTS);
        clickString(context, R.string.myhkt_shop_kln);
    }

    @Test
    public void testNewTerritoriesDisplay() {
        delay(TestTimeouts.ACTIVITY_LOAD_TIMEOUTS);
        clickString(context, R.string.myhkt_shop_nt);
    }

    @Test
    public void testBackClick() {
        clickLeftNavBar();
    }

}