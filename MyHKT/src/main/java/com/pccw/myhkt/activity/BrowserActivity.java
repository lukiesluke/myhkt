package com.pccw.myhkt.activity;

import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import android.webkit.JavascriptInterface;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.pccw.myhkt.APIsManager;
import com.pccw.myhkt.ClnEnv;
import com.pccw.myhkt.DialogHelper;
import com.pccw.myhkt.R;
import com.pccw.myhkt.Utils;
import com.pccw.myhkt.lib.ui.AAQuery;
import com.pccw.myhkt.model.ShareWallet;

import org.apache.http.cookie.Cookie;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/************************************************************************
 * File : BrowserActivity.java
 * Desc : Web Page
 * Name : BrowserActivity
 * by 	: Andy Wong
 * Date : 22/12/2015
 * Change History:
 * Date       Modified By		Description
 * ---------- ----------------	-------------------------------
 * 22/12/2015 Andy Wong 		-First draft
 * 09/05/2016 Andy Wong 		-Extands for base activity
 * 04/12/2017 Abudlmoiz Esmail  -Added getting of boolean for intent if to show livechat, default value is true
 * 28/09/2023 Luke Garces 		-Read QR/Barcode and share link via javascript callback function
 *************************************************************************/
public class BrowserActivity extends BaseActivity {
    public static String INTENTMODULEID = "moduleId";
    public static String INTENTLINK = "link";
    public static String INTENTTITLE = "title";
    public static String INTENTLOB = "lob";
    public static String INTENTLOBTYPE = "type";
    public static String ISTOSHOWLIVECHAT = "is_to_show_live_chat";
    public static String AUTHORIZATION = "Authorization";
    private static int lob = 0;
    private boolean debug = false;
    private String url = null;
    private String title = null;
    private int type = 0;
    private String authorization = "";
    private AAQuery aq;
    private WebView webView;
    private Gson gson;
    private ShareWallet shareWallet;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        debug = getResources().getBoolean(R.bool.DEBUG);
        setContentView(R.layout.activity_browser);
        gson = new Gson();

        Intent intent = getIntent();
        if (intent != null && intent.getExtras() != null) {
            moduleId = intent.getExtras().getString(INTENTMODULEID);
            url = intent.getExtras().getString(INTENTLINK);
            title = intent.getExtras().getString(INTENTTITLE, "");
            lob = intent.getExtras().getInt(INTENTLOB);
            type = intent.getExtras().getInt(INTENTLOBTYPE);
            if (intent.getExtras().getString(AUTHORIZATION) != null) {
                authorization = intent.getExtras().getString(AUTHORIZATION);
            }
            isLiveChatShown = intent.getExtras().getBoolean(ISTOSHOWLIVECHAT, true);
        }
    }

    @Override
    protected void initUI2() {
        aq = new AAQuery(this);

        //navbar style
        aq.navBarButton(R.id.navbar_button_left, R.drawable.btn_back);
        if (lob > 0) {
            aq.navBarTitle(R.id.navbar_title, R.id.navbar_leftdraw, 0, Utils.getLobIcon(lob, type), 0, title);
        } else {
            aq.navBarTitle(R.id.navbar_title, title, Utils.getScreenSize(this));
        }

        // Hide live chat on My Wallet
        if (getString(R.string.MYHKT_HOME_BTN_WALLET_WEB).equals(title)) isLiveChatShown = false;

        if (lob == R.string.CONST_LOB_LTS) {
            String rightText = Utils.getLtsTtypeRightText(this, type);
            aq.id(R.id.navbar_righttext).text(rightText).textColorId(R.color.hkt_txtcolor_grey);
        }

        webView = null;

        aq.id(R.id.navbar_button_left).clicked(this, "onClick");
        webView = aq.id(R.id.activity_browser_webview).getWebView();

        if (url != null) displayWebView(url);
        webView.setOnKeyListener((v, keyCode, event) -> {
            if ((keyCode == KeyEvent.KEYCODE_BACK) && webView.canGoBack()) {
                endAppWithTransition();
                return true;
            }
            return false;
        });
    }

    @SuppressLint({"SetJavaScriptEnabled", "AddJavascriptInterface"})
    private void displayWebView(String url) {
        webView.setWebViewClient(new WebViewOverrideUrl());
        webView.setWebChromeClient(new WebChromeClient());
        webView.setVisibility(View.VISIBLE);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setBuiltInZoomControls(true);
        webView.getSettings().setDisplayZoomControls(false);
        webView.getSettings().setLoadWithOverviewMode(true);
        webView.getSettings().setUseWideViewPort(true);
        webView.getSettings().setDomStorageEnabled(true);

        webView.addJavascriptInterface(new Object() {
            @JavascriptInterface
            public void customWebToAppDataWithJson(String jsonString) {
                shareWallet = new ShareWallet();
                shareWallet = gson.fromJson(jsonString, ShareWallet.class);
                Log.d("lwg", "customWebToAppDataWithJson jsonString: " + jsonString);

                if (shareWallet.getAction().equals("nativeShare")) {
                    Intent sendIntent = new Intent();
                    sendIntent.setAction(Intent.ACTION_SEND);
                    sendIntent.putExtra(Intent.EXTRA_SUBJECT, shareWallet.getTitle());
                    sendIntent.putExtra(Intent.EXTRA_TEXT, shareWallet.getMessage());
                    sendIntent.setType("text/plain");
                    Intent shareIntent = Intent.createChooser(sendIntent, null);
                    startActivity(shareIntent);

                } else if (shareWallet.getAction().equals("scannerQRCode")) {
                    IntentIntegrator intentIntegrator = new IntentIntegrator(BrowserActivity.this);
                    intentIntegrator.setPrompt("Scan a barcode or QR Code");
                    intentIntegrator.setOrientationLocked(true);
                    intentIntegrator.setCaptureActivity(CaptureActivityPortrait.class);
                    intentIntegrator.initiateScan();
                }
            }
        }, "web5gAndroid");

        String cookieString;

        CookieSyncManager cookieSyncManager = CookieSyncManager.createInstance(this);
        CookieManager cookieManager = CookieManager.getInstance();

        List<Cookie> cookies = APIsManager.mCookies;
        if (cookies == null) {
            cookies = ClnEnv.getHttpClient().getCookieStore().getCookies();
        }
        cookieManager.setAcceptCookie(true);

        Cookie sessionInfo;
        // call remove All Cookie after Android 4.0
        cookieManager.removeAllCookie(); //remove

        for (int i = 0; i < cookies.size(); i++) {
            sessionInfo = cookies.get(i);
            cookieString = sessionInfo.getName() + "=" + sessionInfo.getValue() + ";domain=" + sessionInfo.getDomain();

            cookieManager.setCookie(sessionInfo.getDomain(), cookieString);
            cookieSyncManager.sync();
        }

        // Add header Authorization to request
        Map<String, String> extraHeaders = new HashMap<>();
        extraHeaders.put("Authorization", authorization);
        if (!"".equalsIgnoreCase(url)) webView.loadUrl(url, extraHeaders);
    }

    public void onClick(View v) {
        if (v.getId() == R.id.navbar_button_left) {
            onBackPressed();
        }
    }

    public void onBackPressed() {
        endAppWithTransition();
    }

    private void endAppWithTransition() {
        finish();
        overridePendingTransition(R.anim.left_slide_in, R.anim.right_slide_out);
    }

    private void myWalletExitConfirmation() {
        DialogInterface.OnClickListener onPositiveClickListener = (dialogInterface, i) -> {
            finish();
            overridePendingTransition(R.anim.left_slide_in, R.anim.right_slide_out);
        };
        DialogHelper.createSimpleDialog(this, getString(R.string.myhkt_mywallet_exitweb), getString(R.string.myhkt_btn_yes), onPositiveClickListener, getString(R.string.myhkt_btn_no));
    }

    // Free all references to the UI elements
    protected void cleanupUI() {
        url = null;
        aq = null;
    }

    private void loadJavaScriptFunction(String barcodeValue) {
        // Set barcode value to shareWallet data
        shareWallet.setData(barcodeValue);

        // Convert shareWallet object to string
        String dataScript = getApplication().getString(R.string.customAppToWebDataWithJson, gson.toJson(shareWallet));

        // Execute java script function dataScript
        webView.evaluateJavascript(dataScript, result -> Log.d("lwg", "onReceiveValue: " + result));
        Log.d("lwg", "jsonData: " + dataScript);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        IntentResult intentResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, intent);
        if (intentResult != null) {
            if (intentResult.getContents() == null) {
                Toast.makeText(getBaseContext(), "Cancelled Scan QR", Toast.LENGTH_SHORT).show();
            } else {
                // Send QR/Barcode value to loadJavaScriptFunction
                loadJavaScriptFunction(intentResult.getContents());
            }
        } else {
            super.onActivityResult(requestCode, resultCode, intent);
        }
    }

    //web-view override url to avoid redirect at web browser outside the app
    private class WebViewOverrideUrl extends WebViewClient {
        boolean isReceiveError = false;

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            if (debug) Log.d("Loading", "url : " + url);

            if (url.contains("mailto:")) {
                Intent intent = new Intent(Intent.ACTION_SEND);
                String[] tos = {"hktpremiercr@pccw.com"};
                intent.putExtra(Intent.EXTRA_EMAIL, tos);
                intent.setType("message/rfc822");
                me.startActivity(Intent.createChooser(intent, "Your Client"));
            } else {
                if (url.contains(".")) {
                    String extension = url.substring(url.lastIndexOf("."));
                    if (extension.equals(".pdf")) {
                        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                        startActivity(browserIntent);
                    } else {
                        Map<String, String> extraHeaders = new HashMap<>();
                        extraHeaders.put("Authorization", authorization);
                        view.loadUrl(url, extraHeaders);
                    }
                }
            }
            return true;
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            if (!isReceiveError) {
                view.setVisibility(View.VISIBLE);
            } else {
                view.setVisibility(View.GONE);
            }

            String cookies = CookieManager.getInstance().getCookie(ClnEnv.getPref(me, getString(R.string.CONST_PREF_DOMAIN), APIsManager.PRD_domain));
            if (debug)
                Log.d("THIS IS COOKIE", "onPageFinished All the cookies in a string:" + cookies);
        }

        @Override
        public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
            super.onReceivedError(view, errorCode, description, failingUrl);

            isReceiveError = true;
            view.setVisibility(View.GONE);
        }
    }
}
