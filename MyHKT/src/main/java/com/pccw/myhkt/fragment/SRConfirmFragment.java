package com.pccw.myhkt.fragment;

import android.app.Activity;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;

import com.pccw.dango.shared.cra.ApptCra;
import com.pccw.dango.shared.entity.SubnRec;
import com.pccw.myhkt.FAWrapper;
import com.pccw.myhkt.R;
import com.pccw.myhkt.Utils;
import com.pccw.myhkt.fragment.SRCreationFragment.OnLnttListener;
import com.pccw.myhkt.lib.ui.AAQuery;
import com.pccw.myhkt.lib.ui.HKTButton;
import com.pccw.myhkt.model.LnttAgent;

/************************************************************************
 * File : SRConfirmFragment.java 
 * Desc : SR confirm page
 * Name : SRConfirmFragment
 * by : Derek Tsui 
 * Date : 10/03/2016
 * 
 * Change History:
 * Date       Modified By		Description
 * ---------- ----------------	-------------------------------
 * 10/03/2016 Derek Tsui 		-First draft
 *************************************************************************/

public class SRConfirmFragment extends BaseServiceFragment{
	private SRConfirmFragment me;
	private AAQuery aq;
	private FragmentTransaction ft;
	private View myView;
	
	private String[]				timeAry;
	private int 		 			timePos = 0;
	private int 				buttonPadding;
	private String[]			titleAry;
	private int					titlePos = 0;
	private ApptCra				apptCra;
	
	private OnLnttListener callback_lntt;
	
	@Override
	public void onAttach(@NonNull Activity activity) {
		super.onAttach(activity); 
		callback_lntt = (OnLnttListener)getParentFragment();
		apptCra = callback_main.getApptCra();
	}
	
	public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		me = this;
		View fragmentLayout = inflater.inflate(R.layout.fragment_sr_confirm, container, false);
		myView = fragmentLayout;
		initData();
		return fragmentLayout;
	}
	
	protected void initData() {  
		super.initData();	
		buttonPadding = (int) getResources().getDimension(R.dimen.reg_logo_padding);
	}

	@Override
	public void onStart() {
		super.onStart();
	}
	
	protected void initUI(){
		aq = new AAQuery(myView);
		
		aq.marginpx(R.id.srconfirm_layout2, basePadding, 0, basePadding, 0);
		
		//Title
		aq.id(R.id.srconfirm_header).getTextView().setCompoundDrawablesWithIntrinsicBounds(R.drawable.icon_apptx3, 0,0,0);
		aq.id(R.id.srconfirm_header).getTextView().setCompoundDrawablePadding(buttonPadding);
		aq.normText(R.id.srconfirm_header, getResString(R.string.MYHKT_SR_HDR_CREATE));
		
		//Remark
		aq.normTextGrey(R.id.srconfirm_remark, getResString(R.string.MYHKT_SR_MSG_REPORTED), -2);
		aq.id(R.id.srconfirm_remark).height(ViewGroup.LayoutParams.WRAP_CONTENT);
		aq.marginpx(R.id.srconfirm_remark, 0, 0, 0, basePadding);
		
		//SubHeader
		aq.normTextGrey(R.id.srconfirm_subheader, getResString(R.string.ATIMF_HDR), -2);
		aq.id(R.id.srconfirm_subheader).height(ViewGroup.LayoutParams.WRAP_CONTENT);
		aq.id(R.id.srconfirm_subheader).getTextView().setCompoundDrawablesWithIntrinsicBounds(R.drawable.rightarrow_small, 0, 0, 0);
		aq.id(R.id.srconfirm_subheader).getTextView().setCompoundDrawablePadding(basePadding);

		if (callback_main.getLob() == R.string.CONST_LOB_LTS) {
			aq.id(R.id.srconfirm_accnum_desc).text(getResString(R.string.LTTF_LTS_ACCTNUM));
		} else if (callback_main.getLob() == R.string.CONST_LOB_TV) {
			aq.id(R.id.srconfirm_accnum_desc).text(getResString(R.string.LTTF_TV_ACCTNUM));
		} else {
			aq.id(R.id.srconfirm_accnum_desc).text(getResString(R.string.REGM_ASO_SVN_PCD));
		}
		
		aq.marginpx(R.id.srconfirm_accnum_layout, 0, 0, 0, extralinespace);
		aq.marginpx(R.id.srconfirm_time_layout, 0, 0, 0, extralinespace);
		aq.marginpx(R.id.srconfirm_contperson_layout, 0, 0, 0, extralinespace);
		aq.marginpx(R.id.srconfirm_contnum_layout, 0, 0, 0, extralinespace);

		aq.id(R.id.srconfirm_accnum_desc	).textSize(AAQuery.getDefaultTextSize()-2).textColorId(R.color.hkt_txtcolor_grey);
		aq.id(R.id.srconfirm_time_desc		).textSize(AAQuery.getDefaultTextSize()-2).textColorId(R.color.hkt_txtcolor_grey);
		aq.id(R.id.srconfirm_contperson_desc).textSize(AAQuery.getDefaultTextSize()-2).textColorId(R.color.hkt_txtcolor_grey);
		aq.id(R.id.srconfirm_contnum_desc	).textSize(AAQuery.getDefaultTextSize()-2).textColorId(R.color.hkt_txtcolor_grey);
		
		aq.id(R.id.srconfirm_accnum_txt		).textSize(AAQuery.getDefaultTextSize()-2).textColorId(R.color.hkt_textcolor);
		aq.id(R.id.srconfirm_time_txt		).textSize(AAQuery.getDefaultTextSize()-2).textColorId(R.color.hkt_textcolor);
		aq.id(R.id.srconfirm_contperson_txt	).textSize(AAQuery.getDefaultTextSize()-2).textColorId(R.color.hkt_textcolor);
		aq.id(R.id.srconfirm_contnum_txt	).textSize(AAQuery.getDefaultTextSize()-2).textColorId(R.color.hkt_textcolor);

		
		aq.padding(R.id.srconfirm_accnum_txt, basePadding , 0, 0, 0);
		aq.padding(R.id.srconfirm_time_txt, basePadding , 0, 0, 0);
		aq.padding(R.id.srconfirm_contperson_txt, basePadding , 0, 0, 0);
		aq.padding(R.id.srconfirm_contnum_txt, basePadding , 0, 0, 0);
		
		aq.id(R.id.srconfirm_msg_upgrade).text(R.string.MYHKT_SR_MSG_UPGRADE);
		aq.id(R.id.srconfirm_msg_upgrade).textColorId(R.color.hkt_txtcolor_grey);
		aq.id(R.id.srconfirm_msg_upgrade).textSize(AAQuery.getDefaultTextSize()-2);
		aq.marginpx(R.id.srconfirm_msg_upgrade, basePadding, 0, basePadding, extralinespace);
		
		((HKTButton) aq.id(R.id.srconfirm_btn_ok).getView()).initViews(getActivity(), getResources().getString(R.string.btn_ok), LayoutParams.MATCH_PARENT);
		aq.marginpx(R.id.srconfirm_btn_ok, basePadding, basePadding, basePadding, basePadding);
		aq.id(R.id.srconfirm_btn_ok).clicked(this, "onClick");

	}

	@Override
	public void onResume(){
		super.onResume();
		// Screen Tracker
		if(isShownPonCvgUpgrade()){
			if (SubnRec.LOB_LTS.equalsIgnoreCase(callback_main.getSubnRec().lob)) {
				callback_livechat.setModuleId(getResString(R.string.MODULE_LTS_PON));
				FAWrapper.getInstance().sendFAScreen(getActivity(),
						R.string.CONST_SCRN_LTSSRCREATION_CONFIRM, false);
			} else if (SubnRec.LOB_PCD.equalsIgnoreCase(callback_main.getSubnRec().lob)) {
				callback_livechat.setModuleId(getResString(R.string.MODULE_PCD_PON));
				FAWrapper.getInstance().sendFAScreen(getActivity(),
						R.string.CONST_SCRN_PCDSRCREATION_CONFIRM, false);
			} else if (SubnRec.LOB_TV.equalsIgnoreCase(callback_main.getSubnRec().lob)) {
				callback_livechat.setModuleId(getResString(R.string.MODULE_TV_PON));
				FAWrapper.getInstance().sendFAScreen(getActivity(),
						R.string.CONST_SCRN_TVSRCREATION_CONFIRM, false);
			}
		}
		else {
			if (SubnRec.LOB_LTS.equalsIgnoreCase(callback_main.getSubnRec().lob)) {
				callback_livechat.setModuleId(getResString(R.string.MODULE_LTS_SR));
				FAWrapper.getInstance().sendFAScreen(getActivity(),
						R.string.CONST_SCRN_LTSSRCREATION_CONFIRM, false);
			} else if (SubnRec.LOB_PCD.equalsIgnoreCase(callback_main.getSubnRec().lob)) {
				callback_livechat.setModuleId(getResString(R.string.MODULE_PCD_SR));
				FAWrapper.getInstance().sendFAScreen(getActivity(),
						R.string.CONST_SCRN_PCDSRCREATION_CONFIRM, false);
			} else if (SubnRec.LOB_TV.equalsIgnoreCase(callback_main.getSubnRec().lob)) {
				callback_livechat.setModuleId(getResString(R.string.MODULE_TV_SR));
				FAWrapper.getInstance().sendFAScreen(getActivity(),
						R.string.CONST_SCRN_TVSRCREATION_CONFIRM, false);
			}
		}
	}

	private boolean isShownPonCvgUpgrade(){
		return SubnRec.CONST_Y.equalsIgnoreCase(apptCra.getOPendSrvReq().getSrInfo().getPonCvg()) &&
				SubnRec.CONST_N.equalsIgnoreCase(apptCra.getOPendSrvReq().getSrInfo().getPonLine()) &&
				!SubnRec.LOB_LTS.equalsIgnoreCase(callback_main.getSubnRec().lob);
	}
	
	@Override
	public void setUserVisibleHint(boolean isFragmentVisible) {
	    super.setUserVisibleHint(isFragmentVisible);
	}
	
	protected final void refreshData() {
		super.refreshData();
		callback_lntt = (OnLnttListener)getParentFragment();
		aq = new AAQuery(myView);
		if (callback_lntt.getActiveChildview() == R.string.CONST_SELECTEDVIEW_SRCREATION_CONFIRM) {
			apptCra = callback_main.getApptCra();
			if (apptCra != null) {
				aq.id(R.id.srconfirm_accnum_txt).text(apptCra.getISubnRec().srvNum); //TODO check if this works
//				aq.id(R.id.srconfirm_accnum_txt).text("a60058422"); 
				aq.id(R.id.srconfirm_time_txt).text(Utils.getTimeSlotDateTime1(me.getActivity(), apptCra.getISrvReq().getApptTS().getApptDate(), apptCra.getISrvReq().getApptTS().getApptTmslot()));
				aq.id(R.id.srconfirm_contperson_txt).text(String.format("%s %s", apptCra.getISrvReq().getCtNmTtl(), apptCra.getISrvReq().getCtNm()));
				aq.id(R.id.srconfirm_contnum_txt).text(apptCra.getISrvReq().getCtNum());

				if (callback_main.getLob() == R.string.CONST_LOB_LTS) {
					// disable PON upgrade message and button for LTS service SR
					aq.id(R.id.srconfirm_msg_upgrade).visibility(View.GONE);
				} else {
					if ("Y".equalsIgnoreCase(apptCra.getOPendSrvReq().getSrInfo().getPonCvg()) && "N".equalsIgnoreCase(apptCra.getOPendSrvReq().getSrInfo().getPonLine())) {
						aq.id(R.id.srconfirm_msg_upgrade).visibility(View.VISIBLE);
					} else {
						aq.id(R.id.srconfirm_msg_upgrade).visibility(View.GONE);
					}
				}
			}
		}
	}
	
	
	
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.srconfirm_btn_ok:
			// Clear all Data before back to Start LineTest
			callback_main.setLnttAgent(new LnttAgent());
			callback_main.setApptCra(null);
			callback_lntt.setActiveChildview(R.string.CONST_SELECTEDVIEW_LINETEST1);
			callback_lntt.displayChildview(OnLnttListener.BACK);			
			break;
		}
	}
	

	


}
