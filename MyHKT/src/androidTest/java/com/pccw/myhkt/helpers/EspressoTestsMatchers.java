package com.pccw.myhkt.helpers;

        import android.view.View;

        import org.hamcrest.Matcher;

public class EspressoTestsMatchers {
    public static Matcher<View> withDrawable(final int resourceId) {
        return new DrawableMatchers(resourceId);
    }

    public static Matcher<View> noDrawable() {
        return new DrawableMatchers(-1);
    }
}
