package com.pccw.myhkt.fragment;

import android.app.Activity;

import androidx.annotation.NonNull;

import com.pccw.dango.shared.entity.ApptInfo;
import com.pccw.dango.shared.entity.SRApptInfo;
import com.pccw.dango.shared.entity.SrvReq;
import com.pccw.dango.shared.entity.SubnRec;
import com.pccw.myhkt.APIsResponse;

/************************************************************************
 * File : BaseServiceFragment.java
 * Desc : Special base fragment for ServiceActivity related fragments
 * Name : BaseServiceFragment
 * by 	: Derek Tsui
 * Date : 01/03/2016
 * 
 * Change History:
 * Date       Modified By		Description
 * ---------- ----------------	-------------------------------
 * 01/03/2016 Derek Tsui 		-First draft
 *************************************************************************/

public class BaseMyApptFragment extends BaseFragment {
	// CallBack
	protected OnMyApptListener		callback_main;
	
	@Override
	public void onAttach(@NonNull Activity activity) {
		super.onAttach(activity);

		try {
			callback_main = (OnMyApptListener) activity;
		} catch (ClassCastException e) {
			throw new ClassCastException(activity.toString() + " must implement OnMyApptListener");
		}
	}
	
	//Interface
	public interface OnMyApptListener {
		public SubnRec 	getSubnRec();
		public void 	setSubscriptionRec(SubnRec subnRec);
		public void 	setActiveSubview(int index);
		public int 	getActiveSubview();
		public void 	displaySubview();
		public void 	popBackStack();
		public void 	fragmentBack();
		public SrvReq 	getSrvReq();
		public void 		setSrvReq(SrvReq srvReq);
		public SRApptInfo 	getSrApptInfo();
		public void 		setSrApptInfo(SRApptInfo srApptInfo);
		public ApptInfo 	getApptInfo();
		public void 		setApptInfo(ApptInfo apptInfo);
	}

	@Override
	public void onSuccess(APIsResponse response) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onFail(APIsResponse response) {
		// TODO Auto-generated method stub
		
	}

}
