package com.pccw.myhkt.fragment;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Html;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.pccw.myhkt.APIsResponse;
import com.pccw.myhkt.R;
import com.pccw.myhkt.Utils;
import com.pccw.myhkt.lib.ui.AAQuery;
import com.pccw.myhkt.lib.ui.HKTButton;
import com.pccw.myhkt.model.AcctAgent;

/************************************************************************
 File       : IDDNewsFragment.java
 Desc       : View for showing csl/1010/o2f Usage/Roaming
 Name       : IDDNewsFragment
 Created by : Jarlou Valenzuela
 Date       : 07/12/2018

 Change History:
 Date       Modified By			Description
 ---------- ----------------	-------------------------------
 15/03/2016 Jarlou Valenzuela	- First draft
 27/03/2018 In Yong Lee         - Fixed bug for IDD News fragment
 *************************************************************************/

public class IDDNewsFragment extends BaseFragment {
    private AAQuery aq;
    private HKTButton termsAndConditionsBtn;
    private TableLayout iddnews_table;
    private int buttonPadding = 0;
    private boolean isDataLoaded = false;
    private IDDNewsFragment me;

    private OnIDDNewsListener callback;

    public IDDNewsFragment() { }

    public interface OnIDDNewsListener {
        public void goToIDDNewsFragment();
        public void setAcctAgent(AcctAgent acctAgent);
        public AcctAgent getAcctAgent();
    }

    @Override
    protected void initUI() {
        super.initUI();
        if(!isDataLoaded) {
            termsAndConditionsBtn = (HKTButton) aq.id(R.id.iddnews_terms_conditions).getView();
            termsAndConditionsBtn.initViews(getActivity(), getResources().getString(R.string.MYHKT_IDD_NEWS_TNC), ViewGroup.LayoutParams.MATCH_PARENT);
            aq.padding(R.id.iddnews_terms_conditions, basePadding, 0, basePadding, 0);
            aq.marginpx(R.id.iddnews_terms_conditions, basePadding, buttonPadding * 2, basePadding, buttonPadding);
            aq.id(R.id.iddnews_terms_conditions).clicked(this, "onClick");

            iddnews_table = (TableLayout) aq.id(R.id.iddnews_table).getView();
            addTableRow(getTableInfo());
            isDataLoaded = true;
        }
    }

    public static IDDNewsFragment newInstance(String param1, String param2) {
        IDDNewsFragment fragment = new IDDNewsFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    protected void initData() {
        super.initData();
        aq = new AAQuery(getActivity());

        buttonPadding = (int) getResources().getDimension(R.dimen.reg_logo_padding);
    }

    public final void onClick(View view) {
        switch (view.getId()){
            case R.id.iddnews_terms_conditions:
                showTermsAndConditions();
                break;
        }
    }

    public void showTermsAndConditions(){
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        TextView tncTitle = new TextView(getContext());
        tncTitle.setText(getString(R.string.MYHKT_IDD_NEWS_TNC_TITLE));
        tncTitle.setPadding(basePadding,basePadding,basePadding,basePadding);
        tncTitle.setTextSize(18);
        tncTitle.setTextColor(Color.BLACK);
        tncTitle.setGravity(Gravity.CENTER);
        builder.setCustomTitle(tncTitle);
        builder.setMessage(getString(R.string.MYHKT_IDD_NEWS_TNC_CONTENT));

        builder.setPositiveButton(R.string.btn_ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
            }
        });

        AlertDialog alert = builder.create();

        alert.show();

        TextView titleView = (TextView)alert.findViewById(getContext().getResources().getIdentifier("alertTitle", "id", "android"));
        if (titleView != null) {
            titleView.setGravity(Gravity.CENTER);
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initData();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        me = this;
        View fragmentLayout = inflater.inflate(R.layout.fragment_iddnews, container, false);

        aq = new AAQuery(fragmentLayout);
        initData();
        return fragmentLayout;
    }

    @Override
    public void onAttach(@NonNull Activity activity) {
        super.onAttach(activity);
        try {
            callback = (OnIDDNewsListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement OnIDDNewsListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onSuccess(APIsResponse response) throws Exception { }

    @Override
    public void onFail(APIsResponse response) throws Exception { }

    private String[][] getTableInfo() {
        String[][] column1 = new String[2][2];
        column1[0][0] = getString(R.string.MYHKT_IDD_NEWS_c11);
        column1[1][0] = getString(R.string.MYHKT_IDD_NEWS_c21);

        column1[0][1] = getString(R.string.MYHKT_IDD_NEWS_c12);
        column1[1][1] = formatC22();
        return column1;
    }

    private String formatC22(){
        String c22 = String.format(getString(R.string.MYHKT_IDD_NEWS_c22),"<big><font color='red'><b><i>","</b></font>","","</i></big>","<i><small>","</small></i>");
        return c22.replaceAll("\n","<br/>");
    }

    private void addTableRow(String[][] rows){
        for(int i=0;i< rows.length;i++){
            TableRow tr = createRow(rows[i][0],rows[i][1], i%2 == 0 ? R.color.bg_color_grey:R.color.idd_grey);
            iddnews_table.addView(tr, new TableLayout.LayoutParams(TableLayout.LayoutParams.FILL_PARENT, TableLayout.LayoutParams.WRAP_CONTENT));
        }
    }

    private TableRow createRow(String service, String best, int colorId){
        TableRow tr = new TableRow(getContext());
        tr.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.FILL_PARENT, TableRow.LayoutParams.WRAP_CONTENT));
        tr.setBackgroundColor(getResources().getColor(colorId));
        tr.setPadding(basePadding,basePadding,basePadding,basePadding);

        TextView column1 = new TextView(getContext());
        column1.setLayoutParams(new TableRow.LayoutParams(1));
        column1.setText(service);
        column1.setTextColor(getResources().getColor(R.color.black));
        column1.setPadding(0,0,basePadding,0);
        tr.addView(column1);

        TextView column2 = new TextView(getContext());
        column2.setMaxWidth((int)((float) Utils.getScreenWidth(getContext()) * (3.5f/6f)));
        column2.setLayoutParams(new TableRow.LayoutParams(2));
        column2.setTextColor(getResources().getColor(R.color.black));
        column2.setText(Html.fromHtml(best), TextView.BufferType.SPANNABLE);
        column2.setPadding(0,0,0,0);
        column2.setHorizontallyScrolling(false);
        tr.addView(column2);

        return tr;
    }

}
