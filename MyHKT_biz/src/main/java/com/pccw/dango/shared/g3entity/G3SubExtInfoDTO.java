package com.pccw.dango.shared.g3entity;

import java.io.Serializable;

public class G3SubExtInfoDTO implements Serializable 
{
    private static final long serialVersionUID = -4601164089810295731L;
    
    private String errorMsg;
    private String resultCode;
    private String txnSeqNum;
    private String busTxnType;
    private String srvStatus;
    private String statusReaCD;
    
    public String getErrorMsg() {
        return errorMsg;
    }
    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }
    public String getResultCode() {
        return resultCode;
    }
    public void setResultCode(String resultCode) {
        this.resultCode = resultCode;
    }
    public String getTxnSeqNum() {
        return txnSeqNum;
    }
    public void setTxnSeqNum(String txnSeqNum) {
        this.txnSeqNum = txnSeqNum;
    }
    public String getBusTxnType() {
        return busTxnType;
    }
    public void setBusTxnType(String busTxnType) {
        this.busTxnType = busTxnType;
    }
    public String getSrvStatus() {
        return srvStatus;
    }
    public void setSrvStatus(String srvStatus) {
        this.srvStatus = srvStatus;
    }
    public String getStatusReaCD() {
        return statusReaCD;
    }
    public void setStatusReaCD(String statusReaCD) {
        this.statusReaCD = statusReaCD;
    }

}
