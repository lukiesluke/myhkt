package com.pccw.myhkt.service;

/************************************************************************
 File       : LineTestIntentService.java
 Desc       : Background Service For LineTest
 Name       : LineTestIntentService
 Created by : Vincent Fung
 Date       : 12/02/2014

 Change History:
 Date       Modified By			Description
 ---------- ----------------	-------------------------------
 12/02/2014  Vincent Fung		- First draft
 13/02/2014  Vincent Fung		1.) Created IntentService for background lineTest
 2.) Allowed to Normal LineTest and Reset Modem
 3.) Added Notification to foreground or push Notification to mobile device
 24/02/2014 Vincent Fung			- Code cleanup
 *************************************************************************/

import static android.app.ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND;
import static android.app.ActivityManager.RunningAppProcessInfo.IMPORTANCE_VISIBLE;

import android.app.ActivityManager;
import android.app.IntentService;
import android.app.KeyguardManager;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.BitmapDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.os.PowerManager;
import android.util.Log;

import androidx.core.app.NotificationCompat;

import com.google.gson.Gson;
import com.pccw.dango.shared.cra.LnttCra;
import com.pccw.dango.shared.entity.SubnRec;
import com.pccw.dango.shared.tool.RC;
import com.pccw.myhkt.APIsManager;
import com.pccw.myhkt.APIsManager.OnAPIsListener;
import com.pccw.myhkt.APIsResponse;
import com.pccw.myhkt.ClnEnv;
import com.pccw.myhkt.GlobalDialog;
import com.pccw.myhkt.R;
import com.pccw.myhkt.Utils;
import com.pccw.myhkt.activity.BaseActivity;
import com.pccw.myhkt.activity.SplashActivity;
import com.pccw.myhkt.model.LnttAgent;
import com.pccw.myhkt.util.Constant;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

public class LineTestIntentService extends IntentService implements OnAPIsListener {
    private boolean debug = false;

    private static LineTestIntentService me;
    public static int lnttRtryCnt;
    private Timer timer = new Timer();
    private static LnttAgent lnttAgent = null;

    private String errorMsg = "";
    private String TAG = "Lntt";
    public static final String LINE_TEST_CHANNEL_ID = "LINE_TEST_CHANNEL_ID";

    public LineTestIntentService() {
        super("LineTestIntentService");
    }

    @Override
    public void onCreate() {
        // The service is being created
        debug = getResources().getBoolean(R.bool.DEBUG);
        me = this;
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    protected void onHandleIntent(Intent intent) {
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Bundle lnttBundle = intent.getExtras();
        if (lnttBundle.getSerializable("LNTTAGENT") != null) {
            if (debug) Log.i(TAG, "Service start1");
            lnttAgent = (LnttAgent) lnttBundle.getSerializable("LNTTAGENT");
            if ("".equalsIgnoreCase(Utils.getPrefLnttAgent(me).getLnttSrvNum())) {
                if (debug) Log.i(TAG, "Service start2" + lnttAgent.getLtrsRecStatus());
                lnttRtryCnt = 0;
                // Saved Service Number for identifying in ServiceList
                ClnEnv.setPref(me.getApplicationContext(), me.getString(R.string.CONST_PREF_LNTT_AGENT), Utils.serialize(lnttAgent));
                if ("T".equalsIgnoreCase(lnttAgent.getLtrsRecStatus())) {
                    doResetModem();
                } else {
                    doSubmitLnttLoop();
                }
            } else {
                if (debug) Log.i(TAG, "Service start3");
                // Stop Service if the srvNum is not Empty String
                // means Service restarted in unknown case [i.e. user force to kill the apps when line Test is running]
                me.stopSelf();
                if (debug) Log.i(TAG, "Service start3.1");
                Utils.clearLnttService(me);
            }
        } else {
            if (debug) Log.i(TAG, "Service start4");
            me.stopSelf();
            Utils.clearLnttService(me);
        }
        return Service.START_REDELIVER_INTENT;
    }

    public static LnttAgent getLnttAgent() {
        return lnttAgent;
    }

    public final void doLineTestSchedule(com.pccw.dango.shared.cra.LnttCra lnttcra) {
        // clear timer to make sure one task allowed
        timer.cancel();
        timer.purge();
        timer = new Timer();

        try {
            timer.schedule(new TimerTask() {
                @Override
                public void run() {
                    doSubmitLnttLoop();
                }
            }, 30000);
        } catch (Exception e) {
            // fail to sleep
            lnttRtryCnt = 6;
            // Asynctask completed. Cleanup myself
            errorMsg = Utils.getString(me, R.string.LTTM_TIMEOUT);
            lnttAgent.setResultMsg(errorMsg);
            lnttAgent.setLnttCra(lnttcra);

            stopLnttService(errorMsg);
        }
    }

    private final void doResetModemSchedule(LnttCra lnttcra) {
        // clear previous line test reset Record
        // clear timer to make sure one task allowed
        timer.cancel();
        timer.purge();
        timer = new Timer();

        try {
            timer.schedule(new TimerTask() {
                @Override
                public void run() {
                    doResetModem();
                }
            }, 30000);
        } catch (Exception e) {
            // fail to sleep
            lnttRtryCnt = 6;
            // Asynctask completed. Cleanup myself
            errorMsg = Utils.getString(me, R.string.LTTM_TIMEOUT);
            lnttAgent.setResultMsg(errorMsg);
            lnttAgent.setLnttCra(lnttcra);

            stopLnttService(errorMsg);
        }
    }

    private final void doSubmitLnttLoop() {
        if (debug) Log.i(TAG, "doSubmitLnttLoop");
        if (lnttAgent != null) {
            LnttCra lnttCra = lnttAgent.getLnttCra();
            APIsManager.doSubmitLntt(this, lnttCra);

        } else {
            // may be stop at MainMenu
            lnttAgent = new LnttAgent();
            stopLnttService(Utils.getString(me, R.string.MYHKT_LT_ERR_EXPIRED));
        }
    }

    private final void doResetModem() {
        if (debug) Log.i(TAG, "doResetModem");
        if (lnttAgent != null) {
            if (debug) Log.i(TAG, "doResetModem1");
            LnttCra lnttCra = lnttAgent.getLnttCra();
            lnttCra.setILtrsRec(lnttCra.getOLtrsRec());
            lnttCra.setOLtrsRec(null);
            APIsManager.doResetModem(this, lnttCra);
        } else {
            lnttAgent = new LnttAgent();
            stopLnttService(Utils.getString(me, R.string.MYHKT_LT_ERR_EXPIRED));
        }
    }

    @Override
    public void onSuccess(APIsResponse response) {
        LnttCra mLnttCra = null;

        if (debug) Log.i(TAG, "Success");
        if (response != null) {
            if (APIsManager.LNTT.equals(response.getActionTy())) {
                mLnttCra = (LnttCra) response.getCra();
                if (debug) Utils.showLog(TAG, new Gson().toJson(mLnttCra));
                lnttAgent.setLnttCra(mLnttCra.copyMe());
                me.errorMsg = "";
                lnttAgent.setResultMsg(me.errorMsg);
                me.stopLnttService(me.errorMsg);
            } else if (APIsManager.RTRT.equals(response.getActionTy())) {
                mLnttCra = (LnttCra) response.getCra();
                lnttAgent.setLnttCra(mLnttCra.copyMe());
                me.errorMsg = "";
                lnttAgent.setResultMsg(me.errorMsg);
                me.stopLnttService(me.errorMsg);
            }
        }
    }

    @Override
    public void onFail(APIsResponse response) {
        LnttCra mLnttCra = null;
        if (debug) Log.i(TAG, "Fail");
        if (response != null) {
            if (debug) Log.i(TAG, "Fail1" + response.getActionTy() + response.getMessage());
            if (!"".equals(response.getMessage()) && response.getMessage() != null) {
                lnttAgent.setResultMsg(response.getMessage());
                //Pass to MainMenu to show error msg
                ClnEnv.setPref(me.getApplicationContext(), me.getString(R.string.CONST_PREF_LNTT_AGENT), Utils.serialize(lnttAgent));
                me.stopLnttService(response.getMessage());
            } else if (RC.ALT.equalsIgnoreCase(response.getReply().getCode())) {
                me.errorMsg = Utils.getString(me, R.string.LNTT_ALT);
                lnttAgent.setResultMsg(me.errorMsg);

                me.stopLnttService(response.getMessage());
            } else if (RC.TCSESS_MM.equalsIgnoreCase(response.getReply().getCode())) {
                BaseActivity.ivSessDialog();
                Utils.clearLnttService(me.getApplicationContext());
            } else if (APIsManager.LNTT.equals(response.getActionTy())) {
                mLnttCra = (LnttCra) response.getCra();
                lnttAgent.setLnttCra(mLnttCra.copyMe());

                if (mLnttCra.getReply().isBusy() || mLnttCra.getReply().getCode().equals(RC.USRA_LT_PREP)) {
                    ;
                    // RC_USRA_LT_PREP
                    if (lnttRtryCnt < 6 && lnttAgent != null) {
                        lnttRtryCnt++;

                        // Update LtrsRec
                        ClnEnv.setPref(me.getApplicationContext(), me.getString(R.string.CONST_PREF_LNTT_AGENT), Utils.serialize(lnttAgent));
                        // sleep 30 seconds and retry doSubmitLntt();
                        me.doLineTestSchedule(mLnttCra);
                    } else {
                        // Out of 6 times retry
                        me.errorMsg = Utils.getString(me, R.string.LTTM_TIMEOUT);
                        lnttAgent.setResultMsg(me.errorMsg);
                        me.stopLnttService(me.errorMsg);
                    }
                } else {
                    if (mLnttCra.getReply().getCode().equals(RC.GWTGUD_MM)) {
                        // GWTGUD_MM
                        // ALT
                        me.errorMsg = ClnEnv.getRPCErrMsg(me, RC.GWTGUD_MM);
                    } else if (mLnttCra.getReply().getCode().equals(RC.USRA_LT_ERR) || mLnttCra.getReply().getCode().equals(RC.USRA_LT_UXSTS)) {
                        // USRA_LT_ERR
                        // USRA_LT_UXSTS
                        me.errorMsg = Utils.getString(me, R.string.LTTM_ERR);
                    } else {
                        // RC_IVDATA
                        // RC_NOT_AUTH
                        // RC_UXSVLTERR
                        me.errorMsg = Utils.getString(me, R.string.MYHKT_LT_ERR_EXPIRED);
                    }

                    lnttAgent.setResultMsg(me.errorMsg);
                    me.stopLnttService(me.errorMsg);
                }
            } else if (APIsManager.RTRT.equals(response.getActionTy())) {
                if (debug) Log.i(TAG, "Fail2");
                Gson gson = new Gson();
                if (debug) Log.i(TAG, "Fail2" + gson.toJson(response.getCra()));
                mLnttCra = (LnttCra) response.getCra();
                lnttAgent.setLnttCra(mLnttCra.copyMe());
                if (debug) Log.i(TAG, "Fail3" + mLnttCra.getReply().getCode());
                if (mLnttCra.getReply().isBusy() || mLnttCra.getReply().getCode().equals(RC.USRA_LT_PREP)) {

                    // RC_USRA_LT_PREP
                    if (lnttRtryCnt < 6 && lnttAgent != null) {
                        lnttRtryCnt++;

                        // Update LtrsRec
                        ClnEnv.setPref(me.getApplicationContext(), me.getString(R.string.CONST_PREF_LNTT_AGENT), Utils.serialize(lnttAgent));
                        // sleep 30 seconds and retry doResetModem();
                        me.doResetModemSchedule(mLnttCra);
                    } else {
                        // Out of 6 times retry
                        me.errorMsg = Utils.getString(me, R.string.LTTM_TIMEOUT);
                        lnttAgent.setResultMsg(me.errorMsg);
                        me.stopLnttService(me.errorMsg);
                    }
                } else {
                    if (mLnttCra.getReply().getCode().equals(RC.GWTGUD_MM)) {
                        // GWTGUD_MM
                        // ALT
                        me.errorMsg = ClnEnv.getRPCErrMsg(me, RC.GWTGUD_MM);
                    } else if (mLnttCra.getReply().getCode().equals(RC.USRA_LT_ERR) || mLnttCra.getReply().getCode().equals(RC.USRA_LT_UXSTS)) {
                        // USRA_LT_ERR
                        // USRA_LT_UXSTS
                        me.errorMsg = Utils.getString(me, R.string.LTTM_ERR);
                    } else {
                        // RC_IVDATA
                        // RC_NOT_AUTH
                        // RC_UXSVLTERR
                        me.errorMsg = Utils.getString(me, R.string.MYHKT_LT_ERR_EXPIRED);
                    }

                    lnttAgent.setResultMsg(me.errorMsg);
                    me.stopLnttService(me.errorMsg);
                }
            } else {
                if (!"".equals(response.getMessage()) && response.getMessage() != null) {
                    lnttAgent.setResultMsg(response.getMessage());
                    me.stopLnttService(response.getMessage());
                } else if (RC.TCSESS_MM.equalsIgnoreCase(response.getReply().getCode())) {
                    BaseActivity.ivSessDialog();
                } else {
                    lnttAgent.setResultMsg(me.errorMsg);
                    me.stopLnttService(me.errorMsg);
                }
            }
        }
    }

    @Override
    public void onLowMemory() {
    }

    @Override
    public void onDestroy() {
        // clear all Timer schedule to avoid restart line test
        timer.cancel();
        timer.purge();
        timer = new Timer();
    }

    //
    private final void stopLnttService(String message) {
        // Only show LineTest Complete Message
        generateNotification(me.getApplicationContext(), Utils.getString(me, R.string.MYHKT_LT_MSG_COMPLETED));
        me.stopSelf();
    }

    // Copied Code from MyAccount Notification Alert
    private void generateNotification(Context context, String message) {
        if (debug) Log.i(TAG, "generateNotification");
        long when = System.currentTimeMillis();
        SimpleDateFormat dateFormat = new SimpleDateFormat(Utils.getString(me, R.string.input_datetime_format));
        // Save all data and EndTimestamp before stopService
        lnttAgent.setEndTimestamp(dateFormat.format(new Date(when)));

        ClnEnv.setPref(context, context.getString(R.string.CONST_PREF_LNTT_AGENT), Utils.serialize(lnttAgent));
        String title = Utils.getString(me.getApplicationContext(), R.string.app_name);
        if (lnttAgent.getResultMsg() == null) {

            // SubscriptionRec Validation
            boolean isValidSubnRec = false;
            if (ClnEnv.isLoggedIn()) {
                for (SubnRec rSubnRec : ClnEnv.getAssocSubnRecAry(ClnEnv.getQualSvee().getSubnRecAry())) {
                    if (rSubnRec.srvNum.equalsIgnoreCase(lnttAgent.getLnttSrvNum()) && rSubnRec.acctNum.equalsIgnoreCase(lnttAgent.getLnttCra().getISubnRec().acctNum)) {
                        isValidSubnRec = true;
                    }
                }
            }

            if (!isValidSubnRec) {
                Utils.clearLnttService(me);
                return;
            }
            // Validation End
        }
        Intent notificationIntent = null;
        Log.d("lwg", "Line test:: foreground: " + foregrounded());
        if (!isAppActive(context) || !foregrounded()) {
            ClnEnv.setPref(context, context.getString(R.string.CONST_PREF_LNTT_NOTICEFLAG), true);
            // MyHKT is not running in foreground
            notificationIntent = new Intent(context, SplashActivity.class);
            // This Boolean is used for turning on redirection from Splash > MainMenu > ServiceList > TargetService
            notificationIntent.putExtra("LTRS_READNOW", true);

        } else {
            Log.d("lwg", "Line test:: foreground.");

            Intent intent = new Intent();
            intent.setAction("LineTest");
            // Don't send broadcast to Service activity of Reset Modem
            if (!"T".equalsIgnoreCase(lnttAgent.getLtrsRecStatus())) {
                intent.setAction(Constant.ACTION_LINE_TEST_SUCCESS);
                intent.setPackage(getPackageName());
                sendBroadcast(intent);
                me.redirectDialog(context, R.string.CONST_DIALOG_LNTT_RESULT);

                Log.d("lwg", "Line test: Don't send broadcast to Service activity of Reset Modem");
            }
        }

        // Only notification when the App is inactive
        if (notificationIntent != null) {

            PendingIntent pendingIntent;
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.S) {
                pendingIntent = PendingIntent.getActivity(context, 0, notificationIntent, PendingIntent.FLAG_IMMUTABLE | PendingIntent.FLAG_UPDATE_CURRENT);
            } else {
                pendingIntent = PendingIntent.getActivity(context, 0, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);
            }

            NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context, LINE_TEST_CHANNEL_ID);
            mBuilder.setSmallIcon(R.drawable.icon_notification);
            mBuilder.setLargeIcon(((BitmapDrawable) context.getResources().getDrawable(R.drawable.hktappicon)).getBitmap());
            mBuilder.setWhen(when);
            mBuilder.setContentTitle(title);
            mBuilder.setTicker(message);
            mBuilder.setContentText(message);
            mBuilder.setAutoCancel(true);
            mBuilder.setDefaults(Notification.DEFAULT_ALL);
            mBuilder.setContentIntent(pendingIntent);

            Notification notification = new NotificationCompat.BigTextStyle(mBuilder).bigText(message).build();

            NotificationManager mNotificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                int importance = NotificationManager.IMPORTANCE_HIGH;
                NotificationChannel mChannel = new NotificationChannel(LINE_TEST_CHANNEL_ID, "NOTIFICATION", importance);
                mChannel.setShowBadge(false); // Set to TRUE to display badge icon
                if (mNotificationManager != null) {
                    mNotificationManager.createNotificationChannel(mChannel);
                }
            }

            if (mNotificationManager != null) {
                mNotificationManager.notify(context.getResources().getInteger(R.integer.CONST_NOTIFICATION_ID_LNTT), notification);
            }
        }
    }

    public boolean foregrounded() {
        ActivityManager.RunningAppProcessInfo appProcessInfo = new ActivityManager.RunningAppProcessInfo();
        ActivityManager.getMyMemoryState(appProcessInfo);
        return (appProcessInfo.importance == IMPORTANCE_FOREGROUND || appProcessInfo.importance == IMPORTANCE_VISIBLE);
    }

    // redirect to LineTest Target Activity
    protected final void redirectDialog(Context context, int dialogType) {
        Intent dialogIntent = new Intent(context, GlobalDialog.class);
        dialogIntent.putExtra("DIALOGTYPE", dialogType);
        dialogIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        context.startActivity(dialogIntent);
    }

    private boolean isDeviceLocked(Context context) {
        boolean isLocked = false;

        KeyguardManager myKM = (KeyguardManager) context.getSystemService(Context.KEYGUARD_SERVICE);
        boolean isPhoneLocked = false;
        if (myKM != null) {
            isPhoneLocked = (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN && myKM.isKeyguardLocked()) ||
                    (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN && myKM.inKeyguardRestrictedInputMode());
        }

        if (isPhoneLocked) {
            isLocked = true;
        }

        //Check if the device is at sleep mode, then wake up
        PowerManager powerManager = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
        if (powerManager != null) {
            boolean isScreenAwake = (Build.VERSION.SDK_INT < 20 ? powerManager.isScreenOn() : powerManager.isInteractive());
            isLocked = !isScreenAwake; //sleep mode

            if (!isScreenAwake) {
                PowerManager.WakeLock wl = powerManager.newWakeLock(PowerManager.FULL_WAKE_LOCK | PowerManager.ACQUIRE_CAUSES_WAKEUP | PowerManager.ON_AFTER_RELEASE, "AppName:MyLock");
                wl.acquire(10000);
                PowerManager.WakeLock wl_cpu = powerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "AppName:MyCpuLock");

                wl_cpu.acquire(10000);
            }
        }
        return isLocked;
    }

    private boolean isAppActive(Context context) {
        ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningTaskInfo> tasks = null;
        if (am != null) {
            tasks = am.getRunningTasks(1);
        }

        if (tasks != null && !tasks.isEmpty()) {
            ComponentName topActivity = tasks.get(0).topActivity;
            return topActivity.getPackageName().equals(context.getPackageName());
        }
        return false;
    }
}