package com.pccw.myhkt.fragment;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentTransaction;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.MapsInitializer;
import com.pccw.dango.shared.cra.ShopCra;
import com.pccw.dango.shared.entity.ShopRec;
import com.pccw.dango.shared.tool.RC;
import com.pccw.myhkt.APIsManager;
import com.pccw.myhkt.APIsResponse;
import com.pccw.myhkt.ClnEnv;
import com.pccw.myhkt.DialogHelper;
import com.pccw.myhkt.FAWrapper;
import com.pccw.myhkt.R;
import com.pccw.myhkt.activity.BaseActivity;
import com.pccw.myhkt.lib.ui.AAQuery;
import com.pccw.myhkt.mapviewballoons.MapFragmentD;
import com.pccw.myhkt.util.Constant;
import com.pccw.myhkt.util.RuntimePermissionUtil;

import java.util.Objects;

public class ShopMyLocFragment extends BaseShopFragment {
    private ShopMyLocFragment me;
    private AAQuery aq;
    private MapFragmentD mFRaFragment = null;
    private ShopRec[] shopRecAry = null;

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        me = this;
        TAG = "ShopMyLocFragment";
        return inflater.inflate(R.layout.fragment_shopmyloc, container, false);
    }

    @Override
    public void onStart() {
        super.onStart();
        FAWrapper.getInstance().sendFAScreen(getActivity(), R.string.CONST_SCRN_SHOPMYLOC, false);
        init();
        IntentFilter filter = new IntentFilter();
        filter.addAction(Constant.ACTION_LOCATION_REQUEST_GRANTED);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
            requireActivity().registerReceiver(broadcastReceiver, filter, Context.RECEIVER_NOT_EXPORTED);
        } else {
            requireActivity().registerReceiver(broadcastReceiver, filter);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        try {
            if (broadcastReceiver != null) {
                requireActivity().unregisterReceiver(broadcastReceiver);
            }
        } catch (Exception e) {
            broadcastReceiver = null;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        int googlePlayServiceState = GooglePlayServicesUtil.isGooglePlayServicesAvailable(requireActivity().getApplicationContext());
        if (googlePlayServiceState == ConnectionResult.SUCCESS) {
            try {
                // Initialize Map
                if (mFRaFragment == null) {
                    LinearLayout shopMyLocMapView = getActivity().findViewById(R.id.shopmyloc_mapview);
                    shopMyLocMapView.removeAllViews();

                    FragmentTransaction mTransaction = getChildFragmentManager().beginTransaction();
                    mFRaFragment = new MapFragmentD();

                    Bundle args = new Bundle();
                    args.putBoolean("myLoc", true);
                    args.putBoolean("isZh", isZh);
                    mFRaFragment.setArguments(args);

                    mTransaction.add(R.id.shopmyloc_mapview, mFRaFragment);
                    mTransaction.commit();
                }
            } catch (Exception e) {
                // Error adding MapView, we cannot proceed
                e.printStackTrace();
                callback_main.popBackStack();
            }

            try {
                MapsInitializer.initialize(getActivity());
                if (shopRecAry != null) {
                    initPOIs();
                } else {
                    fetchShopList();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            // google play service not available, displaying suitable error message
            getActivity().findViewById(R.id.shopmyloc_button_mylocation).setVisibility(View.GONE);

            // ConnectionResult.SERVICE_MISSING,
            // ConnectionResult.SERVICE_VERSION_UPDATE_REQUIRED,
            // ConnectionResult.SERVICE_DISABLED,
            // ConnectionResult.SERVICE_INVALID
            GooglePlayServicesUtil.getErrorDialog(googlePlayServiceState, getActivity(), -1).show();
        }
    }

    private void init() {
        aq = new AAQuery(getActivity());
        aq.id(R.id.shopmyloc_button_mylocation).clicked(this, "onClick");
    }

    public void onClick(View v) {
        if (v.getId() == R.id.shopmyloc_button_mylocation) {
            showUserCurrentLocation();
        }
    }

    // Passing shop list info to the map for Pin generation
    private final void initPOIs() {
        if (mFRaFragment != null) {
            mFRaFragment.fetchShopList(shopRecAry);
        }
    }

    // Fetching Shop List from CSP API
    private final void fetchShopList() {
//		shopRecAry = callback.getShopRecAry();
        doGetShopByGC();
    }

    @Override
    public void onSuccess(APIsResponse response) {
        ShopCra shopcra = (ShopCra) response.getCra();
        shopRecAry = shopcra.getOShopRecAry();
        initPOIs();
    }

    @Override
    public void onFail(APIsResponse response) {
        // General Error Message
        if (!"".equals(response.getMessage()) && response.getMessage() != null) {
            DialogHelper.createSimpleDialog(getActivity(), response.getMessage());
        } else if (RC.TCSESS_MM.equalsIgnoreCase(response.getReply().getCode())) {
            BaseActivity.ivSessDialog();
        } else {
            DialogHelper.createSimpleDialog(getActivity(), ClnEnv.getRPCErrMsg(getActivity(), response.getReply().getCode()));
        }
    }

    private void doGetShopByGC() {
        ShopCra shopCra = new ShopCra();
        shopCra.setILatitude(0.0);
        shopCra.setILongitude(0.0);
        APIsManager.doGetShopByGC(me, shopCra);
    }

    private void showUserCurrentLocation() {
        if (RuntimePermissionUtil.isLocationPermissionGranted(getActivity())) {
            showMyLocationInMap();
        } else {
            if (ClnEnv.getPref(getActivity(), Constant.LOCATION_PERMISSION_DENIED, false)) {
                displayDialog(getString(R.string.permission_denied_setting_enabled));
            } else {
                RuntimePermissionUtil.requestLocationPermission(getActivity(), Constant.REQUEST_LOCATION_PERMISSION);
            }
        }
    }

    private void showMyLocationInMap() {
        try {
            if (mFRaFragment != null) {
                mFRaFragment.movetomyloc();
            }
            if (shopRecAry == null) {
                me.fetchShopList();
            }
        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(requireActivity().getApplicationContext(), getResources().getString(R.string.myhkt_shop_mylocerr), Toast.LENGTH_SHORT).show();
        }
    }

    BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (Constant.ACTION_LOCATION_REQUEST_GRANTED.equals(intent.getAction())) {
                showMyLocationInMap();
            }
        }
    };
}
