package com.pccw.myhkt.utils;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import androidx.annotation.RequiresApi;
import androidx.core.app.ActivityCompat;

/************************************************************************
 * File : RuntimePermissionUtil.java
 * Desc : Runtime Permission Utility class
 * Name : RuntimePermissionUtil
 * by 	: Abdulmoiz Esmail
 * Date : 04/1/2018
 * 
 * Change History:
 * Date       Modified By		Description
 * ---------- ----------------	-------------------------------
 * 04/1/2018  Abdulmoiz Esmail	- Created class for runtime permission checking and request	
 * 								- File Storage Permission
 * 								- Call Phone Permission
 * 								- Location Permission
 *************************************************************************/
public class RuntimePermissionUtil {
	
	//File Storage Permission
	public static boolean isWriteExternalStoragePermissionGranted(Activity activity) {

		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            int permissionCheck = activity.checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE);
            return permissionCheck == PackageManager.PERMISSION_GRANTED;
        }
        return true;
		 
	       
	}

    @RequiresApi(api = Build.VERSION_CODES.M)
	public static Boolean requestFileStoragePermission(Activity activity) {
        if(!RuntimePermissionUtil.isWriteExternalStoragePermissionGranted(activity)) {

            activity.requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    Constant.REQUEST_FILE_STORAGE_PERMISSION);

        } else {
            return true;
        }

        return false;
    }

	@RequiresApi(api = Build.VERSION_CODES.M)
    public static Boolean requestFileStoragePermissionForLogs(Activity activity) {
        if(!RuntimePermissionUtil.isWriteExternalStoragePermissionGranted(activity)) {

            activity.requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    Constant.REQUEST_LOG_FILE_STORAGE_PERMISSION);

        } else {
            return true;
        }

        return false;
    }

	@RequiresApi(api = Build.VERSION_CODES.M)
    public static boolean shouldShowRequestPermission(Activity activity) {
		return activity.shouldShowRequestPermissionRationale(Manifest.permission.WRITE_EXTERNAL_STORAGE);
	}
	
	
	//Action Call Permission
	public static boolean isActionCallPermissionGranted(Activity activity) {
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            int permissionCheck = activity.checkSelfPermission(Manifest.permission.CALL_PHONE);
            return permissionCheck == PackageManager.PERMISSION_GRANTED;
        }
        return true;
	}
	
	@RequiresApi(api = Build.VERSION_CODES.M)
    public static Boolean requestCallPhonePermission(Activity activity) {
        if(!RuntimePermissionUtil.isActionCallPermissionGranted(activity)) {

            activity.requestPermissions(new String[]{Manifest.permission.CALL_PHONE},
                    Constant.REQUEST_CALL_PHONE_PERMISSION);
        
        } else {
            return true;
        }

        return false;
    }
	
	@RequiresApi(api = Build.VERSION_CODES.M)
    public static boolean shouldShowCallPhoneRequestPermission(Activity activity) {
		return activity.shouldShowRequestPermissionRationale(Manifest.permission.CALL_PHONE);
	}

	//ACCESS_COARSE_LOCATION
	public static boolean isLocationPermissionGranted(Activity activity) {
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            return ActivityCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED ||
                    ActivityCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED;
        }
        return true;
	}
	
	public static Boolean requestLocationPermission(Activity activity, int requestId) {
        if(!RuntimePermissionUtil.isLocationPermissionGranted(activity)) {

            String[] PERMISSIONS = {Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION};

            if(!hasPermissions(activity.getApplicationContext(), PERMISSIONS)){
                ActivityCompat.requestPermissions(activity, PERMISSIONS, requestId);
            }

           
        } else {
            return true;
        }
        return false;
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
	public static boolean shouldShowLocationRequestPermission(Activity activity) {
		return activity.shouldShowRequestPermissionRationale(Manifest.permission.ACCESS_COARSE_LOCATION);
	}

    public static boolean hasPermissions(Context context, String... permissions) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }
}
