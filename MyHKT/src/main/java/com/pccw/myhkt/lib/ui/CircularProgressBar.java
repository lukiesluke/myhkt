package com.pccw.myhkt.lib.ui;

import com.pccw.myhkt.R;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Path.Direction;
import android.graphics.PorterDuff.Mode;
import android.graphics.PorterDuffXfermode;
import android.graphics.RadialGradient;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

public class CircularProgressBar extends View{

	private int viewHeight = 0;
	private int viewWidth = 0;
	private int outerRadius = 0;
	private int circularWidth = 0;
	private int shadowDelta = 0;
	private int downShadowWidth = 0;
	private int strokeWidth = 20;
	
	private int bgColor = R.color.cprogressbar_bg;
	private int progressColor = R.color.cprogressbar_blue;
	private float precentage ;

	private Paint bgPaint = new Paint();
	private Paint progressPaint = new Paint();
	private Paint shadowP = new Paint();
	private Paint clearPaint = new Paint();
	private Paint outerClearPaint = new Paint();
	private Paint downShadowP = new Paint();
	private Paint paint;
	
	private RectF arcRectF;
	private RectF shadowRectF;


	public CircularProgressBar(Context context)  {  
		super(context);  
		setBackgroundColor(Color.TRANSPARENT);
		clearPaint.setColor(Color.TRANSPARENT); 
		setLayerType(View.LAYER_TYPE_SOFTWARE, null);
	} 

	public CircularProgressBar(Context context, AttributeSet attrs) {
		super(context, attrs);
		setBackgroundColor(Color.TRANSPARENT);
		clearPaint.setColor(Color.TRANSPARENT); 
		setLayerType(View.LAYER_TYPE_SOFTWARE, null);
	}

	public CircularProgressBar(Context context, AttributeSet attrs,	int defStyleAttr) {
		super(context, attrs, defStyleAttr);
		setBackgroundColor(Color.TRANSPARENT);
		clearPaint.setColor(Color.TRANSPARENT); 
		setLayerType(View.LAYER_TYPE_SOFTWARE, null);
		// TODO Auto-generated constructor stub
	}
	
	public void setProgress(float precentage) {
		this.precentage = precentage;
		this.invalidate();
	}
	
	public void setProgressColor(int resId) {
		progressColor = resId;
		progressPaint = new Paint();
		progressPaint.setColor(this.getContext().getResources().getColor(progressColor));
//		progressPaint.setAlpha(255);
		progressPaint.setAntiAlias(true);
		this.invalidate();		
	}

	private void setupPaints() {
		int w = viewWidth/2;

		bgPaint = new Paint();
		bgPaint.setColor(this.getContext().getResources().getColor(bgColor));		
		bgPaint.setAntiAlias(true);

		progressPaint = new Paint();
		progressPaint.setColor(this.getContext().getResources().getColor(progressColor));
//		progressPaint.setAlpha(255);
		progressPaint.setAntiAlias(true);

		clearPaint.setColor(Color.TRANSPARENT); 
		clearPaint.setXfermode(new PorterDuffXfermode(Mode.CLEAR));
		clearPaint.setAntiAlias(true);
		
		outerClearPaint.setColor(Color.TRANSPARENT); 
		outerClearPaint.setXfermode(new PorterDuffXfermode(Mode.SRC_ATOP));
		outerClearPaint.setAntiAlias(true);
		
		downShadowP.setColor(Color.WHITE); 
		downShadowP.setAntiAlias(true);
		downShadowP.setShadowLayer(shadowDelta, 0, shadowDelta+5, Color.parseColor("#88000000"));
		
		paint = new Paint();
		paint.setXfermode(new PorterDuffXfermode(Mode.SRC_ATOP));
		paint.setColor(Color.BLACK); 
		paint.setStyle(Paint.Style.STROKE);
		paint.setStrokeWidth(strokeWidth);
		paint.setAntiAlias(true);
		paint.setShadowLayer(shadowDelta, 0, shadowDelta+5, Color.parseColor("#88000000"));
	}


	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
	    int parentWidth = MeasureSpec.getSize(widthMeasureSpec);
	    int parentHeight = MeasureSpec.getSize(heightMeasureSpec);
//	    Log.i("Circular", parentWidth + "/" + parentHeight );
	    this.setMeasuredDimension(parentWidth, parentWidth);
	    super.onMeasure(widthMeasureSpec, widthMeasureSpec);
//	    this.setMeasuredDimension(parentWidth, parentHeight);
//	    super.onMeasure(widthMeasureSpec, heightMeasureSpec);
	}
	
	@Override
	protected void onSizeChanged(int newWidth, int newHeight, int oldWidth, int oldHeight) {
		super.onSizeChanged(newWidth, newHeight, oldWidth, oldHeight);
		viewWidth = Math.max(newWidth, newHeight);
		viewHeight = Math.max(newWidth, newHeight);
		
		outerRadius = viewWidth/2;
		circularWidth = outerRadius/4;
		shadowDelta = circularWidth /7;
		downShadowWidth = (int)(outerRadius - circularWidth + shadowDelta * 0f);
		arcRectF = new RectF(0,0,viewWidth , viewWidth);		
		shadowRectF = new RectF(-strokeWidth/2,-strokeWidth/2,viewWidth +strokeWidth/2, viewWidth +strokeWidth/2) ;	
		//        setupBounds();
		setupPaints();
		invalidate();
	}	 
	 
	@Override  
	protected void onDraw(Canvas canvas)  { 		
		super.onDraw(canvas);		
		canvas.drawCircle( viewWidth/2,  viewWidth/2, outerRadius, bgPaint); 
		canvas.drawArc(arcRectF, - 90, -(int)(360 * precentage /100), true, progressPaint);	
		canvas.drawArc(shadowRectF, 0, 360, false, paint);
//		canvas.drawCircle( viewWidth/2,  viewWidth/2 + shadowDelta, downShadowWidth , shadowP); 		
		canvas.drawCircle( viewWidth/2,  viewWidth/2, outerRadius - circularWidth, downShadowP);
		canvas.drawCircle( viewWidth/2,  viewWidth/2, outerRadius - circularWidth, clearPaint);


	}  
} 	

