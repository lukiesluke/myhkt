package com.pccw.myhkt.activity;

import android.content.Context;
import android.content.SharedPreferences;
import androidx.test.platform.app.InstrumentationRegistry;
import androidx.test.rule.ActivityTestRule;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import com.pccw.myhkt.AccountHelper;
import com.pccw.myhkt.R;
import com.pccw.myhkt.base.BaseTest;
import com.pccw.myhkt.model.MyAccount;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static androidx.test.platform.app.InstrumentationRegistry.getInstrumentation;
import static androidx.test.espresso.Espresso.onData;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.scrollTo;
import static androidx.test.espresso.action.ViewActions.swipeLeft;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withText;
import static com.pccw.myhkt.helpers.EspressoTestsMatchers.withDrawable;
import static org.hamcrest.Matchers.anything;

@Ignore
@RunWith(AndroidJUnit4.class)
public class MyAccountListActivityTest extends BaseTest {

    private Context context;

    private SharedPreferences shared;
    SharedPreferences.Editor prefEditor;


    @Rule
    public ActivityTestRule<MainMenuActivity> activityTestRule =
            new ActivityTestRule<MainMenuActivity>(MainMenuActivity.class) {
                @Override
                protected void beforeActivityLaunched() {
                    clearSharedPrefs(InstrumentationRegistry.getTargetContext());
                    InstrumentationRegistry.getTargetContext().deleteDatabase("myhkt.db");
                    super.beforeActivityLaunched();
                }
            };

    private void clearSharedPrefs(Context context) {
        SharedPreferences prefs =
                context.getSharedPreferences("myhkt", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.clear();
        editor.commit();
    }

    @Before
    public void setUp() {

        context = getInstrumentation().getTargetContext();
        shared = context.getSharedPreferences(
                "myhkt",
                Context.MODE_PRIVATE);
//        Intents.init();
        clickLeftNavBar();
        findStringContaining(context, R.string.myhkt_LGIF_LOGIN).perform(click());
    }

    @Test
    public void testMyAccountsUIDisplay() {
        loginNetvigatorFixedLineOrNow();

        findComponentByID(R.id.servicelist_btn_alias);
        findComponentByID(R.id.servicelist_listView);
    }

    @Test
    public void testLTSDisplay1() {
        delay(2000);

        loginNetvigatorFixedLineOrNow();

        targetAccount(0, scrollTo(), R.id.servicelist_listView);

        MyAccount myAccount = new MyAccount();
        myAccount.setAccountId("34104068  ");
        myAccount.setAccountIcon(R.drawable.lob_lts_plain);
        myAccount.setAccountLabel("eye service");
        myAccount.setNickname("");
        myAccount.setNewIconDisplayed(true);

        testAccountInRecyclerview(myAccount, 0);

    }

    @Test
    public void testLTSDisplay2() {
        loginNetvigatorFixedLineOrNow();

        targetAccount(2, scrollTo(),R.id.servicelist_listView);

        MyAccount myAccount = new MyAccount();
        myAccount.setAccountId("34104222  ");
        myAccount.setAccountIcon(R.drawable.lob_lts_plain);
        myAccount.setAccountLabel("eye service");
        myAccount.setNickname("Lmkkkj");
        myAccount.setNewIconDisplayed(true);

        testAccountInRecyclerview(myAccount, 2);

    }

    @Test
    public void testLTSDisplay3() {
        loginNetvigatorFixedLineOrNow();

        targetAccount(4, scrollTo(), R.id.servicelist_listView);
        MyAccount myAccount = new MyAccount();
        myAccount.setAccountId("36670837  ");
        myAccount.setAccountIcon(R.drawable.lob_lts_plain);
        myAccount.setAccountLabel("eye service");
        myAccount.setNickname("");
        myAccount.setNewIconDisplayed(false);

        testAccountInRecyclerview(myAccount, 4);
    }


    /**
     * No available account with this LOB
     */
    @Ignore
    @Test
    public void test1010Display1() {
        targetAccount(5, scrollTo(),R.id.servicelist_listView);
        MyAccount myAccount = new MyAccount();
        myAccount.setAccountId("93065411");
        myAccount.setAccountIcon(R.drawable.lob_1010_plain);
        myAccount.setAccountLabel("");
        myAccount.setNickname("");
        myAccount.setNewIconDisplayed(true);

        testAccountInRecyclerview(myAccount, 5);

        onData(anything()).inAdapterView(withId(R.id.servicelist_listView))
                .atPosition(5).perform(click());

        delay(5000);

        clickLeftNavBar();

        delay(3000);

        onData(anything()).inAdapterView(withId(R.id.servicelist_listView))
                .atPosition(5)
                .onChildView(withId(R.id.adapter_servicelist_star))
                .check(matches(BaseTest.checkIfGone()));

    }

    @Test
    public void testNetvigatorDisplay1() {
        loginNetvigatorFixedLineOrNow();

        targetAccount(6, scrollTo(),R.id.servicelist_listView);

        MyAccount myAccount = new MyAccount();
        myAccount.setAccountId("a60058422");
        myAccount.setAccountIcon(R.drawable.lob_pcd_plain);
        myAccount.setAccountLabel("");
        myAccount.setNickname("");
        myAccount.setNewIconDisplayed(false);

        testAccountInRecyclerview(myAccount, 6);

    }

    @Test
    public void testNetvigatorDisplay2() {
        loginNetvigatorFixedLineOrNow();

        targetAccount(8, scrollTo(),R.id.servicelist_listView);

        MyAccount myAccount = new MyAccount();
        myAccount.setAccountId("a60058886");
        myAccount.setAccountIcon(R.drawable.lob_pcd_plain);
        myAccount.setAccountLabel("");
        myAccount.setNickname("");
        myAccount.setNewIconDisplayed(true);

        testAccountInRecyclerview(myAccount, 8);

        onData(anything()).inAdapterView(withId(R.id.servicelist_listView))
                .atPosition(8).perform(click());

        delay(5000);

        clickLeftNavBar();

        delay(3000);

        onData(anything()).inAdapterView(withId(R.id.servicelist_listView))
                .atPosition(8)
                .onChildView(withId(R.id.adapter_servicelist_star))
                .check(matches(BaseTest.checkIfGone()));

    }

    @Test
    public void testNowDisplay1() {
        loginNetvigatorFixedLineOrNow();

        targetAccount(17, scrollTo(),R.id.servicelist_listView);

        MyAccount myAccount = new MyAccount();
        myAccount.setAccountId("1068255540");
        myAccount.setAccountIcon(R.drawable.lob_tv_plain);
        myAccount.setAccountLabel("");
        myAccount.setNickname("");
        myAccount.setNewIconDisplayed(true);

        testAccountInRecyclerview(myAccount, 17);

        onData(anything()).inAdapterView(withId(R.id.servicelist_listView))
                .atPosition(17).perform(click());

        delay(5000);

        clickLeftNavBar();

        delay(3000);

        onData(anything()).inAdapterView(withId(R.id.servicelist_listView))
                .atPosition(17)
                .onChildView(withId(R.id.adapter_servicelist_star))
                .check(matches(BaseTest.checkIfGone()));
    }

    @Test
    public void testNowDisplay2() {
        loginNetvigatorFixedLineOrNow();

        targetAccount(13, scrollTo(), R.id.servicelist_listView);

        MyAccount myAccount = new MyAccount();
        myAccount.setAccountId("1068249781");
        myAccount.setAccountIcon(R.drawable.lob_tv_plain);
        myAccount.setAccountLabel("");
        myAccount.setNickname("Test");
        myAccount.setNewIconDisplayed(false);

        testAccountInRecyclerview(myAccount, 13);

    }

    @Test
    public void testCLSDisplay1() {
        loginCSL();

        targetAccount(0, scrollTo(),R.id.servicelist_listView);

        MyAccount myAccount = new MyAccount();
        myAccount.setAccountId("54730532");
        myAccount.setAccountIcon(R.drawable.lob_csl_plain);
        myAccount.setAccountLabel("");
        myAccount.setNickname("");
        myAccount.setNewIconDisplayed(false);

        testAccountInRecyclerview(myAccount, 0);
    }

    @Test
    public void testSetDisplayNameButtonBehavior() {
        loginNetvigatorFixedLineOrNow();

        findComponentByID(R.id.servicelist_btn_alias).perform(click());
    }

    @Test
    public void testDisplayNameButtonVisible() {
        loginNetvigatorFixedLineOrNow();

        targetAccount(6, swipeLeft(),R.id.servicelist_listView);
        findStringContaining(context, R.string.MYMOB_DISPLAY_NAME);
    }

    @Test
    public void testChangeDisplayNameDialogIsVisible() {
        loginNetvigatorFixedLineOrNow();

        targetAccount(0, swipeLeft(),R.id.servicelist_listView);
        delay(TestTimeouts.DIALOG_TIMEOUTS);
        onData(anything()).inAdapterView(withId(R.id.servicelist_listView))
                .atPosition(0)
                .onChildView(withId(R.id.adapter_servicelist_edit))
                .perform(click());
//        clickViewChildInAdapter(R.id.servicelist_listView, 0, R.id.adapter_servicelist_edit);

        checkStringIsDisplayed(context,R.string.MYMOB_PLZ_INPUT_ALIAS);
    }

    @Test
    public void testContentOfChangeDisplayNameDialog() {
        loginNetvigatorFixedLineOrNow();

        targetAccount(6, swipeLeft(),R.id.servicelist_listView);
        delay(TestTimeouts.DIALOG_TIMEOUTS);
        onData(anything()).inAdapterView(withId(R.id.servicelist_listView))
                .atPosition(6)
                .onChildView(withId(R.id.adapter_servicelist_edit))
                .perform(click());
        findStringContaining(context, R.string.MYMOB_PLZ_INPUT_ALIAS);
        clickCANCEL();
    }

    @Test
    public void testChangeDisplayNameResultOK() {
        loginNetvigatorFixedLineOrNow();

        changeDisplayNameBaseTest(context, 6, "", R.id.servicelist_listView,
                R.string.MYMOB_PLZ_INPUT_ALIAS, R.id.adapter_servicelist_edit,
                R.id.adapter_servicelist_header_alias, waitForText("Your list of service(s) has been updated."));

    }

    @Test
    public void testChangeDisplayNameResultCancel() {
        loginNetvigatorFixedLineOrNow();

        targetAccount(0, swipeLeft(),R.id.servicelist_listView);
        delay(TestTimeouts.DIALOG_TIMEOUTS);
        onData(anything()).inAdapterView(withId(R.id.servicelist_listView))
                .atPosition(0)
                .onChildView(withId(R.id.adapter_servicelist_edit))
                .perform(click());
        findStringContaining(context, R.string.MYMOB_PLZ_INPUT_ALIAS);
        findStringContaining(context, R.string.btn_cancel)
                .perform(click());
    }

    @After
    public void tearDown() {
        executeLogout(context);
//        Intents.release();
    }

    public void testAccountInRecyclerview(MyAccount myAccount, int position) {
        onData(anything()).inAdapterView(withId(R.id.servicelist_listView))
                .atPosition(position)
                .onChildView(withId(R.id.adapter_servicelist_header))
                .check(matches(withText(myAccount.getAccountId())));

        onData(anything()).inAdapterView(withId(R.id.servicelist_listView))
                .atPosition(position)
                .onChildView(withId(R.id.adapter_servicelist_logo))
                .check(matches(withDrawable(myAccount.getAccountIcon())));

        onData(anything()).inAdapterView(withId(R.id.servicelist_listView))
                .atPosition(position)
                .onChildView(withId(R.id.adapter_servicelist_ltslabel))
                .check(matches(withText(myAccount.getAccountLabel())));

        onData(anything()).inAdapterView(withId(R.id.servicelist_listView))
                .atPosition(position)
                .onChildView(withId(R.id.adapter_servicelist_header_alias))
                .check(matches(withText(myAccount.getNickname())));

        onData(anything()).inAdapterView(withId(R.id.servicelist_listView))
                .atPosition(position)
                .onChildView(withId(R.id.adapter_servicelist_edit))
                .check(matches(BaseTest.checkIfVisible()));

        onData(anything()).inAdapterView(withId(R.id.servicelist_listView))
                .atPosition(position)
                .onChildView(withId(R.id.adapter_servicelist_star))
                .check(matches(myAccount.isNewIconDisplayed() ? isDisplayed() : BaseTest.checkIfGone()));
    }


    private void loginNetvigatorFixedLineOrNow() {
        loginMyCredentialsUAT(AccountHelper.getUATAccount6().getUsername(), AccountHelper.getUATAccount6().getPassword(), shared);
        delay(3000);
        gotoMainMenuItem(0);
    }

    private void loginCSL() {
        loginMyCredentialsUAT(AccountHelper.getUATAccount10().getUsername(), AccountHelper.getUATAccount10().getPassword(), shared);
        delay(3000);
        gotoMainMenuItem(0);
    }
}
