package com.pccw.myhkt.mymob.fragment;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.pccw.dango.shared.cra.AcMainCra;
import com.pccw.dango.shared.cra.AddOnCra;
import com.pccw.dango.shared.entity.SubnRec;
import com.pccw.dango.shared.tool.RC;
import com.pccw.myhkt.APIsManager;
import com.pccw.myhkt.APIsResponse;
import com.pccw.myhkt.ClnEnv;
import com.pccw.myhkt.DialogHelper;
import com.pccw.myhkt.FAWrapper;
import com.pccw.myhkt.InterpretRCManager;
import com.pccw.myhkt.ProgressManager;
import com.pccw.myhkt.R;
import com.pccw.myhkt.activity.BaseActivity;
import com.pccw.myhkt.activity.LoginActivity;
import com.pccw.myhkt.lib.ui.AAQuery;
import com.pccw.myhkt.lib.ui.HKTButton;
import com.pccw.myhkt.model.AcctAgent;
import com.pccw.myhkt.model.HomeButtonItem.MAINMENU;
import com.pccw.myhkt.mymob.MyMobileAccountHelper;
import com.pccw.myhkt.mymob.model.EditCra;
import com.pccw.wheat.shared.tool.Reply;

public class MyMobAddAccFragment extends BaseMyMobFragment{
	private MyMobAddAccFragment me;
	private View myView;
	private AAQuery aq;
	private MyMobileAccountHelper				myMobileAccountHelper;
	private int					CLICK_BTN_ID			= 0;
	private boolean 				isRememberPw 			= true;
	private boolean				isEditMode			= false;
	private String						localSessTok			= ClnEnv.getSessTok();
	private AcctAgent					selectedAcctAgent		= null;
	private AcctAgent					recallAcctAgent         = null;
	private String							apiKey; 	//record apiKey for login redirection (asyncId)
	private boolean							pwdDlgIsSavePwd			= false;
	private String							pwdDlgPwd				= "";
	private boolean							isPwHintUpdated			= false;
	private SubnRec							ioSubscriptionRec 		= null;
	private AddOnCra 						addonCra;

	@Override
	public void onAttach(@NonNull Activity activity) {
		super.onAttach(activity);
	}

	public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		me = this;		
		View fragmentLayout = inflater.inflate(R.layout.fragment_mymob_addacc, container, false);
		myView = fragmentLayout;
		return fragmentLayout;
	}

	@Override
	public void onStart() {
		super.onStart();

		callback_main.hideLiveChatIcon();
		//tracker TODO
	}

	@Override
	public void onPause(){
		super.onPause();
	}

	@Override
	public void onResume(){
		super.onResume();
	}

	protected void initUI() {
		aq = new AAQuery(myView);

		int extralinespace = (int) getActivity().getResources().getDimension(R.dimen.extralinespace);		
		int buttonPadding =  (int) getActivity().getResources().getDimension(R.dimen.reg_logo_padding);
		int greylineHeight = (int) getActivity().getResources().getDimension(R.dimen.greyline_height); 
		int txtColor = getActivity().getResources().getColor(R.color.hkt_txtcolor_grey);
		int textSize = (int) getActivity().getResources().getDimension(R.dimen.bodytextsize); 
		int basePadding = (int) getActivity().getResources().getDimension(R.dimen.basePadding); 
		int padding_screen =  (int) getActivity().getResources().getDimension(R.dimen.padding_screen); 


		aq.id(R.id.mymob_login_header).textColorId(R.color.hkt_textcolor_orange);
		aq.marginpx(R.id.mymob_login_desc, padding_screen, basePadding, padding_screen, 0);
		aq.marginpx(R.id.mymob_edit_loginbar, 0, buttonPadding, 0, buttonPadding);
		aq.normTxtBtn(R.id.mymob_login_button, getResString(R.string.MYHKT_BTN_SAVE), LinearLayout.LayoutParams.MATCH_PARENT , HKTButton.TYPE_WHITE_ORANGE);
		aq.norm2TxtBtns(R.id.mymob_edit_remove_button, R.id.mymob_edit_cancel_button, getString(R.string.MYMOB_BTN_REMOVE), getString(R.string.btn_cancel), HKTButton.TYPE_ORANGE);
		aq.id(R.id.mymob_login_button).clicked(this, "onClick");
		aq.id(R.id.mymob_edit_remove_button).clicked(this, "onClick");
		aq.id(R.id.mymob_edit_cancel_button).clicked(this, "onClick");
		aq.id(R.id.mymob_label_displayname).textSize(getResources().getInteger(R.integer.textsize_default_int));
		aq.id(R.id.mymob_label_mobilenum).textSize(getResources().getInteger(R.integer.textsize_default_int));
		aq.id(R.id.mymob_label_password).textSize(getResources().getInteger(R.integer.textsize_default_int));
		aq.mymobEditText(R.id.mymob_mobilenum, "", getResString(R.string.myhkt_mymob_mobilenum_hint));
		aq.mymobEditText(R.id.mymob_displayname, "", getResString(R.string.myhkt_mymob_displayname_hint));
		aq.mymobEditText(R.id.mymob_password, "", getResString(R.string.myhkt_mymob_password_hint));
		aq.id(R.id.mymob_password).getEditText().setTypeface(Typeface.DEFAULT);

		LinearLayout mymob_edit_loginbar = (LinearLayout) aq.id(R.id.mymob_edit_loginbar).getView();

		LinearLayout mymob_forget_pw_layout =  (LinearLayout) aq.id(R.id.mymob_forget_pw_layout).getView();
		mymob_forget_pw_layout.setOrientation(LinearLayout.HORIZONTAL);
		mymob_forget_pw_layout.setGravity(Gravity.RIGHT|Gravity.CENTER_VERTICAL);
		aq.id(R.id.mymob_forget_pw_txt).getTextView().setPadding(0, extralinespace, 0, extralinespace);
		aq.normTextGrey(R.id.mymob_forget_pw_txt, getResString(R.string.myhkt_FORGOTPWD));
		aq.id(R.id.mymob_forget_pw_txt).height(LinearLayout.LayoutParams.WRAP_CONTENT, false).getTextView().setPadding(0, extralinespace, 0, extralinespace);
		aq.id(R.id.mymob_forget_pw_txt).getTextView().setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.btn_details_csl, 0);
		aq.id(R.id.mymob_forget_pw_txt).getTextView().setCompoundDrawablePadding(buttonPadding);
		aq.id(R.id.mymob_forget_pw_txt).clicked(this, "onClick");

		RelativeLayout mymob_remember_pw_layout =	(RelativeLayout) aq.id(R.id.mymob_remember_pw_layout).getView();
		mymob_remember_pw_layout.setGravity(Gravity.RIGHT);
		mymob_remember_pw_layout.setGravity(Gravity.RIGHT|Gravity.CENTER_VERTICAL);

		//		aq.lineRelativeLayout(R.id.mymob_remember_pw_line1);	
		TextView mymob_remember_pw_txt = aq.id(R.id.mymob_remember_pw_txt).getTextView();
		mymob_remember_pw_txt.setText(R.string.myhkt_login_remember_pw_txt);	
		mymob_remember_pw_txt.setTextColor(txtColor);
		mymob_remember_pw_txt.setPadding(0, extralinespace, extralinespace, extralinespace);
		mymob_remember_pw_txt.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize);	
		mymob_remember_pw_txt.setGravity(Gravity.CENTER_VERTICAL);

		//		aq.line(R.id.mymob_line1);

		//save password switch button
		com.kyleduo.switchbutton.SwitchButton switchButton = aq.switchButton(R.id.mymob_remember_pw_sb);
		switchButton.setChecked(isRememberPw);
		switchButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				isRememberPw = isChecked;
			}
		});

		//auto fill data if user been redirected from LoginActivity during verification
		if (APIsManager.AO_ADDON.equals(callback_main.getActionTy()) && callback_main.getRecallAcctAgent() != null) {
			aq.id(R.id.mymob_displayname).text(callback_main.getRecallAcctAgent().getAlias());
			aq.id(R.id.mymob_mobilenum).text(callback_main.getRecallAcctAgent().getSrvNum());
			aq.id(R.id.mymob_password).text(callback_main.getRecallAcctAgent().getPassword());

			if (callback_main.isSavePwd()) {
				switchButton.setChecked(true);
			} else {
				switchButton.setChecked(false);
			}

			//Auto login after login success from LoginActivity
			loginAndSave();
			
			//remove saved flags and data
			callback_main.setRecallAcctAgent(null);
			callback_main.setActionTy(null);
			callback_main.setSavePwd(false);
		} else if (APIsManager.AO_AUTH.equals(callback_main.getActionTy()) && callback_main.getRecallAcctAgent() != null) {
			//			AcctAgent acctAgent = recallAcctAgent;
			//			selectedAcctAgent = acctAgent;
			//			
			//			// localSessTok = "DSFS3123123";
			//			if (debug) Log.d("mymob onclick", acctAgent.getSrvNum() + "/" + acctAgent.getPassword() + "/" + acctAgent.getLob());
			//			ioSubscriptionRec = new SubnRec();
			//			ioSubscriptionRec.srvNum = acctAgent.getSrvNum();
			//			ioSubscriptionRec.ivr_pwd = acctAgent.getPassword();
			//			ioSubscriptionRec.lob = acctAgent.getLob();
			//			callback_main.setIoSubscriptionRec(ioSubscriptionRec);
			//
			//			
			//			//remove saved flags and data
			//			callback_main.setRecallAcctAgent(null);
			//			callback_main.setActionTy(null);
			//			callback_main.setSavePwd(false);
			//			
			//			//authenticate
			//			callback_main.setAddOnCra(addoncra);
			//			callback_main.setAutoLogin(true);
			//			callback_main.onActivityBackPress();
		}

		updateUI();
	}

	private void updateUI() { //update UI if it is edit mode
		if (callback_main.isEditMode()) {
			if (callback_main.getEditCra() != null) {
				EditCra editcra = new EditCra();
				editcra = callback_main.getEditCra();
				aq.id(R.id.mymob_login_header).text(R.string.MYMOB_EDIT_TITLE);
				
				// Change Button to up arrow
				aq.id(R.id.mymob_edit_loginbar).getView();
				aq.id(R.id.mymob_edit_loginbar).getView().setVisibility(View.VISIBLE);
				aq.id(R.id.mymob_displayname).text(editcra.getAlias());
				aq.id(R.id.mymob_displayname).getEditText().requestFocus();
				aq.id(R.id.mymob_mobilenum).getEditText().setEnabled(false);
				aq.id(R.id.mymob_mobilenum).getEditText().setCursorVisible(false);
				aq.id(R.id.mymob_mobilenum).getEditText().setFocusable(false);
				aq.id(R.id.mymob_mobilenum).getEditText().setFocusableInTouchMode(false);
				aq.id(R.id.mymob_mobilenum).getEditText().setText(editcra.getMob_num());
				aq.id(R.id.mymob_password).text(editcra.getPassword());

				if (!editcra.getPassword().equals("")) {
					aq.switchButton(R.id.mymob_remember_pw_sb).setChecked(true);
					//					viewholder.savepassword.setChecked(true);
				} else {
					aq.switchButton(R.id.mymob_remember_pw_sb).setChecked(false);
					//					viewholder.savepassword.setChecked(false);
				}
			}

		} else {
			aq.id(R.id.mymob_edit_loginbar).getView().setVisibility(View.GONE);
			aq.id(R.id.mymob_mobilenum).getEditText().setEnabled(true);
			aq.id(R.id.mymob_mobilenum).getEditText().setCursorVisible(true);
			aq.id(R.id.mymob_mobilenum).getEditText().setFocusable(true);
			aq.id(R.id.mymob_mobilenum).getEditText().setFocusableInTouchMode(true);
		}
	}

	public void onClick(View v) {
		RelativeLayout mymob_login_layout = (RelativeLayout) aq.id(R.id.mymob_login_layout).getView();
		InputMethodManager inputManager;
		switch (v.getId()) {
		case R.id.mymob_edit_remove_button:
			try {
				inputManager = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
				inputManager.hideSoftInputFromWindow(mymob_login_layout.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
				inputManager = null;
			} catch (Exception e) {
				// fail to hide the keyboard
			}
			removeAccount();
			break;
		case R.id.mymob_edit_cancel_button:
			try {
				inputManager = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
				inputManager.hideSoftInputFromWindow(mymob_login_layout.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
				inputManager = null;
			} catch (Exception e) {
				// fail to hide the keyboard
			}
			callback_main.onActivityBackPress();
			break;
		case R.id.mymob_forget_pw_txt:
			try {
				inputManager = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
				inputManager.hideSoftInputFromWindow(mymob_login_layout.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
				inputManager = null;
			} catch (Exception e) {
				// fail to hide the keyboard
			}
			DialogHelper.createSimpleDialog(me.getActivity(), getString(R.string.MYMOB_HINT_PASSWORD));
			
			///derek testing
			//overdue amt TODO inserting fake value not finished yet
			
//			String bullshit = "網上行網上行(1068287204) 網上行網上行$1427.00 , 網上行網上行網上行網上行網上行網上行。上行網上行，上行網上行上行網上行。";
//			
//			String fullMsg = "";
//			String overdueAmt = "overdue: ";
//			if (bullshit.contains("$")) {
//					String str = bullshit;
//					String[] parts = str.split("\\$");
//					String part2 = parts[1];
//					String amt = part2.substring(0, part2.indexOf("."));
//					String amtDec = part2.substring(part2.indexOf("."), part2.indexOf(".") + 3);
//					String amount = "$" + amt + amtDec;
//					fullMsg = fullMsg + overdueAmt + amount + "\n";
//			}
//			DialogHelper.createSimpleDialog(me.getActivity(), fullMsg);

			//derek test end
			break;
		case R.id.mymob_login_button:
			EditText mymob_mobilenum = (EditText) aq.id(R.id.mymob_mobilenum).getView();
			EditText mymob_password = (EditText) aq.id(R.id.mymob_password).getView();

			CLICK_BTN_ID = R.id.mymob_login_button;
			// hide the soft keyboard
			try {
				inputManager = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
				inputManager.hideSoftInputFromWindow(mymob_login_layout.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
				inputManager = null;
			} catch (Exception e) {
				// fail to hide the keyboard
			}

			//do login and save if mobile number validation passed
			if (mymob_mobilenum.getText().toString().length() != 8) {
				DialogHelper.createSimpleDialog(getActivity(), InterpretRCManager.interpretMM_Mdu(getActivity(), new Reply(RC.SVEE_IVMOBALRT)));
			} else if (mymob_password.getText().toString().trim().length() == 0) {
				DialogHelper.createSimpleDialog(getActivity(), InterpretRCManager.interpretMM_Mdu(getActivity(), new Reply(RC.AO_EAI_IVPWD)));
			} else {
				myMobileAccountHelper = MyMobileAccountHelper.getInstance(getActivity());

				////////stupid fake code test
				if (!callback_main.isEditMode() && ((myMobileAccountHelper.isRecordExists(mymob_mobilenum.getText().toString()) || 
						isMyAccountRecExist(mymob_mobilenum.getText().toString())))) {
					DialogHelper.createSimpleDialog(getActivity(), getString(R.string.MYMOB_RC_MM_SUBN_FND));
				} else if (!callback_main.isEditMode()){

					// Pop Tnc Dialog Before doVerify
					OnClickListener onPositiveClickListener = new OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							dialog.dismiss();
							loginAndSave();
						}
					};
					DialogHelper.createSimpleDialog(getActivity(), getString(R.string.MYMOB_SAVE_CONFIRM), getString(R.string.MYHKT_BTN_CONFIRM)
							, onPositiveClickListener, getString(R.string.btn_cancel));
				} else {
					loginAndSave();
				}
				//				AddOnCra addon = new AddOnCra();
				//				prepareAcctAgent(addon);
				//				addRecord(addon);
				//				callback_main.onActivityBackPress();
				/////////
			}
			break;
		}
	}

	private boolean isMyAccountRecExist(String mobileNum) {
		if (ClnEnv.isLoggedIn()) {
			if (ClnEnv.getLgiCra().getOQualSvee().getSubnRecAry().length != 0){
				SubnRec[] subnRec = ClnEnv.getLgiCra().getOQualSvee().getSubnRecAry();
				for (int i = 0; i < subnRec.length; i++) {
					if ((subnRec[i].lob.equalsIgnoreCase(SubnRec.LOB_101) ||
							subnRec[i].lob.equalsIgnoreCase(SubnRec.LOB_IOI) ||
							subnRec[i].lob.equalsIgnoreCase(SubnRec.LOB_MOB) ||
							subnRec[i].lob.equalsIgnoreCase(SubnRec.LOB_O2F)) &&
							subnRec[i].srvNum.equalsIgnoreCase(aq.id(R.id.mymob_mobilenum).getText().toString())) {
						return true;
					}
				}
			}
		}
		return false;
	}

	private void loginAndSave() {
		// Set srv_num and pwd from input
		ioSubscriptionRec = new SubnRec();
		ioSubscriptionRec.srvNum = aq.id(R.id.mymob_mobilenum).getText().toString();
		ioSubscriptionRec.ivr_pwd = aq.id(R.id.mymob_password).getText().toString();
		doVerify();
	}

	private final void doVerify() {
		addonCra = new AddOnCra();
		addonCra.setISubnRec(getIoSubscriptionRec());
		addonCra.setISms(callback_main.isEditMode() ? false:true);
		addonCra.setILoginId(ClnEnv.isLoggedIn() ? ClnEnv.getSessionLoginID() : "");
		APIsManager.doVerify(me, addonCra);
	}

	public SubnRec getIoSubscriptionRec() {
		return ioSubscriptionRec;
	}

	private void loginAndSaveDialog(final AddOnCra addoncra) {
		//Track "new account added" event
		FAWrapper.getInstance().sendFAEvents(me.getActivity(), R.string.CONST_GA_CATEGORY_USERLV,
				R.string.CONST_GA_ACTION_BTN, R.string.CONST_GA_LABEL_ADD_MYMOBACC_SUCC, false);

		// Add mode ,insert new record in DB
		addRecord(addoncra);

		//go to serviceList and call this API instead of calling it right here
		callback_main.setAddOnCra(addoncra);
		callback_main.setSelectedAcctAgent(selectedAcctAgent);
		callback_main.setAutoLogin(true);
		callback_main.onActivityBackPress();

		//old code
		//		AddOnCra authAddonCra = new AddOnCra();
		//		authAddonCra.setISms(isEditMode ? false:true);
		//		authAddonCra.setILoginId(ClnEnv.isLoggedIn() ? ClnEnv.getSessionLoginID() : "");
		//
		//		authAddonCra.setISubnRec(addoncra.getOSubnRec());
		//		APIsManager.doAuthen(me, authAddonCra);
		//
		//		loadList();
	}

	// Add mobile number info into local DB
	private void addRecord(AddOnCra addoncra) {
		aq = new AAQuery(myView);

		/////stupid fake data test
		String alias = aq.id(R.id.mymob_displayname).getText().toString();
		String mob_num = addoncra.getOSubnRec().srvNum;
		String cardType = getString(R.string.CONST_POSTPAID);
		String lob = addoncra.getOSubnRec().lob;
		String password = "";
		if (isRememberPw) {
			password = addoncra.getOSubnRec().ivr_pwd;
		}
		//		String alias = "sosad";
		//		String mob_num = "98238921";
		//		String cardType = getString(R.string.CONST_POSTPAID);
		//		String lob = "MOB";
		//		String password = "";
		////		if (isRememberPw) {
		//			password = "000000";
		////		}
		/////

		myMobileAccountHelper = MyMobileAccountHelper.getInstance(getActivity());
		String encPassword = ClnEnv.getEncString(getActivity(), mob_num, password);
		myMobileAccountHelper.insert(alias, mob_num, encPassword, cardType, lob, "");
	}

	private void prepareAcctAgent(AddOnCra addoncra) {
		if (addoncra == null) {
			selectedAcctAgent = new AcctAgent();
			selectedAcctAgent.setAlias(aq.id(R.id.mymob_displayname).getText().toString());
			selectedAcctAgent.setMyMob(true);
			selectedAcctAgent.setSrvNum(aq.id(R.id.mymob_mobilenum).getText().toString());
			selectedAcctAgent.setPassword(aq.id(R.id.mymob_password).getText().toString());
			selectedAcctAgent.setLatest_bill("");
			selectedAcctAgent.setSms(false);

			//default cardtype "POSTPAID"
			selectedAcctAgent.setCardType(getResString(R.string.CONST_POSTPAID));
		} else {
			selectedAcctAgent = new AcctAgent();
			selectedAcctAgent.setAcctNum(addoncra.getOSubnRec().acctNum);
			selectedAcctAgent.setAlias(aq.id(R.id.mymob_displayname).getText().toString());
			selectedAcctAgent.setMyMob(true);
			selectedAcctAgent.setSrvNum(aq.id(R.id.mymob_mobilenum).getText().toString());
			selectedAcctAgent.setPassword(aq.id(R.id.mymob_password).getText().toString());
			selectedAcctAgent.setLob(addoncra.getOSubnRec().lob);
			selectedAcctAgent.setLatest_bill("");
			selectedAcctAgent.setSms(false);

			//default cardtype "POSTPAID"
			selectedAcctAgent.setCardType(getResString(R.string.CONST_POSTPAID));

		}
		callback_main.setSelectedAcctAgent(selectedAcctAgent);
	}

	private void removeAccount() {
		aq = new AAQuery(myView);
		callback_main.setEditMode(true);

			final String mob_num = aq.id(R.id.mymob_mobilenum).getText().toString().trim();

			OnClickListener onPositiveClickListener =  new AlertDialog.OnClickListener() {
				public void onClick(DialogInterface dialog, int which) {
					if (debug) Log.d("MyMobileActivity", "Delete mob num:" + mob_num);
					// Delete Record in Database
					myMobileAccountHelper = MyMobileAccountHelper.getInstance(getActivity());
					myMobileAccountHelper.delete(mob_num);
					if (callback_main.isEmptyList()) {
						callback_main.setEditMode(false);
						initUI();
					} else {
						callback_main.onActivityBackPress();
					}
				}
			};
			DialogHelper.createSimpleDialog(getActivity(), getString(R.string.myhkt_delete_account_confirm), getString(R.string.btn_confirm), onPositiveClickListener, getString(R.string.MYHKT_BTN_CANCEL));
		
	}

	// Translate Reply getCode() to screen-specific string
	public final String interpretRC(Reply rRC) {
		//		if (rRC.getCode().equals(RC.RCUS_IVMOB)) { return getString(R.string.REGM_IVMOB); }
		//		if (rRC.getCode().equals(Reply.RC_MM_EAI_IVPWD) || rRC.isEqual(Reply.RC_MM_MMP_IVPWD)) { return Utils.getString(me, R.string.PAMM_IVCURRPWD); }
		if (rRC.getCode().equals(RC.AO_INDIV_H)) { return getString(R.string.MYMOB_RC_MM_H_INDIV); }
		if (rRC.getCode().equals(RC.AO_INDIV_C)) { return getString(R.string.MYMOB_RC_MM_C_INDIV); }
		if (rRC.getCode().equals(RC.AO_TOO_MANY)) { return getString(R.string.MYMOB_RC_MM_TOO_MANY); }
		if (rRC.getCode().equals(RC.AO_EAI_IVPWD)) { return getString(R.string.MYMOB_RC_MM_MMP_IVPWD); }
		if (rRC.getCode().equals(RC.AO_MMP_IVPWD)) { return getString(R.string.MYMOB_RC_MM_MMP_IVPWD); }
		if (rRC.getCode().equals(RC.MOBA_ALSB_IVPWD)) { return getString(R.string.MYMOB_RC_MM_MMP_IVPWD); }
		//		if (rRC.getCode().equals(RC.RC_MM_MMP_IVPWD)) { return Utils.getString(me, R.string.MYMOB_RC_MM_MMP_IVPWD); }
		if (rRC.getCode().equals(RC.AO_SUBN_FND)) { return getString(R.string.MYMOB_RC_MM_SUBN_FND); }
		if (rRC.getCode().equals(RC.AO_MMP_NO_SUBN)) { return getString(R.string.MYMOB_RC_MM_NO_SUBN); }
		//		if (rRC.getCode().equals(RC.RC_API_NOTAUTH)) {return Utils.getString(me, R.string.MYMOB_RC_CVI_UNSUCC);}
		if (rRC.getCode().equals(RC.CVI_UNSUCC) || rRC.getCode().equals(RC.API_NOTAUTH)) {return getString(R.string.MYMOB_RC_CVI_UNSUCC);}
		if (rRC.getCode().equals(RC.AO_MMP_OA)) {return getString(R.string.MYMOB_RC_MM_MMP_OA);}
		return InterpretRCManager.interpretMM_Mdu(getActivity().getApplicationContext(), rRC);
	}

	public final String interpretRC(Reply rRC, String lob) { 
		if (lob.equalsIgnoreCase(SubnRec.LOB_101)) {
			if (rRC.getCode().equals(RC.API_NOTAUTH)) { 
				return getString(R.string.MYMOB_RC_CVI_UNSUCC_101); 
			} else if (rRC.getCode().equals(RC.CVI_UNSUCC)) { 
				return getString(R.string.MYMOB_RC_CVI_UNSUCC_101); 
			}
		} else if (lob.equalsIgnoreCase(SubnRec.LOB_O2F)) {
			if (rRC.getCode().equals(RC.API_NOTAUTH)) { 
				return getString(R.string.MYMOB_RC_CVI_UNSUCC_O2F); 
			} else if (rRC.getCode().equals(RC.CVI_UNSUCC)) { 
				return getString(R.string.MYMOB_RC_CVI_UNSUCC_O2F); 
			}
		}
		return interpretRC(rRC);
	}

	protected final void redirectDialog(String message) {

	}

	@Override
	public void onSuccess(APIsResponse response) throws Exception {
		aq = new AAQuery(myView);
		Bundle 		bundle;
		Intent		intent;
		if (response != null) {
			if (APIsManager.HELO.equals(response.getActionTy())) {
			} else if (APIsManager.AO_ADDON.equals(response.getActionTy())) {
				AddOnCra addonCra = (AddOnCra) response.getCra();
				ClnEnv.setMyMobFlag(true);
				// Set srv_num and pwd from input

				addonCra.getOSubnRec().srvNum = aq.id(R.id.mymob_mobilenum).getText().toString();
				addonCra.getOSubnRec().ivr_pwd = aq.id(R.id.mymob_password).getText().toString();

				if (callback_main.isEditMode()) {
					// Update DB only ,no need to auth
					String alias = aq.id(R.id.mymob_displayname).getText().toString().trim();
					String mob_num = aq.id(R.id.mymob_mobilenum).getText().toString().trim();
					String password = "";
					if (isRememberPw) {
						password = aq.id(R.id.mymob_password).getText().toString().trim();
						if (me.debug) Log.d("password", password);
					}
					if (me.myMobileAccountHelper.isRecordExists(mob_num)) {
						String encPassword = ClnEnv.getEncString(getActivity(), mob_num, password);
						me.myMobileAccountHelper.updateAccountInfo(alias, mob_num, encPassword, "");
					}
					ProgressManager.getInstance(getActivity()).dismissProgressDialog(true);
					callback_main.onActivityBackPress();
					//					me.loadList(); // loadlist in service list
				} else {
					//when it is a newly added account, create acctagent after verify 
					me.prepareAcctAgent(addonCra);  //TODO
					me.loginAndSaveDialog(addonCra); //TODO
				}

			} else if (APIsManager.SVCASO.equals(response.getActionTy())) {
				AcMainCra acMainCra = (AcMainCra) response.getCra();

				DialogHelper.createSimpleDialog(getActivity(), getString(R.string.RSVM_DONE));
				ClnEnv.getQualSvee().setSveeRec(acMainCra.getOSveeRec());
				ClnEnv.getQualSvee().setSubnRecAry(acMainCra.getOSubnRecAry());

				// Reset check RC_ALT error time
				//				me.errALT2nd = false;
				//				refreshList(); //TODO refresh the service list
			}
		}
	}

	@Override
	public void onFail(APIsResponse response) throws Exception {
		// General Error Message
		if (!"".equals(response.getMessage()) && response.getMessage() != null) {
			DialogHelper.createSimpleDialog(getActivity(), response.getMessage());
		} else if (RC.TCSESS_MM.equalsIgnoreCase(response.getReply().getCode())) {
			if (APIsManager.AO_ADDON.equals(response.getActionTy())) {
				if (ClnEnv.isLoggedIn()) {
					BaseActivity.ivSessDialog();
				} else {
					ClnEnv.setHeloCra(null);
					loginAndSave();
				}
			} else {
				BaseActivity.ivSessDialog();
			}
		} else {
			if (response.getReply().getCode().equals(RC.AO_INDIV_H) || response.getReply().getCode().equals(RC.AO_INDIV_C)) {
				prepareAcctAgent(addonCra);
				if (isRememberPw) {
					callback_main.setSavePwd(true);
				} else {
					callback_main.setSavePwd(false);
				}
				//redirect to login and come back here for add acc retry
				DialogHelper.createSimpleDialog(getActivity(), getString(R.string.MYMOB_RC_MM_H_INDIV), getString(R.string.btn_ok), 
						new OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						Bundle	rbundle;
						Intent intent;
						dialog.dismiss();
						ClnEnv.clear(getActivity());
						intent = new Intent(getActivity().getApplicationContext(), LoginActivity.class);
						intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
						rbundle = new Bundle();
						rbundle.putString("ACTIONTY", APIsManager.AO_ADDON);
						rbundle.putSerializable("CLICKBUTTON", MAINMENU.MYMOB);
						rbundle.putBoolean("ISSAVEPWD", callback_main.isSavePwd());
						rbundle.putBoolean("ISMYMOBLOGIN", false);
						prepareAcctAgent(null);
						rbundle.putSerializable("SELECTACCTAGENT", callback_main.getSelectedAcctAgent());
						//				asyncId = null;
						intent.putExtras(rbundle);
						startActivity(intent);
						getActivity().overridePendingTransition(R.anim.left_slide_in, R.anim.right_slide_out);
					}
				});
			} else {
				DialogHelper.createSimpleDialog(getActivity(), InterpretRCManager.interpretMM_Mdu(getActivity(), response.getReply()));
			}
		}
	}

}
