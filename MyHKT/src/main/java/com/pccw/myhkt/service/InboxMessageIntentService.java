package com.pccw.myhkt.service;

import android.app.IntentService;
import android.content.Intent;

import androidx.annotation.Nullable;

import com.pccw.dango.shared.cra.InbxCra;
import com.pccw.myhkt.APIsManager;
import com.pccw.myhkt.APIsResponse;
import com.pccw.myhkt.ClnEnv;
import com.pccw.myhkt.Utils;
import com.pccw.myhkt.util.Constant;

public class InboxMessageIntentService extends IntentService implements APIsManager.OnAPIsListener{

    public InboxMessageIntentService() {
        super("InboxMessageIntentService");
    }

    public InboxMessageIntentService(String name) {
        super(name);
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {

    }

    @Override
    public int onStartCommand(@Nullable Intent intent, int flags, int startId) {

        InbxCra inbxCra = new InbxCra();
        inbxCra.setILoginId(ClnEnv.getLoginId());

        APIsManager.doGetInboxMessages(this, inbxCra);

        return super.onStartCommand(intent, flags, startId);


    }

    @Override
    public void onSuccess(APIsResponse response) throws Exception {
        if(response != null) {
            if (response.getActionTy().equals(APIsManager.INBX_MSG)) {
                InbxCra inbxCra = (InbxCra) response.getCra();
                if(inbxCra != null) {
                    ClnEnv.setPref(this.getApplicationContext(), Constant.CONST_INBOX_CRA_RESPONSE, Utils.serialize(inbxCra));

                    //broadcast: inbox is updated
                    sendBroadcast(new Intent().setAction(Constant.ACTION_INBOX_LIST_UPDATED).setPackage(getPackageName()));
                } else {
                    ClnEnv.setPref(this.getApplicationContext(), Constant.CONST_INBOX_CRA_RESPONSE, "");
                }
            }
        }
        stopSelf();
    }

    @Override
    public void onFail(APIsResponse response) throws Exception {
        if(response != null) {
            if (!"".equals(response.getMessage()) && response.getMessage() != null) {
                ClnEnv.setPref(this.getApplicationContext(), Constant.CONST_INBOX_CRA_RESPONSE, "");
            }
        }
    }
}
