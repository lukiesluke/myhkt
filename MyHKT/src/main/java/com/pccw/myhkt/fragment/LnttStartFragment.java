package com.pccw.myhkt.fragment;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Point;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextPaint;
import android.text.style.ClickableSpan;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;

import com.pccw.dango.shared.cra.ApptCra;
import com.pccw.dango.shared.cra.LnttCra;
import com.pccw.dango.shared.entity.SubnRec;
import com.pccw.dango.shared.tool.RC;
import com.pccw.myhkt.APIsManager;
import com.pccw.myhkt.APIsResponse;
import com.pccw.myhkt.ClnEnv;
import com.pccw.myhkt.DialogHelper;
import com.pccw.myhkt.FAWrapper;
import com.pccw.myhkt.GlobalDialog;
import com.pccw.myhkt.R;
import com.pccw.myhkt.Utils;
import com.pccw.myhkt.activity.BaseActivity;
import com.pccw.myhkt.cell.model.BigTextCell;
import com.pccw.myhkt.cell.model.Cell;
import com.pccw.myhkt.cell.model.IconTextCell;
import com.pccw.myhkt.cell.model.ImageViewCell;
import com.pccw.myhkt.cell.view.CellViewAdapter;
import com.pccw.myhkt.lib.ui.AAQuery;
import com.pccw.myhkt.model.LnttAgent;
import com.pccw.myhkt.service.LineTestIntentService;
import com.pccw.myhkt.util.Constant;
import com.pccw.myhkt.util.RuntimePermissionUtil;


/************************************************************************
 * File : LnttStartFragment.java 
 * Desc : Reg Menu 
 * Name : LnttStartFragment
 * by : Andy Wong 
 * Date : 23/02/2016
 *
 * Change History:
 * Date       Modified By		Description
 * ---------- ----------------	-------------------------------
 * 23/02/2016 Andy Wong 		-First draft
 *************************************************************************/
public class LnttStartFragment extends BaseServiceFragment{

	private String TAG = "LnttStartFragment";
	private LnttStartFragment me;
	private	AAQuery	aq;
	private int extralinespace;
	private int titleWidth;
	private int	edittextpadding;
	private int botBarHeight;
	private int	deviceWidth;

	private List<Cell> cellList;
	private CellViewAdapter	cellViewAdapter;
	private int btnWidth = 0;
	private LinearLayout frame;
	private LnttCra lnttCra;
	private ApptCra apptCra;
	private LnttAgent lnttAgent;

	@Override
	public void onAttach(@NonNull Activity activity) {
		super.onAttach(activity);
		try {
			lnttAgent = callback_main.getLnttAgent();
			//			callback = (OnIDDCodeNtimesListener) activity;
		} catch (ClassCastException e) {
			throw new ClassCastException(activity.toString() + " must implement OnmyprofloginListener");
		}
	}

	public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		me = this;
		View fragmentLayout = inflater.inflate(R.layout.fragment_linetest_st, container, false);

		aq = new AAQuery(fragmentLayout);
		initData();
		return fragmentLayout;
	}


	protected void initData() {
		Display display = this.getActivity().getWindowManager().getDefaultDisplay();
		Point size = new Point();
		display.getSize(size);
		deviceWidth = size.x;
		extralinespace = (int) getResources().getDimension(R.dimen.extralinespace);
		basePadding = (int) getResources().getDimension(R.dimen.basePadding);
		titleWidth = (int) getResources().getDimension(R.dimen.idd_title_width);
		edittextpadding = (int) getResources().getDimension(R.dimen.edittextpadding);
		botBarHeight	= (int) getResources().getDimension(R.dimen.botbar_height);
		btnWidth = deviceWidth - basePadding * 4;
		cellViewAdapter = new CellViewAdapter(me.getActivity());
	}


	@Override
	protected void initUI() {
		cellList	= new ArrayList<Cell>();
		//ScrollView
		aq.id(R.id.linetest_st_sv).backgroundColorId(R.color.white);
		frame = (LinearLayout) aq.id(R.id.linetest_st_frame).getView();
		IconTextCell titleCell = new IconTextCell(R.drawable.test_start_icon , getResString(R.string.myhkt_linetest));
		titleCell.setLeftPadding(basePadding);
		titleCell.setRightPadding(basePadding);
		titleCell.setTopMargin(0);
		cellList.add(titleCell);

		String intro = getResString(R.string.LTTF_ITO);

		BigTextCell introCell = new BigTextCell("",intro);
		introCell.setLeftMargin(basePadding);
		introCell.setRightMargin(basePadding);
		introCell.setContentSizeDelta(0);
		introCell.setTopMargin(0);
		cellList.add(introCell);

		//default image URL
		ImageViewCell imageViewCell = new ImageViewCell(getResources().getDrawable(R.drawable.lntt_start));
		imageViewCell.setLeftMargin((int) getResources().getDimension(R.dimen.lntt_image_padding));
		imageViewCell.setRightMargin((int) getResources().getDimension(R.dimen.lntt_image_padding));
		cellList.add(imageViewCell);

		//Links sr1 2, 3
		String link = getResString(R.string.linetest_srvideo1);

		String[] stringArr = new String[callback_main.getLob() != R.string.CONST_LOB_LTS ? 3 : 1];
		ClickableSpan[] clickableSpanArr = new ClickableSpan[callback_main.getLob() != R.string.CONST_LOB_LTS ? 3 : 1];

		stringArr[0] = getResString(R.string.linetest_srvideo1);
		clickableSpanArr[0]	= CHECK_PC_SETTING;

		//add the sr2 and sr3 for non LTS
		if(callback_main.getLob() != R.string.CONST_LOB_LTS) {
			link += "\n\n" +
					getResString(R.string.linetest_srvideo2) +"\n\n" +
					getResString(R.string.linetest_srvideo3);

			stringArr[1] = getResString(R.string.linetest_srvideo2);
			stringArr[2] = getResString(R.string.linetest_srvideo3);

			clickableSpanArr[1] = CHECK_EQ_CONNECTION;
			clickableSpanArr[2] = CHECK_WIFI_TIPS;

		}
		BigTextCell linksCell = new BigTextCell("",link);
		linksCell.setLeftMargin(basePadding);
		linksCell.setRightMargin(basePadding);
		linksCell.setContentSizeDelta(0);

		linksCell.setClickableSpans(stringArr, clickableSpanArr);

		cellList.add(linksCell);

		cellViewAdapter.setView(frame, cellList);

		//Bot bar
		aq.id(R.id.linetest_st_botar).height(botBarHeight, false).backgroundColorId(R.color.white);
		aq.padding(R.id.linetest_st_botar, basePadding, 0, basePadding, 0);
		aq.gravity(R.id.linetest_st_botar, Gravity.CENTER);
		aq.normTxtBtn(R.id.linetest_st_btn, getResString(R.string.LTTF_TEST), btnWidth);
		aq.id(R.id.linetest_st_btn).clicked(this, "onClick");

		//		doEnquireSR4NoPend();

	}


	@Override
	public void refresh(){
		super.refresh();
		refreshData();
	}

	protected final void refreshData() {
		super.refreshData();

		if (callback_main!=null){
			isToDimLineTestButton(RuntimePermissionUtil.isServiceRunning(getActivity(), LineTestIntentService.class));
			apptCra = callback_main.getApptCra();
			// Screen Tracker
			if (SubnRec.LOB_LTS.equalsIgnoreCase(callback_main.getSubnRec().lob)) {
				FAWrapper.getInstance().sendFAScreen(getActivity(),
						R.string.CONST_SCRN_LTSLINETEST1, false);
			} else if (SubnRec.LOB_PCD.equalsIgnoreCase(callback_main.getSubnRec().lob)) {
				FAWrapper.getInstance().sendFAScreen(getActivity(),
						R.string.CONST_SCRN_PCDLINETEST1, false);
			} else if (SubnRec.LOB_TV.equalsIgnoreCase(callback_main.getSubnRec().lob)) {
				FAWrapper.getInstance().sendFAScreen(getActivity(),
						R.string.CONST_SCRN_TVLINETEST1, false);
			}
			if (debug) Log.i(TAG, "A"+callback_main.getActiveSubview() + "/" +R.string.CONST_SELECTEDVIEW_LINETEST);
			if (callback_main.getActiveSubview() == R.string.CONST_SELECTEDVIEW_LINETEST) {
				if (debug) Log.i(TAG, "LnttStart : Current Page");
				if ((callback_main.getSubnRec().aloneDialup.equals(Tool.FLG_NO) || !SubnRec.LOB_PCD.equalsIgnoreCase(callback_main.getSubnRec().lob)) && !SubnRec.TOS_IMS_GGDY.equalsIgnoreCase(callback_main.getSubnRec().tos)) {
					// Expiry Checking & Error Checking after Enquire SR ?
					// Line Test Result Expiry Checking
					if (!"".equalsIgnoreCase(lnttAgent.getEndTimestamp()) && Utils.isExpiredLnttResult(me.getActivity(), lnttAgent.getEndTimestamp())) {
						Utils.clearLnttService(me.getActivity());
						if (debug) Log.i(TAG, "LnttStart : Exprie Line Test Result");
						lnttAgent = new LnttAgent();
						apptCra = null;
//						doEnquireSR4NoPend();

					}
					String lnttAgentStr = ClnEnv.getPref(this.getActivity(), this.getString(R.string.CONST_PREF_LNTT_AGENT), "");
					if (!lnttAgent.getLnttSrvNum().equalsIgnoreCase(callback_main.getSubnRec().srvNum) && (!"".equalsIgnoreCase(lnttAgent.getEndTimestamp()))) {
						if (lnttAgentStr.equals("")) {
							// Enquire SR for No Pending
							if (debug) Log.i(TAG, "LnttStart : Enquire SR");
							doEnquireSR4NoPend();
						} else {
							//Pending Line Tesst Result
							if (debug) Log.i(TAG, "LnttStart : Pending Line Test Result");
							displayDialog(getResString(R.string.MYHKT_LT_ERR_PENDING_LINETEST_RESULT));
							aq.id(R.id.linetest_st_btn).getButton().setEnabled(false) ;
							int		m_color				= Color.argb(200, 255, 255, 255);
							aq.id(R.id.linetest_st_btn).getButton().setAlpha(0.2f);
						}

					} else {
						String errorMsg = lnttAgent.getResultMsg();
						if (!"".equalsIgnoreCase(errorMsg)) {
							// Get ApptCra from lnttAgent, instead of call API again
							apptCra = lnttAgent.getApptCra().copyMe();
							// If Session Invalid, redirected to Login
							if (lnttAgent.getLnttCra().getReply().getCode().equals(RC.GWTGUD_MM) || lnttAgent.getLnttCra().getReply().getCode().equals(RC.ALT)) {
								// RC_IVSESS
								// RC_ALT
								//TODO
								//							me.redirectDialog(ClnEnv.getRPCErrMsg(me.getActivity(), RC.GWTGUD_MM));
								if (debug) Log.i(TAG, "LnttStart : Invalid Session");
							} else {
								if (debug) Log.i(TAG, "LnttStart : Server error");
								lnttAgent = new LnttAgent();
								Utils.clearLnttService(me.getActivity());
								doneEnquireSR();
								// Show LineTest Expire if any server Error occur
								me.displayDialog(errorMsg);
							}
						} else {
							if ("".equalsIgnoreCase(lnttAgent.getLnttSrvNum()) || (lnttAgent.getLnttSrvNum().equalsIgnoreCase(callback_main.getSubnRec().srvNum) && !"".equalsIgnoreCase(lnttAgent.getResultMsg())) || (!lnttAgent.getLnttSrvNum()
									.equalsIgnoreCase(callback_main.getSubnRec().srvNum) && (!"".equalsIgnoreCase(lnttAgent.getEndTimestamp())))) {
								if (debug) Log.i(TAG, "LnttStart : T");
							} else if (!(lnttAgent.getLnttSrvNum().equalsIgnoreCase(callback_main.getSubnRec().srvNum) && lnttAgent.getLnttCra().getISubnRec().acctNum.equalsIgnoreCase(callback_main.getSubnRec().acctNum))) {

								if(RuntimePermissionUtil.isServiceRunning(getActivity(), LineTestIntentService.class)){
									displayDialog(getResString(R.string.MYHKT_LT_ERR_RUNNING));

									isToDimLineTestButton(RuntimePermissionUtil.isServiceRunning(getActivity(), LineTestIntentService.class));
									int		m_color				= Color.argb(200, 255, 255, 255);
									aq.id(R.id.linetest_st_btn).getButton().setAlpha(0.2f);
								}
							} else if ("".equalsIgnoreCase(lnttAgent.getEndTimestamp())  && RuntimePermissionUtil.isServiceRunning(getActivity(), LineTestIntentService.class)) {
								isToDimLineTestButton(RuntimePermissionUtil.isServiceRunning(getActivity(), LineTestIntentService.class));
								int		m_color				= Color.argb(200, 255, 255, 255);
								aq.id(R.id.linetest_st_btn).getButton().setAlpha(0.2f);
							} else {
								if (debug) Log.i(TAG, "LnttStart : Enquire SR");
							}
						}

						if (apptCra == null) {
							// Enquire SR for No Pending
							doEnquireSR4NoPend();
						}
					}
					// Pass data back Activity
					callback_main.setApptCra(apptCra);
					callback_main.setLnttAgent(lnttAgent);
					callback_main.setLnttCra(lnttCra);
				}
			}
		}
	}

	public final void onClick(View view) {
		if (view.getId() == R.id.linetest_st_btn) {
			/* Check whether it is a Standalone Dial-up
			 * if yes, it should not be allowed LNTEST */
			if ((callback_main.getSubnRec().aloneDialup.equals(Tool.FLG_NO) || !SubnRec.LOB_PCD.equalsIgnoreCase(callback_main.getSubnRec().lob)) && !SubnRec.TOS_IMS_GGDY.equalsIgnoreCase(callback_main.getSubnRec().tos)) {
				// Broadband
				if (!lnttAgent.getLnttSrvNum().equalsIgnoreCase(callback_main.getSubnRec().srvNum) && (!"".equalsIgnoreCase(lnttAgent.getLnttSrvNum()))) {
					if(RuntimePermissionUtil.isServiceRunning(getActivity(), LineTestIntentService.class)){
						DialogHelper.createSimpleDialog(getActivity(), getString(R.string.MYHKT_LT_ERR_PENDING_LINETEST_RESULT));
					}
					else{
						Utils.clearLnttService(getActivity().getApplicationContext());
						lnttCra = null;
						callback_main.setIsLnttServiceClearable(false);
						doLineTestRequest();
					}
				}
				else if(RuntimePermissionUtil.isServiceRunning(getActivity(), LineTestIntentService.class)){
					DialogHelper.createSimpleDialog(getActivity(), getString(R.string.MYHKT_LT_MSG_NORMAL_START_SUCCESS));
				} else {
					Utils.clearLnttService(getActivity().getApplicationContext());
					lnttCra = null;
					callback_main.setIsLnttServiceClearable(false);
					doLineTestRequest();
				}
			} else {
				// Dialup
				DialogHelper.createSimpleDialog(getActivity(), getString(R.string.myhkt_LNTT_NOTBB));
			}
		}
	}
	private final void doneEnquireSR() {
		if (apptCra != null) {
			if (apptCra.getReply().getCode().equals(RC.USRB_PENDSR_FND)) {
				DialogHelper.createSimpleDialog(me.getActivity(), getResString(R.string.MYHKT_LT_ERR_SR_FOUND));
			}
		}
	}

	private void doEnquireSR4NoPend(){
		if (callback_main.getSubnRec() !=null) {
			apptCra = new ApptCra();
			apptCra.setILoginId(ClnEnv.getLoginId());
			apptCra.setICustRec(ClnEnv.getQualSvee().getCustRec().copyMe());
			apptCra.setISubnRec(callback_main.getSubnRec());
			if (lnttCra != null) {
				apptCra.setIEnqSRTy(lnttCra.getOLtrsRec().srStatus);
			}
		}
		APIsManager.doEnquireSR4NoPend(me, apptCra);
		// Pass data back Activity
		callback_main.setApptCra(apptCra);
	}

	private final void  doLineTestRequest() {
		// Avoid User start Line Test more times
		// If the LineTest started before, just show the InProgressDialog Only.
		//		LnttAgent lnttAgent = Utils.getPrefLnttAgent(me.getActivity());
		Bundle lnttBundle = null;
		LnttAgent lnttAgent = new LnttAgent();
		if (debug) Log.i(TAG, lnttAgent.getLnttSrvNum()+"");
		if ("".equalsIgnoreCase(lnttAgent.getLnttSrvNum())) {
			// Prepare LineTest related variables
			if (debug) Log.i(TAG, "Service start");
			lnttAgent = new LnttAgent();
			lnttAgent.setSessTok(ClnEnv.getSessTok());
			lnttAgent.prepareLnttCra(ClnEnv.getQualSvee().getSubnRecAry(), callback_main.getSubnRec().copyMe(), ClnEnv.getSessionLoginID());
			// Only Reset and Retest, lnttCra is not null and passed Object
			if (lnttCra != null) {
				lnttAgent.getLnttCra().setOLtrsRec(lnttCra.getOLtrsRec().copyMe());
			}
			// Also Store SR Result
			lnttAgent.setApptCra(apptCra);

			lnttBundle = new Bundle();
			lnttBundle.putSerializable("LNTTAGENT", lnttAgent);

		}

		// Redirect to ServiceList and show InProgess Dialog
		Intent dialogIntent = new Intent(me.getActivity(), GlobalDialog.class);
		if (lnttCra == null) {
			dialogIntent.putExtra("DIALOGTYPE", R.string.CONST_DIALOG_LNTT_ONSTART);
		} else {
			dialogIntent.putExtra("DIALOGTYPE", R.string.CONST_DIALOG_LNTT_ONRESET);
		}
		dialogIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
		startActivity(dialogIntent);
		//getActivity().overridePendingTransition(R.anim.left_slide_in, R.anim.right_slide_out);
		//getActivity().finish();
		refreshData();

		//Enable or disable the line test button
		isToDimLineTestButton(RuntimePermissionUtil.isServiceRunning(getActivity(), LineTestIntentService.class));
		if(lnttBundle != null){
			// Directly start a Service
			Intent intent = new Intent(me.getActivity(), LineTestIntentService.class);
			intent.putExtras(lnttBundle);
			getActivity().startService(intent);
		}
	}

	@Override
	public void onSuccess(APIsResponse response) {
		if (response.getActionTy().equals(APIsManager.SR_ENQ_NP)) {
			ApptCra mApptCra = (ApptCra) response.getCra();
			if (mApptCra != null) {
				apptCra.setReply(mApptCra.getReply());
				apptCra.setServerTS(mApptCra.getServerTS());
				callback_main.setApptCra(apptCra.copyMe());
			}
		}

	}

	@Override
	public void onFail(APIsResponse response) {
		if (response.getActionTy().equals(APIsManager.SR_ENQ_NP)) {
			ApptCra mApptCra;
			try {
				mApptCra = (ApptCra)response.getCra();
			} catch (Exception e) {
				mApptCra = null;
			}
			if (mApptCra != null) {
				apptCra.setReply(mApptCra.getReply());
				apptCra.setServerTS(mApptCra.getServerTS());
				callback_main.setApptCra(apptCra.copyMe());
				if (apptCra.getReply().getCode().equals(RC.USRB_PENDSR_FND)) {
					// RC_USRB_PENDSR_FND
					// Have Pending SR
					doneEnquireSR();
				} else if (!"".equals(response.getMessage()) && response.getMessage() != null) {
					DialogHelper.createSimpleDialog(getActivity(),  response.getMessage());
				} else if (RC.TCSESS_MM.equalsIgnoreCase(response.getReply().getCode())) {
					BaseActivity.ivSessDialog();
				} else {
					DialogHelper.createSimpleDialog(getActivity(),  ClnEnv.getRPCErrMsg(getActivity(), response.getReply().getCode()));
				}
			} else {
				// General Error Message
				if (!"".equals(response.getMessage()) && response.getMessage() != null) {
					DialogHelper.createSimpleDialog(getActivity(),  response.getMessage());
				} else if (RC.TCSESS_MM.equalsIgnoreCase(response.getReply().getCode())) {
					BaseActivity.ivSessDialog();
				} else {
					DialogHelper.createSimpleDialog(getActivity(),  ClnEnv.getRPCErrMsg(getActivity(), response.getReply().getCode()));
				}
			}

		}
	}

	//Private Methods
	private void isToDimLineTestButton(boolean isServiceRunning) {

		if(isServiceRunning) {
			//disable
			//aq.id(R.id.linetest_st_btn).getButton().setEnabled(false) ;
			aq.id(R.id.linetest_st_btn).getButton().setAlpha(0.2f);
		} else {
			//aq.id(R.id.linetest_st_btn).getButton().setEnabled(true) ;
			Utils.clearLnttService(getActivity().getApplicationContext());
			aq.id(R.id.linetest_st_btn).getButton().setAlpha(1);
		}
	}

	ClickableSpan CHECK_PC_SETTING = new ClickableSpan() {
		@Override
		public void onClick(View v) {
			navigateToBrowser(Constant.URL_PROD_CHECK_PC_SETTING);
		}

		@Override
		public void updateDrawState(TextPaint ds) {
			super.updateDrawState(ds);
			ds.setColor(getResources().getColor(R.color.hkt_buttonblue));
		}
	};


	ClickableSpan CHECK_EQ_CONNECTION = new ClickableSpan() {
		@Override
		public void onClick(View v) {
			navigateToBrowser(Constant.URL_PROD_CHECK_EQ_CONNECTION);
		}

		@Override
		public void updateDrawState(TextPaint ds) {
			super.updateDrawState(ds);
			ds.setColor(getResources().getColor(R.color.hkt_buttonblue));
		}
	};

	ClickableSpan CHECK_WIFI_TIPS = new ClickableSpan() {
		@Override
		public void onClick(View v) {
			navigateToBrowser(Constant.URL_PROD_CHECK_WIFI_TIPS);
		}

		@Override
		public void updateDrawState(TextPaint ds) {
			super.updateDrawState(ds);
			ds.setColor(getResources().getColor(R.color.hkt_buttonblue));
		}
	};

	private void navigateToBrowser(String url) {
		Intent i = new Intent(Intent.ACTION_VIEW);
		i.setData(Uri.parse(url));
		startActivity(i);
		getActivity().overridePendingTransition(R.anim.right_slide_in, R.anim.left_slide_out);
	}

}
