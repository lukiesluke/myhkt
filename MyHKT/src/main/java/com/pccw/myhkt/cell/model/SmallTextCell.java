package com.pccw.myhkt.cell.model;

import com.pccw.myhkt.R;

import android.util.Log;
import android.view.Gravity;

public class SmallTextCell extends Cell{

	private int titleGravity = Gravity.LEFT|Gravity.CENTER;
	
	


	public SmallTextCell(String title, String content) {
		super();
		type = Cell.SMALLTEXT1;
		this.title = title;
		this.content = content;
		titleColorId = R.color.hkt_txtcolor_grey;
		contentColorId = R.color.hkt_textcolor;
		titleSizeDelta = 0;
		contentSizeDelta = 0;	
		isArrowShown = false;
	}
	
	public SmallTextCell(String title, String content, int titleColor) {
		super();
		type = Cell.SMALLTEXT1;
		this.title = title;
		this.content = content;
		titleColorId = titleColor;
		contentColorId = R.color.hkt_textcolor;
		titleSizeDelta = 0;
		contentSizeDelta = 0;	
		isArrowShown = false;
	}

	public Boolean getIsArrowShown() {
		return isArrowShown;
	}

	public void setIsArrowShown(Boolean isArrowShown) {
		this.isArrowShown = isArrowShown;
	}
	
	public int getTitleGravity() {
		return titleGravity;
	}

	public void setTitleGravity(int titleGravity) {
		this.titleGravity = titleGravity;
	}
}