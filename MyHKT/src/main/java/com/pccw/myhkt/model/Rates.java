package com.pccw.myhkt.model;

import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

import java.util.ArrayList;

@Root(name = "rates", strict = false)
public class Rates {

    @ElementList(name = "Rate", inline = true, required = false)
    public ArrayList<Rate> rateList = new ArrayList<Rate>();

    public ArrayList<Rate> getRateList() {
        return rateList;
    }
}
