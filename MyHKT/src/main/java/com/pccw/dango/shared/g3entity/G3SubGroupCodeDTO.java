package com.pccw.dango.shared.g3entity;

import java.io.Serializable;

public class G3SubGroupCodeDTO implements Serializable 
{
    private static final long serialVersionUID = -6638178114572545287L;
    
    private String[] groupCodeList;
    private String groupExist;
    
	public String[] getGroupCodeList() {
		return groupCodeList;
	}
	public void setGroupCodeList(String[] groupCodeList) {
		this.groupCodeList = groupCodeList;
	}
	public String getGroupExist() {
		return groupExist;
	}
	public void setGroupExist(String groupExist) {
		this.groupExist = groupExist;
	}
}
