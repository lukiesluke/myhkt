package com.pccw.myhkt.activity;

import static com.pccw.myhkt.APIsManager.WALLET_EW_JWT;
import static com.pccw.myhkt.Utils.RESULT_RE_LOGIN;
import static com.pccw.myhkt.util.Constant.ACTION_BROADCAST_CONFIG_FINISH_DOWNLOAD;
import static com.pccw.myhkt.util.Constant.ACTION_BROADCAST_IMAGE_FINISH_DOWNLOAD;
import static com.pccw.myhkt.util.Constant.KEY_SHARED_APP_CONFIG;
import static com.pccw.myhkt.util.Constant.REQUEST_ACCOUNT_DELETE;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.PendingIntent;
import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.Point;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.Display;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.FrameLayout;

import androidx.annotation.ColorInt;
import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentTransaction;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.crashlytics.FirebaseCrashlytics;
import com.google.gson.Gson;
import com.jeremyfeinstein.slidingmenu.lib.SlidingMenu;
import com.pccw.dango.shared.cra.InbxCra;
import com.pccw.dango.shared.cra.LgiCra;
import com.pccw.dango.shared.cra.WalletCra;
import com.pccw.dango.shared.entity.SubnRec;
import com.pccw.dango.shared.entity.SveeRec;
import com.pccw.dango.shared.tool.MyTool;
import com.pccw.dango.shared.tool.RC;
import com.pccw.myhkt.APIsManager;
import com.pccw.myhkt.APIsManager.OnAPIsListener;
import com.pccw.myhkt.APIsResponse;
import com.pccw.myhkt.ClnEnv;
import com.pccw.myhkt.DialogHelper;
import com.pccw.myhkt.FAWrapper;
import com.pccw.myhkt.GlobalDialog;
import com.pccw.myhkt.LiveChatHelper;
import com.pccw.myhkt.R;
import com.pccw.myhkt.SaveAccountHelper;
import com.pccw.myhkt.Utils;
import com.pccw.myhkt.adapter.MainMenuGridViewAdapter;
import com.pccw.myhkt.fragment.SlidingMenuFragment;
import com.pccw.myhkt.lib.ui.AAQuery;
import com.pccw.myhkt.listeners.AppConfigListener;
import com.pccw.myhkt.listeners.OnLogoutListener;
import com.pccw.myhkt.model.AppConfig;
import com.pccw.myhkt.model.HomeButtonItem;
import com.pccw.myhkt.model.HomeButtonItem.MAINMENU;
import com.pccw.myhkt.model.LnttAgent;
import com.pccw.myhkt.mymob.activity.MyMobileActivity;
import com.pccw.myhkt.service.InboxMessageIntentService;
import com.pccw.myhkt.service.VersionCheckService;
import com.pccw.myhkt.util.Constant;
import com.pccw.myhkt.util.RuntimePermissionUtil;
import com.pccw.wheat.shared.tool.Reply;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

/************************************************************************
 * File : MainMenu.java
 * Desc : Screen 4.1 - Main Menu 
 * Name : Main Menu Created
 * by 	: Vincent Fung 
 * Date : 14/10/2015
 * <p>
 * Change History:
 * Date       Modified By		Description
 * ---------- ----------------	-------------------------------
 * 14/10/2015 Vincent Fung		- First draft
 * 30/10/2015 Andy Wong			- Change the home menu to grid view	
 * 26/11/2015 Derek Tsui		- Navbar and BaseActivity handling
 * 29/03/2016 Andy Wong			- To support status bar color, use activity + create sliding menu instead of slidingmenuactivity
 * 30/05/2017 Abdulmoiz Esmail  - Comment line 454 and 455 to remove the non premier buttons in the main menu
 * 01/06/2017 Abdulmoiz Esmail  - Repositioned the Non-premier menu buttons, update the SwitchApptIcon, set the index to 2. 
 * 29/09/2017 Abdulmoiz Esmail  - Added onclick on the banner image and navigate to browser with the saved URL from appconfig
 * 06/11/2017 Abdulmoiz Esmail  - Added new banner for Touch ID
 * 08/03/2017 Abdulmoiz Esmail  - Added broadcast receiver for ro reload the image banner after finish download
 *************************************************************************/
public class MainMenuActivity extends FragmentActivity implements OnAPIsListener, AppConfigListener, OnLogoutListener {
    private AAQuery aq;
    private SaveAccountHelper saveAccountHelper; // SQLite database for a new bill checking
    private boolean isActivityActive = false;
    private Calendar cal;
    public static MainMenuActivity me;
    private final int colMaxNum = 4;
    private List<HomeButtonItem> homeItems;
    private int extralinespace;
    private int basePadding;
    private int logoPadding;
    private int gridlayoutPadding = 0;
    private int deviceWidth = 0;
    private int colWidth = 0;
    private Activity act;
    protected Boolean isZh = false;
    private boolean isCheckedLnttResult = false;
    // FrameLayout as Container for activity
    protected FrameLayout frameContainer;
    // SlideMenu
    protected SlidingMenuFragment menuFrag;
    public SlidingMenu slidingmenu;
    private MainMenuGridViewAdapter mainMenuGridViewAdapter;
    private String moduleId;
    private Boolean isPremier = true;
    private androidx.appcompat.app.AlertDialog apptDialog;
    final int PERMISSION_REQUEST_CODE = 112;
    @ColorInt
    private int transparent;
    @ColorInt
    private int menuColorWhite;
    private Gson gson;
    private AppConfig appConfig;
    private String TAG = MainMenuActivity.class.getSimpleName();
    private String appConfigString;

    @Override
    public final void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        aq = new AAQuery(this);
        gson = new Gson();
        APIsManager.requestAppConfigJSON(this, this);

        transparent = ContextCompat.getColor(this, R.color.transparent);
        menuColorWhite = ContextCompat.getColor(this, R.color.menu_txt_color);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_HARDWARE_ACCELERATED, WindowManager.LayoutParams.FLAG_HARDWARE_ACCELERATED);
        initData();

        moduleId = getResources().getString(R.string.MODULE_HOME);
        ClnEnv.updateUILocale(getBaseContext(), ClnEnv.getAppLocale(getBaseContext()));
        isPremier = ClnEnv.getSessionPremierFlag();

        setContentView(!isPremier ? R.layout.activity_mainmenu : R.layout.activity_mainmenu_prem);

        if (Build.VERSION.SDK_INT > 32) {
            if (!shouldShowRequestPermissionRationale(String.valueOf(PERMISSION_REQUEST_CODE))) {
                getNotificationPermission();
            }
        } else {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_PHONE_STATE}, PERMISSION_REQUEST_CODE);
        }

        setStatusBarColor();
        initSlidingMenu();
        checkIfUAT();

        appConfigSetting();
    }

    public void getNotificationPermission() {
        try {
            if (Build.VERSION.SDK_INT > 32) {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.POST_NOTIFICATIONS, Manifest.permission.READ_PHONE_STATE}, PERMISSION_REQUEST_CODE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // allow
                }
        }
    }

    private void checkIfUAT() {
        if (ClnEnv.getPref(me, getString(R.string.CONST_PREF_DOMAIN), APIsManager.PRD_domain).equals(APIsManager.UAT_domain)) {
            FAWrapper.getInstance().sendFAEvents(me, Constant.USER_TYPE_UAT);
            FAWrapper.getInstance().setUATProperty(me);
        } else {
            FAWrapper.getInstance().sendFAEvents(me, Constant.USER_TYPE_PRD);
            FAWrapper.getInstance().setPRDProperty(me);
        }
    }

    @Override
    public void setContentView(int layoutResID) {
        if (frameContainer != null) {
            frameContainer.removeAllViews();
        }
        frameContainer = new FrameLayout(this);
        LayoutParams layoutParam = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
        frameContainer.addView(getLayoutInflater().inflate(layoutResID, null), layoutParam);
        super.setContentView(frameContainer, layoutParam);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        ClnEnv.updateUILocale(getBaseContext(), ClnEnv.getAppLocale(getBaseContext()));
    }

    @Override
    protected void onStart() {
        super.onStart();
        isActivityActive = true;
        appConfigSetting();

        // Set MyMobileFlag false
        ClnEnv.setMyMobFlag(false);

        // Clear lineTest Data if loginID is not matched before autoLogin
        // if not matched, stop to auto-Login
        if (shouldAutoLogin() && ClnEnv.getPref(me.getApplicationContext(), me.getString(R.string.CONST_PREF_BILLMSG_NOTICEFLAG), false) || shouldAutoLogin()) {
            if (!ClnEnv.isLoggedIn() && !ClnEnv.getSessionLoginID().isEmpty() && !ClnEnv.getSessionPassword().isEmpty() && !Utils.isTouchIDLoginActivated(getApplicationContext())) {
                SveeRec sveeRec = new SveeRec();
                sveeRec.loginId = ClnEnv.getSessionLoginID();
                sveeRec.pwd = ClnEnv.getSessionPassword();
                LgiCra lgiCra = new LgiCra();
                lgiCra.setISveeRec(sveeRec);
                APIsManager.doLoginTest(this, lgiCra, ClnEnv.getPref(me.getApplicationContext(), me.getString(R.string.CONST_PREF_SAVEPASSWORD), false));
            }
        } else {
            if ((ClnEnv.getPushDataBill() != null && ClnEnv.getPushDataBill().isAppActive()) || !Utils.isTouchIDLoginActivated(getApplicationContext())) {
                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    public void run() {
                        if (ClnEnv.getPushDataBill() != null && ClnEnv.getPushDataBill().getType().equals("N")) {
                            Utils.checkBillMsgAndRedirect(me, false);
                        } else if (ClnEnv.getPushDataBill() != null && ClnEnv.getPushDataBill().getType().equals("B")) {
                            checkBillMsgAndRedirect();
                        }
                    }
                }, 200);
            }
        }

        isCheckedLnttResult = true;
        checkLnttResultAndRedirect();
        checkGenMsg();
    }

    public boolean shouldAutoLogin() {
        return !ClnEnv.isLoggedIn() && !ClnEnv.getSessionLoginID().isEmpty() && !ClnEnv.getSessionPassword().isEmpty() && !Utils.isTouchIDLoginActivated(getApplicationContext());
    }

    @Override
    protected final void onResume() {
        super.onResume();
        isActivityActive = true;
        initUI();
        // Update Locale
        ClnEnv.updateUILocale(getBaseContext(), ClnEnv.getAppLocale(getBaseContext()));
        setLiveChangeIcon(LiveChatHelper.getInstance(me, me).isPause);

        // When user stay at MainMenu receiving LineTest Result,
        // it does not run onStart, so check LineTest result at onResume
        if (!isCheckedLnttResult) {
            checkLnttResultAndRedirect();
        }

        isCheckedLnttResult = false;
        updateLoginStatus();

        // Start Version Check first
        Intent mVersionCheckerService = new Intent(this, VersionCheckService.class);
        mVersionCheckerService.setPackage(getPackageName());

        if (!RuntimePermissionUtil.isServiceRunning(getApplicationContext(), VersionCheckService.class)) {
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.S) {
                PendingIntent.getActivity(this, 0, mVersionCheckerService, PendingIntent.FLAG_IMMUTABLE | PendingIntent.FLAG_UPDATE_CURRENT);
            } else {
                PendingIntent.getActivity(this, 0, mVersionCheckerService, PendingIntent.FLAG_UPDATE_CURRENT);
            }
        } else {
            Intent broadCastIntent = new Intent();
            broadCastIntent.setAction(Constant.ACTION_CHECK_APP_VERSION);
            broadCastIntent.setPackage(getPackageName());
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.S) {
                PendingIntent.getBroadcast(this, 0, broadCastIntent, PendingIntent.FLAG_IMMUTABLE | PendingIntent.FLAG_UPDATE_CURRENT);
            } else {
                PendingIntent.getBroadcast(this, 0, broadCastIntent, PendingIntent.FLAG_CANCEL_CURRENT | PendingIntent.FLAG_IMMUTABLE);
            }
            sendBroadcast(broadCastIntent);
        }
        //Screen Tracker
        FAWrapper.getInstance().sendFAScreen(me, R.string.CONST_SCRN_MAINMENU, false);
    }

    private void setMenuButtonFilterBlue() {
        ColorFilter filter = new PorterDuffColorFilter(Color.parseColor("#217EC0"), PorterDuff.Mode.SRC_ATOP);
        aq.id(R.id.navbar_button_left).getImageView().setColorFilter(filter);
    }

    private void setMenuButtonFilterWhite() {
        ColorFilter filter = new PorterDuffColorFilter(menuColorWhite, PorterDuff.Mode.SRC_ATOP);
        aq.id(R.id.navbar_button_left).getImageView().setColorFilter(filter);
    }

    @Override
    protected final void onPause() {
        isActivityActive = false;
        super.onPause();
    }

    @Override
    protected final void onDestroy() {
        super.onDestroy();
        try {
            if (broadcastReceiver != null) {
                this.unregisterReceiver(broadcastReceiver);
            }
        } catch (Exception e) {
            // lineTestBroadcastReceiver is already unregistered
            broadcastReceiver = null;
        }
    }

    //----------------------------------------------------------------------------------------------
    //                              UI Initialization methods
    //----------------------------------------------------------------------------------------------
    public void setStatusBarColor() {
        //Status Bar Color
        Window window = getWindow();
        window.getDecorView().setFitsSystemWindows(false);
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.setStatusBarColor(getResources().getColor(ClnEnv.getSessionPremierFlag() ? R.color.hkt_headingpremier : R.color.hkt_textcolor));
    }

    public void initData() {
        DisplayMetrics metrics = new DisplayMetrics();
        WindowManager wm = (WindowManager) getSystemService(Context.WINDOW_SERVICE);
        wm.getDefaultDisplay().getMetrics(metrics);
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        deviceWidth = size.x;
        gridlayoutPadding = (int) getResources().getDimension(R.dimen.mainmenu_gridlayout_padding);
        extralinespace = (int) getResources().getDimension(R.dimen.extralinespace);
        basePadding = (int) getResources().getDimension(R.dimen.basePadding);
        logoPadding = (int) getResources().getDimension(R.dimen.reg_logo_padding_1);
        colWidth = (deviceWidth - extralinespace * 2 - (colMaxNum - 1) * gridlayoutPadding) / colMaxNum;
        act = this;
        me = MainMenuActivity.this;
    }

    private void setPremierLayout() {
        setMenuButtonFilterWhite();

        homeItems = new ArrayList<>();
        int textSize = (int) getResources().getDimension(R.dimen.smalltext1size);

        homeItems.add(new HomeButtonItem(R.drawable.home_btn_account_premier, getString(R.string.MYHKT_HOME_BTN_ACCT), textSize, transparent, menuColorWhite, MAINMENU.MYACCT));
        homeItems.add(new HomeButtonItem(R.drawable.icon_premier_linetest, getString(R.string.MYHKT_HOME_BTN_LNTT), textSize, transparent, menuColorWhite, MAINMENU.MYLINETEST));
        homeItems.add(new HomeButtonItem(R.drawable.home_btn_appt_premier, getString(R.string.MYHKT_HOME_BTN_APPT), textSize, transparent, menuColorWhite, MAINMENU.MYAPPT));
        homeItems.add(new HomeButtonItem(R.drawable.premier_e_wallet, getString(R.string.MYHKT_HOME_BTN_WALLET), textSize, transparent, menuColorWhite, MAINMENU.MYWALLET));
        homeItems.add(new HomeButtonItem(R.drawable.home_btn_profile_premier, getString(R.string.MYHKT_HOME_BTN_PROFILE), textSize, transparent, menuColorWhite, MAINMENU.MYPROFILE));
        homeItems.add(new HomeButtonItem(R.drawable.home_btn_mymobile_premier, getString(R.string.MYHKT_HOME_BTN_MYMOB), textSize, transparent, menuColorWhite, MAINMENU.MYMOB));
        homeItems.add(new HomeButtonItem(R.drawable.home_btn_dq_premier, getString(R.string.MYHKT_HOME_BTN_DQ), textSize, transparent, menuColorWhite, MAINMENU.DIRECTINQ));
        homeItems.add(new HomeButtonItem(R.drawable.home_btn_idd_premier, getString(R.string.MYHKT_HOME_BTN_IDD), textSize, transparent, menuColorWhite, MAINMENU.IDD));
        homeItems.add(new HomeButtonItem(R.drawable.home_btn_tap_go_premier, getString(R.string.MYHKT_HOME_BTN_TAPNGO), textSize, transparent, menuColorWhite, MAINMENU.TAPNGO));
        homeItems.add(new HomeButtonItem(R.drawable.home_btn_club_premier, getString(R.string.MYHKT_HOME_BTN_CLUB), textSize, transparent, menuColorWhite, MAINMENU.THECLUB));
        homeItems.add(new HomeButtonItem(R.drawable.home_btn_shop_premier, getString(R.string.MYHKT_HOME_BTN_SHOP), textSize, transparent, menuColorWhite, MAINMENU.SHOPS));
        homeItems.add(new HomeButtonItem(R.drawable.home_btn_livechat_premier, getString(R.string.MYHKT_HOME_BTN_CONTACT), textSize, transparent, menuColorWhite, MAINMENU.CONTACTUS));
        homeItems.add(new HomeButtonItem(R.drawable.home_btn_ar_lens, getString(R.string.MYHKT_HOME_BTN_AR_LENS), textSize, transparent, menuColorWhite, MAINMENU.AR_LENS));
        homeItems.add(new HomeButtonItem(R.drawable.home_china_stategy, getString(R.string.MYHKT_HOME_BTN_TRAVEL_GUIDE), textSize, transparent, menuColorWhite, MAINMENU.TRAVEL_GUIDE));
        homeItems.add(new HomeButtonItem(R.drawable.home_travel_zone, getString(R.string.MYHKT_HOME_BTN_TRAVEL_ZONE), textSize, transparent, menuColorWhite, MAINMENU.TRAVEL_ZONE));
        homeItems.add(new HomeButtonItem(R.drawable.home_drgo, getString(R.string.MYHKT_HOME_BTN_DRGO), textSize, transparent, menuColorWhite, MAINMENU.DRGO));
        aq.id(R.id.mainmenu_label_welcome).getTextView().setPadding(0, logoPadding, 0, logoPadding);
        aq.id(R.id.mainmenu_label_welcome).textColorId(R.color.hkt_txtcolor_premier);
        aq.id(R.id.mainmenu_label_welcome).getTextView().setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.header3size));

        mainMenuGridViewAdapter = new MainMenuGridViewAdapter(this, homeItems, colWidth);
        aq.id(R.id.mainmenu_gridlayout).getGridView().setNumColumns(colMaxNum);
        aq.id(R.id.mainmenu_gridlayout).getGridView().setHorizontalSpacing(gridlayoutPadding);
        aq.id(R.id.mainmenu_gridlayout).getGridView().setVerticalSpacing(gridlayoutPadding);
        aq.id(R.id.mainmenu_gridlayout).adapter(mainMenuGridViewAdapter).itemClicked(this, "onHomeBtnClick");

        //Bot bar
        aq.id(R.id.mainmenu_pccwlogo1).getImageView().setImageDrawable(ContextCompat.getDrawable(this, R.drawable.hktlogo));
        aq.id(R.id.mainmenu_pccwlogo).getImageView().setImageDrawable(ContextCompat.getDrawable(this, R.drawable.pccwlogo));

        setTopBannerPremierConfiguration();
    }

    private void setNonPremierLayout() {
        // Init menu buttons
        homeItems = new ArrayList<>();
        int textSize = (int) getResources().getDimension(R.dimen.smalltext1size);
        homeItems.add(new HomeButtonItem(R.drawable.myaccount, getString(R.string.MYHKT_HOME_BTN_ACCT), textSize, Color.parseColor("#00417B"), menuColorWhite, MAINMENU.MYACCT));
        homeItems.add(new HomeButtonItem(R.drawable.icon_customer_linetest, getString(R.string.MYHKT_HOME_BTN_LNTT), textSize, Color.parseColor("#6C4C8C"), menuColorWhite, MAINMENU.MYLINETEST));
        homeItems.add(new HomeButtonItem(R.drawable.myappointment, getString(R.string.MYHKT_HOME_BTN_APPT), textSize, Color.parseColor("#47ADF3"), menuColorWhite, MAINMENU.MYAPPT));
        homeItems.add(new HomeButtonItem(R.drawable.e_wallet, getString(R.string.MYHKT_HOME_BTN_WALLET), textSize, Color.parseColor("#187d8c"), menuColorWhite, MAINMENU.MYWALLET));
        homeItems.add(new HomeButtonItem(R.drawable.myprofile, getString(R.string.MYHKT_HOME_BTN_PROFILE), textSize, Color.parseColor("#303E5F"), menuColorWhite, MAINMENU.MYPROFILE));
        homeItems.add(new HomeButtonItem(R.drawable.mymobile, getString(R.string.MYHKT_HOME_BTN_MYMOB), textSize, Color.parseColor("#EF9A0C"), menuColorWhite, MAINMENU.MYMOB));
        homeItems.add(new HomeButtonItem(R.drawable.dq, getString(R.string.MYHKT_HOME_BTN_DQ), textSize, Color.parseColor("#5E7BCB"), menuColorWhite, MAINMENU.DIRECTINQ));
        homeItems.add(new HomeButtonItem(R.drawable.idd, getString(R.string.MYHKT_HOME_BTN_IDD), textSize, Color.parseColor("#2273BC"), menuColorWhite, MAINMENU.IDD));
        homeItems.add(new HomeButtonItem(R.drawable.tab_go, getString(R.string.MYHKT_HOME_BTN_TAPNGO), textSize, Color.parseColor("#FF793F"), menuColorWhite, MAINMENU.TAPNGO));
        homeItems.add(new HomeButtonItem(R.drawable.icon_theclub, getString(R.string.MYHKT_HOME_BTN_CLUB), textSize, Color.parseColor("#000000"), menuColorWhite, MAINMENU.THECLUB));
        homeItems.add(new HomeButtonItem(R.drawable.shop, getString(R.string.MYHKT_HOME_BTN_SHOP), textSize, Color.parseColor("#52708A"), menuColorWhite, MAINMENU.SHOPS));
        homeItems.add(new HomeButtonItem(R.drawable.contactus, getString(R.string.MYHKT_HOME_BTN_CONTACT), textSize, Color.parseColor("#0693B7"), menuColorWhite, MAINMENU.CONTACTUS));
        homeItems.add(new HomeButtonItem(R.drawable.home_ar_lens_logo, getString(R.string.MYHKT_HOME_BTN_AR_LENS), textSize, Color.parseColor("#9C767474"), menuColorWhite, MAINMENU.AR_LENS));
        homeItems.add(new HomeButtonItem(R.drawable.home_travel_guide_icon, getString(R.string.MYHKT_HOME_BTN_TRAVEL_GUIDE), textSize, Color.parseColor("#9C767474"), menuColorWhite, MAINMENU.TRAVEL_GUIDE));
        homeItems.add(new HomeButtonItem(R.drawable.home_travel_zone_icon, getString(R.string.MYHKT_HOME_BTN_TRAVEL_ZONE), textSize, Color.parseColor("#9C767474"), menuColorWhite, MAINMENU.TRAVEL_ZONE));
        homeItems.add(new HomeButtonItem(R.drawable.home_drgo_logo, getString(R.string.MYHKT_HOME_BTN_DRGO), textSize, Color.parseColor("#9C767474"), menuColorWhite, MAINMENU.DRGO));
        aq.id(R.id.mainmenu_label_welcome).getTextView().setPadding(0, logoPadding, 0, logoPadding);
        aq.id(R.id.mainmenu_label_welcome).textColorId(R.color.hkt_txtcolor_grey);
        aq.id(R.id.mainmenu_label_welcome).getTextView().setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.header3size));

        mainMenuGridViewAdapter = new MainMenuGridViewAdapter(this, homeItems, colWidth);
        aq.id(R.id.mainmenu_gridlayout).getGridView().setNumColumns(colMaxNum);
        aq.id(R.id.mainmenu_gridlayout).getGridView().setHorizontalSpacing(gridlayoutPadding);
        aq.id(R.id.mainmenu_gridlayout).getGridView().setVerticalSpacing(gridlayoutPadding);
        aq.id(R.id.mainmenu_gridlayout).adapter(mainMenuGridViewAdapter).itemClicked(this, "onHomeBtnClick");

        setTapBannerConfiguration();
    }

    private void setTopBannerPremierConfiguration() {
        //Set Banner to match the device width with same ratio
        Drawable drawable = ContextCompat.getDrawable(this, R.drawable.top_banner_premier);

        int drawableWidth = Objects.requireNonNull(drawable).getIntrinsicWidth();
        int drawableHeight = drawable.getIntrinsicHeight();
        int bannerWidth = deviceWidth;
        int bannerHeight = (int) ((float) drawableHeight * (float) bannerWidth / (float) drawableWidth);
        aq.id(R.id.main_top_banner_premier).height(bannerHeight, false);
        aq.id(R.id.main_top_banner_premier).width(bannerWidth, false);
        aq.id(R.id.main_top_banner_premier).clicked(this, "onClick");

        File imgFile;
        imgFile = new File(getFilesDir().getPath() + "/images/" + ClnEnv.getAppLocale(getBaseContext()) + "/after_login_banner_pt.png");
        aq.id(R.id.main_top_banner_premier).getImageView().setImageURI(null);
        if (imgFile.exists()) {
            aq.id(R.id.main_top_banner_premier).getImageView().setImageURI(Uri.fromFile(imgFile));
        } else {
            aq.id(R.id.main_top_banner_premier).getImageView().setImageDrawable(drawable);
        }
        Log.d("lwg", "setTopBannerPremierConfiguration");
    }

    private void setTapBannerConfiguration() {
        setMenuButtonFilterBlue();

        //Set Banner to match the device width with same ratio
        Drawable drawable = (Utils.isToShowTouchIdBanner(this) ? (isZh ? ContextCompat.getDrawable(this, R.drawable.fingerprint_banner_zh) : ContextCompat.getDrawable(this, R.drawable.fingerprint_banner_en)) : ContextCompat.getDrawable(this, R.drawable.menu_banner));

        int drawableWidth = Objects.requireNonNull(drawable).getIntrinsicWidth();
        int drawableHeight = drawable.getIntrinsicHeight();
        int bannerWidth = deviceWidth - (int) getResources().getDimension(R.dimen.basePadding) * 2;
        int bannerHeight = (int) ((float) drawableHeight * (float) bannerWidth / (float) drawableWidth);
        aq.id(R.id.mainmenu_topbanner).height(bannerHeight, false);
        aq.id(R.id.mainmenu_topbanner).width(bannerWidth, false);

        File imgFile;
        aq.id(R.id.mainmenu_topbanner).getImageView().setImageURI(null);

        if (Utils.isToShowTouchIdBanner(this)) {
            imgFile = new File(getFilesDir().getPath() + "/images/" + ClnEnv.getAppLocale(getBaseContext()) + "/fp_top_banner.png");
            if (imgFile.exists()) {
                aq.id(R.id.mainmenu_topbanner).getImageView().setImageURI(Uri.fromFile(imgFile));
            } else {
                aq.id(R.id.mainmenu_topbanner).getImageView().setImageDrawable(drawable);
            }
        } else {
            if (ClnEnv.isLoggedIn()) {
                imgFile = new File(getFilesDir().getPath() + "/images/" + ClnEnv.getAppLocale(getBaseContext()) + "/after_login_banner.png");
            } else {
                imgFile = new File(getFilesDir().getPath() + "/images/" + ClnEnv.getAppLocale(getBaseContext()) + "/main_top_banner.png");
            }
            if (imgFile.exists()) {
                aq.id(R.id.mainmenu_topbanner).getImageView().setImageURI(Uri.fromFile(imgFile));
            } else {
                aq.id(R.id.mainmenu_topbanner).getImageView().setImageDrawable(drawable);
            }
        }
        aq.id(R.id.mainmenu_topbanner).clicked(this, "onClick");

        //Bot bar
        aq.id(R.id.mainmenu_pccwlogo1).getImageView().setImageDrawable(ContextCompat.getDrawable(this, R.drawable.hktlogo));
        aq.id(R.id.mainmenu_pccwlogo).getImageView().setImageDrawable(ContextCompat.getDrawable(this, R.drawable.pccwlogo));
        Log.d("lwg", "setTapBannerConfiguration");
    }

    @SuppressLint("UnspecifiedRegisterReceiverFlag")
    private void initUI() {
        // Download appConfig to update banner
        aq = new AAQuery(this);
        if (isPremier != ClnEnv.getSessionPremierFlag()) {
            isPremier = ClnEnv.getSessionPremierFlag();
            setStatusBarColor();
            setContentView(!isPremier ? R.layout.activity_mainmenu : R.layout.activity_mainmenu_prem);

            slidingmenu.attachToActivity(this, SlidingMenu.SLIDING_CONTENT);
            slidingmenu.setMenu(R.layout.menu_frame);
            // set Background Color to prevent white color edge problem when opening Menu instant
            slidingmenu.setBackgroundColor(getResources().getColor(ClnEnv.getSessionPremierFlag() ? R.color.menu_bg_premier : R.color.menu_bg_grey));
            //Initialize slidingMenu
            FragmentTransaction transaction = this.getSupportFragmentManager().beginTransaction();
            menuFrag = new SlidingMenuFragment();
            transaction.replace(R.id.menu_frame, menuFrag);
            transaction.commit();
        } else {
            if (menuFrag != null) {
                menuFrag.refreshView();
            }
        }

        //navbar style
        aq.navBarBaseLayout(R.id.navbar_base_layout);
        aq.navBarTitle(R.id.navbar_title, getResString(R.string.myhkt_about_appname));
        aq.navBarButton(R.id.navbar_button_left, R.drawable.btn_slidemenu);

        setLiveChangeIcon(false);

        aq.id(R.id.navbar_button_left).clicked(this, "onClick");
        aq.id(R.id.navbar_button_right).clicked(this, "onClick");

        isZh = "zh".equalsIgnoreCase(this.getResources().getString(R.string.myhkt_lang));
        //init menu buttons
        if (this.isPremier) {
            setPremierLayout();
        } else {
            setNonPremierLayout();
        }

        IntentFilter filter = new IntentFilter();
        filter.addAction(ACTION_BROADCAST_IMAGE_FINISH_DOWNLOAD);
        filter.addAction(ACTION_BROADCAST_CONFIG_FINISH_DOWNLOAD);
        filter.addAction(Constant.ACTION_INBOX_LIST_UPDATED);
        filter.addAction(Constant.ACTION_CACHED_APPT_RESPONSE);
        filter.addAction(Constant.ACTION_SESSION_TIMEOUT);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
            registerReceiver(broadcastReceiver, filter, Context.RECEIVER_NOT_EXPORTED);
        } else {
            registerReceiver(broadcastReceiver, filter);
        }
        updateLoginStatus();

        //switch the appointment icon according to flag from login
        if (!ClnEnv.isLoggedIn()) {
            me.SwitchApptIcon(false);
        } else {
            me.SwitchApptIcon(ClnEnv.getPref(getApplicationContext(), getString(R.string.CONST_PREF_APPTIND_FLAG).concat("-").concat(String.valueOf(ClnEnv.getLoginId())), false));
        }
        String tapNGo = getString(R.string.MYHKT_HOME_REMARK_1, "\n", "\n");
        if (Utils.getScreenSize(getApplicationContext()).equals(Constant.SCREEN_LARGE)) {
            tapNGo = getString(R.string.MYHKT_HOME_REMARK_1, "", "\n");
            aq.id(R.id.mainmenu_logobar).height(80);
            aq.id(R.id.mainmenu_logobar).getView().setPadding(0, 30, 0, 5);

            aq.id(R.id.mainmenu_pccwlogo).getImageView().setPadding(0, 15, 0, 15);
            aq.id(R.id.mainmenu_pccwlogo1).getImageView().setPadding(0, 15, 0, 15);
        }
        aq.id(R.id.tv_footer_label).text(tapNGo);

        Log.d("lwg", "InitUI local: " + ClnEnv.getAppLocale(getBaseContext()));
        // OneTimeWorkRequest work = new OneTimeWorkRequest.Builder(MyWorker.class).build();
        // WorkManager.getInstance(this).beginWith(work).enqueue();
    }

    public String getResString(int res) {
        return Utils.getString(this, res);
    }

    private void initSlidingMenu() {
        slidingmenu = new SlidingMenu(this);
        slidingmenu.setShadowWidth(0);
        slidingmenu.setShadowDrawable(null);
        slidingmenu.setBehindOffset(Utils.dpToPx(85));
        slidingmenu.setTouchModeAbove(SlidingMenu.TOUCHMODE_NONE);
        slidingmenu.setFadeEnabled(false);
        slidingmenu.setBehindScrollScale(0.0f);
        slidingmenu.attachToActivity(this, SlidingMenu.SLIDING_CONTENT);
        slidingmenu.setMenu(R.layout.menu_frame);
        // set Background Color to prevent white color edge problem when opening Menu instant
        slidingmenu.setBackgroundColor(getResources().getColor(ClnEnv.getSessionPremierFlag() ? R.color.menu_bg_premier : R.color.menu_bg_grey));

        //Initialize slidingMenu
        FragmentTransaction transaction = this.getSupportFragmentManager().beginTransaction();
        menuFrag = null;
        menuFrag = new SlidingMenuFragment();
        transaction.replace(R.id.menu_frame, menuFrag);
        transaction.commit();
    }

    //----------------------------------------------------------------------------------------------
    //                              Redirect Methods
    //----------------------------------------------------------------------------------------------

    // Check LineTest Result when launch and relaunch MainMenu
    private void checkLnttResultAndRedirect() {
        // Line Test Result completed, redirect to targetService
        if (ClnEnv.getPref(me.getApplicationContext(), me.getString(R.string.CONST_PREF_LNTT_NOTICEFLAG), false)) {
            LnttAgent lnttAgent = Utils.getPrefLnttAgent(me);
            String endTimestamp = lnttAgent.getEndTimestamp();
            if (!"".equals(lnttAgent.getResultMsg()) && lnttAgent.getResultMsg() != null && (lnttAgent.getLnttSrvNum() == null || lnttAgent.getLnttSrvNum().equals(""))) {
                //Http 404
                if (isActivityActive) {
                    DialogHelper.createSimpleDialog(this, lnttAgent.getResultMsg());
                }
                Utils.clearLnttService(me);
            } else {
                // Clear lineTest Data if loginID is not matched
                String loginID = lnttAgent.getLnttCra().getILoginId();
                if ((!"".equalsIgnoreCase(loginID) && !loginID.equalsIgnoreCase(ClnEnv.getSessionLoginID())) || "".equalsIgnoreCase(ClnEnv.getSessionLoginID())) {
                    DialogHelper.createSimpleDialog(this, getResString(R.string.MYHKT_LT_ERR_ACC_NOT_FOUND));
                    Utils.clearLnttService(me);
                } else if (ClnEnv.isLoggedIn()) {
                    // Check subscriptionRec is valid in current ServiceList
                    boolean isValidSubnRec = false;
                    for (SubnRec rSubnRec : ClnEnv.getAssocSubnRecAry(Objects.requireNonNull(ClnEnv.getQualSvee()).getSubnRecAry())) {
                        if (rSubnRec.srvNum.equalsIgnoreCase(lnttAgent.getLnttSrvNum()) && rSubnRec.acctNum.equalsIgnoreCase(lnttAgent.getLnttCra().getISubnRec().acctNum)) {
                            isValidSubnRec = true;
                        }
                    }
                    if (!isValidSubnRec) {
                        DialogHelper.createSimpleDialog(this, getResString(R.string.MYHKT_LT_ERR_SUBS_NOT_FOUND));
                        Utils.clearLnttService(me);
                    } else if (Utils.isExpiredLnttResult(me, endTimestamp)) {
                        // Check the Line Test Result Expired Time
                        DialogHelper.createSimpleDialog(this, getResString(R.string.MYHKT_LT_ERR_EXPIRED));
                        Utils.clearLnttService(me);
                    } else if (!"".equalsIgnoreCase(lnttAgent.getLnttSrvNum())) {
                        Intent nextIntent = new Intent(getApplicationContext(), ServiceListActivity.class);
                        startActivity(nextIntent);
                    } else {
                        // UnExpected Case, clean data to handle it
                        Utils.clearLnttService(me);
                        ClnEnv.setPref(me.getApplicationContext(), me.getString(R.string.CONST_PREF_LNTT_NOTICEFLAG), false);
                    }
                }
            }
        }
    }

    //Check if bill message exist during Main Menu launch
    private void checkBillMsgAndRedirect() {
        //demo to test the notification steps flow
        if (ClnEnv.getPref(me.getApplicationContext(), me.getString(R.string.CONST_PREF_BILLMSG_NOTICEFLAG), false)) {
            try {
                if (ClnEnv.getPushDataBill() != null) {
                    String loginID = ClnEnv.getPushDataBill().getLoginId();
                    if ((!"".equalsIgnoreCase(loginID) && !ClnEnv.getSessionLoginID().equalsIgnoreCase(loginID)) || "".equalsIgnoreCase(ClnEnv.getSessionLoginID())) {
                        billMsgLoginRedirect();
                    } else if (ClnEnv.isLoggedIn()) {
                        // Check subscriptionRec is valid in current ServiceList
                        boolean isValidSubnRec = false;
                        String serviceNum = "";
                        for (SubnRec rSubnRec : ClnEnv.getAssocSubnRecAry(Objects.requireNonNull(ClnEnv.getQualSvee()).getSubnRecAry())) {
                            if (rSubnRec.acctNum.equalsIgnoreCase(ClnEnv.getPushDataBill().getAcctNum())) {
                                isValidSubnRec = true;
                                int ltsType = 0;

                                //service number for bill dialog
                                if (rSubnRec.lob.equals(SubnRec.LOB_LTS)) {
                                    ltsType = Utils.getLtsSrvType(rSubnRec.tos, rSubnRec.eyeGrp, rSubnRec.priMob);
                                    if (ltsType == R.string.CONST_LTS_CALLINGCARD) {
                                        int last4index = (rSubnRec.srvNum.length() - 4) < 0 ? 0 : rSubnRec.srvNum.length() - 4;
                                        serviceNum = String.format("CARD%s", rSubnRec.srvNum.substring(last4index)) + "\n";
                                    } else {
                                        serviceNum = rSubnRec.srvNum + "\n";
                                    }
                                } else {
                                    serviceNum = rSubnRec.srvNum + "\n";
                                }

                                switch (rSubnRec.lob) {
                                    case SubnRec.LOB_LTS:
                                    case SubnRec.LOB_MOB:
                                    case SubnRec.LOB_101:
                                    case SubnRec.LOB_IOI:
                                    case SubnRec.LOB_O2F:
                                        serviceNum = getString(R.string.LTTF_LTS_ACCTNUM) + " " + serviceNum;
                                        break;
                                    case SubnRec.LOB_TV:
                                        serviceNum = getString(R.string.LTTF_TV_ACCTNUM) + " " + serviceNum;
                                        break;
                                    case SubnRec.LOB_PCD:
                                        serviceNum = getString(R.string.LTTF_PCD_ACCTNUM) + " " + serviceNum;
                                        break;
                                }
                                break;
                            }
                        }
                        if (!isValidSubnRec) {
                            Utils.clearBillMsgService(me);
                            billMsgRedirect(Utils.getString(me, R.string.MYHKT_PN_ERR_SUBS_NOT_FOUND));
                        } else if (!Objects.requireNonNull(ClnEnv.getLoginId()).equalsIgnoreCase(ClnEnv.getPushDataBill().getLoginId())) {
                            ClnEnv.setPref(me.getApplicationContext(), me.getString(R.string.CONST_PREF_BILLMSG_NOTICEFLAG), false);
                            billMsgLoginRedirect();
                        } else {
                            billMsgRedirect(ClnEnv.getPushDataBill().getMessage());
                        }
                    }
                }
            } catch (Exception e) {
                //Prevent throw null pointer exception for BillPushData
                ClnEnv.setPref(me.getApplicationContext(), me.getString(R.string.CONST_PREF_BILLMSG_NOTICEFLAG), false);
            }
        }
    }

    private void checkGenMsg() {
        if (ClnEnv.getPref(me.getApplicationContext(), me.getString(R.string.CONST_PREF_GENMSG_NOTICEFLAG), false)) {
            try {
                if (ClnEnv.getPushDataGen() != null) {
                    DialogHelper.createSimpleDialog(this, ClnEnv.getPushDataGen().getMessage());
                }
            } catch (Exception e) {
                //Prevent throw null pointer exception for BillPushData
                ClnEnv.setPref(me.getApplicationContext(), me.getString(R.string.CONST_PREF_GENMSG_NOTICEFLAG), false);
                return;
            }
            ClnEnv.setPref(me.getApplicationContext(), me.getString(R.string.CONST_PREF_GENMSG_NOTICEFLAG), false);
        }
    }

    //----------------------------------------------------------------------------------------------
    //                              Click Methods
    //----------------------------------------------------------------------------------------------
    @SuppressLint("NonConstantResourceId")
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.navbar_button_left:
                Objects.requireNonNull(slidingmenu).toggle();
                break;
            case R.id.navbar_button_right:
                //livechat
                DialogHelper.createSimpleDialog(act, getString(R.string.contact_us_redirect_message), getString(R.string.btn_ok), (dialog, which) -> {
                    Intent intentBrowser = new Intent(Intent.ACTION_VIEW, Uri.parse("https://wa.me/+85260331000"));
                    startActivity(intentBrowser);
                }, getString(R.string.btn_cancel));
                //check login
//                if (LiveChatHelper.isPause) {
//                    LiveChatHelper.getInstance(act, act).showWebView(act, moduleId);
//                } else if (ClnEnv.isLoggedIn()) {
//                    //open live chat
//                    if (!"Y".equalsIgnoreCase(ClnEnv.getChatIsMaintenance())) {
//                        LiveChatHelper.getInstance(act, act).showWebView(act, moduleId);
//                    } else {
//                        DialogHelper.createSimpleDialog(me, getString(R.string.LIVECHAT_UNAVAILABLE));
//                    }
//                } else {
//                    loginFirstDialog();
//                }
                break;
            case R.id.main_top_banner_premier:
                try {
                    String en;
                    String zh;
                    if (appConfig != null && appConfig.getCommonConfig() != null
                            && appConfig.getCommonConfig().getBannerUrlPremierEn() != null) {
                        en = appConfig.getCommonConfig().getBannerUrlPremierEn();
                    } else {
                        en = getResString(R.string.MYHKT_HOME_BTN_ABOUT_US_URL);
                    }
                    if (appConfig != null && appConfig.getCommonConfig() != null
                            && appConfig.getCommonConfig().getBannerUrlPremierZh() != null) {
                        zh = appConfig.getCommonConfig().getBannerUrlPremierZh();
                    } else {
                        zh = getResString(R.string.MYHKT_HOME_BTN_ABOUT_US_URL);
                    }
                    openInAppURL(isZh ? zh : en);
                } catch (Exception e) {
                    throw new RuntimeException(e);
                }
                break;
            case R.id.mainmenu_topbanner:
                if (Utils.isToShowTouchIdBanner(this)) {
                    Intent welcomePage = new Intent(this, TouchIdWelcomeActivity.class);
                    startActivityForResult(welcomePage, Constant.REQUEST_TOUCH_ID_WELCOME_PAGE);
                } else {
                    // get URL either logged in or not
                    String url = "";
                    if (ClnEnv.isLoggedIn()) {
                        if (appConfig != null && appConfig.getCommonConfig() != null
                                && appConfig.getCommonConfig().getBannerAfterUrlEn() != null
                                && appConfig.getCommonConfig().getBannerAfterUrlZh() != null) {
                            url = isZh ? appConfig.getCommonConfig().getBannerAfterUrlZh() : appConfig.getCommonConfig().getBannerAfterUrlEn();
                        }
                    } else {
                        if (appConfig != null && appConfig.getCommonConfig() != null
                                && appConfig.getCommonConfig().getBannerUrlEn() != null
                                && appConfig.getCommonConfig().getBannerUrlZh() != null) {
                            url = isZh ? appConfig.getCommonConfig().getBannerUrlZh() : appConfig.getCommonConfig().getBannerUrlEn();
                        }
                    }
                    openInAppURL(url);
                }
                break;
            case R.id.navbar_message_icon:
                Intent inboxIntent = new Intent(getApplicationContext(), InboxListActivity.class);
                startActivity(inboxIntent);
                overridePendingTransition(R.anim.right_slide_in, R.anim.left_slide_out);
                break;
        }
    }

    private boolean isChromeInstalledAndEnabled() {
        PackageManager pm = getPackageManager();
        try {
            pm.getPackageInfo("com.android.chrome", PackageManager.GET_ACTIVITIES);
            ApplicationInfo ai = getPackageManager().getApplicationInfo("com.android.chrome", 0);
            return ai.enabled;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return false;
    }

    // Android Device Back Button Handling
    public final void onBackPressed() {
        displayConfirmExitDialog();
    }

    //Menu items click
    public void onHomeBtnClick(AdapterView<?> parent, View v, int position, long id) {
        Intent intent;
        Bundle bundle;
        HomeButtonItem homeButtonItem = (HomeButtonItem) parent.getAdapter().getItem(position);

        Utils.ISMYLIENTEST = false;
        Utils.IS_IDD = false;
        switch (homeButtonItem.type) {
            case MYACCT:
                APIsManager.requestAppConfigJSON(this, this);
                if (!ClnEnv.isLoggedIn()) {
                    loginFirstDialog(homeButtonItem.type);
                } else {
                    intent = new Intent(getApplicationContext(), ServiceListActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    overridePendingTransition(R.anim.right_slide_in, R.anim.left_slide_out);
                }
                break;
            case MYLINETEST:
                if (!ClnEnv.isLoggedIn()) {
                    loginFirstDialog(homeButtonItem.type);
                } else {
                    intent = new Intent(getApplicationContext(), ServiceListActivity.class);
                    Utils.ISMYLIENTEST = true;
                    intent.putExtra(Constant.KEY_IS_MY_LINE_TEST, true);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    overridePendingTransition(R.anim.right_slide_in, R.anim.left_slide_out);
                }
                break;
            case MYWALLET:
                if (!ClnEnv.isLoggedIn()) {
                    loginFirstDialog(homeButtonItem.type);
                } else {
                    String iLogID = ClnEnv.getPref(getApplicationContext(), getString(R.string.CONST_PREF_LOGINID), "");

                    WalletCra wallet = new WalletCra();
                    wallet.setIApi(WALLET_EW_JWT);
                    wallet.setIGwtGud(ClnEnv.getSessTok());
                    wallet.setiLoginId(iLogID);
                    wallet.setoJWT("");
                    Reply rReply = new Reply();
                    rReply.setCode(null);
                    wallet.setReply(rReply);
                    wallet.setServerTS(null);

                    APIsManager.walletRequest(this, wallet);
                }
                break;
            case MYPROFILE:
                APIsManager.requestAppConfigJSON(this, this);
                if (!ClnEnv.isLoggedIn()) {
                    loginFirstDialog(homeButtonItem.type);
                } else {
                    intent = new Intent(getApplicationContext(), MyProfileActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivityForResult(intent, REQUEST_ACCOUNT_DELETE);
                    overridePendingTransition(R.anim.right_slide_in, R.anim.left_slide_out);
                }
                break;
            case MYAPPT:
                if (!ClnEnv.isLoggedIn()) {
                    loginFirstDialog(homeButtonItem.type);
                } else {
                    intent = new Intent(getApplicationContext(), MyApptActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    overridePendingTransition(R.anim.right_slide_in, R.anim.left_slide_out);
                }
                break;
            case MYMOB:
                intent = new Intent(getApplicationContext(), MyMobileActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                overridePendingTransition(R.anim.right_slide_in, R.anim.left_slide_out);
                break;
            case IDD:
                intent = new Intent(getApplicationContext(), IDDActivity.class);
                Utils.IS_IDD = true;
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                bundle = new Bundle();
                bundle.putBoolean(IDDActivity.BUNDLEIS0060, ClnEnv.isLoggedIn());
                intent.putExtras(bundle);
                startActivity(intent);
                overridePendingTransition(R.anim.right_slide_in, R.anim.left_slide_out);
                break;
            case THECLUB:
                // Event Tracker
                FAWrapper.getInstance().sendFAEvents(me, R.string.CONST_GA_CATEGORY_USERLV, R.string.CONST_GA_ACTION_BTN, R.string.CONST_GA_LABEL_CLUB, false);

                intent = getPackageManager().getLaunchIntentForPackage(Constant.THE_CLUB_PACKAGE_NAME);
                String messageTC = intent != null ? Utils.getString(me, R.string.THECLUB_TRANSITION_MSG) : Utils.getString(me, R.string.THECLUB_DOWNLOAD_MSG);
                appUrlDialog(intent, messageTC, Constant.THE_CLUB_PACKAGE_NAME);
                break;
            case DIRECTINQ:
                intent = new Intent(getApplicationContext(), DirectoryInquiryActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                overridePendingTransition(R.anim.right_slide_in, R.anim.left_slide_out);
                break;
            case SHOPS:
                intent = new Intent(getApplicationContext(), ShopActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                overridePendingTransition(R.anim.right_slide_in, R.anim.left_slide_out);
                break;
            case TAPNGO:
                // Event Tracker
                FAWrapper.getInstance().sendFAEvents(me, R.string.CONST_GA_CATEGORY_USERLV, R.string.CONST_GA_ACTION_BTN, R.string.CONST_GA_LABEL_TAPNGO, false);

                intent = getPackageManager().getLaunchIntentForPackage(Constant.TAP_AND_GO_PACKAGE_NAME);
                String messageTNG = intent != null ? Utils.getString(me, R.string.TAPNGO_TRANSITION_MSG) : Utils.getString(me, R.string.TAPNGO_DOWNLOAD_MSG);
                appUrlDialog(intent, messageTNG, Constant.TAP_AND_GO_PACKAGE_NAME);
                break;
            case MOX:
                intent = getPackageManager().getLaunchIntentForPackage(Constant.MOX_BANK_PACKAGE_NAME);
                String messageMOX = intent != null ? Utils.getString(me, R.string.MOX_TRANSITION_MSG) : Utils.getString(me, R.string.MOX_DOWNLOAD_MSG);
                appUrlDialog(intent, messageMOX, Constant.MOX_BANK_PACKAGE_NAME);
                break;
            case CONTACTUS:
                intent = new Intent(getApplicationContext(), ContactUsNewActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                overridePendingTransition(R.anim.right_slide_in, R.anim.left_slide_out);
                break;
            case SOLUTION:
                intent = new Intent(getApplicationContext(), BrowserActivity.class);
                intent.putExtra(BrowserActivity.INTENTLINK, "http://www.hktpremier.com/hktpremier/eng/homesolutions/");
                intent.putExtra(BrowserActivity.INTENTTITLE, Utils.getString(this, R.string.MYHKT_HOME_BTN_SOLUTION));
                startActivity(intent);
                overridePendingTransition(R.anim.right_slide_in, R.anim.left_slide_out);
                break;
            case NEWS:
                intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse(isZh ? "http://www.hktpremier.com/hktpremier/tch/whatsnew/" : "http://www.hktpremier.com/hktpremier/eng/whatsnew/"));
                startActivity(intent);
                overridePendingTransition(R.anim.right_slide_in, R.anim.left_slide_out);
                break;
            case HKT_SHOP:
                intent = getPackageManager().getLaunchIntentForPackage(Constant.HKT_SHOP_PACKAGE_NAME);
                String message = intent != null ? Utils.getString(me, R.string.HKTSHOP_TRANSITION_MSG) : Utils.getString(me, R.string.HKTSHOP_DOWNLOAD_MSG);
                appUrlDialog(intent, message, Constant.HKT_SHOP_PACKAGE_NAME);
                break;
            case AR_LENS:
                intent = getPackageManager().getLaunchIntentForPackage(Constant.AR_LENS_PACKAGE_NAME);
                String messageAR_LENS = Utils.getString(me, R.string.AR_LENS_REDIRECT_DOWNLOAD_MSG);
                appUrlDialog(intent, messageAR_LENS, Constant.AR_LENS_PACKAGE_NAME);
                break;
            case TRAVEL_GUIDE:
                openInAppURL(getResString(R.string.url_travel_guide));
                break;
            case TRAVEL_ZONE:
                openInAppURL(getResString(R.string.url_travel_zone));
                break;
            case DRGO:
                intent = getPackageManager().getLaunchIntentForPackage(Constant.DR_GO_PACKAGE_NAME);
                String messageDrGO = Utils.getString(me, R.string.DRGO_LENS_REDIRECT_DOWNLOAD_MSG);
                appUrlDialog(intent, messageDrGO, Constant.DR_GO_PACKAGE_NAME);
                break;
            default:
                break;
        }
    }

    private void openInAppURL(String url) {
        if (!TextUtils.isEmpty(url)) {
            if (isChromeInstalledAndEnabled()) {
                Utils.openUrlOnCustomTab(MainMenuActivity.this, url, getResources().getColor(ClnEnv.getSessionPremierFlag() ? R.color.menu_bg_premier : R.color.hkt_slidemenu_blue));
            } else {
                Intent intentBrowser = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                startActivity(intentBrowser);
                overridePendingTransition(R.anim.right_slide_in, R.anim.left_slide_out);
            }
        }
    }

    //----------------------------------------------------------------------------------------------
    //                              Listeners (interfaces) Methods
    //----------------------------------------------------------------------------------------------
    public void onSuccess(APIsResponse response) {
        if (response != null) {
            if (APIsManager.LGI.equals(response.getActionTy())) {
                //AUTO login
                LgiCra lgiCra = (LgiCra) response.getCra();
                ClnEnv.setLgiCra(lgiCra);
                ClnEnv.setSessionPremierFlag(Objects.requireNonNull(ClnEnv.getQualSvee()).getCustRec().isPremier());

                FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
                crashlytics.setCrashlyticsCollectionEnabled(true);
                crashlytics.setUserId(lgiCra.getOQualSvee().getSveeRec().loginId);
                FirebaseAnalytics.getInstance(this).setUserId(lgiCra.getOQualSvee().getSveeRec().loginId);

                ClnEnv.setPref(this, getString(R.string.CONST_PREF_SAVELOGINID), true);
                ClnEnv.setPref(this, getString(R.string.CONST_PREF_LOGINID), ClnEnv.getSessionLoginID());
                ClnEnv.setPref(me.getApplicationContext(), me.getString(R.string.CONST_PREF_SAVEPASSWORD), true);
                ClnEnv.setEncPref(me.getApplicationContext(), ClnEnv.getSessionLoginID(), me.getString(R.string.CONST_PREF_PASSWORD), ClnEnv.getSessionPassword());
                // New Bill Indicator Process
                saveAccountHelper = SaveAccountHelper.getInstance(me);
                // Removed all records if last bill date is one year or earlier than current Date
                saveAccountHelper.RemoveRecordForInitial(lgiCra.getServerTS());

                int rl = Objects.requireNonNull(ClnEnv.getQualSvee()).getSubnRecAry().length;
                int rx, rlz;
                //live accounts
                for (rx = 0; rx < rl; rx++) {
                    SubnRec subnRec = ClnEnv.getQualSvee().getSubnRecAry()[rx];
                }

                //zombie accounts
                rlz = ClnEnv.getQualSvee().getZmSubnRecAry().length;
                for (rx = 0; rx < rlz; rx++) {
                    SubnRec subnRec = ClnEnv.getQualSvee().getZmSubnRecAry()[rx];
                }

                // Line Test Result completed, redirect to targetService
                me.checkLnttResultAndRedirect();
                // Get GCM Message after autologin

                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    public void run() {
                        if (ClnEnv.getPushDataBill() != null && ClnEnv.getPushDataBill().getType().equals("N")) {
                            Utils.checkBillMsgAndRedirect(me, false);
                        } else if (ClnEnv.getPushDataBill() != null && ClnEnv.getPushDataBill().getType().equals("B")) {
                            checkBillMsgAndRedirect();
                        }
                    }
                }, 200);

                //Get Inbox List using intent service
                Intent intent = new Intent(me, InboxMessageIntentService.class);
                startService(intent);

            } else if (APIsManager.WALLET_EW_JWT.equals(response.getActionTy())) {
                String url = "";
                if (Utils.isUAT(this)) {
                    url = isZh ? Constant.URL_E_WALLET_CN_UAT : Constant.URL_E_WALLET_EN_UAT;
                } else {
                    url = isZh ? Constant.URL_E_WALLET_CN_PRD : Constant.URL_E_WALLET_EN_PRD;
                }

                WalletCra wallet = (WalletCra) response.getCra();

                Intent intent = new Intent(getApplicationContext(), BrowserActivity.class);
                url = url.replace(Constant.WALLET_TIMESTAMP, Utils.getTimeStamp());

                intent.putExtra(BrowserActivity.INTENTLINK, url);
                intent.putExtra(BrowserActivity.AUTHORIZATION, wallet.getoJWT());
                intent.putExtra(BrowserActivity.INTENTTITLE, Utils.getString(this, R.string.MYHKT_HOME_BTN_WALLET_WEB));
                startActivity(intent);
                overridePendingTransition(R.anim.right_slide_in, R.anim.left_slide_out);
            } else if (APIsManager.API_APP_CONFIG.equals(response.getActionTy())) {
                // Update AppConfig data model class
                appConfig = (AppConfig) response.getCra();
            }
        }
        initUI();
    }

    public void onFail(APIsResponse response) {
        if (response != null) {
            if (APIsManager.LGO.equals(response.getActionTy())) {
                // General Error Message
                if (!"".equals(response.getMessage()) && response.getMessage() != null) {
                    if (isActivityActive)
                        DialogHelper.createSimpleDialog(this, response.getMessage());
                } else if (RC.TCSESS_MM.equalsIgnoreCase(response.getReply().getCode())) {
                    BaseActivity.ivSessDialog();
                } else {
                    if (isActivityActive)
                        DialogHelper.createSimpleDialog(this, ClnEnv.getRPCErrMsg(this, response.getReply().getCode()));
                }
                ClnEnv.clear(me);
                ClnEnv.setSessionPremierFlag(false);
            } else if (APIsManager.LGI.equals(response.getActionTy())) {
                // General Error Message
                if (!"".equals(response.getMessage()) && response.getMessage() != null) {
                    if (isActivityActive)
                        DialogHelper.createSimpleDialog(this, response.getMessage());
                } else if (RC.TCSESS_MM.equalsIgnoreCase(response.getReply().getCode())) {
                    BaseActivity.ivSessDialog();
                } else {
                    if (isActivityActive)
                        DialogHelper.createSimpleDialog(this, ClnEnv.getRPCErrMsg(this, response.getReply().getCode()));
                }
                // Keeping the username and password in memory
                ClnEnv.setSessionLoginID("");
                ClnEnv.setSessionPassword("");
                ClnEnv.setSessionSavePw(false);
                ClnEnv.setPref(me.getApplicationContext(), me.getString(R.string.CONST_PREF_SAVEPASSWORD), false);
            } else if (APIsManager.WALLET_EW_JWT.equals(response.getActionTy())) {
                DialogHelper.createSimpleDialog(this, response.getMessage());
            }
        }
        initUI();
    }

    public static void onErrorMessageRequestToken(String message) {
        DialogHelper.createSimpleDialog(me, message);
    }

    @Override
    public void onDlSuccess(String message) {
        if (ACTION_BROADCAST_CONFIG_FINISH_DOWNLOAD.equalsIgnoreCase(message)) {
            // Received the message got the new appConfig json from API
            // Next is to download the list images base from appConfig data.
            downloadAppConfigImages();
        } else if (ACTION_BROADCAST_IMAGE_FINISH_DOWNLOAD.equalsIgnoreCase(message)) {
            // Received the message that the image banner was successfully saved
            // on the local file directory on the device
            // Next is to update the image banner.
            if (this.isPremier) {
                setTopBannerPremierConfiguration();
            } else {
                setTapBannerConfiguration();
            }
        }
    }

    @Override
    public void onActivityResult(final int requestCode, final int resultCode, final Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        if (requestCode == LiveChatHelper.mRequestCodeFilePicker) {
            if (resultCode == Activity.RESULT_OK) {
                if (intent != null) {
                    if (LiveChatHelper.mFileUploadCallbackFirst != null) {
                        LiveChatHelper.mFileUploadCallbackFirst.onReceiveValue(intent.getData());
                        LiveChatHelper.mFileUploadCallbackFirst = null;
                    } else if (LiveChatHelper.mFileUploadCallbackSecond != null) {
                        Uri[] dataUris;
                        try {
                            dataUris = new Uri[]{Uri.parse(intent.getDataString())};
                        } catch (Exception e) {
                            dataUris = null;
                        }
                        LiveChatHelper.mFileUploadCallbackSecond.onReceiveValue(dataUris);
                        LiveChatHelper.mFileUploadCallbackSecond = null;
                    }
                }
            } else {
                if (LiveChatHelper.mFileUploadCallbackFirst != null) {
                    LiveChatHelper.mFileUploadCallbackFirst.onReceiveValue(null);
                    LiveChatHelper.mFileUploadCallbackFirst = null;
                } else if (LiveChatHelper.mFileUploadCallbackSecond != null) {
                    LiveChatHelper.mFileUploadCallbackSecond.onReceiveValue(null);
                    LiveChatHelper.mFileUploadCallbackSecond = null;
                }
            }
        } else if (requestCode == REQUEST_ACCOUNT_DELETE) {
            if (resultCode == Activity.RESULT_OK) {
                onLogout();
                setNonPremierLayout();
            }
            if (resultCode == RESULT_RE_LOGIN) {
                Intent i = new Intent(me.getApplicationContext(), LoginActivity.class);
                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                me.startActivity(i);
                me.overridePendingTransition(R.anim.left_slide_in, R.anim.right_slide_out);

                ClnEnv.setHeloCra(null);
                ClnEnv.setSessionLoginID(null);
                ClnEnv.setSessionPassword(null);
            }
        }
    }

    @Override
    public void onLogout() {
        ClnEnv.clear(me);
        ClnEnv.setSessionPremierFlag(false);
        me.initUI();
    }

    //Custom broadcast receiver after images are downloaded
    BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.d("lwg", "MainActivity broadcastReceived: " + intent.getAction());
            if (ACTION_BROADCAST_IMAGE_FINISH_DOWNLOAD.equalsIgnoreCase(intent.getAction())) {
                onDlSuccess(ACTION_BROADCAST_IMAGE_FINISH_DOWNLOAD);
            } else if (ACTION_BROADCAST_CONFIG_FINISH_DOWNLOAD.equalsIgnoreCase(intent.getAction())) {
                onDlSuccess(ACTION_BROADCAST_CONFIG_FINISH_DOWNLOAD);
            } else if (Constant.ACTION_INBOX_LIST_UPDATED.equalsIgnoreCase(intent.getAction())) {
                showInboxIcon();
            } else if (Constant.ACTION_CACHED_APPT_RESPONSE.equalsIgnoreCase(intent.getAction())) {
                boolean appt_ind = ClnEnv.getPref(getApplicationContext(), getString(R.string.CONST_PREF_APPTIND_FLAG).concat("-").concat(String.valueOf(ClnEnv.getLoginId())), false);
                SwitchApptIcon(appt_ind);
                if (appt_ind) {
                    if ((apptDialog == null || !apptDialog.isShowing()) && isAppointmentPopupNotYetShownOrFromCappt()) {
                        showCachedApptResponsePopup();
                    }
                } else {
                    if (apptDialog != null && apptDialog.isShowing()) {
                        apptDialog.dismiss();
                        ClnEnv.setPref(getApplicationContext(), Constant.CONST_APPT_POPUP_SHOWN_FLAG.concat("-").concat(String.valueOf(ClnEnv.getLoginId())), "");
                    }
                }
            } else if (intent.getAction().equalsIgnoreCase(Constant.ACTION_SESSION_TIMEOUT)) {
                Intent dialogIntent = new Intent(context, GlobalDialog.class);
                dialogIntent.putExtra("DIALOGTYPE", R.string.CONST_DIALOG_SESSION_TIMEOUT);

                dialogIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                context.startActivity(dialogIntent);
            }
        }
    };

    private boolean isAppointmentPopupNotYetShownOrFromCappt() {
        String lastTsSaved = ClnEnv.getPref(getApplicationContext(), Constant.CONST_APPT_POPUP_SHOWN_FLAG.concat("-").concat(ClnEnv.getLoginId()), "");
        return lastTsSaved.isEmpty() || Utils.isCurrentDay(this, "", lastTsSaved, getString(R.string.input_datetime_format));
    }

    private void showCachedApptResponsePopup() {
        SimpleDateFormat sdf = new SimpleDateFormat(getString(R.string.input_datetime_format), Locale.getDefault());
        String currentDateAndTime = sdf.format(new Date());
        ClnEnv.setPref(getApplicationContext(), Constant.CONST_APPT_POPUP_SHOWN_FLAG.concat("-").concat(String.valueOf(ClnEnv.getLoginId())), currentDateAndTime);

        OnClickListener onclick = (dialog, which) -> {
            Intent intent = new Intent();
            intent = new Intent(getApplicationContext(), MyApptActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            overridePendingTransition(R.anim.right_slide_in, R.anim.left_slide_out);
        };

        androidx.appcompat.app.AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder(act);
        builder.setMessage(Utils.getString(this, R.string.cappt_reminder));
        builder.setCancelable(false);

        //To Align to the layout of IOS, will exchange the position of positive button and negative button
        builder.setNegativeButton(Utils.getString(this, R.string.btn_ok), onclick);
        builder.setPositiveButton(Utils.getString(this, R.string.MYHKT_BTN_LATER), new OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        apptDialog = builder.create();
        apptDialog.show();
    }

    //----------------------------------------------------------------------------------------------
    //                              UI Changes Methods
    //----------------------------------------------------------------------------------------------
    private void updateLoginStatus() {
        //nav bar inbox icon
        showInboxIcon();

        if (!ClnEnv.isLoggedIn()) {
            aq.id(R.id.mainmenu_label_welcome).text("");
        } else {
            String rGreet = "";
            cal = Calendar.getInstance();
            int rHr = cal.get(Calendar.HOUR_OF_DAY);

            // the exact logic copied from CSP
            if (rHr >= 5 && rHr < 12) {
                rGreet = Utils.getString(me, R.string.SHLF_MORNING);
            } else {
                if (rHr >= 12 && rHr < 18) {
                    rGreet = Utils.getString(me, R.string.SHLF_AFTERNOON);
                } else {
                    rGreet = Utils.getString(me, R.string.SHLF_NIGHT);
                }
            }
            aq.id(R.id.mainmenu_label_welcome).text(String.format("%s,\n%s", rGreet, Objects.requireNonNull(ClnEnv.getQualSvee()).getSveeRec().nickname));
            aq.id(R.id.mainmenu_label_welcome).textColorId(ClnEnv.getSessionPremierFlag() ? R.color.hkt_txtcolor_premier : R.color.hkt_txtcolor_grey);
        }
    }

    public void setLiveChangeIcon(Boolean isPause) {
        if (isPause) {
            if (aq != null) {
                aq.id(R.id.navbar_button_right).visibility(View.VISIBLE);
                aq.navBarButton(R.id.navbar_button_right, R.drawable.chat);
                ColorFilter filter = new PorterDuffColorFilter(Color.parseColor("#DB9200"), PorterDuff.Mode.MULTIPLY);
                aq.id(R.id.navbar_button_right).getImageView().setColorFilter(filter);
            }
        } else {
            if (aq != null) {
                aq.id(R.id.navbar_button_right).visibility(View.VISIBLE);
                aq.navBarButton(R.id.navbar_button_right, R.drawable.chat);
                aq.id(R.id.navbar_button_right).getImageView().clearColorFilter();
                //Premier: white , Non-Premier : blue
                if (!ClnEnv.getSessionPremierFlag()) {
                    setMenuButtonFilterBlue();
                } else {
                    setMenuButtonFilterWhite();
                }
            }
        }
        assert aq != null;
        aq.id(R.id.navbar_button_right).visibility(View.INVISIBLE);
    }

    private void SwitchCApptIcon(final boolean hasAppt) {
        mainMenuGridViewAdapter.notifyDataSetChanged();
        int apptIconIndex = getIndexByMenuType();
        if (homeItems != null && homeItems.get(apptIconIndex) != null) {
            if (hasAppt) {
                if (ClnEnv.getSessionPremierFlag()) {
                    homeItems.get(apptIconIndex).setImageResource(R.drawable.home_btn_apt_new_p);
                } else {
                    homeItems.get(apptIconIndex).setImageResource(R.drawable.myappointment_2);
                }
            } else {
                if (ClnEnv.getSessionPremierFlag()) {
                    homeItems.get(apptIconIndex).setImageResource(R.drawable.home_btn_appt_premier);
                } else {
                    homeItems.get(apptIconIndex).setImageResource(R.drawable.myappointment);
                }
            }
            mainMenuGridViewAdapter.notifyDataSetChanged();
        }
    }

    private void SwitchApptIcon(final boolean hasAppt) {
        mainMenuGridViewAdapter.notifyDataSetChanged();
        int apptIconIndex = getIndexByMenuType();
        if (homeItems != null && homeItems.get(apptIconIndex) != null) {
            if (hasAppt) {
                if (ClnEnv.getSessionPremierFlag()) {
                    homeItems.get(apptIconIndex).setImageResource(R.drawable.home_btn_apt_new_p);
                } else {
                    homeItems.get(apptIconIndex).setImageResource(R.drawable.myappointment_2);
                }
            } else {
                if (ClnEnv.getSessionPremierFlag()) {
                    homeItems.get(apptIconIndex).setImageResource(R.drawable.home_btn_appt_premier);
                } else {
                    homeItems.get(apptIconIndex).setImageResource(R.drawable.myappointment);
                }
            }
            mainMenuGridViewAdapter.notifyDataSetChanged();
        }
    }

    private int getIndexByMenuType() {
        for (int i = 0; i < homeItems.size(); i++) {
            if (homeItems.get(i).type == MAINMENU.MYAPPT) {
                return i;
            }
        }
        return -1;
    }

    private void showInboxIcon() {
        InbxCra inbxCra = Utils.getPrefInboxCra(this);
        if (inbxCra != null) {
            aq.id(R.id.navbar_message_icon).getView().setVisibility(ClnEnv.isLoggedIn() ? View.VISIBLE : View.GONE);
            aq.id(R.id.navbar_message_icon).getImageView().setImageDrawable(isPremier ? ContextCompat.getDrawable(this, R.drawable.premier_nav_inbox) : ContextCompat.getDrawable(this, R.drawable.inbox));

            //get unread inbox
            if (inbxCra.getOIbxMsgRecAry() != null && MyTool.getIbxUnrdCnt(inbxCra.getOIbxMsgRecAry()) > 0) {
                aq.id(R.id.navbar_message_icon).getImageView().setImageDrawable(isPremier ? ContextCompat.getDrawable(this, R.drawable.premier_nav_inbox_alert) : ContextCompat.getDrawable(this, R.drawable.inbox_alert));
            }
            aq.id(R.id.navbar_message_icon).clicked(this, "onClick");
        }
    }

    //----------------------------------------------------------------------------------------------
    //                              Alert Methods
    //----------------------------------------------------------------------------------------------
    // Login First Dialog
    protected final void loginFirstDialog() {
        //Without redirect
        loginFirstDialog(null);
    }

    protected final void loginFirstDialog(final MAINMENU clickItem) {
        Bundle rbundle;
        Intent intent = new Intent(act.getApplicationContext(), LoginActivity.class);
        rbundle = new Bundle();
        if (clickItem != null) {
            rbundle.putSerializable("CLICKBUTTON", clickItem);
            intent.putExtras(rbundle);
        }
        startActivity(intent);
        overridePendingTransition(R.anim.right_slide_in, R.anim.left_slide_out);
    }

    public final void doLogout() {
        DialogHelper.createLogoutDialog(me, Utils.getString(me, R.string.myhkt_confirmlogout), Utils.getString(me, R.string.myhkt_btn_yes), Utils.getString(this, R.string.myhkt_btn_no), this);
    }

    // Confirm Exit
    public final void displayConfirmExitDialog() {
        OnClickListener onclick = new OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                ClnEnv.setPref(act.getApplicationContext(), Constant.CONST_IDLE_START_TIME, System.currentTimeMillis());
                dialog.dismiss();
                ClnEnv.clear(me);
                int pid = android.os.Process.myPid();
                android.os.Process.killProcess(pid);
                finish();
            }
        };

        if (isActivityActive)
            DialogHelper.createSimpleDialog(me, getResources().getString(R.string.quit_msg), getResources().getString(R.string.myhkt_btn_yes), onclick, getResources().getString(R.string.myhkt_btn_no));
    }

    protected final void billMsgLoginRedirect() {
        AlertDialog.Builder builder = new Builder(MainMenuActivity.this);
        builder.setMessage(Utils.getString(me, R.string.MYHKT_PN_ERR_ACC_NOT_FOUND));
        builder.setPositiveButton(Utils.getString(me, R.string.btn_ok), new OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent intent;
                dialog.dismiss();
                intent = new Intent(getApplicationContext(), LoginActivity.class);

                ClnEnv.setPref(getApplicationContext(), getString(R.string.CONST_PREF_BILLMSG_NOTICEFLAG), false);
                ClnEnv.getPushDataBill().clear();
                //temp
                startActivity(intent);
                overridePendingTransition(R.anim.right_slide_in, R.anim.left_slide_out);
            }
        });
        builder.create().show();
    }

    protected final void billMsgRedirect(String msg) {
        AlertDialog.Builder builder = new Builder(MainMenuActivity.this);
        builder.setMessage(msg);
        builder.setPositiveButton(Utils.getString(me, R.string.btn_ok), new OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent intent;
                dialog.dismiss();
                intent = new Intent(getApplicationContext(), ServiceListActivity.class);
                startActivity(intent);
                overridePendingTransition(R.anim.right_slide_in, R.anim.left_slide_out);
            }
        });
        builder.create().show();
    }

    private void appUrlDialog(final Intent intent, String message, final String packageName) {
        DialogHelper.createSimpleDialog(me, message, Utils.getString(me, R.string.btn_ok), new OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                // Check if the app exist
                if (intent != null && packageName.equalsIgnoreCase(Constant.THE_CLUB_PACKAGE_NAME) && intent.resolveActivity(me.getPackageManager()) != null) {
                    startActivity(intent);
                } else if (intent != null && packageName.equalsIgnoreCase(Constant.TAP_AND_GO_PACKAGE_NAME)) {
                    startActivity(intent);
                } else if (intent != null && packageName.equalsIgnoreCase(Constant.AR_LENS_PACKAGE_NAME)) {
                    startActivity(intent);
                } else if (intent != null && packageName.equalsIgnoreCase(Constant.DR_GO_PACKAGE_NAME)) {
                    startActivity(intent);
                } else if (intent != null && packageName.equalsIgnoreCase(Constant.MOX_BANK_PACKAGE_NAME)) {
                    startActivity(intent);
                } else if (packageName.equalsIgnoreCase(Constant.HKT_SHOP_PACKAGE_NAME)) {
                    Intent intent = new Intent(Intent.ACTION_VIEW);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.setData(Uri.parse(Constant.URL_DEEP_LINK_CLUB));
                    startActivity(intent);
                } else {
                    if (packageName.equalsIgnoreCase(Constant.AR_LENS_PACKAGE_NAME)) {
                        openInAppURL(Constant.URL_DEEP_LINK_AR_LENS);
                        return;
                    }
                    try {
                        Intent intent = new Intent(Intent.ACTION_VIEW);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        intent.setData(Uri.parse(String.format(Constant.URL_DEEP_LINK_MARKET, packageName)));
                        startActivity(intent);
                    } catch (ActivityNotFoundException e) {
                        Intent intent = new Intent(Intent.ACTION_VIEW);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        intent.setData(Uri.parse(String.format(Constant.URL_DEEP_LINK_GOOGLE, packageName)));
                        startActivity(intent);
                    }
                }
            }
        }, Utils.getString(this, R.string.btn_cancel));
    }

    private void downloadAppConfigImages() {
        APIsManager.requestDownloadImage(this, this, appConfig, ClnEnv.getAppLocale(getBaseContext()));
    }

    private void appConfigSetting() {
        appConfigString = ClnEnv.getPref(getApplicationContext(), KEY_SHARED_APP_CONFIG, getResString(R.string.appConfig));
        appConfig = gson.fromJson(appConfigString, AppConfig.class);
    }
}
