package com.pccw.myhkt.adapter;

/************************************************************************
File       : IDDDestListViewAdapter.java
Desc       : Destination list for IDD Rates search function
Name       : IDDDestListViewAdapter
Created by : Derek Tsui
Date       : 23/01/2014

Change History:
Date       Modified By        	Description
---------- ----------------   	-------------------------------
23/01/2014 Derek Tsui       	- First draft

*************************************************************************/

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;

import com.pccw.myhkt.R;
import com.pccw.myhkt.Utils;
import com.pccw.myhkt.lib.ui.RegImageButton;

public class RegServiceGridViewAdapter extends BaseAdapter {
	
	public 		  Context				context;
	public 		  LayoutInflater		inflater;
	public 		  List<Integer>			resIds	;
	private final boolean				isZh;
	private 	  int					itemHeight;
	//Filterable
	
	
	public RegServiceGridViewAdapter(Context context, List<Integer>resLists, int itemHeight) {
		super();
		this.context = context;
		this.itemHeight = itemHeight;
		this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		resIds = resLists;

		// Language indicator
		if ("zh".equalsIgnoreCase(Utils.getString(context, R.string.myhkt_lang))) {
			isZh = true;
		} else {
			isZh = false;
		}
	}
	
	@Override
	public int getCount() {
		if (resIds == null) return 0;
		return resIds.size();
	}

	@Override
	public Object getItem(int position) {
		if (position >= 0 && position < resIds.size()) {
			return resIds.get(position);
		} else {
			return null;
		}
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		RegImageButton regImageButon = new RegImageButton(parent.getContext());
		regImageButon.initViews(parent.getContext(), resIds.get(position), "", 0, 0, itemHeight);
//		try {
//			if (convertView == null) {
//				holder = new ViewHolder();
//				convertView = inflater.inflate(R.layout.adapter_idddestlist, null);
//	
//				holder.adapter_idddestlist_right_circle = (ImageView) convertView.findViewById(R.id.adapter_idddestlist_right_circle);
//				holder.adapter_idddestlist_dest = (TextView) convertView.findViewById(R.id.adapter_idddestlist_dest);
//				holder.adapter_idddestlist_destext = (TextView) convertView.findViewById(R.id.adapter_idddestlist_destext);
//	
//				convertView.setTag(holder);
//			} else {
//				holder = (ViewHolder) convertView.getTag();
//			}
//			
//				holder.adapter_idddestlist_dest.setText((isZh) ? destination[position].chiDestName : destination[position].engDestName);
//				holder.adapter_idddestlist_destext.setText((isZh) ? destination[position].chiDestNameExt : destination[position].engDestNameExt);
//				holder.adapter_idddestlist_destext.setVisibility(View.VISIBLE);
//				
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
		
		
		return regImageButon;
	}
	
}
