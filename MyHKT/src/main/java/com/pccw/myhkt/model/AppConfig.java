package com.pccw.myhkt.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class AppConfig {

    @SerializedName("image_list")
    private List<AppConfigImageSetting> imageList;

    @SerializedName("common_config")
    private AppConfigCommon commonConfig;

    public List<AppConfigImageSetting> getImageList() {
        return imageList;
    }

    public void setImageList(List<AppConfigImageSetting> imageList) {
        this.imageList = imageList;
    }

    public AppConfigCommon getCommonConfig() {
        return commonConfig;
    }

    public void setCommonConfig(AppConfigCommon commonConfig) {
        this.commonConfig = commonConfig;
    }
}
