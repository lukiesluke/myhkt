package com.pccw.myhkt.service;

/************************************************************************
 File       : LineTestIntentService.java
 Desc       : Background Service For LineTest
 Name       : LineTestIntentService
 Created by : Vincent Fung
 Date       : 12/02/2014

 Change History:
 Date       Modified By			Description
 ---------- ----------------	-------------------------------
 12/02/2014  Vincent Fung		- First draft
 13/02/2014  Vincent Fung		1.) Created IntentService for background lineTest
 2.) Allowed to Normal LineTest and Reset Modem
 3.) Added Notification to foreground or push Notification to mobile device
 24/02/2014 Vincent Fung			- Code cleanup
 *************************************************************************/

import android.app.IntentService;
import android.content.Intent;

import androidx.annotation.Nullable;

import android.util.Log;

import com.pccw.dango.shared.cra.CApptCra;
import com.pccw.dango.shared.entity.CApptRec;
import com.pccw.myhkt.APIsManager;
import com.pccw.myhkt.APIsManager.OnAPIsListener;
import com.pccw.myhkt.APIsResponse;
import com.pccw.myhkt.ClnEnv;
import com.pccw.myhkt.R;
import com.pccw.myhkt.util.Constant;

import java.util.Timer;
import java.util.TimerTask;

public class CheckAppointmentIntentService extends IntentService implements OnAPIsListener {
    private boolean debug = true;
    private static CheckAppointmentIntentService me;
    public static int cApptRtryCnt = 0;
    private Timer timer = new Timer();
    private String errorMsg = "";
    private String TAG = "CAppt";

    public CheckAppointmentIntentService() {
        super("CheckAppointmentIntentService");
    }

    public CheckAppointmentIntentService(String name) {
        super(name);
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {

    }

    @Override
    public int onStartCommand(@Nullable Intent intent, int flags, int startId) {
        cApptRtryCnt = 0;
        CApptCra apptCra = new CApptCra();
        apptCra.setiLoginId(ClnEnv.getLoginId());
        apptCra.setiCustRec(ClnEnv.getQualSvee().getCustRec());
        CApptRec cApptRec = new CApptRec();
        cApptRec.setAppt_rmdr(String.valueOf(ClnEnv.getPref(this, getString(R.string.CONST_PREF_APPTIND_DAYS), 3)));
        apptCra.setiCApptRec(cApptRec);

        APIsManager.doGetCAppt(CheckAppointmentIntentService.this, apptCra);

        return super.onStartCommand(intent, flags, startId);


    }

    @Override
    public void onSuccess(APIsResponse response) throws Exception {
        if (response != null) {
            if (response.getActionTy().equals(APIsManager.CAPPT)) {
                CApptCra cApptCra = (CApptCra) response.getCra();
                String currentApptRmdr = String.valueOf(ClnEnv.getPref(this, getString(R.string.CONST_PREF_APPTIND_DAYS), 3));
                if (cApptCra.getoCApptRec().pend_appt.equals("Y") && !ClnEnv.isIsQueryApptServiceRunning()) {
                    ClnEnv.setPref(getApplicationContext(), Constant.CONST_PREF_APPTIND_FLAG.concat("-").concat(String.valueOf(ClnEnv.getLoginId())), true);
                    ClnEnv.setPref(getApplicationContext(), Constant.CONST_PREF_APPTIND_LAST_TS.concat("-").concat(String.valueOf(ClnEnv.getLoginId())), cApptCra.getServerTS());
                    sendBroadcast(new Intent().setAction(Constant.ACTION_CACHED_APPT_RESPONSE).setPackage(getPackageName()));
                    updateFlagForPopupShownWithDelay(cApptCra.getServerTS());
                } else if (cApptCra.getoCApptRec().pend_appt.equals("N") && !ClnEnv.isIsQueryApptServiceRunning()) {
                    ClnEnv.setPref(getApplicationContext(), Constant.CONST_PREF_APPTIND_FLAG.concat("-").concat(String.valueOf(ClnEnv.getLoginId())), false);
                    ClnEnv.setPref(getApplicationContext(), Constant.CONST_PREF_APPTIND_LAST_TS.concat("-").concat(String.valueOf(ClnEnv.getLoginId())), cApptCra.getServerTS());
                    sendBroadcast(new Intent().setAction(Constant.ACTION_CACHED_APPT_RESPONSE).setPackage(getPackageName()));
                    stopSelf();
                } else if (cApptCra.getoCApptRec().pend_appt.equals("P")) {
                    Log.d(TAG, "status == P, cnt: " + cApptRtryCnt);
                    if (cApptRtryCnt < 6) {
                        cApptRtryCnt++;
                        // sleep 30 seconds and retry doGetCAppt();
                        doRecheckAppt(cApptCra);
                    } else {
                        stopSelf();
                    }
                } else {
                    stopSelf();
                }
            }
        }
    }

    private void updateFlagForPopupShownWithDelay(String ts) {
        timer = new Timer();

        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                ClnEnv.setPref(getApplicationContext(), Constant.CONST_APPT_POPUP_SHOWN_FLAG.concat("-").concat(String.valueOf(ClnEnv.getLoginId())), ts);
                stopSelf();
            }
        }, 5000);

    }

    @Override
    public void onFail(APIsResponse response) throws Exception {
        if (response != null) {
            if (!"".equals(response.getMessage()) && response.getMessage() != null) {
                ClnEnv.setPref(this.getApplicationContext(), Constant.CONST_INBOX_CRA_RESPONSE, "");
            }
        }
    }

    private final void doRecheckAppt(CApptCra cApptCra) {
        // clear previous appointment reset Record
        // clear timer to make sure one task allowed
        timer.cancel();
        timer.purge();
        timer = new Timer();

        try {
            timer.schedule(new TimerTask() {
                @Override
                public void run() {
                    doAPIRecheckAppointment(cApptCra);
                }
            }, 30000);
        } catch (Exception e) {
            // fail to sleep
            cApptRtryCnt = 6;
            // Asynctask completed. Cleanup myself
            stopSelf();
        }
    }

    private final void doAPIRecheckAppointment(CApptCra cApptCra) {


        if (debug) Log.i(TAG, "doRecheckAppointment");
        if (cApptCra != null) {
            if (debug) Log.i(TAG, "doRecheckAppointment");
            APIsManager.doGetCAppt(this, cApptCra);
        } else {
            cApptCra = new CApptCra();
            stopSelf();
        }
    }

}