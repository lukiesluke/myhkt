/*
    Unidentified User (likely for Web/API) in Dango
    
    Keywords
    --------
    $URL: svn://10.87.120.207/dango/rel/v16.0/src/com/pccw/dango/shared/entity/NotAPerson.java $
    $Rev: 844 $
    $Date: 2016-10-17 11:54:01 +0800 (¶g¤@, 17 ¤Q¤ë 2016) $
    $Author: upd $
*/

package com.pccw.dango.shared.entity;

import java.io.Serializable;


public class NotAPerson extends BaseUserEx implements Serializable
{
    private static final long serialVersionUID = -6952557558466779388L;


    public static void main(String rArg[])
    {
        System.out.println(getVer());
        return;
    }
    
    
    public static String getVer()
    {
        return ("$URL: svn://10.87.120.207/dango/rel/v16.0/src/com/pccw/dango/shared/entity/NotAPerson.java $, $Rev: 844 $");
    }
    
    
    public NotAPerson()
    {
        super();
    }
    
    
    public String getId()
    {
        return (ID_UNKN);
    }
    
    
    public String getType()
    {
        return (TY_NPSN);
    }
    
    
    public String getRole()
    {
        return ("");
    }
    
    
    public String[] getRghtAry()
    {
        return (new String[0]);
    }
    
    
    public boolean isAuth(String rRght)
    {
        return (false);
    }
}
