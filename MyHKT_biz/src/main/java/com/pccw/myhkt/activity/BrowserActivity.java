package com.pccw.myhkt.activity;

import java.util.List;

import org.apache.http.cookie.Cookie;

import android.content.Intent;
import android.net.http.SslError;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import android.webkit.SslErrorHandler;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.pccw.myhkt.APIsManager;
import com.pccw.myhkt.ClnEnv;
import com.pccw.biz.myhkt.R;
import com.pccw.myhkt.Utils;
import com.pccw.myhkt.lib.ui.AAQuery;

/************************************************************************
 * File : BrowserActivity.java
 * Desc : Web Page
 * Name : BrowserActivity
 * by 	: Andy Wong
 * Date : 22/12/2015
 * 
 * Change History:
 * Date       Modified By		Description
 * ---------- ----------------	-------------------------------
 * 22/12/2015 Andy Wong 		-First draft
 * 09/05/2016 Andy Wong 		-Extands for base activity
 *************************************************************************/
public class BrowserActivity extends BaseActivity{
	private boolean debug = false;
	private String url = null;
	private String title = null;
	private int type = 0;
	private static int lob = 0;

	private AAQuery aq;
	private WebView webView;

	public static String INTENTMODULEID = "moduleId";
	public static String INTENTLINK = "link";
	public static String INTENTTITLE = "title";
	public static String INTENTLOB = "lob";
	public static String INTENTLOBTYPE = "type";

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		debug = getResources().getBoolean(R.bool.DEBUG);

		setContentView(R.layout.activity_browser);
		Intent intent = getIntent();
		if(intent!=null && intent.getExtras() !=null) {
			moduleId = intent.getExtras().getString(INTENTMODULEID);
			url = intent.getExtras().getString(INTENTLINK);
			title = intent.getExtras().getString(INTENTTITLE, "");
			lob = intent.getExtras().getInt(INTENTLOB);
			type = intent.getExtras().getInt(INTENTLOBTYPE);
		}

		intent = null;
	}

	@Override
	protected void initUI2() {
		aq = new AAQuery(this);	

		//navbar style
		aq.navBarButton(R.id.navbar_button_left, R.drawable.btn_back);		
		if (lob > 0) {
			aq.navBarTitle(R.id.navbar_title, R.id.navbar_leftdraw, 0 ,  Utils.getLobIcon(lob, type)  , 0, title);
		} else {
			aq.navBarTitle(R.id.navbar_title, title);
		}

		if (lob == R.string.CONST_LOB_LTS) {			
			String rightText = Utils.getLtsTtypeRightText(this, type);
			aq.id(R.id.navbar_righttext).text(rightText).textColorId(R.color.hkt_txtcolor_grey);
		}

		aq.id(R.id.navbar_button_left).clicked(this, "onClick");
		webView = aq.id(R.id.activity_browser_webview).getWebView();

		if (url != null) displayWebView(url);

	}

	private void displayWebView(String url) {

		webView.setWebViewClient(new WebViewOverrideUrl());
		webView.setWebChromeClient(new WebChromeClient());
		webView.setVisibility(View.VISIBLE);
		webView.getSettings().setJavaScriptEnabled(true);
		webView.getSettings().setBuiltInZoomControls(true);
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			webView.getSettings().setDisplayZoomControls(false);
		}
		webView.getSettings().setLoadWithOverviewMode(true);
		webView.getSettings().setUseWideViewPort(true);

		String cookieString = "";
		//	    CookieSyncManager.createInstance(UsageActivity.this);
		CookieSyncManager cookieSyncManager = CookieSyncManager.createInstance(this);
		//	    CookieSyncManager cookieSyncManager = CookieSyncManager.createInstance(wv.getContext());
		CookieManager cookieManager = CookieManager.getInstance();
		List<Cookie> cookies = APIsManager.mCookies;
		if (cookies == null) {
			cookies = ClnEnv.getHttpClient().getCookieStore().getCookies();
		}
		cookieManager.setAcceptCookie(true);

		Cookie sessionInfo;

		if (android.os.Build.VERSION.SDK_INT >= 14) {
			// call remove All Cookie after Android 4.0
			cookieManager.removeAllCookie(); //remove
		}

		for(int i = 0; i < cookies.size(); i++){
			sessionInfo = cookies.get(i);
			//			if (debug) Log.d("THIS IS COOKIE", "domain=" + sessionInfo.getDomain()+"getPath : "+sessionInfo.getPath()+", session:"+sessionInfo.getValue());
			cookieString = sessionInfo.getName() + "=" + sessionInfo.getValue() + ";domain=" + sessionInfo.getDomain();

			cookieManager.setCookie(sessionInfo.getDomain(), cookieString);
			cookieSyncManager.sync();
		}

		//		browser_webview.setWebViewClient(webview);
		if (!"".equalsIgnoreCase(url)) webView.loadUrl(url);
	}

	//webview override url to avoid redirect at web browser outside the app
	private class WebViewOverrideUrl extends WebViewClient {
		boolean isReceiveError = false;

		@Override
		public boolean shouldOverrideUrlLoading(WebView view, String url) {
			if (debug) Log.d("Loading", "url : " + url);
			
//	         if (url.indexOf("tel:") > -1) {
////	        	 act.startActivity(new Intent(Intent.ACTION_DIAL, Uri.parse(url)));
//					Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse(Const.contact_phone_theclub));
//					me.startActivity(intent); 
//	            return true;
//	         } else 
	        if (url.indexOf("mailto:") > -1){
				Intent intent =  new Intent(Intent.ACTION_SEND);
				String[]  tos =  {"hktpremiercr@pccw.com"};
				intent.putExtra(Intent.EXTRA_EMAIL,tos);
//				intent.putExtra(Intent.EXTRA_TEXT, me.getString(R.string.CLUB_CONTACT_EMAIL_BODY)); 
//				intent.putExtra(Intent.EXTRA_SUBJECT,me.getString(R.string.CLUB_CONTACT_EMAIL_SUBJECT));
				intent.setType("message/rfc822");
				me.startActivity(Intent.createChooser(intent,"Your Client"));
				return true;
	        } else {
	        	view.loadUrl(url);
	            return true;
	        }
//			view.loadUrl(url);
//			return true;
		}

		@Override
		public void onPageFinished(WebView view, String url) {
			super.onPageFinished(view, url);
			if (!isReceiveError) {
				view.setVisibility(View.VISIBLE);
			} else {
				view.setVisibility(View.GONE);
			}
			
			String cookies = CookieManager.getInstance().getCookie(ClnEnv.getPref(me, getString(R.string.CONST_PREF_DOMAIN), APIsManager.PRD_domain));
			if (debug) Log.d("THIS IS COOKIE", "onPageFinished All the cookies in a string:" + cookies);
		}

		@Override
		public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
			super.onReceivedError(view, errorCode, description, failingUrl);

			isReceiveError = true;
			view.setVisibility(View.GONE);
			//				errorExitDialog(Utils.getString(me, R.string.LIVECHAT_UNAVAILABLE));
		}

//		@Override
//		public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
//			handler.proceed(); // Ignore SSL certificate errors
//		}
	}

	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.navbar_button_left:
			onBackPressed();
			break;
		}
	}

	public void onBackPressed() {
		if (webView != null && webView.canGoBack()) {
			webView.goBack();
		} else {
			finish();
			overridePendingTransition(R.anim.left_slide_in, R.anim.right_slide_out);
		}
	}


	// Free all references to the UI elements
	protected void cleanupUI() {
		url = null;
		title = null;		
		aq = null;
		webView = null;
	}
}
