package com.pccw.myhkt.activity;

import java.lang.Thread.UncaughtExceptionHandler;
import java.util.Locale;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.pccw.dango.shared.cra.AcMainCra;
import com.pccw.dango.shared.cra.HeloCra;
import com.pccw.dango.shared.cra.SpssCra;
import com.pccw.dango.shared.entity.SpssRec;
import com.pccw.dango.shared.entity.SveeRec;
import com.pccw.dango.shared.tool.RC;
import com.pccw.myhkt.APIsManager;
import com.pccw.myhkt.APIsResponse;
import com.pccw.myhkt.ClnEnv;
import com.pccw.biz.myhkt.R;
import com.pccw.myhkt.Utils;
import com.pccw.myhkt.lib.ui.AAQuery;
import com.pccw.myhkt.lib.ui.LRButton;
import com.pccw.myhkt.lib.ui.LRButton.OnLRButtonClickListener;
import com.pccw.myhkt.lib.ui.PopOverInputView;
import com.pccw.myhkt.lib.ui.popover.ActionItem;
import com.pccw.myhkt.lib.ui.popover.QuickAction;
import com.pccw.myhkt.service.InitGCMAsyncTask;
import com.pccw.wheat.shared.tool.Reply;

/************************************************************************
File       : SettingsActivity.java
Desc       : Setting Screen
Name       : SettingsActivity
Created by : Derek Tsui
Date       : 16/12/2015

Change History:
Date       Modified By			Description
---------- ----------------	-------------------------------
16/12/2015 Derek Tsui			- First draft

*************************************************************************/

public class SettingsActivity extends BaseActivity implements OnLRButtonClickListener  {
	private String 					TAG = this.getClass().getName();
	// Common Components
	private boolean					debug = false;			// toggle debug logs
	private static SettingsActivity	me;
	private AAQuery 					aq;
	private LRButton   				settings_lrbtn;
	private int 						extralinespace = 0;
	
	private int				parentOnClickId	= 0;
	
	private PopOverInputView 	settings_popover_input;
	private QuickAction		mQuickAction; //popover view
	
	// GCM
	private static String 							   regId;								//registration ID from GCM
	private static AsyncTask<Context, Void, String>  initGCMAsyncTask			= null;

	
	@Override
	public final void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		me = this;
		debug = getResources().getBoolean(R.bool.DEBUG);
		ClnEnv.updateUILocale(getBaseContext(),	ClnEnv.getAppLocale(getBaseContext()));
		setContentView(R.layout.activity_settings);
	}
	
	@Override
	protected void onNewIntent(Intent intent) {
		super.onNewIntent(intent);
		ClnEnv.updateUILocale(getBaseContext(),
				ClnEnv.getAppLocale(getBaseContext()));
	}
	
	@Override
	protected final void onStart() {
		super.onStart();
		//Screen Tracker
		
		initData();
		initUI();
	}
	
	@Override
	protected final void onResume() {
		super.onResume();
		moduleId = getResources().getString(R.string.MODULE_SETTING);	
		regId = ClnEnv.getPref(me.getApplicationContext(), me.getString(R.string.CONST_PREF_GCM_REGID), "");
		if (regId.isEmpty()) {
		    initGCMAsyncTask = new InitGCMAsyncTask(getApplicationContext(), callbackHandler);
			initGCMAsyncTask.execute(this);
		}
	}	
	
	@Override
	protected final void onPause() {
		super.onPause();
	}
	
	@Override
	protected final void onStop() {
		super.onStop();

	}
	
	// Android Device Back Button Handling
	public final void onBackPressed() {
		finish();
		overridePendingTransition(R.anim.left_slide_in, R.anim.right_slide_out);
	}
	
	private void initData(){
		extralinespace = (int) getResources().getDimension(R.dimen.extralinespace);	
	}
	
	private final void initUI() {
		aq = new AAQuery(this);
		
		//navbar
		aq.navBarTitle(R.id.navbar_title, getResources().getString(R.string.myhkt_settings));
		aq.navBarButton(R.id.navbar_button_left, R.drawable.btn_back);
		aq.navBarButton(R.id.navbar_button_right, R.drawable.livechat_small);
		aq.id(R.id.navbar_button_left).clicked(this, "onClick");
//		aq.id(R.id.navbar_button_right).clicked(this, "onClick");
		
		settings_lrbtn = (LRButton) aq.id(R.id.settings_lrbtn).getView();
		settings_lrbtn.initViews(this, "中文", "English");
		settings_lrbtn.setPadding(extralinespace, 0, extralinespace, extralinespace);
		
		if (ClnEnv.getAppLocale(getApplicationContext()).toLowerCase(Locale.US).startsWith(getResString(R.string.CONST_LOCALE_ZH))) {
			settings_lrbtn.setBtns(true);
		} else {
			settings_lrbtn.setBtns(false);
		}

		//Display Lang
		aq.id(R.id.settings_dislang_text).text(getResString(R.string.PAMF_LANG));
		aq.id(R.id.settings_dislang_text).textSize(getResources().getInteger(R.integer.textsize_default_int));
		
		//new bill indicator checkbox
		aq.id(R.id.settings_checkbox_newbill).getCheckBox().setButtonDrawable(R.drawable.checkbox);
		aq.id(R.id.settings_checkbox_newbill).checked(ClnEnv.getPref(getApplicationContext(), getString(R.string.CONST_PREF_NEWBILLIND_FLAG), true));
		aq.id(R.id.settings_checkbox_newbill).getCheckBox().setOnCheckedChangeListener(onCheckedChangeListener);
		aq.id(R.id.settings_newbill_text).text(getResString(R.string.myhkt_settings_newbill));
		aq.id(R.id.settings_newbill_text).textSize(getResources().getInteger(R.integer.textsize_default_int));
		
		//Notification checkbox
		aq.id(R.id.settings_LBL_NOTIFICATION_text).text(getResString(R.string.MYHKT_SETTING_LBL_NOTIFICATION));
		aq.id(R.id.settings_LBL_NOTIFICATION_text).textSize(getResources().getInteger(R.integer.textsize_default_int));
		aq.id(R.id.settings_checkbox_noti).getCheckBox().setButtonDrawable(R.drawable.checkbox);
		int playServiceResultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
		if (ClnEnv.getPref(getApplicationContext(), getString(R.string.CONST_PREF_GENNOTI_FLAG), "Y").equalsIgnoreCase("Y") && playServiceResultCode == ConnectionResult.SUCCESS) {
			aq.id(R.id.settings_checkbox_noti).checked(true);
		} else {
			aq.id(R.id.settings_checkbox_noti).checked(false);
		}
		aq.id(R.id.settings_checkbox_noti).getCheckBox().setOnCheckedChangeListener(onCheckedChangeListener);

		//appointment alert
		aq.id(R.id.settings_appt_text).text(getResString(R.string.myhkt_settings_appt));
		aq.id(R.id.settings_appt_text).textSize(getResources().getInteger(R.integer.textsize_default_int));
		Drawable drawable= getResources().getDrawable(ClnEnv.getSessionPremierFlag() ? R.drawable.icon_appointment_alert_premierx60 : R.drawable.icon_appointment_alertx60); 
		drawable.setBounds(0, 0, drawable.getMinimumWidth(), drawable.getMinimumHeight()); 
		aq.id(R.id.settings_appt_text).getTextView().setCompoundDrawables(null,null,drawable,null);
		drawable = null;
		settings_popover_input = aq.popOverInputView(R.id.settings_popover_input, getResources().getString(R.string.myhkt_settings_appt_3days));
		ActionItem[] actionItem = new ActionItem[3];
		actionItem[0] = new ActionItem(3, getResources().getString(R.string.myhkt_settings_appt_3days));
		actionItem[1] = new ActionItem(5, getResources().getString(R.string.myhkt_settings_appt_5days));
		actionItem[2] = new ActionItem(7, getResources().getString(R.string.myhkt_settings_appt_7days));
		WindowManager wm = (WindowManager) this.getSystemService(Context.WINDOW_SERVICE);
		int screenWidth 	= wm.getDefaultDisplay().getWidth();
		mQuickAction 	= new QuickAction(this, screenWidth/2, 0);
		for (int i = 0; i < actionItem.length; i++) {
			mQuickAction.addActionItem(actionItem[i]);
		}
		
		//setup the action item click listener
		mQuickAction.setOnActionItemClickListener(new QuickAction.OnActionItemClickListener() {
			@Override
			public void onItemClick(QuickAction quickAction, int pos, int actionId) {
				ActionItem actionItem = quickAction.getActionItem(pos);
				aq.id(R.id.popover_input_layouttxt).text(actionItem.getTitle());
				ClnEnv.setPref(getApplicationContext(), getString(R.string.CONST_PREF_APPTIND_DAYS), actionItem.getActionId());
			}
		});
		
		aq.id(R.id.settings_popover_input).clicked(this, "onClick");
		//init appointment selection
		ActionItem action = mQuickAction.getActionItemById(ClnEnv.getPref(getApplicationContext(), getString(R.string.CONST_PREF_APPTIND_DAYS), 3));
		String apptDaysString = action != null ? action.getTitle() : getResources().getString(R.string.myhkt_settings_appt_3days);
		aq.id(R.id.popover_input_layouttxt).text(apptDaysString);
		

		if (!ClnEnv.isLoggedIn()) {
			aq.id(R.id.settings_newbill_layout).visibility(View.GONE);
			aq.id(R.id.settings_appt_layout).visibility(View.GONE);  //CA: Hide
		} else {
			aq.id(R.id.settings_newbill_layout).visibility(View.VISIBLE);
			aq.id(R.id.settings_appt_layout).visibility(View.GONE);
		}
	}
	
	//on checkbox check listener
	OnCheckedChangeListener onCheckedChangeListener = new OnCheckedChangeListener() {
		@Override
		public void onCheckedChanged(CompoundButton buttonView,
				boolean isChecked) {
			// TODO Auto-generated method stub
			switch (buttonView.getId()) {
				case R.id.settings_checkbox_noti:
					if (isChecked) {
						//Check if Google Play Services available
						int playServiceResultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(me);
						if (playServiceResultCode == ConnectionResult.SUCCESS) {
							doUpdateSmartPhone();
						} else {
							//NETWORK_ERROR
							//LICENSE_CHECK_FAILED 
							//INVALID_ACCOUNT
							//SERVICE_DISABLED
							//SERVICE_MISSING 
							aq.id(R.id.settings_checkbox_noti).checked(false);
							try {
								GooglePlayServicesUtil.getErrorDialog(playServiceResultCode, me, -1).show();
							} catch (Exception e) {
								if (debug) Log.e("GooglePlayServicesUtil", e.getMessage());
							}
						}
					} else {
						if (GooglePlayServicesUtil.isGooglePlayServicesAvailable(me) == ConnectionResult.SUCCESS) {
							doUpdateSmartPhone();
						}
					}
					break;
				case R.id.settings_checkbox_newbill:
					if (isChecked) {
						ClnEnv.setPref(getApplicationContext(), getString(R.string.CONST_PREF_NEWBILLIND_FLAG), true);
					} else {
						ClnEnv.setPref(getApplicationContext(), getString(R.string.CONST_PREF_NEWBILLIND_FLAG), false);
					}
			}
		}
	};

	
	//on view click listener
	public void onClick(View v) {
		switch (v.getId()) {
			case R.id.navbar_button_left:
				onBackPressed();
				break;
			case R.id.navbar_button_right:
				//livechat
				break;
			case R.id.settings_popover_input:
				mQuickAction.show(v, aq.id(R.id.settings_parentlayout).getView());
				mQuickAction.setAnimStyle(QuickAction.ANIM_GROW_FROM_RIGHT);
				break;
			case R.id.settings_help_newbill:
				displayDialog(me, getString(R.string.MYHKT_SETTING_HINT_NEW_BILL));
				break;
			case R.id.settings_help_appt:
				displayDialog(me, getString(R.string.MYHKT_SETTING_HINT_NEW_APPT));
				break;
			case R.id.settings_help_noti:
				displayDialog(me, String.format(getResString(R.string.MYHKT_SETTING_HINT_NOTIFICATION), getString(R.string.MYHKT_SETTING_HINT_PLATFORM)));
				break;
		}
	}

	@Override
	public void onLRBtnLeftClick() {
		settings_lrbtn.setBtns(true);
		ClnEnv.setPref(getApplicationContext(), getString(R.string.CONST_PREF_LOCALE), getString(R.string.CONST_LOCALE_ZH));
		doUpdateSmartPhone();
		ClnEnv.updateUILocale(getBaseContext(), ClnEnv.getAppLocale(getBaseContext()));
//		setContentView(R.layout.activity_settings);
		initData();
//		initNavBar();
		initUI();		
		setLiveChangeIcon(false);
	}
	
	@Override
	public void onLRBtnRightClick() {
		settings_lrbtn.setBtns(false);
		ClnEnv.setPref(getApplicationContext(), getString(R.string.CONST_PREF_LOCALE), getString(R.string.CONST_LOCALE_EN));
		doUpdateSmartPhone();
		ClnEnv.updateUILocale(getBaseContext(), ClnEnv.getAppLocale(getBaseContext()));
//		setContentView(R.layout.activity_settings);
		initData();
//		initNavBar();
		initUI();	
		setLiveChangeIcon(false);
	}
	
	//Update Push Notification Settings AsyncTask
	private final void doUpdateSmartPhone() {
		aq = new AAQuery(this);
		
		SpssRec spssRec = new SpssRec();
		if (ClnEnv.getPref(me.getApplicationContext(), me.getString(R.string.CONST_PREF_SAVEPASSWORD), false) && ClnEnv.isLoggedIn()) {							//Bill Notificatiion "Y" if password is saved
			spssRec.bni = "Y";
		} else {
			spssRec.bni = "N";
		}	
		spssRec.gni = aq.id(R.id.settings_checkbox_noti).isChecked() ? "Y" : "N";
		spssRec.lang = ClnEnv.getAppLocale(me.getBaseContext());

		SpssCra spssCra = new SpssCra();
		spssCra.setISpssRec(spssRec);
		
		APIsManager.doUpdSmph(this, spssCra);
	}
	
	public static final Handler	callbackHandler	= new Handler() {
		@Override
		public void handleMessage(Message msg) {
			if (me != null) {
				Bundle bundle = msg.getData();

				if (InitGCMAsyncTask.asyncid.equalsIgnoreCase(bundle.getString("CALLER")) && initGCMAsyncTask != null) {
					regId = bundle.getString("REGID");
					initGCMAsyncTask = null;
					//Save registrationID to shared preference
					ClnEnv.setPref(me.getApplicationContext(), me.getString(R.string.CONST_PREF_GCM_REGID), regId);
				}
			}
		}
	};
	
	public void onFail(APIsResponse response) {
		super.onFail(response);
		aq = new AAQuery(this);
		if (response != null) {
			if (APIsManager.SPSS_UPD.equals(response.getActionTy())) {
				SpssCra spsscra;
				try {
					spsscra = (SpssCra) response.getCra();
				} catch (Exception e) {
					spsscra = null;
				}
				if (!RC.SPSS_IV_DID.equalsIgnoreCase(response.getReply().getCode())) {
					if (!"".equals(response.getMessage()) && response.getMessage() != null) {
						displayDialog(this, response.getMessage());
					} else if (RC.TCSESS_MM.equalsIgnoreCase(response.getReply().getCode())) {
						ivSessDialog();
					} else {
						displayDialog(this, ClnEnv.getRPCErrMsg(this, response.getReply().getCode()));
					}
				}
				// TODO handle difference errorCode
//				if (!spsscra.getISpssRec().gni.equalsIgnoreCase(ClnEnv.getPref(me.getApplicationContext(), me.getString(R.string.CONST_PREF_GENNOTI_FLAG), "Y"))) {
				//invalid session handling //TODO (GWTGUD error code)
//					if (spsscra.getReply().isEqual(Reply.RC_IVSESS)) {
//						me.redirectLoginDialog(ClnEnv.getRPCErrMsg(me.getApplicationContext(), spsscra.getReply().getCode()));
//					} else {
//						displayDialog(this, ClnEnv.getRPCErrMsg(me.getApplicationContext(), spsscra.getReply().getCode()));
//					}
					if (ClnEnv.getPref(me.getApplicationContext(), me.getString(R.string.CONST_PREF_GENNOTI_FLAG), "Y").equalsIgnoreCase("Y")) {
						aq.id(R.id.settings_checkbox_noti).checked(true);
					} else {
						aq.id(R.id.settings_checkbox_noti).checked(false);
					}
//				}
			} else {
				// General Error Message
				if (!"".equals(response.getMessage()) && response.getMessage() != null) {
					displayDialog(this, response.getMessage());
				} else if (RC.TCSESS_MM.equalsIgnoreCase(response.getReply().getCode())) {
					ivSessDialog();
				} else {
					displayDialog(this, ClnEnv.getRPCErrMsg(this, response.getReply().getCode()));
				}
			}
		}
	}
	
	public void onSuccess(APIsResponse response) {
		if (response != null) {
			if (APIsManager.HELO.equals(response.getActionTy())) {
				debugLog(TAG, "doHelo complete!!");
			} else if (APIsManager.SPSS_UPD.equals(response.getActionTy())) {
				SpssCra spsscra = (SpssCra) response.getCra();
				// Display Message if Notification CheckBox is OFF and user changed CheckBox selection
				if (!spsscra.getISpssRec().gni.equalsIgnoreCase(ClnEnv.getPref(me.getApplicationContext(), me.getString(R.string.CONST_PREF_GENNOTI_FLAG), "Y"))) {
					if ("N".equalsIgnoreCase(spsscra.getISpssRec().gni)) {
						displayDialog(this, Utils.getString(me, R.string.MYHKT_PN_MSG_BILL_PN_ALWAYS_ON));
					}
				}
				
				if ("Y".equalsIgnoreCase(spsscra.getISpssRec().gni)) {
					ClnEnv.setPref(me.getApplicationContext(), me.getString(R.string.CONST_PREF_GENNOTI_FLAG), "Y");
				} else {
					ClnEnv.setPref(me.getApplicationContext(), me.getString(R.string.CONST_PREF_GENNOTI_FLAG), "N");
				}
				
				if (ClnEnv.getLoginId() != null) {
					//temporary "update profile" call
					AcMainCra acMainCra = new AcMainCra();		
					acMainCra.setILoginId(ClnEnv.getLoginId());
					SveeRec sveeRec = ClnEnv.getQualSvee().getSveeRec();
					SveeRec mSveeRec = sveeRec.copyMe();
					mSveeRec.lang = ClnEnv.getAppLocale(me.getBaseContext());
					acMainCra.setISveeRec(mSveeRec);
					APIsManager.doUpdProfile(this, null, acMainCra);
				}
			} else if (APIsManager.ACMAIN.equals(response.getActionTy())) {
				AcMainCra acMainCra = (AcMainCra) response.getCra();
				ClnEnv.getQualSvee().setSveeRec(acMainCra.getOSveeRec());
			}
		}
	}
}
