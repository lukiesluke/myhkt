package com.pccw.myhkt.lib.ui;

import android.content.Context;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.widget.ScrollView;

//This scroll view was created to prevent auto scroll from programmatic event changes

public class CustomScrollView extends ScrollView{
	
	  public CustomScrollView(Context context) {
		    super(context);
		  }

		  public CustomScrollView(Context context, AttributeSet attrs) {
		    super(context, attrs);
		  }

		  public CustomScrollView(Context context, AttributeSet attrs, int defStyle) {
		    super(context, attrs, defStyle);
		  }

		  @Override
		  protected boolean onRequestFocusInDescendants(int direction, Rect previouslyFocusedRect) {
		    return true;
		  }
}
