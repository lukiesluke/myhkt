/*
    Base Crate Extension
    
    Add additional fields for DANGO.
    
    Cra defines communication data structure between the 
    Client (GWTRPC and MA) to DANGO backend.
    
    Keywords
    --------
    $URL: $
    $Rev: $
    $Date: $
    $Author: $
*/


package com.pccw.dango.shared.cra;

import java.io.Serializable;

import com.pccw.wheat.shared.rpc.BaseCra;


public class BaseCraEx extends BaseCra implements Serializable 
{
    private static final long serialVersionUID = -3571674234447316808L;
    
    /* Mobile App Specific */
    private String                  iApi;       /* API Name                             */
    private String                  iGwtGud;    /* GWT Guard                            */
    
    /* PageFlow RPC Specific */
    private String                  iMyFrm;     /* Form Id who submit this Request      */
    
    /* PageFlow RPC Specific */
    private String                  oHgg;       /* Hashed GWT Guard                     */
    private String                  oNxUrl;     /* Next URL                             */
    private BaseCraEx               oDuct;      /* Duct Variable                        */

    
    public static void main(String rArg[])
    {
        System.out.println(getVer());
        return;
    }
    
    
    public static String getVer()
    {
        return ("$URL: $, $Rev: $");
    }

    
    public BaseCraEx()
    {
        initAndClear();
    }
    
    
    protected void init()
    {
        super.init();
    }
    
    
    public void clear()
    {
        super.clear();
        
        clearIApi();
        clearIGwtGud();
        clearIMyFrm();

        clearOHgg();
        clearONxUrl();
        clearODuct();
    }
    

    public BaseCraEx copyFrom(BaseCraEx rSrc)
    {
        super.copyFrom(rSrc);
        
        setIApi(rSrc.getIApi());
        setIGwtGud(rSrc.getIGwtGud());
        setIMyFrm(rSrc.getIMyFrm());

        setOHgg(rSrc.getOHgg());
        setONxUrl(rSrc.getONxUrl());
        setODuct(rSrc.getODuct());

        return (this);
    }
    
    
    public BaseCraEx copyTo(BaseCraEx rDes)
    {
        rDes.copyFrom(this);
        return (rDes);
    }
    
    
    public BaseCraEx copyMe()
    {
        BaseCraEx                      rDes;
        
        rDes = new BaseCraEx();
        rDes.copyFrom(this);
        return (rDes);
    }
    
    
    public void clearIApi()
    {
        setIApi("");
    }
    
    
    public void setIApi(String rArg)
    {
        iApi = rArg;
    }
    
    
    public String getIApi()
    {
        return (iApi);
    }
    
    
    public void clearIGwtGud()
    {
        setIGwtGud("");
    }
    
    
    public void setIGwtGud(String rArg)
    {
        iGwtGud = rArg;
    }
    
    
    public String getIGwtGud()
    {
        return (iGwtGud);
    }
    
    
    public void clearIMyFrm()
    {
        setIMyFrm("");
    }
    
    
    public void setIMyFrm(String rArg)
    {
        iMyFrm = rArg;
    }
    
    
    public String getIMyFrm()
    {
        return (iMyFrm);
    }
    
    
    public void clearOHgg()
    {
        setOHgg("");
    }
    
    
    public void setOHgg(String rArg)
    {
        oHgg = rArg;
    }
    
    
    public String getOHgg()
    {
        return (oHgg);
    }
    
    
    public void clearONxUrl()
    {
        setONxUrl("");
    }
    
    
    public void setONxUrl(String rArg)
    {
        oNxUrl = rArg;
    }
    
    
    public String getONxUrl()
    {
        return (oNxUrl);
    }
    
    
    public void clearODuct()
    {
        setODuct(null);
    }
    
    
    public void setODuct(BaseCraEx rArg)
    {
        oDuct = rArg;
    }
    
    
    public BaseCraEx getODuct()
    {
        return (oDuct);
    }
}
