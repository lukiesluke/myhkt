package com.pccw.dango.shared.g3entity;

import java.io.Serializable;

public class G3UsageBarDTO implements Serializable
{
    private static final long serialVersionUID = -5333880239122296090L;
    
    private String showBar;
    private int colorR;
    private int colorG;
    private int colorB;
    private int barPercent;
    
    public String getShowBar() {
        return showBar;
    }
    public void setShowBar(String showBar) {
        this.showBar = showBar;
    }
    public int getColorR() {
        return colorR;
    }
    public void setColorR(int colorR) {
        this.colorR = colorR;
    }
    public int getColorG() {
        return colorG;
    }
    public void setColorG(int colorG) {
        this.colorG = colorG;
    }
    public int getColorB() {
        return colorB;
    }
    public void setColorB(int colorB) {
        this.colorB = colorB;
    }
    public int getBarPercent() {
        return barPercent;
    }
    public void setBarPercent(int barPercent) {
        this.barPercent = barPercent;
    }
}



