package com.pccw.myhkt.model;

import android.os.Parcel;
import android.os.Parcelable;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

@Root(name = "Rate", strict = false)
public class Rate implements Parcelable {
    @Element(name = "timeZone", required = false)
    public String timeZone;

    @Element(name = "chiTimeZone", required = false)
    public String chiTimeZone;

    @Element(name = "timeZoneRemarks", required = false)
    public String timeZoneRemarks;

    @Element(name = "chiTimeZoneRemarks", required = false)
    public String chiTimeZoneRemarks;

    @Element(name = "rate", required = false)
    public String rate;

    public Rate() {
    }

    public Rate(Parcel in) {
        this.timeZone = in.readString();
        this.chiTimeZone = in.readString();
        this.timeZoneRemarks = in.readString();
        this.chiTimeZoneRemarks = in.readString();
        this.rate = in.readString();
    }

    public static final Rate.Creator CREATOR = new Rate.Creator() {
        public Rate createFromParcel(Parcel in) {
            return new Rate(in);
        }

        public Rate[] newArray(int size) {
            return new Rate[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {

    }
}
