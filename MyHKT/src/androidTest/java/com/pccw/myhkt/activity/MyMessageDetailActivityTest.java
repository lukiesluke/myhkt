package com.pccw.myhkt.activity;

import android.content.Context;
import android.content.SharedPreferences;
import androidx.test.rule.ActivityTestRule;
import androidx.test.rule.GrantPermissionRule;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import com.pccw.myhkt.AccountHelper;
import com.pccw.myhkt.ClnEnv;
import com.pccw.myhkt.R;
import com.pccw.myhkt.base.BaseTest;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static androidx.test.platform.app.InstrumentationRegistry.getInstrumentation;

@Ignore
@RunWith(AndroidJUnit4.class)
public class MyMessageDetailActivityTest extends BaseTest {
    private Context context;
    SharedPreferences shared;

    @Rule
    public ActivityTestRule<MainMenuActivity> activityTestRule = new ActivityTestRule<>(MainMenuActivity.class);
    @Rule
    public GrantPermissionRule permissionRule = GrantPermissionRule.grant(android.Manifest.permission.WRITE_EXTERNAL_STORAGE);

    @Before
    public void setUp(){
        context = getInstrumentation().getTargetContext();
        shared = context.getSharedPreferences(
                "myhkt",
                Context.MODE_PRIVATE);
        if(ClnEnv.isLoggedIn()){
            executeLogout(context);
        }
        clickLeftNavBar();
        clickString(context, R.string.myhkt_LGIF_LOGIN);
        loginMyCredentialsPRD(AccountHelper.getPRDAccount3().getUsername(),
                AccountHelper.getPRDAccount3().getPassword(), shared);
        delay(TestTimeouts.ACTIVITY_LOAD_TIMEOUTS);
        clickView(R.id.navbar_message_icon, R.id.navbar_base_layout);
        clickString("Welcome to My HKT!");
    }

    @After
    public void tearDown() {
        if(ClnEnv.isLoggedIn()){
            executeLogout(context);
        }
    }

    @Test
    public void checkMessageDetailDisplay() {
        checkStringIsDisplayed(context, R.string.title_inbox);
        delay(TestTimeouts.ACTIVITY_LOAD_TIMEOUTS);
        checkLiveChatBtnIsDisplayed();
        backBtnExist();
        checkViewIsDisplayed(R.id.email_text_date);
        checkViewIsDisplayed(R.id.email_img_category);
        checkViewIsDisplayed(R.id.email_text_remaining);
        checkViewIsDisplayed(R.id.email_text_title);
        checkViewIsDisplayed(R.id.email_text_content);
        checkViewIsDisplayed(R.id.delete_container);
    }

    @Test
    public void testLOBNetvigatorBtn() {
        clickLiveChatNavBar();
        clickViewChildView(R.id.rv_images,0);
        clickOK();
        delay(TestTimeouts.LIVECHAT_TIMEOUTS);
        clickView(R.id.dialog_livechat_close);
        delay(TestTimeouts.DIALOG_TIMEOUTS, waitForText(btn_OK));
        clickOK();
    }

    @Test
    public void testLOBNOWBtn() {
        clickLiveChatNavBar();
        clickViewChildView(R.id.rv_images,1);
        clickOK();
        delay(TestTimeouts.LIVECHAT_TIMEOUTS);
        clickView(R.id.dialog_livechat_close);
        delay(TestTimeouts.DIALOG_TIMEOUTS, waitForText(btn_OK));
        clickOK();
    }

    @Test
    public void testLOBLTSBtn() {
        clickLiveChatNavBar();
        clickViewChildView(R.id.rv_images,2);
        clickOK();
        delay(TestTimeouts.LIVECHAT_TIMEOUTS);
        clickView(R.id.dialog_livechat_close);
        delay(TestTimeouts.DIALOG_TIMEOUTS, waitForText(btn_OK));
        clickOK();
    }

    @Test
    public void testLOB1010Btn() {
        clickLiveChatNavBar();
        clickViewChildView(R.id.rv_images,3);
        clickOK();
        delay(TestTimeouts.LIVECHAT_TIMEOUTS);
        clickView(R.id.dialog_livechat_close);
        delay(TestTimeouts.DIALOG_TIMEOUTS, waitForText(btn_OK));
        clickOK();
    }

    @Test
    public void testLOBCSLBtn() {
        clickLiveChatNavBar();
        clickViewChildView(R.id.rv_images,4);
        clickOK();
        delay(TestTimeouts.LIVECHAT_TIMEOUTS);
        clickView(R.id.dialog_livechat_close);
        delay(TestTimeouts.DIALOG_TIMEOUTS, waitForText(btn_OK));
        clickOK();
    }

    @Test
    public void testLOBCancelBtn() {
        clickLiveChatNavBar();
        clickCANCEL();
    }

    @Test
    public void testMyMessageListBackButton() {
        //TODO: Implement the test method.
    }

    @Test
    public void testDeleteButton() {
        checkViewIsDisplayed(R.id.delete_container);
        checkStringIsDisplayed(btn_CANCEL);
    }

}