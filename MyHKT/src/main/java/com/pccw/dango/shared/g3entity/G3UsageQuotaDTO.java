package com.pccw.dango.shared.g3entity;

import java.io.Serializable;


public class G3UsageQuotaDTO implements Serializable 
{
    private static final long serialVersionUID = -7291643668566050855L;

    private String asOfDate;

    // mobile data usage
    private String localDataQuotaBaseInd;
    private String roamingDataQuotaBaseInd;
    private String localDataQtaName;
    private String roamingDataQtaName;
    private String mobileDataRtnCd;
    private String mobileDataRtnMsg;
    
    private String mobileLocalDataUsage;
    private String mobileLocalDataQuota;
    private String mobileLocalDataInitialQuota;
    private String mobileLocalDataUnit;
    private String mobileRoamingDataUsage;
    private String mobileRoamingDataInitialQuota;
    private String mobileRoamingDataQuota;
    private String mobileRoamingDataUnit;
    
    private String mobileLocalDataRemaining;
    private String mobileLocalDataRemainingInMB;
    private String mobileLocalDataRemainingInPercent;
    private String mobileLocalDataUsedInPercent;
    
    // other usage
    private String nonMobileUsageEntitlementRtnCode;
    private String nonMobileUsageEntitlementRtnMsg;

    private String localVoicePeakOffpeakBasedInd;
    private String localVoiceIntraUsage;
    private String localVoiceIntraEntitlement;
    private String localVoiceIntraUnit;
    private String localVoiceIntraUsageGrp;
    private String localVoiceInterUsage;
    private String localVoiceInterEntitlement;
    private String localVoiceInterUnit;
    private String localVoiceInterUsageGrp;
    private String localVoicePeakUsage;
    private String localVoicePeakEntitlement;
    private String localVoicePeakUnit;
    private String localVoiceOffPeakUsage;
    private String localVoiceOffPeakEntitlement;
    private String localVoiceOffPeakUnit;
    private String localVideoIntraUsage;
    private String localVideoIntraEntitlement;
    private String localVideoIntraUnit;
    private String localVideoIntraUsageGrp;
    private String localVideoInterUsage;
    private String localVideoInterEntitlement;
    private String localVideoInterUnit;
    private String localVideoInterUsageGrp;
    private String localSMSIntraUsage;
    private String localSMSIntraEntitlement;
    private String localSMSIntraUsageGrp;
    private String localSMSInterUsage;
    private String localSMSInterEntitlement;
    private String localSMSInterUsageGrp;
    private String overseasSMSUsage;
    private String overseasSMSEntitlement;
    private String overseasSMSUsageGrp;
    private String nowTVUsage;
    private String nowTVEntitlement;
    private String nowTVUnit;
    private String nowSportsUsage;
    private String nowSportsEntitlement;
    private String nowSportsUnit;
    private String moovUsage;
    private String moovEntitlement;
    private String moovUnit;
    private String localMMSIntraUsage;
    private String localMMSIntraEntitlement;
    private String localMMSIntraUnit;
    private String localMMSIntraUsageGrp;
    private String localMMSInterUsage;
    private String localMMSInterEntitlement;
    private String localMMSInterUnit;
    private String localMMSInterUsageGrp;
    private String overseasMMSUsage;
    private String overseasMMSEntitlement;
    private String overseasMMSUnit;
    private String overseasMMSUsageGrp;
    private String emailUsage;
    private String emailEntitlement;
    private String emailUnit;
    private String emailUsageGrp;
    
    // new usage fields for CSL
    private String iddVoiceUsage;
    private String iddVoiceEntitlement;
    private String iddVoiceUnit;
    private String localBBUsage;
    private String localBBEntitlement;
    private String localBBUnit;
    private String roamBBUsage;
    private String roamBBEntitlement;
    private String roamBBUnit;
    private String roamSMSUsage;
    private String roamSMSEntitlement;
    private String dataAsOfDate;
    private String primaryFlag;
    private String nxtBilCutoffDt;
    
    public String getLocalVoiceIntraUsage() {
        return localVoiceIntraUsage;
    }
    public void setLocalVoiceIntraUsage(String localVoiceIntraUsage) {
        this.localVoiceIntraUsage = localVoiceIntraUsage;
    }
    public String getLocalVoiceIntraEntitlement() {
        return localVoiceIntraEntitlement;
    }
    public void setLocalVoiceIntraEntitlement(String localVoiceIntraEntitlement) {
        this.localVoiceIntraEntitlement = localVoiceIntraEntitlement;
    }
    public String getLocalVoiceIntraUnit() {
        return localVoiceIntraUnit;
    }
    public String getLocalVoiceIntraUsageGrp() {
        return localVoiceIntraUsageGrp;
    }
    public void setLocalVoiceIntraUsageGrp(String localVoiceIntraUsageGrp) {
        this.localVoiceIntraUsageGrp = localVoiceIntraUsageGrp;
    }
    public String getLocalVideoIntraUsage() {
        return localVideoIntraUsage;
    }
    public void setLocalVideoIntraUsage(String localVideoIntraUsage) {
        this.localVideoIntraUsage = localVideoIntraUsage;
    }
    public String getLocalVideoIntraEntitlement() {
        return localVideoIntraEntitlement;
    }
    public void setLocalVideoIntraEntitlement(String localVideoIntraEntitlement) {
        this.localVideoIntraEntitlement = localVideoIntraEntitlement;
    }
    public String getLocalVideoIntraUnit() {
        return localVideoIntraUnit;
    }
    public void setLocalVideoIntraUnit(String localVideoIntraUnit) {
        this.localVideoIntraUnit = localVideoIntraUnit;
    }
    public String getLocalVideoIntraUsageGrp() {
        return localVideoIntraUsageGrp;
    }
    public void setLocalVideoIntraUsageGrp(String localVideoIntraUsageGrp) {
        this.localVideoIntraUsageGrp = localVideoIntraUsageGrp;
    }
    public String getLocalVideoInterUsage() {
        return localVideoInterUsage;
    }
    public void setLocalVideoInterUsage(String localVideoInterUsage) {
        this.localVideoInterUsage = localVideoInterUsage;
    }
    public String getLocalVideoInterEntitlement() {
        return localVideoInterEntitlement;
    }
    public void setLocalVideoInterEntitlement(String localVideoInterEntitlement) {
        this.localVideoInterEntitlement = localVideoInterEntitlement;
    }
    public String getLocalVideoInterUnit() {
        return localVideoInterUnit;
    }
    public void setLocalVideoInterUnit(String localVideoInterUnit) {
        this.localVideoInterUnit = localVideoInterUnit;
    }
    public String getLocalVideoInterUsageGrp() {
        return localVideoInterUsageGrp;
    }
    public void setLocalVideoInterUsageGrp(String localVideoInterUsageGrp) {
        this.localVideoInterUsageGrp = localVideoInterUsageGrp;
    }
    public void setLocalVoiceIntraUnit(String localVoiceIntraUnit) {
        this.localVoiceIntraUnit = localVoiceIntraUnit;
    }
    public String getLocalVoiceInterUsage() {
        return localVoiceInterUsage;
    }
    public void setLocalVoiceInterUsage(String localVoiceInterUsage) {
        this.localVoiceInterUsage = localVoiceInterUsage;
    }
    public String getLocalVoiceInterUsageGrp() {
        return localVoiceInterUsageGrp;
    }
    public void setLocalVoiceInterUsageGrp(String localVoiceInterUsageGrp) {
        this.localVoiceInterUsageGrp = localVoiceInterUsageGrp;
    }
    public String getLocalVoiceInterEntitlement() {
        return localVoiceInterEntitlement;
    }
    public String getLocalVoicePeakUsage() {
        return localVoicePeakUsage;
    }
    public void setLocalVoicePeakUsage(String localVoicePeakUsage) {
        this.localVoicePeakUsage = localVoicePeakUsage;
    }
    public String getLocalVoicePeakEntitlement() {
        return localVoicePeakEntitlement;
    }
    public String getLocalDataQtaName() {
        return localDataQtaName;
    }
    public void setLocalDataQtaName(String localDataQtaName) {
        this.localDataQtaName = localDataQtaName;
    }
    public String getRoamingDataQtaName() {
        return roamingDataQtaName;
    }
    public void setRoamingDataQtaName(String roamingDataQtaName) {
        this.roamingDataQtaName = roamingDataQtaName;
    }
    public void setLocalVoicePeakEntitlement(String localVoicePeakEntitlement) {
        this.localVoicePeakEntitlement = localVoicePeakEntitlement;
    }
    public String getLocalVoicePeakUnit() {
        return localVoicePeakUnit;
    }
    public String getNowTVEntitlement() {
        return nowTVEntitlement;
    }
    public void setNowTVEntitlement(String nowTVEntitlement) {
        this.nowTVEntitlement = nowTVEntitlement;
    }
    public String getNowSportsEntitlement() {
        return nowSportsEntitlement;
    }
    public void setNowSportsEntitlement(String nowSportsEntitlement) {
        this.nowSportsEntitlement = nowSportsEntitlement;
    }
    public String getMoovEntitlement() {
        return moovEntitlement;
    }
    public void setMoovEntitlement(String moovEntitlement) {
        this.moovEntitlement = moovEntitlement;
    }
    public void setLocalVoicePeakUnit(String localVoicePeakUnit) {
        this.localVoicePeakUnit = localVoicePeakUnit;
    }
    public String getLocalVoiceOffPeakUsage() {
        return localVoiceOffPeakUsage;
    }
    public void setLocalVoiceOffPeakUsage(String localVoiceOffPeakUsage) {
        this.localVoiceOffPeakUsage = localVoiceOffPeakUsage;
    }
    public String getLocalVoiceOffPeakEntitlement() {
        return localVoiceOffPeakEntitlement;
    }
    public void setLocalVoiceOffPeakEntitlement(String localVoiceOffPeakEntitlement) {
        this.localVoiceOffPeakEntitlement = localVoiceOffPeakEntitlement;
    }
    public String getLocalVoiceOffPeakUnit() {
        return localVoiceOffPeakUnit;
    }
    public void setLocalVoiceOffPeakUnit(String localVoiceOffPeakUnit) {
        this.localVoiceOffPeakUnit = localVoiceOffPeakUnit;
    }
    public void setLocalVoiceInterEntitlement(String localVoiceInterEntitlement) {
        this.localVoiceInterEntitlement = localVoiceInterEntitlement;
    }
    public String getLocalVoiceInterUnit() {
        return localVoiceInterUnit;
    }
    public void setLocalVoiceInterUnit(String localVoiceInterUnit) {
        this.localVoiceInterUnit = localVoiceInterUnit;
    }
    public String getLocalSMSIntraUsage() {
        return localSMSIntraUsage;
    }
    public void setLocalSMSIntraUsage(String localSMSIntraUsage) {
        this.localSMSIntraUsage = localSMSIntraUsage;
    }
    public String getLocalSMSIntraEntitlement() {
        return localSMSIntraEntitlement;
    }
    public void setLocalSMSIntraEntitlement(String localSMSIntraEntitlement) {
        this.localSMSIntraEntitlement = localSMSIntraEntitlement;
    }
    public String getLocalSMSIntraUsageGrp() {
        return localSMSIntraUsageGrp;
    }
    public void setLocalSMSIntraUsageGrp(String localSMSIntraUsageGrp) {
        this.localSMSIntraUsageGrp = localSMSIntraUsageGrp;
    }
    public String getLocalSMSInterUsage() {
        return localSMSInterUsage;
    }
    public void setLocalSMSInterUsage(String localSMSInterUsage) {
        this.localSMSInterUsage = localSMSInterUsage;
    }
    public String getLocalSMSInterEntitlement() {
        return localSMSInterEntitlement;
    }
    public void setLocalSMSInterEntitlement(String localSMSInterEntitlement) {
        this.localSMSInterEntitlement = localSMSInterEntitlement;
    }
    public String getLocalSMSInterUsageGrp() {
        return localSMSInterUsageGrp;
    }
    public void setLocalSMSInterUsageGrp(String localSMSInterUsageGrp) {
        this.localSMSInterUsageGrp = localSMSInterUsageGrp;
    }
    public String getOverseasSMSUsage() {
        return overseasSMSUsage;
    }
    public void setOverseasSMSUsage(String overseasSMSUsage) {
        this.overseasSMSUsage = overseasSMSUsage;
    }
    public String getOverseasSMSEntitlement() {
        return overseasSMSEntitlement;
    }
    public void setOverseasSMSEntitlement(String overseasSMSEntitlement) {
        this.overseasSMSEntitlement = overseasSMSEntitlement;
    }
    public String getOverseasSMSUsageGrp() {
        return overseasSMSUsageGrp;
    }
    public void setOverseasSMSUsageGrp(String overseasSMSUsageGrp) {
        this.overseasSMSUsageGrp = overseasSMSUsageGrp;
    }
    public String getNowTVUsage() {
        return nowTVUsage;
    }
    public void setNowTVUsage(String nowTVUsage) {
        this.nowTVUsage = nowTVUsage;
    }
    public String getNowTVUnit() {
        return nowTVUnit;
    }
    public void setNowTVUnit(String nowTVUnit) {
        this.nowTVUnit = nowTVUnit;
    }
    public String getNowSportsUsage() {
        return nowSportsUsage;
    }
    public void setNowSportsUsage(String nowSportsUsage) {
        this.nowSportsUsage = nowSportsUsage;
    }
    public String getNowSportsUnit() {
        return nowSportsUnit;
    }
    public void setNowSportsUnit(String nowSportsUnit) {
        this.nowSportsUnit = nowSportsUnit;
    }
    public String getMoovUsage() {
        return moovUsage;
    }
    public void setMoovUsage(String moovUsage) {
        this.moovUsage = moovUsage;
    }
    public String getMoovUnit() {
        return moovUnit;
    }
    public void setMoovUnit(String moovUnit) {
        this.moovUnit = moovUnit;
    }
    public String getLocalDataQuotaBaseInd() {
        return localDataQuotaBaseInd;
    }
    public void setLocalDataQuotaBaseInd(String localDataQuotaBaseInd) {
        this.localDataQuotaBaseInd = localDataQuotaBaseInd;
    }
    public String getRoamingDataQuotaBaseInd() {
        return roamingDataQuotaBaseInd;
    }
    public void setRoamingDataQuotaBaseInd(String roamingDataQuotaBaseInd) {
        this.roamingDataQuotaBaseInd = roamingDataQuotaBaseInd;
    }
    public String getMobileDataRtnCd() {
        return mobileDataRtnCd;
    }
    public void setMobileDataRtnCd(String mobileDataRtnCd) {
        this.mobileDataRtnCd = mobileDataRtnCd;
    }
    public String getMobileDataRtnMsg() {
        return mobileDataRtnMsg;
    }
    public void setMobileDataRtnMsg(String mobileDataRtnMsg) {
        this.mobileDataRtnMsg = mobileDataRtnMsg;
    }
    public String getMobileLocalDataUsage() {
        return mobileLocalDataUsage;
    }
    public void setMobileLocalDataUsage(String mobileLocalDataUsage) {
        this.mobileLocalDataUsage = mobileLocalDataUsage;
    }
    public String getMobileLocalDataUnit() {
        return mobileLocalDataUnit;
    }
    public void setMobileLocalDataUnit(String mobileLocalDataUnit) {
        this.mobileLocalDataUnit = mobileLocalDataUnit;
    }
    public String getMobileRoamingDataUsage() {
        return mobileRoamingDataUsage;
    }
    public void setMobileRoamingDataUsage(String mobileRoamingDataUsage) {
        this.mobileRoamingDataUsage = mobileRoamingDataUsage;
    }
    public String getMobileLocalDataQuota() {
        return mobileLocalDataQuota;
    }
    public void setMobileLocalDataQuota(String mobileLocalDataQuota) {
        this.mobileLocalDataQuota = mobileLocalDataQuota;
    }
    public String getMobileRoamingDataQuota() {
        return mobileRoamingDataQuota;
    }
    public void setMobileRoamingDataQuota(String mobileRoamingDataQuota) {
        this.mobileRoamingDataQuota = mobileRoamingDataQuota;
    }
    public String getMobileRoamingDataUnit() {
        return mobileRoamingDataUnit;
    }
    public void setMobileRoamingDataUnit(String mobileRoamingDataUnit) {
        this.mobileRoamingDataUnit = mobileRoamingDataUnit;
    }
    public String getAsOfDate() {
        return asOfDate;
    }
    public void setAsOfDate(String asOfDate) {
        this.asOfDate = asOfDate;
    }
    public String getMobileLocalDataInitialQuota() {
        return mobileLocalDataInitialQuota;
    }
    public void setMobileLocalDataInitialQuota(String mobileLocalDataInitialQuota) {
        this.mobileLocalDataInitialQuota = mobileLocalDataInitialQuota;
    }
    public String getMobileRoamingDataInitialQuota() {
        return mobileRoamingDataInitialQuota;
    }
    public void setMobileRoamingDataInitialQuota(
            String mobileRoamingDataInitialQuota) {
        this.mobileRoamingDataInitialQuota = mobileRoamingDataInitialQuota;
    }
    public String getNonMobileUsageEntitlementRtnCode() {
        return nonMobileUsageEntitlementRtnCode;
    }
    public void setNonMobileUsageEntitlementRtnCode(
            String nonMobileUsageEntitlementRtnCode) {
        this.nonMobileUsageEntitlementRtnCode = nonMobileUsageEntitlementRtnCode;
    }
    public String getNonMobileUsageEntitlementRtnMsg() {
        return nonMobileUsageEntitlementRtnMsg;
    }
    public void setNonMobileUsageEntitlementRtnMsg(
            String nonMobileUsageEntitlementRtnMsg) {
        this.nonMobileUsageEntitlementRtnMsg = nonMobileUsageEntitlementRtnMsg;
    }
    public String getLocalVoicePeakOffpeakBasedInd() {
        return localVoicePeakOffpeakBasedInd;
    }
    public void setLocalVoicePeakOffpeakBasedInd(
            String localVoicePeakOffpeakBasedInd) {
        this.localVoicePeakOffpeakBasedInd = localVoicePeakOffpeakBasedInd;
    }
    public String getMobileLocalDataRemaining() {
        return mobileLocalDataRemaining;
    }
    public void setMobileLocalDataRemaining(String mobileLocalDataRemaining) {
        this.mobileLocalDataRemaining = mobileLocalDataRemaining;
    }
    public String getMobileLocalDataRemainingInMB() {
        return mobileLocalDataRemainingInMB;
    }
    public void setMobileLocalDataRemainingInMB(String mobileLocalDataRemainingInMB) {
        this.mobileLocalDataRemainingInMB = mobileLocalDataRemainingInMB;
    }
    public String getMobileLocalDataRemainingInPercent() {
        return mobileLocalDataRemainingInPercent;
    }
    public void setMobileLocalDataRemainingInPercent(
            String mobileLocalDataRemainingInPercent) {
        this.mobileLocalDataRemainingInPercent = mobileLocalDataRemainingInPercent;
    }
    public String getMobileLocalDataUsedInPercent() {
        return mobileLocalDataUsedInPercent;
    }
    public void setMobileLocalDataUsedInPercent(
            String mobileLocalDataUsedInPercent) {
        this.mobileLocalDataUsedInPercent = mobileLocalDataUsedInPercent;
    }
    public String getLocalMMSIntraUsage() {
        return localMMSIntraUsage;
    }
    public void setLocalMMSIntraUsage(String localMMSIntraUsage) {
        this.localMMSIntraUsage = localMMSIntraUsage;
    }
    public String getLocalMMSIntraEntitlement() {
        return localMMSIntraEntitlement;
    }
    public void setLocalMMSIntraEntitlement(String localMMSIntraEntitlement) {
        this.localMMSIntraEntitlement = localMMSIntraEntitlement;
    }
    public String getLocalMMSIntraUnit() {
        return localMMSIntraUnit;
    }
    public void setLocalMMSIntraUnit(String localMMSIntraUnit) {
        this.localMMSIntraUnit = localMMSIntraUnit;
    }
    public String getLocalMMSIntraUsageGrp() {
        return localMMSIntraUsageGrp;
    }
    public void setLocalMMSIntraUsageGrp(String localMMSIntraUsageGrp) {
        this.localMMSIntraUsageGrp = localMMSIntraUsageGrp;
    }
    public String getLocalMMSInterUsage() {
        return localMMSInterUsage;
    }
    public void setLocalMMSInterUsage(String localMMSInterUsage) {
        this.localMMSInterUsage = localMMSInterUsage;
    }
    public String getLocalMMSInterEntitlement() {
        return localMMSInterEntitlement;
    }
    public void setLocalMMSInterEntitlement(String localMMSInterEntitlement) {
        this.localMMSInterEntitlement = localMMSInterEntitlement;
    }
    public String getLocalMMSInterUnit() {
        return localMMSInterUnit;
    }
    public void setLocalMMSInterUnit(String localMMSInterUnit) {
        this.localMMSInterUnit = localMMSInterUnit;
    }
    public String getLocalMMSInterUsageGrp() {
        return localMMSInterUsageGrp;
    }
    public void setLocalMMSInterUsageGrp(String localMMSInterUsageGrp) {
        this.localMMSInterUsageGrp = localMMSInterUsageGrp;
    }
    public String getOverseasMMSUsage() {
        return overseasMMSUsage;
    }
    public void setOverseasMMSUsage(String overseasMMSUsage) {
        this.overseasMMSUsage = overseasMMSUsage;
    }
    public String getOverseasMMSEntitlement() {
        return overseasMMSEntitlement;
    }
    public void setOverseasMMSEntitlement(String overseasMMSEntitlement) {
        this.overseasMMSEntitlement = overseasMMSEntitlement;
    }
    public String getOverseasMMSUnit() {
        return overseasMMSUnit;
    }
    public void setOverseasMMSUnit(String overseasMMSUnit) {
        this.overseasMMSUnit = overseasMMSUnit;
    }
    public String getOverseasMMSUsageGrp() {
        return overseasMMSUsageGrp;
    }
    public void setOverseasMMSUsageGrp(String overseasMMSUsageGrp) {
        this.overseasMMSUsageGrp = overseasMMSUsageGrp;
    }
    public String getEmailUsage() {
        return emailUsage;
    }
    public void setEmailUsage(String emailUsage) {
        this.emailUsage = emailUsage;
    }
    public String getEmailEntitlement() {
        return emailEntitlement;
    }
    public void setEmailEntitlement(String emailEntitlement) {
        this.emailEntitlement = emailEntitlement;
    }
    public String getEmailUnit() {
        return emailUnit;
    }
    public void setEmailUnit(String emailUnit) {
        this.emailUnit = emailUnit;
    }
    public String getEmailUsageGrp() {
        return emailUsageGrp;
    }
    public void setEmailUsageGrp(String emailUsageGrp) {
        this.emailUsageGrp = emailUsageGrp;
    }

    
    public String getIddVoiceUsage() 
    {
        return iddVoiceUsage;
    }
    
    
    public void setIddVoiceUsage(String rIddVoiceUsage) 
    {
        iddVoiceUsage = rIddVoiceUsage;
    }

    
    public String getIddVoiceEntitlement() 
    {
        return iddVoiceEntitlement;
    }
    
    
    public void setIddVoiceEntitlement(String rIddVoiceEntitlement) 
    {
        iddVoiceEntitlement = rIddVoiceEntitlement;
    }

    
    public String getIddVoiceUnit() 
    {
        return iddVoiceUnit;
    }
    
    
    public void setIddVoiceUnit(String rIddVoiceUnit) 
    {
        iddVoiceUnit = rIddVoiceUnit;
    }


    public String getLocalBBUsage() 
    {
        return localBBUsage;
    }
    
    
    public void setLocalBBUsage(String rLocalBBUsage) 
    {
        localBBUsage = rLocalBBUsage;
    }

    
    public String getLocalBBEntitlement() 
    {
        return localBBEntitlement;
    }
    
    
    public void setLocalBBEntitlement(String rLocalBBEntitlement) 
    {
        localBBEntitlement = rLocalBBEntitlement;
    }


    public String getLocalBBUnit() 
    {
        return localBBUnit;
    }
    
    
    public void setLocalBBUnit(String rLocalBBUnit) 
    {
        localBBUnit = rLocalBBUnit;
    }

    
    public String getRoamBBUsage() 
    {
        return roamBBUsage;
    }
    
    
    public void setRoamBBUsage(String rRoamBBUsage) 
    {
        roamBBUsage = rRoamBBUsage;
    }

    
    public String getRoamBBEntitlement() 
    {
        return roamBBEntitlement;
    }
    
    
    public void setRoamBBEntitlement(String rRoamBBEntitlement) 
    {
        roamBBEntitlement = rRoamBBEntitlement;
    }

    
    public String getRoamBBUnit() 
    {
        return roamBBUnit;
    }
    
    
    public void setRoamBBUnit(String rRoamBBUnit) 
    {
        roamBBUnit = rRoamBBUnit;
    }

    
    public String getRoamSMSUsage() 
    {
        return roamSMSUsage;
    }
    
    
    public void setRoamSMSUsage(String rRoamSMSUsage) 
    {
        roamSMSUsage = rRoamSMSUsage;
    }

    
    public String getRoamSMSEntitlement() 
    {
        return roamSMSEntitlement;
    }
    
    
    public void setRoamSMSEntitlement(String rRoamSMSEntitlement) 
    {
        roamSMSEntitlement = rRoamSMSEntitlement;
    }

    
    public String getDataAsOfDate() 
    {
        return dataAsOfDate;
    }
    
    
    public void setDataAsOfDate(String rDataAsOfDate) 
    {
        dataAsOfDate = rDataAsOfDate;
    }
    

    public String getPrimaryFlag() 
    {
        return primaryFlag;
    }

    
    public void setPrimaryFlag(String rPrimaryFlag) 
    {
        primaryFlag = rPrimaryFlag;
    }

    
    public String getNxtBilCutoffDt() 
    {
        return nxtBilCutoffDt;
    }
    
    
    public void setNxtBilCutoffDt(String rNxtBilCutoffDt) 
    {
        nxtBilCutoffDt = rNxtBilCutoffDt;
    }
}
