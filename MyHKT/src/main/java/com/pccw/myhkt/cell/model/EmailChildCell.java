package com.pccw.myhkt.cell.model;

import android.view.View;

import com.pccw.myhkt.enums.MessageCategory;

public class EmailChildCell extends Cell {
    private MessageCategory category;
    private String date;
    private String title;
    private String content;
    private String remaining;
    private boolean isRead;
    private boolean isLastItem;
    private String path;
    private View.OnClickListener onClickListener;

    public EmailChildCell(MessageCategory category, String date, String title, String content, String remaining, boolean isRead, boolean isLastItem, String path, View.OnClickListener onClickListener) {
        type = Cell.MY_MESSAGE_CHILD;
        this.category = category;
        this.date = date;
        this.title = title;
        this.content = content;
        this.remaining = remaining;
        this.isRead = isRead;
        this.isLastItem = isLastItem;
        this.path = path;
        this.onClickListener = onClickListener;
    }

    public boolean isRead() {
        return isRead;
    }

    public void setRead(boolean read) {
        isRead = read;
    }

    public boolean isLastItem() {
        return isLastItem;
    }

    public void setLastItem(boolean lastItem) {
        isLastItem = lastItem;
    }

    public View.OnClickListener getOnClickListener() {
        return onClickListener;
    }

    public void setOnClickListener(View.OnClickListener onClickListener) {
        this.onClickListener = onClickListener;
    }

    public MessageCategory getCategory() {
        return category;
    }

    public String getDate() {
        return date;
    }

    @Override
    public String getTitle() {
        return title;
    }

    @Override
    public String getContent() {
        return content;
    }

    public String getRemaining() {
        return remaining;
    }

    public void setCategory(MessageCategory category) {
        this.category = category;
    }

    public void setDate(String date) {
        this.date = date;
    }

    @Override
    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public void setContent(String content) {
        this.content = content;
    }

    public void setRemaining(String remaining) {
        this.remaining = remaining;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }
}
