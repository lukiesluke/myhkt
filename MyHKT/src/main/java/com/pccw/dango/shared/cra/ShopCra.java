/*
    Crate for Shop
    
    Keywords
    --------
    $URL: svn://10.87.120.207/dango/rel/v16.0/src/com/pccw/dango/shared/cra/ShopCra.java $
    $Rev: 844 $
    $Date: 2016-10-17 11:54:01 +0800 (¶g¤@, 17 ¤Q¤ë 2016) $
    $Author: upd $
*/

package com.pccw.dango.shared.cra;

import java.io.Serializable;

import com.pccw.dango.shared.entity.ShopRec;


public class ShopCra extends BaseCraEx implements Serializable 
{
    private static final long serialVersionUID = -6935241167972256994L;
    
    private String                  iArea;              /* Area                                          */
    private double                  iLongitude;         /* Longitude                                     */
    private double                  iLatitude;          /* Latitude                                      */
    
    private ShopRec[]               oShopRecAry;         /* Array of Shop Rec                             */

    
    public static void main(String rArg[])
    {
        System.out.println(getVer());
        return;
    }
    
    
    public static String getVer()
    {
        return ("$URL: svn://10.87.120.207/dango/rel/v16.0/src/com/pccw/dango/shared/cra/ShopCra.java $, $Rev: 844 $");
    }

    
    public ShopCra()
    {
        initAndClear();
    }
    
    
    protected void init()
    {
        super.init();
    }
    
    
    public void clear()
    {
        super.clear();
        
        clearOShopRecAry();
        clearIArea();
        clearILongitude();
        clearILatitude();
    }
    

    public ShopCra copyFrom(ShopCra rSrc)
    {
        super.copyFrom(rSrc);
        
        setOShopRecAry(rSrc.getOShopRecAry());
        setIArea(rSrc.getIArea());
        setILongitude(rSrc.getILongitude());
        setILatitude(rSrc.getILatitude());
        return (this);
    }
    
    
    public ShopCra copyTo(ShopCra rDes)
    {
        rDes.copyFrom(this);
        return (rDes);
    }
    
    
    public ShopCra copyMe()
    {
        ShopCra                      rDes;
        
        rDes = new ShopCra();
        rDes.copyFrom(this);
        return (rDes);
    }


    public void clearIArea()
    {
        setIArea("");
    }


    public void setIArea(String rArg) 
    {
        iArea = rArg;
    }
    
    
    public String getIArea() 
    {
        return iArea;
    }

    
    public void clearOShopRecAry()
    {
        setOShopRecAry(new ShopRec[0]);
    }
    

    public void setOShopRecAry(ShopRec[] rArg) 
    {
        oShopRecAry = rArg;
    }

    
    public ShopRec[] getOShopRecAry() 
    {
        return oShopRecAry;
    }
    
    
    public void clearILongitude()
    {
        setILongitude(0);
    }


    public void setILongitude(double rArg) 
    {
        iLongitude = rArg;
    }
    
    
    public double getILongitude() 
    {
        return iLongitude;
    }

    
    public void clearILatitude()
    {
        setILongitude(0);
    }


    public void setILatitude(double rArg) 
    {
        iLatitude = rArg;
    }
    
    
    public double getILatitude() 
    {
        return iLatitude;
    }

}
