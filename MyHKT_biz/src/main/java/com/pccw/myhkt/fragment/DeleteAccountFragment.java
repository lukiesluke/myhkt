package com.pccw.myhkt.fragment;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.pccw.biz.myhkt.R;
import com.pccw.myhkt.APIsManager;
import com.pccw.myhkt.APIsResponse;
import com.pccw.myhkt.ClnEnv;
import com.pccw.myhkt.DialogHelper;
import com.pccw.myhkt.lib.ui.AAQuery;
import com.pccw.myhkt.listener.OnAccountDeleteListener;
import com.pccw.myhkt.utils.FirebaseSetting;

import java.util.Objects;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link DeleteAccountFragment#newInstance} factory method to
 * create an instance of this fragment.
 * 09025023  Luke Garces -Enable user to delete account via API request
 */
public class DeleteAccountFragment extends BaseFragment implements OnAccountDeleteListener {
    private static final String EVENT_INFO = "Terminate \"My HKT\" Account";
    private static final String ARG_PARAM1 = "param1";
    private DeleteAccountFragment me;
    private AAQuery aq;
    private OnAccountDeleteListener listener;
    private String mParamEmail;
    private FirebaseSetting firebaseSetting;

    public DeleteAccountFragment() {
    }

    public static DeleteAccountFragment newInstance(String email) {
        DeleteAccountFragment fragment = new DeleteAccountFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, email);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        try {
            listener = (OnAccountDeleteListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context + " must implement OnAccountDeleteListener");
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        me = this;
        aq = new AAQuery(getActivity());
        if (getArguments() != null) {
            mParamEmail = getArguments().getString(ARG_PARAM1);
        }
        firebaseSetting = new FirebaseSetting(this.getContext());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_delete_account, container, false);
    }

    protected void initUI() {
        //header
        aq.id(R.id.myproflogin_delete_account_header).margin(0, 0, 0, 0);
        aq.id(R.id.myproflogin_delete_account_header).getTextView().setText(getResources().getString(R.string.myhkt_delete_account_confirmation));
        aq.id(R.id.myproflogin_delete_account_header).getTextView().setTypeface(Typeface.MONOSPACE, Typeface.BOLD);
        aq.id(R.id.myproflogin_delete_account_header).textSize(getResources().getInteger(R.integer.textsize_default_int));

        aq.id(R.id.delete_label).getTextView().setTypeface(Typeface.MONOSPACE, Typeface.NORMAL);
        aq.id(R.id.delete_label).textSize(getResources().getInteger(R.integer.textsize_13));

        //button layout
        aq.norm2TxtBtns(R.id.myproflogin_changepw_btn_cancel, R.id.myproflogin_changepw_btn_update, getResString(R.string.MYHKT_BTN_CANCEL), getResString(R.string.MYHKT_BTN_PROCEED));
        aq.id(R.id.myproflogin_changepw_btn_update).clicked(this, "onClick");
        aq.id(R.id.myproflogin_changepw_btn_cancel).clicked(this, "onClick");
    }

    @SuppressLint("NonConstantResourceId")
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.myproflogin_changepw_btn_cancel:
                ((MyProfLoginMainFragment) Objects.requireNonNull(getParentFragment())).openLoginidSubFrag();
                break;
            case R.id.myproflogin_changepw_btn_update:
                int rid = Objects.requireNonNull(ClnEnv.getQualSvee()).getSveeRec().getRid();
                String sessionToken = ClnEnv.getSessTok();
                APIsManager.onDeleteAccountRequest(this, mParamEmail, rid, sessionToken, this);
                break;
        }
    }

    @Override
    public void onSuccess(APIsResponse response) throws Exception {
        Log.d("lwg", "onSuccess APIsResponse: DeleteAccountFragment");
        firebaseSetting.onTerminateAccount(EVENT_INFO);
        DialogHelper.createDeleteAccountDialog(me.getActivity(), mParamEmail, this);
    }

    @Override
    public void onFail(APIsResponse response) throws Exception {
        Log.d("lwg", "onFail APIsResponse: DeleteAccountFragment");
        if (response.getReply() != null && "RC_GWTTOK_MM".equalsIgnoreCase(response.getReply().getCode())) {
            me.displayDialog(response.getMessage(), this);
        } else {
            me.displayDialog(response.getMessage());
        }
    }

    @Override
    public void onAccountDeleted() {
        Log.d("lwg", "DeleteAccountFragment onAccountDeleted");
        listener.onAccountDeleted();
    }

    @Override
    public void onFailedDeleted() {
        Log.d("lwg", "DeleteAccountFragment onFailedDeleted");
        listener.onFailedDeleted();
    }
}
