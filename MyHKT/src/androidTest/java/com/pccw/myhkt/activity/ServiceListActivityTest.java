package com.pccw.myhkt.activity;

import android.content.Context;
import android.content.SharedPreferences;
import androidx.test.espresso.intent.Intents;
import androidx.test.rule.ActivityTestRule;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import com.pccw.myhkt.AccountHelper;
import com.pccw.myhkt.R;
import com.pccw.myhkt.base.BaseTest;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static androidx.test.platform.app.InstrumentationRegistry.getInstrumentation;
import static androidx.test.espresso.Espresso.onData;
import static androidx.test.espresso.Espresso.pressBack;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.swipeLeft;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static org.hamcrest.CoreMatchers.anything;
import static org.hamcrest.CoreMatchers.containsString;

@Ignore
@RunWith(AndroidJUnit4.class)
public class ServiceListActivityTest extends BaseTest {
    private Context context;
    SharedPreferences shared;

    @Rule
    public ActivityTestRule<MainMenuActivity> activityTestRule = new ActivityTestRule<>(MainMenuActivity.class);

    @Before
    public void setUp(){
        context = getInstrumentation().getTargetContext();
        shared = context.getSharedPreferences(
                "myhkt",
                Context.MODE_PRIVATE);
        Intents.init();
        clickLeftNavBar();
        findStringContaining(context, R.string.myhkt_LGIF_LOGIN).perform(click());
        loginMyCredentialsUAT(AccountHelper.getUATAccount7().getUsername(),
                AccountHelper.getUATAccount7().getPassword(), shared);
        gotoMainMenuItem(3);
    }

    @After
    public void tearDown() {
        executeLogout(context);
        Intents.release();
    }

    @Test
    public void testTabs() {
        findStringContaining(context, R.string.myhkt_myloginprofile_tabname);
        findStringContaining(context, R.string.myhkt_myservicelist_tabname);
        findStringContaining(context, R.string.myhkt_mycontactinfo_tabname);
    }

    @Test
    public void testDisplayUI() {
//        verify if the following items exist:
        findStringContaining(context, R.string.myhkt_myprof_title);
        backBtnExist();
        checkLiveChatBtnIsDisplayed();
        testTabs();
        findStringContaining(context, R.string.myhkt_myservicelist_tabname).perform(click());
        findStringContaining(context, R.string.myhkt_myprofservlist_header1);
        findStringContaining(context, R.string.myhkt_myprofservlist_header2);
        findStringContaining(context, R.string.myhkt_btn_alias);
        findStringContaining(context, R.string.MYHKT_BTN_CANCEL);
        findStringContaining(context, R.string.MYHKT_BTN_UPDATE);
        findComponentByID(R.id.myprofservlist_listview);
    }

    @Test
    public void testSetDisplayButton(){
        findStringContaining(context, R.string.myhkt_myservicelist_tabname).perform(click());
        findStringContaining(context, R.string.myhkt_btn_alias).perform(click());
    }

    @Test
    public void testSetDisplaySwipe() {
        findStringContaining(context, R.string.myhkt_myservicelist_tabname).perform(click());
        onData(anything())
                .inAdapterView(withId(R.id.myprofservlist_listview))
                .atPosition(0)
                .perform(swipeLeft());
    }

    @Test
    public void testServiceListCancelButton() {
        findStringContaining(context, R.string.myhkt_myservicelist_tabname).perform(click());
        findComponentByID(R.id.myprofservlist_btn_cancel).perform(click());
        clickLeftNavBar();
    }

    @Test
    public void testServiceListUpdateButton() {
        findStringContaining(context, R.string.myhkt_myservicelist_tabname).perform(click());
//        findStringContaining(context, R.string.MYHKT_BTN_UPDATE).perform(click());
        findComponentByID(R.id.myprofservlist_btn_update).perform(click());
        delay(TestTimeouts.ACTIVITY_LOAD_TIMEOUTS);
        findStringContaining(context, R.string.RSVM_DONE);
        findStringContaining(context, R.string.btn_ok).perform(click());
        findComponentByID(R.id.myprofservlist_listview);
    }

    @Test
    public void testServiceWithoutDisplayName() {
        findStringContaining(context, R.string.myhkt_myservicelist_tabname).perform(click());
        onData(containsString("asr3"))
                .inAdapterView(withId(R.id.myprofservlist_listview))
                .atPosition(0);
        findComponentByID(R.id.myprofservlist_listview, R.id.adapter_editservicelist_logo);
        findComponentByID(R.id.myprofservlist_listview, R.id.adapter_editservicelist_checkBox);
//        findComponentByID(R.id.myprofservlist_listview, R.id.adapter_editservicelist_header_alias)
//                .check(matches(compareTextView("")));
    }

    @Test
    public void testServiceWithDisplayName() {
        findStringContaining(context, R.string.myhkt_myservicelist_tabname).perform(click());
        onData(anything())
                .inAdapterView(withId(R.id.myprofservlist_listview))
                .atPosition(0)
                .perform(swipeLeft());
        onData(anything())
                .inAdapterView(withId(R.id.myprofservlist_listview))
                .atPosition(0)
                .onChildView(withId(R.id.adapter_editservicelist_edit))
                .perform(click());
        pressBack();

    }

    @Test
    public void testServiceTickBox() {
        findStringContaining(context, R.string.myhkt_myservicelist_tabname).perform(click());
        onData(anything()).inAdapterView(withId(R.id.myprofservlist_listview))
                .atPosition(0)
                .onChildView(withId(R.id.adapter_editservicelist_checkBox))
                .perform(click());
        findComponentByID(R.id.myprofservlist_btn_update).perform(click());
        delay(TestTimeouts.ACTIVITY_LOAD_TIMEOUTS);
        findStringContaining(context, R.string.btn_ok).perform(click());
        findComponentByID(R.id.myprofservlist_btn_cancel).perform(click());
        gotoMainMenuItem(0);
        clickLeftNavBar();
        gotoMainMenuItem(3);
        findStringContaining(context, R.string.myhkt_myservicelist_tabname).perform(click());
        onData(anything()).inAdapterView(withId(R.id.myprofservlist_listview))
                .atPosition(0)
                .onChildView(withId(R.id.adapter_editservicelist_checkBox))
                .perform(click());
        findComponentByID(R.id.myprofservlist_btn_update).perform(click());
        delay(TestTimeouts.ACTIVITY_LOAD_TIMEOUTS);
        findStringContaining(context, R.string.btn_ok).perform(click());
    }

}