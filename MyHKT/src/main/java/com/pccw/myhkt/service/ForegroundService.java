package com.pccw.myhkt.service;

import static android.app.PendingIntent.FLAG_IMMUTABLE;
import static android.app.PendingIntent.FLAG_UPDATE_CURRENT;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.graphics.drawable.BitmapDrawable;
import android.os.Build;
import android.os.IBinder;
import android.util.Log;

import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;

import com.pccw.myhkt.R;
import com.pccw.myhkt.activity.MainMenuActivity;
import com.pccw.myhkt.activity.SplashActivity;

public class ForegroundService extends Service {
    public static final String GCM_CHANNEL_ID_GENERAL = "GCM_CHANNEL_ID_GENERAL";

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d("lwg", "ForegroundService Class: onStartCommand");

        String input = intent.getStringExtra("message");
        String title = intent.getStringExtra("title");

        long when = System.currentTimeMillis();

        createNotificationChannel();

        Intent notificationIntent = new Intent(this, SplashActivity.class);
        PendingIntent pendingIntent;

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.S) {
            pendingIntent = PendingIntent.getActivity(this,
                    1983, notificationIntent, FLAG_IMMUTABLE | FLAG_UPDATE_CURRENT);
        } else {
            pendingIntent = PendingIntent.getActivity(this,
                    1983, notificationIntent, FLAG_UPDATE_CURRENT);
        }

        Notification notification = new NotificationCompat.Builder(this, GCM_CHANNEL_ID_GENERAL)
                .setContentTitle(title)
                .setContentText(input)
                .setSmallIcon(R.drawable.icon_notification)
                .setLargeIcon(((BitmapDrawable) this.getResources().getDrawable(R.drawable.hktappicon)).getBitmap())
                .setContentIntent(pendingIntent)
                .setDefaults(Notification.DEFAULT_ALL)
                .setBadgeIconType(NotificationCompat.BADGE_ICON_SMALL)
                .setWhen(when)
                .setAutoCancel(true)
                .build();

        startForeground(1, notification);
        return START_NOT_STICKY;
    }

    private void createNotificationChannel() {
        Log.d("lwg", "ForegroundService Class: createNotificationChannel");

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationManager notificationManager = getSystemService(NotificationManager.class);

            NotificationChannel serviceChannel = new NotificationChannel(GCM_CHANNEL_ID_GENERAL,
                    "NOTIFICATION",
                    NotificationManager.IMPORTANCE_HIGH);
            serviceChannel.setShowBadge(false);

            notificationManager.createNotificationChannel(serviceChannel);
        }
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
