package com.pccw.myhkt;

/************************************************************************
 File       : ClnEnv.java
 Desc       : Application-wide common objects holder
 Name       : ClnEnv
 Created by : Ryan Wong
 Date       : 29/11/2012

 Change History:
 Date       Modified By			Description
 ---------- ----------------	-------------------------------
 29/11/2012 Ryan Wong          	- First draft
 30/11/2012 Ryan Wong          	- Added HTTP Proxy options for HttpClient
 19/12/2012 Ryan Wong			- Added boolean shared preferences getter/setters
 31/12/2012 Ryan Wong			- Added getAppLocale and getDeviceLocale
 31/12/2012 Vincent Fung 		- Set up connect timeout in HttpClient
 07/01/2013 Ryan Wong			- Added hasSessTok()
 07/01/2013	Ryan Wong			- Rewritten to keep HeloCra and LoginCra
 07/01/2013	Ryan Wong			- Added getEncPref() and setEncPref() interface
 07/01/2013 Ryan Wong			1.) Added getAppVersion();
 								2.) Added getPref(int) and setPref(int)
 07/01/2013	Ryan Wong			- Added isLoggedIn()
 21/01/2013	Ryan Wong			- Updated isLoggedIn() to check LoginCra
 23/01/2013	Ryan Wong			- Added RPC added error message translations 
 05/03/2013	Ryan Wong			1.) Added SIP and WIP Reply Code error message mapping
 								2.) Added prefix to HTTP error message mapping 
 07/03/2013 Ryan Wong			- getRPCErrMsg added method to display HTTP-related error message (if available)
 08/03/2013 Ryan Wong			1.) fine-tuning getRPCErrMsg
 								2.) added onSaveInstanceState handling to preserve LoginCra
 11/03/2013 Ryan Wong			1.) Added preferences encryption
  								2.) Fixed getSessionLoginID and getSessionPassword not to return null values
 12/03/2013 Ryan Wong			1.) Adjusted Max Connections per Route to 5; Max Connections to 10
  								2.) Added new error message for HTTP 0 related errors
 19/03/2013 Ryan Wong			1.) Improved handling of password decryption exception
 								2.) Added Log File cleanup at 20MB
 								3.) Added dp to px conversion
 22/03/2013 Ryan Wong			- toLogFile() added exception handling for the case that SD card is not available
 25/03/2013	Ryan Wong			1.) Further added checking when dealing with log file I/O
 								2.) Added getRPCErrMsg() outputs to log file if Debug=true
 09/04/2013	Ryan Wong			- Added exception handling to cleanupLogFile()
 29/04/2013	Ryan Wong			- AddedgetSDCardErrMsg()
 31/07/2013 Ryan Wong			- Added password & security answer masking when logging to file
 28/08/2013	Ryan Wong			- Updated RegEx for masking password
 08/11/2013 Vincent Fung		- Added sessionIsPremier
 								- Modified to call common getString function for Premier
 18/11/2013 Vincent Fung		- Updated getString to call String Common function
 								- Added Force Premier Account for demo
 28/11/2013 Derek Tsui			- Added Disclaimer's Prompt Flag for Live Chat
 12/12/2013 Vincent Fung		- Added getChatIsMaintenance for getting LiveChat Server down Indicator Flag
 18/02/2014 Vincent Fung		- Added clear LineTest Result when clear Data
 09/05/2014 Vincent Fung		- Set default Language to be English when device Language is non-English and non-Chinese
 19/03/2013 Rex Liu				- Added global isMyMobFlag for change layout for MyMobile						
 29/10/2017 Abdulmoiz Esmail    - Added method getAppLocaleStatic that will be called in the application class						
 *************************************************************************/

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.net.Socket;
import java.net.UnknownHostException;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.TreeSet;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.apache.http.HttpHost;
import org.apache.http.HttpVersion;
import org.apache.http.client.HttpClient;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.params.ConnManagerParams;
import org.apache.http.conn.params.ConnPerRoute;
import org.apache.http.conn.params.ConnPerRouteBean;
import org.apache.http.conn.params.ConnRoutePNames;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;
import org.apache.http.protocol.HTTP;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.DialogInterface.OnClickListener;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Environment;
import android.provider.Settings.Secure;
import android.util.Log;
import android.widget.Toast;

import com.androidquery.callback.AjaxStatus;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.pccw.biz.myhkt.R;
import com.pccw.dango.shared.cra.BaseCraEx;
import com.pccw.dango.shared.cra.HeloCra;
import com.pccw.dango.shared.cra.LgiCra;
import com.pccw.dango.shared.cra.PlanMobCra;
import com.pccw.dango.shared.cra.ShopCra;
import com.pccw.dango.shared.entity.QualSvee;
import com.pccw.dango.shared.entity.SubnRec;
import com.pccw.dango.shared.tool.RC;
import com.pccw.myhkt.activity.LoginActivity;
import com.pccw.myhkt.fragment.BaseServiceFragment.OnServiceListener;
import com.pccw.myhkt.model.AcctAgent;
import com.pccw.myhkt.model.PushData;
import com.pccw.myhkt.service.ConnectivityChangeReceiver;

//import com.pccw.donut.shared.entity.QualCust;
//import com.pccw.donut.shared.gadget.Reply;
//import com.pccw.donut.shared.rpc.AuthCra;
//import com.pccw.donut.shared.rpc.HeloCra;
//import com.pccw.donut.shared.rpc.LoginCra;
//import com.pccw.donut.shared.rpc.ShopCra;
//import com.pccw.model.PushData;
//import com.pccw.myhkt.service.LineTestIntentService;

public class ClnEnv implements Serializable {

	private static final long			serialVersionUID		= -7067099843403015325L;			// Generated serial version ID
	private static DefaultHttpClient	httpclient				= null;								// Unified HTTPClient to propagate Session cookies

	// CSP RPC Objects - we retain the original objects
	private static HeloCra				heloCra					= null;
    private static LgiCra				lgiCra					= null;
//	private static LoginCra				loginCra				= null;
//	private static AuthCra				authCra					= null;
    private static ShopCra				shopCraHK				= null;
    private static ShopCra				shopCraKLN				= null;
    private static ShopCra				shopCraNT				= null;
    private static PushData				pushDataBill			= null;
    private static PushData				pushDataGen				= null;
    
    //derek temp (swipe list alias storage) START
    private static String[] alias = null;
    
  //Rex temp (swipe list alias storage) START
    private static String[] mymob_alias = null;
    
    //derek END

	// Shared Preferences
	private static SharedPreferences	sharedPreferences;

	// Volatile variables - keep in memory unless write to preferences explicitly
	private static String				sessionLoginID			= "";
	private static String				sessionPassword			= "";
	private static boolean				sessionSavePw			= false;
	private static boolean				sessionIsPremier		= false;
	private static boolean				isMyMobFlag				= false;
	private static boolean				is101Flag				= false;
	private static boolean				showTheClub				= false;
	private static boolean				appConfigDownloaded     = false;
	private static boolean				isUAT					= false;
//	private static boolean 				isStandalone			= false;
	// Reversible encryption
	private static final String			PBE_ALGORITHM			= "PBEWithSHA256And256BitAES-CBC-BC";
	private static final String			CIPHER_ALGORITHM		= "AES/CBC/PKCS5Padding";
	private static final int			NUM_OF_ITERATIONS		= 1000;
	private static final int			KEY_SIZE				= 256;
	private static final byte[]			iv						= "compccwmyhktcad9".getBytes();		// must be 16 bytes
	private static final String			converterDelimitator	= "##";
	
	//GCM Sender ID / Project ID
	public static final String			GCM_SENDER_ID			= "192334617687";
	
	public static boolean 				isForceCloseApp 		= false;
	public static boolean 				isVCFinish				= false;

	// Unless specified we do not use proxy
	public static final DefaultHttpClient getHttpClient() {
		return getHttpClient(false);
	}

	// The setting of proxy is fixed during the lifetime of HttpClient
	public static final DefaultHttpClient getHttpClient(boolean useProxy) {
		if (httpclient == null) {
			// Set up connection timeout
			ConnPerRoute connPerRoute = new ConnPerRouteBean(5);
			HttpParams params = new BasicHttpParams();
			ConnManagerParams.setTimeout(params, 1000);
			ConnManagerParams.setMaxConnectionsPerRoute(params, connPerRoute);
			ConnManagerParams.setMaxTotalConnections(params, 10);
			HttpConnectionParams.setConnectionTimeout(params, 60000);
			HttpConnectionParams.setSoTimeout(params, 60000);
			//original config
//			SchemeRegistry schReg = new SchemeRegistry();
//			schReg.register(new Scheme("http", PlainSocketFactory.getSocketFactory(), 80));
//			schReg.register(new Scheme("https", SSLSocketFactory.getSocketFactory(), 443));
//			ClientConnectionManager conMgr = new ThreadSafeClientConnManager(params, schReg);
//			httpclient = new DefaultHttpClient(conMgr, params);
//			if (useProxy) {
//				HttpHost proxy = new HttpHost("proxy.pccw.com", 8080);
//				httpclient.getParams().setParameter(ConnRoutePNames.DEFAULT_PROXY, proxy);
//			}
			
			//by pass start
			boolean bypass = false;
			if (bypass) {
				try {
			        KeyStore trustStore = KeyStore.getInstance(KeyStore.getDefaultType());
			        trustStore.load(null, null);

			        SSLSocketFactory mySSLSocketFactory = new MySSLSocketFactory(trustStore);
			        mySSLSocketFactory.setHostnameVerifier(SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
			        
					SchemeRegistry schReg = new SchemeRegistry();
					schReg.register(new Scheme("http", PlainSocketFactory.getSocketFactory(), 80));
					schReg.register(new Scheme("https", mySSLSocketFactory, 443));
		
					ClientConnectionManager conMgr = new ThreadSafeClientConnManager(params, schReg);
					httpclient = new DefaultHttpClient(conMgr, params);
		
					if (useProxy) {
						HttpHost proxy = new HttpHost("proxy.pccw.com", 8080);
						httpclient.getParams().setParameter(ConnRoutePNames.DEFAULT_PROXY, proxy);
					}
			    } catch (Exception e) {
			    	e.printStackTrace();
			        return null;
			    }
			} else {
				SchemeRegistry schReg = new SchemeRegistry();
				schReg.register(new Scheme("http", PlainSocketFactory.getSocketFactory(), 80));
				schReg.register(new Scheme("https", SSLSocketFactory.getSocketFactory(), 443));

				ClientConnectionManager conMgr = new ThreadSafeClientConnManager(params, schReg);
				httpclient = new DefaultHttpClient(conMgr, params);

				if (useProxy) {
					HttpHost proxy = new HttpHost("proxy.pccw.com", 8080);
					httpclient.getParams().setParameter(ConnRoutePNames.DEFAULT_PROXY, proxy);
				}
			}
			//by pass end
		}
		return httpclient;
	}
	
	public static final HttpClient getNewHttpClient() {
	    try {
	        KeyStore trustStore = KeyStore.getInstance(KeyStore.getDefaultType());
	        trustStore.load(null, null);

	        SSLSocketFactory sf = new MySSLSocketFactory(trustStore);
	        sf.setHostnameVerifier(SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);

	        HttpParams params = new BasicHttpParams();
	        HttpProtocolParams.setVersion(params, HttpVersion.HTTP_1_1);
	        HttpProtocolParams.setContentCharset(params, HTTP.UTF_8);

	        SchemeRegistry registry = new SchemeRegistry();
	        registry.register(new Scheme("http", PlainSocketFactory.getSocketFactory(), 80));
	        registry.register(new Scheme("https", sf, 443));

	        ClientConnectionManager ccm = new ThreadSafeClientConnManager(params, registry);

	        return new DefaultHttpClient(ccm, params);
	    } catch (Exception e) {
	        return new DefaultHttpClient();
	    }
	}
	
	public static final class MySSLSocketFactory extends SSLSocketFactory {
	    SSLContext sslContext = SSLContext.getInstance("TLS");
	    public MySSLSocketFactory(KeyStore truststore) throws NoSuchAlgorithmException, KeyManagementException, KeyStoreException, UnrecoverableKeyException {
	        super(truststore);
	        TrustManager tm = new X509TrustManager() {
	            public void checkClientTrusted(X509Certificate[] chain, String authType) {
	            }
	            public void checkServerTrusted(X509Certificate[] chain, String authType) throws CertificateException {
	                try {
	                    chain[0].checkValidity();
	                } catch (Exception e) {
	                    throw new CertificateException("Certificate not valid or trusted.");
	                }
	            }
	            public X509Certificate[] getAcceptedIssuers() {
	                return null;
	            }
	        };
	        sslContext.init(null, new TrustManager[] { tm }, null);
	    }

	    @Override
	    public Socket createSocket(Socket socket, String host, int port, boolean autoClose) throws IOException {
	        return sslContext.getSocketFactory().createSocket(socket, host, port, autoClose);
	    }

	    @Override
	    public Socket createSocket() throws IOException {
	        return sslContext.getSocketFactory().createSocket();
	    }
	}

	// App-wide CRA setters
	public static final void setHeloCra(HeloCra hc) {
		heloCra = hc;
	}

//
//	public static final void setLoginCra(LoginCra lc) {
//		loginCra = lc;
//	}
//	
//	public static final void setShopCraHK(ShopCra sc) {
//		shopCraHK = sc;
//	}
//	
//	public static final void setShopCraKLN(ShopCra sc) {
//		shopCraKLN = sc;
//	}
//	
//	public static final void setShopCraNT(ShopCra sc) {
//		shopCraNT = sc;
//	}
	
	public static final void setPushDataBill(PushData pd) {
		pushDataBill = pd;
	}

	public static final void setPushDataGen(PushData pd) {
		pushDataGen = pd;
	}
	
//	public static final String getChatToken() {
//		try {
//			return loginCra.getChatTok();
//		} catch (Exception e) {
//			// Typically null pointer exception
//			e.printStackTrace();
//			return null;
//		}
//	}
//	
	public static final String getChatIsMaintenance() {
		try {
			return heloCra.getOClnCfg().getProp().get("CLN_CHAT_MA");
		} catch (Exception e) {
			// Typically null pointer exception
			e.printStackTrace();
			return null;
		}
	}
//	
//	public static final LoginCra getLoginCra() {
//		return loginCra;
//	}
	
//	public static AuthCra getAuthCra() {
//		return authCra;
//	}
//
//	public static void setAuthCra(AuthCra authCra) {
//		ClnEnv.authCra = authCra;
//	}

//	public static final ShopCra	getShopCraHK() {
//		return shopCraHK;
//	}
//	
//	public static final ShopCra	getShopCraKLN() {
//		return shopCraKLN;
//	}
//	
//	public static final ShopCra	getShopCraNT() {
//		return shopCraNT;
//	}
	
	public static final PushData getPushDataBill() {
		return pushDataBill;
	}
	
	public static final PushData getPushDataGen() {
		return pushDataGen;
	}
	
	// To be called by activities to preserve heloCra and loginCra
	public static final Bundle onSaveInstanceState(Bundle outState) {
		 outState.putSerializable("CLNENV_HELOCRA", heloCra);
//		outState.putSerializable("CLNENV_LOGINCTACRA", loginCra);
		return outState;
	}

	// To be called by activities to preserve heloCra and loginCra
	public static final void onRestoreInstanceState(Bundle savedInstanceState) {
		 heloCra = (HeloCra) savedInstanceState.getSerializable("CLNENV_HELOCRA");
//		loginCra = (LoginCra) savedInstanceState.getSerializable("CLNENV_LOGINCTACRA");
	}

	public static final String getSessTok() {
		try {
			return heloCra.getOGwtGud();
		} catch (Exception e) {
			// Typically null pointer exception
			return null;
		}
	}

	public static final boolean hasSessTok() {
		try {
			if (heloCra.getOGwtGud().length() > 0) { return true; }
		} catch (Exception e) {
			// Typically null pointer exception
			// either at heloCra or heloCra.getSesstok
			// just catch and ignore
		}
		return false;
	}
//
//	public static final QualCust getQualCust() {
//		try {
//			return loginCra.getQualCust();
//		} catch (Exception e) {
//			// Typically null pointer exception
//			// just catch and ignore
//		}
//		return null;
//	}

	public static final void clear(Context cxt) {
		heloCra = null;
		lgiCra = null;

		// clearing volatile variables
		sessionLoginID = "";
		sessionPassword = "";
		
		// clearing Line Test Result if session invalid or logout
//		if (!"".equalsIgnoreCase(getPref(cxt.getApplicationContext(), cxt.getString(R.string.CONST_PREF_LNTT_AGENT), ""))) {
//			cxt.stopService(new Intent(cxt, LineTestIntentService.class));
//			Utils.clearLnttService(cxt);
//		}
	}

	// Shared Preferences
	public static final String getPref(Context cxt, String mKey, String defValue) {
		if (sharedPreferences == null) {
			sharedPreferences = cxt.getSharedPreferences("myhkt", Context.MODE_PRIVATE);
		}
		try {
			String retVal = sharedPreferences.getString(mKey, defValue);
			if ("".equalsIgnoreCase(retVal)) {
				return defValue;
			} else {
				return retVal;
			}
		} catch (ClassCastException e) {
			e.printStackTrace();
			return "";
		}
	}

	public static final boolean getPref(Context cxt, String mKey, boolean defValue) {
		if (sharedPreferences == null) {
			sharedPreferences = cxt.getSharedPreferences("myhkt", Context.MODE_PRIVATE);
		}
		try {
			return sharedPreferences.getBoolean(mKey, defValue);
		} catch (ClassCastException e) {
			e.printStackTrace();
			return false;
		}
	}

	public static final int getPref(Context cxt, String mKey, int defValue) {
		if (sharedPreferences == null) {
			sharedPreferences = cxt.getSharedPreferences("myhkt", Context.MODE_PRIVATE);
		}
		try {
			return sharedPreferences.getInt(mKey, defValue);
		} catch (ClassCastException e) {
			e.printStackTrace();
			return 0;
		}
	}

	public static final void setPref(Context cxt, String mKey, String value) {
		if (sharedPreferences == null) {
			sharedPreferences = cxt.getSharedPreferences("myhkt", Context.MODE_PRIVATE);
		}
		SharedPreferences.Editor editor = sharedPreferences.edit();
		editor.putString(mKey, value); // value to store
		editor.commit();
	}

	public static final void setPref(Context cxt, String mKey, boolean value) {
		if (sharedPreferences == null) {
			sharedPreferences = cxt.getSharedPreferences("myhkt", Context.MODE_PRIVATE);
		}
		SharedPreferences.Editor editor = sharedPreferences.edit();
		editor.putBoolean(mKey, value); // value to store
		editor.commit();
	}

	public static final void setPref(Context cxt, String mKey, int value) {
		if (sharedPreferences == null) {
			sharedPreferences = cxt.getSharedPreferences("myhkt", Context.MODE_PRIVATE);
		}
		SharedPreferences.Editor editor = sharedPreferences.edit();
		editor.putInt(mKey, value); // value to store
		editor.commit();
	}

	// (Weakly) reversible encrypted preference values
	public static final String getEncPref(Context cxt, String loginID, String mKey, String defValue) {
		if (sharedPreferences == null) {
			sharedPreferences = cxt.getSharedPreferences("myhkt", Context.MODE_PRIVATE);
		}
		try {
			String encVal = sharedPreferences.getString(mKey, defValue);

			if (loginID == null || "".equalsIgnoreCase(loginID)) {
				// if no LoginID we do not decrypt
				return encVal;
			}

			if (encVal == null || "".equalsIgnoreCase(encVal)) { return defValue; }

			byte[] encValArr = stringToByteArr(encVal);

			if (encValArr == null) {
				return defValue;
			} else {
				return decrypt(loginID, encValArr, cxt);
			}
		} catch (ClassCastException e) {
			e.printStackTrace();
			return defValue;
		}
	}

	public static final void setEncPref(Context cxt, String loginID, String mKey, String value) {
		if (sharedPreferences == null) {
			sharedPreferences = cxt.getSharedPreferences("myhkt", Context.MODE_PRIVATE);
		}
		SharedPreferences.Editor editor = sharedPreferences.edit();

		String encValue;
		if ("".equalsIgnoreCase(loginID)) {
			// if no LoginID we do not encrypt
			encValue = value;
		} else {
			encValue = byteArrToString(encrypt(loginID, value, cxt));
		}

		editor.putString(mKey, encValue); // value to store
		editor.commit();
	}
	
	//MyMob mobnum login password encryption
	public static final String getEncString(Context cxt, String encKey, String value) {
		String encValue;
		//return "" if value = empty
		if ("".equalsIgnoreCase(encKey) || "".equalsIgnoreCase(value)) {
			// if no LoginID we do not encrypt
			encValue = value;
		} else {
			encValue = byteArrToString(encrypt(encKey, value, cxt));
		}
		return encValue;
	}
	
	public static final String getDecString(Context cxt, String decKey, String value) {
		String decValue = value;
		try {
			if ("".equalsIgnoreCase(decKey) || "".equalsIgnoreCase(value)) { return decValue; }

			byte[] encValArr = stringToByteArr(decValue);

			if (encValArr == null) {
				return decValue;
			} else {
				return decrypt(decKey, encValArr, cxt);
			}
		} catch (ClassCastException e) {
			e.printStackTrace();
			return decValue;
		}
	}

	// Volatile variables
	public static final String getSessionLoginID() {
		if (sessionLoginID == null) return "";
		return sessionLoginID;
	}

	public static final String getSessionPassword() {
		if (sessionPassword == null) return "";
		return sessionPassword;
	}
	
	public static final boolean getSessionPremierFlag() {
//		// For Premier Demo, Force to show preimer Account
//		String loginID = getSessionLoginID();
//		//if (loginID.equals("keith.kk.mak@pccw.com") || loginID.equals("mrleewaiming@hotmail.com") || loginID.equals("man418maggie@yahoo.com.hk")) {
//		if (loginID.equals("man418maggie@yahoo.com.hk")) {
//			return true;
//		}
		return sessionIsPremier;
	}

	public static final void setSessionLoginID(String sid) {
		sessionLoginID = sid;
	}

	public static final void setSessionPassword(String spwd) {
		sessionPassword = spwd;
	}
	
	public static final void setSessionPremierFlag(boolean spremier) {
//		// For Premier Demo, Force to show preimer Account
//		String loginID = getSessionLoginID();
//		//if (loginID.equals("keith.kk.mak@pccw.com") || loginID.equals("mrleewaiming@hotmail.com") || loginID.equals("man418maggie@yahoo.com.hk")) {
//		if (loginID.equals("man418maggie@yahoo.com.hk")) {
//			sessionIsPremier = true;
//			return;
//		}
		sessionIsPremier = spremier;
	}

	public static final boolean isLoggedIn() {
		// TODO: rewrite isLoggedIn
        return lgiCra != null && hasSessTok();
    }

	// Getting app locale
	public static final String getAppLocale(Context cxt) {
		String defLocale = cxt.getApplicationContext().getResources().getConfiguration().locale.getLanguage();
		// Handled Non-Chinese and Non-English case, default set "en"
		if (!cxt.getResources().getString(R.string.CONST_LOCALE_ZH).equalsIgnoreCase(defLocale)) {
			defLocale = cxt.getResources().getString(R.string.CONST_LOCALE_EN);
		}
		return getPref(cxt, cxt.getResources().getString(R.string.CONST_PREF_LOCALE), defLocale);
	}
	
	public static final String getAppLocaleStatic(Context cxt) {
		String defLocale = LocaleHelper.getLanguage(cxt);
		// Handled Non-Chinese and Non-English case, default set "en"
		if (!cxt.getResources().getString(R.string.CONST_LOCALE_ZH).equalsIgnoreCase(defLocale)) {
			defLocale = cxt.getResources().getString(R.string.CONST_LOCALE_EN);
		}
		return getPref(cxt, cxt.getResources().getString(R.string.CONST_PREF_LOCALE), defLocale);
	}

	// Getting device locale
	public static final String getDeviceLocale(Context cxt) {
		return cxt.getApplicationContext().getResources().getConfiguration().locale.getLanguage();
	}

	public static final void updateUILocale(Context cxt, String loc) {
		Locale locale = new Locale(loc);
		Locale.setDefault(locale);
		Configuration config = new Configuration();
		config.locale = locale;
		cxt.getResources().updateConfiguration(config, null);
	}

	// Getting current App version
	public static final int getAppVersion(Context cxt) {
		int v = 0;
		try {
			v = cxt.getPackageManager().getPackageInfo(cxt.getPackageName(), 0).versionCode;
		} catch (NameNotFoundException e) {
			// Not expecting this to happen
		}
		return v;
	}

	// Getting RPC Error messages
	// Currently we use only one general error message for all messages other than IVSESS
	public static final String getRPCErrMsg(Context cxt, String errcode) {
		String retStr = "";
		// TODO: rewrite getRPCErrMsg
		if (errcode.equals(RC.GWTGUD_MM)) {
			retStr = Utils.getString(cxt, R.string.DLGM_ABORT_IVSESS);
		} else if (errcode.equals(RC.SVEE_ICLOGINID)) {
			retStr = Utils.getString(cxt, R.string.LGIM_IVLOGIN_ID);
		} else if (errcode.equals(RC.LOGIN_FAIL)) {
			retStr = Utils.getString(cxt, R.string.LGIM_LOGIN_FAIL);
		} else if (errcode.equals(RC.WIP)) {
			retStr = Utils.getString(cxt, R.string.SHLM_WIP);
		} else if (errcode.equals(RC.SIP)) {
			retStr = Utils.getString(cxt, R.string.SHLM_SIP);
		} else if (errcode.equals(RC.INACTIVE_CUST)) {
			retStr = Utils.getString(cxt, R.string.LGI_INACTIVE_CUST);
		} else if (errcode.equals(RC.INACTIVE_SVEE)) {
			retStr = Utils.getString(cxt, R.string.LGI_INACTIVE_SVEE);
		} else if (errcode.equals(RC.NOT_SUPP_PHYLUM)) {
			retStr = Utils.getString(cxt, R.string.RC_NOT_SUPP_PHYLUM);
		} else if(errcode.equals(RC.NOT_SUPP_SEG)) {
			retStr = Utils.getString(cxt,  R.string.RC_NOT_SUPP_SEG);
		} else if (errcode.equals(RC.TCSESS_EXP)) {
			retStr = Utils.getString(cxt, R.string.DLGM_ABORT_SESSEND);
		} else if (errcode.equals(RC.NOT_AUTH)) {
			if (cxt instanceof OnServiceListener) {
				int lob = ((OnServiceListener) cxt).getLob();
				if (lob == R.string.CONST_LOB_1010 || lob == R.string.CONST_LOB_IOI) {
					return Utils.getString(cxt, R.string.PLNMBM_NOTAUT_101); 
				} else {
					return Utils.getString(cxt, R.string.PLNMBM_NOTAUT_GNL); 
				}
			} else {
				retStr = Utils.getString(cxt, R.string.PLNMBM_NOTAUT_GNL);
			}
		} else if (errcode.equals(RC.USRB_EAT_UNSUCC)) {
			retStr = String.format("%s: %s", Utils.getString(cxt, R.string.APPTM_MAINT_EAT_UNSUCC), errcode);
		} else {
			retStr = String.format("%s: %s", Utils.getString(cxt, R.string.DLGM_RC_GENERAL), errcode);
		} 

		if (cxt.getResources().getBoolean(R.bool.DEBUG)) toLogFile("getRPCErrMsg", retStr);
		return retStr;
	}
	
	// Getting RPC (HTTP transport) with code and Error messages
	public static final String getRPCErrMsg(Context cxt, int errcode, String errstr) {
		Log.i("ClnEnv", errcode+"");
		if (errstr != null && errstr.length() > 0) {
			//Use errstr for error code
			if (errcode == -1) {
				String retStr = String.format(Locale.US, "%s: %s", Utils.getString(cxt, R.string.DLGM_RC_GENERAL), errstr);
				return retStr;
			} else if (errcode != 0 && errcode != AjaxStatus.NETWORK_ERROR) {
				String retStr = String.format(Locale.US, "%s: HTTP %d - %s", Utils.getString(cxt, R.string.DLGM_RC_GENERAL), errcode, errstr);
				if (cxt.getResources().getBoolean(R.bool.DEBUG)) toLogFile("getRPCErrMsg", retStr);
				return retStr;
			} else {
				// HTTP 0 - Customer handset issues
				//Note that AQuery turn network error code 0 to -101(AjaxStatus.NETWORK_ERROR)
//				String retStr = String.format(Locale.US, "%s: %s", Utils.getString(cxt, R.string.myhkt_HANDSET_RC_GENERAL), errstr);
				String retStr = String.format(Locale.US, "%s", Utils.getString(cxt, R.string.DLGM_RC_GENERAL) + ": TM_60"); //API call over 60 seconds timeout
				if (!ConnectivityChangeReceiver.isConnected(cxt)) {
					retStr = String.format(Locale.US, "%s", Utils.getString(cxt, R.string.myhkt_HANDSET_RC_GENERAL)); // no internet connection
				}
				
				if (cxt.getResources().getBoolean(R.bool.DEBUG)) toLogFile("getRPCErrMsg", retStr);
				return retStr;
			}
		} else {
			String retStr = getRPCErrMsg(cxt, errcode);
			if (cxt.getResources().getBoolean(R.bool.DEBUG)) toLogFile("getRPCErrMsg", retStr);
			return retStr;
		}
	}

	// Getting RPC (HTTP transport) with code
	private static final String getRPCErrMsg(Context cxt, int errcode) {
		if (errcode != 0) {
			return String.format(Locale.US, "%s: HTTP %d", Utils.getString(cxt, R.string.DLGM_RC_GENERAL), errcode);
		} else {
			// HTTP 0 - Customer handset issues
			if (ConnectivityChangeReceiver.isConnected(cxt)) {
				return Utils.getString(cxt, R.string.DLGM_RC_GENERAL) + ": TM_60"; //API call over 60 seconds timeout
			} else {
				return Utils.getString(cxt, R.string.myhkt_HANDSET_RC_GENERAL); // no internet connection
			}
		}
	}


	// Encryption routines
	public static final byte[] encrypt(String key, String clearText, Context cxt) {
		byte[] encryptedText;
		byte[] salt;

		try {
			salt = getDeviceID(cxt).getBytes();
			PBEKeySpec pbeKeySpec = new PBEKeySpec(key.toCharArray(), salt, NUM_OF_ITERATIONS, KEY_SIZE);
			SecretKeyFactory keyFactory = SecretKeyFactory.getInstance(PBE_ALGORITHM);
			SecretKey tempKey = keyFactory.generateSecret(pbeKeySpec);

			SecretKey secretKey = new SecretKeySpec(tempKey.getEncoded(), "AES");
			IvParameterSpec ivSpec = new IvParameterSpec(iv);
			Cipher encCipher = Cipher.getInstance(CIPHER_ALGORITHM);
			encCipher.init(Cipher.ENCRYPT_MODE, secretKey, ivSpec);
			encryptedText = encCipher.doFinal(clearText.getBytes());

			return encryptedText;

		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public static final String decrypt(String key, byte[] encryptedText, Context cxt) {
		byte[] decryptedText;
		byte[] salt;

		try {
			salt = getDeviceID(cxt).getBytes();
			PBEKeySpec pbeKeySpec = new PBEKeySpec(key.toCharArray(), salt, NUM_OF_ITERATIONS, KEY_SIZE);
			SecretKeyFactory keyFactory = SecretKeyFactory.getInstance(PBE_ALGORITHM);
			SecretKey tempKey = keyFactory.generateSecret(pbeKeySpec);

			SecretKey secretKey = new SecretKeySpec(tempKey.getEncoded(), "AES");
			IvParameterSpec ivSpec = new IvParameterSpec(iv);
			Cipher decCipher = Cipher.getInstance(CIPHER_ALGORITHM);
			decCipher.init(Cipher.DECRYPT_MODE, secretKey, ivSpec);

			decryptedText = decCipher.doFinal(encryptedText);

			return new String(decryptedText, "utf-8");

		} catch (Exception e) {
			e.printStackTrace();
		}
		// We do not want to return null
		return "";
	}

	private static final String byteArrToString(byte[] byteArr) {
		String retval = "";
		int arrlen = 0;

		if (byteArr != null) {
			arrlen = byteArr.length;
		}

		for (int iCount = 0; iCount < arrlen; iCount++) {
			retval += Integer.toString(((int) byteArr[iCount]));
			if (iCount != (arrlen - 1)) {
				retval += converterDelimitator;
			}
		}

		return retval;
	}

	private static final byte[] stringToByteArr(String strin) {
		String[] str1 = strin.split(converterDelimitator);
		int arrlen = str1.length;
		byte[] retval = new byte[arrlen];

		try {
			for (int iCount = 0; iCount < arrlen; iCount++) {
				retval[iCount] = (byte) Integer.parseInt(str1[iCount]);
			}

			return retval;
		} catch (Exception e) {
			// The given string is not encoded in the given format
			// This means the input is not encrypted by us
			return null;
		}
	}


	
	private static final String getDeviceID(Context cxt) {
		String aid = Secure.getString(cxt.getContentResolver(), Secure.ANDROID_ID);

		if (aid == null || aid.length() == 0) {
			return "1234ABDF6431CDEA";	// Arbitrary mock Android ID if we cannot get one;
		} else {
			return aid;
		}
	}

	// API Debug Logging - to file
	// !!! NOT FOR PRODUCTION. TURN THIS ON ONLY FOR TESTING BUILD !!!
	public static final void toLogFile(String source, String logline) {
		// File location
		File file;
		try {
			file = new File(Environment.getExternalStorageDirectory(), "myhktdebug.txt");
		} catch (NullPointerException e) {
			// Cannot even create the file. Logging disabled to prevent crashing.
			e.printStackTrace();
			return;
		}

		// formatting
		Calendar cal = Calendar.getInstance();
		
		// Mask all password and security answer entries before logging to file
		String processedLogLine
		= logline.replaceAll("currPwd\":([ ]?)\"([^\"]*)\",", "currPwd\": \"***\",")
				.replaceAll("pwd\":([ ]?)\"([^\"]*)\",", "pwd\": \"***\",")
				.replaceAll("sec_ans\":([ ]?)\"([^\"]*)\",", "sec_ans\": \"***\",");
		String output = String.format("\r\n\r\n[%s] %s\r\n%s", source, cal.getTime().toString(), processedLogLine);

		FileOutputStream fileOutputStream = null;
		try {
			fileOutputStream = new FileOutputStream(file, true);

			if (file.exists()) {
				file.createNewFile();
			}

			fileOutputStream.write(output.getBytes(), 0, output.getBytes().length);
			fileOutputStream.flush();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (fileOutputStream != null) {
					fileOutputStream.flush();
					fileOutputStream.close();
				}
				fileOutputStream = null;
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	// DIP to PX
	public static final float dipToPx(Context cxt, int dp) {
		final float scale = cxt.getResources().getDisplayMetrics().density;
		int px = (int) (dp * scale + 0.5f);

		return px;
	}
	
	public static String getTrackerId(Context cxt, boolean isPPS) {
		if (ClnEnv.isMyMobFlag()) {
			if (isPPS) {
//				return R.string.MYHKT_GA_TRACKER_MYMOB_2;
				return ClnEnv.getPref(cxt, cxt.getString(R.string.CONST_PREF_MM_TRACKER2), APIsManager.PRD_mm_tracker2);
			} else {
//				return R.string.MYHKT_GA_TRACKER_MYMOB_1;
				return ClnEnv.getPref(cxt, cxt.getString(R.string.CONST_PREF_MM_TRACKER1), APIsManager.PRD_mm_tracker1);
			}
		} 			
		if (isPPS) {
//			return R.string.MYHKT_GA_TRACKER_2;
			return ClnEnv.getPref(cxt, cxt.getString(R.string.CONST_PREF_TRACKER2), APIsManager.PRD_tracker2);

		} else {
//			return R.string.MYHKT_GA_TRACKER_1;
			return ClnEnv.getPref(cxt, cxt.getString(R.string.CONST_PREF_TRACKER1), APIsManager.PRD_tracker1);
		}
	}

	public static final void cleanupLogFile() {
		File file = null;
		try {
			file = new File(Environment.getExternalStorageDirectory(), "myhktdebug.txt");
		} catch (NullPointerException e) {
			// Cannot even create the file. Logging disabled to prevent crashing.
			e.printStackTrace();
		}

		if (file != null) {
			long length = file.length();

			// 20MB
			if (length > 20971520) {
				file.delete();
			}
		}
	}
	
	public static final SubnRec[] getAssocSubnRecAry(SubnRec[] subnRecAry) {
		List<SubnRec> assocSubnRecList = new ArrayList();
		SubnRec[] assocSubnRecAry = new SubnRec[0];
		for (int i = 0; i < subnRecAry.length; i++) {
			if (subnRecAry[i].isAssoc()) {
				assocSubnRecList.add(subnRecAry[i]);
			}
		}
		return assocSubnRecList.toArray(new SubnRec[assocSubnRecList.size()]);
	}

	
	public static String[] getAlias() {
		return alias;
	}

	public static void setAlias(String[] alias) {
		ClnEnv.alias = alias;
	}

	public static boolean isMyMobFlag() {
		return isMyMobFlag;
	}

	public static void setMyMobFlag(boolean isMyMobFlag) {
		ClnEnv.isMyMobFlag = isMyMobFlag;
	}

	public static String[] getMymob_alias() {
		return mymob_alias;
	}

	public static void setMymob_alias(String[] mymob_alias) {
		ClnEnv.mymob_alias = mymob_alias;
	}

	public static boolean isIs101Flag() {
		return is101Flag;
	}

	public static void setIs101Flag(boolean is101Flag) {
		ClnEnv.is101Flag = is101Flag;
	}

	public static boolean isShowTheClub() {
		return showTheClub;
	}

	public static void setShowTheClub(boolean showTheClub) {
		ClnEnv.showTheClub = showTheClub;
	}

	public static LgiCra getLgiCra() {
		return lgiCra;
	}

	public static void setLgiCra(LgiCra lgiCra) {
		ClnEnv.lgiCra = lgiCra;
	}
	
	public static final QualSvee getQualSvee() {
		try {
			return lgiCra.getOQualSvee();
		} catch (Exception e) {
			// Typically null pointer exception
			// just catch and ignore
		}
		return null;
	}

	public static ShopCra getShopCraHK() {
		return shopCraHK;
	}

	public static void setShopCraHK(ShopCra shopCraHK) {
		ClnEnv.shopCraHK = shopCraHK;
	}

	public static ShopCra getShopCraKLN() {
		return shopCraKLN;
	}

	public static void setShopCraKLN(ShopCra shopCraKLN) {
		ClnEnv.shopCraKLN = shopCraKLN;
	}

	public static ShopCra getShopCraNT() {
		return shopCraNT;
	}

	public static void setShopCraNT(ShopCra shopCraNT) {
		ClnEnv.shopCraNT = shopCraNT;
	}

	public static boolean isSessionSavePw() {
		return sessionSavePw;
	}

	public static void setSessionSavePw(boolean sessionSavePw) {
		ClnEnv.sessionSavePw = sessionSavePw;
	}
	
	public static String getLoginId() {
		if (ClnEnv.getLgiCra() == null) { return null; }
		if (ClnEnv.getLgiCra().getOQualSvee() == null) { return null; }
		if (ClnEnv.getLgiCra().getOQualSvee().getSveeRec() == null) { return null; }
		return ClnEnv.getLgiCra().getOQualSvee().getSveeRec().loginId;
	}
	
	public static boolean isAppConfigDownloaded() {
		return appConfigDownloaded;
	}

	public static void setAppConfigDownloaded(boolean appConfigDownloaded) {
		ClnEnv.appConfigDownloaded = appConfigDownloaded;
	}
	
	public static boolean isUAT() {
		return isUAT;
	}

	public static void setUAT(boolean isUAT) {
		ClnEnv.isUAT = isUAT;
	}
	
//	public static boolean isStandalone() {
//		return isStandalone;
//	}
//
//	public static void setStandalone(boolean isStandalone) {
//		ClnEnv.isStandalone = isStandalone;
//	}



	//Get json obj
	public static <T> T getJSONObject (Activity act, final Class<T> typeClass, String path){
		
		
		StringBuilder buf=new StringBuilder();
		InputStream json;
		String str;
		String jsonS = null;
		try {
			json = act.getAssets().open(path);

			BufferedReader in=   new BufferedReader(new InputStreamReader(json, "UTF-8"));
			while ((str=in.readLine()) != null) {
				buf.append(str);
			}
			jsonS = new String(buf);
			in.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Gson	gson2 = new GsonBuilder().serializeNulls().create();
		T object = gson2.fromJson(jsonS, typeClass);
		return object;
	}
	
	//Get json obj
		public static String getJSONString (Context act, String path){
			
			
			StringBuilder buf=new StringBuilder();
			InputStream json;
			String str;
			String jsonS = null;
			try {
				json = act.getAssets().open(path);

				BufferedReader in=   new BufferedReader(new InputStreamReader(json, "UTF-8"));
				while ((str=in.readLine()) != null) {
					buf.append(str);
				}
				jsonS = new String(buf);
				in.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return jsonS;
		}
	
	public final static boolean isMyMobMyAccExist() {
//		TreeSet<Integer> sectionHeader = null;
//
//		sectionHeader = new TreeSet<Integer>();
//		AcctAgent rAcctAgent;
//		List<AcctAgent> rAcctAgentLst;
		int rx, /*ri,*/ rl;

//		rAcctAgentLst = new ArrayList<AcctAgent>();
		
		if (ClnEnv.isLoggedIn()) {
			// copy all SubnRec to acctAgent, but add ASSOC AcctAgent to list ONLY
			rl = ClnEnv.getLgiCra().getOQualSvee().getSubnRecAry().length;
			
			for (rx = 0; rx < rl; rx++) {
				//If MyAccount has data, add header first
				// Adding MOB/O2F/101 service only for MyMobile
				if (((SubnRec.LOB_MOB.equalsIgnoreCase(ClnEnv.getLgiCra().getOQualSvee().getSubnRecAry()[rx].lob)) ||
					(SubnRec.LOB_IOI.equalsIgnoreCase(ClnEnv.getLgiCra().getOQualSvee().getSubnRecAry()[rx].lob)) ||
					(SubnRec.LOB_101.equalsIgnoreCase(ClnEnv.getLgiCra().getOQualSvee().getSubnRecAry()[rx].lob)) ||
					(SubnRec.LOB_O2F.equalsIgnoreCase(ClnEnv.getLgiCra().getOQualSvee().getSubnRecAry()[rx].lob))) &&
					!ClnEnv.getLgiCra().getOQualSvee().getSubnRecAry()[rx].isZombie()) {
					return true;
				}
			}
			return false;
		}
		return false;
	}
	
}