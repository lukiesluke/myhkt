package com.pccw.myhkt.fragment;

import static com.pccw.myhkt.APIsManager.IDDRATEDEST;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView.ScaleType;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.RelativeLayout;
import android.widget.ScrollView;

import androidx.annotation.NonNull;

import com.pccw.dango.shared.tool.RC;
import com.pccw.myhkt.APIsManager;
import com.pccw.myhkt.APIsResponse;
import com.pccw.myhkt.ClnEnv;
import com.pccw.myhkt.DialogHelper;
import com.pccw.myhkt.R;
import com.pccw.myhkt.Utils;
import com.pccw.myhkt.activity.BaseActivity;
import com.pccw.myhkt.cell.model.Cell;
import com.pccw.myhkt.cell.model.SmallTextCell;
import com.pccw.myhkt.cell.model.TextIconCell;
import com.pccw.myhkt.cell.view.CellViewAdapter;
import com.pccw.myhkt.dialogs.idd.SearchListDialog;
import com.pccw.myhkt.fragment.PlanLTSMainFragment.OnPlanLtsListener;
import com.pccw.myhkt.lib.ui.AAQuery;
import com.pccw.myhkt.model.AcctAgent;
import com.pccw.myhkt.model.Destination;
import com.pccw.myhkt.model.Rate;
import com.pccw.myhkt.model.SRPlan;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Objects;


/************************************************************************
 * File : IDDRatesFragment.java
 * Desc : 
 * Name : IDDRatesFragment
 * by 	: 
 * Date : 
 * 
 * Change History:
 * Date       Modified By		Description
 * ---------- ----------------	-------------------------------
 * 06/23/2017  Abdulmoiz Esmail	- Moved the checking of language on the onAttach method and on the onCreateView method,
 *  							  checked if the locale  has changed to default English (due to webviews in Android 
 *  							  higher or equal 24), if does switch to Chinese. 
 *************************************************************************/
public class IDDRatesFragment extends BaseFragment{

	private final String TAG = "IDDRatesFragment";
	private IDDRatesFragment me;
	private AAQuery aq;

	// CallBack
	private OnIDDRatesListener callback;
	private Boolean isZh;
	private Boolean isResultShown = false;
	private int extralinespace;
	private int textViewHeight;
	private int textViewHeightidd;
	private ArrayList<Rate> rateList = new ArrayList<>();
	private SRPlan srPlan;
	private ArrayList<Destination> destList;
	private LinearLayout frame;
	private ArrayList<Cell> cellList;
	private CellViewAdapter cellViewAdapter;
	private Boolean isFromPlanLts = false;
	private OnPlanLtsListener callback_lts;
	private String serviceNumber;
	private String customerNumber;
	private SearchListDialog searchListDialog;
	private Destination destination;

	public interface OnIDDRatesListener {
		AcctAgent getAcctAgent();

		String getSrvNum();

		String getCustNum();
	}

	@Override
	public void onAttach(@NonNull Activity activity) {
		super.onAttach(activity);

		isZh = "zh".equalsIgnoreCase(getResString(R.string.myhkt_lang));
		if (getParentFragment() != null && getParentFragment() instanceof PlanLTSFragment) {
			try {
				callback_lts = (OnPlanLtsListener) this.getParentFragment();
				isFromPlanLts = true;
			} catch (ClassCastException e) {
				isFromPlanLts = false;
				if (debug) Log.i(TAG, "not from lts");
			}
		}

		try {
			callback = (OnIDDRatesListener) (isFromPlanLts ? getParentFragment() : activity);
		} catch (ClassCastException e) {
			throw new ClassCastException((isFromPlanLts ? getParentFragment().toString() : activity.toString()) + " must implement OnIDDRatesListener");
		}
	}

	public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		me = this;
		View fragmentLayout = inflater.inflate(R.layout.fragment_iddrates, container, false);
		aq = new AAQuery(fragmentLayout);

		//Switch to Zh if the locale changed to En and os is >=24
		if (Build.VERSION.SDK_INT >= 24 && isZh && "en".equalsIgnoreCase(getString(R.string.myhkt_lang))) {
			Utils.switchToZhLocale(getActivity());
		}

		initData();
		initUI();
		return fragmentLayout;
	}

	protected void initData() {
		extralinespace = (int) getResources().getDimension(R.dimen.extralinespace);
		basePadding = (int) getResources().getDimension(R.dimen.basePadding);
		textViewHeight = (int) getResources().getDimension(R.dimen.textviewheight);
		textViewHeightidd = (int) getResources().getDimension(R.dimen.textviewheightidd);
		cellViewAdapter = new CellViewAdapter(requireActivity());

		if (callback != null) {
			if (callback.getSrvNum() != null) {
				serviceNumber = callback.getSrvNum();
			}
			if (callback.getCustNum() != null) {
				customerNumber = callback.getCustNum();
			}
			Utils.setCrashlytics(callback.getAcctAgent());
		}
	}

	protected void initUI() {
		//IDD0060 Title
		if (isFromPlanLts) {
			aq.id(R.id.fragment_iddrates_plan_lts_frame).backgroundColorId(R.color.white);
			aq.marginpx(R.id.fragment_iddrates_plan_lts_frame, 0, 0, 0, 0);
			frame = (LinearLayout) aq.id(R.id.fragment_iddrates_plan_lts_frame).getView();
			cellList = new ArrayList<>();

			SmallTextCell smallTextCell = new SmallTextCell(getResString(R.string.myhkt_CALL_RATE), "");
			smallTextCell.setTitleTypeface(Typeface.BOLD);
			smallTextCell.setTitleColorId(R.color.black);
			smallTextCell.setLeftPadding(basePadding);
			smallTextCell.setRightPadding(basePadding);
			cellList.add(smallTextCell);

			TextIconCell textIconCell = new  TextIconCell(R.drawable.logo_fixedline_0060_1, "IDD");
			textIconCell.setLeftPadding(basePadding);
			textIconCell.setRightPadding(basePadding);
			cellList.add(textIconCell);

			cellViewAdapter.setView(frame, cellList);
			OnClickListener iddClick = new OnClickListener(){

				@Override
				public void onClick(View v) {
					callback_lts.setActiveSubView(R.string.CONST_SELECTEDVIEW_LTSPLAN_MAIN1);
					callback_lts.displayChildview(2);
				}
			};

			Drawable rightDrawable = getResources().getDrawable(R.drawable.btn_details);
		
			aq.normText(R.id.fragment_iddrates_detailclick, getResString(R.string.PLNLTF_TPL1), -2);
			aq.padding(R.id.fragment_iddrates_detailclick, basePadding, 0, basePadding , 0);
			aq.id(R.id.fragment_iddrates_detailclick).clicked(iddClick).textColorId(R.color.hkt_textcolor).height(ViewGroup.LayoutParams.WRAP_CONTENT).width(ViewGroup.LayoutParams.WRAP_CONTENT);
			int detailBtnPadding = (int) getResources().getDimension(R.dimen.detail_btn_padding);
			aq.id(R.id.fragment_iddrates_detailclick).getTextView().setCompoundDrawablesWithIntrinsicBounds(null, null, rightDrawable, null);
			aq.id(R.id.fragment_iddrates_detailclick).getTextView().setCompoundDrawablePadding(detailBtnPadding);
		}

		aq.normTextGrey(R.id.fragment_iddrates_header1, getResString(R.string.MYHKT_IDDRATE_HEADER1), -2);
		aq.padding(R.id.fragment_iddrates_header1, basePadding, 0, basePadding, 0);
		aq.gravity(R.id.fragment_iddrates_header1, Gravity.BOTTOM);

		aq.padding(R.id.fragment_iddrates_header2_layout, basePadding, 0, basePadding, 0);
		aq.normText(R.id.fragment_iddrates_header2_layout_txt1, serviceNumber ,2);
		aq.normTextGrey(R.id.fragment_iddrates_header2_layout_txt2, " " + getResString(R.string.MYHKT_IDDRATE_HEADER2) , -2);

		//Select country
		aq.normTextBlue(R.id.fragment_iddrates_country_layout_name, getResString(R.string.MYHKT_IDDRATE_SEL_COUNTRY));
		aq.normTextBlue(R.id.fragment_iddrates_country_layout_desc, "", -2);
		aq.padding(R.id.fragment_iddrates_country_layout, basePadding, 0, basePadding, 0);
		aq.id(R.id.fragment_iddrates_country_layout).clicked(this, "onClick");

		aq.id(R.id.fragment_iddrates_country_layout_arrow).image(R.drawable.btn_arrowdown);
		aq.gravity(R.id.fragment_iddrates_country_layout_arrow , Gravity.CENTER);

		//Result Time header
		aq.id(R.id.fragment_iddrates_result_header_layout).visibility(isResultShown? View.VISIBLE :View.GONE);
		aq.id(R.id.fragment_iddrates_result_header_layout_txt1).image(R.drawable.arrow).height(textViewHeight, false);
		aq.id(R.id.fragment_iddrates_result_header_layout_txt1).getImageView().setScaleType(ScaleType.CENTER);
		aq.normText(R.id.fragment_iddrates_result_header_layout_txt2, getResString(R.string.MYHKT_IDDRATE_ON) + " " + DateFormat.format("dd/MM/yy", Calendar.getInstance()) + " " + getResString(R.string.MYHKT_IDDRATE_DMY));
		aq.normTextGrey(R.id.fragment_iddrates_result_header_layout_txt3, getResString(R.string.MYHKT_IDDRATE_PER_MIN) , -2);
		aq.gravity(R.id.fragment_iddrates_result_header_layout_txt3, Gravity.RIGHT|Gravity.CENTER);
		aq.padding(R.id.fragment_iddrates_result_header_layout, basePadding, 0, basePadding, 0);

		((LinearLayout)aq.id(R.id.fragment_iddrates_result_frame).getView()).removeAllViews();

		if (isResultShown) {
			setResult();
		}

		aq.line(R.id.fragment_iddrates_line1);
		aq.line(R.id.fragment_iddrates_line2);

		aq.marginpx(R.id.fragment_iddrates_line1, basePadding, 0, basePadding, 0);
		aq.marginpx(R.id.fragment_iddrates_line2, basePadding, 0, basePadding, 0);

		//Remark textview
		aq.normTextGrey(R.id.fragment_iddrates_remark_txt, "", -2);
		aq.padding(R.id.fragment_iddrates_remark_txt, basePadding, 0, basePadding, 0);

		//Remark
		aq.id(R.id.fragment_iddrates_result_scrollview).visibility(isResultShown ? View.VISIBLE : View.INVISIBLE);
		aq.id(R.id.fragment_iddrates_remark).visibility(isResultShown ? View.VISIBLE : View.INVISIBLE);
		aq.marginpx(R.id.fragment_iddrates_remark, basePadding, extralinespace, basePadding, extralinespace);
		aq.id(R.id.fragment_iddrates_remark).getWebView().getSettings().setDefaultTextEncodingName("utf-8");
		aq.id(R.id.fragment_iddrates_remark).getWebView().loadUrl(isZh ? "file:///android_asset/idd_rate_remarks_zh.html" : "file:///android_asset/idd_rate_remarks_en.html");
	}

	public void onClick(View v) {
		if (v.getId() == R.id.fragment_iddrates_country_layout) {
			openPicker();
		}
	}

	/**********************************
	 * 
	 * Destination part
	 * 
	 ***********************************/
	private void openPicker() {
		searchListDialog = new SearchListDialog(this.requireContext(),
				aq.id(R.id.fragment_iddrates_country_layout_name).getText().toString(),
				aq.id(R.id.fragment_iddrates_country_layout_desc).getText().toString(),
				destList, DialogInterface::cancel, DialogInterface::dismiss,
				v -> searchListDialog.cancel(),
				v -> {
					Destination destination = searchListDialog.getSelectedDestination();
					if (destination == null) {
						setDestination(destination);
						setDest(getResString(R.string.MYHKT_IDDRATE_SEL_COUNTRY), "");
						clearResult();
						searchListDialog.dismiss();
					} else if (isZh ? destination.chiDestName.equals(getContext().getString(R.string.MYHKT_IDDRATE_SEL_COUNTRY)) :
							destination.engDestName.equals(getContext().getString(R.string.MYHKT_IDDRATE_SEL_COUNTRY))) {
						setDestination(destination);
						setDest(getResString(R.string.MYHKT_IDDRATE_SEL_COUNTRY), "");
						clearResult();
						searchListDialog.dismiss();
					} else {
						setDestination(destination);
						setDest(isZh ? destination.chiDestName : destination.engDestName,
								isZh ? destination.chiDestNameExt : destination.engDestNameExt);
						HashMap<String, String> hmSelectedDestDetail = new HashMap<String, String>();
						hmSelectedDestDetail.put("dest", destination.engDestName);
						hmSelectedDestDetail.put("destext", destination.engDestNameExt);
						hmSelectedDestDetail.put("customernum", customerNumber);
						hmSelectedDestDetail.put("servicenum", serviceNumber);
						hmSelectedDestDetail.put("srvtype", "MOB");
//						APIsManager.doGetIDRateResult(me, hmSelectedDestDetail);
						APIsManager.requestIDRateResult(me, hmSelectedDestDetail);
						searchListDialog.dismiss();
					}
				}, isZh);
		searchListDialog.show();
	}

	private void setDestination(Destination destination) {
		this.destination = destination;
	}

	public void setDest(String name, String ext) {
		Boolean isExtShown = ext != null && !ext.equals("");
		if (debug) Log.i(TAG, isExtShown + ext);
		if (isExtShown) {
			aq.id(R.id.fragment_iddrates_country_layout).height(textViewHeightidd, false);
		} else {
			aq.id(R.id.fragment_iddrates_country_layout).height(textViewHeight, false);
		}
		aq.id(R.id.fragment_iddrates_country_layout_name).height(!isExtShown ? textViewHeight : textViewHeightidd * 3 / 8, false);
		aq.id(R.id.fragment_iddrates_country_layout_desc).visibility(!isExtShown ? View.GONE : View.VISIBLE);
		aq.id(R.id.fragment_iddrates_country_layout_name).text(name);
		aq.id(R.id.fragment_iddrates_country_layout_desc).text(ext);

		ScrollView sv = (ScrollView) requireView().findViewById(R.id.fragment_iddrates_result_scrollview);
		sv.smoothScrollTo(0, 0);
		sv.postDelayed(() -> sv.smoothScrollTo(0, 0), 800);
	}

	/**********************************
	 *
	 * Result part
	 *
	 ***********************************/
	public void clearResult() {
		isResultShown = false;
		aq.id(R.id.fragment_iddrates_result_header_layout).visibility(isResultShown ? View.VISIBLE : View.INVISIBLE);
		aq.id(R.id.fragment_iddrates_result_scrollview).visibility(isResultShown ? View.VISIBLE : View.INVISIBLE);
		aq.id(R.id.fragment_iddrates_remark).visibility(isResultShown ? View.VISIBLE : View.INVISIBLE);
		((LinearLayout) aq.id(R.id.fragment_iddrates_result_frame).getView()).removeAllViews();
	}

	public void setResult() {
		isResultShown = true;
		aq.id(R.id.fragment_iddrates_result_header_layout).visibility(isResultShown ? View.VISIBLE : View.INVISIBLE);
		aq.id(R.id.fragment_iddrates_result_scrollview).visibility(isResultShown ? View.VISIBLE : View.INVISIBLE);
		aq.id(R.id.fragment_iddrates_remark).visibility(isResultShown ? View.VISIBLE : View.INVISIBLE);
		((LinearLayout) aq.id(R.id.fragment_iddrates_result_frame).getView()).removeAllViews();

		if (rateList != null && rateList.size() > 0) {
			for (int rx = 0; rx < rateList.size(); rx++) {
				LayoutInflater layoutInflater = (LayoutInflater) this.getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				RelativeLayout view = (RelativeLayout) layoutInflater.inflate(R.layout.item_srplan, null, false);
				view.setBackgroundColor(this.getResources().getColor(R.color.idd_grey));

				String name = isZh ? rateList.get(rx).chiTimeZone : rateList.get(rx).timeZone;
				String desc = isZh ? rateList.get(rx).chiTimeZoneRemarks : rateList.get(rx).timeZoneRemarks;
				String rates = "HK $" + rateList.get(rx).rate;
				AAQuery a = new AAQuery(view);
				a.normTextGrey(R.id.item_srplan_title, name);
				a.normTextBlue(R.id.item_srplan_rate, rates);
				a.normTextGrey(R.id.item_srplan_time, desc);

				((LinearLayout) aq.id(R.id.fragment_iddrates_result_frame).getView()).addView(view);
				LinearLayout.LayoutParams params = (LayoutParams) view.getLayoutParams();
				view.setLayoutParams(params);
				view.setPadding(basePadding, basePadding / 2, extralinespace, extralinespace / 2);
			}
		}

		if (!isZh) {
			if (srPlan != null && srPlan.engDestNameExt != null && srPlan.engDestNameExt.trim().length() != 0) {
				aq.id(R.id.fragment_iddrates_remark_txt).visibility(View.VISIBLE);
				aq.normTextGrey(R.id.fragment_iddrates_remark_txt, "*Remarks: " + srPlan.engDestNameExt, -2);
			} else {
				aq.id(R.id.fragment_iddrates_remark_txt).visibility(View.GONE);
			}
		} else {
			if (srPlan != null && srPlan.chiDestNameExt != null && srPlan.chiDestNameExt.trim().length() != 0) {
				aq.id(R.id.fragment_iddrates_remark_txt).visibility(View.VISIBLE);
				aq.normTextGrey(R.id.fragment_iddrates_remark_txt, "*備註: " + srPlan.chiDestNameExt, -2);
			} else {
				aq.id(R.id.fragment_iddrates_remark_txt).visibility(View.GONE);
			}
		}
	}

	@Override
	public void onFail(APIsResponse response) {
		if (debug) Log.i(TAG, "Fail");
		// General Error Message
		if (!"".equals(response.getMessage()) && response.getMessage() != null) {
			DialogHelper.createSimpleDialog(getActivity(), response.getMessage());
		} else if (RC.TCSESS_MM.equalsIgnoreCase(response.getReply().getCode())) {
			BaseActivity.ivSessDialog();
		} else {
			DialogHelper.createSimpleDialog(getActivity(), ClnEnv.getRPCErrMsg(getActivity(), response.getReply().getCode()));
		}
	}

	public void onResume() {
		super.onResume();
		if (this.destination != null) {
			Boolean isExtShown = isZh ? this.destination.chiDestNameExt != null &&
					!this.destination.chiDestNameExt.equals("") :
					this.destination.engDestNameExt != null && !this.destination.engDestNameExt.equals("");

			if (isZh ? this.destination.chiDestName != null && !this.destination.chiDestName.equals("") :
					this.destination.engDestName != null && !this.destination.engDestName.equals("")) {
				aq.id(R.id.fragment_iddrates_country_layout_name).text(isZh ?
						this.destination.chiDestName : this.destination.engDestName);
				aq.id(R.id.fragment_iddrates_country_layout_desc).text(isZh ?
						this.destination.chiDestNameExt : this.destination.engDestNameExt);
				setResult();
			}
		}

		if (destList == null || destList.size() <= 0) {
//			APIsManager.doGetIDDRateDest(me);
			APIsManager.requestDestinationIDDRate(me);
		}
	}

	@Override
	public void onSuccess(APIsResponse response) {
		if (debug) Log.i(TAG, "onSuccess");
		if (response != null) {
			if (IDDRATEDEST.equals(response.getActionTy())) {
				destList = (ArrayList<Destination>) response.getCra();
			} else if (APIsManager.IDDRATERESULT.equals(response.getActionTy())) {
				srPlan = (SRPlan) response.getCra();
				rateList = srPlan.rateList;
				setResult();
			}
		}
	}
}
