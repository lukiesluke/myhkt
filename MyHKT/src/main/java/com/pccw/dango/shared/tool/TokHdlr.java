/*
    Class for Token Extra Utilities

    Standard for Token Operations.
    
    Keywords
    --------
    $URL: svn://10.87.120.207/dango/rel/v16.0/src/com/pccw/dango/shared/tool/TokHdlr.java $
    $Rev: 844 $
    $Date: 2016-10-17 11:54:01 +0800 (¶g¤@, 17 ¤Q¤ë 2016) $
    $Author: upd $
*/

package com.pccw.dango.shared.tool;

import java.io.Serializable;

import com.pccw.wheat.shared.tool.MiniProp;
import com.pccw.wheat.shared.tool.Tool;


public class TokHdlr implements Serializable
{
    private static final long serialVersionUID = -4920017781342401603L;


    public static void main(String rArg[])
    {
        System.out.println(getVer());
        return;
    }
    
    
    public static String getVer()
    {
        return ("$URL: svn://10.87.120.207/dango/rel/v16.0/src/com/pccw/dango/shared/tool/TokHdlr.java $, $Rev: 844 $");
    }
    
    
    public static String replace(String rSrc, String rParmAry[], MiniProp rMp)
    {
        return (Tool.replaceTok(rSrc, 
                                "<?", ">",
                                rParmAry,
                                rMp,
                                "?"));
    }
    
    
    public static String[] tokize2Ary(String rSrc)
    {
        return (Tool.split2Ary(rSrc, ","));
    }
    
    
    public static String[] tokizeAndTrim2Ary(String rSrc)
    {
        return (Tool.splitAndTrim2Ary(rSrc, ","));
    }
}    
