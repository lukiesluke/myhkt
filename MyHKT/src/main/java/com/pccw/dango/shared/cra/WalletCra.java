package com.pccw.dango.shared.cra;

import java.io.Serializable;

public class WalletCra extends BaseCraEx implements Serializable {

    private String iLoginId;
    private String oJWT;

    public String getiLoginId() {
        return iLoginId;
    }

    public void setiLoginId(String iLoginId) {
        this.iLoginId = iLoginId;
    }

    public String getoJWT() {
        return oJWT;
    }

    public void setoJWT(String oJWT) {
        this.oJWT = oJWT;
    }
}
