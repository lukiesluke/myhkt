package com.pccw.myhkt.cell.model;

import com.pccw.myhkt.R;

import android.text.style.ClickableSpan;
import android.view.View;

public class BigTextCell extends Cell{

	
	private Boolean isArrowShown = false;

	String[] links = null;
	ClickableSpan[] clickableSpans = null;

	public BigTextCell(String title, String content) {
		super();
		type = Cell.BIGTEXT1;
		this.title = title;
		this.content = content;
		titleColorId = R.color.black;
		contentColorId = R.color.hkt_txtcolor_grey;
		titleSizeDelta = 4;
		contentSizeDelta = 4;	
		isArrowShown = false;
	}

	public Boolean getIsArrowShown() {
		return isArrowShown;
	}

	public void setIsArrowShown(Boolean isArrowShown) {
		this.isArrowShown = isArrowShown;
	}

	public void setClickableSpans(String[] links, ClickableSpan[] clickableSpans) {
		this.links = links;
		this.clickableSpans = clickableSpans;
	}

	public String[] getLinks() {
		return links;
	}

	public ClickableSpan[] getClickableSpans() {
		return clickableSpans;
	}

}