/*
    Bill Inquiry Extension
    
    Keywords
    --------
    $URL: svn://10.87.120.207/dango/rel/v16.0/src/com/pccw/dango/shared/entity/BillEx.java $
    $Rev: 844 $
    $Date: 2016-10-17 11:54:01 +0800 (¶g¤@, 17 ¤Q¤ë 2016) $
    $Author: upd $
*/

package com.pccw.dango.shared.entity;

import java.io.Serializable;

public class BillEx extends Bill implements Serializable
{
    private static final long serialVersionUID = 2008711370080017929L;
    
    private String                  dtlUrlEn;       /* Detail Utl (En)                  */
    private String                  dtlUrlZh;       /* Detail Utl (Zh)                  */
    private String                  summUrlEn;      /* Summary Utl (En)                 */
    private String                  summUrlZh;      /* Summary Utl (Zh)                 */
    private String                  billRun;        /* bill run number (for CSL)        */

    
    public static void main(String rArg[])
    {
        System.out.println(getVer());
        return;
    }
    
    
    public static String getVer()
    {
        return ("$URL: svn://10.87.120.207/dango/rel/v16.0/src/com/pccw/dango/shared/entity/BillEx.java $, $Rev: 844 $");
    }
    
    
    public BillEx()
    {
        initAndClear();
    }
    
    
    protected void init()
    {
        super.init();
    }
    
    
    public void clear()
    {
        super.clear();
        
        clearDtlUrlEn();
        clearDtlUrlZh();
        clearSummUrlEn();
        clearSummUrlZh();
        clearBillRun();
    }
    
    
    public BillEx copyFrom(BillEx rSrc)
    {
        super.copyFrom(rSrc);
        
        setDtlUrlEn(rSrc.getDtlUrlEn());
        setDtlUrlZh(rSrc.getDtlUrlZh());
        setSummUrlEn(rSrc.getSummUrlEn());
        setSummUrlZh(rSrc.getSummUrlZh());
        setBillRun(rSrc.getBillRun());
        
        return (this);
    }
    
    
    public BillEx copyTo(BillEx rDes)
    {
        rDes.copyFrom(this);
        return (rDes);
    }
    
    
    public BillEx copyMe()
    {
        BillEx              rDes;
        
        rDes = new BillEx();
        rDes.copyFrom(this);
        
        return (rDes);
    }

    
    public void clearDtlUrlEn()
    {
        setDtlUrlEn("");
    }

    
    public void setDtlUrlEn(String rArg) 
    {
        dtlUrlEn = rArg.trim();
    }
    
    
    public String getDtlUrlEn() 
    {
        return dtlUrlEn;
    }

    
    public void clearDtlUrlZh()
    {
        setDtlUrlZh("");
    }

    
    public void setDtlUrlZh(String rArg) 
    {
        dtlUrlZh = rArg.trim();
    }
    
    
    public String getDtlUrlZh() 
    {
        return dtlUrlZh;
    }


    public void clearSummUrlEn()
    {
        setSummUrlEn("");
    }

    
    public void setSummUrlEn(String rArg) 
    {
        summUrlEn = rArg.trim();
    }
    
    
    public String getSummUrlEn() 
    {
        return summUrlEn;
    }

    
    public void clearSummUrlZh()
    {
        setSummUrlZh("");
    }

    
    public void setSummUrlZh(String rArg) 
    {
        summUrlZh = rArg.trim();
    }
    
    
    public String getSummUrlZh() 
    {
        return summUrlZh;
    }

    
    public void clearBillRun()
    {
        setBillRun("");
    }

    
    public void setBillRun(String rArg) 
    {
        billRun = rArg.trim();
    }
    
    
    public String getBillRun() 
    {
        return billRun;
    }

    
    public Bill toBill()
    {
        Bill                    rBill;
        
        rBill = new Bill();
        rBill.copyFrom(this);
        
        return (rBill);
    }
}
