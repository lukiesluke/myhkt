package com.pccw.myhkt.fragment;

import static com.pccw.myhkt.util.Constant.FORMAT_YYYYMMDD;

import android.app.Activity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentTransaction;

import com.pccw.dango.shared.cra.PlanMobCra;
import com.pccw.dango.shared.entity.SubnRec;
import com.pccw.dango.shared.g3entity.G3BoostUpOffer1DTO;
import com.pccw.dango.shared.g3entity.G3DisplayServiceItemDTO;
import com.pccw.dango.shared.tool.RC;
import com.pccw.myhkt.APIsResponse;
import com.pccw.myhkt.ClnEnv;
import com.pccw.myhkt.DialogHelper;
import com.pccw.myhkt.InterpretRCManager;
import com.pccw.myhkt.R;
import com.pccw.myhkt.Utils;
import com.pccw.myhkt.activity.BaseActivity;
import com.pccw.myhkt.fragment.UsageDataChild1Fragment.OnUsageListener;
import com.pccw.myhkt.lib.ui.AAQuery;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/************************************************************************
 * File : UsageFragment.java
 * Desc : Usage Page handle mob and 1010 case
 * Name : UsageFragment
 * by 	: Andy Wong
 * Date : 25/1/2015
 * Change History:
 * Date       Modified By		Description
 * ---------- ----------------	-------------------------------
 * 25/1/2015  Andy Wong 		-First draft
 *************************************************************************/

public class UsageFragment extends BaseServiceFragment implements OnUsageListener {
    private UsageFragment me;
    private AAQuery aq;
    private FragmentTransaction ft;
    private UsageDataFragment usagedataFragment;
    private UsageTopup1Fragment usageTopup1Fragment;
    private UsageTopup2Fragment usageTopup2Fragment;
    private UsageTopup3Fragment usageTopup3Fragment;

    private static final String TAG = "lwg";
    private PlanMobCra planMobCra;
    private G3DisplayServiceItemDTO selectedServiceItem;
    private G3BoostUpOffer1DTO selectedG3BoostUpOfferDTO;
    private Boolean isMob = true;
    private int lob = 0;
    //	private int currentUsagePage = PAGE_USAGEDATA;
    //	public final static int PAGE_USAGEDATA = 0;
    //	public final static int PAGE_TOPUP1 = 1;
    //	public final static int PAGE_TOPUP2 = 2;
    //	public final static int PAGE_TOPUP3 = 3;
    private int basePadding;

    protected int activeChildview = R.string.CONST_SELECTEDFRAG_USAGEDATA;    // Initial default subview
    private Boolean isLocal = true;

    @Override
    public void onAttach(@NonNull Activity activity) {
        super.onAttach(activity);

        try {
            if (debug) Log.i(TAG, "attach:UsageFragment");
            callback_main = (OnServiceListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement OnServiceListener");
        }
    }

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        me = this;
        View fragmentLayout = inflater.inflate(R.layout.fragment_usage, container, false);
        aq = new AAQuery(fragmentLayout);
        try {
			assert this.getArguments() != null;
			isLocal = this.getArguments().getBoolean("islocal");
        } catch (Exception e) {
			e.printStackTrace();
		}
        activeChildview = isLocal ? R.string.CONST_SELECTEDFRAG_USAGEDATA : R.string.CONST_SELECTEDFRAG_ROAMING;
        if (callback_main != null && callback_main.getAcctAgent() != null && callback_main.getAcctAgent().getSubnRec() != null && !TextUtils.isEmpty(callback_main.getAcctAgent().getSubnRec().lob)) {
            isMob = (SubnRec.LOB_MOB.equals(callback_main.getAcctAgent().getSubnRec().lob) ||
                    SubnRec.LOB_IOI.equals(callback_main.getAcctAgent().getSubnRec().lob) ||
                    SubnRec.WLOB_XMOB.equals(callback_main.getAcctAgent().getSubnRec().lob) ||
                    SubnRec.LOB_CSP.equals(callback_main.getAcctAgent().getSubnRec().lob) ||
                    SubnRec.WLOB_XCSP.equals(callback_main.getAcctAgent().getSubnRec().lob));
            lob = Utils.getLobType(callback_main.getAcctAgent().getSubnRec().lob);
        }
        initData();
        return fragmentLayout;
    }

    protected void initData() {
        basePadding = (int) getResources().getDimension(R.dimen.basePadding);
    }

    protected void initUI() {
        //		usagedataFragment = new UsageDataFragment();
        //		ft = getChildFragmentManager().beginTransaction();
        //		currentUsagePage = 0;
        //		ft.replace(R.id.fragment_usage_frameLayout, usagedataFragment).commit();
        //Detail part
        String detail = "";
        if (isMob) {
            // hide monthly fee --
//			detail = getResString(R.string.csl_usage_subscribed_plan) + "$--" +
//					"\n"+getResString(R.string.usage_next_bill_date) + " --/--/----";
            detail = getResString(R.string.usage_next_bill_date) + " --/--/----";
        } else {
            detail = getResString(R.string.csl_usage_contract_end_date) + "--/--/----" +
                    "\n" + getResString(R.string.usage_next_bill_date) + " --/--/----";
        }
        aq.gravity(R.id.fragment_usage_detail, Gravity.CENTER | Gravity.START);
        aq.padding(R.id.fragment_usage_bot_layout, basePadding, basePadding, basePadding, basePadding);
        aq.normText(R.id.fragment_usage_detail, detail, -2);
        aq.id(R.id.fragment_usage_bot_layout).backgroundColorId(R.color.cell_bg_grey);
        aq.id(R.id.fragment_usage_detail).height(LinearLayout.LayoutParams.WRAP_CONTENT).backgroundColorId(R.color.cell_bg_grey);

        displayChildview(true);
    }

    public void setDetailPart() { //TODO csim next bill cutoff date and contract end date handling
        //Fail case in topup will set plsnMobCra to be null
        if (planMobCra != null) {
            Date nextDate = Calendar.getInstance().getTime();
            String nextBillDate = "--/--/----";
            //Use in CSim
            Date contractDate = Calendar.getInstance().getTime();
            String contractEdDate = "--/--/----";
            try {
                if (isMob) {
                    SimpleDateFormat nextDf = new SimpleDateFormat(FORMAT_YYYYMMDD, Locale.ENGLISH);
                    SimpleDateFormat df = new SimpleDateFormat(Utils.getString(me.getActivity(), R.string.usage_date_format), Locale.ENGLISH);

                    if (planMobCra.getOMobUsage().getG3AcctBomNextBillDateDTO().getBomNextBillDate() != null && !planMobCra.getOMobUsage().getG3AcctBomNextBillDateDTO().getBomNextBillDate().trim().isEmpty()) {
                        nextDate = nextDf.parse(planMobCra.getOMobUsage().getG3AcctBomNextBillDateDTO().getBomNextBillDate());
                        nextBillDate = df.format(nextDate);
                    }
                } else {
                    SimpleDateFormat nextDf = new SimpleDateFormat(FORMAT_YYYYMMDD, Locale.ENGLISH);
                    SimpleDateFormat df = new SimpleDateFormat(Utils.getString(me.getActivity(), R.string.usage_date_format), Locale.ENGLISH);

                    if (planMobCra.getOContrEndDt() != null && !planMobCra.getOContrEndDt().trim().isEmpty()) {
                        contractDate = nextDf.parse(planMobCra.getOContrEndDt());
                        contractEdDate = df.format(contractDate);
                    }

                    if (planMobCra.getOMobUsage().getG3UsageQuotaDTO().getNxtBilCutoffDt() != null && !planMobCra.getOMobUsage().getG3UsageQuotaDTO().getNxtBilCutoffDt().trim().isEmpty()) {
                        nextDate = nextDf.parse(planMobCra.getOMobUsage().getG3UsageQuotaDTO().getNxtBilCutoffDt());
                        nextBillDate = df.format(nextDate);
                    }
                }
            } catch (Exception e) {
                if (debug)
                    Log.i(TAG, planMobCra.getOMobUsage().getG3UsageQuotaDTO().getNxtBilCutoffDt() + "Set 21A" + e);
                e.printStackTrace();
            }
            //Detail part
            String detail = "";
            if (isMob) {
                detail = getResString(R.string.usage_next_bill_date) + " " + nextBillDate;
            } else {
                detail = getResString(R.string.csl_usage_contract_end_date) + " " + contractEdDate +
                        "\n" + getResString(R.string.csl_next_bill_date) + " " + nextBillDate;
            }
            aq.normText(R.id.fragment_usage_detail, detail, -2);
            aq.gravity(R.id.fragment_usage_detail, Gravity.CENTER | Gravity.START);
            aq.id(R.id.fragment_usage_detail).height(LinearLayout.LayoutParams.WRAP_CONTENT).backgroundColorId(R.color.cell_bg_grey);
        }
    }

    public void refreshData() {
        super.refreshData();
    }

    public void refresh() {
        super.refresh();
        //Usage
        if (callback_main != null && (callback_main.getActiveSubview() == R.string.CONST_SELECTEDVIEW_MOBUSAGE || callback_main.getActiveSubview() == R.string.CONST_SELECTEDVIEW_1010USAGE)) {
            setModuleIdnTracker();
            if (getActiveChildview() != R.string.CONST_SELECTEDFRAG_USAGEDATA) {
                setActiveChildview(R.string.CONST_SELECTEDFRAG_USAGEDATA);
                displayChildview(2);
            }
            if (getActiveChildview() == R.string.CONST_SELECTEDFRAG_USAGEDATA) {
                usagedataFragment.refresh();
            }
        }
        //Roaming
        if (callback_main != null && (callback_main.getActiveSubview() == R.string.CONST_SELECTEDVIEW_MOBROAMING || callback_main.getActiveSubview() == R.string.CONST_SELECTEDVIEW_1010ROAMING)) {
            setModuleIdnTracker();
            if (getActiveChildview() != R.string.CONST_SELECTEDFRAG_ROAMING) {
                setActiveChildview(R.string.CONST_SELECTEDFRAG_ROAMING);
                displayChildview(2);
            }
            if (getActiveChildview() == R.string.CONST_SELECTEDFRAG_ROAMING) {
                usagedataFragment.refresh();
            }
        }
    }

    protected void cleanupUI() {
        //		usagedataFragment = null;
        //		usageTopup1Fragment = null;
        //		usageTopup2Fragment = null;
        //		usageTopup3Fragment = null;
    }

    @Override
    public void onFail(APIsResponse response) {
        // TODO Auto-generated method stub
        // General Error Message
        if (!"".equals(response.getMessage()) && response.getMessage() != null) {
            DialogHelper.createSimpleDialog(getActivity(), response.getMessage());
        } else if (RC.TCSESS_MM.equalsIgnoreCase(response.getReply().getCode())) {
            BaseActivity.ivSessDialog();
        } else {
            DialogHelper.createSimpleDialog(getActivity(), InterpretRCManager.interpretRC_AcMainMdu(getActivity(), response.getReply()) + "UsageFragment");
        }
    }

    public final void displayChildview(Boolean isBack) {
        displayChildview(isBack ? 1 : 0);
    }

    public final void displayChildview(int type) {
        ft = getChildFragmentManager().beginTransaction();
        if (type == 0) {
            ft.setCustomAnimations(R.anim.right_slide_in, R.anim.left_slide_out);
        } else if (type == 1) {
            //isBack
            ft.setCustomAnimations(R.anim.left_slide_in, R.anim.right_slide_out);
        }
        switch (activeChildview) {
            case R.string.CONST_SELECTEDFRAG_ROAMING:
            case R.string.CONST_SELECTEDFRAG_USAGEDATA:
                if (usagedataFragment == null) {
                    usagedataFragment = new UsageDataFragment();
                }
                ft.replace(R.id.fragment_usage_frameLayout, usagedataFragment);
                //					ft.addToBackStack(null);
                break;
            case R.string.CONST_SELECTEDFRAG_TOPUP1:
                if (usageTopup1Fragment == null) {
                    usageTopup1Fragment = new UsageTopup1Fragment();
                }
                ft.replace(R.id.fragment_usage_frameLayout, usageTopup1Fragment);
                					ft.addToBackStack(null);
                break;
            case R.string.CONST_SELECTEDFRAG_TOPUP2:
                if (usageTopup2Fragment == null) {
                    usageTopup2Fragment = new UsageTopup2Fragment();
                }
                ft.replace(R.id.fragment_usage_frameLayout, usageTopup2Fragment);
                //					ft.addToBackStack(null);
                break;
            case R.string.CONST_SELECTEDFRAG_TOPUP3:
                if (usageTopup3Fragment == null) {
                    usageTopup3Fragment = new UsageTopup3Fragment();
                }
                ft.replace(R.id.fragment_usage_frameLayout, usageTopup3Fragment);
                //					ft.addToBackStack(null);
                break;
        }
        Utils.closeSoftKeyboard(getActivity());
        ft.commit();
    }

    @Override
    public void setPlanMobCra(PlanMobCra planMobCra) {
        this.planMobCra = planMobCra;
        setDetailPart();
    }

    @Override
    public PlanMobCra getPlanMobCra() {
        return planMobCra;
    }

    @Override
    public void setSelectedServiceItem(G3DisplayServiceItemDTO g3DisplayServiceItemDTO) {
        selectedServiceItem = g3DisplayServiceItemDTO;

    }

    @Override
    public G3DisplayServiceItemDTO getSelectedServiceItem() {
        return selectedServiceItem;
    }

    @Override
    public G3BoostUpOffer1DTO getSelectedG3BoostUpOfferDTO() {
        return selectedG3BoostUpOfferDTO;
    }

    @Override
    public void getSelectedG3BoostUpOfferDTO(G3BoostUpOffer1DTO g3BoostUpOffer1DTO) {
        selectedG3BoostUpOfferDTO = g3BoostUpOffer1DTO;
    }

    @Override
    public void setActiveChildview(int index) {
        activeChildview = index;
    }

    @Override
    public int getActiveChildview() {
        return activeChildview;
    }

    public Boolean getIsLocal() {
        return isLocal;
    }

    public void setIsLocal(Boolean isLocal) {
        this.isLocal = isLocal;
    }

    public int getLob() {
        return lob;
    }

    private void setModuleIdnTracker() {
        if (lob == R.string.CONST_LOB_1010) {
            callback_livechat.setModuleId(getResString(ClnEnv.isMyMobFlag() ? R.string.MODULE_101_MM_PLAN : R.string.MODULE_101_PLAN));
        } else if (lob == R.string.CONST_LOB_O2F) {
            callback_livechat.setModuleId(getResString(ClnEnv.isMyMobFlag() ? R.string.MODULE_O2F_MM_PLAN : R.string.MODULE_O2F_PLAN));
        } else if (lob == R.string.CONST_LOB_IOI) {
            callback_livechat.setModuleId(getResString(ClnEnv.isMyMobFlag() ? R.string.MODULE_IOI_MM_PLAN : R.string.MODULE_IOI_PLAN));
        } else if (lob == R.string.CONST_LOB_CSP) {
            callback_livechat.setModuleId(getResString(ClnEnv.isMyMobFlag() ? R.string.MODULE_CLUBSIM_MM_PLAN : R.string.MODULE_CLUBSIM_PLAN));
        } else {
            callback_livechat.setModuleId(getResString(ClnEnv.isMyMobFlag() ? R.string.MODULE_MOB_MM_PLAN : R.string.MODULE_MOB_PLAN));
        }
    }
}
