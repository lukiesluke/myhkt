package com.pccw.myhkt.fragment;

/*
com.pccw.donut.shared.gadget.Tool.java

Class for String Variable Manipulation and Other Utilities in GWT.

History
-------
05-Jan-11       AL              Introduction

Keywords
--------
$URL: svn://xhkalx08/donut/trk/src/com/pccw/donut/shared/gadget/Tool.java $
$Rev: 483 $
$Date: 2014-11-17 14:24:54 +0800 (Mon, 17 Nov 2014) $
$Author: upd $
*/

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.text.DecimalFormatSymbols;

public class Tool {
public static final String	LOW_TS			= "20000101000000";
public static final String	HIG_TS			= "20991231235959";

public static final String	DATEFMT_1		= "dd/MM/yy";
public static final String	DATEFMT_2		= "dd-MMM-yyyy";
public static final String	DATEFMT_3		= "dd/MM/yyyy";
public static final String	DATEFMT_4		= "dd/MM";
public static final String	DATEFMT_5		= "MMM-yyyy";
public static final String  DATEFMT_6       = "dd-MMM";

public static final String  DTFMT_1         = "dd/MM/yyyy HH:mm:ss";
public static final String  DTFMT_2         = "dd/MM/yyyy HH:mm";
public static final String  DTFMT_3         = "HH:mm";

public static final String	NUMFMT_1		= "0.0";
public static final String	NUMFMT_2		= "0";
public static final String	NUMFMT_3		= "0.00";

public static final String	USGUNT			= "KB";
public static final String  USGUNT_MB       = "MB";
public static final String  USGUNT_GB       = "GB";

public static final String   	UNT_MIN         = "min";
public static final String   	UNT_MSG         = "msg";

public static final String	FLG_YES			= "Y";
public static final String	FLG_NO			= "N";

public static final String	FLG_ACTIVE		= "A";
public static final String	FLG_INACTIVE	= "I";

public static final String	NA				= "N/A";

public static void main(String rArg[]) {
    String                      rVer = "$URL: svn://xhkalx08/donut/trk/src/com/pccw/donut/shared/gadget/Tool.java $, $Rev: 483 $";

	System.out.println(rVer);
	return;
}

public Tool() {
	/*Constuctor. */
}

public static String lTrim(String rVar, char rChr) {
	/*Trim Left rChr. */

	StringBuffer rSB;
	int ri, rsi, rdi;
	int rl, rsl, rdl;
	char rVal;

	rSB = new StringBuffer();
	rl = rVar.length();

	for (ri = 0; ri < rl; ri++) {
		if (rVar.charAt(ri) != rChr) break;
	}

	if (ri >= 0) {
		for (; ri < rl; ri++) {
			rSB.append(rVar.charAt(ri));
		}
	}

	return (rSB.toString());
}

public static String lTrim(String rVar) {
	/*Trim Left Spaces. */

	return (lTrim(rVar, ' '));
}

public static String rTrim(String rVar, char rChr) {
	/*Trim Right rChr. */

	StringBuffer rSB;
	int ri, rsi, rdi;
	int rl, rsl, rdl;
	char rVal;

	rSB = new StringBuffer();
	rl = rVar.length();

	for (; rl > 0; rl--) {
		if (rVar.charAt(rl - 1) != rChr) break;
	}

	for (ri = 0; ri < rl; ri++) {
		rSB.append(rVar.charAt(ri));
	}

	return (rSB.toString());
}

public static String rTrim(String rVar) {
	/*Trim Right Spaces. */

	return (rTrim(rVar, ' '));
}

public static String trimAll(String rVar, char rChr) {
	/*Trim left & right rChr. */

	return (rTrim(lTrim(rVar, rChr), rChr));
}

public static String trimAll(String rVar) {
	/*Trim left & right Space. */

	return (trimAll(rVar, ' '));
}

public static String mid(String rVar, int rBeg, int rLen) {
	/*Return a substr of rVar.
	 * From rBeg (0 is the 1st Character).
	 * Pickup rLen characters.
	 * 
	 * No Exception will be generated, unlike String class. */

	StringBuffer rSB;
	int ri, rsi, rdi;
	int rl, rsl, rdl;
	char rVal;

	rSB = new StringBuffer();
	rl = rVar.length();
	rdl = rLen;

	if (rl > 0 && rBeg >= 0 && rdl > 0) {
		for (ri = rBeg; ri < rl && rdl > 0; ri++, rdl--) {
			rSB.append(rVar.charAt(ri));
		}
	}

	return (new String(rSB.toString()));
}


public static String maskTail(String rVar, char rMask, int rShowLen)
{
    StringBuffer                rSB;
    int                         rx, ri, rl;
    
    rl = rVar.length();
    if (rl < rShowLen) return (rVar);
    
    rSB = new StringBuffer();
    for (rx=0; rx<rl; rx++) {
        if (rx < rShowLen) rSB.append(rVar.charAt(rx));
        else rSB.append(rMask);
    }
    
    return (rSB.toString());
}


public static String maskHead(String rVar, char rMask, int rShowLen)
{
    StringBuffer                rSB;
    int                         rx, ri, rl;
    
    rl = rVar.length();
    if (rl < rShowLen) return (rVar);
    
    ri = rl - rShowLen;
    rSB = new StringBuffer();
    for (rx=0; rx<rl; rx++) {
        if (rx < ri) rSB.append(rMask);
        else rSB.append(rVar.charAt(rx));
    }
    
    return (rSB.toString());
}


public static boolean isNum(String rVar, boolean rSign, boolean rDot) {
	/*Return true if rVar is filled by digits.
	 * if rSign is true, allow +/- at the 1st character. */

	int ri, rsi, rdi;
	int rl, rsl, rdl;
	char rChr;
	int rDotPos = -1;

	rl = rVar.length();
    rdl = 0;

	for (ri = 0; ri < rl; ri++) {
		rChr = rVar.charAt(ri);

        if (rChr >= '0' && rChr <= '9') {
            rdl++;
        }
        else {
			if (rChr == '.') {
				if (rDot && rDotPos < 0) {
					rDotPos = ri;
					continue;
				}
			} else if (rChr == '+' || rChr == '-') {
				if (rSign && ri == 0) {
					continue;
				}
			}

			return (false);
		}
	}
    
    if (rdl == 0) {
        /* No Valid '0' - '9' encountered */
        return (false);
    }
    
	return (true);
}

public static boolean isDig(String rVar) {
	/*Return true if rVar is filled by digits. */

	return (isNum(rVar, false, false));
}

public static boolean isDig2(String rVar) {
	/*Return true if rVar is filled by digits & Not Nil. */

	if (rVar.length() > 0) { return (isNum(rVar, false, false)); }

	return (false);
}

public static boolean isHex(String rVar) {
	/*Return true if the rVar is filled by Hex-digits. */

	int ri, rsi, rdi;
	int rl, rsl, rdl;
	char rChr;

	rl = rVar.length();

	for (ri = 0; ri < rl; ri++) {
		rChr = rVar.charAt(ri);
		if (rChr >= '0' && rChr <= '9') continue;
		if (rChr >= 'a' && rChr <= 'f') continue;
		if (rChr >= 'A' && rChr <= 'F') continue;

		return (false);
	}

	return (true);
}

public static boolean isASC(String rVar) {
	/*Return true if rVar is filled by ASCII characters. */

	int ri, rsi, rdi;
	int rl, rsl, rdl;
	char rChr;

	rl = rVar.length();

	for (ri = 0; ri < rl; ri++) {
		rChr = rVar.charAt(ri);
		if (rChr < 0x20 || rChr > 0x7e) return (false);
	}

	return (true);
}

public static boolean isNil(String rVar) {
	/*Return true if rVar == null or rVar == "" */

	if (rVar == null) return (true);
	if (rVar.length() == 0) return (true);

	return (false);
}

public static String nu(String rVar, String rReplace) {
	/*If the rVar is empty, return rReplace
	 * Otherwise, return the rVar. */

	if (rVar.length() == 0) return (rReplace);
	return (rVar);
}

public static String nu(String rVar) {
	return (nu(rVar, " "));
}

public static String nu2(String rVar, String rReplace) {
	/*If the rVar is nil, return rReplace
	 * Otherwise, return the rVar. */

	if (rVar == null) return (rReplace);
	return (rVar);
}

public static String nu2(String rVar) {
	return (nu2(rVar, ""));
}

public static String nil(String rVar, String rReplace) {
	/*If the rVar is null, return rReplace
	 * Otherwise, return the rVar. */

	if (isNil(rVar)) return (rReplace);
	return (rVar);
}

public static String nil(String rVar) {
	return (nil(rVar, " "));
}

public static String[] tokenList(String rStr, String rSep) {
	/*Break the rStr by rSep and return a List
	 * i.e. Array of all String Element (= Tokens) */

	String rRes[];
	int rx, ri, rl;

	rRes = rStr.split(rSep);
	return (rRes);
}

public static String cvTab(String rVar) {
	/*Convert Tab into Space.
	 * NOTE: It is "Convert", not "Expand". */

	StringBuffer rSB;
	int ri, rsi, rdi;
	int rl, rsl, rdl;
	char rVal;

	rSB = new StringBuffer();
	rl = rVar.length();

	for (ri = 0; ri < rl; ri++) {
		rVal = rVar.charAt(ri);
		if (rVal == 0x09) rVal = ' ';
		rSB.append(rVal);
	}

	return (new String(rSB.toString()));
}

public static int toInt(String rVar) {
	/*Convert to Integer.
	 * And convert the error into RuntimeExcetion. */

	try {
		return (Integer.parseInt(rVar));
	} catch (RuntimeException rEx) {
		throw rEx;
	} catch (Exception rEX) {
		throw new RuntimeException(rEX);
	}
}

public static int toInt2(String rVar) {
	if (rVar.length() == 0) return (0);
	return (toInt(rVar));
}

public static String int2Str(String rFmt, int rVal) {
	String rStr;
	DecimalFormat df = new DecimalFormat(rFmt);
	rStr = df.format(rVal);
	return (rStr);
}

/*public static String long2Str(String rFmt, long rVal)
 * {
 * String rStr;
 * 
 * rStr = NumberFormat.getFormat(rFmt).format(rVal);
 * return (rStr);
 * } */

public static double toDouble(String rVar) {
	/*Convert to double.
	 * And convert the error into RuntimeExcetion. */

	try {
		return (Double.parseDouble(rVar));
	} catch (RuntimeException rEx) {
		throw rEx;
	} catch (Exception rEX) {
		throw new RuntimeException(rEX);
	}
}

public static double toDouble2(String rVar) {
	if (rVar.length() == 0) return (0);
	return (toDouble(rVar));
}

/* public static String double2Str(String rFmt, double rVal)
 * {
 * String rStr;
 * 
 * rStr = NumberFormat.getFormat(rFmt).format(rVal);
 * return (rStr);
 * } */

public static long toLong(String rVar) {
	/*Convert to long.
	 * And convert the error into RuntimeExcetion. */

	try {
		return (Long.parseLong(rVar));
	} catch (RuntimeException rEx) {
		throw rEx;
	} catch (Exception rEX) {
		throw new RuntimeException(rEX);
	}
}

public static long toLong2(String rVar) {
	if (rVar.length() == 0) return (0);
	return (toLong(rVar));
}

public static String format(String rFmt, String... rVal) {
	StringBuffer rSB;
	char rC;
	boolean rParm;
	int rx, ri, rl;

	rSB = new StringBuffer();
	rl = rFmt.length();

	rParm = false;
	rC = 0;
	ri = 0;

	for (rx = 0; rx < rl; rx++) {
		rC = rFmt.charAt(rx);
		if (!rParm) {
			if (rC == '%') {
				rParm = true;
				continue;
			} else {
				rSB.append(rC);
			}
		} else {
			if (rC == '%') {
				rSB.append(rC);
			} else {
				if (rC == 's') {
					rSB.append(rVal[ri++]);
				}
			}
			rParm = false;
		}
	}

	return (rSB.toString());
}

public static String formatPos(String rFmt, String... rVal) {
	StringBuffer rSB;
	StringBuffer rParmPos;
	int rPos;
	char rC;
	boolean rParm;
	int rx, ri, rl;

	rSB = new StringBuffer();
	rl = rFmt.length();

	rParmPos = new StringBuffer();

	rParm = false;
	rC = 0;
	ri = 0;

	for (rx = 0; rx < rl; rx++) {
		rC = rFmt.charAt(rx);
		if (!rParm) {
			if (rC == '%') {
				rParm = true;
				continue;
			} else {
				rSB.append(rC);
			}
		} else {
			if (rC == '%') {
				rSB.append(rC);
				rParm = false;
			} else {
				if (rC == 's') {
					if (!Tool.isDig(rParmPos.toString())) { throw new RuntimeException("Invalid Positional Parameter [" + rParmPos.toString() + "]!"); }

					rPos = Tool.toInt(rParmPos.toString());
					if (rPos < 1 || rPos > rVal.length) { throw new RuntimeException("Positional Parameter [" + rParmPos.toString() + "] Out Of Range!"); }

					rSB.append(rVal[rPos - 1]);
					rParmPos.setLength(0);
					rParm = false;
				} else {
					rParmPos.append(rC);
				}
			}
		}
	}

	if (rParm) { throw new RuntimeException("Incomplete Positional Parameter!"); }

	return (rSB.toString());
}

public static String daInit() {
	/*Return the String (14 '0') of the Date Init. */

	return (new String("00000000000000"));
}

public static String daInit(String rVar) {
	/*Return a 14-digit Date String if the rVar is
	 * a Valid Date. Otherwise, return empty String. */

	String rRes;

	if (rVar.length() > 14) return (new String());

	rRes = mid(rVar + daInit(), 0, 14);
	if (isVaDate(rRes)) return rRes;

	return (new String());
}

public static boolean isVaDate(String rVar) {
	/*Validate a String, return true if it is a valid date. */

	String rYY;
	String rMM;
	String rDD;
	String rHR;
	String rMI;
	String rSE;

	int rNYY;
	int rNMM;
	int rNDD;
	int rNHR;
	int rNMI;
	int rNSE;

	try {
		if (rVar.length() == 14 && isDig(rVar)) {
			rYY = rVar.substring(0, 4);
			rMM = rVar.substring(4, 6);
			rDD = rVar.substring(6, 8);
			rHR = rVar.substring(8, 10);
			rMI = rVar.substring(10, 12);
			rSE = rVar.substring(12, 14);

			rNYY = Integer.parseInt(rYY);
			rNMM = Integer.parseInt(rMM);
			rNDD = Integer.parseInt(rDD);
			rNHR = Integer.parseInt(rHR);
			rNMI = Integer.parseInt(rMI);
			rNSE = Integer.parseInt(rSE);

			if (rNYY < 1970) return (false);
			if (rNMM < 1 || rNMM > 12) return (false);
			if (rNDD < 1 || rNDD > dayInMonth(rNYY, rNMM)) return (false);
			if (rNHR < 0 || rNHR > 23) return (false);
			if (rNMI < 0 || rNMI > 59) return (false);
			if (rNSE < 0 || rNSE > 59) return (false);

			return (true);
		}
	} catch (Exception rEX) {
	}

	return (false);
}

public static String validateLoDA(String rVar) {
	/*Validate a Date (8-digit) or TS (14-digit)
	 * if it is a Date and valid, converted as full TS with 000000
	 * 
	 * Other return "" */

	if (rVar.length() == 8) {
		if (daInit(rVar).length() != 0) { return (rVar + "000000"); }
	} else {
		if (rVar.length() == 14) {
			if (isVaDate(rVar)) return (rVar);
		}
	}

	return (new String());
}

public static String validateHiDA(String rVar) {
	/*Validate a Date (8-digit) or TS (14-digit)
	 * if it is a Date and valid, converted as full TS with 235959
	 * 
	 * Other return "" */

	if (rVar.length() == 8) {
		if (daInit(rVar).length() != 0) { return (rVar + "235959"); }
	} else {
		if (rVar.length() == 14) {
			if (isVaDate(rVar)) return (rVar);
		}
	}

	return (new String());
}

public static Date str2Date(String rVar) {
	/*Convert the rVar to Date.
	 * Using the SimpleDateFormat class. */

	String rYY;
	String rMM;
	String rDD;
	String rHR;
	String rMI;
	String rSE;

	int rNYY;
	int rNMM;
	int rNDD;
	int rNHR;
	int rNMI;
	int rNSE;

	Date rDate;

	try {
		if (rVar.length() == 14 && isDig(rVar)) {
			if (isVaDate(rVar)) {
				rYY = rVar.substring(0, 4);
				rMM = rVar.substring(4, 6);
				rDD = rVar.substring(6, 8);
				rHR = rVar.substring(8, 10);
				rMI = rVar.substring(10, 12);
				rSE = rVar.substring(12, 14);

				rNYY = Integer.parseInt(rYY);
				rNMM = Integer.parseInt(rMM);
				rNDD = Integer.parseInt(rDD);
				rNHR = Integer.parseInt(rHR);
				rNMI = Integer.parseInt(rMI);
				rNSE = Integer.parseInt(rSE);

				rDate = new Date(rNYY - 1900, rNMM - 1, rNDD, rNHR, rNMI, rNSE);

				return (rDate);
			}
		}
	} catch (Exception rEX) {
	}

	return (null);
}

public static String date2Str(Date rVar) {
	/*Convert the Date rVar into String. */

	String rStr;

	try {
		rStr = format("%s%s%s%s%s%s", int2Str("0000", rVar.getYear() + 1900), int2Str("00", rVar.getMonth() + 1), int2Str("00", rVar.getDate()), int2Str("00", rVar.getHours()), int2Str("00", rVar.getMinutes()), int2Str("00", rVar.getSeconds()));
	} catch (Exception rEX) {
		return (new String());
	}

	return (rStr);
}

public static String firstDay(String rVar) {
	/*Return the 1st Day of the Month specified in rVar. */

	String rRes;

	if (!isVaDate(rVar)) return (new String());

	rRes = mid(rVar, 0, 6) + "01000000";
	return (rRes);
}

/* public static String monthEnd(String rVar)
 * {
 * /*
 * Return the last Day of the Month specified in rVar. */

/* String rDay, rRes;
 * int rDIM;
 * 
 * if (!isVaDate(rVar)) return (new String());
 * 
 * rDIM = dayInMonth(rVar);
 * rDay = int2Str("00", rDIM);
 * rRes = mid(rVar, 0, 6) + rDay + "235959";
 * 
 * return (rRes);
 * } */

public static String trimTime(String rVar) {
	/*Reset the time to zeros. */

	String rRes;

	if (!isVaDate(rVar)) return (new String());

	rRes = mid(rVar, 0, 8) + "000000";
	return (rRes);
}

public static String lastSec(String rVar) {
	/*Set the date to last second of the day. */

	String rRes;

	if (!isVaDate(rVar)) return (new String());

	rRes = mid(rVar, 0, 8) + "235959";
	return (rRes);
}

/* public static String addDays(String rVar, int rDays)
 * {
 * /*
 * Add Number of Days.
 * Subtract if rDays < 0. */

/* long rTime;
 * Date rRes;
 * 
 * if (!isVaDate(rVar)) return (new String());
 * 
 * rTime = str2Date(rVar).getTime();
 * rTime += rDays * (24L * 60L * 60L * 1000L);
 * 
 * rRes = new Date(rTime);
 * return (date2Str(rRes));
 * } */

/* public static String addSecs(String rVar, int rSecs)
 * {
 * /*
 * Add Number of rSecs.
 * Subtract if rSecs < 0. */

/* long rTime;
 * Date rRes;
 * 
 * if (!isVaDate(rVar)) return (new String());
 * 
 * rTime = str2Date(rVar).getTime();
 * rTime += rSecs * (1000L);
 * 
 * rRes = new Date(rTime);
 * return (date2Str(rRes));
 * } */

public static long elapDays(String rVarA, String rVarB) {
	/*Return Number of Days (in Long)
	 * of two dates : rVarA - rVarB */

	long rMsDf;

	rMsDf = elapSecs(rVarA, rVarB);
	rMsDf /= 60;
	rMsDf /= 60;
	rMsDf /= 24;

	return (rMsDf);
}

public static long elapSecs(String rVarA, String rVarB) {
	/*Return Number of Seconds (in Long)
	 * of two dates : rVarA - rVarB */

	Date rA;
	Date rB;

	long rMsDf;

	if (!isVaDate(rVarA)) { throw new RuntimeException("Invalid Date A"); }

	if (!isVaDate(rVarB)) { throw new RuntimeException("Invalid Date B"); }

	rA = str2Date(rVarA);
	rB = str2Date(rVarB);

	rMsDf = rA.getTime() - rB.getTime();
	rMsDf /= 1000;

	return (rMsDf);
}

private static final int	DAYINMONTH[]	= { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };

public static int dayInMonth(int rYY, int rMM) {
	int rDIM;

	if (rMM < 1 || rMM > 12) { throw new RuntimeException("Invalid Month!"); }

	rDIM = DAYINMONTH[rMM - 1];
	if (rMM == 2) {
		if (((rYY % 4) == 0) && (((rYY % 100) != 0) || ((rYY % 400) == 0))) {
			rDIM++;
		}
	}

	return (rDIM);
}

public static int dayInMonth(String rVar) {
	String rYY;
	String rMM;

	int rNYY;
	int rNMM;

	if (!isVaDate(rVar)) return (-1);

	rYY = rVar.substring(0, 4);
	rMM = rVar.substring(4, 6);

	rNYY = Integer.parseInt(rYY);
	rNMM = Integer.parseInt(rMM);

	return (dayInMonth(rNYY, rNMM));
}

public static final String	MONTHNAME[]	= { "JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG", "SEP", "OCT", "NOV", "DEC" };

public static String getMonthName(String rVar) {
	String rMM;
	int rNMM;

	if (!isVaDate(rVar)) return (new String());

	rMM = rVar.substring(4, 6);
	rNMM = Integer.parseInt(rMM);

	return (MONTHNAME[rNMM - 1]);
}

public static String[] ary(String... rPar) {
	/*Construct a String Array. */

	String[] rRes;
	int ri, rs;

	rs = rPar.length;
	rRes = new String[rs];

	for (ri = 0; ri < rs; ri++) {
		rRes[ri] = rPar[ri];
	}

	return (rRes);
}

public static String formatOrdNum(String rStr) {
	int ri;

	try {
		ri = toInt(rStr);

		if (ri != 0) {
			rStr = rStr + getOrdinalNo(ri);
		}
	} catch (Exception rEX) {
	}
	return rStr;
}

public static String getOrdinalNo(int value) {
	int hunRem = value % 100;
	int tenRem = value % 10;

	if (hunRem - tenRem == 10) { return "th"; }

	switch (tenRem) {
		case 1:
			return "st";
		case 2:
			return "nd";
		case 3:
			return "rd";
		default:
			return "th";
	}
}

public static String formatNum(String rStr) {
	return formatNum(rStr, NUMFMT_1);
}

// Rewritten by Ryan to substitute GWT API by generic Java API
public static String formatNum(String rStr, String rFmt) {
	double rVal;

	try {
		if (!rStr.equals("")) {
			DecimalFormat fm1 = new DecimalFormat(rFmt, new DecimalFormatSymbols(Locale.US));
			rVal = toDouble(rStr);
			rStr = fm1.format(rVal);
		}
	} catch (Exception rEX) {
	}
	return rStr;
}

public static String formatDate(String rStr) {
	return formatDate(rStr, DATEFMT_1);
}

// Rewritten by Ryan to substitute GWT API by generic Java API
public static String formatDate(String rStr, String rFmt) {

	Date rVal;

	try {
		if (rStr.equals("00000000") || rStr.trim().length() == 0) {
			rStr = "-";			
		} else if (!rStr.equals("")) {
			SimpleDateFormat fm1 = new SimpleDateFormat(rFmt, Locale.US);

			rVal = str2Date(rStr + "000000");
			rStr = fm1.format(rVal);
		}
	} catch (Exception e) {
		e.printStackTrace();
	}
	return rStr;
}



public static String formatDT(String rStr)
{
    return formatDT(rStr, DTFMT_1);
}


public static String formatDT(String rStr, String rFmt)
{
    
    Date                        rVal;
    
    try {
        if (rStr.equals("00000000000000"))
        {
            rStr = new String();
        }
        
        if (!rStr.equals(""))
        {
        	SimpleDateFormat fm1 = new SimpleDateFormat(rFmt, Locale.US);
            rVal = str2Date(rStr);
            rStr = fm1.format(rVal);
        }
    }
    catch (Exception rEX) {
    }
    return rStr;
}

public static String formatDate2(String rStr)
{
    return formatDate2(rStr, DATEFMT_1);
}

public static String formatDate2(String rStr, String rFmt)
{
    //to covert datetime string to formatted date only
    Date                        rVal;
    
    try {
        if (rStr.equals("00000000000000"))
        {
            rStr = new String();
        }
        
        if (!rStr.equals("") && rStr.length() == 14)
        {
            rStr = rStr.substring(0, rStr.length()-6);
			SimpleDateFormat fm1 = new SimpleDateFormat(rFmt, Locale.US);
			rVal = str2Date(rStr + "000000");
			rStr = fm1.format(rVal);
        }
    }
    catch (Exception rEX) {
    }
    return rStr;
}


public static String convFromFullDate(String rDate) {
	/*Convert DD/MM/YYYY to YYYYMMDD */

	if (rDate.length() == 10) {
		if (rDate.charAt(2) == '/' && rDate.charAt(5) == '/') {
			rDate = rDate.substring(6) + rDate.substring(3, 5) + rDate.substring(0, 2);
			return (rDate);
		}
	}

	return (new String());
}

public static String formatTime(String rStr) {

	Date rVal;

	try {

		if (!rStr.equals("") && rStr.length() == 4) {
			rStr = mid(rStr, 0, 2) + ":" + mid(rStr, 2, 2);
		}
	} catch (Exception rEX) {
	}
	return rStr;
}

public static int findEle(String rStrAry[], String rStr) {
	/*Return Index if rStr is in the rStrAry[] */

	int rx, ri, rl;

	for (rx = 0; rx < rStrAry.length; rx++) {
		if (rStr.equals(rStrAry[rx])) { return (rx); }
	}

	return (-1);
}

public static boolean isThere(String rStrAry[], String rStr) {
	/*Return true if the rStr is in rStrAry[] */

	return (findEle(rStrAry, rStr) >= 0);
}

public static boolean isThere2(String rStr, String... rParAry) {
	/*Return true if the rStr is in rParAry[] */

	return (findEle(rParAry, rStr) >= 0);
}

public static boolean isFlag(String rStr) {
	/*Return true if the rStr is Y or N */

	return (isThere2(rStr, Tool.FLG_YES, Tool.FLG_NO));
}

public static String cat(String... rPar) {
	/*Concat the Parameters into a Single String. */

	StringBuffer rSB;
	int ri, rl;

	rl = rPar.length;
	rSB = new StringBuffer();

	for (ri = 0; ri < rl; ri++) {
		rSB.append(rPar[ri]);
	}

	return (rSB.toString());
}

public static int max(int rA, int rB) {
	/*Return Max Integer. */

	if (rA > rB) return (rA);
	return (rB);
}

public static int min(int rA, int rB) {
	/*Return Min Integer. */

	if (rA < rB) return (rA);
	return (rB);
}

public static String max(String rA, String rB) {
	/*Return Max String. */

	if (rA.compareTo(rB) > 0) return (rA);
	return (rB);
}

public static String min(String rA, String rB) {
	/*Return Min String. */

	if (rA.compareTo(rB) < 0) return (rA);
	return (rB);
}

public static int divUp(int rA, int rB) {
	/*Return = rA / rB and add 1 if rA % rB > 0 */

	int rRes;

	rRes = rA / rB;
	if ((rA % rB) > 0) rRes++;

	return (rRes);
}

public static boolean isEqual(String rA, String rB) {
	/*Return True if rA.equals(rB) */

	return (rA.equals(rB));
}

public static boolean isEqualIC(String rA, String rB) {
	/*Return True if rA.equalsIgnoreCase(rB) */

	return (rA.equalsIgnoreCase(rB));
}

public static boolean isBetween(String rX, String rA, String rB) {
	/*Return True if rX >= rA and rX <= rB */

	return (rX.compareTo(rA) >= 0 && rX.compareTo(rB) <= 0);
}

public static boolean isBetweenIC(String rX, String rA, String rB) {
	/*Return True if rX >= rA and rX <= rB under IgnoreCase */

	return (rX.compareToIgnoreCase(rA) >= 0 && rX.compareToIgnoreCase(rB) <= 0);
}

public static String htmlEncode(String rSrc) {
	/*Return URL Encoded String */

	StringBuffer rSB;
	char rC;
	int rx, rl;

	rSB = new StringBuffer();
	rl = rSrc.length();

	for (rx = 0; rx < rl; rx++) {
		rC = rSrc.charAt(rx);
		if (rC == '"') rSB.append("&quot;");
		else if (rC == '&') rSB.append("&amp;");
		else if (rC == '<') rSB.append("&lt;");
		else if (rC == '>') rSB.append("&gt;");
		else rSB.append(rC);
	}

	return (rSB.toString());
}

public static String[] breakStr(String rSrc, String rSep) {
	List rList;
	String rSeg;
	int rx, ri, rl;

	rList = new ArrayList();

	ri = 0;
	while (true) {
		rx = rSrc.indexOf(rSep, ri);
		if (rx < 0) break;

		rSeg = rSrc.substring(ri, rx);
		rList.add(rSeg);

		ri = rx + rSep.length();
	}

	rSeg = rSrc.substring(ri);
	rList.add(rSeg);

	return ((String[]) rList.toArray(new String[0]));
}

public static String[] breakNL(String rSrc) {
	return (breakStr(rSrc, "\n"));
}

public static String cvNL(String rSrc) {
	StringBuffer rSB;
	char rC;
	boolean rEsc;
	int rx, ri, rl;

	rSB = new StringBuffer();
	rl = rSrc.length();
	rEsc = false;

	for (rx = 0; rx < rl; rx++) {
		rC = rSrc.charAt(rx);

		if (rEsc) {
			if (rC == 'n') {
				rC = '\n';
				rSB.append(rC);
			}
			rEsc = false;
		} else {
			if (rC == '\\') {
				rEsc = true;
			} else {
				rSB.append(rC);
			}
		}
	}

	return (rSB.toString());
}

public static String clssName(Object rObj) {
	/*Return Class Name of the rObj */

	return (rObj.getClass().getName());
}

//no update here until further notice
/*public static String getUserAgent()
 * {
 * return (Window.Navigator.getUserAgent().toLowerCase());
 * } */

/* public static boolean isIPad()
 * {
 * String rUA;
 * 
 * rUA = getUserAgent().toLowerCase();
 * 
 * if (rUA.indexOf("ipad") >= 0 && rUA.indexOf("safari") >= 0) {
 * return (true);
 * }
 * 
 * return (false);
 * }
 * 
 * 
 * public static boolean isIPhone()
 * {
 * String rUA;
 * 
 * rUA = getUserAgent().toLowerCase();
 * 
 * if (rUA.indexOf("iphone") >= 0 && rUA.indexOf("safari") >= 0) {
 * return (true);
 * }
 * 
 * return (false);
 * }
 * 
 * 
 * public static boolean isAndroid()
 * {
 * String rUA;
 * 
 * rUA = getUserAgent().toLowerCase();
 * 
 * if (rUA.indexOf("android") >= 0 && rUA.indexOf("linux") >= 0) {
 * return (true);
 * }
 * 
 * return (false);
 * }
 * 
 * 
 * public static boolean isTargetUA()
 * {
 * String rUA;
 * String rTargetUA;
 * 
 * rTargetUA = ClnEnv.getClnCfg().getVal(ClnCfg.TARGET_UA);
 * 
 * if (rTargetUA.length() > 0) {
 * rUA = getUserAgent().toLowerCase();
 * 
 * if (rUA.indexOf(rTargetUA) >= 0) {
 * return (true);
 * }
 * }
 * 
 * return (false);
 * } */

public static boolean isEmailFmt(String rStr) {
	String rLocal;
	String rDomain;

	int rx, ri, rl;

	rx = rStr.indexOf('@');
	if (rx < 0) return (false);

	rLocal = rStr.substring(0, rx);
	rDomain = rStr.substring(rx + 1);

	if (isVaEmailLocal(rLocal)) {
		if (isVaEmailDomain(rDomain)) { return (true); }
	}

	return (false);
}

public static boolean isVaEmailLocal(String rStr) {
	char rC;
	char rNC;

	int rx, ri, rl;

	/*refer to: http://en.wikipedia.org/wiki/Email_address#Valid_email_addresses
	 * See "Local Part" Section
	 * 
	 * However, we don't support the follows:
	 * - Special characters are allowed with restrictions "(),:;<>@[\]
	 * - Comments */

	rl = rStr.length();
	if (rl == 0) return (false);

	for (rx = 0; rx < rl; rx++) {
		rC = rStr.charAt(rx);

		if (isAlpNum(rC)) {
			continue;
		} else if (rC == '.') {
			if (rx == 0) return (false);
			if (rx == (rl - 1)) return (false);

			rNC = rStr.charAt(rx - 1);
			if (rC == rNC) return (false);
		} else if ("!#$%&'*+-/=?^_`{|}~".indexOf(rC) >= 0) {
			continue;
		} else {
			return (false);
		}
	}

	return (true);
}

public static boolean isVaEmailDomain(String rStr) {
	char rC;
	char rNC;

	int rx, ri, rl;

	/*Refer to: http://en.wikipedia.org/wiki/Domain_name
	 * See "Technical requirements and process" Section
	 * 
	 * Not Supported: Comments */

	rl = rStr.length();
	if (rl == 0) return (false);

	for (rx = 0; rx < rl; rx++) {
		rC = rStr.charAt(rx);

		if (rC == '.') {
			if (rx == 0) return (false);
			if (rx == (rl - 1)) return (false);

			rNC = rStr.charAt(rx - 1);
			if (!isAlpNum(rNC)) return (false);

			rNC = rStr.charAt(rx + 1);
			if (!isAlpNum(rNC)) return (false);

			continue;
		} else if (rC == '-') {
			if (rx == 0) return (false);
			if (rx == (rl - 1)) return (false);

			rNC = rStr.charAt(rx - 1);
			if (!isAlpNum(rNC)) return (false);

			rNC = rStr.charAt(rx + 1);
			if (!isAlpNum(rNC)) return (false);

			continue;
		} else if (isAlpNum(rC)) {
			continue;
		} else {
			return (false);
		}
	}

	return (true);
}

public static boolean isAlpNum(char rChr) {
	if (rChr >= 'a' && rChr <= 'z') return (true);
	if (rChr >= 'A' && rChr <= 'Z') return (true);
	if (rChr >= '0' && rChr <= '9') return (true);

	return (false);
}

public static boolean isVaPswdCombination(String rStr) {
	char rC;
	int rAlp;
	int rNum;

	int rx, ri, rl;

	rl = rStr.length();

	rAlp = 0;
	rNum = 0;

	for (rx = 0; rx < rl; rx++) {
		rC = rStr.charAt(rx);

		if (rC >= 'A' && rC <= 'Z') rAlp++;
		else if (rC >= 'a' && rC <= 'z') rAlp++;
		else if (rC >= '0' && rC <= '9') rNum++;
		else return (false);
	}

	if (rAlp > 0 && rNum > 0) return (true);

	return (false);
}




public static boolean isYesOrNo(String rStr) {
	return (rStr.equals(FLG_YES) || rStr.equals(FLG_NO));
}

public static boolean isActiveOrInactive(String rStr) {
	return (rStr.equals(FLG_ACTIVE) || rStr.equals(FLG_INACTIVE));
}

public static String nbsp() {
	return ("\u00a0");
}

public static String dash() {
	return ("\u2014");
}

public static native void wlog(String rMsg)
/*-{
    try {
        if (window.console != undefined) {
            console.log("wlog:" + rMsg);
        }
    }
    catch (rErr) {
    }
}-*/;

public static String formatAcctNum(String rSrc) {
	String rRes;

	rRes = rSrc.substring(0, 2) + "-" + rSrc.substring(2, 8) + "-" + rSrc.substring(8, 12) + "-" + rSrc.substring(12);

	return (rRes);
}

/* public static void clearTabIndex(HasWidgets rHw)
 * {
 * Iterator rHwItr;
 * Widget rWg;
 * FocusWidget rFWg;
 * 
 * rHwItr = rHw.iterator();
 * while (rHwItr.hasNext()) {
 * rWg = (Widget) rHwItr.next();
 * 
 * if (rWg instanceof HasWidgets) {
 * clearTabIndex((HasWidgets) rWg);
 * }
 * else {
 * if (rWg instanceof FocusWidget) {
 * rFWg = (FocusWidget) rWg;
 * rFWg.setTabIndex(-1);
 * }
 * }
 * }
 * }
 * 
 * public static HandlerRegistration addKeyDownCh
 * (FocusWidget rFwg, final Tool.Hook rHook, final Tool.Hook rHook4Alt)
 * {
 * /*
 * Use keydown instead of keypressed - keypressed only for printable char */

/* return (rFwg.addKeyDownHandler(
 * new KeyDownHandler() {
 * public void onKeyDown(KeyDownEvent rEvt)
 * {
 * if (rEvt.getNativeKeyCode() == KeyCodes.KEY_ENTER &&
 * !rEvt.getNativeEvent().getAltKey()) {
 * rHook.nextStep();
 * }
 * 
 * if (rEvt.getNativeKeyCode() == KeyCodes.KEY_ENTER &&
 * rEvt.getNativeEvent().getAltKey()) {
 * if (rHook4Alt != null) {
 * rHook4Alt.nextStep();
 * }
 * }
 * }
 * }
 * )
 * );
 * }
 * 
 * 
 * public static HandlerRegistration addKeyDownCh(FocusWidget rFwg, final Tool.Hook rHook)
 * {
 * return (addKeyDownCh(rFwg, rHook, null));
 * } */
/*Inner Class/Interface Definitions */

public interface Hook {
	public void nextStep();
}

}
