/*
    Mobile Usage
    
    Keywords
    --------
    $URL: $
    $Rev: $
    $Date: $
    $Author: $
*/

package com.pccw.dango.shared.entity;

import java.io.Serializable;

import com.pccw.dango.shared.g3entity.G3AcctBomNextBillDateDTO;
import com.pccw.dango.shared.g3entity.G3DisplayServiceItemResultDTO;
import com.pccw.dango.shared.g3entity.G3GetUnbilledDailyRoamWifiResultDTO;
import com.pccw.dango.shared.g3entity.G3UsageQuotaDTO;


public class MobUsage implements Serializable
{
    private static final long serialVersionUID = 5710875232934482141L;
    
    private boolean                 entBased;           /* Entitlement Based Plan?                       */
    private SrvPlan                 ratePlan;           /* Rate Plan                                     */
    private SrvPlan                 localData;          /* Local Data (GPRS)                             */
    private SrvPlan                 lVceIter;           /* local voice inter                             */
    private SrvPlan                 lVceItra;           /* local voice intra                             */
    private SrvPlan                 lVdoIter;           /* local video inter                             */
    private SrvPlan                 lVdoItra;           /* local video intra                             */
    private SrvPlan                 lSMSIter;           /* local sms inter                               */
    private SrvPlan                 lSMSItra;           /* local sms intra                               */
    private SrvPlan                 iSMS;               /* inter sms                                     */
    private SrvPlan                 nowTv;              /* now TV                                        */
    private SrvPlan                 nowSports;          /* now Sports                                    */
    private SrvPlan                 moov;               /* MOOV                                          */
    private SrvPlan[]               optVasAry;          /* Optional VAS                                  */
    private String                  usgLastUpdTs;       /* Usage Last Update Date                        */
    
    private G3DisplayServiceItemResultDTO
                                    g3DisplayServiceItemResultDTO;
                                                        /* Data Usage                                    */
    private G3GetUnbilledDailyRoamWifiResultDTO 
                                    g3GetUnbilledDailyRoamWifiResultDTO;
                                                        /* Roaming Wifi                                  */
    private G3UsageQuotaDTO         g3UsageQuotaDTO;    /* Other Usage                                   */
    private G3UsageQuotaDTO         g3SummUsageQuotaDTO;/* Other Usage (Summary)                         */
    private G3AcctBomNextBillDateDTO 
                                    g3AcctBomNextBillDateDTO;
                                                        /* BOM Next Bill Date                            */


    public static void main(String rArg[])
    {
        System.out.println(getVer());
        return;
    }
    
    
    public static String getVer()
    {
        return ("$URL: $, $Rev: $");
    }

    
    public MobUsage()
    {
        initAndClear();
    }

    
    final void initAndClear()
    {
        init();
        clear();
    }


    protected void init()
    {
    }


    public void clear()
    {
        clearEntBased();
        clearRatePlan();
        clearLocalData();
        clearLVceIter();
        clearLVceItra();
        clearLVdoIter();
        clearLVdoItra();
        clearLSMSIter();
        clearLSMSItra();
        clearISMS();
        clearNowTv();
        clearNowSports();
        clearMoov();
        clearOptVasAry();
        clearUsgLastUpdTs();
        clearG3DisplayServiceItemResultDTO();
        clearG3GetUnbilledDailyRoamWifiResultDTO();
        clearG3UsageQuotaDTO();
        clearG3SummUsageQuotaDTO();
        clearG3AcctBomNextBillDateDTO();
    }


    public MobUsage copyFrom(MobUsage rSrc)
    {
        setEntBased(rSrc.isEntBased());
        setRatePlan(rSrc.getRatePlan());
        setLocalData(rSrc.getLocalData());
        setLVceIter(rSrc.getLVceIter());
        setLVceItra(rSrc.getLVceItra());
        setLVdoIter(rSrc.getLVdoIter());
        setLVdoItra(rSrc.getLVdoItra());
        setLSMSIter(rSrc.getLSMSIter());
        setLSMSItra(rSrc.getLSMSItra());
        setISMS(rSrc.getISMS());
        setNowTv(rSrc.getNowTv());
        setNowSports(rSrc.getNowSports());
        setMoov(rSrc.getMoov());
        setOptVasAry(rSrc.getOptVasAry());
        setUsgLastUpdTs(rSrc.getUsgLastUpdTs());
        setG3DisplayServiceItemResultDTO(rSrc.getG3DisplayServiceItemResultDTO());
        setG3GetUnbilledDailyRoamWifiResultDTO(rSrc.getG3GetUnbilledDailyRoamWifiResultDTO());
        setG3UsageQuotaDTO(rSrc.getG3UsageQuotaDTO());
        setG3SummUsageQuotaDTO(rSrc.getG3SummUsageQuotaDTO());
        setG3AcctBomNextBillDateDTO(rSrc.getG3AcctBomNextBillDateDTO());

        return (this);
    }


    public MobUsage copyTo(MobUsage rDes)
    {
        rDes.copyFrom(this);
        return (rDes);
    }
    
    
    public MobUsage copyMe()
    {
        MobUsage                    rDes;

        rDes = new MobUsage();
        rDes.copyFrom(this);
        return (rDes);
    }


    public void clearEntBased()
    {
        entBased = false;
    }


    public void setEntBased(boolean rArg)
    {
        entBased = rArg;
    }


    public boolean isEntBased()
    {
        return (entBased);
    }


    public void clearRatePlan()
    {
        ratePlan = new SrvPlan();
    }


    public void setRatePlan(SrvPlan rArg)
    {
        ratePlan = rArg;
    }


    public SrvPlan getRatePlan()
    {
        return (ratePlan);
    }


    public void clearLocalData()
    {
        localData = new SrvPlan();
    }


    public void setLocalData(SrvPlan rArg)
    {
        localData = rArg;
    }


    public SrvPlan getLocalData()
    {
        return (localData);
    }

    
    public void clearLVceIter()
    {
        setLVceIter(new SrvPlan());
    }


    public void setLVceIter(SrvPlan rLVceIter) 
    {
        lVceIter = rLVceIter;
    }
    
    
    public SrvPlan getLVceIter() 
    {
        return lVceIter;
    }


    public void clearLVceItra()
    {
        setLVceItra(new SrvPlan());
    }


    public void setLVceItra(SrvPlan rLVceItra) 
    {
        lVceItra = rLVceItra;
    }

    
    public SrvPlan getLVceItra() 
    {
        return lVceItra;
    }


    public void clearLVdoIter()
    {
        setLVdoIter(new SrvPlan());
    }


    public void setLVdoIter(SrvPlan rLVdoIter) 
    {
        lVdoIter = rLVdoIter;
    }

    
    public SrvPlan getLVdoIter() 
    {
        return lVdoIter;
    }


    public void clearLVdoItra()
    {
        setLVdoItra(new SrvPlan());
    }


    public void setLVdoItra(SrvPlan rLVdoItra) 
    {
        lVdoItra = rLVdoItra;
    }

    
    public SrvPlan getLVdoItra() 
    {
        return lVdoItra;
    }

    
    public void clearLSMSIter()
    {
        setLSMSIter(new SrvPlan());
    }
    
    
    public SrvPlan getLSMSIter() 
    {
        return lSMSIter;
    }


    public void setLSMSIter(SrvPlan rLSMSIter) 
    {
        lSMSIter = rLSMSIter;
    }

    
    public void clearLSMSItra()
    {
        setLSMSItra(new SrvPlan());
    }


    public void setLSMSItra(SrvPlan rLSMSItra) 
    {
        lSMSItra = rLSMSItra;
    }

    
    public SrvPlan getLSMSItra() 
    {
        return lSMSItra;
    }

    
    public void clearISMS()
    {
        setISMS(new SrvPlan());
    }


    public void setISMS(SrvPlan rISMS) 
    {
        iSMS = rISMS;
    }

    
    public SrvPlan getISMS() 
    {
        return iSMS;
    }

    
    public void setNowTv(SrvPlan rNowTv) 
    {
        nowTv = rNowTv;
    }

    
    public SrvPlan getNowTv() 
    {
        return nowTv;
    }

    
    public void clearNowTv()
    {
        setNowTv(new SrvPlan());
    }

    
    public void setNowSports(SrvPlan rNowSports) 
    {
        nowSports = rNowSports;
    }

    
    public SrvPlan getNowSports() 
    {
        return nowSports;
    }

    
    public void clearNowSports()
    {
        setNowSports(new SrvPlan());
    }


    public void setMoov(SrvPlan rMoov) 
    {
        moov = rMoov;
    }

    
    public SrvPlan getMoov() 
    {
        return moov;
    }

    
    public void clearMoov()
    {
        setMoov(new SrvPlan());
    }


    public void clearOptVasAry()
    {
        optVasAry = new SrvPlan[0];
    }


    public void setOptVasAry(SrvPlan[] rArg)
    {
        optVasAry = rArg;
    }


    public SrvPlan[] getOptVasAry()
    {
        return (optVasAry);
    }


    public void clearUsgLastUpdTs()
    {
        usgLastUpdTs = "";
    }


    public void setUsgLastUpdTs(String rArg)
    {
        usgLastUpdTs = rArg;
    }


    public String getUsgLastUpdTs()
    {
        return (usgLastUpdTs);
    }


    public void clearG3DisplayServiceItemResultDTO()
    {
        g3DisplayServiceItemResultDTO = new G3DisplayServiceItemResultDTO();
    }


    public void setG3DisplayServiceItemResultDTO(G3DisplayServiceItemResultDTO rArg)
    {
        g3DisplayServiceItemResultDTO = rArg;
    }


    public G3DisplayServiceItemResultDTO getG3DisplayServiceItemResultDTO()
    {
        return (g3DisplayServiceItemResultDTO);
    }

    
    public void clearG3GetUnbilledDailyRoamWifiResultDTO()
    {
        g3GetUnbilledDailyRoamWifiResultDTO = new G3GetUnbilledDailyRoamWifiResultDTO();
    }


    public void setG3GetUnbilledDailyRoamWifiResultDTO(G3GetUnbilledDailyRoamWifiResultDTO rArg)
    {
        g3GetUnbilledDailyRoamWifiResultDTO = rArg;
    }


    public G3GetUnbilledDailyRoamWifiResultDTO getG3GetUnbilledDailyRoamWifiResultDTO()
    {
        return (g3GetUnbilledDailyRoamWifiResultDTO);
    }
    

    public void clearG3UsageQuotaDTO()
    {
        g3UsageQuotaDTO = new G3UsageQuotaDTO();
    }


    public void setG3UsageQuotaDTO(G3UsageQuotaDTO rArg)
    {
        g3UsageQuotaDTO = rArg;
    }


    public G3UsageQuotaDTO getG3UsageQuotaDTO()
    {
        return (g3UsageQuotaDTO);
    }


    public void clearG3SummUsageQuotaDTO()
    {
        g3SummUsageQuotaDTO = new G3UsageQuotaDTO();
    }


    public void setG3SummUsageQuotaDTO(G3UsageQuotaDTO rArg)
    {
        g3SummUsageQuotaDTO = rArg;
    }


    public G3UsageQuotaDTO getG3SummUsageQuotaDTO()
    {
        return (g3SummUsageQuotaDTO);
    }

    
    public void clearG3AcctBomNextBillDateDTO()
    {
        g3AcctBomNextBillDateDTO = new G3AcctBomNextBillDateDTO();
    }


    public void setG3AcctBomNextBillDateDTO(G3AcctBomNextBillDateDTO rArg)
    {
        g3AcctBomNextBillDateDTO = rArg;
    }


    public G3AcctBomNextBillDateDTO getG3AcctBomNextBillDateDTO()
    {
        return (g3AcctBomNextBillDateDTO);
    }
}
