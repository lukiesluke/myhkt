package com.pccw.dango.shared.g3entity;

import java.io.Serializable;


public class G3AccountInformationDTO  implements Serializable 
{
    private static final long serialVersionUID = 3482079097357971871L;
    
    private String accountNumber;
    private String paymentMethod;
    private String accountCreditStatus;
    private String pendingRequestIndicator;
    
    public String getAccountNumber() {
        return accountNumber;
    }
    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }
    public String getAccountCreditStatus() {
        return accountCreditStatus;
    }
    public void setAccountCreditStatus(String accountCreditStatus) {
        this.accountCreditStatus = accountCreditStatus;
    }
    public String getPaymentMethod() {
        return paymentMethod;
    }
    public String getPendingRequestIndicator() {
        return pendingRequestIndicator;
    }
    public void setPendingRequestIndicator(String pendingRequestIndicator) {
        this.pendingRequestIndicator = pendingRequestIndicator;
    }
    public void setPaymentMethod(String paymentMethod) {
        this.paymentMethod = paymentMethod;
    }
    
}
