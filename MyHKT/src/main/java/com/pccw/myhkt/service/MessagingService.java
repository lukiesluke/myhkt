/**
 * Copyright 2016 Google Inc. All Rights Reserved.
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.pccw.myhkt.service;

import static android.app.ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND;
import static android.app.ActivityManager.RunningAppProcessInfo.IMPORTANCE_VISIBLE;
import static java.util.concurrent.TimeUnit.MILLISECONDS;
import static java.util.concurrent.TimeUnit.MINUTES;

import android.app.ActivityManager;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.BitmapDrawable;
import android.os.Build;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.core.app.NotificationCompat;
import androidx.work.OneTimeWorkRequest;
import androidx.work.WorkManager;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.pccw.dango.shared.entity.SubnRec;
import com.pccw.myhkt.ClnEnv;
import com.pccw.myhkt.GlobalDialog;
import com.pccw.myhkt.R;
import com.pccw.myhkt.Utils;
import com.pccw.myhkt.activity.SplashActivity;
import com.pccw.myhkt.model.PushData;
import com.pccw.myhkt.util.Constant;

import java.util.Calendar;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Timer;
import java.util.TimerTask;

public class MessagingService extends FirebaseMessagingService {
    public static final String GCM_CHANNEL_ID_WBADGE = "GCM_CHANNEL_ID_WBADGE";
    public static final String GCM_CHANNEL_ID_WITHOUTBADGE = "GCM_CHANNEL_ID_WITHOUTBADGE";
    public static final String GCM_CHANNEL_ID_GENERAL = "GCM_CHANNEL_ID_GENERAL";
    private static final String TAG = "lwg";
    private static final boolean debug = false;
    private Timer timer;

    public MessagingService() {
        // debug = getResources().getBoolean(R.bool.DEBUG);
    }

    /**
     * Called when message is received.
     *
     * @param remoteMessage Object representing the message received from Firebase Cloud Messaging.
     */
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {

        // Check if message contains a data payload.
        if (!remoteMessage.getData().isEmpty()) {
            Log.d(TAG, "Message data payload: " + remoteMessage.getData());
            handleNow(remoteMessage);
        }

        // Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null) {
            Log.d(TAG, "Message Notification Body: " + remoteMessage.getNotification().getBody());
        }
        // Also if you intend on generating your own notifications as a result of a received FCM
        // message, here is where that should be initiated. See sendNotification method below.
    }

    /**
     * Called if InstanceID token is updated. This may occur if the security of
     * the previous token had been compromised. Note that this is called when the InstanceID token
     * is initially generated so this is where you would retrieve the token.
     */
    @Override
    public void onNewToken(@NonNull String token) {
        Log.d(TAG, "Refreshed token MsgService: " + token);

        // If you want to send messages to this application instance or
        // manage this apps subscriptions on the server side, send the
        // Instance ID token to your app server.
        sendRegistrationToServer(token);
    }

    /**
     * Schedule async work using WorkManager.
     */
    private void scheduleJob(Context context) {
        OneTimeWorkRequest work = new OneTimeWorkRequest.Builder(MyWorker.class).build();
        WorkManager.getInstance(context).beginWith(work).enqueue();
    }

    /**
     * Handle time allotted to BroadcastReceivers.
     */
    private void handleNow(RemoteMessage remoteMessage) {
        //type G Generate General Notification, type B Generate Bill Notification
        String type = remoteMessage.getData().get("type");
        Log.d(TAG, "Notification type: " + type);

        if (type != null && type.equalsIgnoreCase("G")) {
            generateNotificationGeneral(getApplicationContext(), remoteMessage);
        } else if (type != null && type.equalsIgnoreCase("N")) {
            generateNotificationNewBill(getApplicationContext(), remoteMessage);
        } else if (type != null && type.equalsIgnoreCase("B")) {
            generateNotificationLatePayment(getApplicationContext(), remoteMessage);
        }
    }

    private void generateNotificationGeneral(Context context, RemoteMessage remoteMessage) {
        if (debug) Log.d(TAG, "generateNotificationGeneral()");

        PushData pushData = new PushData();
        Map<String, String> data = remoteMessage.getData();
        if (data.get("message") != null) pushData.setMessage(data.get("message"));
        if (data.get("loginId") != null) pushData.setLoginId(data.get("loginId"));
        if (data.get("acctNum") != null) pushData.setAcctNum(data.get("acctNum"));
        if (data.get("type") != null) pushData.setType(data.get("type"));

        Intent notificationIntent = null;
        if (!isAppActive(context) || !foregrounded()) {
            // MyHKT is not running in foreground
            notificationIntent = new Intent(context, SplashActivity.class);
            // specify notification intent by setData()
            //notificationIntent.setData(new Uri.Builder().scheme("data").appendQueryParameter("text", pass).build());
            notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            notificationIntent.putExtra("GENMSG_READNOW", true);
            notificationIntent.putExtra("SERIAL_GENMSG", pushData);
        } else {
            // MyHKT is running in foreground and directly open the ServiceActivity
            ClnEnv.setPushDataGen(pushData);
            redirectDialog(context, R.string.CONST_DIALOG_MSG_GEN);
        }

        // Only notification when the App is inactive
        if (notificationIntent != null) {
            showeBillNotificationWithBadgeNumber(context, pushData, notificationIntent, 2);
        }
    }

    private void generateNotificationLatePayment(Context context, RemoteMessage remoteMessage) {
        if (debug) Log.d(TAG, "generateNotificationLatePayment()");

        Map<String, String> data = remoteMessage.getData();
        PushData pushData = new PushData();
        if (data.get("message") != null) pushData.setMessage(data.get("message"));
        if (data.get("loginId") != null) pushData.setLoginId(data.get("loginId"));
        if (data.get("acctNum") != null) pushData.setAcctNum(data.get("acctNum"));
        if (data.get("type") != null) pushData.setType(data.get("type"));

        Intent notificationIntent = null;
        if (!isAppActive(context) || !foregrounded()) {
            // MyHKT is not running in foreground
            notificationIntent = new Intent(context, SplashActivity.class);
            // specify notification intent by setData()
            //notificationIntent.setData(new Uri.Builder().scheme("data").appendQueryParameter("text", pass).build());
            notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            notificationIntent.putExtra("BILLMSG_READNOW", true);
            notificationIntent.putExtra("SERIAL_BILLMSG", pushData);

            ClnEnv.setPref(getApplicationContext(), getString(R.string.CONST_PREF_BILLMSG_NOTICEFLAG), true);
        } else {
            // MyHKT is running in foreground and directly open the ServiceActivity
            ClnEnv.setPushDataBill(pushData);
            try {
                if (ClnEnv.getPushDataBill() != null) {
                    String loginID = ClnEnv.getPushDataBill().getLoginId();
                    if (ClnEnv.isLoggedIn()) {
                        //Check subscriptionRec is valid in current ServiceList
                        boolean isValidSubnRec = false;
                        for (SubnRec rSubnRec : ClnEnv.getAssocSubnRecAry(Objects.requireNonNull(ClnEnv.getQualSvee()).getSubnRecAry())) {
                            if (rSubnRec.acctNum.equalsIgnoreCase(ClnEnv.getPushDataBill().getAcctNum())) {
                                isValidSubnRec = true;
                                break;
                            }
                        }
                        if (isValidSubnRec) {
                            redirectDialog(context, R.string.CONST_DIALOG_MSG_LATE_PAYMENT);
                        } else {
                            Utils.clearBillMsgService(this);
                        }
                    } else if ((!"".equalsIgnoreCase(loginID) && !loginID.equalsIgnoreCase(ClnEnv.getSessionLoginID())) || "".equalsIgnoreCase(ClnEnv.getSessionLoginID())) {
                        Utils.clearBillMsgService(this);
                    }
                }
            } catch (Exception e) {
                //Prevent throw null pointer exception for BillPushData
                ClnEnv.setPref(this, getString(R.string.CONST_PREF_BILLMSG_NOTICEFLAG), false);
                return;
            }
        }

        // Only notification when the App is inactive
        if (notificationIntent != null) {
            showeBillNotificationWithBadgeNumber(context, pushData, notificationIntent, 1);
        }
    }

    private void generateNotificationNewBill(Context context, RemoteMessage remoteMessage) {
        if (debug) Log.d(TAG, "generateNotificationNewBill()");

        Map<String, String> data = remoteMessage.getData();
        PushData pushData = new PushData();
        if (data.get("message") != null) pushData.setMessage(data.get("message"));
        if (data.get("loginId") != null) pushData.setLoginId(data.get("loginId"));
        if (data.get("acctNum") != null) pushData.setAcctNum(data.get("acctNum"));
        if (data.get("type") != null) pushData.setType(data.get("type"));

        Intent notificationIntent;
        if (!isAppActive(context)) {
            // MyHKT is not running in foreground
            notificationIntent = new Intent(context, SplashActivity.class);
            // specify notification intent by setData()
            //notificationIntent.setData(new Uri.Builder().scheme("data").appendQueryParameter("text", pass).build());
            notificationIntent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
            notificationIntent.putExtra("BILLMSG_READNOW", true);
            notificationIntent.putExtra("SERIAL_BILLMSG", pushData);

            ClnEnv.setPref(context, Constant.CONST_IS_BADGE_SHOWN, true);
            ClnEnv.setPref(getApplicationContext(), getString(R.string.CONST_PREF_BILLMSG_NOTICEFLAG), true);
            ClnEnv.setPushDataBill(pushData);
            showeBillNotificationWithBadgeNumber(context, pushData, notificationIntent, 3);
            pushData.setAppActive(false);
        } else {
            // MyHKT is not running in foreground
            notificationIntent = new Intent(context, SplashActivity.class);
            // specify notification intent by setData()
            //notificationIntent.setData(new Uri.Builder().scheme("data").appendQueryParameter("text", pass).build());
            notificationIntent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
            notificationIntent.putExtra("BILLMSG_READNOW", true);
            notificationIntent.putExtra("SERIAL_BILLMSG", pushData);
            ClnEnv.setPref(getApplicationContext(), getString(R.string.CONST_PREF_BILLMSG_NOTICEFLAG), true);

            startTimerToRemoveBadge(context);
            // MyHKT is running in foreground and directly open the ServiceActivity
            ClnEnv.setPushDataBill(pushData);
            try {
                if (ClnEnv.getPushDataBill() != null) {
                    String loginID = ClnEnv.getPushDataBill().getLoginId();
                    if (ClnEnv.isLoggedIn()) {

                        if ((!"".equalsIgnoreCase(loginID) && !loginID.equalsIgnoreCase(ClnEnv.getSessionLoginID())) || "".equalsIgnoreCase(ClnEnv.getSessionLoginID())) {
                            Utils.clearBillMsgService(this);
                        } else {
                            redirectDialog(context, R.string.CONST_DIALOG_MSG_NEW_BILL);
                        }
                    } else {
                        redirectDialog(context, R.string.CONST_DIALOG_MSG_NEW_BILL);
                    }
                }
            } catch (Exception e) {
                //Prevent throw null pointer exception for BillPushData
                ClnEnv.setPref(this, getString(R.string.CONST_PREF_BILLMSG_NOTICEFLAG), false);
                return;
            }

            ClnEnv.setPref(context, Constant.CONST_IS_BADGE_SHOWN, true);
            if (ClnEnv.isAppForeground()) {
                pushData.setAppActive(true);
                showeBillNotificationWithOutBadge(context, pushData, notificationIntent);
            } else {
                pushData.setAppActive(false);
                showeBillNotificationWithBadgeNumber(context, pushData, notificationIntent, 3);
            }
        }
    }

    private boolean isAppActive(Context context) {
        ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningTaskInfo> tasks = null;
        if (am != null) {
            tasks = am.getRunningTasks(1);
        }

        if (tasks != null && !tasks.isEmpty()) {
            ComponentName topActivity = tasks.get(0).topActivity;
            return Objects.requireNonNull(topActivity).getPackageName().equals(context.getPackageName());
        }
        return false;
    }

    private void startTimerToRemoveBadge(Context context) {
        timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {

                if (ClnEnv.isLoggedIn() && isAppActive(context)) {
                    NotificationManager nMgr = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                    nMgr.cancelAll();
                    ClnEnv.setPref(getApplicationContext(), Constant.CONST_IS_BADGE_SHOWN, true);
                }
            }
        }, MILLISECONDS.convert(1, MINUTES));
    }

    private void showeBillNotificationWithBadgeDot(Context context, PushData pushData, Intent notificationIntent) {
        long when = System.currentTimeMillis();

        String title = Utils.getString(getApplicationContext(), R.string.app_name);

        PendingIntent pendingIntent;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.S) {
            pendingIntent = PendingIntent.getActivity(context, Integer.parseInt(Utils.randomString(5)), notificationIntent, PendingIntent.FLAG_IMMUTABLE | PendingIntent.FLAG_UPDATE_CURRENT);
        } else {
            pendingIntent = PendingIntent.getActivity(context, Integer.parseInt(Utils.randomString(5)), notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        }

        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context, GCM_CHANNEL_ID_WBADGE);
        mBuilder.setSmallIcon(R.drawable.icon_notification);
        mBuilder.setLargeIcon(((BitmapDrawable) context.getResources().getDrawable(R.drawable.hktappicon)).getBitmap());
        mBuilder.setWhen(when);
        mBuilder.setContentTitle(title);
        mBuilder.setTicker(pushData.getMessage());
        mBuilder.setContentText(pushData.getMessage());
        mBuilder.setAutoCancel(true);
        mBuilder.setDefaults(Notification.DEFAULT_ALL);
        mBuilder.setContentIntent(pendingIntent);

        Notification notification =
                new NotificationCompat.BigTextStyle(mBuilder).bigText(pushData.getMessage()).build();

        NotificationManager mNotificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel mChannel = new NotificationChannel(GCM_CHANNEL_ID_WBADGE, "NOTIFICATION", NotificationManager.IMPORTANCE_HIGH);
            mChannel.setShowBadge(false);
            if (mNotificationManager != null) {
                mNotificationManager.createNotificationChannel(mChannel);
            }
        }

        if (mNotificationManager != null) {
            mNotificationManager.notify((int) Calendar.getInstance().getTime().getTime(), notification);
        }
    }

    private void showeBillNotificationWithBadgeNumber(Context context, PushData pushData, Intent notificationIntent, int id) {
        long when = System.currentTimeMillis();

        String title = Utils.getString(getApplicationContext(), R.string.app_name);

        PendingIntent pendingIntent;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.S) {
            pendingIntent = PendingIntent.getActivity(context, Integer.parseInt(Utils.randomString(5)), notificationIntent, PendingIntent.FLAG_IMMUTABLE | PendingIntent.FLAG_UPDATE_CURRENT);
        } else {
            pendingIntent = PendingIntent.getActivity(context, Integer.parseInt(Utils.randomString(5)), notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        }

        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context, GCM_CHANNEL_ID_WBADGE);
        mBuilder.setSmallIcon(R.drawable.icon_notification);
        mBuilder.setLargeIcon(((BitmapDrawable) context.getResources().getDrawable(R.drawable.hktappicon)).getBitmap());
        mBuilder.setWhen(when);
        mBuilder.setContentTitle(title);
        mBuilder.setTicker(pushData.getMessage());
        mBuilder.setContentText(pushData.getMessage());
        mBuilder.setAutoCancel(true);
        mBuilder.setDefaults(Notification.DEFAULT_ALL);
//        mBuilder.setNumber(1); Uncomment to display badge icon
        mBuilder.setContentIntent(pendingIntent);

        Notification notification =
                new NotificationCompat.BigTextStyle(mBuilder).bigText(pushData.getMessage()).build();

        NotificationManager mNotificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel mChannel = new NotificationChannel(GCM_CHANNEL_ID_WBADGE, "NOTIFICATION", NotificationManager.IMPORTANCE_HIGH);
            mChannel.setShowBadge(false); // Set to TRUE to display badge icon
            if (mNotificationManager != null) {
                mNotificationManager.createNotificationChannel(mChannel);
            }
        }

        if (mNotificationManager != null) {
            mNotificationManager.notify(id, notification);
        }
    }

    private void showeBillNotificationWithOutBadge(Context context, PushData pushData, Intent notificationIntent) {
        long when = System.currentTimeMillis();
        // Save all data before stopService
        String title = Utils.getString(getApplicationContext(), R.string.app_name);

        PendingIntent pendingIntent;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.S) {
            pendingIntent = PendingIntent.getActivity(context, Integer.parseInt(Utils.randomString(5)), notificationIntent, PendingIntent.FLAG_IMMUTABLE | PendingIntent.FLAG_UPDATE_CURRENT);
        } else {
            pendingIntent = PendingIntent.getActivity(context, Integer.parseInt(Utils.randomString(5)), notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        }

        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context, GCM_CHANNEL_ID_WITHOUTBADGE);
        mBuilder.setSmallIcon(R.drawable.icon_notification);
        mBuilder.setLargeIcon(((BitmapDrawable) context.getResources().getDrawable(R.drawable.hktappicon)).getBitmap());
        mBuilder.setWhen(when);
        mBuilder.setContentTitle(title);
        mBuilder.setTicker(pushData.getMessage());
        mBuilder.setContentText(pushData.getMessage());
        mBuilder.setAutoCancel(true);
        mBuilder.setDefaults(Notification.DEFAULT_ALL);
        mBuilder.setContentIntent(pendingIntent);

        Notification notification =
                new NotificationCompat.BigTextStyle(mBuilder).bigText(pushData.getMessage()).build();

        NotificationManager mNotificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            int importance = NotificationManager.IMPORTANCE_MIN;
            NotificationChannel mChannel = new NotificationChannel(GCM_CHANNEL_ID_WITHOUTBADGE, "NOTIFICATION", importance);
            if (mNotificationManager != null) {
                mNotificationManager.createNotificationChannel(mChannel);
            }
        }

        if (mNotificationManager != null) {
            mNotificationManager.notify(23456, notification);
        }
    }

    // redirect to Target Activity
    protected final void redirectDialog(Context context, int dialogType) {
        Intent dialogIntent = new Intent(context, GlobalDialog.class);
        dialogIntent.putExtra("DIALOGTYPE", dialogType);
        dialogIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        context.startActivity(dialogIntent);
    }

    /**
     * Persist token to third-party servers.
     * <p>
     * Modify this method to associate the user's FCM InstanceID token with any server-side account
     * maintained by your application.
     *
     * @param token The new token.
     */
    private void sendRegistrationToServer(String token) {
        ClnEnv.setPref(getApplicationContext(), getString(R.string.CONST_PREF_GCM_REGID), token);
    }

    public boolean foregrounded() {
        ActivityManager.RunningAppProcessInfo appProcessInfo = new ActivityManager.RunningAppProcessInfo();
        ActivityManager.getMyMemoryState(appProcessInfo);
        return (appProcessInfo.importance == IMPORTANCE_FOREGROUND || appProcessInfo.importance == IMPORTANCE_VISIBLE);
    }
}
