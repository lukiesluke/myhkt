/*
    Bill Inquiry
    
    Keywords
    --------
    $URL: $
    $Rev: $
    $Date: $
    $Author: $
*/

package com.pccw.dango.shared.entity;

import java.io.Serializable;

public class Bill  implements Serializable
{
    private static final long serialVersionUID = 4277682926362534310L;
    
    private String                  sysTy;              /* System Type                                   */
    private String                  acctNum;            /* Account Number                                */
    private String                  invDate;            /* Invoice Date                                  */
    private String                  invAmt;             /* Invoice Amount                                */
    private String                  withheld;           /* Withhold Indicator (for DRG only)             */
    private String                  dueDate;            /* Due Date (for DRG only)                       */
    private String                  status;             /* Status                                        */

    public static final String      ST_INIT         = "I";
    public static final String      ST_PREPARE      = "P";
    public static final String      ST_READY        = "R";
    public static final String      ST_ERROR        = "E";

    
    public static void main(String rArg[])
    {
        System.out.println(getVer());
        return;
    }
    
    
    public static String getVer()
    {
        return ("$URL: $, $Rev: $");
    }

    
    public Bill()
    {
        initAndClear();
    }

    
    final void initAndClear()
    {
        init();
        clear();
    }


    protected void init()
    {
    }


    public void clear()
    {
        clearSysTy();
        clearAcctNum();
        clearInvDate();
        clearInvAmt();
        clearWithheld();
        clearDueDate();
        clearStatus();
    }


    public Bill copyFrom(Bill rSrc)
    {
        setSysTy(rSrc.getSysTy());
        setAcctNum(rSrc.getAcctNum());
        setInvDate(rSrc.getInvDate());
        setInvAmt(rSrc.getInvAmt());
        setWithheld(rSrc.getWithheld());
        setDueDate(rSrc.getDueDate());
        setStatus(rSrc.getStatus());

        return (this);
    }


    public Bill copyTo(Bill rDes)
    {
        rDes.copyFrom(this);
        return (rDes);
    }
    
    
    public Bill copyMe()
    {
        Bill                        rDes;

        rDes = new Bill();
        rDes.copyFrom(this);
        return (rDes);
    }


    public void clearSysTy()
    {
        sysTy = "";
    }


    public void setSysTy(String rArg)
    {
        sysTy = rArg;
    }


    public String getSysTy()
    {
        return (sysTy);
    }


    public void clearAcctNum()
    {
        acctNum = "";
    }


    public void setAcctNum(String rArg)
    {
        acctNum = rArg;
    }


    public String getAcctNum()
    {
        return (acctNum);
    }


    public void clearInvDate()
    {
        invDate = "";
    }


    public void setInvDate(String rArg)
    {
        invDate = rArg;
    }


    public String getInvDate()
    {
        return (invDate);
    }


    public void clearInvAmt()
    {
        invAmt = "";
    }


    public void setInvAmt(String rArg)
    {
        invAmt = rArg;
    }


    public String getInvAmt()
    {
        return (invAmt);
    }


    public void clearWithheld()
    {
        withheld = "";
    }


    public void setWithheld(String rArg)
    {
        withheld = rArg;
    }


    public String getWithheld()
    {
        return (withheld);
    }


    public void clearDueDate()
    {
        dueDate = "";
    }


    public void setDueDate(String rArg)
    {
        dueDate = rArg;
    }


    public String getDueDate()
    {
        return (dueDate);
    }


    public void clearStatus()
    {
        status = "";
    }


    public void setStatus(String rArg)
    {
        status = rArg;
    }


    public String getStatus()
    {
        return (status);
    }
}
