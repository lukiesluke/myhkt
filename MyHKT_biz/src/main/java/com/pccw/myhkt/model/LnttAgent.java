package com.pccw.myhkt.model;

import java.io.Serializable;

import com.pccw.dango.shared.cra.ApptCra;
import com.pccw.dango.shared.cra.LnttCra;
import com.pccw.dango.shared.entity.SubnRec;

public class LnttAgent  implements Serializable {
	/**
	 * 
	 */
	private static final long	serialVersionUID	= 7746870928790178049L;
	// in parameters
	private String				sessTok;

	// out parameters
	private String				lnttImsSrvNum;
	private String				lnttTvSrvNum;
	private String				lnttLtsSrvNum;
	private String				endTimestamp;
	private LnttCra				lnttCra;
	private String				resultMsg;

	// For SR
	private ApptCra				apptCra;

	public LnttAgent() {
		sessTok = new String();

		lnttImsSrvNum = new String();
		lnttTvSrvNum = new String();
		lnttLtsSrvNum = new String();
		endTimestamp = new String();
		lnttCra = new LnttCra();
		resultMsg = new String();
		apptCra = new ApptCra();
	}

	public String getSessTok() {
		return sessTok;
	}

	public void setSessTok(String sessTok) {
		this.sessTok = sessTok.trim();
	}

	public String getLnttImsSrvNum() {
		return lnttImsSrvNum;
	}

	public String getLnttTvSrvNum() {
		return lnttTvSrvNum;
	}

	public String getLnttLtsSrvNum() {
		return lnttLtsSrvNum;
	}

	public String getEndTimestamp() {
		return endTimestamp;
	}

	public void setEndTimestamp(String endTimestamp) {
		this.endTimestamp = endTimestamp.trim();
	}

	public LnttCra getLnttCra() {
		return lnttCra;
	}

	public void setLnttCra(LnttCra lnttCra) {
		this.lnttCra = lnttCra;
	}

	public String getResultMsg() {
		return resultMsg;
	}

	public void setResultMsg(String resultMsg) {
		this.resultMsg = resultMsg.trim();
	}

	public ApptCra getApptCra() {
		return apptCra;
	}

	public void setApptCra(ApptCra apptCra) {
		this.apptCra = apptCra;
	}

	public String getLtrsRecStatus() {
		if (lnttCra == null) return new String();
		return lnttCra.getOLtrsRec().srStatus.trim();
	}

	public String getLnttSrvNum() {
		if (lnttCra == null) return new String();
		return lnttCra.getISubnRec().srvNum;
	}

	public final void prepareLnttCra(SubnRec[] assocSubnRecAry, SubnRec subscriptionRec, String loginId) {
		int rx;
		SubnRec rSubnRec;
		SubnRec rSubnAry[];

		lnttCra = new LnttCra();
		rSubnRec = subscriptionRec.copyMe();
		lnttCra.setISubnRec(rSubnRec);
		lnttCra.setILoginId(loginId);			// use locally

		lnttCra.setIBBNwInd(false);
		lnttCra.setIFixLnInd(false);
		lnttCra.setIEyeInd(false);
		lnttCra.setIPcdInd(false);
		lnttCra.setITvInd(false);

		lnttImsSrvNum = "";        // required for displaying result
		lnttTvSrvNum = "";        // required for displaying result
		lnttLtsSrvNum = "";        // required for displaying result

		/* The line test only work on a selected device by its BSD/DN
		 * Therefore, we should start with the selected device and
		 * check its related subscription */

		rSubnAry = assocSubnRecAry;

		// rSubnRec = selected SubscrtionRec for line test
		if (rSubnRec.lob.equals(SubnRec.LOB_PCD)) {
			lnttCra.setIBBNwInd(true);

			lnttCra.setIPcdInd(true);
			lnttImsSrvNum = rSubnRec.srvNum;

			rx = findFSA(SubnRec.LOB_TV, rSubnAry, rSubnRec);
			if (rx >= 0) {
				lnttCra.setITvInd(true);
				lnttTvSrvNum = rSubnAry[rx].srvNum;
			}

			findEye(rSubnAry, rSubnRec);
		} else if (rSubnRec.lob.equals(SubnRec.LOB_TV)) {
			lnttCra.setIBBNwInd(true);

			lnttCra.setITvInd(true);
			lnttTvSrvNum = rSubnRec.srvNum;

			rx = findFSA(SubnRec.LOB_PCD, rSubnAry, rSubnRec);
			if (rx >= 0) {
				lnttCra.setIPcdInd(true);
				lnttImsSrvNum = rSubnAry[rx].srvNum;
			}

			findEye(rSubnAry, rSubnRec);
		} else if (rSubnRec.lob.equals(SubnRec.LOB_LTS)) {
			if (rSubnRec.eyeGrp.trim().length() == 0) {
				// Probably a POTS
				lnttCra.setIFixLnInd(true);
				lnttLtsSrvNum = rSubnRec.srvNum;
			} else {
				// Of coz a Eye Phone
				lnttCra.setIBBNwInd(true);
				lnttCra.setIEyeInd(true);
				lnttLtsSrvNum = rSubnRec.srvNum;

				for (rx = 0; rx < rSubnAry.length; rx++) {
					if (rSubnAry[rx].eyeGrp.equals(rSubnRec.eyeGrp)) {
						if (rSubnAry[rx].lob.equals(SubnRec.LOB_PCD)) {
							if (!lnttCra.isIPcdInd()) {
								lnttCra.setIPcdInd(true);
								lnttImsSrvNum = rSubnAry[rx].srvNum;
							}
						} else if (rSubnAry[rx].lob.equals(SubnRec.LOB_TV)) {
							if (!lnttCra.isITvInd()) {
								lnttCra.setITvInd(true);
								lnttTvSrvNum = rSubnAry[rx].srvNum;
							}
						}
					}
				}
			}
		}
	}

	private final int findFSA(String rLob, SubnRec rSubnAry[], SubnRec rSubnRec) {
		int rx, ri, rl;

		for (rx = 0; rx < rSubnAry.length; rx++) {
			if (rLob.equals(rSubnAry[rx].lob)) {
				if (rSubnRec.fsa.equals(rSubnAry[rx].fsa)) { return (rx); }
			}
		}

		return (-1);
	}

	private final void findEye(SubnRec rSubnAry[], SubnRec rSubnRec) {
		int rx, ri, rl;

		if (rSubnRec.eyeGrp.trim().length() > 0) {
			// Search for a related Eye rec with the same eye_grp (if any)
			for (rx = 0; rx < rSubnAry.length; rx++) {
				if (rSubnAry[rx].lob.equals(SubnRec.LOB_LTS) && rSubnRec.eyeGrp.equals(rSubnAry[rx].eyeGrp)) {
					lnttLtsSrvNum = rSubnAry[rx].srvNum;
					lnttCra.setIEyeInd(true);
					break;
				}
			}
		}
	}
}
