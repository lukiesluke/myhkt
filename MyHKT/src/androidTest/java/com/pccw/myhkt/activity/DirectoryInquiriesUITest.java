package com.pccw.myhkt.activity;

import android.content.Context;
import android.content.SharedPreferences;
import androidx.test.espresso.intent.Intents;
import androidx.test.rule.ActivityTestRule;
import androidx.test.ext.junit.runners.AndroidJUnit4;
import android.view.View;

import com.pccw.myhkt.R;
import com.pccw.myhkt.base.BaseTest;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static androidx.test.platform.app.InstrumentationRegistry.getInstrumentation;

@Ignore
@RunWith(AndroidJUnit4.class)
public class DirectoryInquiriesUITest extends BaseTest {
    private Context context;
    SharedPreferences shared;


    @Rule
    public ActivityTestRule<MainMenuActivity> activityTestRule = new ActivityTestRule<>(MainMenuActivity.class);

    @Before
    public void setUp(){
        context = getInstrumentation().getTargetContext();
        shared = context.getSharedPreferences(
                "myhkt",
                Context.MODE_PRIVATE);
        Intents.init();
        gotoMainMenuItem(5);
        delay(7000);
    }

    @After
    public void tearDown() {
        Intents.release();
    }

    @Test
    public void testDisplayCompanyTabUI() {
        backBtnExist();
        checkStringIsDisplayed(context, R.string.MYHKT_DQ_PAGE_TITLE);
        checkStringIsDisplayed(context, R.string.MYHKT_DQ_LBL_FIND_COMPANY);
        checkStringIsDisplayed(context, R.string.MYHKT_DQ_LBL_FIND_PERSON);
        checkStringIsDisplayed(context, R.string.MYHKT_DQ_LBL_NAME);
        checkViewVisibility(R.id.dq_name_input, View.VISIBLE);
        checkStringIsDisplayed(context, R.string.MYHKT_DQ_LBL_AREA_DISTRICT);
        checkViewVisibility(R.id.dq_area_spinner, View.VISIBLE);
        checkViewVisibility(R.id.dq_btns, View.VISIBLE);
        checkViewIsDisabled(R.id.dq_btns);
        checkViewVisibility(R.id.dq_remarks, View.VISIBLE);
    }

    @Test
    public void testBackButton() {
        clickLeftNavBar();
    }

    @Test
    public void testCompanySearchWithManyMatchingRecords() {
        typeTextView(R.id.dq_name_input, "A");
        clickView(R.id.dq_btns);
        checkDialogStringIsDisplayed(context, R.string.MYHKT_DQ_ERR_3);
        clickDialogString("OK");
    }

    @Test
    public void testCompanySearchWithValidInput() {
        typeTextView(R.id.dq_name_input, "Lee Bik Wah");
        clickView(R.id.dq_btns);
        delay(10000);
        checkStringIsDisplayed(context, R.string.MYHKT_CNT_SEARCHING_HEADER);
        checkViewIsDisplayed(R.id.dq_result_header3);
        checkStringIsDisplayed(context, R.string.MYHKT_DQ_ERR_4);
        checkViewIsDisplayed(R.id.dq_listview);
    }

    @Test
    public void testCompanySearchWithInvalidInput() {
        typeTextView(R.id.dq_name_input, "Ggwp");
        clickView(R.id.dq_btns);
        delay(10000);
        checkDialogStringIsDisplayed(String.format("%s: %s", context.getString(R.string.DLGM_RC_GENERAL), "RC_UXPERR"));
        clickDialogString("OK");
    }

    @Test
    public void testCompanyTapResultOnValidSearch() {
        typeTextView(R.id.dq_name_input, "Lee Bik Wah");
        clickView(R.id.dq_btns);
        delay(10000);
        checkStringIsDisplayed(context, R.string.MYHKT_CNT_SEARCHING_HEADER);
        checkViewIsDisplayed(R.id.dq_result_header3);
        checkStringIsDisplayed(context, R.string.MYHKT_DQ_ERR_4);
        checkViewIsDisplayed(R.id.dq_listview);
        clickViewInAdapter(R.id.dq_listview, 1);
    }

    @Test
    public void testCompanyAreaDistrictDropdown() {
        clickView(R.id.dq_area_spinner);
    }

    @Test
    public void testDisplayPersonTabUI() {
        clickString(context, R.string.MYHKT_DQ_LBL_FIND_PERSON);
        backBtnExist();
        checkStringIsDisplayed(context, R.string.MYHKT_DQ_PAGE_TITLE);
        checkStringIsDisplayed(context, R.string.MYHKT_DQ_LBL_FIND_COMPANY);
        checkStringIsDisplayed(context, R.string.MYHKT_DQ_LBL_FIND_PERSON);
        checkStringIsDisplayed(context, R.string.MYHKT_DQ_LBL_GIVEN_NAME);
        checkViewVisibility(R.id.dq_givenname_input, View.VISIBLE);
        checkStringIsDisplayed(context, R.string.MYHKT_DQ_LBL_SURNAME);
        checkViewVisibility(R.id.dq_surname_input, View.VISIBLE);
        checkStringIsDisplayed(context, R.string.MYHKT_DQ_LBL_AREA_DISTRICT);
        checkViewVisibility(R.id.dq_area_spinner, View.VISIBLE);
        checkViewVisibility(R.id.dq_btns, View.VISIBLE);
        checkViewIsDisabled(R.id.dq_btns);
        checkViewVisibility(R.id.dq_remarks, View.VISIBLE);
    }

    @Test
    public void testPersonSearchWithManyMatchingRecords() {
        clickString(context, R.string.MYHKT_DQ_LBL_FIND_PERSON);
        typeTextView(R.id.dq_surname_input, "Lee");
        clickView(R.id.dq_btns);
        delay(10000);
        checkDialogStringIsDisplayed(context, R.string.MYHKT_DQ_ERR_3);
        clickDialogString("OK");
    }

    @Test
    public void testPersonSearchWithValidInput() {
        clickString(context, R.string.MYHKT_DQ_LBL_FIND_PERSON);
        typeTextView(R.id.dq_surname_input, "Lee");
        typeTextView(R.id.dq_givenname_input, "Lee");
        clickView(R.id.dq_btns);
        delay(10000);
        checkStringIsDisplayed(context, R.string.MYHKT_CNT_SEARCHING_HEADER);
        checkViewIsDisplayed(R.id.dq_result_header3);
        checkStringIsDisplayed(context, R.string.MYHKT_DQ_ERR_4);
        checkViewIsDisplayed(R.id.dq_listview);
    }

    @Test
    public void testPersonSearchWithInvalidInput() {
        clickString(context, R.string.MYHKT_DQ_LBL_FIND_PERSON);
        typeTextView(R.id.dq_surname_input, "Ggwp");
        clickView(R.id.dq_btns);
        delay(10000);
        checkDialogStringIsDisplayed(context, R.string.MYHKT_DQ_ERR_10);
        clickDialogString("OK");
    }

    @Test
    public void testPersonTapResultOnValidSearch() {
        clickString(context, R.string.MYHKT_DQ_LBL_FIND_PERSON);
        typeTextView(R.id.dq_surname_input, "Lee");
        typeTextView(R.id.dq_givenname_input, "Lee");
        clickView(R.id.dq_btns);
        delay(10000);
        checkStringIsDisplayed(context, R.string.MYHKT_CNT_SEARCHING_HEADER);
        checkViewIsDisplayed(R.id.dq_result_header3);
        checkStringIsDisplayed(context, R.string.MYHKT_DQ_ERR_4);
        checkViewIsDisplayed(R.id.dq_listview);
        clickString("Sai Wan Ho St Shau Kei Wan");
    }

    @Test
    public void testPersonAreaDistrictDropdown() {
        clickString(context, R.string.MYHKT_DQ_LBL_FIND_PERSON);
        clickView(R.id.dq_area_spinner);
    }

}