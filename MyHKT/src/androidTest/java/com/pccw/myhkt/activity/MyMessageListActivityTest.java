package com.pccw.myhkt.activity;

import android.content.Context;
import android.content.SharedPreferences;
import androidx.test.espresso.intent.Intents;
import androidx.test.rule.ActivityTestRule;
import androidx.test.rule.GrantPermissionRule;
import androidx.test.ext.junit.runners.AndroidJUnit4;
import android.view.View;

import com.pccw.myhkt.AccountHelper;
import com.pccw.myhkt.R;
import com.pccw.myhkt.base.BaseTest;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static androidx.test.platform.app.InstrumentationRegistry.getInstrumentation;

@Ignore
@RunWith(AndroidJUnit4.class)
public class MyMessageListActivityTest extends BaseTest {
    private Context context;
    SharedPreferences shared;

    @Rule
    public ActivityTestRule<MainMenuActivity> activityTestRule = new ActivityTestRule<>(MainMenuActivity.class);
    @Rule
    public GrantPermissionRule permissionRule = GrantPermissionRule.grant(android.Manifest.permission.WRITE_EXTERNAL_STORAGE);

    @Before
    public void setUp(){
        context = getInstrumentation().getTargetContext();
        shared = context.getSharedPreferences(
                "myhkt",
                Context.MODE_PRIVATE);
        Intents.init();
        clickLeftNavBar();
        clickString(context, R.string.myhkt_LGIF_LOGIN);
    }

    @After
    public void tearDown() {
        executeLogout(context);
        Intents.release();
    }

    @Test
    public void testEmptyMessageListDisplay() {
        loginMyCredentialsUAT(AccountHelper.getUATAccount8().getUsername(),
                AccountHelper.getUATAccount8().getPassword(), shared);
        clickView(R.id.navbar_message_icon, R.id.navbar_base_layout);
        checkStringIsDisplayed(context, R.string.title_inbox);
        checkViewIsDisplayed(R.id.navbar_button_left, R.id.navbar_base_layout);
        checkViewIsDisplayed(R.id.navbar_button_right, R.id.navbar_base_layout);
        checkViewIsDisplayed(R.id.inbox_no_message);
        checkViewIsDisplayed(R.id.inbox_bottom_message);
    }

    @Test
    public void testMessageListDisplay() {
        loginMyCredentialsPRD(AccountHelper.getPRDAccount3().getUsername(),
                AccountHelper.getPRDAccount3().getPassword(), shared);
        clickView(R.id.navbar_message_icon, R.id.navbar_base_layout);
        checkStringIsDisplayed(context, R.string.title_inbox);
        checkViewIsDisplayed(R.id.navbar_button_left, R.id.navbar_base_layout);
        checkViewIsDisplayed(R.id.navbar_button_right, R.id.navbar_base_layout);
        checkViewVisibility(R.id.inbox_no_message, View.GONE);
        checkViewIsDisplayed(R.id.inbox_bottom_message);
    }

    @Test
    public void testLOBNetvigatorBtn() {
        testLiveChat(0);
    }

    @Test
    public void testLOBNOWBtn() {
        testLiveChat(1);
    }

    @Test
    public void testLOBLTSBtn() {
        testLiveChat(2);
    }

    @Test
    public void testLOB1010Btn() {
        testLiveChat(3);
    }

    @Test
    public void testLOBCSLBtn() {
        testLiveChat(4);
    }

    @Test
    public void testLOBCancelBtn() {
        loginMyCredentialsUAT(AccountHelper.getUATAccount8().getUsername(),
                AccountHelper.getUATAccount8().getPassword(), shared);
        clickView(R.id.navbar_message_icon, R.id.navbar_base_layout);
        clickLiveChatNavBar();
        clickCANCEL();
    }

    @Test
    public void testMyMessageListBackButton() {
        loginMyCredentialsUAT(AccountHelper.getUATAccount8().getUsername(),
                AccountHelper.getUATAccount8().getPassword(), shared);
        clickView(R.id.navbar_message_icon, R.id.navbar_base_layout);
    }

    public void testLiveChat(int position){
        loginMyCredentialsUAT(AccountHelper.getUATAccount8().getUsername(),
                AccountHelper.getUATAccount8().getPassword(), shared);
        clickView(R.id.navbar_message_icon, R.id.navbar_base_layout);
        clickLiveChatNavBar();
        clickViewChildView(R.id.rv_images,position);
        delay(TestTimeouts.DIALOG_TIMEOUTS,waitForText(btn_OK));
        clickOK();
        delay(TestTimeouts.LIVECHAT_TIMEOUTS,waitForLiveChat());
        clickView(R.id.dialog_livechat_close);
        delay(TestTimeouts.DIALOG_TIMEOUTS,waitForText(btn_OK));
        clickOK();
    }

}