package com.pccw.myhkt;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.Date;

import org.apache.http.util.ByteArrayBuffer;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import com.pccw.biz.myhkt.R;

/************************************************************************
 * File : AppConfigManager.java
 * 
 * Change History:
 * Date       Modified By		Description
 * ---------- ----------------	-------------------------------
 * 29/09/2017 Abdulmoiz Esmail  - Saved the banner url into shared pref from the app_config
 *************************************************************************/
public class AppConfigManager {
	private static boolean								debug					= false;	// toggle debug logs
	private static Context cxt;
	private static AppConfigManager 					appConfigManager 		= null;
	private static AsyncTask<Context, Void, String> 	appDataAsyncTask 		= null;

	private static AppConfigListener appconfiglistener;
	
	private String endpoint = "/mba/biz/data/app_config.json";
	
	private int   imageTaskCount = 0;
	
	private String PATH = null;  //app config storage path
	private String HOST = null; // CSP HOST path
	
	private String livechat_consumer_flag = "";
	private String livechat_premier_flag = "";
	private String banner_url_en = "";
	private String banner_url_zh = "";	

	
	private AppConfigManager(Context context) {
		cxt = context;
		debug = context.getResources().getBoolean(R.bool.DEBUG);
		PATH = context.getResources().getString(R.string.PATH);  //app config storage path
//		HOST = context.getResources().getString(R.string.HOST);  //image download path
		HOST = ClnEnv.getPref(context, context.getString(R.string.CONST_PREF_FTP), APIsManager.PRD_FTP);  //image download path
	}
	
	
	public static synchronized AppConfigManager getInstance(Context context) {
		if (appConfigManager == null) {
			appConfigManager = new AppConfigManager(context);
		}
		return appConfigManager;
	}
	
	public interface AppConfigListener {
	    void onDlSuccess();
	}
	
	public void setAppConfigListener(AppConfigListener listener) {
		AppConfigManager.appconfiglistener = listener;
	}

	
	public void downloadAppConfigJson() {
		appDataAsyncTask = new AppDataAsyncTask(cxt, callbackHandler).execute(cxt);

	}
	
	//App config Json download asynctask
	public class AppDataAsyncTask extends AsyncTask<Context, Void, String> {
		private Context cxt;
		private Handler				handler;
		public static final String		asyncid			= "APPDATA";
		
		public AppDataAsyncTask(Context cxt, Handler handler) {
			super();
			this.cxt = cxt;
			this.handler = handler;
			copyJsonFromAsset(cxt, "app_config.json");
		}
		
		@Override
		protected String doInBackground(Context... params) {
			if ("OK".equalsIgnoreCase(DownloadFromUrl(PATH, HOST + endpoint, "app_config.json"))) {
				ClnEnv.setAppConfigDownloaded(true);
			} else {
				ClnEnv.setAppConfigDownloaded(false);
			}
			
			//get jsonObject
			try {
				JSONObject obj = new JSONObject(loadJson(cxt, "app_config.json")); // get json file from directory
				
				//create image directories if they are not exist
				File imgDir = new File(PATH + "images/en");
				File zhImgDir = new File(PATH + "images/zh");
				if(!imgDir.exists()) {
					imgDir.mkdirs();
				}
				if (!zhImgDir.exists()) {
					zhImgDir.mkdirs();
				}
					
				//download image
				JSONArray image_list = obj.getJSONArray("image_list");
				imageTaskCount = image_list.length() * 2;
				for (int i = 0; i < image_list.length(); i++) {
					JSONObject imagedetail = image_list.getJSONObject(i);

					String fileName = imagedetail.getString("key");
					String en_file = imagedetail.getString("en_img");
					String zh_file = imagedetail.getString("zh_img");

					new ImageAsyncTask().execute(PATH + "images/en/", HOST + "/mba/biz/" + imagedetail.getString("en_img"), fileName + ".png");
					new ImageAsyncTask().execute(PATH + "images/zh/", HOST + "/mba/biz/" + imagedetail.getString("zh_img"), fileName + ".png");
				}

				JSONObject common_config = obj.getJSONObject("common_config");
				livechat_consumer_flag = common_config.getString("livechat_consumer_flag");
				livechat_premier_flag = common_config.getString("livechat_premier_flag");
				banner_url_en = common_config.getString("banner_url_en");
				banner_url_zh = common_config.getString("banner_url_zh");
				
				return "success";
			} catch (JSONException e) {
				e.printStackTrace();
				return "failed";
			}
		}

		protected void onPostExecute(String status) {
			Bundle bundle = new Bundle();
			bundle.putString("CALLER", asyncid);
			bundle.putString("STATUS", status);
			
			ClnEnv.setPref(cxt.getApplicationContext(), "livechat_consumer_flag", "Y".equalsIgnoreCase(livechat_consumer_flag));
			ClnEnv.setPref(cxt.getApplicationContext(), "livechat_premier_flag", "Y".equalsIgnoreCase(livechat_premier_flag));
			ClnEnv.setPref(cxt.getApplicationContext(), "banner_url_en", banner_url_en);
			ClnEnv.setPref(cxt.getApplicationContext(), "banner_url_zh", banner_url_zh);
			Message msg = new Message();
			msg.setData(bundle);
			handler.sendMessage(msg);
		}
	}
	
	public class ImageAsyncTask extends AsyncTask<String, String, String>{
	    public ImageAsyncTask() {
	    } 
		
		@Override
		protected String doInBackground(String... params) {

//			dm.DownloadFromUrl(params[0], params[1]);
			if ("OK".equalsIgnoreCase(DownloadFromUrl(params[0], params[1], params[2]))) {
				ClnEnv.setAppConfigDownloaded(true);
				return "success";
			} else {
				ClnEnv.setAppConfigDownloaded(false);
				return "failed";
			}
		}

		protected void onPostExecute(String result) {
			imageTaskCount = imageTaskCount - 1;
			if (imageTaskCount == 0) {
				if ("success".equalsIgnoreCase(result))
				appconfiglistener.onDlSuccess();
			}
		}
	}

	
	//Utils
	//-------------------------------------------------------------------------------------------------------------
	public String getCommonConfigByKey(String key) {
		String ccfg = "";
		//get jsonObject
		try {
			JSONObject obj = new JSONObject(loadJson(cxt, "app_config.json"));
			//download partnership image
			JSONObject common_config = obj.getJSONObject("common_config");
			ccfg = common_config.getString(key);
			
			return ccfg;
		} catch (JSONException e) {
			e.printStackTrace();
			return "";
		}
	}
	
	public String getImagePathByKey(String key, boolean isZh) {
		try {
			JSONObject obj = new JSONObject(loadJson(cxt, "app_config.json"));
			JSONArray image_list = obj.getJSONArray("image_list");
			imageTaskCount = image_list.length() * 2;  
			for (int i = 0; i < image_list.length(); i++) {
				JSONObject imagedetail = image_list.getJSONObject(i);
				if (key.equalsIgnoreCase(imagedetail.getString("key")));
				
				String enPath = imagedetail.getString("en_img");
				String zhPath = imagedetail.getString("zh_img");
				
				return isZh ? zhPath : enPath;
			}
		} catch (JSONException e) {
			e.printStackTrace();
			return "";
		}
		return "";
	}
	
	
	public String DownloadFromUrl(String storagePath, String fileURL, String fileName) {  //this is the downloader method
		String status = "OK";
		try {
			URL url = new URL(fileURL);
			File file = new File(storagePath+fileName);
			Boolean doDownload = false;
			HttpURLConnection httpCon = (HttpURLConnection) url.openConnection();
			long date = httpCon.getLastModified();

			Date serverfileLastModified = new Date(date);
			if (date == 0){
				System.out.println("No last-modified information.");
				status = "FAILED";
			}else{
				System.out.println("Last-Modified: " + serverfileLastModified);
				if (file.exists()) {
					Date lastModified = new Date(file.lastModified());
					if(lastModified.compareTo(serverfileLastModified) != 0){
						doDownload = true;
					}else{
						if (debug) Log.e("AppConfigManager", "File :"+fileName+" Same");
					}
				}else{
					doDownload = true;
				}
			}

			if(doDownload){
				if (debug) Log.d("AppConfigManager", "download url:" + url);
				URLConnection ucon = url.openConnection();
				InputStream is = ucon.getInputStream();
				BufferedInputStream bis = new BufferedInputStream(is);
				ByteArrayBuffer baf = new ByteArrayBuffer(50);
				int current = 0;
				while ((current = bis.read()) != -1) {
					baf.append((byte) current);
				}
				FileOutputStream fos = new FileOutputStream(file,false);
				fos.write(baf.toByteArray());
				fos.close();
				file.setReadable(true, false);
				file.setWritable(true, false);
				file.setLastModified(date);
			}
			
			return status;
		} catch (IOException e) {
			e.printStackTrace();
//			Log.d("AppConfigManager", "Error: " + e);
			status = "FAILED";
			return status;
		}
	}


	public void copyJsonFromAsset(Context ctx,String fileName)
	{
		File file = new File(PATH+fileName);
		if(!file.exists()){
			try{
				InputStream is = ctx.getAssets().open(fileName);
				BufferedInputStream bis = new BufferedInputStream(is);
				ByteArrayBuffer baf = new ByteArrayBuffer(50);
				int current = 0;
				while ((current = bis.read()) != -1) {
					baf.append((byte) current);
				}
				FileOutputStream fos = new FileOutputStream(file,false);
				fos.write(baf.toByteArray());
				fos.close();
				if (debug) Log.d("AppConfigManager","File Copy Complete");
			}catch (IOException e) {
				e.printStackTrace();
//				Log.d("AppConfigManager", "Error: " + e);
			}
		}
	}

	public String loadJson(Context ctx, String fileName)
	{
		String json = null;
		File file = new File(PATH+fileName);
		InputStream in = null;
		try {
			in = new BufferedInputStream(new FileInputStream(file));
			if (in != null) {
				int size = in.available();

				byte[] buffer = new byte[size];

				in.read(buffer);
				in.close();
				json = new String(buffer, "UTF-8");
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
		return json;
	}

	public String loadJSONFromAsset(Context ctx,String fileName) {
		String json = null;
		try {

			InputStream is = ctx.getAssets().open(fileName);

			int size = is.available();

			byte[] buffer = new byte[size];

			is.read(buffer);

			is.close();

			json = new String(buffer, "UTF-8");


		} catch (IOException ex) {
			ex.printStackTrace();
			return null;
		}
		return json;

	}

	
	//Async Callbacks
	//----------------------------------------------------------------------------------------------------------------------------------
	public static final Handler	callbackHandler	= new Handler() {
		@Override
		public void handleMessage(Message msg) {
			Bundle bundle = msg.getData();
			if (cxt != null) {
				
				//App_config.json / AppURL callback
				if (AppDataAsyncTask.asyncid.equalsIgnoreCase(bundle.getString("CALLER"))) {
					appDataAsyncTask = null;
					if ("success".equalsIgnoreCase(bundle.getString("STATUS"))) {

//						appconfiglistener.onDlSuccess();
					}
					
				} 
				
			}
		}
	};
	
	
	
}
