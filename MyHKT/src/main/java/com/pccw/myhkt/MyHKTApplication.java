package com.pccw.myhkt;

import static java.util.concurrent.TimeUnit.MILLISECONDS;
import static java.util.concurrent.TimeUnit.MINUTES;

import android.app.NotificationManager;
import android.content.Context;
import android.content.res.Configuration;

import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleObserver;
import androidx.lifecycle.OnLifecycleEvent;
import androidx.lifecycle.ProcessLifecycleOwner;
import androidx.multidex.MultiDex;
import androidx.multidex.MultiDexApplication;

import com.pccw.myhkt.util.Constant;

import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

public class MyHKTApplication extends MultiDexApplication implements LifecycleObserver {
    private Timer timer = new Timer();
    private static boolean isTimerRunning = false;


    @Override
    public void onCreate() {
        super.onCreate();
//        FontsOverride.setDefaultFont(this, "DEFAULT", "Roboto-Regular.ttf");
//        FontsOverride.setDefaultFont(this, "DEFAULT_BOLD", "Roboto-Bold.ttf");
        FontsOverride.setDefaultFont(this, "MONOSPACE", "Roboto-Regular.ttf");
//        FontsOverride.setDefaultFont(this, "SERIF", "Roboto-Regular.ttf");
//        FontsOverride.setDefaultFont(this, "SANS_SERIF", "Roboto-Regular.ttf");
        ProcessLifecycleOwner.get().getLifecycle().addObserver(this);
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(LocaleHelper.onAttach(base, ClnEnv.getAppLocaleStatic(base)));
        MultiDex.install(this);
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    public void onAppBackgrounded() {
        //App in background
        ClnEnv.setAppForeground(false);

        if (ClnEnv.isLoggedIn()) {
            ClnEnv.setPref(getApplicationContext(), Constant.CONST_IDLE_START_TIME, System.currentTimeMillis());

            if (isTimerRunning) {
                timer.cancel();
                timer.purge();
                isTimerRunning = false;
            }
        }
    }


    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    public void onAppForegrounded() {
        ClnEnv.setAppForeground(true);
        // App in foreground

        // when app received notif while on Background, do checking if idle for 30 minutes or more
        if (!isTimerRunning && ClnEnv.getPref(getApplicationContext(), Constant.CONST_IS_BADGE_SHOWN, false)) {

            long MAX_DURATION = MILLISECONDS.convert(30, MINUTES);
            long duration = System.currentTimeMillis() - ClnEnv.getPref(getApplicationContext(), Constant.CONST_IDLE_START_TIME, System.currentTimeMillis());

            if (duration < MAX_DURATION) {
                // if idle for less then 30 minutes, start 1 min timer to remove badge
                timer.cancel();
                timer.purge();
                timer = new Timer();
                isTimerRunning = true;

                timer.schedule(new TimerTask() {
                    @Override
                    public void run() {
                        if (ClnEnv.isLoggedIn()) {
                            NotificationManager nMgr = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                            nMgr.cancelAll();
                            ClnEnv.setPref(getApplicationContext(), Constant.CONST_IS_BADGE_SHOWN, false);
                        }

                        isTimerRunning = false;
                    }
                }, MILLISECONDS.convert(1, MINUTES));
            } else {
                // if idle for 30 minutes and more, force a session timeout scenario
                ClnEnv.setPref(getApplicationContext(), Constant.CONST_IS_BADGE_SHOWN, false);
                ClnEnv.setPref(getApplicationContext(), Constant.CONST_SHOW_SESS_TIMEOUT, true);
            }
        }
    }

    public static void setLocale(Context context, String lang) {
        Locale locale = new Locale(lang);
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        context.getApplicationContext().getResources().updateConfiguration(config, null);
    }
}
