package com.pccw.myhkt.presenters;

import com.hannesdorfmann.mosby3.mvp.MvpBasePresenter;
import com.pccw.myhkt.views.IDDNewsView;

public class IDDNewsPresenter extends MvpBasePresenter<IDDNewsView> {

    private static final String TAG = ContactUsFragmentPresenter.class.getName();

    private IDDNewsView view;

    public void initialise(IDDNewsView view) { this.view = view; }

    public void initDialogs(String title, String content) {
        view.doPopupDialog(title, content);
    }
}
