package com.pccw.myhkt.fragment;

import android.app.Activity;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;

import com.pccw.dango.shared.cra.LnttCra;
import com.pccw.dango.shared.entity.SubnRec;
import com.pccw.myhkt.FAWrapper;
import com.pccw.myhkt.R;
import com.pccw.myhkt.cell.model.BigTextCell;
import com.pccw.myhkt.cell.model.Cell;
import com.pccw.myhkt.cell.model.IconTextCell;
import com.pccw.myhkt.cell.model.ImageViewCell;
import com.pccw.myhkt.cell.view.CellViewAdapter;
import com.pccw.myhkt.lib.ui.AAQuery;
import com.pccw.myhkt.lib.ui.HKTButton;
import com.pccw.myhkt.model.LnttAgent;

import java.util.ArrayList;
import java.util.List;

public class LnttRebootModemIntroFragment extends BaseServiceFragment {

    private static final String TAG = LnttRebootModemIntroFragment.class.getSimpleName();

    private AAQuery aq;


    private List<Cell> cellList;
    private CellViewAdapter cellViewAdapter;

    //Cell part
    private int btnWidth = 0;
    private LinearLayout frame;

    private SRCreationFragment.OnLnttListener callback_lntt;
    private LnttAgent lnttAgent;
    private LnttCra lnttCra;

    private HKTButton hktBtn; // Reboot modem button

    public LnttRebootModemIntroFragment() {

    }

    @Override
    public void onAttach(@NonNull Activity activity) {
        super.onAttach(activity);
        try {
            lnttCra = new LnttCra();
            callback_lntt = (SRCreationFragment.OnLnttListener) getParentFragment();
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement OnmyprofloginListener");
        }
    }

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View fragmentLayout = inflater.inflate(R.layout.fragment_linetest_st, container, false);

        aq = new AAQuery(fragmentLayout);
        initData();
        return fragmentLayout;
    }


    protected void initData() {
        super.initData();
        btnWidth = deviceWidth - basePadding * 4;
        cellViewAdapter = new CellViewAdapter(getActivity());

        lnttAgent = callback_main.getLnttAgent();
        lnttCra = lnttAgent.getLnttCra();
    }


    @Override
    protected void initUI() {

        cellList = new ArrayList<Cell>();
        //ScrollView
        aq.id(R.id.linetest_st_sv).backgroundColorId(R.color.white);
        frame = (LinearLayout) aq.id(R.id.linetest_st_frame).getView();

        setLTSUI();

        //Bot bar
        aq.id(R.id.linetest_st_botar).backgroundColorId(R.color.white);
        aq.padding(R.id.linetest_st_botar, basePadding, 0, basePadding, 0);
        aq.gravity(R.id.linetest_st_botar, Gravity.CENTER);
        hktBtn = aq.normTxtBtn(R.id.linetest_st_btn, getActivity().getResources().getString(R.string.reboot_start_btn), btnWidth);
        aq.id(R.id.linetest_st_btn).margin(0, 5, 0, 5);
        aq.id(R.id.linetest_st_btn).clicked(this, "onClick");

        if (callback_main != null) {
            // Screen Tracker
            if (SubnRec.LOB_PCD.equalsIgnoreCase(callback_main.getSubnRec().lob)) {
                FAWrapper.getInstance().sendFAScreen(getActivity(),
                        R.string.CONST_SCRN_PCD_REBOOT_INTRO, false);
            } else if (SubnRec.LOB_TV.equalsIgnoreCase(callback_main.getSubnRec().lob)) {
                FAWrapper.getInstance().sendFAScreen(getActivity(),
                        R.string.CONST_SCRN_TV_REBOOT_INTRO, false);
            }

            Log.d(TAG, "LOB: " + callback_main.getSubnRec().lob);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
    }


    public void setLTSUI() {

        IconTextCell titleCell = new IconTextCell(R.drawable.test_start_icon , getResString(R.string.myhkt_reboot));
        titleCell.setLeftPadding(basePadding);
        titleCell.setRightPadding(basePadding);
        titleCell.setTopMargin(0);
        cellList.add(titleCell);

        String description = callback_main.getSubnRec().lob.equalsIgnoreCase(SubnRec.LOB_PCD) ? getResString(R.string.reboot_description_pcd) :
                getResString(R.string.reboot_description_tv);

        BigTextCell introCell = new BigTextCell("", description);
        introCell.setLeftMargin(basePadding);
        introCell.setRightMargin(basePadding);
        introCell.setContentSizeDelta(0);
        cellList.add(introCell);

        //default image
        Drawable drawable = callback_main.getSubnRec().lob.equalsIgnoreCase(SubnRec.LOB_PCD) ? getResources().getDrawable(R.drawable.lntt_pcd) : getResources().getDrawable(R.drawable.lntt_tv);
        ImageViewCell imageViewCell = new ImageViewCell(drawable);
        imageViewCell.setLeftMargin((int) getResources().getDimension(R.dimen.lntt_image_padding));
        imageViewCell.setRightMargin((int) getResources().getDimension(R.dimen.lntt_image_padding));
        cellList.add(imageViewCell);

        cellViewAdapter.setView(frame, cellList);
    }


    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.linetest_st_btn:

                // Event Tracker
                FAWrapper.getInstance().sendFAEvents(this.getActivity(),
                        R.string.CONST_GA_CATEGORY_USERLV, R.string.CONST_GA_ACTION_BTN,
                        R.string.CONST_GA_LABEL_PPS,
                        " (" + callback_main.getSubnRec().lob + ")", true);

                callback_main.onShowModemTab(true);

            break;
        }

    }

}
