package com.pccw.myhkt.activity;

import android.content.Context;
import android.content.SharedPreferences;
import androidx.test.platform.app.InstrumentationRegistry;
import androidx.test.rule.ActivityTestRule;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import com.pccw.myhkt.AccountHelper;
import com.pccw.myhkt.R;
import com.pccw.myhkt.base.BaseTest;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static androidx.test.platform.app.InstrumentationRegistry.getInstrumentation;

@RunWith(AndroidJUnit4.class)
public class BillingAddressLTSTest extends BaseTest {

    private Context context;

    private SharedPreferences shared;
    SharedPreferences.Editor prefEditor;


    @Rule
    public ActivityTestRule<MainMenuActivity> activityTestRule =
            new ActivityTestRule<MainMenuActivity>(MainMenuActivity.class) {
                @Override
                protected void beforeActivityLaunched() {
                    clearSharedPrefs(InstrumentationRegistry.getTargetContext());
                    InstrumentationRegistry.getTargetContext().deleteDatabase("myhkt.db");
                    super.beforeActivityLaunched();
                }
            };

    private void clearSharedPrefs(Context context) {
        SharedPreferences prefs =
                context.getSharedPreferences("myhkt", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.clear();
        editor.commit();
    }

    @Before
    public void setUp() {
        context = getInstrumentation().getTargetContext();
        shared = context.getSharedPreferences(
                "myhkt",
                Context.MODE_PRIVATE);
//        Intents.init();
        clickLeftNavBar();
        clickString(context, R.string.myhkt_LGIF_LOGIN);
        loginTestSR1();
        clickViewInAdapter(R.id.servicelist_listView, 0);
        delay(10000);
        clickString(context, R.string.myhkt_summary_updateBillAddr);
        delay(10000);
    }

    @After
    public void tearDown() {
        clickLeftNavBar();
        clickString(context,R.string.SHLF_LOGOUT);
        clickString(context,R.string.myhkt_btn_yes);
    }

    @Test
    public void checkDisplayUI() {
        //check lang ng display ui tinatamad pa ko gawin wait lang
    }

    @Test
    public void billLanguagePopupClick() {
        swipeUpView(R.id.billinfolts_viewpager);
        clickView(R.id.popover_input_layout, R.id.billinfo_popover_lang);
        clickString(context, R.string.BUPLTF_BILANGEN);
        clickView(R.id.popover_input_layout, R.id.billinfo_popover_lang);
        clickString(context, R.string.BUPLTF_BILANGBI);
        clickView(R.id.popover_input_layout, R.id.billinfo_popover_lang);
        clickString(context, R.string.BUPLTF_BILANGZH);
    }

    @Test
    public void billMediaPopupClick() {
        swipeUpView(R.id.billinfolts_viewpager);
        clickView(R.id.popover_input_layout, R.id.billinfo_popover_billmedia);
        clickString(context, R.string.BUPLTF_MEDIA_P);
        clickView(R.id.popover_input_layout, R.id.billinfo_popover_billmedia);
        clickString(context, R.string.BUPLTF_MEDIA_E);
    }

    @Test
    public void changeButtonClick() {
        clickString(context, R.string.BUPLTF_BTN_UPDATE);
        delay(10000);
        clickLeftNavBar();
        delay(10000);
        clickLeftNavBar();
        clickLeftNavBar();
    }

    @Test
    public void noEmailUpdateClick() {
        clearTextView(R.id.billinfo_etxt_email);
        clickString(context, R.string.BUPLTF_BTN_UPDATE);
        clickDialogString("OK");
    }

    @Test
    public void validEmailUpdateClick() {
        typeTextView(R.id.reg_input_layoutet,R.id.billinfo_etxt_email,"uatbill11@yahoo.com.hk");
        clickString(context, R.string.BUPLTF_BTN_UPDATE);
        delay(10000);
        clickDialogString("CONFIRM");
    }

    @Test
    public void invalidEmailUpdateClick() {
        typeTextView(R.id.reg_input_layoutet,R.id.billinfo_etxt_email,"a@");
        clickString(context, R.string.BUPLTF_BTN_UPDATE);
        clickDialogString("OK");
    }

    @Test
    public void noMobileUpdateClick() {
        clearTextView(R.id.billinfo_etxt_sms);clickString(context, R.string.BUPLTF_BTN_UPDATE);
        delay(10000);
        clickDialogString("CONFIRM");
        delay(10000);
        clickDialogString("OK");
    }

    @Test
    public void validMobileUpdateClick() {
        typeTextView(R.id.reg_input_layoutet,R.id.billinfo_etxt_email,"61234567");
        clickString(context, R.string.BUPLTF_BTN_UPDATE);
        delay(10000);
        clickDialogString("CONFIRM");
        delay(10000);
        clickDialogString("OK");
        clearTextView(R.id.billinfo_etxt_sms);clickString(context, R.string.BUPLTF_BTN_UPDATE);
        delay(10000);
        clickDialogString("CONFIRM");
        delay(10000);
        clickDialogString("OK");
    }

    @Test
    public void invalidMobileUpdateClick() {
        typeTextView(R.id.reg_input_layoutet,R.id.billinfo_etxt_email,"1234567890");
        clickString(context, R.string.BUPLTF_BTN_UPDATE);
        clickDialogString("OK");
    }

    @Test
    public void backButtonClick() {
        clickString(context, R.string.btn_back);
        clickLeftNavBar();
    }

    @Test
    public void backButtonNavigationClick() {
        clickLeftNavBar();
    }

    @Test
    public void swipeLeftExecute() {
        swipeLeftView(R.id.billinfolts_viewpager);
        delay(10000);
        clickLeftNavBar();
        clickLeftNavBar();
    }

    private void loginTestSR1() {
        loginMyCredentialsUAT(AccountHelper.getUATAccount6().getUsername(), AccountHelper.getUATAccount6().getPassword(), shared);
        delay(3000);
        gotoMainMenuItem(0);
    }
}
