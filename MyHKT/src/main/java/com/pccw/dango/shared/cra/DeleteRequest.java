package com.pccw.dango.shared.cra;

public class DeleteRequest extends BaseCraEx {
    private String iLoginId;
    private int iSveeRid;

    public void setiLoginId(String iLoginId) {
        this.iLoginId = iLoginId;
    }

    public void setiSveeRid(int iSveeRid) {
        this.iSveeRid = iSveeRid;
    }
}
