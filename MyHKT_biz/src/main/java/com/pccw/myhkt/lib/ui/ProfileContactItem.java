package com.pccw.myhkt.lib.ui;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.Transformation;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.LinearLayout.LayoutParams;

import com.androidquery.AQuery;
import com.pccw.myhkt.DialogHelper;
import com.pccw.biz.myhkt.R;
import com.pccw.myhkt.Utils;

public class ProfileContactItem extends LinearLayout{

	private AAQuery aq;
	private int extralineSpace = 0;
	private int padding_screen = 0;
	private int itemHeight = 0;
	private Boolean isExpand = true;
	private Context context;

	/**
	 * If customer has subscribed C-SIM or 1O1O services only (without H-SIM) 
	 * Clicks the arrow (highlighted in BLACK) next to the option of “1O1O/ csl” , will pop-up a message box 
	 */
	public Boolean isCSimOnly = false;
	
	private TextView textViewHeader;
	private TextView textViewMobNum;
	private TextView textViewOfficeNum;
	private TextView textViewHomeNum;
	private TextView textViewEmail;
	private int 	 greylineHeight = 0;

	private static int DEFAULT_TITLE_SIZE = 14;
	private static final int DEFAULT_TITLE_TXT_COLOR = R.color.hkt_navbarblue;
	private static final int DEFAULT_NA_TXT_COLOR = R.color.hkt_txtcolor_grey_disable;
	public ProfileContactItem(Context context) {
		super(context);
		//		initView(context, "hi");
		DEFAULT_TITLE_SIZE = context.getResources().getInteger(R.integer.textsize_default_int);
	}
	public ProfileContactItem(Context context, AttributeSet attrs) {
		super(context, attrs);
		initView(context, attrs);
		// TODO Auto-generated constructor stub
		DEFAULT_TITLE_SIZE = context.getResources().getInteger(R.integer.textsize_default_int);

	}
	public ProfileContactItem(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		initView(context, attrs);
		DEFAULT_TITLE_SIZE = context.getResources().getInteger(R.integer.textsize_default_int);
	}

	private void initView(final Context context, AttributeSet attrs) {
		TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.HomeImageButton, 0, 0);
		aq = new AAQuery(this);
		greylineHeight = (int) getResources().getDimension(R.dimen.greyline_height);
		padding_screen = (int) getResources().getDimension(R.dimen.padding_screen);
		LayoutInflater.from(context).inflate(R.layout.item_profile_contact, this);
		try {
			//			initView(context,"", "","","","");	

		} finally {
			a.recycle();
		}
	}

	public void initView(Context context, String header, String mob, String officeMob, String homeMob, String email){
		this.context = context;
		LayoutInflater.from(context).inflate(R.layout.item_profile_contact, this , false );
		textViewHeader = (TextView) aq.id(R.id.item_profilecontact_header).getView();
		aq.id(R.id.item_profilecontact_header).textSize(DEFAULT_TITLE_SIZE);
		textViewHeader.setGravity(Gravity.CENTER_VERTICAL);
		aq.id(R.id.item_profilecontact_header).height(context.getResources().getDimensionPixelOffset(R.dimen.edittext_height), false);
		aq.id(R.id.item_profilecontact_header).text(header);
		Drawable drawable = context.getResources().getDrawable(isExpand? R.drawable.btn_arrowup :R.drawable.btn_arrowdown);
		drawable.setBounds(0, 0, drawable.getMinimumWidth(),drawable.getMinimumHeight());	
		textViewHeader.setOnClickListener(new OnClickListener(){
			@Override
			public void onClick(View v) {
				if (isExpand){
					collapse(aq.id(R.id.profilecontactInfo).getView());					
				} else {
					expand(aq.id(R.id.profilecontactInfo).getView());					
				}
				isExpand = !isExpand;
				setArrow();
			}			
		});
		setArrow();

		setLeftDrawable(R.id.item_profilecontact_mobilenum, R.drawable.contact_mobileno);
		setLeftDrawable(R.id.item_profilecontact_officenum, R.drawable.contact_daycontact);
		setLeftDrawable(R.id.item_profilecontact_homenum, R.drawable.contact_nightcontact);
		setLeftDrawable(R.id.item_profilecontact_email, R.drawable.contact_email);

		setText(R.id.item_profilecontact_mobilenum, mob);
		setText(R.id.item_profilecontact_officenum, officeMob);
		setText(R.id.item_profilecontact_homenum, homeMob);
		setText(R.id.item_profilecontact_email, email);
		aq.id(R.id.item_profilecontact_line).image(R.drawable.greyline).height(greylineHeight, false);
	}
	
	public void setText(String mob, String officeMob, String homeMob, String email) {
		setText(R.id.item_profilecontact_mobilenum, mob);
		setText(R.id.item_profilecontact_officenum, officeMob);
		setText(R.id.item_profilecontact_homenum, homeMob);
		setText(R.id.item_profilecontact_email, email);
	}

	private void setLeftDrawable(int viewId, int resId) {
		Drawable drawable = context.getResources().getDrawable(resId);		
		drawable.setBounds(0, 0, drawable.getMinimumWidth(),drawable.getMinimumHeight());
		aq.id(viewId).getTextView().setCompoundDrawablesWithIntrinsicBounds(drawable,null,null,null);
		aq.id(viewId).getTextView().setCompoundDrawablePadding(padding_screen);
		drawable = null;
	}

	private void setText(int viewId, String text){
		if (text == null || text.equals("")) {
			aq.id(viewId).text("N/A").textColorId(DEFAULT_NA_TXT_COLOR).textSize(DEFAULT_TITLE_SIZE);
		} else {
			aq.id(viewId).text(text).textColorId(DEFAULT_TITLE_TXT_COLOR).textSize(DEFAULT_TITLE_SIZE);
		}
	}
	public void setArrow(){
		Drawable drawable = context.getResources().getDrawable(isExpand? R.drawable.btn_arrowup :R.drawable.btn_arrowdown);		
		drawable.setBounds(0, 0, drawable.getMinimumWidth(),drawable.getMinimumHeight());
		textViewHeader.setCompoundDrawablesWithIntrinsicBounds(null,null,drawable,null);
		drawable = null;		
	}
	
	public void setExpandState(Boolean isExpand) {
		this.isExpand = isExpand;
		setArrow();
		View v = aq.id(R.id.profilecontactInfo).getView();
		if (isExpand) {			
			v.getLayoutParams().height = 0;
			v.setVisibility(View.VISIBLE);
			v.measure(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT);
			v.getLayoutParams().height = v.getMeasuredHeight();
		} else {
			v.setVisibility(View.GONE);
		}
	}



	// Animation of Expand Info
	private void expand(final View v) {
		if (isCSimOnly){
			AlertDialog.Builder builder = new Builder(context);
			builder.setMessage(R.string.myhkt_myprofcontact_update_alert);
			builder.setPositiveButton(R.string.btn_ok, null);
			builder.setCancelable(false);
			builder.create();
			builder.show();
		}
		v.measure(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT);
		final int targtetHeight = v.getMeasuredHeight();

		v.getLayoutParams().height = 0;
		v.setVisibility(View.VISIBLE);
		Animation a = new Animation() {
			@Override
			protected void applyTransformation(float interpolatedTime, Transformation t) {
				v.getLayoutParams().height = interpolatedTime == 1 ? LayoutParams.WRAP_CONTENT : (int) (targtetHeight * interpolatedTime);
				v.requestLayout();
			}

			@Override
			public boolean willChangeBounds() {
				return true;
			}
		};

		// 1dp/ms
		a.setDuration((int) (targtetHeight / v.getContext().getResources().getDisplayMetrics().density) * 3);
		v.startAnimation(a);
	}

	private void collapse(final View v) {
		final int initialHeight = v.getMeasuredHeight();

		Animation a = new Animation() {
			@Override
			protected void applyTransformation(float interpolatedTime, Transformation t) {
				if (interpolatedTime == 1) {
					v.setVisibility(View.GONE);
				} else {
					v.getLayoutParams().height = initialHeight - (int) (initialHeight * interpolatedTime);
					v.requestLayout();
				}
			}

			@Override
			public boolean willChangeBounds() {
				return true;
			}
		};

		// 1dp/ms
		a.setDuration((int) (initialHeight / v.getContext().getResources().getDisplayMetrics().density) * 3);
		v.startAnimation(a);
	}


}
