/*
    Class for Service Request Appointment Info
    
    Keywords
    --------
    $URL: $
    $Rev: $
    $Date: $
    $Author: $
*/

package com.pccw.dango.shared.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;

public class SRApptInfo implements Serializable
{
    private static final long serialVersionUID = 7511017040550096327L;
    
    private String                  refNum;
    private String                  prodTy;
    private String                  srvTy;
    private String                  areaCd;
    private String                  districtCd;
    private String                  srvNum;
    private String                  prodId;
    private SRApptTS                apptTS;    
    
    private SRApptTS[]              apptTimeSlotAry;
    
    
    public static void main(String rArg[])
    {
        System.out.println(getVer());
        return;
    }
    
    
    public static String getVer()
    {
        return ("$URL: $, $Rev: $");
    }

    
    public SRApptInfo()
    {
        initAndClear();
    }

    
    final void initAndClear()
    {
        init();
        clear();
    }
    
    
    protected void init()
    {
    }
    
    
    public void clear()
    {
        clearRefNum();
        clearProdTy();
        clearSrvTy();
        clearAreaCd();
        clearDistrictCd();
        clearSrvNum();
        clearProdId();
        clearApptTS();
        clearApptTimeSlotAry();
    }
    
    
    public SRApptInfo copyFrom(SRApptInfo rSrc)
    {
        setRefNum(rSrc.getRefNum());
        setProdTy(rSrc.getProdTy());
        setSrvTy(rSrc.getSrvTy());
        setAreaCd(rSrc.getAreaCd());
        setDistrictCd(rSrc.getDistrictCd());
        setSrvNum(rSrc.getSrvNum());
        setProdId(rSrc.getProdId());
        setApptTS(rSrc.getApptTS().copyMe());
        setApptTimeSlotAry(rSrc.getApptTimeSlotAry());
        
        return (this);
    }
    
    
    public SRApptInfo copyTo(SRApptInfo rDes)
    {
        rDes.copyFrom(this);
        return (rDes);
    }
    
    
    public SRApptInfo copyMe()
    {
        SRApptInfo             rDes;
        
        rDes = new SRApptInfo();
        rDes.copyFrom(this);
        
        return (rDes);
    }


    public void clearRefNum()
    {
        setRefNum("");
    }
    
    
    public void setRefNum(String rArg)
    {
        refNum = rArg;
    }
    
    
    public String getRefNum()
    {
        return (refNum);
    }

    
    public void clearProdTy()
    {
        setProdTy("");
    }
    
    
    public void setProdTy(String rArg)
    {
        prodTy = rArg;
    }
    
    
    public String getProdTy()
    {
        return (prodTy);
    }

    
    
    public void clearSrvTy()
    {
        setSrvTy("");
    }
    
    
    public void setSrvTy(String rArg)
    {
        srvTy = rArg;
    }
    
    
    public String getSrvTy()
    {
        return (srvTy);
    }

    
    public void clearAreaCd()
    {
        setAreaCd("");
    }
    
    
    public void setAreaCd(String rArg)
    {
        areaCd = rArg;
    }
    
    
    public String getAreaCd()
    {
        return (areaCd);
    }
    
    
    public void clearDistrictCd()
    {
        setDistrictCd("");
    }
    
    
    public void setDistrictCd(String rArg)
    {
        districtCd = rArg;
    }
    
    
    public String getDistrictCd()
    {
        return (districtCd);
    }
    
    
    public void clearSrvNum()
    {
        setSrvNum("");
    }
    
    
    public void setSrvNum(String rArg)
    {
        srvNum = rArg;
    }
    
    
    public String getSrvNum()
    {
        return (srvNum);
    }

    
    public void clearProdId()
    {
        setProdId("");
    }
    
    
    public void setProdId(String rArg)
    {
        prodId = rArg;
    }
    
    
    public String getProdId()
    {
        return (prodId);
    }

    
    public void clearApptTS()
    {
        apptTS = new SRApptTS();
    }
    
    
    public void setApptTS(SRApptTS rArg)
    {
        apptTS = rArg;
    }
    
    
    public SRApptTS getApptTS()
    {
        return apptTS;
    }

    
    public void clearApptTimeSlotAry()
    {
        setApptTimeSlotAry(new SRApptTS[0]);
    }
    
    
    public void setApptTimeSlotAry(SRApptTS[] rArg)
    {
        apptTimeSlotAry = rArg;
    }
    
    
    public void setApptTimeSlotAry(ArrayList<SRApptTS> rApptTimeSlotLst) 
    {
        if (rApptTimeSlotLst.size() > 0) {
            apptTimeSlotAry = rApptTimeSlotLst.toArray(new SRApptTS[rApptTimeSlotLst.size()]);
        }
    }
    
    
    public SRApptTS[] getApptTimeSlotAry()
    {
        return (apptTimeSlotAry);
    }

    
    public void sortApptTimeSlotAry() 
    {
        if (apptTimeSlotAry.length <= 0) return;
        
        Arrays.sort(apptTimeSlotAry,
                new Comparator<SRApptTS>()
                {
                    public int compare(SRApptTS rA, SRApptTS rB)
                    {
                        int             rx;

                        /*
                            Sort the available appointment timeslot by: 
                            
                            (1) ApptDate ==> [YYYYMMDD]
                            (2) ApptDtTime().substring(7) ==> [HH-HH]
                        */
                        rx = rA.getApptDate().compareTo(rB.getApptDate());
                        if (rx == 0) rx = rA.getApptDT().substring(7).compareTo(rB.getApptDT().substring(7));
                        
                        return (rx);
                    }
                }
            );
    }
 }
