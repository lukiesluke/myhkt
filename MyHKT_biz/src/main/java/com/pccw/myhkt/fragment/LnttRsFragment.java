package com.pccw.myhkt.fragment;

import java.lang.Thread.UncaughtExceptionHandler;
import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Point;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.google.gson.Gson;
import com.pccw.dango.shared.cra.ApptCra;
import com.pccw.dango.shared.cra.LnttCra;
import com.pccw.dango.shared.entity.LtrsRec;
import com.pccw.dango.shared.entity.SubnRec;
import com.pccw.dango.shared.tool.RC;
import com.pccw.myhkt.APIsManager;
import com.pccw.myhkt.APIsResponse;
import com.pccw.myhkt.ClnEnv;
import com.pccw.myhkt.DialogHelper;
import com.pccw.myhkt.GlobalDialog;
import com.pccw.myhkt.InterpretRCManager;
import com.pccw.biz.myhkt.R;
import com.pccw.myhkt.Utils;
import com.pccw.myhkt.activity.BaseActivity;
import com.pccw.myhkt.activity.ImageViewerActivity;
import com.pccw.myhkt.activity.MyApptActivity;
import com.pccw.myhkt.cell.model.BigTextCell;
import com.pccw.myhkt.cell.model.Cell;
import com.pccw.myhkt.cell.model.IconTextCell;
import com.pccw.myhkt.cell.model.ImageViewCell;
import com.pccw.myhkt.cell.model.LineTest;
import com.pccw.myhkt.cell.model.SingleBtnCell;
import com.pccw.myhkt.cell.model.WebViewCell;
import com.pccw.myhkt.cell.view.CellViewAdapter;
import com.pccw.myhkt.fragment.SRCreationFragment.OnLnttListener;
import com.pccw.myhkt.lib.ui.AAQuery;
import com.pccw.myhkt.lib.ui.HKTButton;
import com.pccw.myhkt.model.LnttAgent;
import com.pccw.myhkt.service.LineTestIntentService;


/************************************************************************
 * File : LnttStartFragment.java 
 * Desc : Reg Menu 
 * Name : LnttStartFragment
 * by : Andy Wong 
 * Date : 23/02/2016
 * 
 * Change History:
 * Date       Modified By		Description
 * ---------- ----------------	-------------------------------
 * 23/02/2016 Andy Wong 		-First draft
 *************************************************************************/
public class LnttRsFragment extends BaseServiceFragment{

	private String TAG = "LnttRsFragment";
	private LnttRsFragment		me;
	private	AAQuery					aq;
	private int 					extralinespace;
	private int 					titleWidth;
	private int 					botBarHeight;

	//Image URL
	//	private final String modemImageLargeURL = "http://interfacelift.com/wallpaper/previews/03567_risingfromorange@2x.jpg"; //warning
	//	private final String modemImageURL = "https://cspuat.pccw.com/cs/mdm_sample.JPG"; //warning
	//	private final String modemImageURL = "https://cspuat.pccw.com/cs/MD_2B_S.png"; //warning
	//	private String modemImageLargeURL = "https://cspuat.pccw.com/cs/MD_2B_L2.png"; //warning
	private String modemImageLargeURL = "";

	private List<Cell>				cellList;
	private CellViewAdapter			cellViewAdapter;
	private IconTextCell titleCell;
	private WebViewCell enquiryCell;
	private SingleBtnCell singleBtnCell;

	//Cell part
	private BigTextCell mdmMsgCell;
	private BigTextCell timeCell;
	private BigTextCell bbMsgCell;
	private BigTextCell fixlineMsgCell;
	private BigTextCell resultCell;
	private BigTextCell fixlineTitleCell;
	private ImageViewCell imageViewCell;
	private LineTest pcdLiveTestCell;
	private LineTest tvLiveTestCell;
	private LineTest eyeLiveTestCell;
	private int btnWidth = 0;
	private LinearLayout frame;

	private OnLnttListener callback_lntt;
	private LnttAgent  lnttAgent;
	private LnttCra    lnttCra;
	private ApptCra		apptCra;
	private Boolean isLnttStart = false;

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		try {

			//			callback = (OnIDDCodeNtimesListener) activity;
			lnttCra = new LnttCra();
			apptCra = new ApptCra();
		} catch (ClassCastException e) {
			throw new ClassCastException(activity.toString() + " must implement OnmyprofloginListener");
		}
	}

	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		me = this;		
		View fragmentLayout = inflater.inflate(R.layout.fragment_linetest_st, container, false);

		aq = new AAQuery(fragmentLayout);
		initData();	
		if (debug) Log.i(TAG, "onCreateView");
		return fragmentLayout;
	}


	protected void initData() {
		super.initData();
		titleWidth = (int) getResources().getDimension(R.dimen.idd_title_width);
		botBarHeight	= (int) getResources().getDimension(R.dimen.botbar_height);
		btnWidth = deviceWidth - basePadding * 4; 
		cellViewAdapter = new CellViewAdapter(me.getActivity());
	}

	private HKTButton hktBtn;
	@Override
	protected void initUI() {

		cellList	= new ArrayList<Cell>();
		//ScrollView
		aq.id(R.id.linetest_st_sv).backgroundColorId(R.color.white);
		frame = (LinearLayout) aq.id(R.id.linetest_st_frame).getView();

		setLTSUI();

		//Bot bar
		aq.id(R.id.linetest_st_botar).height(botBarHeight, false).backgroundColorId(R.color.white);
		aq.padding(R.id.linetest_st_botar, basePadding, 0, basePadding, 0);
		aq.gravity(R.id.linetest_st_botar, Gravity.CENTER);
		hktBtn = aq.normTxtBtn(R.id.linetest_st_btn, getResString(R.string.LTTF_TEST), btnWidth);
		aq.id(R.id.linetest_st_btn).clicked(this, "onClick");
	}


	public void setLTSUI(){
		titleCell = new IconTextCell(R.drawable.test_result_icon , getResString(R.string.myhkt_linetest_result));
		titleCell.setTopMargin(0);
		cellList.add(titleCell);

		//		BigTextCell timeCell = new BigTextCell("", Utils.toDateString(lnttCra.getServerTS(), Utils.getString(me.getActivity(), R.string.input_datetime_format), "dd/MM/yyyy HH:mm");
		timeCell = new BigTextCell("", "dd/MM/yyyy HH:mm");
		timeCell.setTopMargin(0);
		timeCell.setBotMargin(0);
		timeCell.setContentSizeDelta(0);
		timeCell.setContentColorId(R.color.hkt_textcolor);
		timeCell.setArrowShown(true);		

		enquiryCell = new WebViewCell(isZh ? "file:///android_asset/lntt_reslut_lts_enquiry_en.html" : "file:///android_asset/lntt_reslut_lts_enquiry_zh.html");
		cellList.add(enquiryCell);

		singleBtnCell = new SingleBtnCell(getResString(R.string.myhkt_linetest_hotline), deviceWidth /2- extralinespace, null);
		singleBtnCell.setDraw(R.drawable.phone_small);
		singleBtnCell.setBgColorId(R.color.cell_bg_grey);
		singleBtnCell.setTopPadding(0);
		cellList.add(singleBtnCell);

		cellViewAdapter.setView(frame, cellList);
	}



	public void onImageClick(View v){	
		Intent intent = new Intent(me.getActivity(), ImageViewerActivity.class);
		intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		Bundle bundle = new Bundle();
		bundle.putString("NAVTEXT" , callback_main.getSubnRec().srvNum);
		bundle.putInt("LOBICON", callback_main.getLob());			
		bundle.putInt("LOBTYPE ", callback_main.getLtsType());			
		bundle.putString("IMAGEURL", modemImageLargeURL);
		
		intent.putExtras(bundle);
		startActivity(intent);
		me.getActivity().overridePendingTransition(R.anim.right_slide_in, R.anim.left_slide_out);
	}

	public void onClick(View v) {
		Intent intent;
		Bundle bundle;
		if (debug) Log.i(TAG, "Click");
		if (lnttCra != null && apptCra != null) {
			String srStatus =  lnttCra.getOLtrsRec().srStatus;		
			if (apptCra.getReply().getCode().equals(RC.USRB_PENDSR_FND)) {
				if (debug) Log.i(TAG, "Click1");
				// SR found , goto Appt List
				intent = new Intent(getActivity().getApplicationContext(), MyApptActivity.class);
				startActivity(intent);
				getActivity().overridePendingTransition(R.anim.right_slide_in, R.anim.left_slide_out);
			} else if (apptCra.getReply().getCode().equals(RC.SUCC) && (("T".equalsIgnoreCase(srStatus) && lnttCra.isIBBNwInd()) || "V".equalsIgnoreCase(srStatus) || "B".equalsIgnoreCase(srStatus))) {
				if (debug) Log.i(TAG, "Click2");
				// Service NOT Good && No SR found, then Reset and ReTest / goto Report Fault
				if ("T".equalsIgnoreCase(srStatus)) {
					if (debug) Log.i(TAG, "Click2.1");
					doLineTestRequest();										// Reset and ReTest
				} else if ("V".equalsIgnoreCase(srStatus) || "B".equalsIgnoreCase(srStatus)) {
					// recall doEnquireSR4NoPend to get oPendSrvReq Object for GetTimeSlot and Create SR
					if (debug) Log.i(TAG, "Click2.2");
					doEnquireSR4NoPend();
				}
			} else {
				if (debug) Log.i(TAG, "Click3");
				// Case : lnttRec is Good || status != "T" / "V" / "B", then Test Again Button
				// Any other cases, handle it as test again
				lnttCra = null;
				doLineTestRequest();
			}
		}
	}

	private final void doEnquireSR4NoPend() {
		if (callback_main.getSubnRec() != null) {
			// doEnquireSR4NoPend
			if (callback_main.getSubnRec() !=null) {
				apptCra = new ApptCra();
				apptCra.setILoginId(ClnEnv.getLoginId());
				apptCra.setICustRec(ClnEnv.getQualSvee().getCustRec().copyMe());
				apptCra.setISubnRec(callback_main.getSubnRec());
				if (lnttCra != null) {
					apptCra.setIEnqSRTy(lnttCra.getOLtrsRec().srStatus);
				}
			}
			APIsManager.doEnquireSR4NoPend(me, apptCra);
			// Pass data back Activity
			callback_main.setApptCra(apptCra);
		}
	}

	private final void doLineTestRequest() {
		if (debug) Log.i(TAG, "Do LTT Ag");
		// Avoid User start Line Test more times
		// If the LineTest started before, just show the InProgressDialog Only.
		LnttAgent lnttAgent = Utils.getPrefLnttAgent(me.getActivity());

		if ("".equalsIgnoreCase(lnttAgent.getLnttSrvNum())) {
			if (debug) Log.i(TAG, "Do LTT Ag1");
			// Prepare LineTest related variables
			lnttAgent = new LnttAgent();
			lnttAgent.setSessTok(ClnEnv.getSessTok());
			lnttAgent.prepareLnttCra(ClnEnv.getAssocSubnRecAry(ClnEnv.getQualSvee().getSubnRecAry()), callback_main.getSubnRec().copyMe(), ClnEnv.getSessionLoginID());
			// Only Reset and Retest, lnttCra is not null and passed Object
			if (lnttCra != null) {
				lnttAgent.getLnttCra().setOLtrsRec(lnttCra.getOLtrsRec().copyMe());
			}
			// Also Store SR Result
			lnttAgent.setApptCra(apptCra);

			Bundle lnttBundle = new Bundle();
			lnttBundle.putSerializable("LNTTAGENT", lnttAgent);
			// Directly start a Service
			isLnttStart = true;
			Intent intent = new Intent(me.getActivity(), LineTestIntentService.class);
			intent.putExtras(lnttBundle);
			getActivity().startService(intent);
		}
		// Redirect to ServiceList and show InProgess Dialog
		Intent dialogIntent = new Intent(me.getActivity(), GlobalDialog.class);
		if (lnttCra == null) {
			dialogIntent.putExtra("DIALOGTYPE", R.string.CONST_DIALOG_LNTT_ONSTART);
		} else {
			dialogIntent.putExtra("DIALOGTYPE", R.string.CONST_DIALOG_LNTT_ONRESET);
		}
		dialogIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
		startActivity(dialogIntent);
		getActivity().overridePendingTransition(R.anim.left_slide_in, R.anim.right_slide_out);
		getActivity().finish();
	}

	private final void doGetAvailableTimeSlot() {
		// Assume lnttAgent and apptCra defined in LineTest Result Page
		if (lnttAgent != null && apptCra != null) {
			//			// doGetAvailableTimeSlot
			ApptCra rApptCra = new ApptCra();
			rApptCra.setILoginId(ClnEnv.getSessionLoginID());
			rApptCra.setICustRec(ClnEnv.getQualSvee().getCustRec().copyMe());
			rApptCra.setISRApptInfo(apptCra.getOPendSrvReq().getApptInfo());
			APIsManager.doGetAvailableTimeSlot(me, rApptCra);
		} 
	}

	private final void doneEnquireSR() {
		if (apptCra != null) {
			if (apptCra.getReply().getCode().equals(RC.USRB_PENDSR_FND)) {
				me.displayDialog(getResString(R.string.MYHKT_LT_ERR_SR_FOUND));
			}
		}
	}

	protected final void refreshData() {
		super.refreshData();
		if (callback_main != null) {
			// Screen Tracker

			if (callback_main.getActiveSubview() == R.string.CONST_SELECTEDVIEW_LINETEST) {
				lnttAgent = callback_main.getLnttAgent();
				lnttCra = lnttAgent.getLnttCra();
				apptCra = lnttAgent.getApptCra();

				String lnttImsSrvNum = lnttAgent.getLnttImsSrvNum();
				String lnttTvSrvNum = lnttAgent.getLnttTvSrvNum();
				String lnttLtsSrvNum = lnttAgent.getLnttLtsSrvNum();
				String endTimestamp = Utils.toDateString(lnttCra.getServerTS(), Utils.getString(me.getActivity(), R.string.input_datetime_format), "dd/MM/yyyy HH:mm:ss");

				// Clear All Data if line Test Result read once
				Utils.clearLnttService(me.getActivity());

				String rStr = "";
				if (lnttCra != null) {
					Boolean isFixedLine = false;
					//check if LTS is fixed line
					if (lnttCra.getOLtrsRec().txtg4fxln.trim().length() > 0 && !(lnttCra.getOLtrsRec().txtg4bb.trim().length() > 0) && callback_main.getLob() == R.string.CONST_LOB_LTS) isFixedLine = true;
//					if (!lnttCra.isIBBNwInd() && lnttCra.isIFixLnInd() && !(lnttCra.getOLtrsRec().txtg4bb.trim().length() > 0) && callback_main.getLob() == R.string.CONST_LOB_LTS) isFixedLine = true;
					//Show Modem Image if available
					if (!isFixedLine || callback_main.getLob() == R.string.CONST_LOB_PCD || callback_main.getLob() == R.string.CONST_LOB_TV) {
						//if modem model number exist, show modem img and text, otherwise show default url image only
						if (lnttCra.getOLtrsRec().mdmMdl.trim().length() > 0) {
							if (lnttCra.getOLtrsRec().mdmimgL.trim().length() > 0) modemImageLargeURL = lnttCra.getOLtrsRec().mdmimgL.trim();
							//show modem message if available
							if(lnttCra.getOMdmEnMsg() != null && lnttCra.getOMdmZhMsg() != null) {
								if (lnttCra.getOMdmEnMsg().trim().length() > 0 && lnttCra.getOMdmZhMsg().trim().length() > 0 && !isFixedLine) {
									String msg = isZh ? lnttCra.getOMdmZhMsg() : lnttCra.getOMdmEnMsg();
									msg = msg.trim().replace("~", "\n");
									mdmMsgCell = new BigTextCell("", msg);
								} else {
									mdmMsgCell = null;
								}
							}
						} else {
//							modemImageLargeURL =  getResString(R.string.MYHKT_URL_MDMIMG);
							modemImageLargeURL = ClnEnv.getPref(getActivity(), getString(R.string.CONST_PREF_FTP), APIsManager.PRD_FTP) + "/mba/images/default_large.png";
						}
						if (modemImageLargeURL.trim().length() > 0) {
							String[] click = {"onImageClick"};
							//						imageViewCell = new ImageViewCell(0,"http://interfacelift.com/wallpaper/previews/03567_risingfromorange@2x.jpg" , click);
							imageViewCell = new ImageViewCell(0,modemImageLargeURL , click);
							imageViewCell.setOnClickListener(new OnClickListener(){

								@Override
								public void onClick(View v) {
									Intent intent = new Intent(me.getActivity(), ImageViewerActivity.class);
									intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
									Bundle bundle = new Bundle();
									bundle.putString("NAVTEXT" , callback_main.getNavTitle());
									bundle.putInt("LOBICON", callback_main.getLob());			
									bundle.putString("IMAGEURL", modemImageLargeURL);
									bundle.putInt("LOBTYPE", callback_main.getLtsType());	
									intent.putExtras(bundle);
									startActivity(intent);
									me.getActivity().overridePendingTransition(R.anim.right_slide_in, R.anim.left_slide_out);

								}

							});
							//						ImageViewCell imageViewCell = new ImageViewCell(0,"http://interfacelift.com/wallpaper/previews/03567_risingfromorange@2x.jpg" , click);
							//						cellList.add(imageViewCell);
							//Set tag
							//TODO
							//					viewholder.linetestpcd3_modem/*_divider.setVisibility(View.VISIBLE);
							//					viewholder.linetestpcd3_result_*/img.setTag(modemImageLargeURL);
							//Download pic
							//					new DownloadImagesTask().execute(viewholder.linetestpcd3_result_img);
						}
					}

					// show the LineTest return timestamp
					timeCell.setContent(endTimestamp);

					if (lnttCra.isIBBNwInd()) {
						int resultImgId;
						String connectState="";
						// Require Graphical Result View
						// Display Modem Result
						if (lnttCra.getOLtrsRec().modem.equals(LtrsRec.RLT_GOOD)) {
							// Show result image : IMG_MANG
							resultImgId = R.drawable.icon_man_green;
							connectState = getResString(R.string.myhkt_linetest_RLT_GOOD);
						} else if (lnttCra.getOLtrsRec().modem.equals(LtrsRec.RLT_OUT_SYNC)) {
							// Show result image : IMG_MANR;
							resultImgId = R.drawable.icon_man_red;
							connectState = getResString(R.string.myhkt_linetest_RLT_OUT_SYNC);
						} else {
							// Show result image : IMG_MANY;
							resultImgId = R.drawable.icon_man_yellow;
							connectState = getResString(R.string.myhkt_linetest_RLT_NOT_GOOD);
						}

						// Display Result for associated subscription (Connected Services)
						if (lnttCra.getOLtrsRec().subnPcdInd.equals(Tool.FLG_YES)) {
							// Display for PCD
							//lob_pcd_plain
							pcdLiveTestCell = new LineTest(lnttImsSrvNum ,connectState ,R.drawable.lob_pcd_plain, resultImgId);
						} else {
							pcdLiveTestCell = null;
						}

						if (lnttCra.getOLtrsRec().subnTvInd.equals(Tool.FLG_YES)) {
							// Display for TV
							//lob_tv_plain
							tvLiveTestCell = new LineTest(lnttTvSrvNum ,connectState ,R.drawable.lob_tv_plain, resultImgId);
						} else {
							tvLiveTestCell = null;
						}

						if (lnttCra.getOLtrsRec().subnEyeInd.equals(Tool.FLG_YES)) {
							// Display for Eye
							//lob_eye_plain
							eyeLiveTestCell = new LineTest(lnttLtsSrvNum ,connectState ,R.drawable.lob_lts_plain1, resultImgId);
						} else {
							eyeLiveTestCell = null;
						}

						//show bb message if available
						if (lnttCra.getOLtrsRec().txtg4bb.trim().length() > 0 && !isFixedLine) {
							bbMsgCell = new BigTextCell("", InterpretRCManager.interpretRC_LttLtsMdu(this.getActivity(), lnttCra.getOLtrsRec().txtg4bb.trim()));					
						} else {
							bbMsgCell = null;
						}

						//show fixedline message if available
						if (lnttCra.getOLtrsRec().txtg4fxln.trim().length() > 0 && callback_main.getLob() == R.string.CONST_LOB_LTS) {
							fixlineMsgCell  = new BigTextCell("", InterpretRCManager.interpretRC_LttLtsMdu(this.getActivity(), lnttCra.getOLtrsRec().txtg4fxln.trim()));
						} else {
							fixlineMsgCell = null;
						}

						if (apptCra != null) {
							// SR found , Show additional message
							if (apptCra.getReply().getCode().equals(RC.USRB_PENDSR_FND)) {
								if (lnttCra.getOLtrsRec().modem.equals(LtrsRec.RLT_GOOD)) {
									rStr = rStr + getResString(R.string.LTTM_PSR);
								} else {
									rStr = rStr + getResString(R.string.LTTM_PSR_NG);
								}
								hktBtn.setText(getResString(R.string.MYHKT_LT_BTN_DETAIL));
							} else {
								// (SR Not found && lnttRec Not Good)
								// [Assume: Good status never shows "T" / "V" / "B"]
								String srStauts = lnttCra.getOLtrsRec().srStatus;
								if (apptCra.getReply().getCode().equals(RC.SUCC) && ("T".equalsIgnoreCase(srStauts) || "V".equalsIgnoreCase(srStauts) || "B".equalsIgnoreCase(srStauts))) {
									if ("T".equalsIgnoreCase(srStauts)) {
										rStr = rStr + getResString(R.string.LTTM_PORT_RESET);
										hktBtn.setText(getResString(R.string.MYHKT_LT_BTN_RESET));
									} else if ("V".equalsIgnoreCase(srStauts) || "B".equalsIgnoreCase(srStauts)) {
										rStr = rStr + getResString(R.string.LTTM_AUTOSR);
										hktBtn.setText(getResString(R.string.MYHKT_LT_BTN_REPORT));
									}
								} else {
									hktBtn.setText(getResString(R.string.myhkt_btn_testagain));
								}
							}
						}

						//show SR result if available
						//	rStr = "SR result wow";
						if (rStr.trim().length() > 0) {
							resultCell = new BigTextCell("", rStr);				
						} else {
							resultCell = null;
						}				
					} else {
						// Text Result Only – should use LTS result screen style
						//TODO
						//Image inv
						//				private BigTextCell mdmMsgCell;
						//				private BigTextCell timeCell;
						bbMsgCell = null;
						fixlineMsgCell = null;
						resultCell = null;
						pcdLiveTestCell = null;
						tvLiveTestCell = null;
						eyeLiveTestCell = null;
						fixlineTitleCell = new BigTextCell("", getResString(R.string.MYHKT_LT_LBL_FIXLN_HEADER));
						fixlineMsgCell = new BigTextCell("", InterpretRCManager.interpretRC_LttLtsMdu(this.getActivity(), lnttCra.getOLtrsRec().txtg4fxln.trim()));
						if (apptCra != null) {
							// SR found , Show additional message
							if (apptCra.getReply().getCode().equals(RC.USRB_PENDSR_FND)) {
								rStr = rStr + getResString(R.string.LTTM_PSR);
								hktBtn.setText(getResString(R.string.MYHKT_LT_BTN_DETAIL));
							} else {
								// SR Not found && lnttRec Not Good
								// [Assume: Good status never shows "V" / "B"]
								String srStatus = lnttCra.getOLtrsRec().srStatus;
								if (apptCra.getReply().getCode().equals(RC.SUCC) && ("V".equalsIgnoreCase(srStatus) || "B".equalsIgnoreCase(srStatus))) {
									rStr = rStr + getResString(R.string.LTTM_AUTOSR);
									hktBtn.setText(getResString(R.string.MYHKT_LT_BTN_REPORT));
								} else {
									hktBtn.setText(getResString(R.string.myhkt_btn_testagain));
								}
							}
						}

						//show fixedline message if available
						if (lnttCra.getOLtrsRec().txtg4fxln.trim().length() > 0 && callback_main.getLob() == R.string.CONST_LOB_LTS) {
							fixlineMsgCell = new BigTextCell("",InterpretRCManager.interpretRC_LttLtsMdu(this.getActivity() ,lnttCra.getOLtrsRec().txtg4fxln.trim()));
						}

						//show SR result if available
						//				rStr = "SR result wow";
						if (rStr.trim().length() > 0) {
							resultCell = new BigTextCell("", rStr);
						}
					}
					callback_main.setApptCra(apptCra);
					//					callback_main.setLnttAgent(Utils.getPrefLnttAgent(this.getActivity()));
					callback_main.setLnttCra(lnttCra);

				}
				cellList = new ArrayList<Cell>();
				titleCell.setLeftPadding(basePadding);
				titleCell.setRightPadding(basePadding);
				cellList.add(titleCell);
				if (timeCell !=null) {timeCell.setLeftMargin(basePadding);timeCell.setRightMargin(basePadding);cellList.add(timeCell);}
				if (pcdLiveTestCell !=null) {pcdLiveTestCell.setLeftPadding(basePadding);pcdLiveTestCell.setRightPadding(basePadding);cellList.add(pcdLiveTestCell);}
				if (tvLiveTestCell !=null) {tvLiveTestCell.setLeftPadding(basePadding);tvLiveTestCell.setRightPadding(basePadding);cellList.add(tvLiveTestCell);}
				if (eyeLiveTestCell !=null) {eyeLiveTestCell.setLeftPadding(basePadding);eyeLiveTestCell.setRightPadding(basePadding);cellList.add(eyeLiveTestCell);}
				if (imageViewCell !=null) cellList.add(imageViewCell);				
				if (bbMsgCell !=null) {bbMsgCell.setLeftMargin(basePadding);bbMsgCell.setRightMargin(basePadding); bbMsgCell.setContentSizeDelta(-3);cellList.add(bbMsgCell);}
				if (mdmMsgCell !=null) {mdmMsgCell.setLeftMargin(basePadding);mdmMsgCell.setRightMargin(basePadding);mdmMsgCell.setContentSizeDelta(-3);cellList.add(mdmMsgCell);}
				if (fixlineTitleCell !=null) {fixlineTitleCell.setMargin(basePadding);fixlineTitleCell.setContentSizeDelta(-3);cellList.add(fixlineTitleCell);}
				if (fixlineMsgCell !=null) {fixlineMsgCell.setMargin(basePadding);fixlineMsgCell.setContentSizeDelta(-3);cellList.add(fixlineMsgCell);}
				if (resultCell !=null) {resultCell.setLeftMargin(basePadding);resultCell.setRightMargin(basePadding);resultCell.setContentSizeDelta(-3);cellList.add(resultCell);}
//				if (lnttCra!=null && !lnttCra.isIBBNwInd()) {				
//					cellList.add(enquiryCell);
//					cellList.add(singleBtnCell);				
//				}
				cellViewAdapter.setView(frame, cellList);
			}
		}
	}

	@Override
	public void onStart() {
		super.onStart();
	}

	@Override
	public void onStop() {
		if (debug) Log.i(TAG, "on stop");
		super.onStop();
	}

	@Override
	public void onPause() {
		if (debug) Log.i(TAG, "on pause");
		super.onPause();
	}
	@Override
	public void onDestroy() {
		if (debug) Log.i(TAG, "on Destroy");
		super.onDestroy();
		if (!isLnttStart) { 
			// Clear All Data if line Test Result read once
			Utils.clearLnttService(me.getActivity());
		}
	}
	@Override
	public void onSuccess(APIsResponse response) {
		if (response != null) {
			ApptCra mApptCra ;
			if (response.getActionTy().equals(APIsManager.SR_ENQ_NP)) {
				mApptCra = (ApptCra) response.getCra();
				if (mApptCra != null) {
					apptCra.setReply(mApptCra.getReply());
					apptCra.setServerTS(mApptCra.getServerTS());		
					me.apptCra.setOPendSrvReq(mApptCra.getOPendSrvReq());
					// Pass data back Activity
					me.callback_main.setApptCra(me.apptCra.copyMe());
					// call doGetTimeSlot
					me.doGetAvailableTimeSlot();
				}
			} else if (response.getActionTy().equals(APIsManager.SR_AVA_TS)) {
				mApptCra = (ApptCra) response.getCra();
				if (mApptCra != null) {
					apptCra.setOSRApptInfo(mApptCra.getOSRApptInfo());
					// Pass data back Activity
					me.callback_main.setApptCra(me.apptCra.copyMe());
					//TODO go to SR create
					callback_lntt = (OnLnttListener)getParentFragment();
					callback_lntt.setActiveChildview(R.string.CONST_SELECTEDVIEW_SRCREATION);
					callback_lntt.displayChildview(OnLnttListener.ENTER);
				}
			}
		}
	}

	@Override
	public void onFail(APIsResponse response) {
		if (response != null) {
			// General Error Message
			if (!"".equals(response.getMessage()) && response.getMessage() != null) {
				displayDialog(response.getMessage());
			} else if (RC.TCSESS_MM.equalsIgnoreCase(response.getReply().getCode())) {
				BaseActivity.ivSessDialog();
			} else {
				ApptCra mApptCra ;
				if (response.getActionTy().equals(APIsManager.SR_ENQ_NP)) {
					mApptCra = (ApptCra) response.getCra();
					if (mApptCra != null) {
						apptCra.setReply(mApptCra.getReply());
						apptCra.setServerTS(mApptCra.getServerTS());
						// Pass Data Back Activity
						callback_main.setApptCra(apptCra.copyMe());
						if (mApptCra.getReply().getCode().equals(RC.GWTGUD_MM) || mApptCra.getReply().getCode().equals(RC.ALT)) {
							// RC_IVSESS
							// RC_ALT
							//TODO
							//							me.redirectDialog(ClnEnv.getRPCErrMsg(me.getActivity(), RC.GWTGUD_MM));
						} else if (mApptCra.getReply().getCode().equals(RC.USRB_PENDSR_FND)) {
							// RC_USRB_PENDSR_FND
							// Have Pending SR
							me.doneEnquireSR();
						} else {
							me.displayDialog(ClnEnv.getRPCErrMsg(me.getActivity().getApplicationContext(), mApptCra.getReply().getCode()));
						}
					}
				} else if (response.getActionTy().equals(APIsManager.SR_AVA_TS)) {
					mApptCra = (ApptCra) response.getCra();
					if (mApptCra != null) {
						if (mApptCra.getReply().getCode().equals(RC.GWTGUD_MM) || mApptCra.getReply().getCode().equals(RC.ALT)) {
							//							// RC_IVSESS
							//							// RC_ALT
							//TODO
							//							me.redirectDialog(ClnEnv.getRPCErrMsg(me.getActivity(), RC.GWTGUD_MM));
							me.displayDialog(ClnEnv.getRPCErrMsg(me.getActivity().getApplicationContext(), mApptCra.getReply().getCode()));
						} else {
							me.displayDialog(ClnEnv.getRPCErrMsg(me.getActivity().getApplicationContext(), mApptCra.getReply().getCode()));
						}
					}
				}

			}
		}

	}
}
