package com.pccw.myhkt.activity;

import java.util.ArrayList;
import java.util.List;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.Point;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.TypedValue;
import android.view.Display;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.LinearLayout;
import android.widget.ScrollView;

import com.pccw.myhkt.ClnEnv;
import com.pccw.myhkt.DialogHelper;
import com.pccw.myhkt.FAWrapper;
import com.pccw.myhkt.R;
import com.pccw.myhkt.Utils;
import com.pccw.myhkt.cell.model.BigTextCell;
import com.pccw.myhkt.cell.model.Cell;
import com.pccw.myhkt.cell.model.SmallTextCell;
import com.pccw.myhkt.cell.model.TextBtnCell;
import com.pccw.myhkt.cell.model.TextImageBtnCell;
import com.pccw.myhkt.cell.model.TwoTextBtnCell;
import com.pccw.myhkt.cell.view.CellViewAdapter;
import com.pccw.myhkt.lib.ui.AAQuery;
import com.pccw.myhkt.lib.ui.LRTabButton;
import com.pccw.myhkt.model.HomeButtonItem.MAINMENU;
import com.pccw.myhkt.util.Constant;
import com.pccw.myhkt.util.RuntimePermissionUtil;

public class ContactUsActivity extends BaseActivity {
    // Common Components
    private boolean debug = false;
    private String TAG = this.getClass().getName();

    //UI
    private AAQuery aq;
    private LRTabButton lrButton;

    private List<Cell> cellList;

    /*
     * showType Definition
     * 0 : Consumer
     * 1 : HKT Premier
     * 2 : Business
     */

    private int showType = 0;
    private int svPos = 0;

    private int deviceWidth;
    private int extralinespace;
    private int colWidth;
    private int sColWidth;

    //List Data
    List<String> listCol1Title;
    List<Integer> listCol2ButtonResId;
    List<String> listCol2Content;
    List<String> listCol2SubTitle;

    CellViewAdapter cellViewAdapter;
    LinearLayout frame;

    //Property of text
    private float textSize;
    private int textColor;
    private int textColorDisabled;

    private Drawable callIcon;
    private Drawable emailIcon;

    private String phoneNum;

    @Override
    public final void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        debug = getResources().getBoolean(R.bool.DEBUG);

        isLiveChatShown = false;

        ClnEnv.updateUILocale(getBaseContext(), ClnEnv.getAppLocale(getBaseContext()));
        isZh = !ClnEnv.getAppLocale(this).equals(Utils.getString(this, R.string.CONST_LOCALE_EN));

        setContentView(R.layout.activity_contactus);
        initData();
        if (savedInstanceState != null) {
            showType = savedInstanceState.getInt("showType");
            svPos = savedInstanceState.getInt("svPos");
        }
        // Screen Tracker
        FAWrapper.getInstance().sendFAScreen(me, R.string.CONST_SCRN_HELP, false);
    }

    public void onStart() {
        super.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();
        moduleId = Utils.isUAT(this) ?
                getResources().getString(R.string.MODULE_CONTACT_US_UAT) :
                getResources().getString(R.string.MODULE_CONTACT_US_PRD);
        // Screen Tracker
        FAWrapper.getInstance().sendFAScreen(me, R.string.CONST_SCRN_HELP, false);
    }

    // Android Device Back Button Handling
    @Override
    public final void onBackPressed() {
        //			clearNotificationData();
        finish();
        overridePendingTransition(R.anim.left_slide_in, R.anim.right_slide_out);
    }

    private void initData() {
        me = this;

        Display display = me.getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        deviceWidth = size.x;
        extralinespace = (int) getResources().getDimension(R.dimen.extralinespace);
        basePadding = (int) getResources().getDimension(R.dimen.basePadding);
        textSize = (float) getResources().getDimension(R.dimen.bodytextsize);
        textColor = R.color.hkt_txtcolor_grey;
        textColorDisabled = R.color.hkt_txtcolor_grey_disable;
        colWidth = (deviceWidth - basePadding * 2) / 4;
        sColWidth = (deviceWidth - basePadding * 2) * 2 / 9;
        showType = 0;
        cellViewAdapter = new CellViewAdapter(me);

        int bh = (int) getResources().getDimension(R.dimen.buttonblue_height);
        int padding_twocol = (int) getResources().getDimension(R.dimen.padding_twocol);
        ColorFilter filter = new PorterDuffColorFilter(Color.parseColor("#187DC3"), PorterDuff.Mode.SRC_IN);

        callIcon = getResources().getDrawable(R.drawable.phone_small);
        int ch = callIcon.getIntrinsicHeight();
        int cw = callIcon.getIntrinsicWidth();
        callIcon.setColorFilter(filter);
        int imageCH = bh - padding_twocol * 3;
        int imageCW = cw * (imageCH) / ch;
        callIcon.setBounds(0, 0, imageCW, imageCH);

        emailIcon = getResources().getDrawable(R.drawable.btn_mail);
        int mh = emailIcon.getIntrinsicHeight();
        int mw = emailIcon.getIntrinsicWidth();
        emailIcon.setColorFilter(filter);
        int imageMH = bh / 3;
        int imageMW = mw * (imageMH) / mh;
        emailIcon.setBounds(0, 0, imageMW, imageMH);
    }

    @Override
    final protected void initUI2() {
        aq = new AAQuery(this);
        frame = (LinearLayout) aq.id(R.id.cu_frame2).getView();

        //navbar
        aq.navBarBaseLayout(R.id.navbar_base_layout);
        aq.navBarTitle(R.id.navbar_title, getResources().getString(R.string.myhkt_cu_title));
        aq.navBarButton(R.id.navbar_button_left, R.drawable.btn_back);

        aq.id(R.id.navbar_button_left).clicked(this, "onClick");

        //Initialize the container
        aq.id(R.id.cu_frame).backgroundColorId(R.color.white);
        aq.marginpx(R.id.cu_frame, basePadding, 0, basePadding, 0);
        aq.id(R.id.cu_frame).getView().setPadding(0, basePadding, 0, 0);

        //Top tabbar
        aq.marginpx(R.id.cu_buttonsframe, 0, 0, 0, extralinespace);
        int[] buttonResIds = new int[]{R.id.cu_button0, R.id.cu_button1, R.id.cu_button2};
        int[] stringsResIds = new int[]{R.string.support_consumer, R.string.SUPPORT_PREMIER, R.string.support_biz};
        for (int j = 0; j < 3; j++) {
            aq.id(buttonResIds[j]).height((int) getResources().getDimension(R.dimen.button_height), false).textColorId(textColor).getTextView().setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize);
            aq.id(buttonResIds[j]).text(stringsResIds[j]);
            aq.id(buttonResIds[j]).clicked(this, "onClick");
        }

        if (ClnEnv.isLoggedIn()) {
            if (ClnEnv.getSessionPremierFlag()) {
                // Login and Premier
                showType = 1;
                aq.id(R.id.cu_button0).visibility(View.GONE);

                //Screen Tracker
                FAWrapper.getInstance().sendFAScreen(me, R.string.CONST_SCRN_HELPHKTPREMIER, false);
            } else {
                // Login and Non Premier
                showType = 0;
                aq.id(R.id.cu_button1).visibility(View.GONE);

                //Screen Tracker
                FAWrapper.getInstance().sendFAScreen(me, R.string.CONST_SCRN_HELPCONSUMER, false);
            }
        }

        //Show the list
        prepareListData();
        refreshView();
    }

    private void refreshView() {
        setBtns();
        aq.id(R.id.cu_sv).getView().scrollTo(0, 0);
        cellList = new ArrayList<Cell>();

        for (int i = 0; i < listCol1Title.size(); i++) {
            if (debug) Utils.showLog(TAG, "Sub" + listCol2SubTitle.get(i) + "Title");
            if (listCol2Content.get(i).equals("")) {
                //Add facebook Button
                if (listCol2ButtonResId.get(i) == R.drawable.ic_facebook) {
                    OnClickListener fbImageClicked = new OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            String fburl = "https://www.facebook.com/pccwcs";
                            Intent fbintent = new Intent(Intent.ACTION_VIEW);
                            fbintent.setData(Uri.parse(fburl));
                            startActivity(fbintent);
                        }
                    };

                    TextImageBtnCell fbImageCell = new TextImageBtnCell(fbImageClicked, listCol1Title.get(i), (int) listCol2ButtonResId.get(i));
                    fbImageCell.setTopMargin(0);
                    fbImageCell.setLeftPadding(basePadding);
                    fbImageCell.setRightPadding(basePadding);
                    fbImageCell.setBotMargin(0);
                    cellList.add(fbImageCell);
                } else {
                    BigTextCell bigTextCell = new BigTextCell(listCol1Title.get(i), "");
                    bigTextCell.setTopMargin(0);
                    bigTextCell.setBotMargin(0);
                    bigTextCell.setLeftMargin(basePadding);
                    bigTextCell.setRightMargin(basePadding);
                    if (!listCol2SubTitle.get(i).equals("")) {
                        bigTextCell.setTitleColorId(R.color.hkt_txtcolor_grey);
                        bigTextCell.setTitleSizeDelta(0);
                    } else {
                        bigTextCell.setTitleTypeface(Typeface.BOLD);
                    }
                    cellList.add(bigTextCell);

                }

            } else {
                OnClickListener detailClicked = null;
                final int pos = i;
                String btnTextName = "";
                String title = listCol1Title.get(i);
                String content = listCol2Content.get(i);
                String subTitle = listCol2SubTitle.get(i);
                Integer btnId = listCol2ButtonResId.get(i);


                if (!title.equals("") && !title.equals(getResString(R.string.CONST_TEXTBTNCELL_DARK_CONTENT))) {
                    SmallTextCell smallTextCell = new SmallTextCell(title, "");
                    smallTextCell.setLeftPadding(basePadding);
                    smallTextCell.setRightPadding(basePadding);
                    cellList.add(smallTextCell);
                }

                if (btnId == R.drawable.btn_mail) {
                    btnTextName = getResString(R.string.btn_send);
                    detailClicked = new OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent intent = new Intent(Intent.ACTION_SENDTO);
                            intent.setData(Uri.fromParts(
                                    "mailto", listCol2Content.get(pos), null)); // only email apps should handle this
                            if (intent.resolveActivity(me.getPackageManager()) != null) {
                                //to app directly
                                startActivity(intent);
                            } else {
                                DialogHelper.createSimpleDialog(me, "No app can perform this action");
                            }
                        }
                    };
                } else if (btnId == R.drawable.livechat_small) {
                    btnTextName = getResString(R.string.MYHKT_BTN_LIVECHAT);
                    if (content.contains(getString(R.string.support_liveChat_description_1010))) {
                        detailClicked = new OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                if (!ClnEnv.isLoggedIn()) {
                                    loginFirstDialog(MAINMENU.CONTACTUS);
                                } else {
                                    openLiveChat(getString(R.string.MODULE_101_BILL));
                                }
                            }
                        };

                    } else if (content.contains(getString(R.string.support_liveChat_description_csl))) {
                        detailClicked = new OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                if (!ClnEnv.isLoggedIn()) {
                                    loginFirstDialog(MAINMENU.CONTACTUS);
                                } else {
                                    openLiveChat(getString(R.string.MODULE_MOB_BILL));
                                }
                            }
                        };
                    } else {
                        detailClicked = new OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                if (!ClnEnv.isLoggedIn()) {
                                    loginFirstDialog(MAINMENU.CONTACTUS);
                                } else {
                                    openLiveChat();
                                }
                            }
                        };
                    }

                }else if (btnId == R.drawable.phone_small){
                    btnTextName = getResString(R.string.btn_call_now);
                    detailClicked = new OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            callPhone(listCol2Content.get(pos));
                        }
                    };
                }
                if (subTitle.equals("")) {
                    TextBtnCell textBtnCell = new TextBtnCell(detailClicked, content, btnTextName, (int) listCol2ButtonResId.get(i));
                    if (content.equals(getResString(R.string.support_fixed_line_service_email_info))
                            || content.equals(getResString(R.string.support_consumer_email_info))
                            || content.equals(getResString(R.string.support_biz_email_info))
                            || content.equals(getResString(R.string.SUPPORT_PREMIER_EMAIL_CS))) {
                        textBtnCell.setTitleSizeDelta(-2);
                    }
                    else{
                        textBtnCell.setTitleSizeDelta(-1);
                    }

                    if (title.equals(getResString(R.string.CONST_TEXTBTNCELL_DARK_CONTENT))){
                        textBtnCell.setTitleColorId(R.color.hkt_txtcolor_grey);
                    } else {
                        textBtnCell.setTitleColorId(R.color.hkt_textcolor);
                    }
                    textBtnCell.setTitleGravity(Gravity.TOP | Gravity.LEFT);
                    textBtnCell.setTitleHeight(ViewGroup.LayoutParams.WRAP_CONTENT);
                    textBtnCell.setBtnWidth((int) getResources().getDimension(R.dimen.buttonblue_width));
                    if ((int) listCol2ButtonResId.get(i) == R.drawable.phone_small) {
                        textBtnCell.setDrawable(callIcon);
                    } else if ((int) listCol2ButtonResId.get(i) == R.drawable.btn_mail) {
                        textBtnCell.setDrawable(emailIcon);
                    }
                    cellList.add(textBtnCell);
                } else {
                    TwoTextBtnCell textBtnCell = new TwoTextBtnCell(detailClicked, subTitle, content, btnTextName, (int) listCol2ButtonResId.get(i));
                    textBtnCell.setContentSizeDelta(-2);
                    textBtnCell.setTitleSizeDelta(0);
                    textBtnCell.setContentColorId(R.color.hkt_textcolor);
                    textBtnCell.setTopMargin(extralinespace);
                    textBtnCell.setBotMargin(extralinespace);
                    textBtnCell.setBtnWidth((int) getResources().getDimension(R.dimen.buttonblue_width));
                    if ((int) listCol2ButtonResId.get(i) == R.drawable.phone_small) {
                        textBtnCell.setDrawable(callIcon);
                    } else if ((int) listCol2ButtonResId.get(i) == R.drawable.btn_mail) {
                        textBtnCell.setDrawable(emailIcon);
                    }

                    cellList.add(textBtnCell);
                }
            }

            if (i != listCol1Title.size() - 1 && listCol2ButtonResId.get(i + 1) == -1 && listCol2SubTitle.get(i + 1).equals("") && !listCol1Title.get(i + 1).equals("") ) {
                Cell cellLine = new Cell(Cell.LINE);
                cellLine.setTopPadding(extralinespace);
                cellLine.setLeftPadding(basePadding);
                cellLine.setRightPadding(basePadding);
                cellLine.setBottompadding(extralinespace);
                cellLine.setBotMargin(extralinespace);
                cellList.add(cellLine);
            }
        }
        cellViewAdapter.setView(frame, cellList, svPos);
    }

    /*
     * Preparing the list data
     */
    private void prepareListData() {
        switch (showType) {
            //Consumer
            case 0:
                listCol1Title = new ArrayList<String>();
                listCol2ButtonResId = new ArrayList<Integer>();
                listCol2Content = new ArrayList<String>();
                listCol2SubTitle = new ArrayList<String>();

                //Live Chat Group
                listCol1Title.add(getResString(R.string.TITLE_LIVE_CHAT));
                listCol2ButtonResId.add(-1);
                listCol2Content.add("");
                listCol2SubTitle.add("");

                listCol1Title.add(getResString(R.string.CONST_TEXTBTNCELL_DARK_CONTENT));
                listCol2ButtonResId.add(R.drawable.livechat_small);
                listCol2Content.add(getResString(R.string.SUB_TITLE_LIVE_CHAT) + "\n");
                listCol2SubTitle.add("");

                //Live Chat Group
                listCol1Title.add(getResString(R.string.CONST_TEXTBTNCELL_DARK_CONTENT));
                listCol2ButtonResId.add(R.drawable.livechat_small);
                listCol2Content.add(getResString(R.string.support_liveChat_description_1010) + "\n");
                listCol2SubTitle.add("");

                listCol1Title.add("");
                listCol2ButtonResId.add(-1);
                listCol2Content.add(" ");
                listCol2SubTitle.add("");

                listCol1Title.add(getResString(R.string.CONST_TEXTBTNCELL_DARK_CONTENT));
                listCol2ButtonResId.add(R.drawable.livechat_small);
                listCol2Content.add(getResString(R.string.support_liveChat_description_csl) + "\n");
                listCol2SubTitle.add("");


                //HotLines Group
                listCol1Title.add(getResString(R.string.support_hotlines));
                listCol2ButtonResId.add(-1);
                listCol2Content.add("");
                listCol2SubTitle.add("");

                listCol1Title.add(getResString(R.string.support_consumer_hotline_title));
                listCol2ButtonResId.add(R.drawable.phone_small);
                listCol2Content.add(getResString(R.string.support_consumer_hotline_no));
                listCol2SubTitle.add("");

                //Fixed Line Costumer
                listCol1Title.add(getResString(R.string.support_fixed_line_costumer_service_title));
                listCol2ButtonResId.add(R.drawable.phone_small);
                listCol2Content.add(getResString(R.string.support_fixed_line_costumer_service_hotline_info));
                listCol2SubTitle.add("");

                listCol1Title.add(getResString(R.string.support_1010_customer_hotline_title));
                listCol2ButtonResId.add(R.drawable.phone_small);
                listCol2Content.add(getResString(R.string.support_mobile_hotline_no_1010));
                listCol2SubTitle.add("");

                listCol1Title.add(getResString(R.string.support_csl_customer_hotline_title));
                listCol2ButtonResId.add(R.drawable.phone_small);
                listCol2Content.add(getResString(R.string.support_mobile_hotline_no));
                listCol2SubTitle.add("");

                listCol1Title.add(getResString(R.string.support_consumer_tv_hotline_title));
                listCol2ButtonResId.add(R.drawable.phone_small);
                listCol2Content.add(getResString(R.string.support_consumer_tv_hotline_no));
                listCol2SubTitle.add("");

                //Sales Hotline Group
                listCol1Title.add(getResString(R.string.support_sales_hotline_title));
                listCol2ButtonResId.add(-1);
                listCol2Content.add("");
                listCol2SubTitle.add("");

                listCol1Title.add(getResString(R.string.support_hotline));
                listCol2ButtonResId.add(R.drawable.phone_small);
                listCol2Content.add(getResString(R.string.support_sales_hotline_no));
                listCol2SubTitle.add("");

                //fixed line service sales
                listCol1Title.add(getResString(R.string.support_fixed_line_sales_service_title));
                listCol2ButtonResId.add(R.drawable.phone_small);
                listCol2Content.add(getResString(R.string.support_fixed_line_sales_service_hotline_info));
                listCol2SubTitle.add("");

                listCol1Title.add(getResString(R.string.myhkt_cu_1010Title) + " " + getResString(R.string.support_sales_hotline_title));
                listCol2ButtonResId.add(R.drawable.phone_small);
                listCol2Content.add(getResString(R.string.support_sales_1010_hotline_no));
                listCol2SubTitle.add("");

                listCol1Title.add(getResString(R.string.support_sales_csl_hotline_title));
                listCol2ButtonResId.add(R.drawable.phone_small);
                listCol2Content.add(getResString(R.string.support_sales_csl_hotline_no));
                listCol2SubTitle.add("");

                //Email Inq Group
                listCol1Title.add(getResString(R.string.support_email_enquiries));
                listCol2ButtonResId.add(-1);
                listCol2Content.add("");
                listCol2SubTitle.add("");

                //fixed line email
                listCol1Title.add(getResString(R.string.support_fixed_line_service_title));
                listCol2ButtonResId.add(R.drawable.btn_mail);
                listCol2Content.add(getResString(R.string.support_fixed_line_service_email_info));
                listCol2SubTitle.add("");


                listCol1Title.add(getResString(R.string.support_consumer_email));
                listCol2ButtonResId.add(R.drawable.btn_mail);
                listCol2Content.add(getResString(R.string.support_consumer_email_info));
                listCol2SubTitle.add("");

                //NETVIGATOR
                listCol1Title.add(getResString(R.string.myhkt_cu_pcdTitle));
                listCol2ButtonResId.add(-1);
                listCol2Content.add("");
                listCol2SubTitle.add(getResString(R.string.myhkt_cu_pcdTitle));

                listCol1Title.add("");
                listCol2ButtonResId.add(R.drawable.btn_mail);
                listCol2Content.add(getResString(R.string.myhkt_cu_pcdemail_cs));
                listCol2SubTitle.add(getResString(R.string.myhkt_cu_GI));

                listCol1Title.add("");
                listCol2ButtonResId.add(R.drawable.btn_mail);
                listCol2Content.add(getResString(R.string.myhkt_cu_pcdemail_ts));
                listCol2SubTitle.add(getResString(R.string.myhkt_cu_TS));

                //Now TV
                listCol1Title.add(getResString(R.string.myhkt_cu_tvTitle));
                listCol2ButtonResId.add(-1);
                listCol2Content.add("");
                listCol2SubTitle.add(getResString(R.string.myhkt_cu_tvTitle));

                listCol1Title.add("");
                listCol2ButtonResId.add(R.drawable.btn_mail);
                listCol2Content.add(getResString(R.string.myhkt_cu_tvemail_cs));
                listCol2SubTitle.add(getResString(R.string.myhkt_cu_GI));

                listCol1Title.add("");
                listCol2ButtonResId.add(R.drawable.btn_mail);
                listCol2Content.add(getResString(R.string.myhkt_cu_tvemail_ts));
                listCol2SubTitle.add(getResString(R.string.myhkt_cu_TS));

                //Other
                listCol1Title.add(getResString(R.string.support_consumer_others));
                listCol2ButtonResId.add(-1);
                listCol2Content.add("");
                listCol2SubTitle.add("");

                listCol1Title.add(getResString(R.string.support_consumer_others_fb) + "\n" + getResString(R.string.support_consumer_others_fbinfo));
                listCol2ButtonResId.add(R.drawable.ic_facebook);
                listCol2Content.add("");
                listCol2SubTitle.add("");

                break;
            //HKT Premier
            case 1:
                listCol1Title = new ArrayList<String>();
                listCol2ButtonResId = new ArrayList<Integer>();
                listCol2Content = new ArrayList<String>();
                listCol2SubTitle = new ArrayList<String>();

                //Live Chat Group
                listCol1Title.add(getResString(R.string.BTN_LIVE_CHAT));
                listCol2ButtonResId.add(-1);
                listCol2Content.add("");
                listCol2SubTitle.add("");

                listCol1Title.add(getResString(R.string.support_liveChat_description_prermier));
                listCol2ButtonResId.add(R.drawable.livechat_small);
                listCol2Content.add(getResString(R.string.support_liveChat_description_prermier_detail));
                listCol2SubTitle.add("");


                //HotLines Group
                listCol1Title.add(getResString(R.string.support_hotlines_premier));
                listCol2ButtonResId.add(-1);
                listCol2Content.add("");
                listCol2SubTitle.add("");

                listCol1Title.add(getResString(R.string.SUPPORT_PREMIER_HOTLINE_TITLE));
                listCol2ButtonResId.add(R.drawable.phone_small);
                listCol2Content.add(getResString(R.string.SUPPORT_PREMIER_HOTLINE_NO));
                listCol2SubTitle.add("");

                //Email Inq Group
                listCol1Title.add(getResString(R.string.support_email_enquiries));
                listCol2ButtonResId.add(-1);
                listCol2Content.add("");
                listCol2SubTitle.add("");

                listCol1Title.add(getResString(R.string.SUPPORT_PREMIER_HOTLINE_TITLE));
                listCol2ButtonResId.add(R.drawable.btn_mail);
                listCol2Content.add(getResString(R.string.SUPPORT_PREMIER_EMAIL_CS));
                listCol2SubTitle.add("");

                break;
            //Business
            case 2:
                listCol1Title = new ArrayList<String>();
                listCol2ButtonResId = new ArrayList<Integer>();
                listCol2Content = new ArrayList<String>();
                listCol2SubTitle = new ArrayList<String>();

                //HotLines Group
                listCol1Title.add(getResString(R.string.support_hotlines));
                listCol2ButtonResId.add(-1);
                listCol2Content.add("");
                listCol2SubTitle.add("");

                listCol1Title.add(getResString(R.string.support_biz_hotline_title));
                listCol2ButtonResId.add(R.drawable.phone_small);
                listCol2Content.add(getResString(R.string.support_biz_hotline_no));
                listCol2SubTitle.add("");

                //Email Inq Group
                listCol1Title.add(getResString(R.string.support_email_enquiries));
                listCol2ButtonResId.add(-1);
                listCol2Content.add("");
                listCol2SubTitle.add("");

                listCol1Title.add(getResString(R.string.support_biz_email));
                listCol2ButtonResId.add(R.drawable.btn_mail);
                listCol2Content.add(getResString(R.string.support_biz_email_info));
                listCol2SubTitle.add("");

                listCol1Title.add(getResString(R.string.support_biz_email_pcd));
                listCol2ButtonResId.add(-1);
                listCol2Content.add("");
                //Use for ind , will not be shown
                listCol2SubTitle.add(getResString(R.string.support_biz_email_pcd));


                listCol1Title.add("");
                listCol2ButtonResId.add(R.drawable.btn_mail);
                listCol2Content.add(getResString(R.string.myhkt_cu_bizpcdemail_sales));
                listCol2SubTitle.add(getResString(R.string.myhkt_cu_SI));

                listCol1Title.add("");
                listCol2ButtonResId.add(R.drawable.btn_mail);
                listCol2Content.add(getResString(R.string.myhkt_cu_bizpcdemail_cs));
                listCol2SubTitle.add(getResString(R.string.myhkt_cu_GI));

                listCol1Title.add("");
                listCol2ButtonResId.add(R.drawable.btn_mail);
                listCol2Content.add(getResString(R.string.myhkt_cu_bizpcdemail_ts));
                listCol2SubTitle.add(getResString(R.string.myhkt_cu_TS));
                break;
        }

    }


    public void onClick(View v) {
        //Screen Tracker

        switch (v.getId()) {
            case R.id.navbar_button_left:
                onBackPressed();
                break;
            case R.id.cu_button0:
                //tracker
                FAWrapper.getInstance().sendFAScreen(me, R.string.CONST_SCRN_HELPCONSUMER, false);

                Utils.closeSoftKeyboard(me, v);
                showType = 0;
                prepareListData();
                refreshView();
                break;
            case R.id.cu_button1:
                //tracker
                FAWrapper.getInstance().sendFAScreen(me, R.string.CONST_SCRN_HELPHKTPREMIER, false);

                Utils.closeSoftKeyboard(me, v);
                showType = 1;
                prepareListData();
                refreshView();
                break;
            case R.id.cu_button2:
                //tracker
                FAWrapper.getInstance().sendFAScreen(me, R.string.CONST_SCRN_HELPBUSINESS, false);

                Utils.closeSoftKeyboard(me, v);
                showType = 2;
                prepareListData();
                refreshView();
                break;
        }
    }

    private void setBtns() {
        aq.id(R.id.cu_button0).background(showType == 0 ? R.drawable.hkt_cubtnleft_bg_sel : R.drawable.hkt_cubtnleft_bg_not_sel);
        aq.id(R.id.cu_button0).textColorId(showType == 0 ? textColor : textColorDisabled);
        aq.id(R.id.cu_button1).background(showType == 1 ? R.drawable.hkt_cubtnmiddle_bg_sel : R.drawable.hkt_cubtnmiddle_bg_not_sel);
        aq.id(R.id.cu_button1).textColorId(showType == 1 ? textColor : textColorDisabled);
        aq.id(R.id.cu_button2).background(showType == 2 ? R.drawable.hkt_cubtnright_bg_sel : R.drawable.hkt_cubtnright_bg_not_sel);
        aq.id(R.id.cu_button2).textColorId(showType == 2 ? textColor : textColorDisabled);
    }

    // Saved data before Destroy Activity
    protected void onSaveInstanceState(Bundle outState) {

        super.onSaveInstanceState(outState);
        svPos = ((ScrollView) aq.id(R.id.cu_sv).getView()).getScrollY();
        outState.putInt("showType", showType);
        outState.putInt("svPos", svPos);
    }

    //MOIZ: 4/1/18 - Check if the permission granted,  before  dialing the phone number
    private void callPhone(String phoneNumber) {
        if (!TextUtils.isEmpty(phoneNumber)) {
            this.phoneNum = phoneNumber;
            if (RuntimePermissionUtil.isActionCallPermissionGranted(this)) {
                String num = phoneNum;
                if (num.contains("(")) {
                    num = num.substring(0, num.indexOf("(") - 1);
                }
                Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + num));
                startActivity(intent);

            } else {
                if (ClnEnv.getPref(this, Constant.CALL_PHONE_PERMISSION_DENIED, false)) {
                    displayDialog(this, getString(R.string.permission_denied_setting_enabled));
                } else {
                    RuntimePermissionUtil.requestCallPhonePermission(this);
                }
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == Constant.REQUEST_CALL_PHONE_PERMISSION) {
            //Permission is granted
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED && !TextUtils.isEmpty(phoneNum)) {
                String num = phoneNum;
                if (num.contains("(")) {
                    num = num.substring(0, num.indexOf("(") - 1);
                }
                Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + num));
                startActivity(intent);
            } else {

                displayDialog(this, getString(R.string.permission_denied));

                //This will return false if the user tick the Don't ask again checkbox
                if (!RuntimePermissionUtil.shouldShowCallPhoneRequestPermission(this)) {
                    ClnEnv.setPref(getApplicationContext(), Constant.CALL_PHONE_PERMISSION_DENIED, true);
                }
            }
        }
    }


}
