/*
    Class for Directory Inquiry Search Result Details
    
    Keywords
    --------
    $URL: $
    $Rev: $
    $Date: $
    $Author: $
*/


package com.pccw.dango.shared.entity;

import java.io.Serializable;

public class DQSrchDtl  implements Serializable
{
    private static final long serialVersionUID = -843035052263764089L;
    
    public String                   name;
    public String                   addr;
    public String                   cap1;
    public String                   cap2;
    public String                   cap3;
    public String                   cap4;   
    public String                   type;
    public String                   number;


    public static void main(String rArg[])
    {
        System.out.println(getVer());
        return;
    }
    
    
    public static String getVer()
    {
        return ("$URL: $, $Rev: $");
    }
    
    
    public DQSrchDtl()
    {
        initAndClear();
    }
    
    
    final void initAndClear()
    {
        init();
        clear();
    }
    
    
    protected void init()
    {
    }
    
    
    public void clear()
    {
        name    = "";
        addr    = "";
        cap1    = "";
        cap2    = "";
        cap3    = "";
        cap4    = "";
        type    = "";
        number  = "";
    }
    
    
    public DQSrchDtl copyFrom(DQSrchDtl rSrc)
    {
        name    = rSrc.name;
        addr    = rSrc.addr;
        cap1    = rSrc.cap1;
        cap2    = rSrc.cap2;
        cap3    = rSrc.cap3;
        cap4    = rSrc.cap4;
        type    = rSrc.type;
        number  = rSrc.number;
    
        return (this);
    }
    
    
    public DQSrchDtl copyTo(DQSrchDtl rDes)
    {
        rDes.copyFrom(this);
        return (rDes);
    }
    
    
    public DQSrchDtl copyMe()
    {
        DQSrchDtl             rDes;
        
        rDes = new DQSrchDtl();
        rDes.copyFrom(this);
        
        return (rDes);
    }
}
