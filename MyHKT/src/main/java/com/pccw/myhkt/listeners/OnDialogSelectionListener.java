package com.pccw.myhkt.listeners;

/**
 * Created by AMoiz Esmail on 13/02/2018.
 */

public interface OnDialogSelectionListener {
    void onPositiveClickListener(boolean isChecked);
    void onCancelClickListener();
}
