package com.pccw.myhkt.activity;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.Point;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.TypedValue;
import android.view.Display;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.pccw.myhkt.ClnEnv;
import com.pccw.myhkt.FAWrapper;
import com.pccw.myhkt.R;
import com.pccw.myhkt.Utils;
import com.pccw.myhkt.fragment.ContactUsBusinessFragment;
import com.pccw.myhkt.fragment.ContactUsConsumerFragment;
import com.pccw.myhkt.fragment.ContactUsPremierFragment;
import com.pccw.myhkt.lib.ui.AAQuery;
import com.pccw.myhkt.model.HomeButtonItem.MAINMENU;
import com.pccw.myhkt.util.Constant;
import com.pccw.myhkt.util.RuntimePermissionUtil;

import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ContactUsNewActivity extends BaseActivity {
    /*
     * showType Definition
     * 0 : Consumer
     * 1 : HKT Premier
     * 2 : Business
     */
    private int showType = 0;
    private int extraLineSpace;

    //Property of text
    private float textSize;

    private String phoneNum;

    @BindView(R.id.cu_button0)
    Button btnConsumer;
    @BindView(R.id.cu_button1)
    Button btnPremier;
    @BindView(R.id.cu_button2)
    Button btnBusiness;

    @Override
    public final void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        isLiveChatShown = false;

        ClnEnv.updateUILocale(getBaseContext(), ClnEnv.getAppLocale(getBaseContext()));
        isZh = !ClnEnv.getAppLocale(this).equals(Utils.getString(this, R.string.CONST_LOCALE_EN));

        setContentView(R.layout.activity_contactus_new);
        ButterKnife.bind(this);
        initData();
        // Screen Tracker
        FAWrapper.getInstance().sendFAScreen(me, R.string.CONST_SCRN_HELP, false);
    }

    @Override
    public void onResume() {
        super.onResume();
        moduleId = Utils.isUAT(this) ?
                getResources().getString(R.string.MODULE_CONTACT_US_UAT) :
                getResources().getString(R.string.MODULE_CONTACT_US_PRD);
        FAWrapper.getInstance().sendFAScreen(me, R.string.CONST_SCRN_HELP, false);
    }

    // Android Device Back Button Handling
    @Override
    public final void onBackPressed() {
        finish();
        overridePendingTransition(R.anim.left_slide_in, R.anim.right_slide_out);
    }

    private void initData() {
        me = this;
        Display display = me.getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int deviceWidth = size.x;
        extraLineSpace = (int) getResources().getDimension(R.dimen.extralinespace);
        basePadding = (int) getResources().getDimension(R.dimen.basePadding);
        textSize = getResources().getDimension(R.dimen.bodytextsize);
        showType = 0;

        ColorFilter filter = new PorterDuffColorFilter(Color.parseColor("#187DC3"), PorterDuff.Mode.SRC_IN);

        Drawable callIcon = ContextCompat.getDrawable(this, R.drawable.phone_small);
        Objects.requireNonNull(callIcon).setColorFilter(filter);

        Drawable emailIcon = ContextCompat.getDrawable(this, R.drawable.btn_mail);
        Objects.requireNonNull(emailIcon).setColorFilter(filter);
    }

    @Override
    final protected void initUI2() {
        AAQuery aq = new AAQuery(this);
        LinearLayout frame = (LinearLayout) aq.id(R.id.cu_frame2).getView();

        //navbar
        aq.navBarBaseLayout(R.id.navbar_base_layout);
        aq.navBarTitle(R.id.navbar_title, getResources().getString(R.string.myhkt_cu_title));
        aq.navBarButton(R.id.navbar_button_left, R.drawable.btn_back);

        aq.id(R.id.navbar_button_left).clicked(this, "onClick");

        //Initialize the container
        aq.id(R.id.cu_frame).backgroundColorId(R.color.white);
        aq.marginpx(R.id.cu_frame, basePadding, 0, basePadding, 0);
        aq.id(R.id.cu_frame).getView().setPadding(0, basePadding, 0, 0);

        //Top tab bar
        aq.marginpx(R.id.cu_buttonsframe, 0, 0, 0, extraLineSpace);
        int[] buttonResIds = new int[]{R.id.cu_button0, R.id.cu_button1, R.id.cu_button2};
        int[] stringsResIds = new int[]{R.string.support_consumer, R.string.SUPPORT_PREMIER, R.string.support_biz};
        for (int j = 0; j < 3; j++) {
            aq.id(buttonResIds[j]).height((int) getResources().getDimension(R.dimen.button_height), false).textColorId(R.color.hkt_txtcolor_grey).getTextView().setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize);
            aq.id(buttonResIds[j]).text(stringsResIds[j]);
            aq.id(buttonResIds[j]).clicked(this, "onClick");
        }

        btnPremier.setVisibility(View.GONE);
        showType = 0;
        if (ClnEnv.isLoggedIn()) {
            if (ClnEnv.getSessionPremierFlag()) {
                //Screen Tracker
                FAWrapper.getInstance().sendFAScreen(this, R.string.CONST_SCRN_HELPHKTPREMIER, false);
            } else {
                //Screen Tracker
                FAWrapper.getInstance().sendFAScreen(me, R.string.CONST_SCRN_HELPCONSUMER, false);
            }
        }

        //Show the list
        initNewFragment();
        refreshView();
    }

    private void initNewFragment() {
        spawnFragment(showType == 1 ?
                new ContactUsPremierFragment() : new ContactUsConsumerFragment());
    }

    private void spawnFragment(Fragment fragment) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.fl_view, fragment);
        fragmentTransaction.commit();
    }

    private void refreshView() {
        setBtn();
    }

    public void onClick(View v) {
        //Screen Tracker
        switch (v.getId()) {
            case R.id.navbar_button_left:
                onBackPressed();
                break;
            case R.id.cu_button0:
                //tracker
                FAWrapper.getInstance().sendFAScreen(me, R.string.CONST_SCRN_HELPCONSUMER, false);

                Utils.closeSoftKeyboard(me, v);
                showType = 0;
                spawnFragment(new ContactUsConsumerFragment());
                break;
            case R.id.cu_button1:
                //tracker
                FAWrapper.getInstance().sendFAScreen(me, R.string.CONST_SCRN_HELPHKTPREMIER, false);

                Utils.closeSoftKeyboard(me, v);
                showType = 1;
                spawnFragment(new ContactUsPremierFragment());
                break;
            case R.id.cu_button2:
                //tracker
                FAWrapper.getInstance().sendFAScreen(me, R.string.CONST_SCRN_HELPBUSINESS, false);

                Utils.closeSoftKeyboard(me, v);
                showType = 2;
                spawnFragment(new ContactUsBusinessFragment());
                break;
        }
        refreshView();
    }

    private void setBtn() {
        btnConsumer.setBackground(showType == 0 ?
                ContextCompat.getDrawable(this, R.drawable.hkt_cubtnleft_bg_sel) :
                ContextCompat.getDrawable(this, R.drawable.hkt_cubtnleft_bg_not_sel));
        btnConsumer.setTextColor(showType == 0 ?
                getResources().getColor(R.color.hkt_txtcolor_grey) :
                getResources().getColor(R.color.hkt_txtcolor_grey_disable));
        btnPremier.setBackground(showType == 1 ?
                ContextCompat.getDrawable(this, R.drawable.hkt_cubtnmiddle_bg_sel) :
                ContextCompat.getDrawable(this, R.drawable.hkt_cubtnmiddle_bg_not_sel));
        btnPremier.setTextColor(showType == 1 ?
                getResources().getColor(R.color.hkt_txtcolor_grey) :
                getResources().getColor(R.color.hkt_txtcolor_grey_disable));
        btnBusiness.setBackground(showType == 2 ?
                ContextCompat.getDrawable(this, R.drawable.hkt_cubtnright_bg_sel) :
                ContextCompat.getDrawable(this, R.drawable.hkt_cubtnright_bg_not_sel));
        btnBusiness.setTextColor(showType == 2 ?
                getResources().getColor(R.color.hkt_txtcolor_grey) :
                getResources().getColor(R.color.hkt_txtcolor_grey_disable));
    }

    public void loginFirst(MAINMENU clickItem) {
        loginFirstDialog(clickItem);
    }

    // Check if the permission granted,  before  dialing the phone number
    public void callPhone(String phoneNumber) {
        if (!TextUtils.isEmpty(phoneNumber)) {
            this.phoneNum = phoneNumber;
            if (RuntimePermissionUtil.isActionCallPermissionGranted(this)) {
                String num = phoneNum;
                if (num.contains("(")) {
                    num = num.substring(0, num.indexOf("(") - 1);
                }
                Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + num));
                startActivity(intent);

            } else {
                if (ClnEnv.getPref(this, Constant.CALL_PHONE_PERMISSION_DENIED, false)) {
                    displayDialog(this, getString(R.string.permission_denied_setting_enabled));
                } else {
                    RuntimePermissionUtil.requestCallPhonePermission(this);
                }
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == Constant.REQUEST_CALL_PHONE_PERMISSION) {
            //Permission is granted
            if (grantResults.length > 0 &&
                    grantResults[0] == PackageManager.PERMISSION_GRANTED &&
                    !TextUtils.isEmpty(phoneNum)) {
                String num = phoneNum;
                if (num.contains("(")) {
                    num = num.substring(0, num.indexOf("(") - 1);
                }
                Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + num));
                startActivity(intent);
            } else {

                displayDialog(this, getString(R.string.permission_denied));

                //This will return false if the user tick the Don't ask again checkbox
                if (!RuntimePermissionUtil.shouldShowCallPhoneRequestPermission(this)) {
                    ClnEnv.setPref(getApplicationContext(), Constant.CALL_PHONE_PERMISSION_DENIED, true);
                }
            }
        }
    }
}
