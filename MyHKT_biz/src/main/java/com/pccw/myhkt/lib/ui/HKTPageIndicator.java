package com.pccw.myhkt.lib.ui;
import androidx.viewpager.widget.ViewPager;
//Ref : https://github.com/JakeWharton/ViewPagerIndicator/blob/master/library/src/com/viewpagerindicator/PageIndicator.java
public interface HKTPageIndicator extends ViewPager.OnPageChangeListener {

	 void setViewPager(ViewPager view);
	 void setViewPager(ViewPager view, int initialPosition);
	 void setCurrentItem(int item);
	 void setOnPageChangeListener(ViewPager.OnPageChangeListener listener);
	 void notifyDataSetChanged();
}
