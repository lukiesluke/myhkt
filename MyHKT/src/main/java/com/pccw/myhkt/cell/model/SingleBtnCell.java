package com.pccw.myhkt.cell.model;

import android.view.View.OnClickListener;

import com.pccw.myhkt.R;

public class SingleBtnCell extends Cell{

	
	//If you setDraw and setText to be "" , it becomes imageButton.
	private OnClickListener onClickListener = null;
	private int btnWidth;
		
	public int getBtnWidth() {
		return btnWidth;
	}

	public void setBtnWidth(int btnWidth) {
		this.btnWidth = btnWidth;
	}

	public OnClickListener getOnClickListener() {
		return onClickListener;
	}

	public void setOnClickListener(OnClickListener onClickListener) {
		this.onClickListener = onClickListener;
	}
	

	public SingleBtnCell(String title, int btnWidth, OnClickListener onClickListener) {
		super();
		type = Cell.SINGLEBTN;
		this.btnWidth = btnWidth;
		drawable = null;
		titleColorId = R.color.hkt_textcolor;
		titleSizeDelta = 0;
		contentSizeDelta = 0;
		this.onClickListener = onClickListener;
		this.title = title;
	}
}