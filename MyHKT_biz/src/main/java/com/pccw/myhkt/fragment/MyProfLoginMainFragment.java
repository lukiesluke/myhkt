package com.pccw.myhkt.fragment;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.FragmentTransaction;

import com.pccw.biz.myhkt.R;
import com.pccw.dango.shared.entity.BcifRec;
import com.pccw.dango.shared.entity.SveeRec;
import com.pccw.myhkt.APIsResponse;
import com.pccw.myhkt.Utils;
import com.pccw.myhkt.fragment.UpdateCompanyInfoFragment.OnUpdateCompInfoListener;
import com.pccw.myhkt.lib.ui.AAQuery;

public class MyProfLoginMainFragment extends BaseFragment implements OnUpdateCompInfoListener {
    private MyProfLoginMainFragment me;
    private AAQuery aq;

    private SveeRec sveeRec = null;
    private FragmentTransaction ft;
    private int activeSubView = 0;

    // CallBack
    private OnMyProfLoginListener callback;

    private String TAG = "MyProfLoginMainFragment";

    public interface OnMyProfLoginListener {
        SveeRec getSveeRec();

        void setSveeRec(SveeRec sveeRec);

        void closeActivity();

        int getCurrentPage();

        void saveBcifRec(BcifRec bcifRec); //ClnEnv.getLgiCra().getOQualSvee()

        BcifRec getBcifRec();
    }


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        try {
            callback = (OnMyProfLoginListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement OnMyProfLoginListener");
        }
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        me = this;
        ft = getChildFragmentManager().beginTransaction();
        View fragmentLayout = inflater.inflate(R.layout.fragment_myproflogin, container, false);

        activeSubView = 0;
        displayChildview(0);
        return fragmentLayout;
    }

    public void openLoginidSubFrag() {
        activeSubView = 0;
        displayChildview(2);
    }

    public void openChangePwSubFrag() {
        activeSubView = 1;
        displayChildview(1);
    }

    public void openDeleteAccountFrag(String email) {
        activeSubView = 3;
        displayChildViewEmail(3, email);
    }

    public void displayChildview(int type) {
        displayChildview(type, "");
    }

    private void displayChildViewEmail(int type, String email) {
        displayChildview(type, email);
    }

    public void displayChildview(int type, String email) {
        ft = getChildFragmentManager().beginTransaction();
        if (type == 1) {
            ft.setCustomAnimations(R.anim.right_slide_in, R.anim.left_slide_out);
        } else if (type == 2) {
            ft.setCustomAnimations(R.anim.left_slide_in, R.anim.right_slide_out);
        } else if (type == 3) {
            ft.setCustomAnimations(R.anim.left_slide_in, R.anim.right_slide_out);
        }
        //Screen Tracker
        switch (activeSubView) {
            case 0:
                MyProfLoginFragment myProfLoginIDSubFragment = new MyProfLoginFragment();
                myProfLoginIDSubFragment.setOnUpdateCopanyInfoListener(this);
                ft.replace(R.id.fragment_myproflogin_fragment, myProfLoginIDSubFragment);
                break;
            case 1:
                ChangePwdFragment myProfLoginChangePwSubFragment = new ChangePwdFragment();
                ft.replace(R.id.fragment_myproflogin_fragment, myProfLoginChangePwSubFragment);
                break;
            case 2:
                UpdateCompanyInfoFragment updateCimpInfoFragment = new UpdateCompanyInfoFragment();
                updateCimpInfoFragment.setOnUpdateInfoListener(this);
                ft.replace(R.id.fragment_myproflogin_fragment, updateCimpInfoFragment);
                break;
            case 3:
                ft.replace(R.id.fragment_myproflogin_fragment, DeleteAccountFragment.newInstance(email));
                break;
        }

        Utils.closeSoftKeyboard(getActivity());
        ft.commit();
    }

    public void closeActivity() {
        callback.closeActivity();
    }

    @Override
    public void onStart() {
        super.onStart();
        sveeRec = callback.getSveeRec();
    }

    @Override
    public void onSuccess(APIsResponse response) {
        // TODO Auto-generated method stub
    }

    @Override
    public void onFail(APIsResponse response) {
        // TODO Auto-generated method stub
    }

    public void refresh() {
        if (debug) Log.i(TAG, "refresh");
        sveeRec = callback.getSveeRec();
        try {
            activeSubView = 0;
            displayChildview(0);
        } catch (Exception e) {

        }
    }

    @Override
    public void onDismissUpdateCompanuInfo() {
        // TODO Auto-generated method stub
        activeSubView = 0;
        displayChildview(2);
    }

    @Override
    public void onShowUpdateCompInfo() {
        // TODO Auto-generated method stub
        activeSubView = 2;
        displayChildview(1);
    }
}
