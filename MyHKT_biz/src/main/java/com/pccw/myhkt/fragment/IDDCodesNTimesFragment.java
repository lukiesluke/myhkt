package com.pccw.myhkt.fragment;

import java.lang.Thread.UncaughtExceptionHandler;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

import android.app.Activity;
import android.graphics.Point;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.ImageView.ScaleType;
import android.widget.LinearLayout;

import com.pccw.dango.shared.entity.SveeRec;
import com.pccw.dango.shared.tool.RC;
import com.pccw.myhkt.APIsManager;
import com.pccw.myhkt.APIsResponse;
import com.pccw.myhkt.ClnEnv;
import com.pccw.myhkt.DialogHelper;
import com.pccw.biz.myhkt.R;
import com.pccw.myhkt.Utils;
import com.pccw.myhkt.ViewUtils;
import com.pccw.myhkt.activity.BaseActivity;
import com.pccw.myhkt.lib.ui.AAQuery;
import com.pccw.myhkt.lib.ui.HKTButton;
import com.pccw.myhkt.lib.ui.RegInputItem;
import com.pccw.myhkt.lib.ui.popover.QuickAction;
import com.pccw.myhkt.lib.ui.popover.QuickTimeAction;
import com.pccw.myhkt.model.IDDCodesNTimes;
import com.pccw.myhkt.model.IDDCountry;


/************************************************************************
 * File : IDDCodesNTimesFragment.java
 * Desc : 
 * Name : IDDCodesNTimesFragment
 * by 	: 
 * Date : 
 * 
 * Change History:
 * Date       Modified By		Description
 * ---------- ----------------	-------------------------------
 * 06/23/2017  Abdulmoiz Esmail	- Moved the checking of language on the onAttach method and on the onCreateView method,
 *  							  checked if the locale  has changed to default English (due to webviews in Android 
 *  							  higher or equal 24), if does switch to Chinese. 
 *************************************************************************/
public class IDDCodesNTimesFragment extends BaseFragment{

	private String TAG = "IDDCodesNTimesFragment";
	private IDDCodesNTimesFragment me;
	private AAQuery aq;

	// CallBack
	private OnIDDCodeNtimesListener callback;

	private int 		titleWidth;
	private int			edittextpadding;
	private int 		btnWidth = 0;
	private int			fromCountryPos = 0;
	private int 		fromCityPos = 0;
	private int 		toCountryPos = 0;
	private int 		toCityPos = 0;	
	private int 		hrsPos = 0;
	private int 		minPos = 0;

	private List<IDDCountry> 		countryList;
	private Boolean 				isZh;

	private String[] 				hrsList;
	private String[] 				minsList;
	
	//Default country and city values
	private final String hongkong = "HONG KONG";
	private final String china	  = "China";
	
	//Country value to bypass server's fault 
	private final String taiwan	  = "Taiwan";

	private String[] countryNames;
	private String[][] cityNames;
	private IDDCodesNTimes codeNTime ;

	private Boolean isResultShown = false;
	private Boolean isCollpase = false;
	
	public interface OnIDDCodeNtimesListener {
		List<IDDCountry> countriesList = new ArrayList<IDDCountry>();
		List<IDDCountry> getCountryList();
		void setCountryList(List<IDDCountry> countryList);
	}


	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		
		// Language indicator
        isZh = "zh".equalsIgnoreCase(getResString(R.string.myhkt_lang));
		
		try {
			callback = (OnIDDCodeNtimesListener) activity;
		} catch (ClassCastException e) {
			throw new ClassCastException(activity.toString() + " must implement OnmyprofloginListener");
		}
	}

	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		me = this;		
		View fragmentLayout = inflater.inflate(R.layout.fragment_iddcodesntimes, container, false);

		//Switch to Zh if the locale changed to En and os is >=24
		if (Build.VERSION.SDK_INT >= 24 && isZh && "en".equalsIgnoreCase(getString(R.string.myhkt_lang))) {
				Utils.switchToZhLocale(getActivity());
		}
		
		
		aq = new AAQuery(fragmentLayout);

		initData();
		initUI();
		return fragmentLayout;
	}

	@Override
	public void onStart() {
		Drawable rightDrawable = getResources().getDrawable( R.drawable.btn_details);
		aq.id(R.id.iddcodesntimes_result_time_layout_txt4).getTextView().setCompoundDrawablesWithIntrinsicBounds(null, null, rightDrawable, null);
		int detailBtnPadding = (int) getResources().getDimension(R.dimen.detail_btn_padding);
		aq.id(R.id.iddcodesntimes_result_time_layout_txt4).getTextView().setCompoundDrawablePadding(detailBtnPadding);
		rightDrawable = null;
		super.onStart();
	}
	
	protected void initData() {
		super.initData();
		isResultShown = false;
		titleWidth = (int) getResources().getDimension(R.dimen.idd_title_width);
		edittextpadding = (int) getResources().getDimension(R.dimen.edittextpadding);
		btnWidth = deviceWidth / 2 ;
		
		hrsList = new String[24];
		for (int i=0; i < 24; i++) {
			hrsList[i] = String.format("%02d", i);					
		}
		minsList = new String[60];
		for (int i=0; i < 60; i++) {
			minsList[i] =  String.format("%02d", i);					
		}
		initTimePos();
	}
	
	protected void initUI() {		
		aq.id(R.id.iddcodesntimes_header1_layout_txt1).image(R.drawable.rightarrow_small);
		aq.id(R.id.iddcodesntimes_header1_layout_txt1).getImageView().setScaleType(ScaleType.CENTER);
		aq.id(R.id.iddcodesntimes_header1_layout_txt1).width(extralinespace , false);
		aq.spinText(R.id.iddcodesntimes_header1_layout_txt2, getResString(R.string.MYHKT_IDD_HEADER1), -2, false);
		aq.id(R.id.iddcodesntimes_header1_layout_txt2).textColorId(R.color.hkt_txtcolor_grey).width(LinearLayout.LayoutParams.MATCH_PARENT, false).background(0);
//				aq.normText(R.id.iddcodesntimes_header1_layout_txt2, getResString(R.string.MYHKT_IDD_HEADER1), -2);
		aq.id(R.id.iddcodesntimes_header1_layout_txt2).getTextView().setGravity(Gravity.CENTER_VERTICAL);	
		aq.id(R.id.iddcodesntimes_header1_layout_txt2).getTextView().setPadding(0, 0, extralinespace + edittextpadding, 0);
		aq.id(R.id.iddcodesntimes_header1_layout).clicked(this, "onClick");

		aq.id(R.id.iddcodesntimes_callfrom_layout).getView().setPadding(extralinespace, 0, extralinespace, 0);
		aq.id(R.id.iddcodesntimes_callto_layout).getView().setPadding(extralinespace, 0, extralinespace, 0);
		aq.id(R.id.iddcodesntimes_time_layout).getView().setPadding(extralinespace, 0, extralinespace, 0);

		aq.normText(R.id.iddcodesntimes_tableLayout_title_callto,getResString(R.string.MYHKT_CNT_CALL_TO));
		aq.normText(R.id.iddcodesntimes_tableLayout_title_callfrom,getResString(R.string.MYHKT_CNT_CALL_FROM));
		aq.normText(R.id.iddcodesntimes_tableLayout_title_time,getResString(R.string.MYHKT_CNT_CALL_AT));
		aq.id(R.id.iddcodesntimes_tableLayout_title_callto).width(titleWidth, false);
		aq.id(R.id.iddcodesntimes_tableLayout_title_callfrom).width(titleWidth, false);
		aq.id(R.id.iddcodesntimes_tableLayout_title_time).width(titleWidth, false);
		aq.id(R.id.iddcodesntimes_tableLayout_title_callto).getTextView().setGravity(Gravity.RIGHT|Gravity.CENTER_VERTICAL);
		aq.id(R.id.iddcodesntimes_tableLayout_title_callfrom).getTextView().setGravity(Gravity.RIGHT|Gravity.CENTER_VERTICAL);
		aq.id(R.id.iddcodesntimes_tableLayout_title_time).getTextView().setGravity(Gravity.RIGHT|Gravity.CENTER_VERTICAL);

		aq.spinText(R.id.iddcodesntimes_tableLayout_et_callto, "", 0, false);
		aq.spinText(R.id.iddcodesntimes_tableLayout_et_callfrom, "", 0, false);
		aq.spinText(R.id.iddcodesntimes_tableLayout_et_time, "", 0, false);
		aq.id(R.id.iddcodesntimes_tableLayout_et_callto).width(LinearLayout.LayoutParams.MATCH_PARENT, false).clicked(this,"onClick").background(0);
		aq.id(R.id.iddcodesntimes_tableLayout_et_callfrom).width(LinearLayout.LayoutParams.MATCH_PARENT, false).clicked(this, "onClick").background(0);
		aq.id(R.id.iddcodesntimes_tableLayout_et_time).width(LinearLayout.LayoutParams.MATCH_PARENT, false).clicked(this, "onClick").background(0);
		setEditTextName(true,fromCountryPos,fromCityPos);		
		setEditTextName(false,toCountryPos,toCityPos);
		setTime();

		aq.normText(R.id.iddcodesntimes_header2, getResString(R.string.MYHKT_CNT_REMARK1), -5);
		aq.id(R.id.iddcodesntimes_header2).getTextView().setGravity(Gravity.LEFT|Gravity.CENTER_VERTICAL);
		aq.id(R.id.iddcodesntimes_header2).getView().setPadding(extralinespace, 0, extralinespace, 0);

		aq.line(R.id.iddcodesntime_line1);
		aq.line(R.id.iddcodesntime_line2);
		aq.line(R.id.iddcodesntime_line3);
		aq.line(R.id.iddcodesntime_line4);
		aq.line(R.id.iddcodesntime_line5);
		aq.marginpx(R.id.iddcodesntime_line1, titleWidth + extralinespace, 0, extralinespace, 0);
		aq.marginpx(R.id.iddcodesntime_line2, titleWidth + extralinespace, 0, extralinespace, 0);
		aq.marginpx(R.id.iddcodesntime_line3, titleWidth + extralinespace, 0, extralinespace, 0);
		aq.marginpx(R.id.iddcodesntime_line4, titleWidth + extralinespace, 0, extralinespace, 0);
		aq.marginpx(R.id.iddcodesntime_line5, extralinespace, 0, extralinespace, 0);

		aq.normTxtBtn(R.id.iddcodesntimes_search, getResString(R.string.MYHKT_IDDRATE_SEARCHBAR_PLACEHOLDER), btnWidth);
		aq.marginpx(R.id.iddcodesntimes_search, 0, 0, 0, extralinespace);
		aq.id(R.id.iddcodesntimes_search).clicked(this, "onClick");
		((HKTButton)aq.id(R.id.iddcodesntimes_search).getView()).setEnableClick(false);
		

		//Result part
		aq.id(R.id.iddcodesntimes_result_frame).visibility(isResultShown? View.VISIBLE : View.GONE);

		aq.id(R.id.iddcodesntimes_result_header_layout).clicked(this, "onClick");
		aq.marginpx(R.id.iddcodesntimes_result_remark, extralinespace, extralinespace/2, extralinespace, 0);
		aq.id(R.id.iddcodesntimes_result_remark).getWebView().getSettings().setDefaultTextEncodingName("utf-8");
		
		aq.id(R.id.iddcodesntimes_result_remark).getWebView().loadUrl(isZh? "file:///android_asset/idd_code_remarks_zh.html" :"file:///android_asset/idd_code_remarks_en.html" );
		aq.id(R.id.iddcodesntimes_result_header_layout_txt1).image(R.drawable.rightarrow_small);
		aq.id(R.id.iddcodesntimes_result_header_layout_txt1).getImageView().setScaleType(ScaleType.CENTER);
		aq.id(R.id.iddcodesntimes_result_header_layout_txt1).width(extralinespace , false);
		aq.normTextGrey(R.id.iddcodesntimes_result_header_layout_txt2, getResString(R.string.MYHKT_CNT_SEARCHING_HEADER), -2);
		aq.gravity(R.id.iddcodesntimes_result_header_layout_txt2, Gravity.LEFT|Gravity.CENTER_VERTICAL);

		aq.padding(R.id.iddcodesntimes_result_time_layout, extralinespace, 0, extralinespace, 0);
		aq.normText(R.id.iddcodesntimes_result_time_layout_txt1,"", -2);
		normTextBlue(R.id.iddcodesntimes_result_time_layout_txt2, "", -2);
		aq.normText(R.id.iddcodesntimes_result_time_layout_txt3,")", -2);
		normTextBlue(R.id.iddcodesntimes_result_time_layout_txt4, getResString(R.string.MYHKT_CNT_SEARCHING_REMARK_BTN), -2);		
		aq.gravity(R.id.iddcodesntimes_result_time_layout_txt4, Gravity.RIGHT|Gravity.CENTER_VERTICAL);
		aq.id(R.id.iddcodesntimes_result_time_layout_txt4).clicked(this, "onClick");


		aq.normTextGrey(R.id.iddcodesntimes_result_title_access_code, getResString(R.string.MYHKT_CNT_INT_ACC_CODE), -5);
		aq.normTextGrey(R.id.iddcodesntimes_result_title_territory_code,getResString(R.string.MYHKT_CNT_COUNTRY_TERR_CODE), -5);
		aq.normTextGrey(R.id.iddcodesntimes_result_title_area_code,getResString(R.string.MYHKT_CNT_AREA_CODE_IFANY), -5);
		aq.gravity(R.id.iddcodesntimes_result_title_access_code, Gravity.CENTER);
		aq.gravity(R.id.iddcodesntimes_result_title_territory_code, Gravity.CENTER);
		aq.gravity(R.id.iddcodesntimes_result_title_area_code, Gravity.CENTER);
		aq.padding(R.id.iddcodesntimes_result_title_layout, extralinespace, 0, extralinespace, 0);
		aq.normText(R.id.iddcodesntimes_result_country,"" , 4);
		aq.padding(R.id.iddcodesntimes_result_country, extralinespace, 0, extralinespace, 0);

		aq.padding(R.id.iddcodesntimes_result_fix_layout, extralinespace, 0, extralinespace, 0);
		aq.id(R.id.iddcodesntimes_result_fix_layout).backgroundColorId(R.color.idd_grey);
		aq.marginpx(R.id.iddcodesntimes_result_fix_layout, 0, 0, 0, 0);
		normTextBlue(R.id.iddcodesntimes_result_fix_territory_code, "", 0);
		normTextBlue(R.id.iddcodesntimes_result_fix_area_code, "", 0);
		normTextBlue(R.id.iddcodesntimes_result_fix_access_code, "", 0);
		normTextBlue(R.id.iddcodesntimes_result_fix_name, "", -5);

		aq.padding(R.id.iddcodesntimes_result_mob_layout, extralinespace, 0, extralinespace, 0);
		aq.id(R.id.iddcodesntimes_result_mob_layout).backgroundColorId(R.color.idd_grey);
		aq.marginpx(R.id.iddcodesntimes_result_mob_layout, 0, extralinespace/2, 0, extralinespace);
		normTextBlue(R.id.iddcodesntimes_result_mob_territory_code, "", 0);
		normTextBlue(R.id.iddcodesntimes_result_mob_area_code, "", 0);
		normTextBlue(R.id.iddcodesntimes_result_mob_access_code, "", 0);
		normTextBlue(R.id.iddcodesntimes_result_mob_name,"", -5);
		

	}

	public void normTextBlue(int id, String text, int spdelta){
		aq.normText(id, text, spdelta );
		aq.id(id).textColorId(R.color.hkt_textcolor).height(extralinespace);
		aq.gravity(id, Gravity.CENTER);
	}

	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.iddcodesntimes_tableLayout_et_callfrom:
			openCounrtyPicker(true);
			break;
		case R.id.iddcodesntimes_tableLayout_et_callto:
			openCounrtyPicker(false);
			break;	
		case R.id.iddcodesntimes_tableLayout_et_time:
			openTimePicker();
			break;
		case R.id.iddcodesntimes_result_time_layout_txt4:
			DialogHelper.createInfoDialog(this.getActivity(), getResString(R.string.MYHKT_CNT_SEARCHING_REMARK));
			break;
		case R.id.iddcodesntimes_search:
			// Event Tracker
			
			HashMap<String,String> hmSelectedCodeDetail = new HashMap<String, String>();
			hmSelectedCodeDetail.put("local_v1", countryList.get(fromCountryPos).getVal());
			hmSelectedCodeDetail.put("local_v2", countryList.get(fromCountryPos).getCity().get(fromCityPos).getVal());
			hmSelectedCodeDetail.put("dest_v1", countryList.get(toCountryPos).getVal());
			hmSelectedCodeDetail.put("dest_v2", countryList.get(toCountryPos).getCity().get(toCityPos).getVal());
			hmSelectedCodeDetail.put("hr", hrsList[hrsPos]);
			hmSelectedCodeDetail.put("min", minsList[minPos]);
			APIsManager.doGetIDDCodeResult(me, hmSelectedCodeDetail);
			break;
		case R.id.iddcodesntimes_header1_layout:
			if (isResultShown) {
				if (isCollpase) {
					isCollpase = false;
					ViewUtils.expand(aq.id(R.id.iddcodesntimes_search_sub_frame).getView());
				} else {
					isCollpase = true;
					ViewUtils.collapse(aq.id(R.id.iddcodesntimes_search_sub_frame).getView());	
				}
				aq.spinText(R.id.iddcodesntimes_header1_layout_txt2, getResString(R.string.MYHKT_IDD_HEADER1), -2, !isCollpase);
				aq.id(R.id.iddcodesntimes_header1_layout_txt2).textColorId(R.color.hkt_txtcolor_grey).width(LinearLayout.LayoutParams.MATCH_PARENT, false).background(0);
				aq.id(R.id.iddcodesntimes_header1_layout_txt2).getTextView().setPadding(0, 0, extralinespace + edittextpadding, 0);
				aq.id(R.id.iddcodesntimes_header1_layout_txt2).getTextView().setGravity(Gravity.CENTER_VERTICAL);
			}
		}
	}

	

	/**********************************
	 * 
	 * Time part
	 * 
	 ***********************************/

	private void openTimePicker() {
		final QuickTimeAction pickerWheelAction = new QuickTimeAction(getActivity(), 0, 0);
		pickerWheelAction.initPickerWheel(hrsList, minsList, hrsPos, minPos);

		pickerWheelAction.setOnPickerScrollListener(new QuickTimeAction.OnPickerScrollListener() {
			@Override
			public void onPickerScroll(String leftResult, String rightResult, int leftPos, int rightPos) {

				hrsPos = leftPos;
				minPos = rightPos;			
				setTime();
			}
		});
		pickerWheelAction.showPicker(aq.id(R.id.iddcodesntimes_tableLayout_et_time).getView(), aq.id(R.id.iddcodesntimes_search_frame).getView());
	}
	private void initTimePos(){
		hrsPos = Calendar.getInstance().get(Calendar.HOUR_OF_DAY) ;
		minPos = Calendar.getInstance().get(Calendar.MINUTE) ;
	}
	private void setTime(){
		aq.id(R.id.iddcodesntimes_tableLayout_et_time).text(hrsList[hrsPos] + ":" + minsList[minPos]);
	}

	/**********************************
	 * 
	 * Country part
	 * 
	 ***********************************/
	private void prepareCountryData(){
		countryNames = new String[countryList.size()];
		cityNames = new String[countryList.size()][];
		if(countryList.isEmpty()) {
		}
		for (int i=-0; i < countryList.size(); i++){
			countryNames[i] = isZh ? countryList.get(i).getCname() : countryList.get(i).getEname();
			cityNames[i] = new String[countryList.get(i).getCity().size()];
			for (int j = 0; j < countryList.get(i).getCity().size(); j++) {				
				cityNames[i][j] = isZh ? countryList.get(i).getCity().get(j).getCname() : countryList.get(i).getCity().get(j).getEname();
			}			
		}
	}
	private void initCountryNCityPos(){
		for (int i=0; i< countryList.size(); i++) {
			if (countryList.get(i).getVal().equals(china)) {
				for (int j = 0; j < countryList.get(i).getCity().size(); j++) {
					if (countryList.get(i).getCity().get(j).getVal().equals(hongkong)) {
						fromCountryPos = i;
						fromCityPos = j;
						toCountryPos = i;
						toCityPos = j;
					}
				}
			}
		}
	}

	private void openCounrtyPicker(final Boolean isCallFrom){
		if ((countryNames !=null && countryNames.length >0) && (cityNames !=null && cityNames.length >0)) {
			final QuickAction pickerWheelAction = new QuickAction(getActivity(), 0, 0);
			if (isCallFrom) {
				pickerWheelAction.initPickerWheel(countryNames, cityNames, fromCountryPos, fromCityPos);
			} else {
				pickerWheelAction.initPickerWheel(countryNames, cityNames, toCountryPos, toCityPos);
			}
			pickerWheelAction.setOnPickerScrollListener(new QuickAction.OnPickerScrollListener() {
				@Override
				public void onPickerScroll(String leftResult, String rightResult, int leftPos, int rightPos) {
					if (isCallFrom) {
						fromCountryPos = leftPos;
						fromCityPos = rightPos;
					} else {
						toCountryPos = leftPos;
						toCityPos = rightPos;
					}
					if (debug) Log.i(TAG, leftResult+ "/" + rightResult+ "/" + leftPos + "/" + rightPos);
					setEditTextName(isCallFrom, isCallFrom?fromCountryPos:toCountryPos, isCallFrom?fromCityPos:toCityPos);
				}
			});
			pickerWheelAction.showPicker(aq.id(isCallFrom? R.id.iddcodesntimes_tableLayout_et_callfrom:R.id.iddcodesntimes_tableLayout_et_callto).getView(), aq.id(R.id.iddcodesntimes_search_frame).getView());
		}
	}

	private void setEditTextName(Boolean isCallFrom, int countryPos ,int cityPos) {
		String name = "";
		if ((countryNames !=null && countryNames.length >0) && (cityNames !=null && cityNames.length >0)) {

			if (isZh) {
				if (countryList.get(countryPos).getCity().get(cityPos).getCname() !=null && !countryList.get(countryPos).getCity().get(cityPos).getCname().equals("")){
					name = countryList.get(countryPos).getCname() + ", " + countryList.get(countryPos).getCity().get(cityPos).getCname();
				} else {
					name = countryList.get(countryPos).getCname();
				}
			} else {
				if (countryList.get(countryPos).getCity().get(cityPos).getEname() !=null && !countryList.get(countryPos).getCity().get(cityPos).getEname().equals("")){
					name = countryList.get(countryPos).getCity().get(cityPos).getEname() + ", " + countryList.get(countryPos).getEname();
				} else {
					name = countryList.get(countryPos).getEname();
				}
			}
		}
		aq.id(isCallFrom? R.id.iddcodesntimes_tableLayout_et_callfrom:R.id.iddcodesntimes_tableLayout_et_callto).text(name);
	}

	/**********************************
	 * 
	 * Result part
	 * 
	 ***********************************/

	public void setResult(){

		aq.id(R.id.iddcodesntimes_result_frame).visibility(isResultShown? View.VISIBLE : View.GONE);
		String name = aq.id(R.id.iddcodesntimes_tableLayout_et_callto).getText().toString();
		aq.id(R.id.iddcodesntimes_result_country).text(name).height(ViewGroup.LayoutParams.WRAP_CONTENT);
		aq.id(R.id.iddcodesntimes_result_fix_access_code).text("0060").height(ViewGroup.LayoutParams.WRAP_CONTENT);
		aq.id(R.id.iddcodesntimes_result_fix_territory_code).text(codeNTime.getCountryCode()).height(ViewGroup.LayoutParams.WRAP_CONTENT);
		aq.id(R.id.iddcodesntimes_result_fix_area_code).text(codeNTime.getAreaCode()).height(ViewGroup.LayoutParams.WRAP_CONTENT);
		aq.id(R.id.iddcodesntimes_result_fix_name).text(getResString(R.string.MYHKT_CNT_LTS_NUM)).height(ViewGroup.LayoutParams.WRAP_CONTENT);

		aq.id(R.id.iddcodesntimes_result_mob_access_code).text("0060").height(ViewGroup.LayoutParams.WRAP_CONTENT);
		aq.id(R.id.iddcodesntimes_result_mob_territory_code).text(codeNTime.getCountryCode()).height(ViewGroup.LayoutParams.WRAP_CONTENT);
		aq.id(R.id.iddcodesntimes_result_mob_area_code).text("").height(ViewGroup.LayoutParams.WRAP_CONTENT);
		aq.id(R.id.iddcodesntimes_result_mob_name).text(getResString(R.string.MYHKT_CNT_MOB_NUM)).height(ViewGroup.LayoutParams.WRAP_CONTENT);

		aq.id(R.id.iddcodesntimes_result_time_layout_txt1).text("(" + codeNTime.getTimeDiff() + " " + getResString(R.string.MYHKT_CNT_TIMEDIFF) + " ");
		aq.id(R.id.iddcodesntimes_result_time_layout_txt2).text(codeNTime.getTime());
		
		aq.spinText(R.id.iddcodesntimes_header1_layout_txt2, getResString(R.string.MYHKT_IDD_HEADER1), -2, !isCollpase);
		aq.id(R.id.iddcodesntimes_header1_layout_txt2).textColorId(R.color.hkt_txtcolor_grey).width(LinearLayout.LayoutParams.MATCH_PARENT, true).background(0);
		aq.id(R.id.iddcodesntimes_header1_layout_txt2).getTextView().setPadding(0, 0, extralinespace + edittextpadding, 0);
		aq.id(R.id.iddcodesntimes_header1_layout_txt2).getTextView().setGravity(Gravity.CENTER_VERTICAL);
	}


	
	public void onResume(){
		if (debug) Log.i(TAG, "Resume");
		super.onResume();	
		countryList = callback.getCountryList();
		if (countryList == null) {
			((HKTButton)aq.id(R.id.iddcodesntimes_search).getView()).setEnableClick(false);
			APIsManager.doGetIDDCountries(this);			
		} else {
			prepareCountryData();
			initCountryNCityPos();
			setEditTextName(true,fromCountryPos,fromCityPos);
			setEditTextName(false,toCountryPos,toCityPos);
			((HKTButton)aq.id(R.id.iddcodesntimes_search).getView()).setEnableClick(true);
		}
		//Handle the case that the activity has been restart and the result should show
		if (isResultShown == true) {
			setResult();
		}
	}
	
	@Override
	public void onSuccess(APIsResponse response) {
		if (response != null) {
			if ((response.getActionTy() != null && response.getActionTy() == APIsManager.IDDCTY)) {				
				countryList = (List<IDDCountry>) response.getCra();
				callback.setCountryList(countryList);
				prepareCountryData();
				initCountryNCityPos();
				setEditTextName(true,fromCountryPos,fromCityPos);
				setEditTextName(false,toCountryPos,toCityPos);		
				((HKTButton)aq.id(R.id.iddcodesntimes_search).getView()).setEnableClick(true);
			} else if ((response.getActionTy() != null && response.getActionTy() == APIsManager.IDDCODE)) {
				codeNTime = (IDDCodesNTimes) response.getCra();
				isResultShown = true;
				setResult();
				isCollpase = true;
				ViewUtils.collapse(aq.id(R.id.iddcodesntimes_search_sub_frame).getView());
				aq.id(R.id.iddcodesntimes_header1_layout_txt2).getTextView().setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.btn_arrowdown, 0);
			}
		}
	}

	@Override
	public void onFail(APIsResponse response) {
		if (debug) Log.i(TAG, "Fail");
		// General Error Message
		if (!"".equals(response.getMessage()) && response.getMessage() != null) {
			DialogHelper.createSimpleDialog(getActivity(),  response.getMessage());
		} else if (RC.TCSESS_MM.equalsIgnoreCase(response.getReply().getCode())) {
			BaseActivity.ivSessDialog();
		} else {
			DialogHelper.createSimpleDialog(getActivity(),  ClnEnv.getRPCErrMsg(getActivity(), response.getReply().getCode()));
		}		
	}



	

}
