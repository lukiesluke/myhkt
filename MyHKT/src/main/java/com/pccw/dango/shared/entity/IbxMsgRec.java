/*
    Inbox Message Record
    
    Keywords
    --------
    $URL: $
    $Rev: $
    $Date: $
    $Author: $
*/

package com.pccw.dango.shared.entity;

import java.io.Serializable;

public class IbxMsgRec implements Serializable
{
    private static final long serialVersionUID = -3116426102055111564L;
    
    public int                      rid;                /* Record Id                                     */
    public int                      custRid;            /* CUST rid                                      */
    public String                   subjEn;             /* Subject (English)                             */
    public String                   ctntEn;             /* Message Content (English)                     */
    public String                   urlEn;              /* URL (English)                                 */
    public String                   pathEn;             /* Image Path (English)                          */
    public String                   subjZh;             /* Subject (Chinese)                             */
    public String                   ctntZh;             /* Message Content (Chinese)                     */
    public String                   urlZh;              /* URL (Chinese)                                 */
    public String                   pathZh;             /* Image Path (Chinese)                          */
    public String                   readSts;            /* Read Status                                   */
    public String                   msgTy;              /* Message Type                                  */
    public String                   msgCat;             /* Message Category                              */
    public int                      pmbtRid;            /* Postman Beat rid                              */
    public String                   createTs;           /* Record Create TS                              */
    public String                   expTs;              /* Expiry TS                                     */
    public String                   readTs;             /* Read TS                                       */
    public int                      rev;                /* Record Revision                               */
    
    public static final int         L_SUBJ_EN      = 128;
    public static final int         L_CTNT_EN      = 1024;
    public static final int         L_URL_EN       = 128;
    public static final int         L_PATH_EN      = 128;
    public static final int         L_SUBJ_ZH      = 128;
    public static final int         L_CTNT_ZH      = 1024;
    public static final int         L_URL_ZH       = 128;
    public static final int         L_PATH_ZH      = 128;
    public static final int         L_READ_STS     = 1;
    public static final int         L_MSG_TY       = 1;
    public static final int         L_MSG_CAT      = 2;
    public static final int         L_CREATE_TS    = 14;
    public static final int         L_EXP_TS       = 14;
    public static final int         L_READ_TS      = 14;


    
    public static void main(String rArg[])
    {
        System.out.println(getVer());
        return;
    }
    
    
    public static String getVer()
    {
        return ("$URL: $, $Rev: $");
    }


    public IbxMsgRec()
    {
        initAndClear();
    }


    final void initAndClear()
    {
        init();
        clear();
    }

    
    protected void init()
    {
    }


    public void clear()
    {
        rid             = 0;
        custRid         = 0;
        subjEn          = "";
        ctntEn          = "";
        urlEn           = "";
        pathEn          = "";
        subjZh          = "";
        ctntZh          = "";
        pathZh          = "";
        urlZh           = "";
        readSts         = "";
        msgTy           = "";
        msgCat          = "";
        pmbtRid         = 0;
        createTs        = "";
        expTs           = "";
        readTs          = "";
        rev             = 0;
    }


    public IbxMsgRec copyFrom(IbxMsgRec rSrc)
    {
        rid             = rSrc.rid;
        custRid         = rSrc.custRid;
        subjEn          = rSrc.subjEn;
        ctntEn          = rSrc.ctntEn;
        urlEn           = rSrc.urlEn;
        pathEn          = rSrc.pathEn; 
        subjZh          = rSrc.subjZh;
        ctntZh          = rSrc.ctntZh;
        urlZh           = rSrc.urlZh;
        pathZh          = rSrc.pathZh;
        readSts         = rSrc.readSts;
        msgTy           = rSrc.msgTy;
        msgCat          = rSrc.msgCat;
        pmbtRid         = rSrc.pmbtRid;
        createTs        = rSrc.createTs;
        expTs           = rSrc.expTs;
        readTs          = rSrc.readTs;
        rev             = rSrc.rev;
        
        return (this);
    }


    public IbxMsgRec copyTo(IbxMsgRec rDes)
    {
        rDes.copyFrom(this);
        return (rDes);
    }
    
    
    public IbxMsgRec copyMe()
    {
        IbxMsgRec                     rDes;

        rDes = new IbxMsgRec();
        rDes.copyFrom(this);
        return (rDes);
    }

}
