package com.pccw.myhkt.dialogs;

import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import android.text.InputFilter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;

import com.pccw.myhkt.R;

public class ChangeDisplayNameDialog extends DialogFragment {

    private OnChangeDisplayName mCallback;
    private static String TAG_NICKNAME = "nickname";

    private static String TAG_POSITION = "postion";

    public interface OnChangeDisplayName {
        void onChangeNickname(String name, int position);
    }

    public void setCallback(OnChangeDisplayName callback) {
        mCallback = callback;
    }

    public static ChangeDisplayNameDialog newInstance(String nickname, int position) {
        ChangeDisplayNameDialog changeDisplayNameDialog = new ChangeDisplayNameDialog();

        // Supply num input as an argument.
        Bundle args = new Bundle();
        args.putString(TAG_NICKNAME, nickname);
        args.putInt(TAG_POSITION, position);
        changeDisplayNameDialog.setArguments(args);

        return changeDisplayNameDialog;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        getDialog().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.display_name_dialog, container, false);
        String nickname = getArguments().getString(TAG_NICKNAME);
        int position = getArguments().getInt(TAG_POSITION);

        final EditText etChangeName = rootView.findViewById(R.id.et_change_display_name);
        etChangeName.setFilters(new InputFilter[]{new InputFilter.LengthFilter(getResources().getInteger(R.integer.CONST_MAX_ALIAS))});
        etChangeName.setText(nickname);
        etChangeName.setSelection(etChangeName.getText().length());
        etChangeName.setFocusable(true);
        etChangeName.requestFocus();

        Button okBtn = rootView.findViewById(R.id.ok_button);
        okBtn.setOnClickListener(v -> {
            mCallback.onChangeNickname(etChangeName.getText().toString(), position);
            getDialog().dismiss();
        });

        Button cancelBtn = rootView.findViewById(R.id.cancel_button);
        cancelBtn.setOnClickListener(v -> getDialog().dismiss());


        return rootView;
    }

}