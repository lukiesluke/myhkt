# MyHKT Android app

### Before build
  - check `$projectDir/src/main/res/values/clncfg.xml`
  - `UATPRDSWITCH` true to enable user to switch to UAT by typing 'changemode' on username
  - `DEBUG`, `DEBUGGRQ` and `DEBUGCRA` to be `false` for production

### Building release

For MyHKT (Consumer)
```sh
$ cd ROOT_FOLDER
$ ./gradlew :MyHKT:assembleRelease
```

For MyHKT (Business)
```sh
$ cd ROOT_FOLDER
$ ./gradlew :MyHKT_Biz:assembleRelease
```

Refresh dependencies by and check lint error:
```sh
$ cd ROOT_FOLDER
$ ./gradlew clean --refresh-dependencies buildNeeded --debug
```
The build will appear on your desktop
