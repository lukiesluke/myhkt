/*
    Service Plan
    
    Keywords
    --------
    $URL: $
    $Rev: $
    $Date: $
    $Author: $
*/

package com.pccw.dango.shared.entity;

import java.io.Serializable;

public class SrvPlan implements Serializable
{
    private static final long serialVersionUID = 2820281217556985815L;
    
    private String                  srvCd;              /* service code or id                            */
    private String                  StDt;               /* service eff start date                        */
    private String                  EnDt;               /* service eff end date                          */
    private String                  fee;                /* service fee                                   */
    private String                  desnEn;             /* service desc in eng                           */
    private String                  desnZh;             /* service desc in chi                           */
    private String                  longDesn1En;        /* svc FullDesc 1 in eng                         */
    private String                  longDesn2En;        /* svc FullDesc 2 in eng                         */
    private String                  longDesn1Zh;        /* svc FullDesc 1 in chi                         */
    private String                  longDesn2Zh;        /* svc FullDesc 2 in chi                         */
    private String                  tncEn;              /* TNC in eng                                    */
    private String                  tncZh;              /* TNC in chi                                    */
    private String                  usg;                /* service usage                                 */
    private String                  usgStDt;            /* service usage start date                      */
    private String                  usgEnDt;            /* service usage end date                        */
    private String                  usgUnt;             /* service usage unit                            */
    private String                  srvCmmtPrd;         /* commitment period                             */
    private String                  srvPenalty;         /* service penalty                               */
    private String                  srvCmmtRmn;         /* service remaining period                      */
    private String                  srvEnt;             /* service entitlement                           */
    private String                  rebtEn;             /* Rebate Desn in eng (LTS)                      */
    private String                  rebtZh;             /* Rebate Desn in chi (LTS)                      */
    private String                  rebtAmt;            /* rebate ($ in List)                            */
    private String                  monthlyTermRate;    /* Monthly Term Rate                             */
    private String                  m2mRate;            /* month to month rate                           */
    private String                  plnTy;              /* Plan Type                                     */
    private String                  plnSubTy;           /* Plan Sub Type                                 */
    private String                  cntrStDt;           /* Contract Start                                */
    private String                  cntrEnDt;           /* Contract End                                  */
    private String                  cntrDur;            /* Contract Duration                             */
    private String                  cmmtInd;            /* Commitment Indicator                          */
    private String                  seasInd;            /* Seasonal Indicator                            */
    private String                  suppInd;            /* Suppress Indicator                            */
    private String                  summInd;            /* Summary Indicator                             */

    
    public static void main(String rArg[])
    {
        System.out.println(getVer());
        return;
    }
    
    
    public static String getVer()
    {
        return ("$URL: $, $Rev: $");
    }

    
    public SrvPlan()
    {
        initAndClear();
    }

    
    final void initAndClear()
    {
        init();
        clear();
    }

    
    protected void init()
    {
    }


    public void clear()
    {
        clearSrvCd();
        clearStDt();
        clearEnDt();
        clearFee();
        clearDesnEn();
        clearDesnZh();
        clearLongDesn1En();
        clearLongDesn2En();
        clearLongDesn1Zh();
        clearLongDesn2Zh();
        clearTncEn();
        clearTncZh();
        clearUsg();
        clearUsgStDt();
        clearUsgEnDt();
        clearUsgUnt();
        clearSrvCmmtPrd();
        clearSrvPenalty();
        clearSrvCmmtRmn();
        clearSrvEnt();
        clearRebtEn();
        clearRebtZh();
        clearRebtAmt();
        clearMonthlyTermRate();
        clearM2mRate();
        clearPlnTy();
        clearPlnSubTy();
        clearCntrStDt();
        clearCntrEnDt();
        clearCntrDur();
        clearCmmtInd();
        clearSeasInd();
        clearSuppInd();
        clearSummInd();
    }


    public SrvPlan copyFrom(SrvPlan rSrc)
    {
        setSrvCd(rSrc.getSrvCd());
        setStDt(rSrc.getStDt());
        setEnDt(rSrc.getEnDt());
        setFee(rSrc.getFee());
        setDesnEn(rSrc.getDesnEn());
        setDesnZh(rSrc.getDesnZh());
        setLongDesn1En(rSrc.getLongDesn1En());
        setLongDesn2En(rSrc.getLongDesn2En());
        setLongDesn1Zh(rSrc.getLongDesn1Zh());
        setLongDesn2Zh(rSrc.getLongDesn2Zh());
        setTncEn(rSrc.getTncEn());
        setTncZh(rSrc.getTncZh());
        setUsg(rSrc.getUsg());
        setUsgStDt(rSrc.getUsgStDt());
        setUsgEnDt(rSrc.getUsgEnDt());
        setUsgUnt(rSrc.getUsgUnt());
        setSrvCmmtPrd(rSrc.getSrvCmmtPrd());
        setSrvPenalty(rSrc.getSrvPenalty());
        setSrvCmmtRmn(rSrc.getSrvCmmtRmn());
        setSrvEnt(rSrc.getSrvEnt());
        setRebtEn(rSrc.getRebtEn());
        setRebtZh(rSrc.getRebtZh());
        setRebtAmt(rSrc.getRebtAmt());
        setMonthlyTermRate(rSrc.getMonthlyTermRate());
        setM2mRate(rSrc.getM2mRate());
        setPlnTy(rSrc.getPlnTy());
        setPlnSubTy(rSrc.getPlnSubTy());
        setCntrStDt(rSrc.getCntrStDt());
        setCntrEnDt(rSrc.getCntrEnDt());
        setCntrDur(rSrc.getCntrDur());
        setCmmtInd(rSrc.getCmmtInd());
        setSeasInd(rSrc.getSeasInd());
        setSuppInd(rSrc.getSuppInd());
        setSummInd(rSrc.getSummInd());

        return (this);
    }


    public SrvPlan copyTo(SrvPlan rDes)
    {
        rDes.copyFrom(this);
        return (rDes);
    }
    
    
    public SrvPlan copyMe()
    {
        SrvPlan                     rDes;

        rDes = new SrvPlan();
        rDes.copyFrom(this);
        return (rDes);
    }


    public void clearSrvCd()
    {
        srvCd = "";
    }


    public void setSrvCd(String rArg)
    {
        srvCd = rArg;
    }


    public String getSrvCd()
    {
        return (srvCd);
    }


    public void clearStDt()
    {
        StDt = "";
    }


    public void setStDt(String rArg)
    {
        StDt = rArg;
    }


    public String getStDt()
    {
        return (StDt);
    }


    public void clearEnDt()
    {
        EnDt = "";
    }


    public void setEnDt(String rArg)
    {
        EnDt = rArg;
    }


    public String getEnDt()
    {
        return (EnDt);
    }


    public void clearFee()
    {
        fee = "";
    }


    public void setFee(String rArg)
    {
        fee = rArg;
    }


    public String getFee()
    {
        return (fee);
    }


    public void clearDesnEn()
    {
        desnEn = "";
    }


    public void setDesnEn(String rArg)
    {
        desnEn = rArg;
    }


    public String getDesnEn()
    {
        return (desnEn);
    }


    public void clearDesnZh()
    {
        desnZh = "";
    }


    public void setDesnZh(String rArg)
    {
        desnZh = rArg;
    }


    public String getDesnZh()
    {
        return (desnZh);
    }


    public void clearLongDesn1En()
    {
        longDesn1En = "";
    }


    public void setLongDesn1En(String rArg)
    {
        longDesn1En = rArg;
    }


    public String getLongDesn1En()
    {
        return (longDesn1En);
    }


    public void clearLongDesn2En()
    {
        longDesn2En = "";
    }


    public void setLongDesn2En(String rArg)
    {
        longDesn2En = rArg;
    }


    public String getLongDesn2En()
    {
        return (longDesn2En);
    }


    public void clearLongDesn1Zh()
    {
        longDesn1Zh = "";
    }


    public void setLongDesn1Zh(String rArg)
    {
        longDesn1Zh = rArg;
    }


    public String getLongDesn1Zh()
    {
        return (longDesn1Zh);
    }


    public void clearLongDesn2Zh()
    {
        longDesn2Zh = "";
    }


    public void setLongDesn2Zh(String rArg)
    {
        longDesn2Zh = rArg;
    }


    public String getLongDesn2Zh()
    {
        return (longDesn2Zh);
    }


    public void clearTncEn()
    {
        tncEn = "";
    }


    public void setTncEn(String rArg)
    {
        tncEn = rArg;
    }


    public String getTncEn()
    {
        return (tncEn);
    }


    public void clearTncZh()
    {
        tncZh = "";
    }


    public void setTncZh(String rArg)
    {
        tncZh = rArg;
    }


    public String getTncZh()
    {
        return (tncZh);
    }


    public void clearUsg()
    {
        usg = "";
    }


    public void setUsg(String rArg)
    {
        usg = rArg;
    }


    public String getUsg()
    {
        return (usg);
    }


    public void clearUsgStDt()
    {
        usgStDt = "";
    }


    public void setUsgStDt(String rArg)
    {
        usgStDt = rArg;
    }


    public String getUsgStDt()
    {
        return (usgStDt);
    }


    public void clearUsgEnDt()
    {
        usgEnDt = "";
    }


    public void setUsgEnDt(String rArg)
    {
        usgEnDt = rArg;
    }


    public String getUsgEnDt()
    {
        return (usgEnDt);
    }


    public void clearUsgUnt()
    {
        usgUnt = "";
    }


    public void setUsgUnt(String rArg)
    {
        usgUnt = rArg;
    }


    public String getUsgUnt()
    {
        return (usgUnt);
    }


    public void clearSrvCmmtPrd()
    {
        srvCmmtPrd = "";
    }


    public void setSrvCmmtPrd(String rArg)
    {
        srvCmmtPrd = rArg;
    }


    public String getSrvCmmtPrd()
    {
        return (srvCmmtPrd);
    }


    public void clearSrvPenalty()
    {
        srvPenalty = "";
    }


    public void setSrvPenalty(String rArg)
    {
        srvPenalty = rArg;
    }


    public String getSrvPenalty()
    {
        return (srvPenalty);
    }


    public void clearSrvCmmtRmn()
    {
        srvCmmtRmn = "";
    }


    public void setSrvCmmtRmn(String rArg)
    {
        srvCmmtRmn = rArg;
    }


    public String getSrvCmmtRmn()
    {
        return (srvCmmtRmn);
    }


    public void clearSrvEnt()
    {
        srvEnt = "";
    }


    public void setSrvEnt(String rArg)
    {
        srvEnt = rArg;
    }


    public String getSrvEnt()
    {
        return (srvEnt);
    }


    public void clearRebtEn()
    {
        rebtEn = "";
    }


    public void setRebtEn(String rArg)
    {
        rebtEn = rArg;
    }


    public String getRebtEn()
    {
        return (rebtEn);
    }


    public void clearRebtZh()
    {
        rebtZh = "";
    }


    public void setRebtZh(String rArg)
    {
        rebtZh = rArg;
    }


    public String getRebtZh()
    {
        return (rebtZh);
    }


    public void clearRebtAmt()
    {
        rebtAmt = "";
    }


    public void setRebtAmt(String rArg)
    {
        rebtAmt = rArg;
    }


    public String getRebtAmt()
    {
        return (rebtAmt);
    }


    public void clearMonthlyTermRate()
    {
        monthlyTermRate = "";
    }


    public void setMonthlyTermRate(String rArg)
    {
        monthlyTermRate = rArg;
    }


    public String getMonthlyTermRate()
    {
        return (monthlyTermRate);
    }


    public void clearM2mRate()
    {
        m2mRate = "";
    }


    public void setM2mRate(String rArg)
    {
        m2mRate = rArg;
    }


    public String getM2mRate()
    {
        return (m2mRate);
    }


    public void clearPlnTy()
    {
        plnTy = "";
    }


    public void setPlnTy(String rArg)
    {
        plnTy = rArg;
    }


    public String getPlnTy()
    {
        return (plnTy);
    }


    public void clearPlnSubTy()
    {
        plnSubTy = "";
    }


    public void setPlnSubTy(String rArg)
    {
        plnSubTy = rArg;
    }


    public String getPlnSubTy()
    {
        return (plnSubTy);
    }


    public void clearCntrStDt()
    {
        cntrStDt = "";
    }


    public void setCntrStDt(String rArg)
    {
        cntrStDt = rArg;
    }


    public String getCntrStDt()
    {
        return (cntrStDt);
    }


    public void clearCntrEnDt()
    {
        cntrEnDt = "";
    }


    public void setCntrEnDt(String rArg)
    {
        cntrEnDt = rArg;
    }


    public String getCntrEnDt()
    {
        return (cntrEnDt);
    }


    public void clearCntrDur()
    {
        cntrDur = "";
    }


    public void setCntrDur(String rArg)
    {
        cntrDur = rArg;
    }


    public String getCntrDur()
    {
        return (cntrDur);
    }


    public void clearCmmtInd()
    {
        cmmtInd = "";
    }


    public void setCmmtInd(String rArg)
    {
        cmmtInd = rArg;
    }


    public String getCmmtInd()
    {
        return (cmmtInd);
    }


    public void clearSeasInd()
    {
        seasInd = "";
    }


    public void setSeasInd(String rArg)
    {
        seasInd = rArg;
    }


    public String getSeasInd()
    {
        return (seasInd);
    }


    public void clearSuppInd()
    {
        suppInd = "";
    }


    public void setSuppInd(String rArg)
    {
        suppInd = rArg;
    }


    public String getSuppInd()
    {
        return (suppInd);
    }


    public void clearSummInd()
    {
        summInd = "";
    }


    public void setSummInd(String rArg)
    {
        summInd = rArg;
    }


    public String getSummInd()
    {
        return (summInd);
    }
}
