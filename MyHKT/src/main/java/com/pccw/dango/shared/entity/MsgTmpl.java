/*
    Class for Message Template for Inbox
    
    Keywords
    --------
    $URL: $
    $Rev: $
    $Date: $
    $Author: $
*/

package com.pccw.dango.shared.entity;

import java.io.Serializable;

public class MsgTmpl implements Serializable 
{
    private static final long serialVersionUID = 1598180908175783663L;

    private String                  subjEn;             /* Subject (English)                             */
    private String                  ctntEn;             /* Message Content (English)                     */
    private String                  urlEn;              /* URL (English)                                 */
    private String                  subjZh;             /* Subject (Chinese)                             */
    private String                  ctntZh;             /* Message Content (Chinese)                     */
    private String                  urlZh;              /* URL (Chinese)                                 */
    private String                  pslImg;             /* Is Personal Image?                            */
    private int                     mimgRid;            /* MIMG Rid                                      */
    
    
    public static void main(String rArg[])
    {
        System.out.println(getVer());
        return;
    }
    
    
    public static String getVer()
    {
        return ("$URL:  $, $Rev: $");
    }


    public MsgTmpl()
    {
        initAndClear();
    }

    
    final void initAndClear()
    {
        init();
        clear();
    }


    protected void init()
    {
    }


    public void clear()
    {
        clearSubjEn();
        clearCtntEn();
        clearUrlEn();
        clearSubjZh();
        clearCtntZh();
        clearUrlZh();
        clearPslImg();
        clearMimgRid();
    }


    public MsgTmpl copyFrom(MsgTmpl rSrc)
    {
        setSubjEn(rSrc.getSubjEn());
        setCtntEn(rSrc.getCtntEn());
        setUrlEn(rSrc.getUrlEn());
        setSubjZh(rSrc.getSubjZh());
        setCtntZh(rSrc.getCtntZh());
        setUrlZh(rSrc.getUrlZh());
        setPslImg(rSrc.getPslImg());
        setMimgRid(rSrc.getMimgRid());

        return (this);
    }


    public MsgTmpl copyTo(MsgTmpl rDes)
    {
        rDes.copyFrom(this);
        return (rDes);
    }
    
    
    public MsgTmpl copyMe()
    {
        MsgTmpl                     rDes;

        rDes = new MsgTmpl();
        rDes.copyFrom(this);
        return (rDes);
    }


    public void clearSubjEn()
    {
        subjEn = "";
    }


    public void setSubjEn(String rArg)
    {
        subjEn = rArg;
    }


    public String getSubjEn()
    {
        return (subjEn);
    }

    
    public void clearCtntEn()
    {
        ctntEn = "";
    }


    public void setCtntEn(String rArg)
    {
        ctntEn = rArg;
    }


    public String getCtntEn()
    {
        return (ctntEn);
    }

    
    public void clearUrlEn()
    {
        urlEn = "";
    }


    public void setUrlEn(String rArg)
    {
        urlEn = rArg;
    }


    public String getUrlEn()
    {
        return (urlEn);
    }

    
    public void clearSubjZh()
    {
        subjZh = "";
    }


    public void setSubjZh(String rArg)
    {
        subjZh = rArg;
    }


    public String getSubjZh()
    {
        return (subjZh);
    }

    
    public void clearCtntZh()
    {
        ctntZh = "";
    }


    public void setCtntZh(String rArg)
    {
        ctntZh = rArg;
    }


    public String getCtntZh()
    {
        return (ctntZh);
    }

    
    public void clearUrlZh()
    {
        urlZh = "";
    }


    public void setUrlZh(String rArg)
    {
        urlZh = rArg;
    }


    public String getUrlZh()
    {
        return (urlZh);
    }

    
    public void clearPslImg()
    {
        pslImg = "";
    }


    public void setPslImg(String rArg)
    {
        pslImg = rArg;
    }


    public String getPslImg()
    {
        return (pslImg);
    }

    
    public void clearMimgRid()
    {
        mimgRid = 0;
    }


    public void setMimgRid(int rArg)
    {
        mimgRid = rArg;
    }


    public int getMimgRid()
    {
        return (mimgRid);
    }
}
