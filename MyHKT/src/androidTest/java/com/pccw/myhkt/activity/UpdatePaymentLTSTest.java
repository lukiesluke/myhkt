package com.pccw.myhkt.activity;

import android.content.Context;
import android.content.SharedPreferences;
import androidx.test.platform.app.InstrumentationRegistry;
import androidx.test.rule.ActivityTestRule;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import com.pccw.myhkt.AccountHelper;
import com.pccw.myhkt.R;
import com.pccw.myhkt.base.BaseTest;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static androidx.test.platform.app.InstrumentationRegistry.getInstrumentation;
import static androidx.test.espresso.action.ViewActions.click;

@RunWith(AndroidJUnit4.class)
public class UpdatePaymentLTSTest extends BaseTest {

    private Context context;

    private SharedPreferences shared;
    SharedPreferences.Editor prefEditor;


    @Rule
    public ActivityTestRule<MainMenuActivity> activityTestRule =
            new ActivityTestRule<MainMenuActivity>(MainMenuActivity.class) {
                @Override
                protected void beforeActivityLaunched() {
                    clearSharedPrefs(InstrumentationRegistry.getTargetContext());
                    InstrumentationRegistry.getTargetContext().deleteDatabase("myhkt.db");
                    super.beforeActivityLaunched();
                }
            };

    private void clearSharedPrefs(Context context) {
        SharedPreferences prefs =
                context.getSharedPreferences("myhkt", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.clear();
        editor.commit();
    }

    @Before
    public void setUp() {
        context = getInstrumentation().getTargetContext();
        shared = context.getSharedPreferences(
                "myhkt",
                Context.MODE_PRIVATE);
//        Intents.init();
        clickLeftNavBar();
        clickString(context, R.string.myhkt_LGIF_LOGIN);
        loginTestSR1();
        clickViewInAdapter(R.id.servicelist_listView, 0);
        delay(10000);
        clickString(context, R.string.myhkt_summary_updatePayMethod);
        delay(10000);
    }

    @After
    public void tearDown() {
        clickLeftNavBar();
        clickString(context,R.string.SHLF_LOGOUT);
        clickString(context,R.string.myhkt_btn_yes);
    }

    @Test
    public void checkDisplayUI() {
        //check lang ng display ui tinatamad pa ko gawin wait lang
    }

    @Test
    public void changeButtonClick() {
        clickString(context, R.string.BUPLTF_BTN_CHANGE);
        delay(10000);
        clickLeftNavBar();
        delay(10000);
        clickLeftNavBar();
        clickLeftNavBar();
    }

    @Test
    public void backButtonClick() {
        clickString(context, R.string.btn_back);
        clickLeftNavBar();
    }

    @Test
    public void backButtonNavigationClick() {
        clickLeftNavBar();
    }

    @Test
    public void swipeRightExecute() {
        swipeRightView(R.id.billinfolts_viewpager);
        delay(10000);
        clickLeftNavBar();
        clickLeftNavBar();
    }

    private void loginTestSR1() {
        loginMyCredentialsUAT(AccountHelper.getUATAccount6().getUsername(), AccountHelper.getUATAccount6().getPassword(), shared);
        delay(3000);
        gotoMainMenuItem(0);
    }
}
