package com.pccw.myhkt;

import com.pccw.myhkt.enums.HKTDataType;

import org.junit.Test;

import java.util.Date;

import static org.junit.Assert.*;

public class UtilsTest {

    @Test
    public void testDataFormatterEntitlementTest1() {
        assertEquals(Utils.formattedDataByMBStr("2 ,100", HKTDataType.HKTDataTypeEntitled, true), "2.1GB");
    }

    @Test
    public void testDataFormatterEntitlementTest2() {
        assertEquals(Utils.formattedDataByMBStr("950" , HKTDataType.HKTDataTypeEntitled, true),"950MB");
    }
    @Test
    public void testDataFormatterEntitlementTest3() {
        assertEquals(Utils.formattedDataByMBStr("25,600" , HKTDataType.HKTDataTypeEntitled, true),"25.0GB");
    }

    @Test
    public void testDataFormatterEntitlementTest4() {
        assertEquals(Utils.formattedDataByMBStr("0.16" , HKTDataType.HKTDataTypeEntitled, true),"164KB");
    }

    @Test
    public void testDataFormatterUsedTest1() {
        assertEquals(Utils.formattedDataByMBStr("2,754.56" , HKTDataType.HKTDataTypeUsed, true),"2.6GB");
    }

    @Test
    public void testDataFormatterUsedTest2() {
        assertEquals(Utils.formattedDataByMBStr("950.9" , HKTDataType.HKTDataTypeUsed, true),"950MB");
    }
    
    @Test
    public void testDataFormatterUsedTest3() {
        assertEquals(Utils.formattedDataByMBStr("25,600" , HKTDataType.HKTDataTypeUsed, true),"25.0GB");
    }
    @Test
    public void testDataFormatterUsedTest4(){
        assertEquals(Utils.formattedDataByMBStr("0.16" , HKTDataType.HKTDataTypeUsed, true),"163KB");
    }

    @Test
    public void testDataFormatterRemainingTest1() {
        assertEquals(Utils.formattedDataByMBStr("2,590.72" , HKTDataType.HKTDataTypeRemaining, true),"2.6GB");
    }

    @Test
    public void testDataFormatterRemainingTest2() {
        assertEquals(Utils.formattedDataByMBStr("949.1" , HKTDataType.HKTDataTypeRemaining, true),"950MB");
    }

    @Test
    public void testDataFormatterRemainingTest3() {
        assertEquals(Utils.formattedDataByMBStr("25,600" , HKTDataType.HKTDataTypeRemaining, true),"25.0GB");
    }
    @Test
    public void testDataFormatterRemainingTest4() {
        assertEquals(Utils.formattedDataByMBStr("0.14" , HKTDataType.HKTDataTypeRemaining, true),"144KB");
    }
    @Test
    public void testDataFormatterWithoutUnitTest1() {
        assertEquals(Utils.formattedDataByMBStr("0.14" , HKTDataType.HKTDataTypeUsed , false),"0.0");
    }
    @Test
    public void testDataFormatterWithoutUnitTest2() {
        assertEquals(Utils.formattedDataByMBStr("1" , HKTDataType.HKTDataTypeUsed , false),"0.0");
    }
    @Test
    public void testDataFormatterWithoutUnitTest3() {
        assertEquals(Utils.formattedDataByMBStr("1023" , HKTDataType.HKTDataTypeUsed , false),"0.9");
    }
    @Test
    public void testDataFormatterWithoutUnitTest3_1() {
        assertEquals(Utils.formattedDataByMBStr("1023" , HKTDataType.HKTDataTypeRemaining , false),"1.0");
    }
    @Test
    public void testDataFormatterWithoutUnitTest4() {
        assertEquals(Utils.formattedDataByMBStr("499" , HKTDataType.HKTDataTypeRemaining , false),"0.5");
    }
    @Test
    public void testDataFormatterWithoutUnitTest5() {
        assertEquals(Utils.formattedDataByMBStr("513" , HKTDataType.HKTDataTypeRemaining , false),"0.6");
    }

    @Test
    public void testDataFormatterRemaining() {
        // This is an example of a functional test case. user:greatchina1
        assertEquals(Utils.formattedDataByMBStr("79304.0400390625" , HKTDataType.HKTDataTypeRemaining , true),"77.5GB");
    }
    @Test
    public void testDataFormatterRemaining_2() {
        // This is an example of a functional test case. user: greatchina3
        assertEquals(Utils.formattedDataByMBStr("27329.2236328125" , HKTDataType.HKTDataTypeRemaining , true),"26.7GB");
    }

    @Test
    public void testDataFormatterEmpty() {
        // This is an example of a functional test case. user: greatchina3
        assertEquals(Utils.formattedDataByMBStr("" , HKTDataType.HKTDataTypeRemaining , true),"0KB");
    }

    @Test
    public void testDataFormatterNullPointer() {
        // This is an example of a functional test case. user: greatchina3
        assertEquals(Utils.formattedDataByMBStr(null , HKTDataType.HKTDataTypeRemaining , false),"0.0");
    }

    @Test
    public void testDataFormatterSpace() {
        // This is an example of a functional test case. user: greatchina3
        assertEquals(Utils.formattedDataByMBStr("1 024" , HKTDataType.HKTDataTypeRemaining , false),"1.0");
    }

    @Test
    public void testDataFormatterTopUp() {
        // This is an example of a functional test case. user: greatchina3
        assertEquals(Utils.formattedDataByMBStr("1024" , HKTDataType.HKTDataTypeRemaining , false),"1.0");
    }

    @Test
    public void testDaysRemaining() {
        assertEquals(Utils.daysRemaining(new Date(Utils.getCurrentDate())), 2);
    }

    @Test
    public void testDaysRemainingZeo() {
        assertEquals(Utils.daysRemaining(new Date("11/18/2020")), 0);
    }


}
