package com.pccw.myhkt.cell.model;

import android.view.View.OnClickListener;
import android.view.ViewGroup;

import com.pccw.biz.myhkt.R;

public class TwoTextBtnCell extends Cell{

	private OnClickListener onClickListener = null;


	private String btnText =""; 
	private int btnSizeDelta;
	private int btnColorId;
	private int btnWidth = ViewGroup.LayoutParams.WRAP_CONTENT;
	
	


	public TwoTextBtnCell(OnClickListener onClickListener, String title, String content, String btnText, int drawId) {
		super();
		type = Cell.TWOTEXTBTN;
		
		titleColorId = R.color.hkt_txtcolor_grey;
		contentColorId = R.color.hkt_textcolor;
		btnColorId = R.color.hkt_textcolor;
		titleSizeDelta = 0;
		contentSizeDelta = 0;
		btnSizeDelta = 0;
		
		this.title = title;
		this.content = content;
		this.btnText =  btnText;
		this.onClickListener = onClickListener;
		this.draw = drawId;
	}
	
	public OnClickListener getOnClickListener() {
		return onClickListener;
	}

	public void setOnClickListener(OnClickListener onClickListener) {
		this.onClickListener = onClickListener;
	}
	public String getBtnText() {
		return btnText;
	}

	public void setBtnText(String btnText) {
		this.btnText = btnText;
	}

	public int getBtnSizeDelta() {
		return btnSizeDelta;
	}

	public void setBtnSizeDelta(int btnSizeDelta) {
		this.btnSizeDelta = btnSizeDelta;
	}

	public int getBtnColorId() {
		return btnColorId;
	}

	public void setBtnColorId(int btnColorId) {
		this.btnColorId = btnColorId;
	}
	public int getBtnWidth() {
		return btnWidth;
	}

	public void setBtnWidth(int btnWidth) {
		this.btnWidth = btnWidth;
	}
	

}