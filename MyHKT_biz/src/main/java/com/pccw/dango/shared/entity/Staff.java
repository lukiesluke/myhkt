/*
    Staff (likely for API) in Dango
    
        Staff should also associate with the "Phylum".
    
    Which means she/he can access those Customers (CUST)
    with the Same Phylum.
    
    
    Keywords
    --------
    $URL: $
    $Rev: $
    $Date: $
    $Author: $
*/

package com.pccw.dango.shared.entity;

import java.io.Serializable;


public class Staff extends BaseUserEx implements Serializable
{
    private static final long serialVersionUID = -5242508445378007781L;
    
    private String                  staffId;
    private String                  phylum;

    
    public static void main(String rArg[])
    {
        System.out.println(getVer());
        return;
    }
    
    
    public static String getVer()
    {
        return ("$URL: $, $Rev: $");
    }
    
    
    private Staff()
    {
        /* Constructor Prohibited (I need the StaffId) */
        super();
    }
    
    
    public Staff(String rStaffId, String rPhylum)
    {
        this();
        
        staffId = rStaffId;
        phylum  = rPhylum;
    }
    
    
    public String getId()
    {
        return (staffId);
    }
    
    public String getPhylum()
    {
        return (phylum);
    }
  
    
    public String getType()
    {
        return (TY_STFF);
    }
    
    
    public String getRole()
    {
        return ("");
    }
    
    
    public String[] getRghtAry()
    {
        return (new String[0]);
    }
    
    
    public boolean isAuth(String rRght)
    {
        return (false);
    }
}
