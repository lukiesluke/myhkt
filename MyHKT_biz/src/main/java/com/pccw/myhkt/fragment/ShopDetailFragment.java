package com.pccw.myhkt.fragment;

import java.lang.Thread.UncaughtExceptionHandler;

import android.app.Activity;
import android.os.Bundle;
import androidx.fragment.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.MapsInitializer;
import com.pccw.dango.shared.entity.ShopRec;
import com.pccw.myhkt.APIsResponse;
import com.pccw.myhkt.ClnEnv;
import com.pccw.biz.myhkt.R;
import com.pccw.myhkt.Utils;
import com.pccw.myhkt.lib.ui.AAQuery;
import com.pccw.myhkt.mapviewballoons.MapFragmentD;

public class ShopDetailFragment extends BaseShopFragment implements MapFragmentD.OnMapEventListener{
	private ShopDetailFragment me;
	private AAQuery aq;
	private int					   googlePlayServiceState;
	
	// Map v2
	MapFragmentD		mFRaFragment;
	
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		me = this;

		TAG = "ShopDetailFragment";

		View fragmentLayout = inflater.inflate(R.layout.fragment_shopdetail, container, false);
		return fragmentLayout;
	}
	
	@Override
	public void onStart() {
		super.onStart();
		//Screen Tracker
	
		init();
	}
	
	@Override
	public void onPause(){
		super.onPause();
//		callback.setPause(true);
	}
	
	@Override
	public void onResume(){
		super.onResume();
		//map
		if ((googlePlayServiceState == ConnectionResult.SUCCESS) && mFRaFragment != null) {
			mFRaFragment.fetchShopList(new ShopRec[] { callback_main.getShopRec() });

			try {
				mFRaFragment.gotoPOI(callback_main.getShopRec().latitude, callback_main.getShopRec().longitude);
			} catch (Exception e) {
				// mFraFragment / shopRec might be null
				e.printStackTrace();
			}
		}
	}
	
	private void init() {
		aq = new AAQuery(getActivity());
		int padding = (int) getResources().getDimension(R.dimen.padding_screen);

		aq.id(R.id.shopdetail_shopname).getTextView().setCompoundDrawablesWithIntrinsicBounds(R.drawable.rightarrow_small, 0, 0, 0);
		aq.id(R.id.shopdetail_shopname).getTextView().setCompoundDrawablePadding(padding);
		aq.id(R.id.shopdetail_shopname).text((isZh) ? callback_main.getShopRec().name_zh : callback_main.getShopRec().name_en);
		aq.id(R.id.shopdetail_shopname).textSize(getResources().getInteger(R.integer.textsize_default_int));
		aq.id(R.id.shopdetail_address).textColorId(R.color.hkt_textcolor);
		aq.id(R.id.shopdetail_address).text((isZh) ? callback_main.getShopRec().addr_zh : callback_main.getShopRec().addr_en);
		aq.id(R.id.shopdetail_address).getTextView().append(callback_main.getShopRec().shop_cat.startsWith("Y") ? "\n\n" + getString(R.string.myhkt_shop_smartliv_rmk) : "");
		aq.id(R.id.shopdetail_tel).textColorId(R.color.hkt_textcolor);
		aq.id(R.id.shopdetail_tel).text(callback_main.getShopRec().ct_num);
		aq.id(R.id.shopdetail_bushrs).textColorId(R.color.hkt_textcolor);
		aq.id(R.id.shopdetail_bushrs).text((isZh) ? callback_main.getShopRec().bz_hr_zh : callback_main.getShopRec().bz_hr_en);
		googlePlayServiceState = GooglePlayServicesUtil.isGooglePlayServicesAvailable(getActivity().getApplicationContext());

		if (googlePlayServiceState == ConnectionResult.SUCCESS) {
			
			
			// Initialize Map
			FragmentTransaction mTransaction = getChildFragmentManager().beginTransaction();
			mFRaFragment = new MapFragmentD();

			Bundle args = new Bundle();
			args.putBoolean("myLoc", false);
			args.putBoolean("isZh", isZh);
			mFRaFragment.setArguments(args);

			mTransaction.add(R.id.shopdetail_mapview, mFRaFragment);
			mTransaction.commit();

			try {
				MapsInitializer.initialize(getActivity());
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else {
			// google play service not available, displaying suitable error message
//			((com.pccw.myhkt.mapviewballoons.CustomizeFrameLayout) findViewById(R.id.shopmap_mapview)).setVisibility(View.GONE);

			// ConnectionResult.SERVICE_MISSING,
			// ConnectionResult.SERVICE_VERSION_UPDATE_REQUIRED,
			// ConnectionResult.SERVICE_DISABLED,
			// ConnectionResult.SERVICE_INVALID
			GooglePlayServicesUtil.getErrorDialog(googlePlayServiceState, getActivity(), -1).show();
		}
		

	}	

	@Override
	public void onSuccess(APIsResponse response) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onFail(APIsResponse response) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onFirstFix() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onShopDetail(ShopRec shop) {
		// TODO Auto-generated method stub
		
	}

}
