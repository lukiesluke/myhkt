/*
    Class for Contact Information
    
    Keywords
    --------
    $URL: svn://10.87.120.207/dango/rel/v16.0/src/com/pccw/dango/shared/entity/Ctac.java $
    $Rev: 844 $
    $Date: 2016-10-17 11:54:01 +0800 (¶g¤@, 17 ¤Q¤ë 2016) $
    $Author: upd $
*/

package com.pccw.dango.shared.entity;

import java.io.Serializable;

import com.pccw.dango.shared.tool.MyTool;
import com.pccw.dango.shared.tool.RC;
import com.pccw.wheat.shared.tool.Reply;
import com.pccw.wheat.shared.tool.Tool;


public class Ctac implements Serializable 
{
    private static final long serialVersionUID = -3051257660169455743L;
    
    private BomCust                 bomCust;            /* BOM Customer Info                             */
    private String                  officeNum;          /* Office Number                                 */
    private String                  homeNum;            /* Home Number                                   */
    private String                  mobile;             /* Mobile Number                                 */
    private String                  email;              /* Email                                         */

    public static final int         MAX_EMAIL = 40;

    
    public static void main(String rArg[])
    {
        System.out.println(getVer());
        return;
    }
    
    
    public static String getVer()
    {
        return ("$URL: svn://10.87.120.207/dango/rel/v16.0/src/com/pccw/dango/shared/entity/Ctac.java $, $Rev: 844 $");
    }

    
    public Ctac()
    {
        initAndClear();
    }

    
    final void initAndClear()
    {
        init();
        clear();
    }


    protected void init()
    {
    }


    public void clear()
    {
        clearBomCust();
        clearOfficeNum();
        clearHomeNum();
        clearMobile();
        clearEmail();
    }


    public Ctac copyFrom(Ctac rSrc)
    {
        setBomCust(rSrc.getBomCust());
        setOfficeNum(rSrc.getOfficeNum());
        setHomeNum(rSrc.getHomeNum());
        setMobile(rSrc.getMobile());
        setEmail(rSrc.getEmail());

        return (this);
    }


    public Ctac copyTo(Ctac rDes)
    {
        rDes.copyFrom(this);
        return (rDes);
    }
    
    
    public Ctac copyMe()
    {
        Ctac                        rDes;

        rDes = new Ctac();
        rDes.copyFrom(this);
        return (rDes);
    }


    public void clearBomCust()
    {
        bomCust = new BomCust();
    }


    public void setBomCust(BomCust rArg)
    {
        bomCust = rArg;
    }


    public BomCust getBomCust()
    {
        return (bomCust);
    }


    public void clearOfficeNum()
    {
        officeNum = "";
    }


    public void setOfficeNum(String rArg)
    {
        officeNum = rArg;
    }


    public String getOfficeNum()
    {
        return (officeNum);
    }


    public void clearHomeNum()
    {
        homeNum = "";
    }


    public void setHomeNum(String rArg)
    {
        homeNum = rArg;
    }


    public String getHomeNum()
    {
        return (homeNum);
    }


    public void clearMobile()
    {
        mobile = "";
    }


    public void setMobile(String rArg)
    {
        mobile = rArg;
    }


    public String getMobile()
    {
        return (mobile);
    }


    public void clearEmail()
    {
        email = "";
    }


    public void setEmail(String rArg)
    {
        email = rArg;
    }


    public String getEmail()
    {
        return (email);
    }
    
    
    public Reply validate()
    {
        Reply                       rRR;
        
        rRR = validOfficeNum();
        if (rRR.isSucc()) rRR = validHomeNum();
        if (rRR.isSucc()) rRR = validMobile();
        if (rRR.isSucc()) rRR = validEmail();
        if (rRR.isSucc()) rRR = validAllNil();
        
        return (rRR);
    }
    
    
    public Reply validAllNil()
    {
        /*
            Likely pass this.
            Since all fields must be supplied
        */
        
        if (getOfficeNum().trim().length() > 0 ||
            getHomeNum().trim().length() > 0   ||
            getMobile().trim().length() > 0    ||
            getEmail().trim().length() > 0) {
            return (Reply.getSucc());
        }
        
        return (new Reply(RC.CTAC_REQ_INPUT));
    }
    
    
    public Reply validOfficeNum()
    {
        Reply                       rRR;
        
        if (getOfficeNum().trim().length() == 0) {
            return (new Reply(RC.CTAC_REQ_OFCNUM));
        }
        
        if (getOfficeNum().length() > 0) {
            if (!MyTool.isVaDirNum(getOfficeNum())) {
                return (new Reply(RC.CTAC_IVOFCNUM)); 
            }
        }
    
        return (Reply.getSucc());
    }
    
    
    public Reply validHomeNum()
    {
        Reply                       rRR;

        if (getHomeNum().trim().length() == 0) {
            return (new Reply(RC.CTAC_REQ_HMNUM));
        }
        
        if (getHomeNum().length() > 0) {
            if (!MyTool.isVaDirNum(getHomeNum())) {
                return (new Reply(RC.CTAC_IVHMNUM)); 
            }
        }
    
        return (Reply.getSucc());
    }
        
    
    public Reply validMobile()
    {
        Reply                       rRR;

        if (getMobile().trim().length() == 0) {
            return (new Reply(RC.CTAC_REQ_MOB));
        }
        
        if (getMobile().length() > 0) {
            if (!MyTool.isVaMob(getMobile())) {
                return (new Reply(RC.CTAC_IVMOB)); 
            }
        }
        
        if (getBomCust().getLob().equals(SubnRec.LOB_PCD)) {
            if (getMobile().length() == 0) {
                return (new Reply(RC.CTAC_REQ_MOB));
            }
        }

        return (Reply.getSucc());
    }

    
    public Reply validEmail()
    {
        Reply                       rRR;

        if (getEmail().trim().length() == 0) {
            return (new Reply(RC.CTAC_REQ_EMAIL));
        }

        if (getEmail().length() > 0) {
            if (!Tool.isASC(getEmail())) {
                return (new Reply(RC.CTAC_IVCTMAIL));
            }
            
            if (getEmail().length() > MAX_EMAIL) {
                return (new Reply(RC.CTAC_ILCTMAIL));
            }
            
            if (!MyTool.isVaEmail(getEmail())) {
                return (new Reply(RC.CTAC_IVCTMAIL));
            }
        }
        
        return (Reply.getSucc());
    }
}
