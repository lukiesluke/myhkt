package com.pccw.myhkt.dialogs;

import android.app.Activity;
import androidx.appcompat.app.AppCompatDialogFragment;

public abstract class BaseDialogFragment<T> extends AppCompatDialogFragment {
    private T mActivityInstance;

    public final T getActivityInstance() {
        return mActivityInstance;
    }

    @Override
    public void onAttach(Activity activity) {
        mActivityInstance = (T) activity;
        super.onAttach(activity);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mActivityInstance = null;
    }
}