package com.pccw.myhkt.fragment;

import java.lang.Thread.UncaughtExceptionHandler;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.RelativeLayout;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.pccw.dango.shared.cra.CtacCra;
import com.pccw.dango.shared.cra.LgiCra;
import com.pccw.dango.shared.entity.BomCust;
import com.pccw.dango.shared.entity.Ctac;
import com.pccw.dango.shared.entity.QualSvee;
import com.pccw.dango.shared.entity.SubnRec;
import com.pccw.dango.shared.entity.SveeRec;
import com.pccw.dango.shared.tool.DirNum;
import com.pccw.dango.shared.tool.RC;
import com.pccw.myhkt.APIsManager;
import com.pccw.myhkt.APIsResponse;
import com.pccw.myhkt.ClnEnv;
import com.pccw.myhkt.DialogHelper;
import com.pccw.myhkt.InterpretRCManager;
import com.pccw.biz.myhkt.R;
import com.pccw.myhkt.Utils;
import com.pccw.myhkt.activity.BaseActivity;
import com.pccw.myhkt.fragment.MyProfLoginMainFragment.OnMyProfLoginListener;
import com.pccw.myhkt.lib.ui.AAQuery;
import com.pccw.myhkt.lib.ui.HKTButton;
import com.pccw.myhkt.lib.ui.ProfileContactItem;
import com.pccw.wheat.shared.tool.Reply;
/************************************************************************
File       : ProfileContactFragment.java
Desc       : View for Change MyHKT ContactInfo
Name       : ProfileContactFragment
Created by : Andy Wong
Date       : 12/11/2015

Change History:
Date       Modified By			Description
---------- ----------------	-------------------------------
12/11/2015 Andy Wong			- First draft 
 *************************************************************************/

public class ProfileContactFragment extends BaseFragment{
	private ProfileContactFragment me;
	private Activity act;
	private AAQuery aq;

	private SveeRec sveeRec = null;

	// CallBack
	private OnMyProfLoginListener		callback;

	private Ctac                    drgCtac;                               
	private Ctac                    imsCtac;                                
	private Ctac                    mobCtac;  

	private CtacCra                 ctacCra; 
	private CtacCra 				toSrvCtacCra;

	private ProfileContactItem     profileContactItemDrg;
	private ProfileContactItem     profileContactItemIms;
	private ProfileContactItem     profileContactItemMob;

	private CheckBox checkBoxDrg;
	private CheckBox checkBoxIms;
	private CheckBox checkBoxMob;
	private Boolean isApplyShown = false;

	private final int 		PAGE_NO = 2;

	private boolean isChangePwdMode = false;
	private String TAG = "MyProfContactFragment";

	private Boolean isDrgNull = false;
	private Boolean isImsNull = false;
	private Boolean isMobNull = false;

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		try {
			callback = (OnMyProfLoginListener) activity;
		} catch (ClassCastException e) {
			throw new ClassCastException(activity.toString() + " must implement OnMyProfLoginListener");
		}
	}

	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		me = this;

		if (debug) Log.i(TAG, "onCreateView");
		View fragmentLayout = inflater.inflate(R.layout.fragment_myprofcontact, container, false);
		aq = new AAQuery(fragmentLayout);
		act = getActivity();
		initData();
		return fragmentLayout;
	}

	protected void initData(){
		super.initData();
		String mobPfx = ClnEnv.getPref(getActivity(), "mobPfx", "51,52,53,54,55,56,57,59,6,9,84,85,86,87,89");
		String ltsPfx = ClnEnv.getPref(getActivity(), "ltsPfx", "2,3,81,82,83");
		DirNum.getInstance(mobPfx, ltsPfx);
	}

	
	protected void initUI(){
		//remark
		aq.id(R.id.myprofcontact_remark).text(R.string.myhkt_myprofcontact_remark).textSize(AAQuery.getDefaultTextSize()-2).getView().setPadding(basePadding, 0, basePadding, 0);

        profileContactItemDrg = (ProfileContactItem) aq.id(R.id.myprofcontact_drg).getView();
		profileContactItemIms = (ProfileContactItem) aq.id(R.id.myprofcontact_ims).getView();    
		profileContactItemMob  = (ProfileContactItem) aq.id(R.id.myprofcontact_mob).getView();
		profileContactItemMob.setVisibility(View.GONE);

		profileContactItemMob.isCSimOnly = Utils.isCsimOnly();
		
		profileContactItemDrg.setPadding(basePadding, 0, basePadding, 0);
		profileContactItemIms.setPadding(basePadding, 0, basePadding, 0);
		profileContactItemMob.setPadding(basePadding, 0, basePadding, 0);

		profileContactItemDrg.initView(getActivity(), getResString(R.string.comm_CTAF_LT_LOB), "", "", "", "");
		profileContactItemIms.initView(getActivity(), getResString(R.string.comm_CTAF_IM_LOB), "", "", "", "");
		profileContactItemMob.initView(getActivity(), getResString(R.string.CTAF_MB_LOB), "", "", "", "");
		profileContactItemMob.setVisibility(View.GONE);	
		//Btn layout
		((HKTButton)aq.id(R.id.myprofcontact_btn_update).getView()).initViews(act, getResString(R.string.MYHKT_BTN_UPDATECONTACTINFO));
		aq.id(R.id.myprofcontact_btn_update).width(deviceWidth /2- basePadding ,false);
		aq.norm2TxtBtns(R.id.myprofcontact_btn_cancel, R.id.myprofcontact_btn_apply, getResString(R.string.MYHKT_BTN_CANCEL),  getResString(R.string.MYHKT_BTN_UPDATE));
		aq.id(R.id.myprofcontact_btn_update).clicked(this, "onClick");
		aq.id(R.id.myprofcontact_btn_cancel).clicked(this, "onClick");
		// aq.id(R.id.myprofcontact_btn_apply).clicked(this, "onClick"); For CA

		//init input field
		aq.id(R.id.myprofcontact_mob_layout).getView().setPadding(basePadding, extralinespace/2, basePadding, extralinespace/2);
		aq.id(R.id.myprofcontact_mob_icon).getView().setPadding(0, 0,extralinespace/2,0);
		aq.id(R.id.myprofcontact_mob_icon).image(R.drawable.b_contact_mobileno);
		aq.normEditText(R.id.myprofcontact_mob_et, "",getResString(R.string.CTAF_IM_MOBPH)); 
		aq.id(R.id.myprofcontact_mob_et).getView().setPadding(extralinespace/2, 0,0,0);
		aq.id(R.id.myprofcontact_mob_et).getEditText().setInputType(InputType.TYPE_CLASS_PHONE);
		aq.maxLength(R.id.myprofcontact_mob_et, getResInt(R.integer.CONST_MAX_MOBNUM));

		aq.id(R.id.myprofcontact_email_layout).getView().setPadding(basePadding, 0, basePadding, extralinespace/2);
		aq.id(R.id.myprofcontact_email_icon).getView().setPadding(0, 0,extralinespace/2,0);
		aq.id(R.id.myprofcontact_email_icon).image(R.drawable.b_contact_email);
		aq.normEditText(R.id.myprofcontact_email_et, "", getResString(R.string.CTAF_EMAIL)); 
		aq.id(R.id.myprofcontact_email_et).getView().setPadding(extralinespace/2, 0,0,0);
		aq.id(R.id.myprofcontact_email_et).getEditText().setInputType(InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);
		aq.maxLength(R.id.myprofcontact_email_et, getResInt(R.integer.CONST_MAX_EMAIL));

		aq.id(R.id.myprofcontact_daymob_layout).getView().setPadding(basePadding, 0, basePadding, extralinespace/2);
		aq.id(R.id.myprofcontact_daymob_icon).getView().setPadding(0, 0,extralinespace/2,0);
		aq.id(R.id.myprofcontact_daymob_icon).image(R.drawable.b_contact_daycontact);
		aq.normEditText(R.id.myprofcontact_daymob_et, "", getResString(R.string.CTAF_IM_OFCPH)); 
		aq.id(R.id.myprofcontact_daymob_et).getView().setPadding(extralinespace/2, 0,0,0);	
		aq.id(R.id.myprofcontact_daymob_et).getEditText().setInputType(InputType.TYPE_CLASS_PHONE);
		aq.maxLength(R.id.myprofcontact_daymob_et, getResInt(R.integer.CONST_MAX_PHONENUM));

		aq.id(R.id.myprofcontact_nightmob_layout).getView().setPadding(basePadding, 0, basePadding, extralinespace/2);
		aq.id(R.id.myprofcontact_nightmob_icon).getView().setPadding(0, 0,extralinespace/2,0);
		aq.id(R.id.myprofcontact_nightmob_icon).image(R.drawable.b_contact_nightcontact);
		aq.normEditText(R.id.myprofcontact_nightmob_et, "", getResString(R.string.CTAF_IM_HOMEPH)); 
		aq.id(R.id.myprofcontact_nightmob_et).getView().setPadding(extralinespace/2, 0,0,0);
		aq.id(R.id.myprofcontact_nightmob_et).getEditText().setInputType(InputType.TYPE_CLASS_PHONE);
		aq.maxLength(R.id.myprofcontact_nightmob_et, getResInt(R.integer.CONST_MAX_PHONENUM));

		//Apply layout
		aq.normTextGrey(R.id.myprofcontact_text, getResString(R.string.myhkt_myprofcontact_text));
		aq.id(R.id.myprofcontact_text).getTextView().setPadding(basePadding, 0,basePadding,0);
		aq.id(R.id.myprofcontact_text).getTextView().setTextSize(AAQuery.getDefaultTextSize() -2);

		aq.id(R.id.myprofcontact_checkbox_layout).getView().setPadding(basePadding, 0,basePadding,0);
		aq.normTextGrey(R.id.myprofcontact_drg_txt, getResString(R.string.CTAF_LT_LOB));
		aq.id(R.id.myprofcontact_drg_txt).getTextView().setTextSize(AAQuery.getDefaultTextSize() -2);

		aq.normTextGrey(R.id.myprofcontact_mob_txt, getResString(R.string.CTAF_MB_LOB));
		aq.id(R.id.myprofcontact_mob_txt).getTextView().setTextSize(AAQuery.getDefaultTextSize() -2);

		aq.normTextGrey(R.id.myprofcontact_ims_txt, getResString(R.string.CTAF_IM_LOB));
		aq.id(R.id.myprofcontact_ims_txt).getTextView().setTextSize(AAQuery.getDefaultTextSize() -2);

		checkBoxDrg = (CheckBox)aq.id(R.id.myprofcontact_drg_checkbox).getView(); 
		checkBoxIms = (CheckBox)aq.id(R.id.myprofcontact_ims_checkbox).getView();
		checkBoxMob = (CheckBox)aq.id(R.id.myprofcontact_mob_checkbox).getView();
		checkBoxDrg.setButtonDrawable(R.drawable.checkbox);
		checkBoxIms.setButtonDrawable(R.drawable.checkbox);
		checkBoxMob.setButtonDrawable(R.drawable.checkbox);
		
		aq.id(R.id.myprofcontact_btn_update).visibility(View.GONE);
		setApplyLayout();		

		//		aq.line(R.id.myprofcontact_line1,R.id.myprofcontact_line2,R.id.myprofcontact_line3);
	}

	public void onClick(View v) {
		switch(v.getId()){
		case R.id.myprofcontact_btn_update:
			isApplyShown = true;
			setApplyLayout();
			break;
		case R.id.myprofcontact_btn_cancel:
			isApplyShown = false;
			setApplyLayout();
			break;
		case R.id.myprofcontact_btn_apply:
			Utils.closeSoftKeyboard(getActivity(), v);
			Reply reply = loadInput();
			if (!reply.isSucc()) {
				displayDialog(InterpretRCManager.interpretRC_CtacMdu(getActivity(), reply));
				//							displayDialog(interpretRC_CtacMdu(reply));
			} else if (checkSelectedList()) {
				displayDialog(Utils.getString(me.getActivity(), R.string.CTAM_SEL_TO_UPD));
			} else {
				updateConfirmDialog();
			}
			break;
		}
	}

	public void setApplyLayout(){
		aq.id(R.id.myprofcontact_btns_update_layout).visibility(isApplyShown? View.GONE : View.VISIBLE);
		aq.id(R.id.myprofcontact_btns_apply_layout).visibility(isApplyShown? View.VISIBLE : View.GONE);
		aq.id(R.id.myprofcontact_apply_layout).visibility(isApplyShown? View.VISIBLE : View.GONE);
		//As btns layout may be gone , scrollView need to change its dependency
		RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) aq.id(R.id.myprofcontact_scrollview).getView().getLayoutParams();
		params.addRule(RelativeLayout.ABOVE, isApplyShown ? R.id.myprofcontact_btns_apply_layout : R.id.myprofcontact_btns_update_layout);
		aq.id(R.id.myprofcontact_scrollview).getView().setLayoutParams(params);
	}


	/************************************
	 * 
	 * Read data
	 * 
	 ************************************/
	public final void refresh() {
		//Screen Tracker

		// init layout

		isApplyShown = false;
		setApplyLayout();

		profileContactItemDrg.setText("","","","");	
		profileContactItemIms.setText("","","","");
		profileContactItemMob.setText("","","","");

		if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.HONEYCOMB) {
			aq.id(R.id.myprofcontact_drg_txt).getTextView().setAlpha((float) 0.4);
		} 
		checkBoxDrg.setEnabled(false);
		aq.id(R.id.myprofcontact_drg_txt).getTextView().setEnabled(false);
		//		aq.id(R.id.myprofcontact_drg_txt).textColorId(R.color.hkt_txtcolor_grey_disable);

		if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.HONEYCOMB) {
			aq.id(R.id.myprofcontact_ims_txt).getTextView().setAlpha((float) 0.4);
		} 
		checkBoxIms.setEnabled(false);
		aq.id(R.id.myprofcontact_ims_txt).getTextView().setEnabled(false);
		//		aq.id(R.id.myprofcontact_ims_txt).textColorId(R.color.hkt_txtcolor_grey_disable);	

		if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.HONEYCOMB) {
			aq.id(R.id.myprofcontact_mob_txt).getTextView().setAlpha((float) 0.4);
		} 
		checkBoxMob.setEnabled(false);
		aq.id(R.id.myprofcontact_mob_txt).getTextView().setEnabled(false);
		//		aq.id(R.id.myprofcontact_mob_txt).textColorId(R.color.hkt_txtcolor_grey_disable);	

		if (ClnEnv.getQualSvee().getBomCustAry().length > 0)  {		
			getContactInfo();
		} else {			
			// Case: No Subscription Record, then hide the Update button
			aq.id(R.id.myprofcontact_btns_update_layout).visibility(View.GONE);
			aq.id(R.id.myprofcontact_btns_apply_layout).visibility(View.GONE);

		}


	}


	private final void getContactInfo() {
		// readCtac	
		ctacCra = new CtacCra();
		ctacCra.setILoginId(ClnEnv.getQualSvee().getSveeRec().loginId);
		// preparing the highlighted inputs before calling readCtac
		prepareContact(ClnEnv.getQualSvee());
		ctacCra.setIMobCtac(mobCtac);
		ctacCra.setIImsCtac(imsCtac);
		ctacCra.setIDrgCtac(drgCtac);
		isDrgNull = drgCtac == null;
		isImsNull = imsCtac == null;
		isMobNull = mobCtac == null;

		APIsManager.doReadCtInfo(this, ctacCra);		
	}

	// preparing the highlighted inputs before calling readCtac
	private final void prepareContact(QualSvee rQualSvee) {

		mobCtac = buildContact(rQualSvee, SubnRec.LOB_MOB);
		if (mobCtac == null) {
			mobCtac = buildContact(rQualSvee, SubnRec.LOB_IOI);
		}

		imsCtac = buildContact(rQualSvee, SubnRec.LOB_PCD);
		if (imsCtac == null) {
			imsCtac = buildContact(rQualSvee, SubnRec.LOB_TV);
		}

		drgCtac = buildContact(rQualSvee, SubnRec.LOB_LTS);
	}

	private final Ctac buildContact(QualSvee rQualSvee, String rLob) {
		Ctac rCtac;
		BomCust rBomCust;
		int rx, ri, rl;

		rCtac = null;
		rBomCust = null;
		rl = rQualSvee.getBomCustAry().length;
		for (rx = (rl - 1); rx >= 0; rx--) {
			rBomCust = rQualSvee.getBomCustAry()[rx];
			if (debug) Log.i(TAG, rBomCust.getLob());
			if (rLob.equals(rBomCust.getLob())) {
				break;
			}
		}

		if (rx >= 0) {
			rCtac = new Ctac();
			rCtac.setBomCust(rBomCust);
		}
		return (rCtac);
	}

	public final void initContactInfo(){
		if (debug) Log.i(TAG, "contact info");
		if (ctacCra != null) {	
			Ctac  mCtacDrg =  isDrgNull ? null : ctacCra.getODrgCtac();
			Ctac  mCtacIms =  isImsNull ? null : ctacCra.getOImsCtac();
			Ctac  mCtacMob =  isMobNull ? null : ctacCra.getOMobCtac();

			// mCtacDrg is not null and at least one field length is not 0 
			if (mCtacDrg !=null && (mCtacDrg.getEmail().trim().length() > 0 || mCtacDrg.getHomeNum().trim().length() > 0 || mCtacDrg.getMobile().trim().length() > 0 || mCtacDrg.getOfficeNum().trim().length() > 0)) {
				profileContactItemDrg.setText(trimPhoneNumber(mCtacDrg.getMobile()), trimPhoneNumber(mCtacDrg.getOfficeNum()), trimPhoneNumber(mCtacDrg.getHomeNum()), mCtacDrg.getEmail());
				profileContactItemDrg.setEnabled(true);
				profileContactItemDrg.setExpandState(true);
				checkBoxDrg.setEnabled(true);
				if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.HONEYCOMB) {
					aq.id(R.id.myprofcontact_drg_txt).getTextView().setAlpha((float)1);
				} 
			} else {	
				//Combine 2 cases to simplification 
				//1 mCtacDrg is null
				//2 mCtacDrg is not null but all field length = 0
				profileContactItemDrg.setText("","","","");
				if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.HONEYCOMB) {
					aq.id(R.id.myprofcontact_drg_txt).getTextView().setAlpha((float) 0.4);
				} 
				profileContactItemDrg.setExpandState(false);
				checkBoxDrg.setEnabled(false);
				aq.id(R.id.myprofcontact_drg_txt).getTextView().setEnabled(mCtacDrg != null);
				aq.id(R.id.myprofcontact_drg_txt).textColorId(mCtacDrg ==null ? R.color.hkt_txtcolor_grey_disable : R.color.hkt_txtcolor_grey);	
			}

			// mCtacIms is not null and at least one field length is not 0 
			if (mCtacIms !=null && (mCtacIms.getEmail().trim().length() > 0 || mCtacIms.getHomeNum().trim().length() > 0 || mCtacIms.getMobile().trim().length() > 0 || mCtacIms.getOfficeNum().trim().length() > 0)) {
				profileContactItemIms.setText(trimPhoneNumber(mCtacIms.getMobile()), trimPhoneNumber(mCtacIms.getOfficeNum()), trimPhoneNumber(mCtacIms.getHomeNum()), mCtacIms.getEmail());
				profileContactItemIms.setEnabled(true);
				profileContactItemIms.setExpandState(true);
				checkBoxIms.setEnabled(true);
				if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.HONEYCOMB) {
					aq.id(R.id.myprofcontact_ims_txt).getTextView().setAlpha((float)1);
				} 
			} else {		
				if (debug) Log.i(TAG, "ims null");
				//Combine 2 cases to simplification 
				//1 mCtacIms is null
				//2 mCtacIms is not null but all field length = 0
				profileContactItemIms.setText("","","","");
				if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.HONEYCOMB) {
					aq.id(R.id.myprofcontact_ims_txt).getTextView().setAlpha((float) 0.4);
				} 
				profileContactItemIms.setExpandState(false);
				checkBoxIms.setEnabled(false);
				aq.id(R.id.myprofcontact_ims_txt).getTextView().setEnabled(false);
				aq.id(R.id.myprofcontact_ims_txt).textColorId(mCtacIms ==null ? R.color.hkt_txtcolor_grey_disable : R.color.hkt_txtcolor_grey);	
			}

			// mCtacMob is not null and at least one field length is not 0 
			if (mCtacMob !=null && (mCtacMob.getEmail().trim().length() > 0 || mCtacMob.getHomeNum().trim().length() > 0 || mCtacMob.getMobile().trim().length() > 0 || mCtacMob.getOfficeNum().trim().length() > 0)) {
				profileContactItemMob.setText(trimPhoneNumber(mCtacMob.getMobile()), trimPhoneNumber(mCtacMob.getOfficeNum()), trimPhoneNumber(mCtacMob.getHomeNum()), mCtacMob.getEmail());
				profileContactItemMob.setEnabled(true);
				profileContactItemMob.setExpandState(true);
				checkBoxMob.setEnabled(true);
				if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.HONEYCOMB) {
					aq.id(R.id.myprofcontact_mob_txt).getTextView().setAlpha((float)1);
				} 
			} else {
				//Combine 2 cases to simplification 
				//1 mCtacMob is null
				//2 mCtacMob is not null but all field length = 0
				profileContactItemMob.setText("","","","");
				if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.HONEYCOMB) {
					aq.id(R.id.myprofcontact_mob_txt).getTextView().setAlpha((float) 0.4);
				} 
				profileContactItemMob.setExpandState(false);
				checkBoxMob.setEnabled(false);
				aq.id(R.id.myprofcontact_mob_txt).getTextView().setEnabled(false);
				aq.id(R.id.myprofcontact_mob_txt).textColorId(mCtacMob ==null ? R.color.hkt_txtcolor_grey_disable : R.color.hkt_txtcolor_grey);	
			}

			setApplyLayout();
			//			if (checkBoxDrg.isEnabled() || checkBoxIms.isEnabled() || checkBoxMob.isEnabled()) {
			////				setApplyLayout();
			//			} else {
			//				aq.id(R.id.myprofcontact_btns_update_layout).visibility(View.GONE);
			//				aq.id(R.id.myprofcontact_btns_apply_layout).visibility(View.GONE);
			//			}
			//						setApplyLayout();
			//			aq.id(R.id.myprofcontact_btns_update_layout).visibility(View.VISIBLE);




			BomCust rBomCust;
			int rx, ri, rl;
			Boolean mobEnable = false;
			Boolean imsEnable = false;
			Boolean drgEnable = false;

			rl = ClnEnv.getQualSvee().getBomCustAry().length;
			for (rx = (rl - 1); rx >= 0; rx--) {
				rBomCust = ClnEnv.getQualSvee().getBomCustAry()[rx];
				if (debug) Log.i(TAG, rBomCust.getLob());
				if (rBomCust.getLob().equals(SubnRec.LOB_MOB) || rBomCust.getLob().equals(SubnRec.LOB_IOI)) {
					mobEnable = true;
				} else if (rBomCust.getLob().equals(SubnRec.LOB_PCD) || rBomCust.getLob().equals(SubnRec.LOB_TV)){
					imsEnable = true;
				} else if (rBomCust.getLob().equals(SubnRec.LOB_LTS)) {
					drgEnable = true;
				}
			}

			checkBoxDrg.setEnabled(drgEnable); 
			profileContactItemDrg.setExpandState(drgEnable);
			aq.id(R.id.myprofcontact_drg_txt).getTextView().setEnabled(drgEnable);
			if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.HONEYCOMB) {
				aq.id(R.id.myprofcontact_drg_txt).getTextView().setAlpha(drgEnable? (float)1 : 0.4f);
			}
			aq.id(R.id.myprofcontact_drg_txt).textColorId(!drgEnable? R.color.hkt_txtcolor_grey_disable : R.color.hkt_txtcolor_grey);

			checkBoxIms.setEnabled(imsEnable); 
			profileContactItemIms.setExpandState(imsEnable);
			aq.id(R.id.myprofcontact_ims_txt).getTextView().setEnabled(imsEnable);
			if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.HONEYCOMB) {
				aq.id(R.id.myprofcontact_ims_txt).getTextView().setAlpha(imsEnable? (float)1 : 0.4f);
			}
			aq.id(R.id.myprofcontact_ims_txt).textColorId(!imsEnable? R.color.hkt_txtcolor_grey_disable : R.color.hkt_txtcolor_grey);	

			checkBoxMob.setEnabled(mobEnable); 
			profileContactItemMob.setExpandState(mobEnable);
			aq.id(R.id.myprofcontact_mob_txt).getTextView().setEnabled(mobEnable);
			if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.HONEYCOMB) {
				aq.id(R.id.myprofcontact_mob_txt).getTextView().setAlpha(mobEnable? (float)1 : 0.4f);
			}
			aq.id(R.id.myprofcontact_mob_txt).textColorId(!mobEnable? R.color.hkt_txtcolor_grey_disable : R.color.hkt_txtcolor_grey);	
		}
	}


	/**************************
	 * 
	 * Update data
	 * 
	 ******************************/
	// My Contact Info
	private final boolean checkSelectedList() {
		return (!(checkBoxDrg.isChecked() || checkBoxIms.isChecked() || checkBoxMob.isChecked()));
	}
	//Ask user if the update is comfirm
	protected final void updateConfirmDialog() {
		OnClickListener onPositiveListener = new OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();
				// UpdateCtac
				APIsManager.doUpdCtInfo(me, toSrvCtacCra);					
			}
		};
		OnClickListener onNegativeListener = new OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();
				displayDialog(getResString(R.string.CFMM_DISCARD));
				// Clear all Input values
				aq.id(R.id.myprofcontact_email_et).getEditText().setText("");	
				aq.id(R.id.myprofcontact_daymob_et).getEditText().setText("");				
				aq.id(R.id.myprofcontact_nightmob_et).getEditText().setText("");				
				aq.id(R.id.myprofcontact_mob_et).getEditText().setText("");		
				checkBoxDrg.setChecked(false);
				checkBoxIms.setChecked(false);
				checkBoxMob.setChecked(false);
			}
		};
		DialogHelper.createTitleDialog(getActivity(), getResString(R.string.CTAM_CFMHDR), getResString(R.string.CTAM_CFMMSG), getResString(R.string.CFMF_CFM), onPositiveListener, getResString(R.string.CFMF_NOTCFM), onNegativeListener);
	}

	// Update contact info codes ported from CSP
	private Reply loadInput() {
		Reply rRC;
		//TODO

		toSrvCtacCra = ctacCra.copyMe();
		Gson gson = new GsonBuilder().serializeNulls().create();
		if (debug) Log.i(TAG, gson.toJson(ctacCra));
		toSrvCtacCra.setIDrgCtac(ctacCra.copyMe().getODrgCtac());
		toSrvCtacCra.setIImsCtac(ctacCra.copyMe().getOImsCtac());
		toSrvCtacCra.setIMobCtac(ctacCra.copyMe().getOMobCtac());
		toSrvCtacCra.clearODrgCtac();
		toSrvCtacCra.clearOImsCtac();
		toSrvCtacCra.clearOMobCtac();
		//		Log.i(TAG, "T");
		if (toSrvCtacCra.getIDrgCtac() != null) {
			//			Log.i(TAG, "T1");
			if (checkBoxDrg.isChecked()) {
				//				Log.i(TAG, "T2");
				loadCtac(toSrvCtacCra.getIDrgCtac());
				rRC = toSrvCtacCra.getIDrgCtac().validate();
				if (!rRC.isSucc()) { return (rRC); }
				//					/* Re-validate for LOB Specific */
				//					rRC = toSrvCtacCra.getIDrgCtac().validate();
				//					if (!rRC.isSucc()) {
				//						interpretRC_CtacMdu(rRC);
				//						return (rRC);
				//					}
			} else {
								toSrvCtacCra.setIDrgCtac(null);
			}
		}

		if (toSrvCtacCra.getIImsCtac() != null) {
			//			Log.i(TAG, "T3");
			if (checkBoxIms.isChecked()) {
				//				Log.i(TAG, "T4");
				loadCtac(toSrvCtacCra.getIImsCtac());
				rRC = toSrvCtacCra.getIImsCtac().validate();
				if (!rRC.isSucc()) { return (rRC); }
				//					/* Re-validate for LOB Specific */
				//					rRC = toSrvCtacCra.getIDrgCtac().validate();
				//					if (!rRC.isSucc()) {
				//						interpretRC_CtacMdu(rRC);
				//						return (rRC);
				//					}
			} else {
								toSrvCtacCra.setIImsCtac(null);
			}
		}

		if (toSrvCtacCra.getIMobCtac() != null) {
			//			Log.i(TAG, "T5");	
			if (checkBoxMob.isChecked()) {
				//				Log.i(TAG, "T6");
				loadCtac(toSrvCtacCra.getIMobCtac());
				rRC = toSrvCtacCra.getIMobCtac().validate();
				if (!rRC.isSucc()) { return (rRC); }
				//					/* Re-validate for LOB Specific */
				//					rRC = toSrvCtacCra.getIDrgCtac().validate();
				//					if (!rRC.isSucc()) {
				//						interpretRC_CtacMdu(rRC);
				//						return (rRC);
				//					}
			} else {
								toSrvCtacCra.setIMobCtac(null);
			}
		}
		return (Reply.getSucc());
	}

	private Ctac loadCtac(Ctac rCtac) {
		if (rCtac == null) {
			rCtac = new Ctac();
		}
		rCtac.setOfficeNum(aq.id(R.id.myprofcontact_daymob_et).getEditText().getText().toString().trim());
		rCtac.setHomeNum(aq.id(R.id.myprofcontact_nightmob_et).getEditText().getText().toString().trim());
		rCtac.setMobile(aq.id(R.id.myprofcontact_mob_et).getEditText().getText().toString().trim());
		rCtac.setEmail(aq.id(R.id.myprofcontact_email_et).getEditText().getText().toString().trim());
		return (rCtac);
	}

	// Simple UI Dialog
	protected final void displayDoneUpdateCtacDialog(String message) {
		if (alertDialog != null) {
			alertDialog.dismiss();
		}
		OnClickListener OnClickListener= new OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();
				alertDialog = null;

				// refresh UI
				isApplyShown = false; //hide edit mode
				setApplyLayout(); 
				me.getContactInfo();
			}
		};
		DialogHelper.createSimpleDialog(getActivity(), message, getResString(R.string.btn_ok), OnClickListener);

	}

	// The phone number returned from BOM might have string suffix that we need to remove
	private final String trimPhoneNumber(String phonenum) {
		if (phonenum == null) return null;
		if (phonenum.length() <= 8) return phonenum;
		return phonenum.substring(0, 8);
	}

	@Override
	public void onResume(){
		super.onResume();
		if (callback.getCurrentPage() == 2) {
			refresh();
		}
	}

	@Override
	public void onSuccess(APIsResponse response) {
		CtacCra	mCtacCra;
		if (debug) Log.i(TAG, "Succ");
		// TODO Auto-generated method stub
		if (APIsManager.HELO.equals(response.getActionTy())) {
			//			debugLog(TAG, "doHelo complete!!");
		} else if (APIsManager.READ_CTAC.equals(response.getActionTy())) {
			//Read ctac
			mCtacCra = (CtacCra) response.getCra();
			if (callback.getCurrentPage() == PAGE_NO) {
				Gson gson = new GsonBuilder().serializeNulls().create();
				if (debug) Log.i(TAG, "Result" + gson.toJson(mCtacCra));

				aq.id(R.id.myprofcontact_mob_et).enabled(true);
				aq.id(R.id.myprofcontact_email_et).enabled(true);
				aq.id(R.id.myprofcontact_daymob_et).enabled(true);
				aq.id(R.id.myprofcontact_nightmob_et).enabled(true);
				aq.id(R.id.myprofcontact_btn_apply).enabled(true);	
				ctacCra = mCtacCra.copyMe();
				if (isDrgNull) { ctacCra.setODrgCtac(null); }
				if (isImsNull) { ctacCra.setOImsCtac(null); }
				if (isMobNull) { ctacCra.setOMobCtac(null); }
				initContactInfo();
			}			
		} else if (APIsManager.UPD_CTAC.equals(response.getActionTy())) {
			mCtacCra = (CtacCra) response.getCra();
			if (callback.getCurrentPage() == PAGE_NO) {
				//Suppose iDrgCtac is null,
				//After update/read oDrgCtac/ will not be null but object with empty strings
				//need to turn it be to null for second update correct
				ctacCra = mCtacCra.copyMe();
				if (isDrgNull) { ctacCra.setODrgCtac(null); }
				if (isImsNull) { ctacCra.setOImsCtac(null); }
				if (isMobNull) { ctacCra.setOMobCtac(null); }
				displayDoneUpdateCtacDialog(Utils.getString(me.getActivity(), R.string.CTAM_DONE));
			}
		}
	}

	@Override
	public void onFail(APIsResponse response) {
		CtacCra mCtacCra = (CtacCra) response.getCra();
		Gson gson = new Gson();

		if (debug) Log.i(TAG, gson.toJson(mCtacCra));
		// General Error Message
		if (!"".equals(response.getMessage()) && response.getMessage() != null) {
			displayDialog(response.getMessage());
		} else if (RC.TCSESS_MM.equalsIgnoreCase(response.getReply().getCode())) {
			BaseActivity.ivSessDialog();
		} else {
			displayDialog(InterpretRCManager.interpretRC_CtacMdu(getActivity(), response.getReply()));
		}
	}

}

