package com.pccw.myhkt.fragment;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentTransaction;

import com.pccw.dango.shared.entity.SveeRec;
import com.pccw.myhkt.APIsResponse;
import com.pccw.myhkt.FAWrapper;
import com.pccw.myhkt.R;
import com.pccw.myhkt.Utils;

public class MyProfLoginMainFragment extends BaseFragment {
    private MyProfLoginMainFragment me;

    private SveeRec sveeRec = null;
    private FragmentTransaction ft;
    private int activeSubView = 0;

    // CallBack
    private OnMyProfLoginListener callback;

    private String TAG = "MyProfLoginMainFragment";

    public interface OnMyProfLoginListener {
        SveeRec getSveeRec();

        void setSveeRec(SveeRec sveeRec);

        void closeActivity();

        int getCurrentPage();
    }

    @Override
    public void onAttach(@NonNull Activity activity) {
        super.onAttach(activity);

        try {
            callback = (OnMyProfLoginListener) activity;

        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement OnMyProfLoginListener");
        }
    }

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        me = this;
        ft = getChildFragmentManager().beginTransaction();
        View fragmentLayout = inflater.inflate(R.layout.fragment_myproflogin, container, false);

        activeSubView = 0;
        displayChildView(0);
        return fragmentLayout;
    }

    public void openLoginidSubFrag() {
        activeSubView = 0;
        displayChildView(2);
    }

    public void openChangePwSubFrag() {
        activeSubView = 1;
        displayChildView(activeSubView);
    }

    public void openDeleteAccountFrag(String email) {
        activeSubView = 2;
        displayChildView(1, email);
    }

    private void displayChildView(int type) {
        displayChildView(type, "");
    }

    public void displayChildView(int type, String email) {
        ft = getChildFragmentManager().beginTransaction();
        if (type == 1) {
            ft.setCustomAnimations(R.anim.right_slide_in, R.anim.left_slide_out);
        } else if (type == 2) {
            ft.setCustomAnimations(R.anim.left_slide_in, R.anim.right_slide_out);
        }

        switch (activeSubView) {
            case 0:
                FAWrapper.getInstance().sendFAScreen(getActivity(), R.string.CONST_SCRN_MYPROFILE_MYHKT,
                        false);
                MyProfLoginFragment myProfLoginIDSubFragment = new MyProfLoginFragment();
                ft.replace(R.id.fragment_myproflogin_fragment, myProfLoginIDSubFragment);
                break;
            case 1:
                FAWrapper.getInstance().sendFAScreen(getActivity(), R.string.CONST_SCRN_MYPROFILE_CHANGEPWD,
                        false);
                ChangePwdFragment myProfLoginChangePwSubFragment = new ChangePwdFragment();
                ft.replace(R.id.fragment_myproflogin_fragment, myProfLoginChangePwSubFragment);
                break;
            case 2:
                FAWrapper.getInstance().sendFAScreen(getActivity(), R.string.CONST_SCRN_MYPROFILE_DELETE_ACCOUNT,
                        false);
                ft.replace(R.id.fragment_myproflogin_fragment, DeleteAccountFragment.newInstance(email));
                break;
        }

        Utils.closeSoftKeyboard(getActivity());
        ft.commit();
    }

    @Override
    public void onResume() {
        super.onResume();
        switch (activeSubView) {
            case 0:
                FAWrapper.getInstance().sendFAScreen(getActivity(), R.string.CONST_SCRN_MYPROFILE_MYHKT, false);
                break;
            case 1:
                FAWrapper.getInstance().sendFAScreen(getActivity(), R.string.CONST_SCRN_MYPROFILE_CHANGEPWD, false);
                break;
        }
    }

    public void closeActivity() {
        callback.closeActivity();
    }

    @Override
    public void onStart() {
        super.onStart();
        sveeRec = callback.getSveeRec();
    }

    @Override
    public void onSuccess(APIsResponse response) {
        // TODO Auto-generated method stub
        Log.d("lwg", "onSuccess APIsResponse xxx activity");
    }

    @Override
    public void onFail(APIsResponse response) {
        // TODO Auto-generated method stub
        Log.d("lwg", "onFail APIsResponse xxx activity");
    }

    public void refresh() {
        if (debug) Log.i(TAG, "refresh");
        sveeRec = callback.getSveeRec();
        try {
            activeSubView = 0;
            displayChildView(0);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
