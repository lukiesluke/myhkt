package com.pccw.myhkt;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.security.MessageDigest;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.Enumeration;
import java.util.Locale;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import android.app.Activity;
import androidx.appcompat.app.AlertDialog;
import android.app.NotificationManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.PorterDuff;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.text.InputFilter;
import android.text.Spanned;
import android.text.TextUtils;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.TextView;
import com.pccw.biz.myhkt.R;
import com.pccw.dango.shared.entity.SubnRec;
import com.pccw.myhkt.model.LnttAgent;
import com.pccw.myhkt.service.LineTestIntentService;

public class Utils {
	private static Context	myContext	= null;
	private static int		id			= 1;
	// REF-JF-20111220
	private static String	androidId;
	private static String   currentModule;
	private static boolean	needLiveChatDisclaimer 	= true; //true = required, false = not required

	static public Context getContext(Context context) {
		if (myContext == null) {
			myContext = context.getApplicationContext();
		}
		return context;
	}

	public static int getUniqueId() {
		return id++;
	}

	public static void alert(Context ctx, String message, String okLabel) {
		alert(ctx, Utils.getString(ctx, R.string.alert), message, okLabel);
	}

	public static void alert(Context ctx, String title, String message, String okLabel) {
		AlertDialog.Builder builder = new AlertDialog.Builder(ctx);
		builder.setTitle(title);
		builder.setMessage(message);
		builder.setNeutralButton(okLabel, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface arg0, int arg1) {

				return;
			}
		});
		AlertDialog ad = builder.create();
		ad.setCanceledOnTouchOutside(false);
		ad.show();
	}

	public static String truncateStringDot(String input) {
		String output = "";

		try {
			if (input.indexOf(".") != -1) {
				output = input.substring(0, input.indexOf("."));
				return output;
			} else return input;
		} catch (Exception e) {
			e.printStackTrace();
			return input;
		}

	}

	public static String convertAcctFormat(String acctNum) {
		try {
			acctNum = acctNum.substring(0, 2) + "-" + acctNum.substring(2, 8) + "-" + acctNum.substring(8, 12) + "-" + acctNum.substring(12, 14);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return acctNum;
	}

	public static String convertDoubleToString(double value, int decimalPlace) {
		String result = "";
		try {
			result = String.valueOf(value);
			String token = "#0.";
			for (int i = 0; i < decimalPlace; i++) {
				token += "0";
			}
			DecimalFormat decimalFormat = new DecimalFormat(token);
			result = decimalFormat.format(value);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	public static String convertStringToPrice(String str) {
		DecimalFormat myFormatter = new DecimalFormat("$###,##0.00");

		try {
			return myFormatter.format(parseDouble(str));
		} catch (Exception e) {
			e.printStackTrace();
			return "$" + str;
		}
	}

	public static String convertStringToPrice1dp(String str) {
		DecimalFormat myFormatter = new DecimalFormat("$###,##0.0");

		try {
			return myFormatter.format(parseDouble(str));
		} catch (Exception e) {
			e.printStackTrace();
			return "$" + str;
		}
	}
	
	public static String convertStringToPrice0dec(String str) {
		DecimalFormat myFormatter = new DecimalFormat("$###,###");

		try {
			return myFormatter.format(parseDouble(str));
		} catch (Exception e) {
			e.printStackTrace();
			return "$" + str;
		}
	}

	public static double parseDouble(String str) {
		str = str.trim();
		str = str.replace("$", "");
		str = str.replace(",", "");

		try {
			return Double.parseDouble(str);
		} catch (Exception e) {
			throw e;
		}
	}

	public static String convertPriceToString(String str) {
		if (str == null) { return str; }
		return removeAllCommas(str.replaceAll("\\$", ""));
	}

	public static String removeAllCommas(String str) {
		return str.replaceAll(",", "");
	}

	private static ProgressDialog	dialog	= null;

	public static boolean isVersionUptodate(String currentVersion, String serverVersion) {
		boolean isUptodate = false;

		// JF-20111011 ++
		// change version number format from x.xx to x.x.x
		// boolean isUptodate = Double.parseDouble(currentVersion) >= Double.parseDouble(serverVersion);
		int currentDotCount = currentVersion.length() - currentVersion.replaceAll("\\.", "").length();
		int serverDotCount = serverVersion.length() - serverVersion.replaceAll("\\.", "").length();

		if ((currentDotCount == 1) && (serverDotCount == 1)) {
			// old version number x.x
			isUptodate = Double.parseDouble(currentVersion) >= Double.parseDouble(serverVersion);
		} else if ((currentDotCount == 2) && (serverDotCount == 2)) {
			// new version number x.x.x
			isUptodate = Double.parseDouble(currentVersion.replaceFirst("\\.", "")) >= Double.parseDouble(serverVersion.replaceFirst("\\.", ""));
		} else isUptodate = (currentDotCount != 1) || (serverDotCount != 2);
		// JF-20111011 --

		return isUptodate;
	}

	public static void boldTextView(TextView tv) {
		tv.getPaint().setFakeBoldText(true);
	}

	public static InputFilter lowerCaseInputFilter() {
		return new InputFilter() {
			@Override
			public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
				return source.toString().toLowerCase();
			}
		};
	}

	// convert date string to locale based format. English locale pattern by default
	public static String toLocaleDateString(String dateString, String patternString) {
		SimpleDateFormat sdf = new SimpleDateFormat(patternString, Locale.ENGLISH);

		// FL 20110928 ++
		// String patternStrEn = "dd/MM/yyyy";
		// String patternStrZh = "yyyy�~MM��dd��";
		String patternStr = Utils.getString(myContext, R.string.pcd_settle_date_format);
		// FL 20110928 --
		Date d;
		String localeStr = myContext.getResources().getConfiguration().locale.getLanguage();

		try {
			d = sdf.parse(dateString);
			// FL 20110928 ++
			// if (localeStr.equalsIgnoreCase("zh")) {
			// sdf = new SimpleDateFormat(patternStrZh);
			// } else {
			// sdf = new SimpleDateFormat(patternStrEn);
			// }
			sdf = new SimpleDateFormat(patternStr);
			// FL 20110928 --

			return sdf.format(d);

		} catch (Exception e) {
			Log.e("Utils", "toLocaleDateString error: dateString=" + dateString + ", patternString=" + patternString + ", Exception:" + e.toString());
			return dateString;
		}

	}

	public static String toDateString(String dateString, String patternString, String outPatternString) {
		SimpleDateFormat sdf = new SimpleDateFormat(patternString, Locale.ENGLISH);
		Date d;

		try {
			d = sdf.parse(dateString);
			sdf = new SimpleDateFormat(outPatternString);

			return sdf.format(d);

		} catch (Exception e) {
			Log.e("Utils", "toLocaleDateString error: dateString=" + dateString + ", patternString=" + patternString + ", Exception:" + e.toString());
			return dateString;
		}

	}

	public static String toTimeString(String timeString, String patternString, String outPatternString) {
		SimpleDateFormat sdf = new SimpleDateFormat(patternString, Locale.ENGLISH);
		Date d;

		try {
			d = sdf.parse(timeString);
			sdf = new SimpleDateFormat(outPatternString);

			return sdf.format(d);

		} catch (Exception e) {
			Log.e("Utils", "toLocaleDateString error: dateString=" + timeString + ", patternString=" + patternString + ", Exception:" + e.toString());
			return timeString;
		}

	}

	public static String inputStreamToString(InputStream is) throws Exception {
		BufferedReader reader = new BufferedReader(new InputStreamReader(is));
		StringBuilder sb = new StringBuilder();
		String line = null;
		while ((line = reader.readLine()) != null) {
			sb.append(line + "\n");
		}

		return sb.toString();
	}

	public static void changeButtonImageColor(Button button, int color, int position){
		//drawable position Left=0 Top=1 Right=2 Bottom=3
		Drawable drawable = button.getCompoundDrawables()[position];
		if(drawable != null){
			drawable.setColorFilter(color, PorterDuff.Mode.SRC_ATOP);
			switch (position) {
			case 0:
				button.setCompoundDrawablesWithIntrinsicBounds(drawable, null, null, null);
				break;
			case 1:
				button.setCompoundDrawablesWithIntrinsicBounds(null, drawable, null, null);
				break;
			case 2:
				button.setCompoundDrawablesWithIntrinsicBounds(null, null, drawable, null);
				break;
			case 3:
				button.setCompoundDrawablesWithIntrinsicBounds(null, null, null, drawable);
				break;
			}
		}
	}

	public static Drawable changeDrawableColor(Drawable drawable, int color) {
		if(drawable != null){
			Drawable temp = drawable.getConstantState().newDrawable().mutate();

			temp.setColorFilter(color, PorterDuff.Mode.SRC_ATOP);
			return temp;
		}
		return drawable;
	}

	// REF-FL 20111104 ++
	public static String formatNumber(double value) {
		DecimalFormat formatter = new DecimalFormat("#,###,###,###,###,###");
		return formatter.format(value);
	}

	// REF-FL 20111104 --

	public static String formatNumber(String value) {
		try {
			return formatNumber(Double.parseDouble(value));
		} catch (Exception e) {
			return value;
		}
	}

	// CSP-CR2013011 - Adding IDD service & Onecall at CS Portal
	public static int getLtsSrvTypeDemo(String srv_num) {
		int num = Integer.parseInt(srv_num);

		switch (num % 5) {
		case 0:
			return R.string.CONST_LTS_IDD0060;
		case 1:
			return R.string.CONST_LTS_EYE;
		case 2:
			return R.string.CONST_LTS_CALLINGCARD;
		case 3:
			return R.string.CONST_LTS_ONECALL;
		case 4:
			return R.string.CONST_LTS_FIXEDLINE;
		default:
			return R.string.CONST_LTS_INVALID;
		}
	}

	public static int getLtsSrvType(String tos, String eye_grp, String pri_login_ind) {
		// validation
		if (tos == null || eye_grp == null || pri_login_ind == null) { return R.string.CONST_LTS_INVALID; }

		// IDD0060 - MOB MOB / MOB TEL
		if (tos.equals(SubnRec.TOS_LTS_MOB)) { return R.string.CONST_LTS_IDD0060; }

		// ITS - Global Calling Card
		if (tos.equals(SubnRec.TOS_LTS_ITS)) { return R.string.CONST_LTS_CALLINGCARD; }
		
		// eye
		if (tos.equals(SubnRec.TOS_LTS_EYE)) { return R.string.CONST_LTS_EYE; }

		// OneCall Service
		if (tos.equals(SubnRec.TOS_LTS_ONC)) { return R.string.CONST_LTS_ONECALL; }

		// ICFS
		if (tos.equals(SubnRec.TOS_LTS_ICF)) { return R.string.CONST_LTS_ICFS; }

		if (tos.equals(SubnRec.TOS_LTS_TEL)) {
			// eye
			if (eye_grp.trim().length() > 0) { return R.string.CONST_LTS_EYE; }

			// OneCall Service
			if (pri_login_ind.equalsIgnoreCase("O")) { return R.string.CONST_LTS_ONECALL; }

			// ICFS
			if (pri_login_ind.equalsIgnoreCase("I")) { return R.string.CONST_LTS_ICFS; }

			// Fixed Line
			return R.string.CONST_LTS_FIXEDLINE;
		}
		return R.string.CONST_LTS_INVALID;
	}

	// Update the NavBar Style for Premier
	/*	public static void changeToThemeNavBar(Context cxt, LinearLayout changeLayout, TextView changeTextView, ImageButton changeBackBtn) {
		boolean isPremier = ClnEnv.getSessionPremierFlag();
		boolean isMyMob  = ClnEnv.isMyMobFlag();
		boolean is101 = ClnEnv.isIs101Flag();
		if(isMyMob) {
			if(is101) {
				changeLayout.setBackgroundColor(cxt.getResources().getColor(R.color.black));
			}
			else {
				changeLayout.setBackgroundColor(cxt.getResources().getColor(R.color.mymob_orange));
			}
			changeTextView.setTextColor(cxt.getResources().getColor(R.color.hkt_headingfont));
			changeTextView.setTypeface(null, Typeface.NORMAL);
		}
		else if (isPremier) {
			changeLayout.setBackgroundColor(cxt.getResources().getColor(R.color.hkt_headingpremier));
			changeTextView.setTextColor(cxt.getResources().getColor(R.color.hkt_headingfontpremier));
			changeTextView.setTypeface(null, Typeface.BOLD);
		} else {
			changeLayout.setBackgroundColor(cxt.getResources().getColor(R.color.hkt_headingbluelighter));
			changeTextView.setTextColor(cxt.getResources().getColor(R.color.hkt_headingfont));
			changeTextView.setTypeface(null, Typeface.NORMAL);
		}

		if (changeBackBtn != null) {
			changeBackBtn.setOnTouchListener(new CCMCHoverButton());
		}
	}


// Update the Button Style for Premier
public static void changeToThemeButton(Context cxt, int btnType,Button... changeBtns) {
	final int 	iconPadding = (int) cxt.getResources().getDimension(R.dimen.padding_screen);
	final int 	btnBlueTextColor	= cxt.getResources().getColor(R.color.btn_blue_textcolor);
	final int 	btnOrangeTextColor	= cxt.getResources().getColor(R.color.mymob_orange);
	final int 	btnBlackTextColor	= cxt.getResources().getColor(R.color.black);
	final int	btnBlueTextColor_P	= cxt.getResources().getColor(R.color.btn_blue_textcolor_p);
	final int 	btnWhiteTextColor   = cxt.getResources().getColor(R.color.btn_white_textcolor);
	final int 	btnWhiteTextColor_P = cxt.getResources().getColor(R.color.btn_white_textcolor_p);
	// Commented Code until start Premier function
	boolean isPremier = ClnEnv.getSessionPremierFlag();
	boolean isMyMob  = ClnEnv.isMyMobFlag();
	boolean is101 = ClnEnv.isIs101Flag();
	for (Button changeBtn:changeBtns) {
		try {
			switch (btnType) {
				case R.string.CONST_BTN_BLUE:
					if (isMyMob) {
						if(is101)  {
							changeBtn.setBackgroundResource(R.drawable.borderline2pxblack);
							changeBtn.setTextColor(btnBlackTextColor);
						}
						else {
							changeBtn.setBackgroundResource(R.drawable.borderlineorange2px);
							changeBtn.setTextColor(btnOrangeTextColor);
						}
					}
					else if (isPremier) {
						changeBtn.setBackgroundResource(R.drawable.premier_but_blue);
						changeBtn.setTextColor(btnBlueTextColor_P);
					} else {
						changeBtn.setBackgroundResource(R.drawable.borderline2px);
						changeBtn.setTextColor(btnBlueTextColor);
					}
					break;
				case R.string.CONST_BTN_BLUE_SMALL:
					if (isMyMob) {
						if(is101)  {
							changeBtn.setBackgroundResource(R.drawable.borderline2pxblack);
							changeBtn.setTextColor(btnBlackTextColor);
						}
						else {
							changeBtn.setBackgroundResource(R.drawable.borderlineorange2px);
							changeBtn.setTextColor(btnOrangeTextColor);
						}
					}
					else if (isPremier) {
						changeBtn.setBackgroundResource(R.drawable.premier_but_blue);
						changeBtn.setTextColor(btnBlueTextColor_P);
					} else {
						changeBtn.setBackgroundResource(R.drawable.borderline2px);
						changeBtn.setTextColor(btnBlueTextColor);
					}
					changeBtn.setPadding(iconPadding*2, 0, iconPadding*2, 0);
					break;
				case R.string.CONST_BTN_WHITE:
					if (isPremier) {
						changeBtn.setBackgroundResource(R.drawable.premier_but_grey);
						changeBtn.setTextColor(btnWhiteTextColor_P);
					} else {
						changeBtn.setBackgroundResource(R.drawable.borderlinegray2px);
						changeBtn.setTextColor(btnWhiteTextColor);
					}
					break;
				case R.string.CONST_BTN_CALL:
					if (isMyMob) {
						if (is101) {
							changeBtn.setBackgroundResource(R.drawable.borderline2pxblack);
							changeBtn.setTextColor(btnBlackTextColor);
							changeBtn.setCompoundDrawablesWithIntrinsicBounds(R.drawable.call, 0, 0, 0);
							//Sameple for use
							Utils.changeButtonImageColor(changeBtn, Color.BLACK, 0);
						} else {
							changeBtn.setBackgroundResource(R.drawable.borderlineorange2px);
							changeBtn.setTextColor(btnOrangeTextColor);
							changeBtn.setCompoundDrawablesWithIntrinsicBounds(R.drawable.call, 0, 0, 0);
							//Sameple for use
							Utils.changeButtonImageColor(changeBtn, cxt.getResources().getColor(R.color.mymob_orange), 0);
						}
					}
					else if (isPremier) {
						changeBtn.setBackgroundResource(R.drawable.premier_but_blue);
						changeBtn.setTextColor(btnBlueTextColor_P);
						changeBtn.setCompoundDrawablesWithIntrinsicBounds(R.drawable.call_p, 0, 0, 0);
					} else {
						changeBtn.setBackgroundResource(R.drawable.borderline2px);
						changeBtn.setTextColor(btnBlueTextColor);
						changeBtn.setCompoundDrawablesWithIntrinsicBounds(R.drawable.call, 0, 0, 0);
					}
					changeBtn.setPadding(iconPadding, 0, 0, 0);
					break;
				case R.string.CONST_BTN_LIVECHAT:
					if(isPremier) {
						changeBtn.setBackgroundResource(R.drawable.premier_but_blue);
						changeBtn.setTextColor(btnBlueTextColor_P);
						changeBtn.setCompoundDrawablesWithIntrinsicBounds(R.drawable.chat_p, 0, 0, 0);
					} else {
						changeBtn.setBackgroundResource(R.drawable.borderline2px);
						changeBtn.setTextColor(btnBlueTextColor);
						changeBtn.setCompoundDrawablesWithIntrinsicBounds(R.drawable.chat, 0, 0, 0);
					}
					changeBtn.setPadding(iconPadding, 0, 0, 0);
					break;
				case R.string.CONST_BTN_SEND:
					if(isMyMob) {
						changeBtn.setBackgroundResource(R.drawable.borderlineorange2px);
						changeBtn.setTextColor(btnOrangeTextColor);
						changeBtn.setCompoundDrawablesWithIntrinsicBounds(R.drawable.mail, 0, 0, 0);
					}
					else if (isPremier) {
						changeBtn.setBackgroundResource(R.drawable.premier_but_blue);
						changeBtn.setTextColor(btnBlueTextColor_P);
						changeBtn.setCompoundDrawablesWithIntrinsicBounds(R.drawable.mail_p, 0, 0, 0);
					} else {
						changeBtn.setBackgroundResource(R.drawable.borderline2px);
						changeBtn.setTextColor(btnBlueTextColor);
						changeBtn.setCompoundDrawablesWithIntrinsicBounds(R.drawable.mail, 0, 0, 0);
					}
					changeBtn.setPadding(iconPadding, 0, 0, 0);
					break;
				case R.string.CONST_BTN_PON:
					if (isPremier) {
						changeBtn.setBackgroundResource(R.drawable.premier_but_bluesolid);
						changeBtn.setTextColor(btnBlueTextColor_P);
					} else {
						changeBtn.setBackgroundResource(R.drawable.borderline2pxsolid);
						changeBtn.setTextColor(btnBlueTextColor);
					}
					break;
			}

			changeBtn.setOnTouchListener(new CCMCHoverButton(cxt, btnType, isPremier,isMyMob));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}

// Update Button behavior
public static void setButtonOnTouchBehavior(Context cxt, Button...changeBtns) {
	for (Button changeBtn:changeBtns) {
		try {
			changeBtn.setOnTouchListener(new CCMCHoverButton());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}

// Update Switch behavior with different button effect
public static void setSwitchOnChangeStateBehavior(Context cxt, Button...changeBtns) {
	final int 	btnBlueTextColor	= cxt.getResources().getColor(R.color.btn_blue_textcolor);
	for (int i = 0 ; i < changeBtns.length ; i++) {
		try {
			if (i == 0) {
				changeBtns[i].setBackgroundResource(R.drawable.borderline2pxhighlight);
				changeBtns[i].setTextColor(Color.WHITE);
			} else {
				changeBtns[i].setBackgroundResource(R.drawable.borderline2px);
				changeBtns[i].setTextColor(btnBlueTextColor);
			}
			changeBtns[i].setOnTouchListener(new CCMCHoverButton());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}

public static void setSwitchOnChangeStateBehavior(Context cxt, String lob, Button...changeBtns) {
	if (ClnEnv.isMyMobFlag()) {
		boolean is1010 = lob.equalsIgnoreCase(SubscriptionRec.LOB_101);
		final int backgroundHighlight = (is1010)? R.drawable.borderline2pxblackhighlight : R.drawable.borderorangeline2pxhighlight;
		final int background = (is1010)? R.drawable.borderline2pxblack : R.drawable.borderlineorange2px2;
		final int textColorHightlight = (is1010)? cxt.getResources().getColor(R.color.btn_1010_yellow) : cxt.getResources().getColor(R.color.hkt_headingfont);
		final int textColor = (is1010)? Color.BLACK : cxt.getResources().getColor(R.color.mymob_orange);

		for (int i = 0 ; i < changeBtns.length ; i++) {
			try {
				if (i == 0) {
					changeBtns[i].setBackgroundResource(backgroundHighlight); //black
					changeBtns[i].setTextColor(textColorHightlight); //yellow
				} else {
					changeBtns[i].setBackgroundResource(background); //yellow
					changeBtns[i].setTextColor(textColor); //black
				}
				changeBtns[i].setOnTouchListener(new CCMCHoverButton());
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	} else {
		setSwitchOnChangeStateBehavior(cxt, changeBtns);
	}

}*/
	public static void changeToThemeButton(Context cxt ,Button... changeBtns) {

		for (Button changeBtn:changeBtns) {
			changeBtn.setBackgroundResource(R.drawable.hkt_btn_bg_blue_selector);
			changeBtn.setTextColor(cxt.getResources().getColorStateList(R.color.hkt_btn_blue_txtcolor_selector));
		}

	}


	// Replaced all 1000 by 28886228 when Account Type is Premier
	public static String getString(Context cxt, int stringResId) {
		String str = "";
		boolean isPremier = ClnEnv.getSessionPremierFlag();
		str = cxt.getString(stringResId);
		if (isPremier) {
			switch (stringResId) {
			case R.string.boost_result_fail:
//			case R.string.BIIMM_NOBILL:
//			case R.string.BIIMM_RTOS_FAIL:
//			case R.string.BIIMM_RTOS_NNOR:
//			case R.string.BILTM_NOBILL:
//			case R.string.BIMBM_HDUN_ALDY:
//			case R.string.BIMBM_NCBAR:
//			case R.string.BIMBM_NOBILL:
//			case R.string.BIMBM_RTOS_FAIL:
//			case R.string.FGCM_IVSTATE:
//			case R.string.FGCM_FILL_RCUS:
//			case R.string.LTT_NP_EYE_MODEM_NGD:
//			case R.string.LTT_NP_EYE_NORMAL:
//			case R.string.LTT_NP_EYE2_MODEM_NGD:
//			case R.string.LTT_NP_EYE2_NORMAL:
//			case R.string.LTT_NP_IMS_MODEN_NGD:
//			case R.string.LTT_NP_IMS_NORMAL:
//			case R.string.LTT_NP_MODEM_OSY:
//			case R.string.LTT_NP_TV_MODEM_NGD:
//			case R.string.LTT_NP_TV_NORMAL:
//			case R.string.LTT_PE_FAU:
//			case R.string.LTT_PE_GOD:
//			case R.string.LTT_PON_EYE_MODEM_NGD:
//			case R.string.LTT_PON_EYE_NORMAL:
//			case R.string.PLNIMM_NOCMT:
//			case R.string.PLNIMM_NOINC:
//			case R.string.PLNIMM_NOVAS:
//			case R.string.PLNLTM_NO_CPL:
//			case R.string.PLNLTM_NO_TPL:
//			case R.string.PLNMBM_NOVAS:
//			case R.string.PLNMBM_OPTVAS_REM:
//			case R.string.PLNTVM_NOSVC:
//			case R.string.LTT_PON_MODEM_OSY:
//			case R.string.LTT_PON_TV_MODEM_NGD:
//			case R.string.LTT_PON_TV_NORMAL:
//			case R.string.LTT_PON_EYE2_MODEM_NGD:
//			case R.string.LTT_PON_EYE2_NORMAL:
//			case R.string.LTT_PON_IMS_MODEM_NGD:
//			case R.string.LTT_PON_IMS_NORMAL:
//			case R.string.LTT_VOICE_FAU:
//			case R.string.LTT_VOICE_GOD:
//			case R.string.SHLM_SIP:
//			case R.string.SHLM_WIP:
//			case R.string.myhkt_appointment_call1000:
//			case R.string.LGIM_INITPWD_RCUS:
//			case R.string.LGIM_FILL_RCUS:
			case R.string.ATIMF_HTLN_NUM:
//			case R.string.support_consumer_hotline:
//			case R.string.DLGM_RC_GENERAL:
			case R.string.support_consumer_hotline_no:
			case R.string.myhkt_linetest_hotline:
			case R.string.LGI_INACTIVE_CUST:
			case R.string.LGI_INACTIVE_SVEE:
				str = str.replaceAll("1000", "28886228");
				str = str.replaceAll("28834609", "28886228");
				break;
			}
		}
		return str;
	}

	public static String getCurrentModule() {
		return currentModule;
	}

	public static void setCurrentModule(String moduleId) {
		currentModule = moduleId;
	}

	public static final boolean getLiveChatDisclaimerFlag() {
		return needLiveChatDisclaimer;
	}

	public static final void setLiveChatDisclaimerFlag(boolean needDisclaimer) {
		needLiveChatDisclaimer = needDisclaimer;
	}

	// Get LnttCra from SharePreference, if empty String return new LnttAgent Object
	// TODO: Please update this functions
	/*public static final LnttAgent getPrefLnttAgent(Context cxt) {
	String strLnttAgent = ClnEnv.getPref(cxt, cxt.getString(R.string.CONST_PREF_LNTT_AGENT), "");
	//LnttAgent rlnttAgent = !"".equalsIgnoreCase(ClnEnv.getPref(cxt, cxt.getString(R.string.CONST_PREF_LNTT_AGENT), "")) ? (LnttAgent) Utils.deserialize(ClnEnv.getPref(cxt, cxt.getString(R.string.CONST_PREF_LNTT_AGENT), "")) : new LnttAgent();
	LnttAgent rlnttAgent = !"".equalsIgnoreCase(strLnttAgent) ? (LnttAgent) Utils.deserialize(strLnttAgent) : new LnttAgent();
	return rlnttAgent;
}

public static final boolean isLnttCompleted(Context cxt) {
	LnttAgent rlnttAgent = getPrefLnttAgent(cxt);
	if (rlnttAgent.getLnttCra() == null) return false;
	if ("".equalsIgnoreCase(rlnttAgent.getLnttCra().getSubscriptionRec().srv_num) || "".equalsIgnoreCase(rlnttAgent.getEndTimestamp())) {
		return false;
	} else {
		return true;
	}
}

// Check Lntt Result expired or not
public static final boolean isExpiredLnttResult(Context cxt, String LtrsTimestamp) {
	boolean debug = cxt.getResources().getBoolean(R.bool.DEBUG);
	SimpleDateFormat inputdf = new SimpleDateFormat(Utils.getString(cxt, R.string.input_datetime_format), Locale.US);
	Date ltrsDate, currDate;
	try {
		Calendar cal = Calendar.getInstance();
		currDate = new Date(System.currentTimeMillis());
		if (debug) Log.d("isExpiredLnttResult", "currDate = "+currDate.toString());
		ltrsDate = inputdf.parse(LtrsTimestamp);
		// TODO: test on expired case
		//lnrsDate = inputdf.parse("20140106135731");
		cal.setTime(ltrsDate);
		cal.add(Calendar.HOUR_OF_DAY, 2);
		ltrsDate = cal.getTime();
		if (debug) Log.d("isExpiredLnttResult", "ltrsDate = "+ltrsDate.toString());
		if (ltrsDate.compareTo(currDate) > 0) {
			return false;
		}
	} catch (Exception e) {
		// fail to convert date
		e.printStackTrace();
	}
	return true;
}

public static final void clearLnttService(Context cxt) {
	LineTestIntentService.lnttRtryCnt = 0;
	ClnEnv.setPref(cxt.getApplicationContext(), cxt.getString(R.string.CONST_PREF_LNTT_AGENT), "");
	ClnEnv.setPref(cxt.getApplicationContext(), cxt.getString(R.string.CONST_PREF_LNTT_NOTICEFLAG), false);

	// Remove Notification
	((NotificationManager) cxt.getSystemService(Context.NOTIFICATION_SERVICE)).cancel(cxt.getResources().getInteger(R.integer.CONST_NOTIFICATION_ID_LNTT));
}
	 */
	public static final void clearBillMsgService(Context cxt) {
		ClnEnv.setPref(cxt.getApplicationContext(), cxt.getString(R.string.CONST_PREF_BILLMSG_NOTICEFLAG), false);

		// Remove Notification
		//((NotificationManager) cxt.getSystemService(Context.NOTIFICATION_SERVICE)).cancel(cxt.getResources().getInteger(R.integer.CONST_NOTIFICATION_ID_BILL));
	}

	// Convert Object to String by Serializing Object
	public static <T extends Serializable> String serialize(T item) {
		final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
		final ObjectOutputStream objectOutputStream;
		try {
			objectOutputStream = new ObjectOutputStream(byteArrayOutputStream);
			objectOutputStream.writeObject(item);
			objectOutputStream.close();
			return Base64.encodeToString(byteArrayOutputStream.toByteArray(), Base64.DEFAULT);
		} catch (Exception e) {
			// fail to convert serializable Object
			return "";
		}
	}
	// Convert String to Object by deSerializing String
	public static <T extends Serializable> T deserialize(String data) {
		try {
			byte[] dataBytes = Base64.decode(data, Base64.DEFAULT);
			final ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(dataBytes);
			final ObjectInputStream objectInputStream = new ObjectInputStream(byteArrayInputStream);

			@SuppressWarnings({"unchecked"})
			final T obj = (T) objectInputStream.readObject();

			objectInputStream.close();
			return obj;
		} catch (Exception e){
			e.printStackTrace();
			return null;
		}
	}

	// Convert timeslot to datetime string for SR
	// Timeslot Code represents to corresponding timeslots [refer to CSP excel]
	// 09 : 0900 - 1100
	// 11 : 1100 - 1300
	// 14 : 1400 - 1600
	// 16 : 1600 - 1800
	// 18 : 1800 - 2000
	// 20 : 2000 - 2200
	// AM : 1000 - 1300
	// PM : 1400 - 1800
	public static final String getDateTime(String timeslot, String outputFormat, boolean isStartTime) {
		String	datetime = "";

		if ("09".equalsIgnoreCase(timeslot)) {
			datetime = isStartTime ? "090000" : "110000";
		} else if ("11".equalsIgnoreCase(timeslot)) {
			datetime = isStartTime ? "110000" : "130000";
		} else if ("14".equalsIgnoreCase(timeslot)) {
			datetime = isStartTime ? "140000" : "160000";
		} else if ("16".equalsIgnoreCase(timeslot)) {
			datetime = isStartTime ? "160000" : "180000";
		} else if ("18".equalsIgnoreCase(timeslot)) {
			datetime = isStartTime ? "180000" : "200000";
		} else if ("20".equalsIgnoreCase(timeslot)) {
			datetime = isStartTime ? "200000" : "220000";
		} else if ("AM".equalsIgnoreCase(timeslot)) {
			datetime = isStartTime ? "100000" : "130000";
		} else if ("PM".equalsIgnoreCase(timeslot)) {
			datetime = isStartTime ? "140000" : "180000";
		}

		return toDateString(datetime, "HHmmss", outputFormat);
	}

	// Output TimeSlot String format : dd-MMM HH:mm - HH:mm
	// TODO: Please rewrite this function
	public static final String getTimeSlotDateTime(Context cxt, String dateString, String timeslot){
		//String timeslotDate = toDateString(dateString, Utils.getString(cxt, R.string.input_date_format), Tool.DATEFMT_6);
		String timeslotDate = dateString;
		try {
			SimpleDateFormat inputdf = new SimpleDateFormat(Utils.getString(cxt, R.string.input_date_format), Locale.US);
			Date apptDate = inputdf.parse(dateString);
			//			inputdf = new SimpleDateFormat(Tool.DATEFMT_6, Locale.US);
			inputdf = new SimpleDateFormat("dd-MMM", Locale.US);
			timeslotDate = inputdf.format(apptDate);
		} catch (Exception e) {
			timeslotDate = dateString;
		}
		String timeslotTime = String.format("%s - %s", getDateTime(timeslot, getString(cxt, R.string.output_time_format), true), getDateTime(timeslot, getString(cxt, R.string.output_time_format), false));
		//special handling for time string that is "09:00 to 13:00" switch to "10:00 to 13:00", for display only
		timeslotTime = "09:00 - 13:00".equals(timeslotTime) ? "10:00 - 13:00" : timeslotTime;
		return String.format("%s %s", timeslotDate, timeslotTime).trim();
	}

	// Output TimeSlot String format : dd/MM/YYYY HH:mm - HH:mm
		// TODO: Please rewrite this function
		public static final String getTimeSlotDateTime1(Context cxt, String dateString, String timeslot){
			//String timeslotDate = toDateString(dateString, Utils.getString(cxt, R.string.input_date_format), Tool.DATEFMT_6);
			String timeslotDate = dateString;
			try {
				SimpleDateFormat inputdf = new SimpleDateFormat(Utils.getString(cxt, R.string.input_date_format), Locale.US);
				Date apptDate = inputdf.parse(dateString);
				//			inputdf = new SimpleDateFormat(Tool.DATEFMT_6, Locale.US);
				inputdf = new SimpleDateFormat("dd/MM/yyyy", Locale.US);
				timeslotDate = inputdf.format(apptDate);
//				Log.i("TImeslor", timeslotDate);
			} catch (Exception e) {
//				Log.i("TImeslor", e.toString());
				timeslotDate = dateString;
			}
			String timeslotTime = String.format("%s - %s", getDateTime(timeslot, getString(cxt, R.string.output_time_format), true), getDateTime(timeslot, getString(cxt, R.string.output_time_format), false));
			//special handling for time string that is "09:00 to 13:00" switch to "10:00 to 13:00", for display only
			timeslotTime = "09:00 - 13:00".equals(timeslotTime) ? "10:00 - 13:00" : timeslotTime;
			return String.format("%s %s", timeslotDate, timeslotTime).trim();
		}
		
	// Translate SR Validation Message
	public final static String interpretRC_SRMdu(Context cxt, String rRC) {
		if (rRC.equals("RC_SR_IVCTNAME")) {return Utils.getString(cxt, R.string.MYHKT_SR_ERR_INVALID_NAME);}
		if (rRC.equals("RC_SR_NONENG_NAME")) {return Utils.getString(cxt, R.string.MYHKT_SR_ERR_NOT_ENGLISH_NAME);}
		if (rRC.equals("RC_SR_IVCTNUM")) {return Utils.getString(cxt, R.string.MYHKT_SR_ERR_INVALID_TEL);}
		if (rRC.equals("RC_SR_IVAPPTTS")) {return Utils.getString(cxt, R.string.MYHKT_SR_ERR_INVALID_TIMESLOT);}
		if (rRC.equals("RC_SR_IVUPDTY")) {return Utils.getString(cxt, R.string.MYHKT_SR_ERR_NO_UPDATE_CHANGE);}

		return ClnEnv.getRPCErrMsg(cxt, rRC);
	}

	public static final String getIpAddress() {
		try {
			for (Enumeration en = NetworkInterface.getNetworkInterfaces(); en.hasMoreElements();) {
				NetworkInterface intf = (NetworkInterface) en.nextElement();
				for (Enumeration enumIpAddr = intf.getInetAddresses(); enumIpAddr.hasMoreElements();) {
					InetAddress inetAddress = (InetAddress) enumIpAddr.nextElement();
					if (!inetAddress.isLoopbackAddress() && inetAddress instanceof Inet4Address) {
						String ipAddress = inetAddress.getHostAddress().toString();
						//Log.e("IP address", "" + ipAddress);
						return ipAddress;
					}
				}
			}
		} catch (SocketException ex) {
			//Log.e("Socket exception in GetIP Address of Utilities", ex.toString());
		}
		return null;
	}

	public static final String sha256(String input, String salt) {
		//final String SALT = "1234567890";
		input = salt + input;
		try {
			MessageDigest digest = MessageDigest.getInstance("SHA-256");
			digest.reset();

			byte[] byteData = digest.digest(input.getBytes("UTF-8"));
			StringBuffer sb = new StringBuffer();

			for (int i = 0; i < byteData.length; i++){
				sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16).substring(1));
			}
			return sb.toString();
		} catch (Exception e) {
			return null;
		}
	}

	public static final String randomString( int length ) {
		String AB = "123456789";
		Random rnd = new Random();
		StringBuilder sb = new StringBuilder( length );
		for( int i = 0; i < length; i++ ) 
			sb.append( AB.charAt( rnd.nextInt(AB.length()) ) );
		return sb.toString();
	}

	public static final int dpToPx(int dp) {
		float px = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, Resources.getSystem().getDisplayMetrics());
		return (int) px;
	}

	public static final String getTimeToSeconds() {
		long timeUS = System.currentTimeMillis() * 1000;
		final SimpleDateFormat SDF = new SimpleDateFormat("yyyy/MM/dd'T'HH:mm:ss.SSS");
		final DecimalFormat DF = new DecimalFormat("000");
		return SDF.format(timeUS / 1000) + DF.format(timeUS % 1000);
	}

	//Search and remove punctuation between 2 strings from a pool of string 
	public static final String punctuationRemover(String str, String startPoint, String endPoint, String textToRemove) {
		Pattern pattern = Pattern.compile(startPoint + "(.*?)" + endPoint);
		Matcher matcher = pattern.matcher(str);
		StringBuffer sb = new StringBuffer(str.length());
		while (matcher.find()) {
			String result = matcher.group(1).replaceAll(textToRemove, "");
			matcher.appendReplacement(sb, startPoint + Matcher.quoteReplacement(result) + endPoint);
			//        System.out.println(matcher.group(1));
		}
		matcher.appendTail(sb);
		return sb.toString();
	}

	//scale image bitmap from drawable
	public static final Bitmap scaleImage(Context cxt, int imgResource, int width, int height) {
		Bitmap image = BitmapFactory.decodeResource(cxt.getResources(), imgResource);
		Bitmap resized = Bitmap.createScaledBitmap(image, width, height, true);
		return resized;
		//	Bitmap original = BitmapFactory.decodeResource(cxt.getResources(), imgResource);
		//	Bitmap b = Bitmap.createScaledBitmap(original, width, height, false);
		//	Drawable d = new BitmapDrawable(cxt.getResources(), b);
		//	return d;
	}

	public static final void scaleImage(ImageView view, float reduceScale) {
		// Get bitmap from the the ImageView.
		Bitmap bitmap = null;
		Drawable drawing = view.getDrawable();
		bitmap = ((BitmapDrawable) drawing).getBitmap();

		// Get current dimensions AND the desired bounding box
		int width = 0;
		int height = 0;
		width = bitmap.getWidth();
		height = bitmap.getHeight();
		//	    int bounding = dpToPx(250);
		//	    Log.i("Test", "original width = " + Integer.toString(width));
		//	    Log.i("Test", "original height = " + Integer.toString(height));
		//	    Log.i("Test", "bounding = " + Integer.toString(bounding));

		// Determine how much to scale: the dimension requiring less scaling is
		// closer to the its side. This way the image always stays inside your
		// bounding box AND either x/y axis touches it.  
		//	    float xScale = ((float) bounding) / width;
		//	    float yScale = ((float) bounding) / height;
		//	    float scale = (xScale <= yScale) ? xScale : yScale;
		//	    Log.i("Test", "xScale = " + Float.toString(xScale));
		//	    Log.i("Test", "yScale = " + Float.toString(yScale));
		//	    Log.i("Test", "scale = " + Float.toString(scale));

		// Create a matrix for the scaling and add the scaling data
		Matrix matrix = new Matrix();
		matrix.postScale(reduceScale, reduceScale);

		// Create a new bitmap and convert it to a format understood by the ImageView 
		Bitmap scaledBitmap = Bitmap.createBitmap(bitmap, 0, 0, width, height, matrix, true);
		int new_width = scaledBitmap.getWidth(); // re-use
		int new_height = scaledBitmap.getHeight(); // re-use
		BitmapDrawable result = new BitmapDrawable(scaledBitmap);
		//	    Log.i("Test", "scaled width = " + Integer.toString(width));
		//	    Log.i("Test", "scaled height = " + Integer.toString(height));

		// Apply the scaled bitmap
		view.setImageDrawable(result);

		// Now change ImageView's dimensions to match the scaled image
		ViewGroup.LayoutParams params = view.getLayoutParams(); 
		params.width = width;
		params.height = height;
		view.setLayoutParams(params);
		view.setScaleType(ScaleType.CENTER_INSIDE);
	}

	//Concat array
	public static <T> T[] concatArray(T[] first, T[] second) {
		T[] result = Arrays.copyOf(first, first.length + second.length);
		System.arraycopy(second, 0, result, first.length, second.length);
		return result;
	}
	public static String convertCsimAccNum(String accnum) {
		//convert c-sim account number to 14 digit account number
		accnum = "7700" + accnum;
		int sum = Integer.parseInt(Character.toString(accnum.charAt(0))) * 12;
		sum = sum + Integer.parseInt(Character.toString(accnum.charAt(1))) * 11;
		sum = sum + Integer.parseInt(Character.toString(accnum.charAt(2))) * 10;
		sum = sum + Integer.parseInt(Character.toString(accnum.charAt(3))) * 9;
		sum = sum + Integer.parseInt(Character.toString(accnum.charAt(4))) * 8;
		sum = sum + Integer.parseInt(Character.toString(accnum.charAt(5))) * 7;
		sum = sum + Integer.parseInt(Character.toString(accnum.charAt(6))) * 6;
		sum = sum + Integer.parseInt(Character.toString(accnum.charAt(7))) * 5;
		sum = sum + Integer.parseInt(Character.toString(accnum.charAt(8))) * 4;
		sum = sum + Integer.parseInt(Character.toString(accnum.charAt(9))) * 3;
		sum = sum + Integer.parseInt(Character.toString(accnum.charAt(10))) * 2;
		sum = sum + Integer.parseInt(Character.toString(accnum.charAt(11))) * 1;

		int checkdigit = 97 - (sum % 97);
		String sCheckDigit = Integer.toString(checkdigit).length() == 1 ?  "0"+Integer.toString(checkdigit) : Integer.toString(checkdigit);
		accnum = accnum + sCheckDigit;
		return accnum;
	}
	public static String convertPCDTVAccNum(String accnum) {
		//convert PCDTV account number to 14 digit account number
		accnum = "68" + accnum;
		int sum = Integer.parseInt(Character.toString(accnum.charAt(0))) * 12;
		sum = sum + Integer.parseInt(Character.toString(accnum.charAt(1))) * 11;
		sum = sum + Integer.parseInt(Character.toString(accnum.charAt(2))) * 10;
		sum = sum + Integer.parseInt(Character.toString(accnum.charAt(3))) * 9;
		sum = sum + Integer.parseInt(Character.toString(accnum.charAt(4))) * 8;
		sum = sum + Integer.parseInt(Character.toString(accnum.charAt(5))) * 7;
		sum = sum + Integer.parseInt(Character.toString(accnum.charAt(6))) * 6;
		sum = sum + Integer.parseInt(Character.toString(accnum.charAt(7))) * 5;
		sum = sum + Integer.parseInt(Character.toString(accnum.charAt(8))) * 4;
		sum = sum + Integer.parseInt(Character.toString(accnum.charAt(9))) * 3;
		sum = sum + Integer.parseInt(Character.toString(accnum.charAt(10))) * 2;
		sum = sum + Integer.parseInt(Character.toString(accnum.charAt(11))) * 1;

		int checkdigit = 97 - (sum % 97);
		String sCheckDigit = Integer.toString(checkdigit).length() == 1 ?  "0"+Integer.toString(checkdigit) : Integer.toString(checkdigit);
		accnum = accnum + sCheckDigit;
		return accnum;
	}
	


	public static void closeSoftKeyboard(Activity act, View focusView) {
		try {
			InputMethodManager inputManager = (InputMethodManager) act.getSystemService(Context.INPUT_METHOD_SERVICE);
			inputManager.hideSoftInputFromWindow(focusView.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
			inputManager = null;
		} catch (Exception e) {
			// fail to hide the keyboard
		}
	}
	public static void closeSoftKeyboard(Activity act) {
		try {
			InputMethodManager inputManager = (InputMethodManager) act.getSystemService(Context.INPUT_METHOD_SERVICE);
			inputManager.hideSoftInputFromWindow(act.getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
			inputManager = null;
		} catch (Exception e) {
			// fail to hide the keyboard
		}
	}

	// Get LnttCra from SharePreference, if empty String return new LnttAgent Object
	public static final LnttAgent getPrefLnttAgent(Context cxt) {
		String strLnttAgent = ClnEnv.getPref(cxt, cxt.getString(R.string.CONST_PREF_LNTT_AGENT), "");
		//LnttAgent rlnttAgent = !"".equalsIgnoreCase(ClnEnv.getPref(cxt, cxt.getString(R.string.CONST_PREF_LNTT_AGENT), "")) ? (LnttAgent) Utils.deserialize(ClnEnv.getPref(cxt, cxt.getString(R.string.CONST_PREF_LNTT_AGENT), "")) : new LnttAgent();
		LnttAgent rlnttAgent = !"".equalsIgnoreCase(strLnttAgent) ? (LnttAgent) Utils.deserialize(strLnttAgent) : new LnttAgent();
		return rlnttAgent;
	}

	public static final boolean isLnttCompleted(Context cxt) {
		LnttAgent rlnttAgent = getPrefLnttAgent(cxt);
		if (rlnttAgent.getLnttCra() == null) return false;
        return !"".equalsIgnoreCase(rlnttAgent.getLnttCra().getISubnRec().srvNum) && !"".equalsIgnoreCase(rlnttAgent.getEndTimestamp());
	}

	// Check Lntt Result expired or not
	public static final boolean isExpiredLnttResult(Context cxt, String LtrsTimestamp) {
		boolean debug = cxt.getResources().getBoolean(R.bool.DEBUG);
		SimpleDateFormat inputdf = new SimpleDateFormat(Utils.getString(cxt, R.string.input_datetime_format), Locale.US);
		Date ltrsDate, currDate;
		try {
			Calendar cal = Calendar.getInstance();
			currDate = new Date(System.currentTimeMillis());
			if (debug) Log.d("isExpiredLnttResult", "currDate = "+currDate.toString());
			ltrsDate = inputdf.parse(LtrsTimestamp);
			// TODO: test on expired case
			//lnrsDate = inputdf.parse("20140106135731");
			cal.setTime(ltrsDate);
			cal.add(Calendar.HOUR_OF_DAY, 2);
			ltrsDate = cal.getTime();
			if (debug) Log.d("isExpiredLnttResult", "ltrsDate = "+ltrsDate.toString());
			if (ltrsDate.compareTo(currDate) > 0) {
				return false;
			}
		} catch (Exception e) {
			// fail to convert date
			e.printStackTrace();
		}
		return true;
	}

	public static final void clearLnttService(Context cxt) {
//		Log.i("Utils ", "Clean Lntt");
		LineTestIntentService.lnttRtryCnt = 0;
		ClnEnv.setPref(cxt.getApplicationContext(), cxt.getString(R.string.CONST_PREF_LNTT_AGENT), "");
		ClnEnv.setPref(cxt.getApplicationContext(), cxt.getString(R.string.CONST_PREF_LNTT_NOTICEFLAG), false);

		// Remove Notification
		((NotificationManager) cxt.getSystemService(Context.NOTIFICATION_SERVICE)).cancel(cxt.getResources().getInteger(R.integer.CONST_NOTIFICATION_ID_LNTT));
	}

	public final static int getLobType(String lob) {
		if (SubnRec.LOB_LTS.equalsIgnoreCase(lob)) return R.string.CONST_LOB_LTS;
		else if (SubnRec.LOB_101.equalsIgnoreCase(lob)) return R.string.CONST_LOB_1010;
		else if (SubnRec.LOB_MOB.equalsIgnoreCase(lob)) return R.string.CONST_LOB_MOB;
		else if (SubnRec.LOB_IOI.equalsIgnoreCase(lob)) return R.string.CONST_LOB_IOI;
		else if (SubnRec.LOB_O2F.equalsIgnoreCase(lob)) return R.string.CONST_LOB_O2F;
		else if (SubnRec.LOB_PCD.equalsIgnoreCase(lob)) return R.string.CONST_LOB_PCD;
		else if (SubnRec.LOB_TV.equalsIgnoreCase(lob)) return R.string.CONST_LOB_TV;
		else return 0;
	}
	
	public final static int theme(int resId, int lob) {
		int res = resId;
		if (ClnEnv.isMyMobFlag()) {
			switch(resId) {
				case R.drawable.icon_mms:
					if (lob == R.string.CONST_LOB_1010) {
						res = R.drawable.icon_mms_1010;
					} else {
						res = R.drawable.icon_mms_csl;
					}
				break;
				case R.drawable.icon_sms:
					if (lob == R.string.CONST_LOB_1010) {
						res = R.drawable.icon_sms_1010;
					} else {
						res = R.drawable.icon_sms_csl;
					}
				break;
				case R.drawable.icon_voicecall:
					if (lob == R.string.CONST_LOB_1010) {
						res = R.drawable.icon_voicecall_1010;
					} else {
						res = R.drawable.icon_voicecall_csl;
					}
				break;
				case R.drawable.icon_videocall:
					if (lob == R.string.CONST_LOB_1010) {
						res = R.drawable.icon_videocall_1010;
					} else {
						res = R.drawable.icon_videocall_csl;
					}
				break;
				case R.drawable.billinfo_icon:
					if (lob == R.string.CONST_LOB_1010) {
						res = R.drawable.billinfo_icon_1010;
					} else {
						res = R.drawable.billinfo_icon_csl;
					}
				break;
				case R.drawable.icon_topup:
					if (lob == R.string.CONST_LOB_1010) {
						res = R.drawable.icon_topup_1010;
					} else {
						res = R.drawable.icon_topup_csl;
					}
				break;
				case R.drawable.icon_mobusage:
					if (lob == R.string.CONST_LOB_1010) {
						res = R.drawable.icon_mobusage_1010;
					} else {
						res = R.drawable.icon_mobusage_csl;
					}
				break;
				case R.drawable.icon_bbmail:
					if (lob == R.string.CONST_LOB_1010) {
						res = R.drawable.icon_bbmail_1010;
					} else {
						res = R.drawable.icon_bbmail_csl;
					}
				break;
				case R.drawable.icon_others:
					if (lob == R.string.CONST_LOB_1010) {
						res = R.drawable.icon_others_1010;
					} else {
						res = R.drawable.icon_others_csl;
					}
				break;
			}
		}
		return res;
	}

	public static void showLog(String tag , String msg) {
		int length = msg.toCharArray().length;
		int row  = (int) Math.floor((double)length/2000);
		if (msg.toCharArray().length < 2000) {
			Log.i(tag, msg);
		} else {			
			for (int i=0; i < row; i++) {
				if (i !=row-1) {
					Log.i(tag, msg.substring(i*2000, (i+1)*2000));
				} else {
					Log.i(tag, msg.substring((i)*2000, length));
				}
			}
		}
	}

	// Compare T-n Days
	public final static boolean CompareDateAdd3(Context cxt, String apptcsdt, String gettodaydt, String srcDateFormat) {
		SimpleDateFormat inputdf = new SimpleDateFormat(getString(cxt, R.string.input_datetime_format), Locale.US);
		SimpleDateFormat sdf = new SimpleDateFormat(srcDateFormat);
		
		Date apptcsdtDate, gettodaydtDate;
		try {
			//converted apptcsdt to correct date format
			Date d = sdf.parse(apptcsdt);
			sdf.applyPattern(getString(cxt, R.string.input_datetime_format));
			String convertedApptcsdt = sdf.format(d);
			apptcsdt = convertedApptcsdt;

			apptcsdtDate = inputdf.parse(apptcsdt);
			gettodaydtDate = inputdf.parse(gettodaydt);

			// Reset Time = 00:00 before compare
			Calendar cal = Calendar.getInstance();
			cal.setTime(apptcsdtDate);
			cal.set(Calendar.HOUR_OF_DAY, 0);  
			cal.set(Calendar.MINUTE, 0);  
			cal.set(Calendar.SECOND, 0);  
			cal.set(Calendar.MILLISECOND, 0);  
			apptcsdtDate = cal.getTime();

			cal.setTime(gettodaydtDate);
			cal.set(Calendar.HOUR_OF_DAY, 0);  
			cal.set(Calendar.MINUTE, 0);  
			cal.set(Calendar.SECOND, 0);  
			cal.set(Calendar.MILLISECOND, 0);  
			gettodaydtDate = cal.getTime();

			cal.setTime(gettodaydtDate);
			cal.add(Calendar.DATE, ClnEnv.getPref(cxt.getApplicationContext(), getString(cxt, R.string.CONST_PREF_APPTIND_DAYS), 3));
			gettodaydtDate = cal.getTime();

			if (apptcsdtDate.compareTo(gettodaydtDate) <= 0) return true;
		} catch (Exception e) {
			// fail to convert day
			return false;
		}
		return false;
	}

	//Search for matching SubnRec (for Alias editing in service list)
	public static boolean matchSubnRec(SubnRec subnRec, SubnRec rSubnRec) {
        return subnRec.acctNum.equalsIgnoreCase(rSubnRec.acctNum) &&
                //				subnRec.acct_ty.equalsIgnoreCase(rSubnRec.acct_ty) &&
                subnRec.assoc.equalsIgnoreCase(rSubnRec.assoc) &&
                //				subnRec.auth_ph.equalsIgnoreCase(rSubnRec.auth_ph) &&
                subnRec.bsn.equalsIgnoreCase(rSubnRec.bsn) &&
                subnRec.createPsn.equalsIgnoreCase(rSubnRec.createPsn) &&
                subnRec.createTs.equalsIgnoreCase(rSubnRec.createTs) &&
                subnRec.cusNum.equalsIgnoreCase(rSubnRec.cusNum) &&
                subnRec.custRid == rSubnRec.custRid &&
                subnRec.datCd.equalsIgnoreCase(rSubnRec.datCd) &&
                subnRec.domainTy.equalsIgnoreCase(rSubnRec.domainTy) &&
                //				subnRec.eff_begdate.equalsIgnoreCase(rSubnRec.eff_begdate) &&
                //				subnRec.eff_enddate.equalsIgnoreCase(rSubnRec.eff_enddate) &&
                subnRec.eyeGrp.equalsIgnoreCase(rSubnRec.eyeGrp) &&
                subnRec.fsa.equalsIgnoreCase(rSubnRec.fsa) &&
                subnRec.ind2l2b.equalsIgnoreCase(rSubnRec.ind2l2b) &&
                subnRec.indEye.equalsIgnoreCase(rSubnRec.indEye) &&
                subnRec.indPcd.equalsIgnoreCase(rSubnRec.indPcd) &&
                subnRec.indTv.equalsIgnoreCase(rSubnRec.indTv) &&
                subnRec.instAdr.equalsIgnoreCase(rSubnRec.instAdr) &&
                //				subnRec.ivr_pwd.equalsIgnoreCase(rSubnRec.ivr_pwd) &&
                subnRec.lastupdPsn.equalsIgnoreCase(rSubnRec.lastupdPsn) &&
                subnRec.lastupdTs.equalsIgnoreCase(rSubnRec.lastupdTs) &&
                //				subnRec.line_status.equalsIgnoreCase(rSubnRec.line_status) &&
                subnRec.lob.equalsIgnoreCase(rSubnRec.lob) &&
                subnRec.netLoginId.equalsIgnoreCase(rSubnRec.netLoginId) &&
                //				subnRec.pri_login_ind.equalsIgnoreCase(rSubnRec.pri_login_ind) &&
                subnRec.priMob.equalsIgnoreCase(rSubnRec.priMob) &&
                subnRec.refFsa2l2b.equalsIgnoreCase(rSubnRec.refFsa2l2b) &&
                subnRec.rev == rSubnRec.rev &&
                subnRec.rid == rSubnRec.rid &&
                subnRec.sipSubr.equalsIgnoreCase(rSubnRec.sipSubr) &&
                subnRec.srvId.equalsIgnoreCase(rSubnRec.srvId) &&
                subnRec.srvNum.equalsIgnoreCase(rSubnRec.srvNum) &&
                subnRec.storeTy.equalsIgnoreCase(rSubnRec.storeTy) &&
                subnRec.systy.equalsIgnoreCase(rSubnRec.systy) &&
                subnRec.tariff.equalsIgnoreCase(rSubnRec.tariff) &&
                subnRec.tos.equalsIgnoreCase(rSubnRec.tos) &&
                subnRec.wipCust.equalsIgnoreCase(rSubnRec.wipCust);
    }
	
	public static boolean isCsimOnly() {
		int csimCount = 0;
		if (ClnEnv.isLoggedIn()) {
			if (ClnEnv.getLgiCra() != null) {
				if (ClnEnv.getLgiCra().getOQualSvee().getSubnRecAry().length == 0) {
					return false;
				} else {
					for (int i = 0; i < ClnEnv.getLgiCra().getOQualSvee().getSubnRecAry().length; i++) {
						if (!(SubnRec.LOB_101.equals(ClnEnv.getLgiCra().getOQualSvee().getSubnRecAry()[i].lob) || SubnRec.LOB_O2F.equals(ClnEnv.getLgiCra().getOQualSvee().getSubnRecAry()[i].lob))) {
							return false;
						} else {
							csimCount++;
						}
					}
                    return csimCount != 0;
				}
			}
		}
		return false;
	}
	
	//for banner image
	public static void setImageDrawable(Context ctx,final ImageView img,String fileName) {
		DisplayMetrics metrics = ctx.getResources().getDisplayMetrics();
		
		String pathName = "/data/data/"+ctx.getPackageName()+"/";
		if (ClnEnv.getAppLocale(ctx).toLowerCase(Locale.US).startsWith(ctx.getString(R.string.CONST_LOCALE_ZH))) {
			pathName = pathName+"zh_"+fileName+".png";
		}else{
			pathName = pathName+"en_"+fileName+".png";
		}
		Bitmap myBitmap = BitmapFactory.decodeFile(pathName);
//		Log.d("Utils.setImageDrawable", String.format("Height : %d , Width : %d", myBitmap.getHeight(), myBitmap.getWidth()));
		if(myBitmap != null) {
			myBitmap.getWidth();
			img.setImageBitmap(Bitmap.createScaledBitmap(myBitmap,(int)(((double)myBitmap.getWidth() / 2.0 )*metrics.density), (int)(((double)myBitmap.getHeight() / 2.0 )*metrics.density), false));
			/*if (fileName.contains("T")) {
				//scaling for ticket image
				img.setImageBitmap(Bitmap.createScaledBitmap(myBitmap,(int)(279.0*metrics.density), (int)(155.5*metrics.density), false));
			} else if (fileName.contains("G") || fileName.contains("F") || fileName.contains("F")) {
				//scaling for G8888 - 1,000,000 club point
				img.setImageBitmap(Bitmap.createScaledBitmap(myBitmap,(int)(279.0*metrics.density), (int)(77*metrics.density), false));
			} else {
				//scaling for points image
			    img.setImageBitmap(Bitmap.createScaledBitmap(myBitmap,(int)(169.0*metrics.density), (int)(134.5*metrics.density), false));
			}*/
		} else {
			img.setImageBitmap(null);
		}
	}
	
	public static int getLobIcon(int lob, int ltsType){
		//There will be no acctAgent if it come from line test 
		//		int lobType = acctAgent.getLobType();
		if (lob == R.string.CONST_LOB_1010 || lob == R.string.CONST_LOB_IOI) {
			return R.drawable.logo_1010;
		} else if (lob == R.string.CONST_LOB_MOB || lob == R.string.CONST_LOB_O2F) {
			return R.drawable.logo_csl;
		} else if (lob == R.string.CONST_LOB_LTS) {
//			if (ltsType != 0) {
//				switch (ltsType) {
//				case R.string.CONST_LTS_FIXEDLINE:
//					return R.drawable.logo_fixedline;
//	
//				case R.string.CONST_LTS_EYE:
//					return R.drawable.logo_fixedline;
//	
//				case R.string.CONST_LTS_IDD0060:
//					return R.drawable.logo_iddservices;
//	
//				case R.string.CONST_LTS_CALLINGCARD:
//					return R.drawable.logo_iddservices;
//	
//				case R.string.CONST_LTS_ONECALL:
//					return R.drawable.logo_fixedline;
//	
//				case R.string.CONST_LTS_ICFS:
//					return R.drawable.logo_iddservices;
//	
//				default:
//					return R.drawable.logo_fixedline;
//				}
//			} else {
//				return R.drawable.logo_fixedline;
//			}
			return R.drawable.lob_lts_tel;
		} else if (lob == R.string.CONST_LOB_PCD) {
			return R.drawable.logo_netvigator2;
		} else if (lob == R.string.CONST_LOB_TV) {
			return R.drawable.logo_now;
		} else if(lob == R.string.CONST_LOB_IMS) {
			return R.drawable.lob_ims;
		}
		return 0;		
	}
	
	//lob icon
	public static int getLobIcon(int lob, int ltsType, String tos, boolean isZh){
		//There will be no acctAgent if it come from line test 
		//		int lobType = acctAgent.getLobType();
		if (lob == R.string.CONST_LOB_1010 || lob == R.string.CONST_LOB_IOI) {
			return R.drawable.logo_1010;
		} else if (lob == R.string.CONST_LOB_MOB || lob == R.string.CONST_LOB_O2F) {
			return R.drawable.logo_csl;
		} else if (lob == R.string.CONST_LOB_LTS) {
//			if (ltsType != 0) {
//				switch (ltsType) {
//				case R.string.CONST_LTS_FIXEDLINE:
//					return R.drawable.logo_fixedline;
//	
//				case R.string.CONST_LTS_EYE:
//					return R.drawable.logo_fixedline;
//	
//				case R.string.CONST_LTS_IDD0060:
//					return R.drawable.logo_iddservices;
//	
//				case R.string.CONST_LTS_CALLINGCARD:
//					return R.drawable.logo_iddservices;
//	
//				case R.string.CONST_LTS_ONECALL:
//					return R.drawable.logo_fixedline;
//	
//				case R.string.CONST_LTS_ICFS:
//					return R.drawable.logo_iddservices;
//	
//				default:
//					return R.drawable.logo_fixedline;
//				}
//			} else {
//				return R.drawable.logo_fixedline;
//			}
			if(tos.equals(SubnRec.TOS_LTS_OCM)) {
				return R.drawable.lob_lts_ocmc_nav;
			} else {
				return R.drawable.lob_lts_tel;
			}
		} else if (lob == R.string.CONST_LOB_PCD) {
			return R.drawable.logo_netvigator2;
		} else if (lob == R.string.CONST_LOB_TV) {
			return R.drawable.logo_now;
		} else if(lob == R.string.CONST_LOB_IMS) {
			return R.drawable.lob_ims;
		}
		return 0;		
	}
	
	
	public final static String getLtsTtypeRightText(Context ctx, int ltsType) {
		String title;
		switch (ltsType) {
		case R.string.CONST_LTS_FIXEDLINE:
			title = ctx.getResources().getString(R.string.myhkt_lts_fixedline);
			break;
		case R.string.CONST_LTS_EYE:
			title = ctx.getResources().getString(R.string.myhkt_lts_eye);
			break;
		case R.string.CONST_LTS_IDD0060:
			title = ctx.getResources().getString(R.string.myhkt_lts_idd0060);
			break;
		case R.string.CONST_LTS_CALLINGCARD:
			title = ctx.getResources().getString(R.string.myhkt_lts_callingcard);
			break;
		case R.string.CONST_LTS_ONECALL:
			title = ctx.getResources().getString(R.string.myhkt_lts_onecall);
			break;
		case R.string.CONST_LTS_ICFS:
			title = ctx.getResources().getString(R.string.myhkt_lts_icfs);
			break;
		default:
			title = "";			
			break;
		}
		return title;
	}
	
	public static String mbToGb(double mb, boolean isRoundDown){
		//changing double mb value to gb if nesscessary
		String result = "";
		
		if(mb >= 1024) { //GB
			mb = mb / 1024; 
			NumberFormat decFormat = new DecimalFormat("#.00"); 
			return isRoundDown ? decFormat.format(Math.floor(mb*100)/100) + "GB" : decFormat.format((mb*100)/100) + "GB";
		} else { //MB
			//as is
			return ((int) Math.floor(mb)) + "MB";
		}
		
	}
	
	/**
	 * 06-14-17: Moiz
	 * @param remainingMB
	 * @param remainingKB
	 * @return DataUsage depending on size, if in remainingKV <= 1024 return the KB otherwise return MB
	 */
	public static String getRemainingDataUsage(String remainingMB, String remainingKB) {
		String dataUsage = "---";
		int dataNumberFormat = 0;
		if(remainingKB != null && !TextUtils.isEmpty(remainingKB)) {
			dataNumberFormat = Integer.parseInt(remainingKB.replace(",", ""));
			if(dataNumberFormat <= 1024) {
				dataUsage = remainingKB + "KB";
			} else {
				// return the MB if not empty
				dataUsage = getRemaingDataUsageMB(remainingMB);
			}
		} else {
			// return the MB if not empty
			dataUsage = getRemaingDataUsageMB(remainingMB);
		}
		
		return dataUsage;
	}
	
	private static String getRemaingDataUsageMB(String remainingMB) {
		String dataUsage = "---";
		
		if(remainingMB != null && !TextUtils.isEmpty(remainingMB)) {
			dataUsage = remainingMB + "MB";
		}
		
		return dataUsage;
	}
	
	/**
	 * 06-13-17: Moiz
	 * @param context
	 * Description: Change the locale to Chinese, 
	 * This method will be called once the webview set the locale to default English on the first launch
	 * This fix apply only apply in the API 24 and above (Nougat)
	 */
	public static void switchToZhLocale(Context context) {
		
		Locale locale = new Locale("zh");
        Locale.setDefault(locale);
        Configuration config = new Configuration();

        if (Build.VERSION.SDK_INT >= 24) {
            config.setLocale(locale);
        } else {
            config.locale = locale;
        }
        
        context.getResources().updateConfiguration(config, context.getResources().getDisplayMetrics());
		Log.d("Language Utils ", context.getResources().getString(R.string.myhkt_lang));

    }
}