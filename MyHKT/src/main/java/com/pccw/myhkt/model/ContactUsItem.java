package com.pccw.myhkt.model;

public class ContactUsItem {

    private int title;
    private String hardTitle;
    private int subtitle;
    private String hardSubtitle;
    private int icon;
    private int actionText;
    private ItemClickListener itemClickListener;

    public ContactUsItem(int title, int subtitle, int icon, int actionText,
                         ItemClickListener itemClickListener) {
        this.title = title;
        this.hardTitle = "";
        this.subtitle = subtitle;
        this.hardSubtitle = "";
        this.icon = icon;
        this.actionText = actionText;
        this.itemClickListener = itemClickListener;
    }

    public ContactUsItem(String hardTitle, int subtitle, int icon, int actionText,
                         ItemClickListener itemClickListener) {
        this.title = -1;
        this.hardTitle = hardTitle;
        this.subtitle = subtitle;
        this.hardSubtitle = "";
        this.icon = icon;
        this.actionText = actionText;
        this.itemClickListener = itemClickListener;
    }

    public ContactUsItem(int title, String hardSubtitle, int icon, int actionText,
                         ItemClickListener itemClickListener) {
        this.title = title;
        this.hardTitle = "";
        this.subtitle = -1;
        this.hardSubtitle = hardSubtitle;
        this.icon = icon;
        this.actionText = actionText;
        this.itemClickListener = itemClickListener;
    }

    public ContactUsItem(String hardTitle, String hardSubtitle, int icon, int actionText,
                         ItemClickListener itemClickListener) {
        this.title = -1;
        this.hardTitle = hardTitle;
        this.subtitle = -1;
        this.hardSubtitle = hardSubtitle;
        this.icon = icon;
        this.actionText = actionText;
        this.itemClickListener = itemClickListener;
    }

    public int getTitle() {
        return title;
    }

    public void setTitle(int title) {
        this.title = title;
    }

    public String getHardTitle() {
        return hardTitle;
    }

    public void setHardTitle(String hardTitle) {
        this.hardTitle = hardTitle;
    }

    public int getSubtitle() {
        return subtitle;
    }

    public void setSubtitle(int subtitle) {
        this.subtitle = subtitle;
    }

    public String getHardSubtitle() {
        return hardSubtitle;
    }

    public void setHardSubtitle(String hardSubtitle) {
        this.hardSubtitle = hardSubtitle;
    }

    public int getIcon() {
        return icon;
    }

    public void setIcon(int icon) {
        this.icon = icon;
    }

    public int getActionText() {
        return actionText;
    }

    public void setActionText(int actionText) {
        this.actionText = actionText;
    }

    public ItemClickListener getItemClickListener() {
        return itemClickListener;
    }

    public void setItemClickListener(ItemClickListener itemClickListener) {
        this.itemClickListener = itemClickListener;
    }

    public interface ItemClickListener {
        void onItemClick();
    }

}
