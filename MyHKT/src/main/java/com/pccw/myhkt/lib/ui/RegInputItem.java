package com.pccw.myhkt.lib.ui;

import com.androidquery.AQuery;
import com.pccw.myhkt.R;

import android.content.Context;
import android.content.res.TypedArray;
import android.os.Build;
import android.text.Editable;
import android.text.InputFilter;
import android.text.InputType;
import android.text.TextWatcher;
import android.text.method.DigitsKeyListener;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.LinearLayout;

public class RegInputItem extends LinearLayout {

	private AQuery aq;

	public RegInputItem(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
	}

	public RegInputItem(Context context, AttributeSet attrs) {
		super(context, attrs);
		initViews(context, attrs);
	}

	public RegInputItem(Context context, AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);
		initViews(context, attrs);
	}

	private void initViews(final Context context, AttributeSet attrs) {
		TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.HomeImageButton, 0, 0);
		aq = new AQuery(this);

		LayoutInflater.from(context).inflate(R.layout.edittext_reg_input, this);
		try {
			// Style Initialize
			String text = (String) a.getText(R.styleable.RegInputItem_title1);
			aq.id(R.id.reg_input_layouttxt).text(text!=null ? text :"");
			String hint = (String) a.getText(R.styleable.RegInputItem_hint);
			aq.id(R.id.reg_input_layoutet).getEditText().setHint(hint!=null ? hint :"");
			String input = (String) a.getText(R.styleable.RegInputItem_input);
			aq.id(R.id.reg_input_layoutet).getEditText().setText(input!=null ? input :"");		
			aq.id(R.id.home_imageview).image(a.getDrawable(R.styleable.HomeImageButton_android_src));

		} finally {
			a.recycle();
		}
	}

	public void setHint(String hint){
		aq.id(R.id.reg_input_layoutet).getEditText().setHint(hint!=null ? hint :"");
	}

	public void initViews(final Context context, int height, String title, String hint, String input) {
		initViews(context, height, title, hint, input , InputType.TYPE_CLASS_TEXT) ;
	}

	public void initViews(final Context context, String title, String hint, String input) {
		initViews(context, (int)this.getResources().getDimension(R.dimen.reg_input_height), title, hint, input , InputType.TYPE_CLASS_TEXT) ;
	}

	public void initViews(final Context context, String title, String hint, String input , int inputType) {
		initViews(context, (int)this.getResources().getDimension(R.dimen.reg_input_height), title, hint, input , inputType) ;
	}

	public void initViews(final Context context, int height, String title, String hint, String input , final int inputType) {
		LayoutInflater.from(context).inflate(R.layout.edittext_reg_input, this, false);
			
		aq.id(R.id.reg_input_layouttxt).text(title);
		if (title ==null || title.equals("")){ aq.id(R.id.reg_input_layouttxt).gone(); }
		if(Build.VERSION.SDK_INT <= Build.VERSION_CODES.KITKAT) {aq.id(R.id.reg_input_layoutet).getEditText().setHintTextColor(this.getResources().getColor(R.color.hkt_txtcolor_grey_disable));}
		aq.id(R.id.reg_input_layoutet).getEditText().setHint(hint);
		aq.id(R.id.reg_input_layoutet).getEditText().setText(input);
		aq.id(R.id.reg_input_layoutet).getEditText().setInputType(inputType);
		aq.id(R.id.reg_input_layoutet).getEditText().setTextColor(this.getResources().getColor(R.color.hkt_brightblue));
		if (inputType == InputType.TYPE_TEXT_VARIATION_PASSWORD) {
			aq.id(R.id.reg_input_layoutet).getEditText().setTransformationMethod(new PasswordTransformationMethod());
			aq.id(R.id.reg_input_button_show_pwd).visibility(View.VISIBLE);
		}
		//aq.id(R.id.reg_input_layouttxt).height(height/2, false);
		//aq.id(R.id.reg_input_layoutet).height(height/2, false);
		
		aq.id(R.id.reg_input_layoutet).getEditText().setOnFocusChangeListener(new View.OnFocusChangeListener() {
			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				aq.id(R.id.reg_input_buttonclear).visibility(View.GONE);
				if (hasFocus && aq.id(R.id.reg_input_layoutet).getEditText().getText().toString().trim().length() != 0 &&
						inputType != InputType.TYPE_TEXT_VARIATION_PASSWORD) {

					aq.id(R.id.reg_input_buttonclear).visibility(View.VISIBLE);
				}
			}
		});
		aq.id(R.id.reg_input_layoutet).getEditText().addTextChangedListener(new TextWatcher()
		{
			//Enable clear button on text change
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				if (s.length() > 0 && inputType != InputType.TYPE_TEXT_VARIATION_PASSWORD)
						aq.id(R.id.reg_input_buttonclear).visibility(View.VISIBLE);
				 else
					aq.id(R.id.reg_input_buttonclear).visibility(View.GONE);
			}
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) { }
			@Override
			public void afterTextChanged(Editable s) { }
		});
		
		aq.id(R.id.reg_input_buttonclear).clicked(new View.OnClickListener()
		{
			@Override
			public void onClick(View v) {
				aq.id(R.id.reg_input_layoutet).text("");
			}
		});
		if (!aq.id(R.id.reg_input_layoutet).getEditText().isFocused())
			aq.id(R.id.reg_input_buttonclear).visibility(View.GONE);


		aq.id(R.id.reg_input_button_show_pwd).getCheckBox().setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

				if(isChecked) {
					aq.id(R.id.reg_input_layoutet).getEditText().setTransformationMethod(new HideReturnsTransformationMethod());
				} else {
					aq.id(R.id.reg_input_layoutet).getEditText().setTransformationMethod(new PasswordTransformationMethod());
				}
				//set the cursor to the end of the text
				aq.id(R.id.reg_input_layoutet).getEditText().setSelection(aq.id(R.id.reg_input_layoutet).getEditText().getText().length());
			}
		});
	}

	public String getInput(){
		return aq.id(R.id.reg_input_layoutet).getEditText().getEditableText().toString().trim();
	}
	
	public void setInputType(int inputType) {
		aq.id(R.id.reg_input_layoutet).getEditText().setInputType(inputType);
	}
	
	public void setKeyListener(DigitsKeyListener digitsKeyListener) {
		aq.id(R.id.reg_input_layoutet).getEditText().setKeyListener(digitsKeyListener);
	}
	
	public void setText(String text) {
		aq.id(R.id.reg_input_layouttxt).text(text);
	}
	
	public void setEditText(String text) {
		aq.id(R.id.reg_input_layoutet).text(text);
		if (!aq.id(R.id.reg_input_layoutet).getEditText().isFocused())
			aq.id(R.id.reg_input_buttonclear).visibility(View.GONE);
	}
	
	public void setTexts(String text, String hints) {
		aq.id(R.id.reg_input_layouttxt).text(text);
		aq.id(R.id.reg_input_layoutet).getEditText().setHint(hints);
	}

	public void setMaxLength(int max) {
		InputFilter[] fileter = new InputFilter[1] ;
		fileter[0] = 	new InputFilter.LengthFilter(max);
		aq.id(R.id.reg_input_layoutet).getEditText().setFilters(fileter);
	}	
	
	public void setEditTextColor(int resColorID) {
		aq.id(R.id.reg_input_layoutet).getEditText().setTextColor(this.getResources().getColor(resColorID));
	}

	public void setEditTextPadding(int left, int top, int right, int bottom) {
		aq.id(R.id.reg_input_layoutet).getEditText().setPadding(left, top, right, bottom);
	}

	public void setEditTextMargin(int left, int top, int right, int bottom) {
		aq.id(R.id.reg_input_layout).getView().setPadding(left, top, right, bottom);
	}

}
