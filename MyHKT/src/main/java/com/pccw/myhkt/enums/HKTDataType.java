package com.pccw.myhkt.enums;

public enum HKTDataType {
    HKTDataTypeEntitled(0),
    HKTDataTypeRemaining(1),
    HKTDataTypeUsed(2);

    private int _value;

    HKTDataType(int Value) {
        this._value = Value;
    }

    public int getValue() {
        return _value;
    }

    public static HKTDataType fromInt(int i) {
        for (HKTDataType b : HKTDataType.values()) {
            if (b.getValue() == i) { return b; }
        }
        return null;
    }
}
