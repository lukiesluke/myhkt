package com.pccw.myhkt.model;

public class HomeButtonItem {
	
//	public final static int MAINMENU_MY_ACC = 0;
//	public final static int MAINMENU_MY_PROFILE = 1;
//	public final static int MAINMENU_MY_APPOINT = 2;
//	public final static int MAINMENU_MY_MOB = 3;
//	public final static int MAINMENU_IDD = 4;
//	public final static int MAINMENU_THECLUB = 5;
//	public final static int MAINMENU_DIRECTORY = 6;
//	public final static int MAINMENU_SHOPS = 7;
//	public final static int MAINMENU_CHAT = 8;
//	public final static int MAINMENU_CONTACT = 9;
	
	public enum MAINMENU {
		MYACCT,
		MYPROFILE,
		MYAPPT,
		MYMOB,
		THECLUB,
		TAPNGO,
		DIRECTINQ,
		IDD,
		SHOPS,
		CONTACTUS,
		NULL,
		SETTING,
		SOLUTION,
		NEWS
	}
	
	
	public int 		image;
	public String  	text;
	public int 		textSize;
	public int		bgcolor;
	public int 		txtColor;
//	public int 		type;
	public MAINMENU type;
		
	public HomeButtonItem(int image, String text, int textSize, int bgcolor , int txtColor, MAINMENU type) {
		this.image = image;
		this.text = text;
		this.textSize = textSize;
		this.bgcolor = bgcolor;
		this.txtColor = txtColor;
		this.type = type;
	}
	
	public void setImageResource(int resId) {
		this.image = resId;
	}
}