package com.pccw.myhkt.util


import android.Manifest
import android.app.Activity
import android.app.DownloadManager
import android.content.Context
import android.content.Context.DOWNLOAD_SERVICE
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Build
import android.os.Environment
import android.webkit.CookieManager
import android.webkit.WebResourceRequest
import android.webkit.WebResourceResponse
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.Toast
import androidx.core.app.ActivityCompat
import com.google.gson.Gson
import com.pccw.myhkt.DialogHelper
import com.pccw.myhkt.R
import com.pccw.myhkt.Utils
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.OkHttpClient
import okhttp3.Request
import java.io.File
import java.io.IOException
import java.io.InputStream
import java.net.HttpCookie
import java.net.URLDecoder
import java.nio.charset.StandardCharsets
import java.util.regex.Pattern


/**
 *  Created by tommyli on 3/10/2018
 */

class LiveChatWebViewClient(val context: Context) : WebViewClient() {

    @Deprecated(
        "Deprecated in Java", ReplaceWith(
            "if (filterRequest(view, url)) null else super.shouldInterceptRequest(view, url)",
            "android.webkit.WebViewClient"
        )
    )
    override fun shouldInterceptRequest(view: WebView, url: String): WebResourceResponse? {
        return if (filterRequest(view, url)) null else super.shouldInterceptRequest(view, url)
    }

    override fun shouldInterceptRequest(
        view: WebView,
        request: WebResourceRequest
    ): WebResourceResponse? {
        return if (filterRequest(
                view,
                request.url.toString()
            )
        ) null else super.shouldInterceptRequest(view, request)
    }

    private fun checkPermission(): Boolean {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            return true
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (context.checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                if ((context as Activity).shouldShowRequestPermissionRationale(Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                    // show an alert dialog
                    context.runOnUiThread {
                        DialogHelper.createSimpleDialog(
                            context,
                            context.getResources()
                                .getString(R.string.permission_denied_setting_enabled),
                            Utils.getString(context, R.string.btn_ok)
                        )
                    }
                } else {
                    // Request permission
                    ActivityCompat.requestPermissions(
                        context,
                        arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE),
                        Constant.REQUEST_FILE_STORAGE_PERMISSION
                    )
                }
                return false
            } else {
                // Permission already granted
                return true
            }
        }
        return true
    }

    private fun filterLoadUrl(view: WebView, url: String): Boolean {
        var p =
            Pattern.compile("http[s|]://livechat\\d\\.pccw\\.com.*|http[s|]://livechathost\\.pccw\\.com.*|http[s|]://customersurvey.{0,3}\\.pccw\\.com.*|http[s|]://\\w{3}\\.clubsim\\.com\\.hk.*action=300.*")
        var m = p.matcher(url)
        val isUnderLiveChat = m.matches()
        p = Pattern.compile("^http[s|]://livechathost\\.pccw\\.com.*/download$")
        m = p.matcher(url)
        val isDownloadFile = m.matches()

        return if (isUnderLiveChat || isDownloadFile)
            false
        else {
            //Navigate to external browser
            val launchBrowser = Intent(Intent.ACTION_VIEW, Uri.parse(url))
            context.startActivity(launchBrowser)
            true
        }
    }

    private fun filterRequest(view: WebView, url: String): Boolean {
        val p = Pattern.compile("^http[s|]://livechathost\\.pccw\\.com.*/download$")
        val m = p.matcher(url)
        val checkDownloadLink = m.matches()
        if (checkDownloadLink) {
            if (checkPermission()) {
                try {
                    GlobalScope.launch {
                        val httpClient = OkHttpClient()
                        val builder = Request.Builder().url(url)
                        val cookieManager = CookieManager.getInstance()
                        val cookies = cookieManager.getCookie(url)

                        builder.addHeader("cookie",
                            withContext(Dispatchers.IO) {
                                URLDecoder.decode(cookies, StandardCharsets.UTF_8.toString())
                            })

                        val formBodyBuilder = MultipartBody.Builder()
                            .setType(MediaType.parse("multipart/form-data")!!)
                        withContext(Dispatchers.IO) {
                            URLDecoder.decode(cookies, StandardCharsets.UTF_8.toString())
                        }.split("; ".toRegex()).find {
                            it.contains("_genesys.widgets.webchat.state.keys")
                        }.run {
                            Gson().fromJson(
                                HttpCookie.parse(this)
                                    .find { it.name.equals("_genesys.widgets.webchat.state.keys") }?.value,
                                Map::class.java) as Map<*, *>
                        }.run {
                            this.forEach {
                                formBodyBuilder.addFormDataPart(it.key as String, it.value as String)
                            }
                        }

                        val req = builder.post(formBodyBuilder.build()).build()
                        val response = httpClient.newCall(req).execute()

                        val contentMineType : String = response.body()!!.contentType()!!.toString()
                        var fileName = FileUtil.randomFileName(contentMineType)

                        if (response.headers().names().contains("Content-Disposition")) {
                           var filenameMatcher = Pattern.compile(".*filename=\"(.*)\"")
                                .matcher(response.header("Content-Disposition")!!)

                            if (filenameMatcher.find()) {
                                withContext(Dispatchers.IO) {
                                    fileName = filenameMatcher.group(1)!!.replace(" ", "")
                                }
                            }

                            filenameMatcher = Pattern.compile(".*filename\\*=utf-8\'\'(.*)")
                                    .matcher(response.header("Content-Disposition")!!)

                            if (filenameMatcher.find()) {
                                withContext(Dispatchers.IO) {
                                    fileName = URLDecoder.decode(filenameMatcher.group(1), "UTF-8")
                                        .replace(" ", "")
                                }
                            }
                        }

                        if (response.isSuccessful) {
                            try {
                                (context as Activity).runOnUiThread {
                                    Toast.makeText(context, R.string.MYHKT_BTN_DONE, Toast.LENGTH_LONG).show()
                                }

                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                                    var uri : Uri
                                    withContext(Dispatchers.IO) {
                                        uri = FileUtil.writeFromInputStream(context, fileName,
                                            contentMineType, response.body()!!.byteStream())
                                    }

                                    val downloadManager = context.getSystemService(DOWNLOAD_SERVICE) as DownloadManager
                                    downloadManager.run {
                                        downloadManager.addCompletedDownload(
                                            fileName,
                                            "Downloading file.",
                                            true, contentMineType,
                                            FileUtil.getFilePathFromURI(context, uri),
                                            fileName.length.toLong(), true)
                                    }
                                } else {
                                    File(context.getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS), fileName)
                                        .writeFromInputStream(response.body()!!.byteStream())
                                        .run fileRun@{
                                            (this@LiveChatWebViewClient.context.getSystemService(DOWNLOAD_SERVICE) as DownloadManager).run {
                                                addCompletedDownload(
                                                    fileName,
                                                    "Downloading file.",
                                                    true,
                                                    contentMineType,
                                                    this@fileRun.path,
                                                    this@fileRun.length(),
                                                    true)
                                            }
                                        }
                                }
                            } catch (e: Exception) {
                                e.printStackTrace()
                            }
                        }
                    }
                } catch (e: IOException) {
                    try {
                        (context as Activity).runOnUiThread {
                            DialogHelper.createSimpleDialog(
                                context,
                                context.getResources().getString(R.string.livechat_download_error),
                                Utils.getString(context, R.string.btn_ok))
                        }
                    } catch (e: Exception) {
                        //return null to tell WebView we failed to fetch it WebView should try again.
                    }
                }
            }
            return true
        }
        return false
    }

    private fun File.writeFromInputStream(inputStream: InputStream): File {
        inputStream.use { input ->
            this.outputStream().use {
                input.copyTo(it)
            }
        }
        return this
    }

    @Deprecated("Deprecated in Java", ReplaceWith("filterLoadUrl(view, url)"))
    override fun shouldOverrideUrlLoading(view: WebView, url: String): Boolean {
        /*if (this.callback != null && this.callback.call(Uri.parse(url))) true else */
        return filterLoadUrl(view, url)
    }

    override fun shouldOverrideUrlLoading(view: WebView, request: WebResourceRequest): Boolean {
        /* if (this.callback != null) this.callback.call(request.url) else */
        return filterLoadUrl(view, request.url.toString())
    }
}