package com.pccw.myhkt.fragment;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import android.app.Activity;
import android.graphics.Point;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AbsListView.LayoutParams;
import android.widget.LinearLayout;

import com.google.gson.Gson;
import com.pccw.dango.shared.cra.PlanLtsCra;
import com.pccw.dango.shared.entity.SrvPlan;
import com.pccw.dango.shared.tool.RC;
import com.pccw.myhkt.APIsManager;
import com.pccw.myhkt.APIsResponse;
import com.pccw.myhkt.ClnEnv;
import com.pccw.myhkt.DialogHelper;
import com.pccw.biz.myhkt.R;
import com.pccw.myhkt.Utils;
import com.pccw.myhkt.activity.BaseActivity;
import com.pccw.myhkt.cell.model.ArrowTextCell;
import com.pccw.myhkt.cell.model.BigTextCell;
import com.pccw.myhkt.cell.model.Cell;
import com.pccw.myhkt.cell.model.SingleBtnCell;
import com.pccw.myhkt.cell.model.SmallTextCell;
import com.pccw.myhkt.cell.model.WebViewCell;
import com.pccw.myhkt.cell.view.CellViewAdapter;
import com.pccw.myhkt.fragment.PlanLTSMainFragment.OnPlanLtsListener;
import com.pccw.myhkt.lib.ui.AAQuery;

/************************************************************************
 * File : PlanLTSFragment.java
 * Desc : LTS Plan
 * Name : PlanLTSFragment
 * by 	: Andy Wong
 * Date : 25/1/2015
 * 
 * Change History:
 * Date       Modified By		Description
 * ---------- ----------------	-------------------------------
 * 18/1/2015  Andy Wong 		-First draft
 *************************************************************************/

public class PlanLTS_TPFragment extends BaseServiceFragment {
	private PlanLTS_TPFragment me;
	private AAQuery aq;

	private static final String TAG = "PlanLTS_TPFragment";
	private List<Cell> cellList;

	private int colWidth;
	private int sColWidth;

	private LinearLayout frame;

	private CellViewAdapter			cellViewAdapter;
	private OnPlanMainLtsListener callback_planlts;
	private OnPlanLtsListener callback_lts;


	private PlanLtsCra 		planLtsCra; 

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		try {
			callback_main = (OnServiceListener) activity;
		} catch (ClassCastException e) {
			throw new ClassCastException(activity.toString() + " must implement OnServiceListener");
		}
		try {
			callback_planlts = (OnPlanMainLtsListener) this.getParentFragment();
		} catch (ClassCastException e) {
			throw new ClassCastException(this.getParentFragment().toString() + " must implement OnServiceListener");
		}

	}

	public interface OnPlanMainLtsListener {
		PlanLtsCra getPlanltsCra();
		void setPlanLtsCra(PlanLtsCra mplanLtscra);
		int getCurrentPlanPage();
		String getReturnCode();
		void setReturnCode(String returnCode);
	}

	@Override 
	public void onCreate(Bundle savedInstanceState) {
//		Log.i(TAG, "onCreate"+java.lang.System.identityHashCode(this));		
		super.onCreate(savedInstanceState);
	}

	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		me = this;
		View fragmentLayout = inflater.inflate(R.layout.fragment_plan_lts_tp, container, false);
		aq = new AAQuery(fragmentLayout);		
		initData();
//		Log.i(TAG, java.lang.System.identityHashCode(this)+"S");
		return fragmentLayout;
	}

	protected void initData(){
		super.initData();
		colWidth = (deviceWidth - extralinespace *2 ) /4;
		sColWidth = (deviceWidth - extralinespace *2 ) * 2/9;
		cellViewAdapter = new CellViewAdapter(getActivity());
	}

	protected void initUI() {
		cellViewAdapter = new CellViewAdapter(getActivity());

		cellList = new ArrayList<Cell>();

		aq.id(R.id.fragment_plan_lts_tp_sv).backgroundColorId(R.color.white);
		aq.marginpx(R.id.fragment_plan_lts_tp_sv, 0, 0, 0, 0);
		frame = (LinearLayout) aq.id(R.id.fragment_plan_lts_tp_frame).getView();

		//Header
		SmallTextCell smallTextCell = new SmallTextCell(getResString(R.string.PLNLTF_TPL1), "");
		smallTextCell.setTitleTypeface(Typeface.BOLD);
		smallTextCell.setTitleColorId(R.color.black);
		smallTextCell.setLeftPadding(basePadding);
		smallTextCell.setRightPadding(basePadding);
		cellList.add(smallTextCell);

		//No data will show if planLtsCra is null(include before api call) or srvPlans size is 0		
		if (planLtsCra !=null && planLtsCra.getOTermPlanAry() != null && planLtsCra.getOTermPlanAry().length > 0) {
			SrvPlan[]  srvPlans = planLtsCra.getOTermPlanAry();
			String colMthTR = getResString(R.string.PLNLTF_TPL_MONTR);
			String colStDt = getResString(R.string.PLNLTF_TPL_EFF1);
			String colEnDt = getResString(R.string.PLNLTF_TPL_EFFEND1);
			int[] widthAry = { sColWidth, sColWidth, sColWidth};
			int ltsType = callback_main.getAcctAgent().getLtsType();
			String typeName ;
			switch(ltsType) {
			case R.string.CONST_LTS_FIXEDLINE:
				typeName = getResString(R.string.myhkt_lts_fixedline);
				break;
			case R.string.CONST_LTS_EYE:
				typeName = getResString(R.string.myhkt_lts_eye);
				break;
			case R.string.CONST_LTS_IDD0060:
				typeName = getResString(R.string.myhkt_lts_idd0060);
				break;
			case R.string.CONST_LTS_CALLINGCARD:
				typeName = getResString(R.string.myhkt_lts_callingcard);
				break;
			case R.string.CONST_LTS_ONECALL:
				typeName = getResString(R.string.myhkt_lts_onecall);
				break;
			case R.string.CONST_LTS_ICFS:
				typeName = getResString(R.string.myhkt_lts_icfs);
				break;
			default:
				typeName = "";
				break;
			}
			
			//Service Name
			SmallTextCell typeNameCell = new SmallTextCell(typeName, "");
			typeNameCell.setLeftPadding(basePadding);
			typeNameCell.setRightPadding(basePadding);
			typeNameCell.setTitleColorId(R.color.black);
			cellList.add(typeNameCell);

			for (int i = 0; i < srvPlans.length; i++) {
				SrvPlan srvPlan = srvPlans[i];
				String[] col;
				String[] data;
				String[] note;
				Boolean isEnqRemarkShown = false;
				
				//1 .Plan Code/ Plan Full Description
				String fuDesc = isZh ? srvPlan.getLongDesn1Zh() : srvPlan.getLongDesn1En();
				if (fuDesc.length() == 0) {
					fuDesc = getResString(R.string.PLNLTF_TPL_CODE) + " " + srvPlan.getSrvCd();
				}
				//2 .plan desc (e.g. XXX Month Term Plan)
				String desc = com.pccw.wheat.shared.tool.Tool.format("%s %s", srvPlan.getSrvCmmtPrd(), getResString(R.string.PLNLTF_TPL_DESC));


				//3 .effective start date
				String stDt = Tool.formatDate(srvPlan.getStDt(), "dd/MM/yy");
				//4 .effective End date
				String enDt ;
//				if (srvPlan.getEnDt().equals("00000000") || srvPlan.getEnDt().trim().length() == 0) {
//					enDt = "-";
//				} else {
					enDt = Tool.formatDate(srvPlan.getEnDt(), "dd/MM/yy");
//				}

				//CR2016002 check if expire within 6 mths			  		   
			    Calendar c = Calendar.getInstance();
			    c.add(Calendar.MONTH, 6);
			    Date after6mthsDate = c.getTime();			    
			    try {
				    if (enDt == null || "".equals(enDt) || "-".equals(enDt)) {
			    		isEnqRemarkShown = true;
			    		stDt = "-";
			    		enDt = "-";
				    } else {
				    	Date endDate;
				    	DateFormat df = new SimpleDateFormat("dd/MM/yy"); 	
				    	endDate = df.parse(enDt);
				    	if (endDate.before(after6mthsDate)) {
				    		isEnqRemarkShown = true;
				    		stDt = "-";
				    		enDt = "-";
				    	}
				    }
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
					
				//5 .Monthly Term Rate
				String mthTR;
				if (srvPlan.getMonthlyTermRate().length() > 0) {
					mthTR = Utils.convertStringToPrice1dp(srvPlan.getMonthlyTermRate());	
					col = new String[3];
					data = new String[3];
					note = new String[3];
					col[2] = colMthTR;
					note[2] = getResString(R.string.PLNLTF_CPL_MTHLY_NOTE);
					data[2] = mthTR;
				} else {
					col = new String[2];
					data = new String[2];
					note =  new String[2];
				}
				col[0] = colStDt;
				col[1] = colEnDt;
				data[0] = stDt;
				data[1] = enDt;
				note[0] = isZh ? "(日/月/年)":"(DD/MM/YY)";
				note[1] = isZh ? "(日/月/年)":"(DD/MM/YY)";

				//View for 1 to 5
				BigTextCell msgCell = new BigTextCell(fuDesc, "");
				msgCell.setLeftMargin(basePadding);
				msgCell.setRightMargin(basePadding);
				msgCell.setArrowShown(true);
				msgCell.setTopMargin(0);
				msgCell.setBotMargin(0);
				msgCell.setTitleSizeDelta(-2);
				cellList.add(msgCell);

				ArrowTextCell arrowTextCell = new ArrowTextCell ("", col , note, widthAry);
				arrowTextCell.setContentSizeDelta(-4);		
				arrowTextCell.setNoteSizeDelta(-4);
				cellList.add(arrowTextCell);

				ArrowTextCell arrowTextCell1 = new ArrowTextCell (desc, data , null, widthAry, R.color.cell_bg_grey);
				arrowTextCell1.setArrowShown(false);
				arrowTextCell1.setTitleSizeDelta(-4);
				arrowTextCell1.setContentSizeDelta(-4);
				arrowTextCell1.setContentColorId(R.color.hkt_buttonblue);
				cellList.add(arrowTextCell1);


				//6 .Offer details
				String detail1 = getResString(R.string.PLNLTF_TPL_OFFDTL);
				String rebt = isZh? srvPlan.getRebtZh() : srvPlan.getRebtEn();
				double rAmt;
				String rMonStr;

				if (rebt.length() > 0) {
					BigTextCell bigTextCell = new BigTextCell(detail1 + " " + rebt, "");
					bigTextCell.setTitleSizeDelta(-2);
					bigTextCell.setTitleColorId(R.color.hkt_txtcolor_grey);
					cellList.add(bigTextCell);
				} else {
					String[] rRebateLv1Ary = Tool.tokenList(srvPlan.getRebtAmt(), "\\|");
					String[] rRebateLv2Ary;
					for (int j=0; j < rRebateLv1Ary.length; j++) {
						rRebateLv2Ary = Tool.tokenList(rRebateLv1Ary[j], "@");
						if (rRebateLv2Ary.length == 2) {
							// Skip element if Amt = 0
							rAmt = Tool.toDouble2(rRebateLv2Ary[1]);
							if (rAmt == 0) {
								continue;
							}

							rMonStr = Tool.int2Str(Tool.NUMFMT_2, Tool.toInt2(rRebateLv2Ary[0]) + 1);
							if (!isZh) rMonStr = Tool.formatOrdNum(rMonStr);
							rebt = Tool.formatPos(Utils.getString(me.getActivity(), R.string.PLNLTF_TPL_REBATE), Utils.convertStringToPrice1dp(rRebateLv2Ary[1]), rMonStr);


							BigTextCell bigTextCell = new BigTextCell(detail1 + " " + rebt, "");
							bigTextCell.setTitleSizeDelta(-2);
							bigTextCell.setTitleColorId(R.color.hkt_txtcolor_grey);
							cellList.add(bigTextCell);
						} else {
							continue;
						}			
					}
				}
				
				if (isEnqRemarkShown) {
					BigTextCell bigTextCell = new BigTextCell(getResString(R.string.PLNLTM_TPL_REMARK), "");			
					bigTextCell.setTitleSizeDelta(-2);
					bigTextCell.setLeftMargin(basePadding);
					bigTextCell.setRightMargin(basePadding);
					bigTextCell.setTitleColorId(R.color.hkt_txtcolor_grey);
					cellList.add(bigTextCell);
				} else {
					/* CA: Moiz- hide live chat feature
					//Enquiry part
					WebViewCell webViewCell = new WebViewCell(isZh? "file:///android_asset/plan_lts_enquiry_zh.html" : "file:///android_asset/plan_lts_enquiry_en.html");
					webViewCell.setTopMargin(extralinespace/3);
					cellList.add(webViewCell);

					SingleBtnCell singleBtnCell = new SingleBtnCell(getResString(R.string.MYHKT_BTN_LIVECHAT), colWidth *2, new OnClickListener(){

						@Override
						public void onClick(View v) {
							((BaseActivity)me.getActivity()).openLiveChat();
						}			
					});	
					singleBtnCell.setLeftMargin(basePadding);
					singleBtnCell.setRightMargin(basePadding);
					singleBtnCell.setLeftPadding(basePadding);
					singleBtnCell.setRightPadding(basePadding);
					singleBtnCell.setBgcolorId(R.color.cell_bg_grey);
					singleBtnCell.setDraw(R.drawable.livechat_small);
					cellList.add(singleBtnCell); */
				}
			}
		} else {			
			//No data
			BigTextCell bigTextCell = new BigTextCell(getResString(R.string.PLNLTM_TPL_REMARK), "");			
			bigTextCell.setTitleSizeDelta(-2);
			bigTextCell.setLeftMargin(basePadding);
			bigTextCell.setRightMargin(basePadding);
			bigTextCell.setTitleColorId(R.color.hkt_txtcolor_grey);
			cellList.add(bigTextCell);
		}
		
		

		cellViewAdapter.setView(frame, cellList);
	}

	//Refresh the data only when it is on current page
	public void refreshData(){
//		Log.i(TAG, java.lang.System.identityHashCode(this)+"");
		if (callback_main.getActiveSubview() == R.string.CONST_SELECTEDVIEW_LTSPLAN){
			planLtsCra = callback_planlts.getPlanltsCra();
			if (planLtsCra  == null) {
				if (callback_planlts.getCurrentPlanPage() == 0) {
					PlanLtsCra mPlanLtsCra = new PlanLtsCra(); 
					mPlanLtsCra.setILoginId(ClnEnv.getLoginId());
					mPlanLtsCra.setISubnRec(callback_main.getSubnRec());
					APIsManager.doGetPlanLts(me, mPlanLtsCra);	
				}
			} else {
				initUI();
			}				
		}
	}	

	public void refresh() {
		super.refresh();
//		Log.i(TAG, java.lang.System.identityHashCode(this.getParentFragment())+"");
//		Log.i(TAG, java.lang.System.identityHashCode(this)+"");
		if (callback_planlts == null) {
			if (debug) Log.i(TAG, "null");
		} else {
		planLtsCra = callback_planlts.getPlanltsCra();
		if (planLtsCra  == null) {
			PlanLtsCra mPlanLtsCra = new PlanLtsCra(); 
			mPlanLtsCra.setILoginId(ClnEnv.getLoginId());
			mPlanLtsCra.setISubnRec(callback_main.getSubnRec());
			APIsManager.doGetPlanLts(me, mPlanLtsCra);
		} else {
			initUI();
		}	
		}
	}


	@Override
	public void onViewStateRestored( Bundle savedInstancestate) {	
		super.onViewStateRestored(savedInstancestate);
	}

	@Override
	public void onDestroyView() {	
		super.onDestroyView();
	}

	@Override
	public void onDetach() {	
		super.onDetach();
	}


	protected void cleanupUI() {

	}

	@Override
	public void onSuccess(APIsResponse response) {
		if (APIsManager.PLAN_LTS.equals(response.getActionTy())) {
			planLtsCra = (PlanLtsCra) response.getCra();
			//			Gson gson = new Gson();				
			//			Log.i(TAG, "LTS1:" + gson.toJson(planLtsCra));
			//			planLtsCra =  ClnEnv.getJSONObject(this.getActivity(), PlanLtsCra.class, "json/plan_lts.json");
			//			Log.i(TAG, "LTS2:" + gson.toJson(planLtsCra));
			callback_planlts.setPlanLtsCra(planLtsCra);
			callback_planlts.setReturnCode(null);
			initUI();
		}
	}

	@Override
	public void onFail(APIsResponse response) {
		if (response != null) {
			if (APIsManager.PLAN_LTS.equals(response.getActionTy())) {
				callback_planlts.setReturnCode(null);
				// General Error Message
				if (!"".equals(response.getMessage()) && response.getMessage() != null) {
					DialogHelper.createSimpleDialog(me.getActivity(), response.getMessage());
				} else if (RC.TCSESS_MM.equalsIgnoreCase(response.getReply().getCode())) {
					BaseActivity.ivSessDialog();
				} else if (RC.LTSA_CPLN_11_17.equalsIgnoreCase(response.getReply().getCode())) {
					callback_planlts.setReturnCode(getResString(R.string.PLNLTM_CPL_11_17));
					initUI();
				} else {
					DialogHelper.createSimpleDialog(me.getActivity(), ClnEnv.getRPCErrMsg(this.getActivity(), response.getReply().getCode()));
				}
				//				planLtsCra =  ClnEnv.getJSONObject(this.getActivity(), PlanLtsCra.class, "json/plan_lts.json");
				//				callback_planlts.setPlanLtsCra(planLtsCra);
				//				Gson gson = new Gson();				
				//				Log.i(TAG, gson.toJson(planLtsCra));
				//				updateUI();
			}
		}
	}
}

