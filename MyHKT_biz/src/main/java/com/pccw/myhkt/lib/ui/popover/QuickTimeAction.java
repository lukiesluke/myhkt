package com.pccw.myhkt.lib.ui.popover;

import java.util.ArrayList;
import java.util.List;

import kankan.wheel.widget.OnWheelChangedListener;
import kankan.wheel.widget.OnWheelScrollListener;
import kankan.wheel.widget.WheelView;
import kankan.wheel.widget.adapters.AbstractWheelTextAdapter;
import kankan.wheel.widget.adapters.ArrayWheelAdapter;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.animation.Animation;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow.OnDismissListener;
import android.widget.ScrollView;
import android.widget.TextView;

import com.pccw.biz.myhkt.R;
import com.pccw.myhkt.lib.ui.PopOverView;


/**
 * QuickAction dialog.
 * 
 * @author Lorensius W. L. T <lorenz@londatiga.net>
 * 
 * Contributors:
 * - Kevin Peck <kevinwpeck@gmail.com>
 */
public class QuickTimeAction extends PopupWindows implements OnDismissListener {
	private int lineCount = 0;
	private int popOverHeight = 0;
	private ImageView mArrowUp;
	private ImageView mArrowDown;
	private FrameLayout mHeaderBar;
	private Animation mTrackAnim;
	private LayoutInflater inflater;
	private ViewGroup mTrack;
	private ScrollView mScroll;

	private LinearLayout mScrollLayout;
	private OnActionItemClickListener mItemClickListener;
	private OnDismissListener mDismissListener;

	//wheel action
	private OnWheelChangedListener mWheelChangedListener;
	private OnPickerScrollListener mPickerScrollListener;
	private boolean scrolling = false;
	private boolean isFullWidth = false;

	private List<ActionItem> mActionItemList = new ArrayList<ActionItem>();

	private boolean mDidAction;
	private boolean mAnimateTrack;


	private int mChildPos;    
	private int mAnimStyle;

	public static final int ANIM_GROW_FROM_LEFT = 1;
	public static final int ANIM_GROW_FROM_RIGHT = 2;
	public static final int ANIM_GROW_FROM_CENTER = 3;
	public static final int ANIM_AUTO = 4;

	private Context context;

	private PopOverView mPopOverView;
	private ImageView mUpperLine;
	private ImageView mBottomLine;

	/**
	 * Constructor.
	 * 
	 * @param context Context
	 */
	public QuickTimeAction(Context context, int rootViewWidth, int rootViewHeight) {
		super(context);
		this.context = context;

		inflater 	= (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		setRootViewId(R.layout.quickaction, rootViewWidth, rootViewHeight);

		mAnimStyle		= ANIM_AUTO;
		mAnimateTrack	= true;
		mChildPos		= 0;
	}

	/**
	 * Get action item at an index
	 * 
	 * @param index  Index of item (position from callback)
	 * 
	 * @return  Action Item at the position
	 */
	public ActionItem getActionItem(int index) {
		return mActionItemList.get(index);
	}

	public int getrootWidth() {
		return mRootView.getWidth();
	}

	/**
	 * Set root view.
	 * 
	 * @param id Layout resource id
	 */
	public void setRootViewId(int id, int rootViewWidth, int rootViewHeight) {
		isFullWidth = rootViewWidth == 0;
		int screenWidth 	= mWindowManager.getDefaultDisplay().getWidth();

		mRootView	= inflater.inflate(id, null);
		mTrack 		= mRootView.findViewById(R.id.tracks);

		mScroll	= mRootView.findViewById(R.id.scroll);
		mPopOverView = mRootView.findViewById(R.id.popOverView);
		mUpperLine = mRootView.findViewById(R.id.upper_line);
		mBottomLine = mRootView.findViewById(R.id.bottom_line);

		//This was previously defined on show() method, moved here to prevent force close that occured
		//when tapping fastly on a view to show quickaction dialog.
		//Thanx to zammbi (github.com/zammbi)
		mRootView.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));

		//		LayoutParams rparams = mRootView.getLayoutParams();
		//		rparams.width = (rootViewWidth != 0) ? rootViewWidth : screenWidth;
		//		mRootView.setLayoutParams(rparams);

		LayoutParams params = mScroll.getLayoutParams();
		params.height = (rootViewHeight != 0) ? rootViewHeight : LayoutParams.WRAP_CONTENT;
		params.width = (rootViewWidth != 0) ? rootViewWidth : screenWidth;
		mScroll.setLayoutParams(params);
		//

		setContentView(mRootView);
	}


	/**
	 * Animate track.
	 * 
	 * @param mAnimateTrack flag to animate track
	 */
	public void mAnimateTrack(boolean mAnimateTrack) {
		this.mAnimateTrack = mAnimateTrack;
	}

	/**
	 * Set animation style.
	 * 
	 * @param mAnimStyle animation style, default is set to ANIM_AUTO
	 */
	public void setAnimStyle(int mAnimStyle) {
		this.mAnimStyle = mAnimStyle;
	}

	/**
	 * Add action item
	 * 
	 * @param action  {@link ActionItem}
	 */
	public void addActionItem(ActionItem action) {
		mActionItemList.add(action);

		String title 	= action.getTitle();
		Drawable icon 	= action.getIcon();

		View container	= inflater.inflate(R.layout.action_item, null);

		//		ImageView img 	= (ImageView) container.findViewById(R.id.iv_icon);
		TextView text 	= container.findViewById(R.id.tv_title);

		//		if (icon != null) { 
		//			img.setImageDrawable(icon);
		//		} else {
		//			img.setVisibility(View.GONE);
		//		}

		if (title != null) {
			text.setText(title);
		} else {
			text.setVisibility(View.GONE);
		}

		final int pos 		=  mChildPos;
		final int actionId 	= action.getActionId();

		container.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (mItemClickListener != null) {
					mItemClickListener.onItemClick(QuickTimeAction.this, pos, actionId);
				}

				if (!getActionItem(pos).isSticky()) {  
					mDidAction = true;

					//workaround for transparent background bug
					//thx to Roman Wozniak <roman.wozniak@gmail.com>
					v.post(new Runnable() {
						@Override
						public void run() {
							dismiss();
						}
					});
				}
			}
		});

		container.setFocusable(true);
		container.setClickable(true);

		mTrack.addView(container, mChildPos+1);

		mChildPos++;
	}

	public void initPickerWheel(final String[] leftStringList, final String[]rightStringList, int leftPos, int rightPos) {

		View container	= inflater.inflate(R.layout.wheel_view, null);

		final WheelView wheelLeft = container.findViewById(R.id.wheelLeft);
		wheelLeft.setVisibleItems(3);
		wheelLeft.setViewAdapter(new LeftWheelAdapter(context, leftStringList));

		final WheelView wheelRight = container.findViewById(R.id.wheelRight);
		wheelRight.setVisibleItems(5);
		wheelRight.setViewAdapter(new LeftWheelAdapter(context, rightStringList));
		wheelLeft.addChangingListener(new OnWheelChangedListener() {
			public void onChanged(WheelView wheel, int oldValue, int newValue) {
				if (!scrolling) {
				}
			}
		});

		wheelLeft.addScrollingListener( new OnWheelScrollListener() {
			public void onScrollingStarted(WheelView wheel) {
				scrolling = true;
			}
			public void onScrollingFinished(WheelView wheel) {
				scrolling = false;
				if (mPickerScrollListener != null) {
					String leftResult = leftStringList[wheelLeft.getCurrentItem()];
					String rightResult = rightStringList[wheelRight.getCurrentItem()];
					int leftPos = wheelLeft.getCurrentItem();
					int rightPos = wheelRight.getCurrentItem();
					mPickerScrollListener.onPickerScroll(leftResult, rightResult, leftPos, rightPos);
				}

			}
		});

		wheelRight.addScrollingListener( new OnWheelScrollListener() {
			public void onScrollingStarted(WheelView wheel) {
				// TODO Auto-generated method stub

			}

			public void onScrollingFinished(WheelView wheel) {
				if (mPickerScrollListener != null) {
					String leftResult = leftStringList[wheelLeft.getCurrentItem()];
					String rightResult = rightStringList[wheelRight.getCurrentItem()];
					int leftPos = wheelLeft.getCurrentItem();
					int rightPos = wheelRight.getCurrentItem();
					mPickerScrollListener.onPickerScroll(leftResult, rightResult, leftPos, rightPos);
				}

			}
		});

		wheelLeft.setCurrentItem(leftPos);
		wheelRight.setCurrentItem(rightPos);

		mTrack.addView(container, 0);
	}

	public void setOnActionItemClickListener(OnActionItemClickListener listener) {
		mItemClickListener = listener;
	}

	public void setOnPickerScrollListener(OnPickerScrollListener listener) {
		mPickerScrollListener = listener;
	}

	/**
	 * Show popup mWindow
	 */
	public void showPicker(View anchor, View parentLayout) {
		//THe code now support full width only
		preShow(isFullWidth);

		int[] location 		= new int[2];

		mDidAction 			= false;

		anchor.getLocationOnScreen(location);

		Rect anchorRect 	= new Rect(location[0], location[1], location[0] + anchor.getWidth(), location[1] + anchor.getHeight());
		mScroll.measure(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
		mRootView.measure(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);

		int rootWidth 		= mRootView.getMeasuredWidth();
		int rootHeight 		= mRootView.getMeasuredHeight();
		int screenWidth 	= mWindowManager.getDefaultDisplay().getWidth();
		int screenHeight 	= mWindowManager.getDefaultDisplay().getHeight();
		int scrollWidth     = mScroll.getMeasuredWidth();
		int scrollHeight    = mScroll.getMeasuredHeight();
		int arrowPos 		= anchorRect.centerX();
		int yPos	 		= anchorRect.top - rootHeight;		
		int xPos 			= 0;

		//Check if the scroll is onTop of the arrow or not
		boolean onTop		= true;
		// display on bottom
		if ((screenHeight - anchorRect.bottom) > (anchorRect.top - parentLayout.getTop())) {
			yPos 	= anchorRect.bottom;
			onTop	= false;
		}

		//Set the Pos of the Scroll
		if (!onTop) {
			android.widget.RelativeLayout.LayoutParams mSrollParams = (android.widget.RelativeLayout.LayoutParams) mScroll.getLayoutParams();
			mSrollParams.topMargin = mPopOverView.getArrowHeight()+mPopOverView.getShadowWidth()/2;
			mSrollParams.leftMargin = mPopOverView.getRoundAngle()+mPopOverView.getShadowWidth()/2;
			mSrollParams.rightMargin = mPopOverView.getRoundAngle()+mPopOverView.getShadowWidth()/2;
			mScroll.setLayoutParams(mSrollParams);
		} else {
			android.widget.RelativeLayout.LayoutParams mSrollParams = (android.widget.RelativeLayout.LayoutParams) mScroll.getLayoutParams();
			mSrollParams.topMargin = mPopOverView.getShadowWidth()/2;
			mSrollParams.leftMargin = mPopOverView.getRoundAngle()+mPopOverView.getShadowWidth()/2;
			mSrollParams.rightMargin = mPopOverView.getRoundAngle()+mPopOverView.getShadowWidth()/2;
			mScroll.setLayoutParams(mSrollParams);
		}

		//PopOverBg Setting
		android.widget.RelativeLayout.LayoutParams popOverViewParams = (android.widget.RelativeLayout.LayoutParams) mPopOverView.getLayoutParams();
		popOverViewParams.height = mPopOverView.getOuterBndHeight(scrollHeight);
		mPopOverView.setLayoutParams(popOverViewParams);
		mPopOverView.setIsTop(!onTop);		
		mPopOverView.setArrowPos(arrowPos);

		//Upper Line
		WheelView wheel = mTrack.getChildAt(0).findViewById(R.id.wheelLeft);
		android.widget.RelativeLayout.LayoutParams mUpperLineParams = (android.widget.RelativeLayout.LayoutParams) mUpperLine.getLayoutParams();
		mUpperLineParams.topMargin = scrollHeight/2 - wheel.getOffset();
		mUpperLineParams.width = ViewGroup.LayoutParams.MATCH_PARENT;
		mUpperLineParams.leftMargin = mPopOverView.getShadowWidth()/2;
		mUpperLineParams.rightMargin = mPopOverView.getShadowWidth()/2;
		mUpperLine.setLayoutParams(mUpperLineParams);

		//Bottom Line
		android.widget.RelativeLayout.LayoutParams mBottomLineParams = (android.widget.RelativeLayout.LayoutParams) mBottomLine.getLayoutParams();
		mBottomLineParams.topMargin =scrollHeight/2 + wheel.getOffset();
		mBottomLineParams.width = ViewGroup.LayoutParams.MATCH_PARENT;
		mBottomLineParams.leftMargin = mPopOverView.getShadowWidth()/2;
		mBottomLineParams.rightMargin = mPopOverView.getShadowWidth()/2;
		mBottomLine.setLayoutParams(mBottomLineParams);

		setAnimationStyle(screenWidth, anchorRect.centerX(), onTop);

		mWindow.showAtLocation(anchor, Gravity.NO_GRAVITY, xPos, yPos);
	}
	
	/**
	 * Set animation style
	 * 
	 * @param screenWidth Screen width
	 * @param requestedX distance from left screen
	 * @param onTop flag to indicate where the popup should be displayed. Set TRUE if displayed on top of anchor and vice versa
	 */
	private void setAnimationStyle(int screenWidth, int requestedX, boolean onTop) {
//		int arrowPos = requestedX - mArrowUp.getMeasuredWidth()/2;
		int arrowPos = requestedX ;
		//		int arrowPos = arrowPosition;

		switch (mAnimStyle) {
		case ANIM_GROW_FROM_LEFT:
			mWindow.setAnimationStyle((onTop) ? R.style.Animations_PopUpMenu_Left : R.style.Animations_PopDownMenu_Left);
			break;

		case ANIM_GROW_FROM_RIGHT:
			mWindow.setAnimationStyle((onTop) ? R.style.Animations_PopUpMenu_Right : R.style.Animations_PopDownMenu_Right);
			break;

		case ANIM_GROW_FROM_CENTER:
			mWindow.setAnimationStyle((onTop) ? R.style.Animations_PopUpMenu_Center : R.style.Animations_PopDownMenu_Center);
			break;

		case ANIM_AUTO:
			if (arrowPos <= screenWidth/4) {
				mWindow.setAnimationStyle((onTop) ? R.style.Animations_PopUpMenu_Left : R.style.Animations_PopDownMenu_Left);
			} else if (arrowPos > screenWidth/4 && arrowPos < 3 * (screenWidth/4)) {
				mWindow.setAnimationStyle((onTop) ? R.style.Animations_PopUpMenu_Center : R.style.Animations_PopDownMenu_Center);
			} else {
				mWindow.setAnimationStyle((onTop) ? R.style.Animations_PopDownMenu_Right : R.style.Animations_PopDownMenu_Right);
			}

			break;
		}
	}

	/**
	 * Set listener for window dismissed. This listener will only be fired if the quicakction dialog is dismissed
	 * by clicking outside the dialog or clicking on sticky item.
	 */
	public void setOnDismissListener(QuickTimeAction.OnDismissListener listener) {
		setOnDismissListener(this);

		mDismissListener = listener;
	}

	@Override
	public void onDismiss() {
		if (!mDidAction && mDismissListener != null) {
			mDismissListener.onDismiss();
		}
	}

	/**
	 * Listener for item click
	 *
	 */
	public interface OnActionItemClickListener {
		void onItemClick(QuickTimeAction source, int pos, int actionId);
	}

	/**
	 * derek listener for wheel picker scroll
	 *
	 */
	public interface OnPickerScrollListener {
		void onPickerScroll(String leftResult, String rightResult, int leftPos, int rightPos);
	}

	/**
	 * Listener for window dismiss
	 * 
	 */
	public interface OnDismissListener {
		void onDismiss();
	}


	/**
	 * Adapter for countries
	 */
	private class LeftWheelAdapter extends AbstractWheelTextAdapter {
		// Countries names
		private String[] leftList;
		//        private String countries[] =
		//            new String[] {"USA", "Canada", "Ukraine", "France"};
		//        // Countries flags
		//        private int flags[] =
		//            new int[] {R.drawable.usa, R.drawable.canada, R.drawable.ukraine, R.drawable.france};

		/**
		 * Constructor
		 */
		protected LeftWheelAdapter(Context context, String[] leftStringList) {
			super(context, R.layout.leftwheel_layout, NO_RESOURCE);

			leftList = leftStringList;
			setItemTextResource(R.id.leftwheel_name);
		}

		@Override
		public View getItem(int index, View cachedView, ViewGroup parent) {
			View view = super.getItem(index, cachedView, parent);
			//            ImageView img = (ImageView) view.findViewById(R.id.flag);
			//            img.setImageResource(flags[index]);
			return view;
		}

		@Override
		public int getItemsCount() {
			return leftList.length;
		}

		@Override
		protected CharSequence getItemText(int index) {
			return leftList[index];
		}
	}    

}