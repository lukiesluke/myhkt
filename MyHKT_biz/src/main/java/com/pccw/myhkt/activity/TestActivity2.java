package com.pccw.myhkt.activity;

import android.graphics.Point;
import android.os.Bundle;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentTransaction;
import android.util.Log;
import android.view.Display;

import com.pccw.biz.myhkt.R;
import com.pccw.myhkt.lib.ui.AAQuery;

public class TestActivity2 extends FragmentActivity {  
	private boolean 				debug = false;
	AAQuery aq ;
	private int 					extralinespace;
	private int 					gridlayoutPadding = 0;
	private int 					deviceWidth = 0;
	private int 					colWidth = 0;

	@Override  
	public void onCreate(Bundle savedInstanceState)  {  
		super.onCreate(savedInstanceState);  
		setContentView(R.layout.activity_test1);  
		debug = getResources().getBoolean(R.bool.DEBUG);
		Display display = getWindowManager().getDefaultDisplay();
		Point size = new Point();
		display.getSize(size);
		deviceWidth = size.x;
		gridlayoutPadding = (int) getResources().getDimension(R.dimen.mainmenu_gridlayout_padding);
		extralinespace = (int) getResources().getDimension(R.dimen.extralinespace);
		initUI();
		if (debug) Log.i("TestActivity", colWidth+"");    
	}  
	
	 
	private FragmentTransaction ft;
	public void initUI(){
//		oSRCreationFragment testF = new oSRCreationFragment();
//		ft = getSupportFragmentManager().beginTransaction();
//		ft.setCustomAnimations(R.anim.left_slide_in, R.anim.right_slide_out);
//		ft.replace(R.id.fragment, testF).commit();
	
	}
}  