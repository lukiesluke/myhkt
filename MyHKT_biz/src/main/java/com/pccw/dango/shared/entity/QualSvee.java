/*
    Qualified Servee
    
    Hold all relevant records and derives of a Servee.

    Keywords
    --------
    $URL: $
    $Rev: $
    $Date: $
    $Author: $
*/

package com.pccw.dango.shared.entity;

import java.io.Serializable;

public class QualSvee implements Serializable {
    private static final long serialVersionUID = -3343276325000790245L;

    private CustRec custRec;            /* Customer Record                               */
    private SveeRec sveeRec;            /* Servee Record                                 */
    private BcifRec bcifRec;            /* HKBR Customer Information                     */
    private CareRec careRec;            /* Care Record                                   */
    private SubnRec[] subnRecAry;         /* Array of Subscription Record                  */
    private SubnRec[] zmSubnRecAry;       /* Array of Zombie Subn Record                   */
    private BomCust[] bomCustAry;         /* Array of BOM Customer                         */
    private Account[] acctAry;            /* Array of Account                              */

    public QualSvee() {
        initAndClear();
    }

    public static void main(String[] rArg) {
        System.out.println(getVer());
    }

    public static String getVer() {
        return ("$URL: $, $Rev: $");
    }

    final void initAndClear() {
        init();
        clear();
    }

    protected void init() {
    }

    public void clear() {
        clearCustRec();
        clearSveeRec();
        clearBcifRec();
        clearCareRec();
        clearSubnRecAry();
        clearZmSubnRecAry();
        clearBomCustAry();
        clearAcctAry();
    }

    public QualSvee copyFrom(QualSvee rSrc) {
        setCustRec(rSrc.getCustRec());
        setSveeRec(rSrc.getSveeRec());
        setBcifRec(rSrc.getBcifRec());
        setCareRec(rSrc.getCareRec());
        setSubnRecAry(rSrc.getSubnRecAry());
        setZmSubnRecAry(rSrc.getZmSubnRecAry());
        setBomCustAry(rSrc.getBomCustAry());
        setAcctAry(rSrc.getAcctAry());

        return (this);
    }

    public QualSvee copyTo(QualSvee rDes) {
        rDes.copyFrom(this);
        return (rDes);
    }

    public QualSvee copyMe() {
        QualSvee rDes;

        rDes = new QualSvee();
        rDes.copyFrom(this);
        return (rDes);
    }

    public void clearCustRec() {
        custRec = new CustRec();
    }

    public CustRec getCustRec() {
        return (custRec);
    }

    public void setCustRec(CustRec rArg) {
        custRec = rArg;
    }

    public void clearSveeRec() {
        sveeRec = new SveeRec();
    }

    public SveeRec getSveeRec() {
        return (sveeRec);
    }

    public void setSveeRec(SveeRec rArg) {
        sveeRec = rArg;
    }

    public void clearBcifRec() {
        bcifRec = new BcifRec();
    }

    public BcifRec getBcifRec() {
        return (bcifRec);
    }

    public void setBcifRec(BcifRec rArg) {
        bcifRec = rArg;
    }

    public void clearCareRec() {
        careRec = new CareRec();
    }

    public CareRec getCareRec() {
        return (careRec);
    }

    public void setCareRec(CareRec rArg) {
        careRec = rArg;
    }

    public void clearSubnRecAry() {
        subnRecAry = new SubnRec[0];
    }

    public SubnRec[] getSubnRecAry() {
        return (subnRecAry);
    }

    public void setSubnRecAry(SubnRec[] rArg) {
        subnRecAry = rArg;
    }

    public void clearZmSubnRecAry() {
        zmSubnRecAry = new SubnRec[0];
    }

    public SubnRec[] getZmSubnRecAry() {
        return (zmSubnRecAry);
    }

    public void setZmSubnRecAry(SubnRec[] rArg) {
        zmSubnRecAry = rArg;
    }

    public void clearBomCustAry() {
        bomCustAry = new BomCust[0];
    }

    public BomCust[] getBomCustAry() {
        return (bomCustAry);
    }

    public void setBomCustAry(BomCust[] rArg) {
        bomCustAry = rArg;
    }

    public void clearAcctAry() {
        acctAry = new Account[0];
    }

    public Account[] getAcctAry() {
        return (acctAry);
    }

    public void setAcctAry(Account[] rArg) {
        acctAry = rArg;
    }
}
