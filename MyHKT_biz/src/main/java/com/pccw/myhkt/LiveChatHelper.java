package com.pccw.myhkt;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import android.annotation.SuppressLint;
import android.app.Activity;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AlertDialog.Builder;
import android.app.Dialog;
import android.app.DownloadManager;
import android.app.DownloadManager.Request;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.MutableContextWrapper;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Rect;
import android.net.Uri;
import android.net.http.SslError;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.CookieManager;
import android.webkit.DownloadListener;
import android.webkit.SslErrorHandler;
import android.webkit.URLUtil;
import android.webkit.ValueCallback;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;
import com.pccw.biz.myhkt.R;
import com.pccw.dango.shared.cra.LgiCra;
import com.pccw.myhkt.activity.BaseActivity;
import com.pccw.myhkt.activity.MainMenuActivity;
import com.pccw.myhkt.lib.ui.AAQuery;

//import com.pccw.donut.shared.rpc.LoginCra;

/************************************************************************
File       : LiveChatHelper.java
Desc       : Handling All Screens' LiveChat WebView
Name       : LiveChatHelper
Created by : Vincent Fung
Date       : 16/11/2015

Change History:
Date       Modified By			Description
---------- ----------------	-------------------------------
16/11/2015 Vincent Fung			- First draft 
15/6/2017  Abdulmoiz Esmail	    - Added the SHLF_PA_PLN_PCD_4MOBCS and SHLF_PA_BIL_PCD_4MOBCS in the MOB-CS, 
								  and revert them to SHLF_PA_PLN_PCD, SHIF_PA_BIL_PCD respectively before passing it to HTTPS
 *************************************************************************/

public class LiveChatHelper {
	private boolean					debug			= false;	// toggle debug logs
	private static LiveChatHelper	liveChatHelper	= null;
	private static MutableContextWrapper 	mMutableContext;
	private WebView 				webview = null;
	private ImageView				liveChatIcon = null;

	private float 					m_lastTouchX, m_lastTouchY, m_posX, m_posY, m_prevX, m_prevY, m_dx, m_dy;
	private boolean 				isOpen;
	public static boolean isPause = false;
	private static boolean isFailed = false;
	private String moduleId      ="";
	private static Context cxt;
	private static Activity act;
	private static Dialog lDialog;
	
	//Attachment function (2016-8-25)
	private BroadcastReceiver		downloadCompleteReceiver;															 // download
	private DownloadManager			downloadManager;
	private long					downloadID;
	/** File upload callback for platform versions prior to Android 5.0 */
    public static ValueCallback<Uri> mFileUploadCallbackFirst;
    /** File upload callback for Android 5.0+ */
    public static ValueCallback<Uri[]> mFileUploadCallbackSecond;
    protected String mUploadableFileTypes = "*/*";
    protected static final int REQUEST_CODE_FILE_PICKER = 51426;
    public static int mRequestCodeFilePicker = REQUEST_CODE_FILE_PICKER;

	private LiveChatHelper (Context context) {
		debug = context.getResources().getBoolean(R.bool.DEBUG);
		cxt = context;
	}

	public static synchronized LiveChatHelper getInstance(Context context, Activity actt) {
		act = actt;
		if (liveChatHelper == null) {
			mMutableContext = new MutableContextWrapper(context);
			liveChatHelper = new LiveChatHelper(mMutableContext);
		} else {
			mMutableContext.setBaseContext(context);
		}
		return liveChatHelper;
	}

	public final WebView getWebView(Activity act) {
		if (debug) Log.i("LiveChatHelper", "Module Id : " + moduleId + "\n is Webview Pause: " + isPause);
		if (webview == null) {			
			mMutableContext = new MutableContextWrapper(act);
			initWebView(mMutableContext, act);
		} else {
			mMutableContext.setBaseContext(act);
		}

		return webview;
	}

	private final void initWebView(final Context context, final Activity act) {
//		this.act = act;
		webview = new WebView(context);
		webview.setWebViewClient(new WebViewOverrideUrl(context));
		webview.setVisibility(View.VISIBLE);
		webview.getSettings().setJavaScriptEnabled(true);
		webview.setDownloadListener(new DownloadListener() {
			@SuppressLint("NewApi")
			@Override
			public void onDownloadStart(String url, String userAgent, String contentDisposition, String mimetype, long contentLength) {
				final String fileName= URLUtil.guessFileName(url, contentDisposition, mimetype);
				if (debug) Log.i("download", "request");
                Request request = new Request(Uri.parse(url));
                request.setMimeType(mimetype);
                
//                String cookies = CookieManager.getInstance().getCookie(url);
//                request.addRequestHeader("cookie", cookies);
//                request.addRequestHeader("User-Agent", userAgent);
//                request.setDescription(context.getString(R.string.MYHKT_DOWNLOADING));
                request.setTitle(URLUtil.guessFileName(url, contentDisposition, mimetype));
                
                // in order for this if to run, you must use the android 3.2 to compile your app
                if (debug) Log.i("download", "honeycomb scanning");
                request.allowScanningByMediaScanner();
                request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
                if (debug) Log.i("download", "3");
                request.setVisibleInDownloadsUi(true);
                if (debug) Log.i("download", "4");
                request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, fileName);
                if (debug) Log.i("download", "5");
                // get download service and enqueue file
                downloadID = downloadManager.enqueue(request);
                
                if (debug) Log.i("download", "6");
                Toast.makeText(context, context.getString(R.string.MYHKT_START_DOWNLOAD), Toast.LENGTH_SHORT).show();
                if (debug) Log.i("download", "7");
			}
		});
		webview.setWebChromeClient(new WebChromeClient() {

            // file upload callback (Android 2.2 (API level 8) -- Android 2.3 (API level 10)) (hidden method)
            @SuppressWarnings("unused")
            public void openFileChooser(ValueCallback<Uri> uploadMsg) {
                openFileChooser(uploadMsg, null);
            }

            // file upload callback (Android 3.0 (API level 11) -- Android 4.0 (API level 15)) (hidden method)
            public void openFileChooser(ValueCallback<Uri> uploadMsg, String acceptType) {
                openFileChooser(uploadMsg, acceptType, null);
            }

            // file upload callback (Android 4.1 (API level 16) -- Android 4.3 (API level 18)) (hidden method)
            public void openFileChooser(ValueCallback<Uri> uploadMsg, String acceptType, String capture) {
                openFileInput(uploadMsg, null, act);
            }

            // file upload callback (Android 5.0 (API level 21) -- current) (public method)
            @SuppressWarnings("all")
            public boolean onShowFileChooser(WebView webView, ValueCallback<Uri[]> filePathCallback, WebChromeClient.FileChooserParams fileChooserParams) {
                openFileInput(null, filePathCallback, act);
                return true;
            }
        });
		
		webview.getSettings().setLoadWithOverviewMode(true);
		Utils.setCurrentModule(Utils.getString(context, R.string.MODULE_REG));
//		String livechatURL = Utils.getString(context, R.string.MYHKT_URL_LIVECHAT);
		String livechatURL = ClnEnv.getPref(context, context.getString(R.string.CONST_PREF_LIVECHAT), APIsManager.PRD_livechat);
		if (debug) Log.i("TAG", livechatURL + "?" + genLiveChatUrlParam(context));
//		String html = "<iframe src=\"http://docs.google.com/gview?url=https://cspuat.pccw.com/cs2/gen_cond_en.pdf&embedded=true\" style=\"width:600px; height:1200px;\" frameborder=\"0\"></iframe>";
//		webview.loadUrl("https://docs.google.com/viewer?url=https://cspuat.pccw.com/cs2/gen_cond_en.pdf");
//		webview.loadData(html, "text/html", "UTF-8");
		webview.loadUrl(livechatURL + "?" + genLiveChatUrlParam(context));
		//		webview.loadUrl("http://hk.yahoo.com");
	}


	public final void showWebView(final Activity act , String mModuleId){
//		this.act = act;
		downloadManager = (DownloadManager) act.getSystemService(Context.DOWNLOAD_SERVICE);
		downloadCompleteReceiver = new BroadcastReceiver() {
			@Override
			public void onReceive(Context context, Intent intent) {
				long id = intent.getLongExtra(DownloadManager.EXTRA_DOWNLOAD_ID, 0L);
				if (id == downloadID) {
					if (debug) Log.i("download", "r1");
					Toast.makeText(act, act.getResources().getString(R.string.MYHKT_BTN_DONE), Toast.LENGTH_LONG).show();
				}
			}

		};
		if (debug) Log.i("download", "r2");
		act.registerReceiver(downloadCompleteReceiver, new IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE));
		
		if (!isPause && (!ClnEnv.isLoggedIn() || Utils.getLiveChatDisclaimerFlag())) {
			displayDialog(act, Utils.getString(act, R.string.LIVE_CHAT_DISCLAIMER), mModuleId);
		} else {
			displayWebView(act, mModuleId);
		}
	}


	//Module Id may not be used if the web view is pause only
	public final void displayWebView(final Activity act , String mModuleId){
		if (mModuleId == null ||mModuleId.equals("")){
			mModuleId = "SHLF_PA_ROOT";
			//			throw new ClassCastException(act.toString() + " did not initiate modeuleId");	
		}

		//If the webview is puase only, use the existing moduleId
		//Else use the new one
		if (!isPause) {
			moduleId = mModuleId;
		}

		//To find the height of notification bar
		Rect rectangle= new Rect();
		Window window= act.getWindow();
		window.getDecorView().getWindowVisibleDisplayFrame(rectangle);
		int statusBarHeight= rectangle.top;
		int contentViewTop=  window.findViewById(Window.ID_ANDROID_CONTENT).getTop();
		int titleBarHeight= Math.abs(contentViewTop - statusBarHeight);
		int btnH = act.getResources().getDimensionPixelOffset(R.dimen.livechat_browser_btn);
		//Status Bar Color
		if (android.os.Build.VERSION.SDK_INT >= 21) {
			window.getDecorView().setFitsSystemWindows(false);
			window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
			if (ClnEnv.isMyMobFlag()){
				window.setStatusBarColor(act.getResources().getColor(R.color.hkt_edittextbgorange));
			} else {
				window.setStatusBarColor(act.getResources().getColor(R.color.hkt_textcolor));
			}
		}


		//Dialog setting
		int extralinespace = (int) act.getResources().getDimension(R.dimen.extralinespace);
		int imageHeight = act.getResources().getDimensionPixelOffset(R.dimen.button_height) +  act.getResources().getDimensionPixelOffset(R.dimen.padding_screen)*5/2;
		int padding = act.getResources().getDimensionPixelOffset(R.dimen.edittextpadding);
		lDialog = new Dialog(act,android.R.style.Theme_Translucent_NoTitleBar);
		lDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		LayoutInflater inflater = (LayoutInflater)   act.getSystemService(Context.LAYOUT_INFLATER_SERVICE); 
		View ctView = inflater.inflate(R.layout.dialog_livechat, null);
		lDialog.setContentView(ctView);
		lDialog.setCanceledOnTouchOutside(false);
		lDialog.setCancelable(false);

		AAQuery aq = new AAQuery(ctView);
		ImageView mini = lDialog.findViewById(R.id.dialog_livechat_mini);
		ImageView close = lDialog.findViewById(R.id.dialog_livechat_close);
		LinearLayout frame = lDialog.findViewById(R.id.dialog_livechat_frame);

		aq.id(R.id.dialog_livechat_iv).height(imageHeight, false);
		aq.id(R.id.dialog_livechat_iv).visibility(View.GONE);
//		aq.normText(R.id.dialog_livechat_title, "Live Chat - " + moduleId);
		aq.normText(R.id.dialog_livechat_title, act.getResources().getString(R.string.MYHKT_BTN_LIVECHAT));
		aq.id(R.id.dialog_livechat_title).height(ViewGroup.LayoutParams.WRAP_CONTENT, false);
		aq.padding(R.id.dialog_livechat_title, extralinespace, 0, 0, 0);
		aq.id(R.id.dialog_livechat_title).textColor(Color.WHITE).height(act.getResources().getDimensionPixelOffset(R.dimen.livechat_header_height),false);
		Bitmap bmmini = ViewUtils.scaleToFitHeight(act, R.drawable.btn_minimize, btnH);	
		Boolean isZh;
        isZh = "zh".equalsIgnoreCase(act.getResources().getString(R.string.myhkt_lang));
		Bitmap bmmclose = ViewUtils.scaleToFitHeight(act, isZh? R.drawable.btn_close2_zh_1 : R.drawable.btn_close2, btnH);
		aq.id(R.id.dialog_livechat_mini).image(bmmini);
		aq.id(R.id.dialog_livechat_close).image(bmmclose);
		aq.marginpx(R.id.dialog_livechat_close, padding*3/2, padding/2, padding/2, padding/2);
		aq.marginpx(R.id.dialog_livechat_main_bg, padding, padding/2, padding, padding);
		aq.marginpx(R.id.dialog_livechat_frame, padding/2, 0   , padding/2, padding);

		//Web view part
		frame.removeAllViews();		
		final WebView mWebView = getWebView(act); 
		if (mWebView.getParent() !=null){
			((LinearLayout)mWebView.getParent()).removeView(mWebView);
		}
		frame.removeView(mWebView);
		frame.addView(mWebView);
		LinearLayout.LayoutParams para3 = (android.widget.LinearLayout.LayoutParams) mWebView.getLayoutParams();
		para3.height = LinearLayout.LayoutParams.MATCH_PARENT;
		para3.bottomMargin = padding/2;
		mWebView.setLayoutParams(para3);
		mWebView.setWebViewClient(new WebViewOverrideUrl(act));
		close.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				DialogInterface.OnClickListener onPositiveClickListener =  new DialogInterface.OnClickListener() {
					Bundle	rbundle;
					@Override
					public void onClick(DialogInterface dialog, int which) {
						//close livechat
						closeLiveChat();
//						lDialog = null;
					}
				};
				
				DialogHelper.createSimpleDialog(act, Utils.getString(act, R.string.LIVE_CHAT_EXIT_ALERT), Utils.getString(act, R.string.btn_ok), onPositiveClickListener, Utils.getString(act, R.string.btn_cancel));
			}
		});
		mini.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				//				if (mWebView.getParent() !=null){
				//					((LinearLayout)mWebView.getParent()).removeView(mWebView);
				//				}
				isPause = true;
				if(act instanceof BaseActivity) {
					((BaseActivity)act).setLiveChangeIcon(isPause);
				} else if (act instanceof MainMenuActivity) {
					((MainMenuActivity)act).setLiveChangeIcon(isPause);
				}
				lDialog.cancel();		
//				lDialog = null;
			}			
		});
		lDialog.show();
	}
	
	
	//webview override url to avoid redirect at web browser outside the app
    private class WebViewOverrideUrl extends WebViewClient {
    	boolean isReceiveError = false;
    	private Activity act;
    	private Context ctx;
    	WebViewOverrideUrl(Context ctx){
    		this.ctx = ctx;
    	}
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
        	view.loadUrl(url);
            return true;
        }
        
		@Override
		public void onPageFinished(WebView view, String url) {
			super.onPageFinished(view, url);
//			if (!isReceiveError) {
//				view.setVisibility(View.VISIBLE);
//			} else {
//				view.loadUrl(url);
//				view.setVisibility(View.GONE);
//			}
		}
        
		@Override
		public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
			super.onReceivedError(view, errorCode, description, failingUrl);
//			view.loadUrl(failingUrl);

//			if (!isFailed) {
//				isFailed = true;
//				errorExitDialog(cxt, Utils.getString(ctx, R.string.LIVECHAT_DC));
//			}
				
//			isReceiveError = true;
			view.setVisibility(View.GONE);
			errorExitDialog(Utils.getString(cxt, R.string.LIVECHAT_DC));
		}
		
		private void hideErrorPage(WebView view) {
		    String customErrorPageHtml = "<html></html>";
		    view.loadData(customErrorPageHtml, "text/html", null);
		}
		
//		 @Override
//		 public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
//		     handler.proceed(); // Ignore SSL certificate errors
//		 }
    }

	//disclaimer dialog
	protected final void displayDialog(final Activity act, String message, final String mModuleId) {
		AlertDialog alertDialog;
		AlertDialog.Builder builder = new Builder(act);
		builder.setMessage(message);
		builder.setCancelable(false);
		builder.setNegativeButton(R.string.btn_ok, new Dialog.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();
				Utils.setLiveChatDisclaimerFlag(false);
				displayWebView(act ,mModuleId);
			}
		});		
		builder.setPositiveButton(R.string.btn_cancel, new Dialog.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();				
			}
		});		
		alertDialog = builder.create();
		alertDialog.show();
	}

	public void closeLiveChat(){
		webview.loadUrl("about:blank");
		webview = null;
		isPause = false;
		if(act instanceof BaseActivity) {
			((BaseActivity)act).setLiveChangeIcon(isPause);
		} else if (act instanceof MainMenuActivity) {
			((MainMenuActivity)act).setLiveChangeIcon(isPause);
		}
		lDialog.cancel();
		isFailed = false;
//		lDialog = null;
	}
//	protected final void errorExitDialog(final Context cxt, String message) {
//		AlertDialog.Builder builder;
//		if(cxt instanceof BaseActivity) {
//			builder = new Builder((BaseActivity)cxt);
//		} else if (cxt instanceof MainMenuActivity) {
//			builder = new Builder((MainMenuActivity)cxt);
//		} else {
//			//should not reach this line
//			builder = new Builder((BaseActivity)cxt);
//		}
//		
//		builder.setMessage(message);
//		builder.setCancelable(false);
//		builder.setPositiveButton(R.string.btn_ok, new Dialog.OnClickListener() {
//			@Override
//			public void onClick(DialogInterface dialog, int which) {
//				//close livechat
//				webview.loadUrl("about:blank");
//				webview = null;
//				isPause = false;
//				if(cxt instanceof BaseActivity) {
//					((BaseActivity)cxt).setLiveChangeIcon(isPause);
//				} else if (cxt instanceof MainMenuActivity) {
//					((MainMenuActivity)cxt).setLiveChangeIcon(isPause);
//				}
//				dialog.dismiss();
//				lDialog.cancel();
//				isFailed = false;
//			}
//		});		
//		builder.create().show();
//	}
	
	protected final void errorExitDialog(String message) {
		DialogInterface.OnClickListener onPositiveClickListener =  new DialogInterface.OnClickListener() {
			Bundle	rbundle;
			@Override
			public void onClick(DialogInterface dialog, int which) {
				//close livechat
				webview.loadUrl("about:blank");
				webview = null;
				isPause = false;
				if(act instanceof BaseActivity) {
					((BaseActivity)act).setLiveChangeIcon(isPause);
				} else if (act instanceof MainMenuActivity) {
					((MainMenuActivity)act).setLiveChangeIcon(isPause);
				}
				lDialog.cancel();
				isFailed = false;
	//			lDialog = null;
			}
		};
		DialogHelper.createSimpleDialog(act, message, Utils.getString(act, R.string.btn_ok), onPositiveClickListener);
	}
	
	//Build Live Chat URL param with login
	private String genLiveChatUrlParam(Context context) {
		//		LoginCra loginCra = null;
		String str 	= "";
		String c_id = "";
		//		String m_n 	= Utils.getCurrentModule();
		String m_n 	= moduleId;
		String l	= ClnEnv.getAppLocale(context);
		String c_t	= "";
		String n_n	= "";
		String e_s	= "cs.hkt.com (Android)";
		String c_s 	= "";
		if (ClnEnv.isLoggedIn()) {
			LgiCra loginCra = ClnEnv.getLgiCra();	
			c_id 	= loginCra.getOQualSvee().getSveeRec().loginId;
			c_t	= loginCra.getOChatTok();
			n_n	= loginCra.getOQualSvee().getSveeRec().nickname;
		}
		if (ClnEnv.getSessionPremierFlag()) {
			//LOB = MOB
			if(moduleId.equalsIgnoreCase(Utils.getString(mMutableContext, R.string.MODULE_MOB_MM_BILL)) || 
					moduleId.equalsIgnoreCase(Utils.getString(mMutableContext, R.string.MODULE_MOB_BILL)) || 
					moduleId.equalsIgnoreCase(Utils.getString(mMutableContext, R.string.MODULE_MOB_MM_PLAN)) || 
					moduleId.equalsIgnoreCase(Utils.getString(mMutableContext, R.string.MODULE_MOB_PLAN)) ||
					moduleId.equalsIgnoreCase(Utils.getString(mMutableContext, R.string.MODULE_101_MM_BILL)) || 
					moduleId.equalsIgnoreCase(Utils.getString(mMutableContext, R.string.MODULE_O2F_MM_BILL)) || 
					moduleId.equalsIgnoreCase(Utils.getString(mMutableContext, R.string.MODULE_101_MM_PLAN)) || 
					moduleId.equalsIgnoreCase(Utils.getString(mMutableContext, R.string.MODULE_O2F_MM_PLAN)) ||
					moduleId.equalsIgnoreCase(Utils.getString(mMutableContext, R.string.MODULE_101_BILL)) || 
					moduleId.equalsIgnoreCase(Utils.getString(mMutableContext, R.string.MODULE_O2F_BILL)) ||
					moduleId.equalsIgnoreCase(Utils.getString(mMutableContext, R.string.MODULE_101_PLAN)) || 
					moduleId.equalsIgnoreCase(Utils.getString(mMutableContext, R.string.MODULE_O2F_PLAN)) ||
					moduleId.equalsIgnoreCase(Utils.getString(mMutableContext, R.string.MODULE_MM_MAIN))) {
				if(ClnEnv.getPref(mMutableContext.getApplicationContext(), "livechat_premier_flag", false)) {
					c_s = "MOB-CS";
				} else {
					c_s = "PREM";
				}
			} 
			//LOB = Others
			else {
				c_s = "PREM";
			}
		} else {
			if(moduleId.equalsIgnoreCase(Utils.getString(mMutableContext, R.string.MODULE_MOB_MM_BILL)) || 
					moduleId.equalsIgnoreCase(Utils.getString(mMutableContext, R.string.MODULE_MOB_BILL)) || 
					moduleId.equalsIgnoreCase(Utils.getString(mMutableContext, R.string.MODULE_MOB_MM_PLAN)) || 
					moduleId.equalsIgnoreCase(Utils.getString(mMutableContext, R.string.MODULE_MOB_PLAN)) ||
					moduleId.equalsIgnoreCase(Utils.getString(mMutableContext, R.string.MODULE_101_MM_BILL)) || 
					moduleId.equalsIgnoreCase(Utils.getString(mMutableContext, R.string.MODULE_O2F_MM_BILL)) || 
					moduleId.equalsIgnoreCase(Utils.getString(mMutableContext, R.string.MODULE_101_MM_PLAN)) || 
					moduleId.equalsIgnoreCase(Utils.getString(mMutableContext, R.string.MODULE_O2F_MM_PLAN)) ||
					moduleId.equalsIgnoreCase(Utils.getString(mMutableContext, R.string.MODULE_101_BILL)) || 
					moduleId.equalsIgnoreCase(Utils.getString(mMutableContext, R.string.MODULE_O2F_BILL)) ||
					moduleId.equalsIgnoreCase(Utils.getString(mMutableContext, R.string.MODULE_101_PLAN)) || 
					moduleId.equalsIgnoreCase(Utils.getString(mMutableContext, R.string.MODULE_O2F_PLAN)) ||
					moduleId.equalsIgnoreCase(Utils.getString(mMutableContext, R.string.MODULE_MM_MAIN))) {
				if(ClnEnv.getPref(mMutableContext.getApplicationContext(), "livechat_consumer_flag", false)) {
					c_s = "MOB-CS";
				} else {
					c_s = "CONS";
				}
			} 
			//LOB = Others
			else {
				c_s = "CONS";
			}
		}
		
		if(m_n.equalsIgnoreCase(Utils.getString(mMutableContext, R.string.MODULE_PCD_PLAN_4MOBCS))) {
			c_s = "MOB-CS";
			m_n = mMutableContext.getString(R.string.MODULE_PCD_PLAN);
		}
		
		if(m_n.equalsIgnoreCase(Utils.getString(mMutableContext, R.string.MODULE_PCD_BILL_4MOBCS))) {
			c_s = "MOB-CS";
			m_n = mMutableContext.getString(R.string.MODULE_PCD_BILL);
		}
		
		//String overrideChatTok = "8838a93cbe8f0941b3fe78f2faab6ee3239da01ee572b424f1f1d4e1a954f08d";
		if (c_id.equals(""))
			str = str + "c_id=" + urlEncode("Guest");
		else
			str = str + "c_id=" + urlEncode(c_id);

		if (m_n.equals(""))
			str = str + "&" + "m_n=" + "";
		else
			str = str + "&" + "m_n=" + urlEncode(m_n);

		if (l.equals(""))
			str = str + "&" + "l=" + "";
		else
			str = str + "&" + "l=" + urlEncode(l);

		if (c_t.equals(""))
			str = str + "&" + "c_t=" + urlEncode("NTK");
		else
			str = str + "&" + "c_t=" + urlEncode(c_t);

		if (n_n.equals(""))
			str = str + "&" + "n_n=" + urlEncode("Guest");
		else
			str = str + "&" + "n_n=" + urlEncode(n_n);

		if (e_s.equals(""))
			str = str + "&" + "e_s=" + "";
		else
			str = str + "&" + "e_s=" + urlEncode(e_s);

		if (c_s.equals("")) {
			//do nothing	
		}
		else
			str = str + "&" + "c_s=" + urlEncode(c_s);
		if (debug) Log.d("LiveChat URL: ", str);
		return str;
	}

	private String urlEncode(String url) {
		String encodedurl = "";
		try {
			encodedurl = URLEncoder.encode(url,"UTF-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		return encodedurl;
	}
	
	@SuppressLint("NewApi")
    protected void openFileInput(final ValueCallback<Uri> fileUploadCallbackFirst, final ValueCallback<Uri[]> fileUploadCallbackSecond, Activity act) {
        if (mFileUploadCallbackFirst != null) {
            mFileUploadCallbackFirst.onReceiveValue(null);
        }
        mFileUploadCallbackFirst = fileUploadCallbackFirst;

        if (mFileUploadCallbackSecond != null) {
            mFileUploadCallbackSecond.onReceiveValue(null);
        }
        mFileUploadCallbackSecond = fileUploadCallbackSecond;

        Intent i = new Intent(Intent.ACTION_GET_CONTENT);
        i.addCategory(Intent.CATEGORY_OPENABLE);
        i.setType(mUploadableFileTypes);
        Log.i("BaseActivity", "AAAAA1");
        act.startActivityForResult(Intent.createChooser(i, "File Chooser"), mRequestCodeFilePicker);
    }
}