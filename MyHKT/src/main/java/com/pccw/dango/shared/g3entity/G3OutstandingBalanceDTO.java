package com.pccw.dango.shared.g3entity;

import java.io.Serializable;
import java.sql.Timestamp;

public class G3OutstandingBalanceDTO  implements Serializable 
{
    private static final long serialVersionUID = -3083228616817108855L;
    
    private double                  currOSBal;//MrbAcctTotalOSBalDTO Current Balance
    private Timestamp               lastBillDate; //MrbAcctTotalOSBalDTO Last Bill Date
    private String                  acctNum; //MrbAcctDTO Account No.
    private double                  totalOSBal;
    private Timestamp               pymtDueDate;
    
    // newly added for CSL
    private double                  overdueAmt;
    private Timestamp               contractEndDate;
    
    
    public double getCurrOSBal() {
        return currOSBal;
    }
    public void setCurrOSBal(double currOSBal) {
        this.currOSBal = currOSBal;
    }
    
    public String getAcctNum() {
        return acctNum;
    }
    public void setAcctNum(String acctNum) {
        this.acctNum = acctNum;
    }   
    
    public Timestamp getLastBillDate() {
        return lastBillDate;
    }
    public void setLastBillDate(Timestamp lastBillDate) {
        this.lastBillDate = lastBillDate;
    }
    
    public double getTotalOSBal() {
        return totalOSBal;
    }
    public void setTotalOSBal(double totalOSBal) {
        this.totalOSBal = totalOSBal;
    }   

    public Timestamp getPymtDueDate() {
        return pymtDueDate;
    }
    public void setPymtDueDate(Timestamp pymtDueDate) {
        this.pymtDueDate = pymtDueDate;
    }
    
    
    public double getOverdueAmt() 
    {
        return overdueAmt;
    }
    
    
    public void setOverdueAmt(double rOverdueAmt) 
    {
        overdueAmt = rOverdueAmt;
    }
    
    
    public Timestamp getContractEndDate() 
    {
        return contractEndDate;
    }
    
    
    public void setContractEndDate(Timestamp rContractEndDate) 
    {
        contractEndDate = rContractEndDate;
    }
}
