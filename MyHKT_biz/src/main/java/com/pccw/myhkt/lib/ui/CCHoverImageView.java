package com.pccw.myhkt.lib.ui;

import com.pccw.myhkt.ClnEnv;
import com.pccw.biz.myhkt.R;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.PorterDuff;
import android.os.Build;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;

public class CCHoverImageView extends ImageView {

	public CCHoverImageView(Context context) {
		super(context);
		setOnTouch();
		// TODO Auto-generated constructor stub
	}
	public CCHoverImageView(Context context, AttributeSet attrs) {
		super(context, attrs);
		setOnTouch();
		// TODO Auto-generated constructor stub
	}
	public CCHoverImageView(Context context, AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);
		// TODO Auto-generated constructor stub
		setOnTouch();
	}

	public ColorFilter colorFilter = null;
	private  void setOnTouch(){
		setOnTouchListener(new OnTouchListener(){
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				switch (event.getAction()) {
				case MotionEvent.ACTION_DOWN: {
					if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.JELLY_BEAN) {
						colorFilter = ((ImageView) v).getColorFilter();
					}
					
					if (v.getId() == R.id.navbar_button_left) {
						if (!ClnEnv.getSessionPremierFlag()) {
							int		m_color				= Color.argb(128, 128, 128, 128);
							((ImageView) v).setColorFilter(m_color, PorterDuff.Mode.SRC_ATOP);
						} else {
							int		m_color				= Color.argb(255, 191, 191, 191);
							((ImageView) v).setColorFilter(m_color, PorterDuff.Mode.SRC_IN);
						}
					} else {
						int		m_color				= Color.argb(128, 128, 128, 128);
						((ImageView) v).setColorFilter(m_color, PorterDuff.Mode.SRC_ATOP);
					} 
					break;
				}
				case MotionEvent.ACTION_CANCEL:
				case MotionEvent.ACTION_UP: {
					((ImageView) v).setColorFilter(colorFilter);
				}
				}
				return false;
			}
		});
	}	
}
