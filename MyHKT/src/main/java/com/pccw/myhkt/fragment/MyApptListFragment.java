package com.pccw.myhkt.fragment;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import androidx.annotation.NonNull;

import com.pccw.dango.shared.cra.ApptCra;
import com.pccw.dango.shared.entity.GnrlAppt;
import com.pccw.dango.shared.entity.SrvReq;
import com.pccw.dango.shared.tool.RC;
import com.pccw.myhkt.APIsManager;
import com.pccw.myhkt.APIsResponse;
import com.pccw.myhkt.ClnEnv;
import com.pccw.myhkt.DialogHelper;
import com.pccw.myhkt.FAWrapper;
import com.pccw.myhkt.R;
import com.pccw.myhkt.Utils;
import com.pccw.myhkt.activity.BaseActivity;
import com.pccw.myhkt.adapter.MyApptListViewAdapter;
import com.pccw.myhkt.lib.ui.AAQuery;

public class MyApptListFragment extends BaseMyApptFragment {
	private MyApptListFragment me;
	private View myView;
	private AAQuery aq;
	private SrvReq selectSrvReq = null;
	private ApptCra	apptCra;
	
	protected String moduleId;
	
	// List View
	private MyApptListViewAdapter myApptListViewAdapter;
	
	@Override
	public void onAttach(@NonNull Activity activity) {
		super.onAttach(activity);
	}

	public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		me = this;		
		View fragmentLayout = inflater.inflate(R.layout.fragment_myapptlist, container, false);
		myView = fragmentLayout;
		return fragmentLayout;
	}

	@Override
	public void onStart() {
		super.onStart();
	}
	
	@Override
	public void onPause(){
		super.onPause();
	}
	
	@Override
	public void onResume(){
		super.onResume();
		moduleId = getResources().getString(R.string.MODULE_MY_APPT);
		// Screen Tracker
		FAWrapper.getInstance().sendFAScreen(getActivity(), R.string.CONST_SCRN_APPOINTMENTLIST, false);
	}

	protected void initUI() {
		aq = new AAQuery(myView);
		int buttonPadding =  (int) getResources().getDimension(R.dimen.reg_logo_padding);

		//UI
		aq.id(R.id.myapptlist_header).textSize(getResources().getInteger(R.integer.textsize_default_int));
		aq.id(R.id.myapptlist_empty).textSize(getResources().getInteger(R.integer.textsize_default_int));
		aq.marginpx(R.id.myapptlist_listview, buttonPadding, 0, buttonPadding, 0);
		aq.line(R.id.myapptlist_listview_line);
		aq.marginpx(R.id.myapptlist_listview_line, buttonPadding, 0, buttonPadding, 0);
		
		aq.id(R.id.question_mark).clicked(this, "onClick");
		aq.marginpx(R.id.question_mark, buttonPadding, 0, buttonPadding, 0);
		
		// Clear all data before calling getApptList API
		doneGetApptList(new ApptCra());
		selectSrvReq = null;
		// Get SubscriptionRec from arraylist
		getApptList();
	}

	protected final void refreshData() {
		super.refreshData();
	}
	
	public void onClick(View v) {
		switch (v.getId()) {
			case R.id.question_mark:
				DialogHelper.createSimpleDialog(me.getActivity(), getResString(R.string.MYHKT_APPT_REMARK) , getString(R.string.btn_ok));
				break;
		}
	}
	
	private final void doneGetApptList(ApptCra rApptCra) {
		aq = new AAQuery(myView);
		// Get ApptAry from Cra
		GnrlAppt[] myApptAry = rApptCra.getOGnrlApptAry();
		SrvReq[] mySRApptAry = rApptCra.getOSrvReqAry();
		
		// List View
		myApptListViewAdapter = new MyApptListViewAdapter(this, myApptAry, mySRApptAry);
		aq.id(R.id.myapptlist_listview).getListView().setChoiceMode(ListView.CHOICE_MODE_SINGLE);
		aq.id(R.id.myapptlist_listview).getListView().setAdapter(myApptListViewAdapter);
		aq.id(R.id.myapptlist_listview).getListView().setEmptyView(aq.id(R.id.myapptlist_empty).getTextView());
		aq.id(R.id.myapptlist_listview).getListView().setOnItemClickListener(onItemClickListener);
	}
	
	private final void getApptList() {
		if(isQualSveeNotNull()){
			ApptCra apptCra = new ApptCra();

			apptCra.setILoginId(ClnEnv.getQualSvee().getSveeRec().loginId);
			apptCra.setICustRec(ClnEnv.getQualSvee().getCustRec().copyMe());
			apptCra.setISubnRecAry(ClnEnv.getQualSvee().getSubnRecAry());

			APIsManager.doGetAppointment(this, apptCra);
		}
	}

	private final boolean isQualSveeNotNull(){
		boolean result = false;

		if( ClnEnv.getQualSvee() != null
				&& ClnEnv.getQualSvee().getSveeRec() != null
				&& ClnEnv.getQualSvee().getSveeRec().loginId != null){
			result = true;
		}

		return result;
	}
	
	private final void doGetAvailableTimeSlot(SrvReq srvReq) {
		ApptCra apptCra = new ApptCra();
		apptCra.setILoginId(ClnEnv.getQualSvee().getSveeRec().loginId);
		apptCra.setICustRec(ClnEnv.getQualSvee().getCustRec());
		apptCra.setISRApptInfo(srvReq.getApptInfo().copyMe());
		apptCra.getISRApptInfo().setApptTS(srvReq.getApptTS().copyMe());
		
		APIsManager.doGetAvailableTimeSlot(this, apptCra);
	}
	
	private OnItemClickListener	onItemClickListener	= new OnItemClickListener() {
		public void onItemClick(AdapterView<?> adapterView, View view, int position, long arg3) {
			if (myApptListViewAdapter.isSRItem(position)) {
				me.selectSrvReq = (SrvReq) myApptListViewAdapter.getItem(position);
				if ("Y".equalsIgnoreCase(me.selectSrvReq.getAllowUpdInd())) {
					//TODO getavailabletimeslot API call
					me.doGetAvailableTimeSlot(me.selectSrvReq);
				} else {
					callback_main.setSrvReq(me.selectSrvReq);
					
					//TODO go to MyApptUpdateFragment
					callback_main.setActiveSubview(R.string.CONST_SELECTEDFRAG_MYAPPTUPDATE);
					callback_main.displaySubview();
				}
			} else {
				GnrlAppt gnrlAppt = (GnrlAppt) myApptListViewAdapter.getItem(position);
				if (gnrlAppt.getShowDtls()) {
					String drgOrdNumAry ="";
					
					for (int i = 0 ; i < gnrlAppt.getDrgOrdNumAry().length ; i++) {
						drgOrdNumAry = drgOrdNumAry + gnrlAppt.getDrgOrdNumAry()[i] + "\n";
					}
					
					//ApptDetailDialog
					String message = String.format("%s\n\n%s\n%s\n%s\n%s", getString(R.string.ATIMF_ORD_NUM), drgOrdNumAry, getString(R.string.ATIMF_HTLN), getString(R.string.ATIMF_HTLN_NUM), getString(R.string.ATIMF_HTLN_SRVHR));
					DialogHelper.createSimpleDialog(getActivity(), getString(R.string.ATIMF_HDR), message, getString(R.string.btn_ok), new OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							dialog.dismiss();
							alertDialog = null;
						}
					});
				}
			}
		}
	};

	@Override
	public void onSuccess(APIsResponse response) {
		if (APIsManager.APPT.equals(response.getActionTy())) {
			apptCra = new ApptCra();
			apptCra = (ApptCra) response.getCra();
			
			Utils.filterApptToShowClock(getActivity(), apptCra);
			
			doneGetApptList(apptCra);
		} else if (APIsManager.SR_AVA_TS.equals(response.getActionTy())) {
			apptCra = new ApptCra();
			apptCra = (ApptCra) response.getCra();
			
			callback_main.setSrvReq(me.selectSrvReq);
			callback_main.setApptInfo(apptCra.getOSRApptInfo()); //TODO it should be callback_main.setSRApptInfo(apptCra.getOSRApptInfo());
			
			//go to update fragment
			callback_main.setActiveSubview(R.string.CONST_SELECTEDFRAG_MYAPPTUPDATE);
			callback_main.displaySubview();
		}
	}

	@Override
	public void onFail(APIsResponse response) {
		// TODO Auto-generated method stub
		// General Error Message
		if (!"".equals(response.getMessage()) && response.getMessage() != null) {
			DialogHelper.createSimpleDialog(getActivity(),  response.getMessage());
		} else if (RC.TCSESS_MM.equalsIgnoreCase(response.getReply().getCode())) {
			BaseActivity.ivSessDialog();
		} else {
			DialogHelper.createSimpleDialog(getActivity(),  ClnEnv.getRPCErrMsg(getActivity(), response.getReply().getCode()));
		}		
	}

}
