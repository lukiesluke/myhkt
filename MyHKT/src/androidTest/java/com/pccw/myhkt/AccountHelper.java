package com.pccw.myhkt;

import com.pccw.myhkt.model.LoginInfo;

public class AccountHelper {

    public static LoginInfo getUATAccount1() {
        return new LoginInfo("del001@pccw.com", "Aa12345");
    }

    public static LoginInfo getUATAccount2() {
        return new LoginInfo("del002@pccw.com", "Aa12345");
    }

    public static LoginInfo getUATAccount3() {
        return new LoginInfo("del003@pccw.com", "Aa12345");
    }

    public static LoginInfo getUATAccount4() {
        return new LoginInfo("del004@pccw.com", "Aa12345");
    }

    public static LoginInfo getUATAccount5() {
        return new LoginInfo("del005@pccw.com", "Aa12345");
    }

    public static LoginInfo getUATAccount6() {
        return new LoginInfo("testingsr001@gmail.com", "Aa12345");
    }

    public static LoginInfo getUATAccount7() {
        return new LoginInfo("testingsr002@gmail.com", "Aa12345");
    }

    public static LoginInfo getUATAccount8() {
        return new LoginInfo("greatchina2@pccw.com", "Aa12345");
    }

    public static LoginInfo getUATAccount9() {
        return new LoginInfo("uatsr3@gmail.com", "Aa12345");
    }

    public static LoginInfo getUATAccount10() {
        return new LoginInfo("greatchina3@pccw.com", "a12345");
    }

    public static LoginInfo getPRDAccount1() {
        return new LoginInfo("keith.kk.mak@pccw.com", "Abcd1234");
    }

    public static LoginInfo getPRDAccount2() {
        return new LoginInfo("mrleewaiming@hotmail.com", "k1297937");
    }

    public static LoginInfo getPRDAccount3() {
        return new LoginInfo("myhkt1@gmail.com", "Aa12345");
    }

}
