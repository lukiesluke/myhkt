package com.pccw.myhkt.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.pccw.myhkt.R;
import com.pccw.myhkt.Utils;

public class ShopTypeListViewAdapter extends BaseAdapter {
    // ShopRec
    // Assume the input array has been processed
    private final String[] shopType;
    private final boolean isZh;
    public Context context;
    public LayoutInflater inflater;

    public ShopTypeListViewAdapter(Context context, String[] st) {
        super();
        this.context = context;
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        shopType = st;

        // Language indicator
        isZh = "zh".equalsIgnoreCase(Utils.getString(context, R.string.myhkt_lang));
    }

    public int getCount() {
        if (shopType == null) return 0;
        return shopType.length;
    }

    public Object getItem(int position) {
        if (position >= 0 && position < shopType.length) {
            return shopType[position];
        } else {
            return null;
        }
    }

    public long getItemId(int position) {
        return position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        try {
            ViewHolder holder;

            if (convertView == null) {
                holder = new ViewHolder();
                convertView = inflater.inflate(R.layout.adapter_shoptypelist, null);

                holder.adapter_shoptypelist_logo = (ImageView) convertView.findViewById(R.id.adapter_shoptypelist_logo);
                holder.adapter_shoptypelist_header = (TextView) convertView.findViewById(R.id.adapter_shoptypelist_header);

                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }

            // Shop type - PCCWHKT Shop or SmartLiving or CSC?
            //new icon by shop type
            switch (position) {
                case 0:
                    holder.adapter_shoptypelist_logo.setImageResource(R.drawable.shop_self);
                    holder.adapter_shoptypelist_header.setText(R.string.myhkt_shop_myloc);
                    break;
                case 1:
                    holder.adapter_shoptypelist_logo.setImageResource(R.drawable.shop_all);
                    holder.adapter_shoptypelist_header.setText(R.string.myhkt_shop_allshop);
                    break;
                case 2:
                    holder.adapter_shoptypelist_logo.setImageResource(R.drawable.shop_iot);
                    holder.adapter_shoptypelist_header.setText(R.string.myhkt_shop_iot);
                    break;
//				case 3:
//					holder.adapter_shoptypelist_logo.setImageResource(R.drawable.shop_hkt);
//					holder.adapter_shoptypelist_header.setText(R.string.myhkt_shop_hkt);
//					break;
                case 3:
                    holder.adapter_shoptypelist_logo.setImageResource(R.drawable.shop_1010);
                    holder.adapter_shoptypelist_header.setText(R.string.myhkt_shop_1010);
                    break;
                case 4:
                    holder.adapter_shoptypelist_logo.setImageResource(R.drawable.shop_csl);
                    holder.adapter_shoptypelist_header.setText(R.string.myhkt_shop_csl);
                    break;
                case 5:
                    holder.adapter_shoptypelist_logo.setImageResource(R.drawable.shop_cs);
                    holder.adapter_shoptypelist_header.setText(R.string.myhkt_shop_cs);
                    break;
//				case 6:
//					holder.adapter_shoptypelist_logo.setImageResource(R.drawable.shop_smart);
//					holder.adapter_shoptypelist_header.setText(R.string.myhkt_shop_smart_living);
//					break;
                case 6:
                    holder.adapter_shoptypelist_logo.setImageResource(R.drawable.shop_biz);
                    holder.adapter_shoptypelist_header.setText(R.string.myhkt_shop_biz);
                    break;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return convertView;
    }

    public static class ViewHolder {
        ImageView adapter_shoptypelist_logo;
        TextView adapter_shoptypelist_header;
    }
}
