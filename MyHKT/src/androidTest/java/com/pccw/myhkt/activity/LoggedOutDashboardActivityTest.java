package com.pccw.myhkt.activity;

import android.content.Context;
import androidx.test.espresso.intent.Intents;
import androidx.test.rule.ActivityTestRule;
import androidx.test.ext.junit.runners.AndroidJUnit4;
import android.view.View;

import com.pccw.myhkt.R;
import com.pccw.myhkt.base.BaseTest;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static androidx.test.platform.app.InstrumentationRegistry.getInstrumentation;

@Ignore
@RunWith(AndroidJUnit4.class)
public class LoggedOutDashboardActivityTest extends BaseTest {
    private Context context;

    @Rule
    public ActivityTestRule<MainMenuActivity> activityTestRule =
            new ActivityTestRule<>(MainMenuActivity.class);

    @Before
    public void setUp() {
        context = getInstrumentation().getTargetContext();
        Intents.init();
    }

    @After
    public void tearDown(){
        Intents.release();
    }

    @Test
    public void testLoggedOutDisplayUI() {
        checkStringIsDisplayed(context, R.string.app_name);
//        checkViewMatchingDrawable(R.id.navbar_button_left, R.id.navbar_base_layout,
//                R.drawable.btn_slidemenu);
        checkViewVisibility(R.id.navbar_button_left,
                R.id.navbar_base_layout, View.VISIBLE);
        checkViewVisibility(R.id.navbar_message_icon,
                R.id.navbar_base_layout, View.GONE);
        checkViewVisibility(R.id.mainmenu_topbanner, View.VISIBLE);
        checkViewVisibility(R.id.mainmenu_pccwlogo, View.VISIBLE);
        checkViewVisibility(R.id.mainmenu_pccwlogo1, View.VISIBLE);
        checkViewWithText(R.id.mainmenu_label_welcome, "");
        checkViewVisibility(R.id.mainmenu_gridlayout, View.VISIBLE);
    }

    @Test
    public void testLoggedOutMenuClick() {
        clickLeftNavBar();
        checkStringIsDisplayed(context, R.string.myhkt_LGIF_LOGIN);
        checkStringIsDisplayed(context, R.string.myhkt_settings);
        checkStringIsDisplayed(context, R.string.myhkt_about);
        checkStringIsDisplayed(context, R.string.myhkt_menu_tnc);
    }

    @Test
    public void testRedirectLoginScreen() {
        gotoMainMenuItem(0);
        checkIntendedActivity(LoginActivity.class.getName());
    }

}
