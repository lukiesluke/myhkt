package com.pccw.myhkt.fragment;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.text.Html;
import android.text.TextPaint;
import android.text.style.ClickableSpan;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.hannesdorfmann.mosby3.mvp.MvpFragment;
import com.pccw.myhkt.ClnEnv;
import com.pccw.myhkt.R;
import com.pccw.myhkt.Utils;
import com.pccw.myhkt.presenters.IDDNewsPresenter;
import com.pccw.myhkt.views.IDDNewsView;

import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;

/************************************************************************
 File       : IDDNewsFragment.java
 Desc       : View for showing csl/1010/o2f Usage/Roaming
 Name       : IDDNewsFragment
 Created by : Jarlou Valenzuela
 Date       : 07/12/2018

 Change History:
 Date       Modified By			Description
 ---------- ----------------	-------------------------------
 15/03/2016 Jarlou Valenzuela	- First draft
 27/03/2019 In Yong Lee         - Fixed bug for IDD News fragment
 01/04/2019 In Yong Lee         - Constraint Layout
 *************************************************************************/

public class IDDNewsV2Fragment extends MvpFragment<IDDNewsView,
        IDDNewsPresenter> implements IDDNewsView {

    private static final String TAG = ContactUsBusinessFragment.class.getName();

    @BindView(R.id.tv_idd_news_c22) TextView tvIddNewsC22;
    @BindView(R.id.tv_idd_news_local_descriptor) TextView tvIddLocalDescriptor;

    private boolean isZh;

    public IDDNewsV2Fragment() {
        // Required empty public constructor
    }

    @Override
    public IDDNewsPresenter createPresenter() {
        return new IDDNewsPresenter();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_iddnewsv2, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        initialise();
    }

    private void initialise() {
        checkIsZh();
        presenter.initialise(this.getMvpView());
        tvIddNewsC22.setText(Html.fromHtml(formatC22()));
        formatSpan();
    }

    private void checkIsZh() {
        Context ctx = getContext();
        isZh = !ClnEnv.getAppLocale(ctx).equals(Utils.getString(ctx, R.string.CONST_LOCALE_EN));
    }


    private String formatC22(){
        String c22 = String.format(getString(R.string.MYHKT_IDD_NEWS_c22),"<big><font color='red'><b><i>","</b></font>","","</i></big>","<i><small>","</small></i>");
        return c22.replaceAll("\n","<br/>");
    }

    private void formatSpan() {
        Utils.setTextViewClickableSpan(tvIddLocalDescriptor,
                new String[]{getString(R.string.MYHKT_IDD_NEWS_CODESTIMES_CONTENT_hyperlink)}, new ClickableSpan[]{LINK_BROWSER});
    }

    @Override
    public void doPopupDialog (String title, String content){
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        TextView tncTitle = new TextView(getContext());
        tncTitle.setText(title);
        tncTitle.setTextSize(18);
        tncTitle.setTextColor(Color.BLACK);
        tncTitle.setGravity(Gravity.CENTER);
        builder.setCustomTitle(tncTitle);
        builder.setMessage(getString(R.string.MYHKT_IDD_NEWS_TNC_CONTENT));
        builder.setPositiveButton(R.string.btn_ok, (dialog, id) -> dialog.dismiss());

        AlertDialog alert = builder.create();

        alert.show();

        TextView titleView = alert.findViewById(
                getContext().getResources().getIdentifier(
                        "alertTitle", "id", "android"));
        if (titleView != null) {
            titleView.setGravity(Gravity.CENTER);
        }
    }

    ClickableSpan LINK_BROWSER = new ClickableSpan() {
        @Override
        public void onClick(@NonNull View view) {
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setData(Uri.parse(isZh ?
                    "https://www.hkt.com/customer-services/directory-enquiries/international-telephone-numbers?locale=zh" :
                    "https://www.hkt.com/customer-services/directory-enquiries/international-telephone-numbers?locale=en"));
            startActivity(intent);
            requireActivity().overridePendingTransition(R.anim.right_slide_in, R.anim.left_slide_out);
        }

        @Override
        public void updateDrawState(TextPaint ds) {
            super.updateDrawState(ds);
            ds.setColor(getResources().getColor(R.color.hkt_buttonblue));
        }
    };
}
