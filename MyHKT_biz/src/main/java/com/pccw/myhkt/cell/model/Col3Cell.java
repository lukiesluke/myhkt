package com.pccw.myhkt.cell.model;

import com.pccw.biz.myhkt.R;

import android.graphics.Typeface;
import android.util.Log;

public class Col3Cell extends Cell{


	private int widthArray[];

	@Deprecated
	public Col3Cell(String title, String[] textArray, int[] widthArray, int bgColorId) {
		super();
		type = Cell.COL3;
		this.title = title;
		this.textArray = textArray;
		this.widthArray = widthArray;
		titleColorId = R.color.black;
		contentColorId = R.color.hkt_txtcolor_grey;
		titleTypeface = Typeface.NORMAL;
		titleSizeDelta = -2;
		contentSizeDelta = -2;	
		this.bgColorId = bgColorId;
	}
	
	public int[] getWidthArray() {
		return widthArray;
	}

	public void setWidthArray(int[] widthArray) {
		this.widthArray = widthArray;
	}
	
}