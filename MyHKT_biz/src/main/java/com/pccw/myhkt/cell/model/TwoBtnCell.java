package com.pccw.myhkt.cell.model;

import android.graphics.drawable.Drawable;
import android.view.View.OnClickListener;

import com.pccw.biz.myhkt.R;

public class TwoBtnCell extends Cell{

	private int leftDraw;	
	private int rightDraw;
	private OnClickListener leftClickListener;
	private OnClickListener rightClickListener;
	private Drawable leftDrawable;	
	private Drawable rightDrawable;
	

	private Boolean isLeftHide = true;
	private Boolean isRightHide = true;
	
	public TwoBtnCell(String title, String content, int leftDraw, int rightDraw, String[] clickArray){
		super();
		//Default setting
		type = Cell.TWOBTN;
		titleColorId = R.color.black;
		contentColorId = R.color.hkt_textcolor;
		titleSizeDelta = 0;
		contentSizeDelta = -2;

		this.title = title;
		this.content = content;
		this.leftDraw = leftDraw;
		this.rightDraw = rightDraw;
		this.clickArray = clickArray;
	}
	
	public int getLeftDraw() {
		return leftDraw;
	}

	public void setLeftDraw(int leftDraw) {
		this.leftDraw = leftDraw;
	}

	public int getRightDraw() {
		return rightDraw;
	}

	public void setRightDraw(int rightDraw) {
		this.rightDraw = rightDraw;
	}
	public OnClickListener getLeftClickListener() {
		return leftClickListener;
	}

	public void setLeftClickListener(OnClickListener leftClickListener) {
		this.leftClickListener = leftClickListener;
	}

	public OnClickListener getRightClickListener() {
		return rightClickListener;
	}

	public void setRightClickListener(OnClickListener rightClickListener) {
		this.rightClickListener = rightClickListener;
	}
	
	public Drawable getLeftDrawable() {
		return leftDrawable;
	}

	public void setLeftDrawable(Drawable leftDrawable) {
		this.leftDrawable = leftDrawable;
	}

	public Drawable getRightDrawable() {
		return rightDrawable;
	}

	public void setRightDrawable(Drawable rightDrawable) {
		this.rightDrawable = rightDrawable;
	}
	public Boolean getIsLeftHide() {
		return isLeftHide;
	}

	public void setIsLeftHide(Boolean isLeftHide) {
		this.isLeftHide = isLeftHide;
	}

	public Boolean getIsRightHide() {
		return isRightHide;
	}

	public void setIsRightHide(Boolean isRightHide) {
		this.isRightHide = isRightHide;
	}

}