/*
    Class for Journal Action IDs

    Journal provides an communication mechanism between the Web F/E
    and Back-end Admin Workstation. In short, a API will be provided
    to write an Activity Log but no other action.
    
    This Class defines the corresponding ACTION_ID.

    Keywords
    --------
    $URL: $
    $Rev: $
    $Date: $
    $Author: $
*/

package com.pccw.dango.shared.tool;

import java.io.Serializable;


public class Jrnl implements Serializable
{
    private static final long serialVersionUID = 974074874123960651L;
    
    public final static String  JRL_CHAT4PON    = "JRL_CHAT4PON";   /* Live Chat triggered by PON       */
 

    public static void main(String rArg[])
    {
        System.out.println(getVer());
        return;
    }
    
    
    public static String getVer()
    {
        return ("$URL: $, $Rev: $");
    }


    public Jrnl()
    {
        /*
            Constuctor.
        */
    }
}    
