package com.pccw.myhkt.fragment;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;

import com.pccw.dango.shared.cra.ApptCra;
import com.pccw.dango.shared.cra.BaseCraEx;
import com.pccw.dango.shared.cra.LnttCra;
import com.pccw.dango.shared.entity.LtrsRec;
import com.pccw.dango.shared.entity.SubnRec;
import com.pccw.dango.shared.tool.RC;
import com.pccw.myhkt.APIsManager;
import com.pccw.myhkt.APIsResponse;
import com.pccw.myhkt.ClnEnv;
import com.pccw.myhkt.FAWrapper;
import com.pccw.myhkt.GlobalDialog;
import com.pccw.myhkt.InterpretRCManager;
import com.pccw.myhkt.R;
import com.pccw.myhkt.Utils;
import com.pccw.myhkt.activity.BaseActivity;
import com.pccw.myhkt.activity.ImageViewerActivity;
import com.pccw.myhkt.activity.MyApptActivity;
import com.pccw.myhkt.cell.model.BigTextCell;
import com.pccw.myhkt.cell.model.Cell;
import com.pccw.myhkt.cell.model.IconTextCell;
import com.pccw.myhkt.cell.model.ImageViewCell;
import com.pccw.myhkt.cell.model.LineTest;
import com.pccw.myhkt.cell.model.SingleBtnCell;
import com.pccw.myhkt.cell.model.WebViewCell;
import com.pccw.myhkt.cell.view.CellViewAdapter;
import com.pccw.myhkt.fragment.SRCreationFragment.OnLnttListener;
import com.pccw.myhkt.lib.ui.AAQuery;
import com.pccw.myhkt.lib.ui.HKTButton;
import com.pccw.myhkt.model.LnttAgent;
import com.pccw.myhkt.service.LineTestIntentService;

import java.util.ArrayList;
import java.util.List;


/************************************************************************
 * File : LnttStartFragment.java 
 * Desc : Reg Menu 
 * Name : LnttStartFragment
 * by : Andy Wong 
 * Date : 23/02/2016
 *
 * Change History:
 * Date       Modified By		Description
 * ---------- ----------------	-------------------------------
 * 23/02/2016 Andy Wong 		-First draft
 *************************************************************************/
public class LnttRsFragment extends BaseServiceFragment {

    private String TAG = "LnttRsFragment";
    private LnttRsFragment me;
    private AAQuery aq;
    private int extralinespace;
    private int titleWidth;
    private int botBarHeight;

    //Image URL
    private String modemImageLargeURL = "";

    private List<Cell> cellList;
    private CellViewAdapter cellViewAdapter;
    private IconTextCell titleCell;
    private WebViewCell enquiryCell;
    private SingleBtnCell singleBtnCell;

    //Cell part
    private BigTextCell mdmMsgCell;
    private BigTextCell timeCell;
    private BigTextCell bbMsgCell;
    private BigTextCell fixlineMsgCell;
    private BigTextCell resultCell;
    private BigTextCell fixlineTitleCell;
    private ImageViewCell imageViewCell;
    private LineTest pcdLiveTestCell;
    private LineTest tvLiveTestCell;
    private LineTest eyeLiveTestCell;
    private int btnWidth = 0;
    private LinearLayout frame;

    private OnLnttListener callback_lntt;
    private LnttAgent lnttAgent;
    private LnttCra lnttCra;
    private ApptCra apptCra;
    private Boolean isLnttStart = false;

    private HKTButton hktBtn; // Test Again
    private HKTButton mRebootModemBtn;
    private String moduleId;

    @Override
    public void onAttach(@NonNull Activity activity) {
        super.onAttach(activity);
        try {
            lnttCra = new LnttCra();
            apptCra = new ApptCra();
            callback_lntt = (OnLnttListener) getParentFragment();
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement OnmyprofloginListener");
        }
    }

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        me = this;
        View fragmentLayout = inflater.inflate(R.layout.fragment_linetest_st, container, false);

        aq = new AAQuery(fragmentLayout);
        initData();
        if (debug) Log.i(TAG, "onCreateView");
        return fragmentLayout;
    }

    protected void initData() {
        super.initData();
        titleWidth = (int) getResources().getDimension(R.dimen.idd_title_width);
        botBarHeight = (int) getResources().getDimension(R.dimen.botbar_height);
        btnWidth = deviceWidth - basePadding * 4;
        cellViewAdapter = new CellViewAdapter(me.getActivity());
    }

    @Override
    protected void initUI() {
        cellList = new ArrayList<Cell>();
        //ScrollView
        aq.id(R.id.linetest_st_sv).backgroundColorId(R.color.white);
        frame = (LinearLayout) aq.id(R.id.linetest_st_frame).getView();

        setLTSUI();

        //Bot bar
        aq.id(R.id.linetest_st_botar).backgroundColorId(R.color.white);
        aq.padding(R.id.linetest_st_botar, basePadding, 0, basePadding, 0);
        aq.gravity(R.id.linetest_st_botar, Gravity.CENTER);
        hktBtn = aq.normTxtBtn(R.id.linetest_st_btn, getResString(R.string.LTTF_TEST), btnWidth);
        aq.id(R.id.linetest_st_btn).clicked(this, "onClick");

        //Reboot modem button
        String title = callback_main.getLobString().equalsIgnoreCase(SubnRec.LOB_TV) ? getResString(R.string.MYHKT_LT_BTN_REBOOT_MODEM_STB) : getResString(R.string.MYHKT_LT_BTN_REBOOT_MODEM);
        mRebootModemBtn = aq.normTxtBtn(R.id.linetest_reboot_mdm_btn, title, btnWidth);
        aq.id(R.id.linetest_reboot_mdm_btn).clicked(this, "onClick");
    }

    public void setLTSUI() {
        titleCell = new IconTextCell(R.drawable.test_result_icon, getResString(R.string.myhkt_linetest_result));
        titleCell.setTopMargin(0);
        cellList.add(titleCell);

        timeCell = new BigTextCell("", "dd/MM/yyyy HH:mm");
        timeCell.setTopMargin(0);
        timeCell.setBotMargin(0);
        timeCell.setContentSizeDelta(0);
        timeCell.setContentColorId(R.color.hkt_textcolor);
        timeCell.setArrowShown(true);

        enquiryCell = new WebViewCell(isZh ? "file:///android_asset/lntt_reslut_lts_enquiry_en.html" : "file:///android_asset/lntt_reslut_lts_enquiry_zh.html");
        cellList.add(enquiryCell);

        singleBtnCell = new SingleBtnCell(getResString(R.string.myhkt_linetest_hotline), deviceWidth / 2 - extralinespace, null);
        singleBtnCell.setDraw(R.drawable.phone_small);
        singleBtnCell.setBgColorId(R.color.cell_bg_grey);
        singleBtnCell.setTopPadding(0);
        cellList.add(singleBtnCell);

        cellViewAdapter.setView(frame, cellList);
    }

    public void onImageClick(View v) {
        Intent intent = new Intent(me.getActivity(), ImageViewerActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        Bundle bundle = new Bundle();
        bundle.putString("SVRNUM", callback_main.getSubnRec().srvNum);
        bundle.putInt("LOBICON", callback_main.getLob());
        bundle.putInt("LOBTYPE ", callback_main.getLtsType());
        bundle.putString("IMAGEURL", modemImageLargeURL);

        intent.putExtras(bundle);
        startActivity(intent);
        me.getActivity().overridePendingTransition(R.anim.right_slide_in, R.anim.left_slide_out);
    }

    public void onClick(View v) {
        Intent intent;
        Bundle bundle;
        if (debug) Log.i(TAG, "Click");
        switch (v.getId()) {
            case R.id.linetest_st_btn:
                onTestButtonClicked();
                break;
            case R.id.linetest_reboot_mdm_btn:
                onRebootButtonClicked();
                break;
        }

    }

    private void onTestButtonClicked() {
        Intent intent;
        if (lnttCra != null && apptCra != null) {
            String srStatus = lnttCra.getOLtrsRec().srStatus;
            if (apptCra.getReply().getCode().equals(RC.USRB_PENDSR_FND)) {
                if (debug) Log.i(TAG, "Appointment detail");
                // SR found , goto Appt List
                intent = new Intent(getActivity().getApplicationContext(), MyApptActivity.class);
                startActivity(intent);
                getActivity().overridePendingTransition(R.anim.right_slide_in, R.anim.left_slide_out);
            } else if (apptCra.getReply().getCode().equals(RC.SUCC)) {
                if (debug) Log.i(TAG, "Reset Line Test");
                // Service NOT Good && No SR found, then Reset and ReTest / goto Report Fault
                if (LtrsRec.ST_RETEST.equalsIgnoreCase(srStatus)) {
                    if (debug) Log.i(TAG, "Reset Line Test");
                    doLineTestRequest();                                        // Reset and ReTest
                } else if (LtrsRec.SR_STS_VCE.equalsIgnoreCase(srStatus) || LtrsRec.SR_STS_BB.equalsIgnoreCase(srStatus)) {
                    // recall doEnquireSR4NoPend to get oPendSrvReq Object for GetTimeSlot and Create SR
                    if (debug) Log.i(TAG, "Create Appointment");
                    doEnquireSR4NoPend();
                }
            }
        }
    }

    private void onRebootButtonClicked() {
        if (debug) Log.i(TAG, "Reboot button clicked");

        //if LOB is LTS Eye,
        if (callback_main.getLob() == R.string.CONST_LOB_LTS) {
            if (callback_main.getLtsType() == R.string.CONST_LTS_EYE) {
                FAWrapper.getInstance().sendFAEvents(getActivity(),
                        R.string.CONST_GA_CATEGORY_USERLV,
                        R.string.CONST_GA_ACTION_BTN, R.string.CONST_GA_LABEL_REBOOT,
                        " (" + callback_main.getSubnRec().lob + ")",false);

                LnttCra lnttCra = new LnttCra();
                lnttCra.setISubnRec(callback_main.getSubnRec());
                if (lnttAgent != null) {
                    //required if it is called via line test page
                    lnttCra.setILtrsRec(lnttAgent.getLnttCra().getOLtrsRec());
                }

                APIsManager.doRebootModem(this, lnttCra);
            }
        } else {
            // go to modem tab and do reboot automatically
            // TODO:pass the lnttAgent to rebootFragment
            callback_main.setLnttAgent(lnttAgent);
            callback_main.onShowModemTab(true);
        }
    }

    private final void doEnquireSR4NoPend() {
        if (callback_main.getSubnRec() != null) {
            // doEnquireSR4NoPend
            if (callback_main.getSubnRec() != null) {
                apptCra = new ApptCra();
                apptCra.setILoginId(ClnEnv.getLoginId());
                apptCra.setICustRec(ClnEnv.getQualSvee().getCustRec().copyMe());
                apptCra.setISubnRec(callback_main.getSubnRec());
                if (lnttCra != null) {
                    apptCra.setIEnqSRTy(lnttCra.getOLtrsRec().srStatus);
                }
            }
            APIsManager.doEnquireSR4NoPend(me, apptCra);
            // Pass data back Activity
            callback_main.setApptCra(apptCra);
        }
    }

    private final void doLineTestRequest() {
        if (debug) Log.i(TAG, "Do LTT Ag");
        // Avoid User start Line Test more times
        // If the LineTest started before, just show the InProgressDialog Only.
        LnttAgent lnttAgent = Utils.getPrefLnttAgent(me.getActivity());

        if ("".equalsIgnoreCase(lnttAgent.getLnttSrvNum())) {
            if (debug) Log.i(TAG, "Do LTT Ag1");
            // Prepare LineTest related variables
            lnttAgent = new LnttAgent();
            lnttAgent.setSessTok(ClnEnv.getSessTok());
            lnttAgent.prepareLnttCra(ClnEnv.getAssocSubnRecAry(ClnEnv.getQualSvee().getSubnRecAry()), callback_main.getSubnRec().copyMe(), ClnEnv.getSessionLoginID());
            // Only Reset and Retest, lnttCra is not null and passed Object
            if (lnttCra != null) {
                lnttAgent.getLnttCra().setOLtrsRec(lnttCra.getOLtrsRec().copyMe());
            }
            // Also Store SR Result
            lnttAgent.setApptCra(apptCra);

            Bundle lnttBundle = new Bundle();
            lnttBundle.putSerializable("LNTTAGENT", lnttAgent);
            // Directly start a Service
            isLnttStart = true;
            Intent intent = new Intent(me.getActivity(), LineTestIntentService.class);
            intent.putExtras(lnttBundle);
            getActivity().startService(intent);
        }

        // Redirect to ServiceList and show InProgess Dialog
        Intent dialogIntent = new Intent(me.getActivity(), GlobalDialog.class);
        if (lnttCra == null) {
            dialogIntent.putExtra("DIALOGTYPE", R.string.CONST_DIALOG_LNTT_ONSTART);
        } else {
            dialogIntent.putExtra("DIALOGTYPE", R.string.CONST_DIALOG_LNTT_ONRESET);
        }
        dialogIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivity(dialogIntent);

    }

    private final void doGetAvailableTimeSlot() {
        // Assume lnttAgent and apptCra defined in LineTest Result Page
        if (lnttAgent != null && apptCra != null) {
            // doGetAvailableTimeSlot
            ApptCra rApptCra = new ApptCra();
            rApptCra.setILoginId(ClnEnv.getSessionLoginID());
            rApptCra.setICustRec(ClnEnv.getQualSvee().getCustRec().copyMe());
            rApptCra.setISRApptInfo(apptCra.getOPendSrvReq().getApptInfo());
            APIsManager.doGetAvailableTimeSlot(me, rApptCra);
        }
    }

    private final void doneEnquireSR() {
        if (apptCra != null) {
            if (apptCra.getReply().getCode().equals(RC.USRB_PENDSR_FND)) {
                me.displayDialog(getResString(R.string.MYHKT_LT_ERR_SR_FOUND));
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (SubnRec.LOB_LTS.equalsIgnoreCase(callback_main.getSubnRec().lob)) {
            FAWrapper.getInstance().sendFAScreen(getActivity(),
                    R.string.CONST_SCRN_LTSLINETEST3, false);
        } else if (SubnRec.LOB_PCD.equalsIgnoreCase(callback_main.getSubnRec().lob)) {
            FAWrapper.getInstance().sendFAScreen(getActivity(),
                    R.string.CONST_SCRN_PCDLINETEST3, false);
        }
    }

    protected final void refreshData() {
        super.refreshData();
        if (callback_main != null) {
             moduleId = getActivity().getResources().getString(R.string.MODULE_LGIF);
            // Screen Tracker
            ClnEnv.updateUILocale(getActivity().getBaseContext(),ClnEnv.getAppLocale(getActivity().getBaseContext()));
            if (SubnRec.LOB_LTS.equalsIgnoreCase(callback_main.getSubnRec().lob)) {
                FAWrapper.getInstance().sendFAScreen(getActivity(),
                        R.string.CONST_SCRN_LTSLINETEST3, false);
                moduleId = getActivity().getResources().getString(R.string.MODULE_LTS_LTT);
            } else if (SubnRec.LOB_PCD.equalsIgnoreCase(callback_main.getSubnRec().lob)) {
                FAWrapper.getInstance().sendFAScreen(getActivity(),
                        R.string.CONST_SCRN_PCDLINETEST3, false);
                moduleId = getActivity().getResources().getString(R.string.MODULE_PCD_LTT);
            } else if (SubnRec.LOB_TV.equalsIgnoreCase(callback_main.getSubnRec().lob)) {
                FAWrapper.getInstance().sendFAScreen(getActivity(),
                        R.string.CONST_SCRN_TVLINETEST3, false);
                moduleId = getActivity().getResources().getString(R.string.MODULE_TV_LTT);
            }

            if (callback_main.getActiveSubview() == R.string.CONST_SELECTEDVIEW_LINETEST) {
                lnttAgent = callback_main.getLnttAgent();
                lnttCra = lnttAgent.getLnttCra();
                apptCra = lnttAgent.getApptCra();

                String lnttImsSrvNum = lnttAgent.getLnttImsSrvNum();
                String lnttTvSrvNum = lnttAgent.getLnttTvSrvNum();
                String lnttLtsSrvNum = lnttAgent.getLnttLtsSrvNum();
                String endTimestamp = Utils.toDateString(lnttCra.getServerTS(), Utils.getString(me.getActivity(), R.string.input_datetime_format), "dd/MM/yyyy HH:mm:ss");

                // Clear All Data if line Test Result read once
                Utils.clearLnttService(me.getActivity());

                String rStr = "";
                if (lnttCra != null) {
                    Boolean isFixedLine = false;
                    //check if LTS is fixed line
                    if (lnttCra.getOLtrsRec().txtg4fxln.trim().length() > 0 && !(lnttCra.getOLtrsRec().txtg4bb.trim().length() > 0) && callback_main.getLob() == R.string.CONST_LOB_LTS)
                        isFixedLine = true;
                    //Show Modem Image if available
                    if (!isFixedLine || callback_main.getLob() == R.string.CONST_LOB_PCD || callback_main.getLob() == R.string.CONST_LOB_TV) {
                        //if modem model number exist, show modem img and text, otherwise show default url image only
                        if (lnttCra.getOLtrsRec().mdmMdl.trim().length() > 0) {
                            if (lnttCra.getOLtrsRec().mdmimgL.trim().length() > 0)
                                modemImageLargeURL = lnttCra.getOLtrsRec().mdmimgL.trim();
                            //show modem message if available
                            if (lnttCra.getOMdmEnMsg() != null && lnttCra.getOMdmZhMsg() != null) {
                                if (lnttCra.getOMdmEnMsg().trim().length() > 0 && lnttCra.getOMdmZhMsg().trim().length() > 0 && !isFixedLine) {
                                    String msg = isZh ? lnttCra.getOMdmZhMsg() : lnttCra.getOMdmEnMsg();
                                    msg = msg.trim().replace("~", "\n");
                                    mdmMsgCell = new BigTextCell("", msg);
                                } else {
                                    mdmMsgCell = null;
                                }
                            }
                        } else {
                            modemImageLargeURL = ClnEnv.getPref(getActivity(), getString(R.string.CONST_PREF_FTP), APIsManager.PRD_FTP) + "/mba/images/default_large.png";
                        }

                        if (modemImageLargeURL.trim().length() > 0) {
                            String[] click = {"onImageClick"};
                            imageViewCell = new ImageViewCell(0, modemImageLargeURL, click);
                            imageViewCell.setOnClickListener(new OnClickListener() {

                                @Override
                                public void onClick(View v) {
                                    Intent intent = new Intent(me.getActivity(), ImageViewerActivity.class);
                                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                    Bundle bundle = new Bundle();
                                    bundle.putString("SVRNUM", callback_main.getSubnRec().srvNum);
                                    bundle.putString("ACCNUM", callback_main.getSubnRec().acctNum);
                                    bundle.putBoolean("ISZOMBIE", callback_main.getSubnRec().isZombie());
                                    bundle.putInt("LOBICON", callback_main.getLob());
                                    bundle.putString("IMAGEURL", modemImageLargeURL);
                                    bundle.putString("LOBMODULEID", moduleId);
                                    bundle.putInt("LOBTYPE", callback_main.getLtsType());
                                    intent.putExtras(bundle);
                                    startActivity(intent);
                                    me.getActivity().overridePendingTransition(R.anim.right_slide_in, R.anim.left_slide_out);
                                }
                            });
                        }
                    }

                    // show the LineTest return timestamp
                    timeCell.setContent(endTimestamp);

                    if (lnttCra.isIBBNwInd()) {
                        int resultImgId;
                        String connectState = "";
                        // Require Graphical Result View
                        // Display Modem Result
                        if (lnttCra.getOLtrsRec().modem.equals(LtrsRec.RLT_GOOD)) {
                            // Show result image : IMG_MANG
                            resultImgId = R.drawable.icon_man_green;
                            connectState = getResString(R.string.LNTT_MDM_STS_GOD);
                        } else if (lnttCra.getOLtrsRec().modem.equals(LtrsRec.RLT_OUT_SYNC)) {
                            // Show result image : IMG_MANR;
                            resultImgId = R.drawable.icon_man_red;
                            connectState = getResString(R.string.LNTT_MDM_STS_OSY);
                        } else {
                            // Show result image : IMG_MANY;
                            resultImgId = R.drawable.icon_man_yellow;
                            connectState = getResString(R.string.LNTT_MDM_STS_NGD);
                        }

                        // Display Result for associated subscription (Connected Services)
                        if (lnttCra.getOLtrsRec().subnPcdInd.equals(Tool.FLG_YES)) {
                            // Display for PCD
                            //lob_pcd_plain
                            if(lnttCra.getOLtrsRec().bbNtwk.equals(LtrsRec.RLT_NO_GOOD)) {
                                pcdLiveTestCell = null;
                            } else {
                                pcdLiveTestCell = new LineTest(lnttImsSrvNum, connectState, R.drawable.lob_pcd_plain, resultImgId);
                            }

                        } else {
                            pcdLiveTestCell = null;
                        }

                        if (lnttCra.getOLtrsRec().subnTvInd.equals(Tool.FLG_YES)) {
                            // Display for TV
                            //lob_tv_plain
                            if(lnttCra.getOLtrsRec().bbNtwk.equals(LtrsRec.RLT_NO_GOOD)) {
                                tvLiveTestCell = null;
                            } else {
                                tvLiveTestCell = new LineTest(lnttTvSrvNum, connectState, R.drawable.lob_tv_plain, resultImgId);
                            }

                        } else {
                            tvLiveTestCell = null;
                        }

                        if (lnttCra.getOLtrsRec().subnEyeInd.equals(Tool.FLG_YES)) {
                            // Display for Eye
                            //lob_eye_plain
                            if(lnttCra.getOLtrsRec().bbNtwk.equals(LtrsRec.RLT_NO_GOOD)) {
                                eyeLiveTestCell = null;
                            } else {
                                eyeLiveTestCell = new LineTest(lnttLtsSrvNum, connectState, R.drawable.lob_lts_plain1, resultImgId);
                            }

                        } else {
                            eyeLiveTestCell = null;
                        }

                        //show bb message if available
                        if (lnttCra.getOLtrsRec().txtg4bb.trim().length() > 0 && !isFixedLine) {
                            bbMsgCell = new BigTextCell("", InterpretRCManager.interpretRC_LttLtsMdu(this.getActivity(), lnttCra.getOLtrsRec().txtg4bb.trim()));
                        } else {
                            bbMsgCell = null;
                        }

                        //show fixedline message if available
                        if (lnttCra.getOLtrsRec().txtg4fxln.trim().length() > 0 && callback_main.getLob() == R.string.CONST_LOB_LTS) {
                            fixlineMsgCell = new BigTextCell("", InterpretRCManager.interpretRC_LttLtsMdu(this.getActivity(), lnttCra.getOLtrsRec().txtg4fxln.trim()));
                        } else {
                            fixlineMsgCell = null;
                        }

                    } else {
                        // Text Result Only â€“ should use LTS result screen style
                        //TODO
                        //Image inv
                        bbMsgCell = null;
                        fixlineMsgCell = null;
                        resultCell = null;
                        pcdLiveTestCell = null;
                        tvLiveTestCell = null;
                        eyeLiveTestCell = null;
                        fixlineTitleCell = new BigTextCell("", getResString(R.string.MYHKT_LT_LBL_FIXLN_HEADER));
                        fixlineMsgCell = new BigTextCell("", InterpretRCManager.interpretRC_LttLtsMdu(this.getActivity(), lnttCra.getOLtrsRec().txtg4fxln.trim()));

                        //show fixedline message if available
                        if (lnttCra.getOLtrsRec().txtg4fxln.trim().length() > 0 && callback_main.getLob() == R.string.CONST_LOB_LTS) {
                            fixlineMsgCell = new BigTextCell("", InterpretRCManager.interpretRC_LttLtsMdu(this.getActivity(), lnttCra.getOLtrsRec().txtg4fxln.trim()));
                        }
                    }

                    if (apptCra != null && apptCra.getReply().getCode().equals(RC.USRB_PENDSR_FND)) {
                        // SR found , Show additional message
                        if (lnttCra.getOLtrsRec().modem.equals(LtrsRec.RLT_GOOD)) {
                            rStr = rStr + getResString(R.string.LTTM_PSR);
                        } else {
                            rStr = rStr + getResString(R.string.LTTM_PSR_NG);
                        }
                        hktBtn.setText(getResString(R.string.MYHKT_LT_BTN_DETAIL));
                    } else if (lnttCra.getReply().isSucc()) {
                        // SR Not found
                        if (LtrsRec.RLT_GOOD.equals(lnttCra.getOLtrsRec().modem)) {
                            //Reboot modem instead
                            //if LOB is TV, change the Reboot modem title to Reboot modem and STB
                            hktBtn.setVisibility(View.GONE);
                            mRebootModemBtn.setVisibility(View.VISIBLE);

                            if (callback_main.getLob() == R.string.CONST_LOB_TV) {
                                mRebootModemBtn.setText(getResString(R.string.MYHKT_LT_BTN_REBOOT_MODEM_STB));
                            } else if (callback_main.getLob() == R.string.CONST_LOB_PCD) {
                                mRebootModemBtn.setText(getResString(R.string.MYHKT_LT_BTN_REBOOT_MODEM));
                            } else if (callback_main.getLob() == R.string.CONST_LOB_LTS) {
                                if (callback_main.getLtsType() == R.string.CONST_LTS_EYE) {
                                    mRebootModemBtn.setText(getResString(R.string.MYHKT_LT_BTN_REBOOT_MODEM));
                                } else {
                                    mRebootModemBtn.setVisibility(View.GONE); // LTS but not eye
                                }
                            }
                        } else {
                            // (lnttRec Not Good)
                            // [Assume: Good status never shows "T" / "V" / "B"]
                            String srStatus = lnttCra.getOLtrsRec().srStatus;
                            if (apptCra.getReply().getCode().equals(RC.SUCC) &&
                                    (LtrsRec.SR_STS_RETEST.equalsIgnoreCase(srStatus) || LtrsRec.SR_STS_VCE.equalsIgnoreCase(srStatus) || LtrsRec.SR_STS_BB.equalsIgnoreCase(srStatus))) {
                                if (LtrsRec.SR_STS_VCE.equalsIgnoreCase(srStatus) || LtrsRec.SR_STS_BB.equalsIgnoreCase(srStatus)) {
                                    rStr = rStr + getResString(R.string.LTTM_AUTOSR);
                                    hktBtn.setText(getResString(R.string.MYHKT_LT_BTN_REPORT)); //Appointment

                                } else {
                                    hktBtn.setVisibility(View.VISIBLE);
                                    mRebootModemBtn.setVisibility(View.GONE);
                                    hktBtn.setText(getResString(R.string.MYHKT_LT_BTN_RESET));
                                }
                            } else {
                                hktBtn.setVisibility(View.GONE);
                                mRebootModemBtn.setVisibility(View.GONE);
                            }
                        }
                    }

                    //show SR result if available
                    if (rStr.trim().length() > 0) {
                        resultCell = new BigTextCell("", rStr);
                    } else {
                        resultCell = null;
                    }

                    callback_main.setApptCra(apptCra);
                    callback_main.setLnttCra(lnttCra);
                }

                cellList = new ArrayList<Cell>();
                titleCell.setLeftPadding(basePadding);
                titleCell.setRightPadding(basePadding);
                cellList.add(titleCell);
                if (timeCell != null) {
                    timeCell.setLeftMargin(basePadding);
                    timeCell.setRightMargin(basePadding);
                    cellList.add(timeCell);
                }
                if (pcdLiveTestCell != null) {
                    pcdLiveTestCell.setLeftPadding(basePadding);
                    pcdLiveTestCell.setRightPadding(basePadding);
                    cellList.add(pcdLiveTestCell);
                }
                if (tvLiveTestCell != null) {
                    tvLiveTestCell.setLeftPadding(basePadding);
                    tvLiveTestCell.setRightPadding(basePadding);
                    cellList.add(tvLiveTestCell);
                }
                if (eyeLiveTestCell != null) {
                    eyeLiveTestCell.setLeftPadding(basePadding);
                    eyeLiveTestCell.setRightPadding(basePadding);
                    cellList.add(eyeLiveTestCell);
                }
                if (imageViewCell != null) cellList.add(imageViewCell);
                if (bbMsgCell != null) {
                    bbMsgCell.setLeftMargin(basePadding);
                    bbMsgCell.setRightMargin(basePadding);
                    bbMsgCell.setContentSizeDelta(-3);
                    cellList.add(bbMsgCell);
                }
                if (mdmMsgCell != null) {
                    mdmMsgCell.setLeftMargin(basePadding);
                    mdmMsgCell.setRightMargin(basePadding);
                    mdmMsgCell.setContentSizeDelta(-3);
                    cellList.add(mdmMsgCell);
                }
                if (fixlineTitleCell != null) {
                    fixlineTitleCell.setMargin(basePadding);
                    fixlineTitleCell.setContentSizeDelta(-3);
                    cellList.add(fixlineTitleCell);
                }
                if (fixlineMsgCell != null) {
                    fixlineMsgCell.setMargin(basePadding);
                    fixlineMsgCell.setContentSizeDelta(-3);
                    cellList.add(fixlineMsgCell);
                }
                if (resultCell != null) {
                    resultCell.setLeftMargin(basePadding);
                    resultCell.setRightMargin(basePadding);
                    resultCell.setContentSizeDelta(-3);
                    cellList.add(resultCell);
                }
                cellViewAdapter.setView(frame, cellList);
            }
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        Utils.ISMYLIENTEST = true;
    }

    @Override
    public void onStop() {
        if (debug) Log.i(TAG, "on stop");
        super.onStop();
    }

    @Override
    public void onPause() {
        if (debug) Log.i(TAG, "on pause");
        super.onPause();
    }

    @Override
    public void onDestroy() {
        if (debug) Log.i(TAG, "on Destroy");
        super.onDestroy();
    }

    @Override
    public void onSuccess(APIsResponse response) {
        if (response != null) {
            ApptCra mApptCra;
            if (response.getActionTy().equals(APIsManager.SR_ENQ_NP)) {
                mApptCra = (ApptCra) response.getCra();
                if (mApptCra != null) {
                    apptCra.setReply(mApptCra.getReply());
                    apptCra.setServerTS(mApptCra.getServerTS());
                    me.apptCra.setOPendSrvReq(mApptCra.getOPendSrvReq());
                    // Pass data back Activity
                    me.callback_main.setApptCra(me.apptCra.copyMe());
                    // call doGetTimeSlot
                    me.doGetAvailableTimeSlot();
                }
            } else if (response.getActionTy().equals(APIsManager.SR_AVA_TS)) {
                mApptCra = (ApptCra) response.getCra();
                if (mApptCra != null) {
                    apptCra.setOSRApptInfo(mApptCra.getOSRApptInfo());
                    // Pass data back Activity
                    me.callback_main.setApptCra(me.apptCra.copyMe());
                    //TODO go to SR create
                    callback_lntt = (OnLnttListener) getParentFragment();
                    callback_lntt.setActiveChildview(R.string.CONST_SELECTEDVIEW_SRCREATION);
                    callback_lntt.displayChildview(OnLnttListener.ENTER);
                }
            } else if (response.getActionTy().equals(APIsManager.RBMD)) {
                //If LTS Fixed line, show the 2 minute screen
                callback_lntt.onRebootModemShowLoadingPage();
            }
        }
    }

    @Override
    public void onFail(APIsResponse response) {
        if (response != null) {
            // General Error Message
            if (!"".equals(response.getMessage()) && response.getMessage() != null) {
                displayDialog(response.getMessage());
            } else if (RC.TCSESS_MM.equalsIgnoreCase(response.getReply().getCode())) {
                BaseActivity.ivSessDialog();
            } else {
                BaseCraEx cra;
                if (response.getActionTy().equals(APIsManager.SR_ENQ_NP)) {
                    cra = (BaseCraEx) response.getCra();
                    if (cra != null) {
                        apptCra.setReply(cra.getReply());
                        apptCra.setServerTS(cra.getServerTS());
                        // Pass Data Back Activity
                        callback_main.setApptCra(apptCra.copyMe());
                        if (cra.getReply().getCode().equals(RC.GWTGUD_MM) || cra.getReply().getCode().equals(RC.ALT)) {
                            // RC_IVSESS
                            // RC_ALT
                        } else if (cra.getReply().getCode().equals(RC.USRB_PENDSR_FND)) {
                            // RC_USRB_PENDSR_FND
                            // Have Pending SR
                            me.doneEnquireSR();
                        } else {
                            me.displayDialog(ClnEnv.getRPCErrMsg(me.getActivity().getApplicationContext(), cra.getReply().getCode()));
                        }
                    }
                } else {
                    cra = (BaseCraEx) response.getCra();
                    if (cra != null) {
                        me.displayDialog(ClnEnv.getRPCErrMsg(me.getActivity().getApplicationContext(), cra.getReply().getCode()));
                    }
                }
            }

        }
    }
}