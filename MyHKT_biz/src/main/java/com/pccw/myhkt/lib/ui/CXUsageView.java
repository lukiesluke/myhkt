package com.pccw.myhkt.lib.ui;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.ViewGroup.LayoutParams;

import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;


import com.androidquery.AQuery;
import com.pccw.biz.myhkt.R;
import com.pccw.myhkt.Utils;

public class CXUsageView extends Button {
	public final static int TYPE_BLUE = 0;
	public final static int TYPE_WHITE = 1;
	private int type = TYPE_BLUE;

	public CXUsageView(Context context) {
		super(context);
		initViews(context, "");		
	}

	public CXUsageView(Context context, AttributeSet attrs) {
		super(context, attrs);
		initViews(context, attrs);
	}

	public CXUsageView(Context context, AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);
		initViews(context, attrs);
	}

	private void initViews(final Context context, AttributeSet attrs) {
		TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.HKTButton, 0, 0);
		try {
			int textSize =	a.getInt(R.styleable.HKTButton_android_textSize, (int)getResources().getDimension(R.dimen.bodytextsize));
			String text = a.getString(R.styleable.HKTButton_android_text);
			int height = (int) a.getDimension(R.styleable.HKTButton_android_layout_height, (int)context.getResources().getDimension(R.dimen.buttonblue_height));
			int width = (int) a.getDimension(R.styleable.HKTButton_android_layout_width, (int)context.getResources().getDimension(R.dimen.buttonblue_width));
			initViews(context, text, textSize, height, width);
		} finally {
			a.recycle();
		}
	}


	public void initViews(final Context context, String text) {	
		initViews(context, text, getResources().getDimension(R.dimen.bodytextsize));
	}

	public void initViews(final Context context, int btnType , String text) {
		type = btnType;
		initViews(context, text, getResources().getDimension(R.dimen.bodytextsize));
	}

	public void initViews(final Context context, String text, float textSize) {	
		initViews(context, text, textSize, (int)getResources().getDimension(R.dimen.buttonblue_height) ,(int)getResources().getDimension(R.dimen.buttonblue_width));
	}

	public void initViews(final Context context, int btnType , String text , float textSize) {
		type = btnType;
		initViews(context, text, textSize, (int)getResources().getDimension(R.dimen.buttonblue_height) ,(int)getResources().getDimension(R.dimen.buttonblue_width));
	}

	public void initViews(final Context context, int btnType , String text, int width) {	
		type = btnType;
		initViews(context, text, getResources().getDimension(R.dimen.bodytextsize), (int)getResources().getDimension(R.dimen.buttonblue_height) ,width) ;
	}

	public void initViews(final Context context, String text, int width) {	
		initViews(context, text, getResources().getDimension(R.dimen.bodytextsize), (int)getResources().getDimension(R.dimen.buttonblue_height) ,width) ;
	}

	public void initViews(final Context context, int btnType, String text, float textSize, int height, int width) {	
		type = btnType;
		initViews(context, text, textSize, height, width) ;

	}

	public void initViews(final Context context, String text, float textSize, int height, int width) {	
//		Log.i("Btn", height+"");
//		Log.i("Btn", 	context.getResources().getDisplayMetrics().density+"");
//	
//		Log.i("Btn", (int)this.getResources().getDimension(R.dimen.buttonblue_height)+"");
//		Log.i("Btn", (int)context.getResources().getDimension(R.dimen.buttonblue_height)+"");
		if (this.getLayoutParams()!=null) {
			setHeight(height);
			setWidth(width);
		} else {
			LayoutParams params = new LayoutParams(LayoutParams.MATCH_PARENT,height);
			setLayoutParams(params);
		}
				
		setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize);
		setGravity(Gravity.CENTER);
		setPadding(0, 0, 0, 0);
		setText(text);

		//TODO theme
		switch (type) {
		case TYPE_BLUE:
			setBackgroundResource(R.drawable.hkt_btn_bg_blue_selector);
			setTextColor(context.getResources().getColorStateList(R.color.hkt_btn_blue_txtcolor_selector));
			break;
		case TYPE_WHITE:
			setBackgroundResource(R.drawable.hkt_btn_bg_white_blue_selector);
			setTextColor(context.getResources().getColorStateList(R.color.hkt_btn_white_blue_txtcolor_selector));
			break;
		default:
			setBackgroundResource(R.drawable.hkt_btn_bg_blue_selector);
			setTextColor(context.getResources().getColorStateList(R.color.hkt_btn_blue_txtcolor_selector));
			break;
		}

	}

	public void setLeftMargin(int leftMargin) {


	}
}
