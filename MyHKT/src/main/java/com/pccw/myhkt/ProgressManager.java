package com.pccw.myhkt;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.DialogInterface.OnDismissListener;
import android.util.Log;

public class ProgressManager {
	
	private static boolean				debug					= false;	// toggle debug logs
	private static ProgressManager 		progressManager 		= null;

	private ProgressDialog 				progressDialog 			= null;
	private int 						asyncCount				= 0;
	private int							totalCount				= 0;

	private ProgressManager(Context context) {
		debug = context.getResources().getBoolean(R.bool.DEBUG);
	}
	
	public static synchronized ProgressManager getInstance(Context context) {
		if (progressManager == null) {
			progressManager = new ProgressManager(context);
		}
		return progressManager;
	}
	
	public final void pushAsyncCount() {
		if (asyncCount <= 0) {
			asyncCount = 0;
			totalCount = 0;
		}
		asyncCount++;
		totalCount = asyncCount;
	}
	
	public final void popAsyncCount() {
		if (debug) Log.i(getClass().getName(),"popAsyncCount");
		asyncCount--;
	}
	
	public final void clearAsyncCount() {
		asyncCount = 0;
		totalCount = 0;
	}
	
	public final int getAsyncCount() {
		return asyncCount;
	}
	
	public final void checkNull(){
		if (progressDialog != null) {
			if (debug) android.util.Log.i("ProgressDialog",  java.lang.System.identityHashCode(progressDialog) + "not null");
		} else {
			if (debug) android.util.Log.i("ProgressDialog",   java.lang.System.identityHashCode(progressDialog)+ "null");
		}
	}
	
	public final void showProgressDialog(Context cxt) {
		try {
			if (debug) Log.i(getClass().getName(), "ASYNCCOUNT" + asyncCount);
			pushAsyncCount();
			// Check ProgressDialog Whether is showing
			if (progressDialog != null) {
				if (progressDialog.isShowing()) {
					if (debug) android.util.Log.i("ProgressDialog", "showing");
					return;
				} else {
					if (debug) android.util.Log.i("ProgressDialog", "dis");
					dismissProgressDialog();
					pushAsyncCount();
				}
			}
			
//			progressDialog = null;
			progressDialog = ProgressDialog.show(cxt, "", Utils.getString(cxt, R.string.please_wait), true, false);
//			progressDialog = new ProgressDialog(cxt);
//			progressDialog.setTitle(Utils.getString(cxt, R.string.please_wait));
//			progressDialog.setCancelable(false);
//			progressDialog.setIndeterminate(true);
			progressDialog.setProgress(0);
			progressDialog.setCanceledOnTouchOutside(false);
			progressDialog.show();
			progressDialog.setOnDismissListener(new OnDismissListener() {
				@Override
				public void onDismiss(DialogInterface dialog) {
//					if (dialog != null) {
//						dialog.dismiss();
//					}
					progressDialog = null;
				}
			});
			// Cancel ProgressDialog and also Stop all Display data AsyncTasks
			progressDialog.setOnCancelListener(new OnCancelListener() {
				@Override
				public void onCancel(DialogInterface dialog) {
//					if (dialog != null) {
//						dialog.dismiss();
//					}
					progressDialog = null;
				}
			});
		} catch (Exception e) {
			if (debug) e.printStackTrace();
			dismissProgressDialog(true);
		}
	}
	
	public final void dismissProgressDialog() {
		popAsyncCount();
		if (getAsyncCount() <= 0) {
			if (progressDialog != null) {
				progressDialog.dismiss();
				progressDialog = null;
			}
			clearAsyncCount();
		} else {
			int percent = asyncCount * 100 / totalCount  ;
			progressDialog.setProgress(percent);
		}
	}
	
	// Force to dismiss Dialog and clear AsyncCount
	public final void dismissProgressDialog(boolean forceToDismiss) {
		if (forceToDismiss) {
			if (progressDialog != null) {
				progressDialog.dismiss();
				progressDialog = null;
			}
			clearAsyncCount();
		} else {
			dismissProgressDialog();
		}
	}
}