/*
    Base User template in DANGO
    
    This is implemented as a Class instead of Abstract
    to cope with JSON limitation.
    
    Keywords
    --------
    $URL: svn://10.87.120.207/dango/rel/v16.0/src/com/pccw/dango/shared/entity/BaseUserEx.java $
    $Rev: 844 $
    $Date: 2016-10-17 11:54:01 +0800 (¶g¤@, 17 ¤Q¤ë 2016) $
    $Author: upd $
*/

package com.pccw.dango.shared.entity;

import com.pccw.wheat.shared.entity.BaseUser;
import com.pccw.wheat.shared.tool.MiniRtException;

import java.io.Serializable;


public class BaseUserEx implements BaseUser, Serializable
{
    private static final long serialVersionUID = -5566845720063209827L;

    public static final String      TY_NPSN     = "NPSN";   /* Not a Person     */
    public static final String      TY_SVEE     = "SVEE";   /* Servee           */ 
    public static final String      TY_STFF     = "STFF";   /* HKT/PCCW Staff   */
    public static final String      TY_OSUR     = "OSUR";   /* OS User          */

    
    public BaseUserEx()
    {
    }
    
    
    public boolean isNotAPerson()
    {
        return (getType().equals(TY_NPSN));
    }
    
    
    public boolean isSvee()
    {
        return (getType().equals(TY_SVEE));
    }
    
    
    public boolean isStaff()
    {
        return (getType().equals(TY_STFF));
    }
    
    
    public boolean isOsUser()
    {
        return (getType().equals(TY_OSUR));
    }


    public String getId()
    {
        throw new MiniRtException("Non-Implemented Function!");
    }


    public String getType()
    {
        throw new MiniRtException("Non-Implemented Function!");
    }


    public String getRole()
    {
        throw new MiniRtException("Non-Implemented Function!");
    }


    public String[] getRghtAry()
    {
        throw new MiniRtException("Non-Implemented Function!");
    }


    public boolean isAuth(String rRght)
    {
        throw new MiniRtException("Non-Implemented Function!");
    }
}