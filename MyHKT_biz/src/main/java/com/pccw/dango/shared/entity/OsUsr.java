/*
    Default User in Dango
    
    Keywords
    --------
    $URL: $
    $Rev: $
    $Date: $
    $Author: $
*/

package com.pccw.dango.shared.entity;

import java.io.Serializable;


public class OsUsr extends BaseUserEx implements Serializable
{
    private static final long serialVersionUID = 3532348210328423763L;
    
    private String                  usrId;
    

    public static void main(String rArg[])
    {
        System.out.println(getVer());
        return;
    }
    
    
    public static String getVer()
    {
        return ("$URL: $, $Rev: $");
    }
    
    
    public OsUsr()
    {
        super();
        usrId = ID_UNKN;
    }
    
    
    public OsUsr(String rUsrId)
    {
        super();
        usrId = rUsrId;
    }
    
    
    public String getId()
    {
        return (usrId);
    }
    
    
    public String getType()
    {
        return (TY_OSUR);
    }
    
    
    public String getRole()
    {
        return ("");
    }
    
    
    public String[] getRghtAry()
    {
        return (new String[0]);
    }
    
    
    public boolean isAuth(String rRght)
    {
        return (false);
    }
}
