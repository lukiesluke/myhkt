/*
    ApptInfo
    
    Keywords
    --------
    $URL: svn://10.87.120.207/dango/rel/v16.0/src/com/pccw/dango/shared/entity/ApptInfo.java $
    $Rev: 844 $
    $Date: 2016-10-17 11:54:01 +0800 (¶g¤@, 17 ¤Q¤ë 2016) $
    $Author: upd $
*/

package com.pccw.dango.shared.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;

public class ApptInfo implements Serializable
{
    private static final long serialVersionUID = 4790290539652839800L;
    
    private String                  refNum;             /* Reference Number                              */
    private String                  prodTy;             /* Product Type                                  */
    private String                  srvTy;              /* Service Type                                  */
    private String                  areaCd;             /* Area Code                                     */
    private String                  distCd;             /* District Code                                 */
    private String                  srvNum;             /* Service Number                                */
    private String                  prodId;             /* Product ID                                    */
    private SRApptTS                apptTS;             /* Appointment Timeslot                          */
    private SRApptTS[]              apptTSAry;          /* Array of Appointment Timeslot                 */

    
    public static void main(String rArg[])
    {
        System.out.println(getVer());
        return;
    }
    
    
    public static String getVer()
    {
        return ("$URL: svn://10.87.120.207/dango/rel/v16.0/src/com/pccw/dango/shared/entity/ApptInfo.java $, $Rev: 844 $");
    }


    public ApptInfo()
    {
        initAndClear();
    }

    
    final void initAndClear()
    {
        init();
        clear();
    }


    protected void init()
    {
    }


    public void clear()
    {
        clearRefNum();
        clearProdTy();
        clearSrvTy();
        clearAreaCd();
        clearDistCd();
        clearSrvNum();
        clearProdId();
        clearApptTS();
        clearApptTSAry();
    }


    public ApptInfo copyFrom(ApptInfo rSrc)
    {
        setRefNum(rSrc.getRefNum());
        setProdTy(rSrc.getProdTy());
        setSrvTy(rSrc.getSrvTy());
        setAreaCd(rSrc.getAreaCd());
        setDistCd(rSrc.getDistCd());
        setSrvNum(rSrc.getSrvNum());
        setProdId(rSrc.getProdId());
        setApptTS(rSrc.getApptTS());
        setApptTSAry(rSrc.getApptTSAry());

        return (this);
    }


    public ApptInfo copyTo(ApptInfo rDes)
    {
        rDes.copyFrom(this);
        return (rDes);
    }
    
    
    public ApptInfo copyMe()
    {
        ApptInfo                    rDes;

        rDes = new ApptInfo();
        rDes.copyFrom(this);
        return (rDes);
    }


    public void clearRefNum()
    {
        refNum = "";
    }


    public void setRefNum(String rArg)
    {
        refNum = rArg;
    }


    public String getRefNum()
    {
        return (refNum);
    }


    public void clearProdTy()
    {
        prodTy = "";
    }


    public void setProdTy(String rArg)
    {
        prodTy = rArg;
    }


    public String getProdTy()
    {
        return (prodTy);
    }


    public void clearSrvTy()
    {
        srvTy = "";
    }


    public void setSrvTy(String rArg)
    {
        srvTy = rArg;
    }


    public String getSrvTy()
    {
        return (srvTy);
    }


    public void clearAreaCd()
    {
        areaCd = "";
    }


    public void setAreaCd(String rArg)
    {
        areaCd = rArg;
    }


    public String getAreaCd()
    {
        return (areaCd);
    }


    public void clearDistCd()
    {
        distCd = "";
    }


    public void setDistCd(String rArg)
    {
        distCd = rArg;
    }


    public String getDistCd()
    {
        return (distCd);
    }


    public void clearSrvNum()
    {
        srvNum = "";
    }


    public void setSrvNum(String rArg)
    {
        srvNum = rArg;
    }


    public String getSrvNum()
    {
        return (srvNum);
    }


    public void clearProdId()
    {
        prodId = "";
    }


    public void setProdId(String rArg)
    {
        prodId = rArg;
    }


    public String getProdId()
    {
        return (prodId);
    }


    public void clearApptTS()
    {
        apptTS = new SRApptTS();
    }


    public void setApptTS(SRApptTS rArg)
    {
        apptTS = rArg;
    }


    public SRApptTS getApptTS()
    {
        return (apptTS);
    }


    public void clearApptTSAry()
    {
        apptTSAry = new SRApptTS[0];
    }


    public void setApptTSAry(SRApptTS[] rArg)
    {
        apptTSAry = rArg;
    }


    public void setApptTSAry(ArrayList<SRApptTS> rApptTimeSlotLst) 
    {
        if (rApptTimeSlotLst.size() > 0) {
            apptTSAry = rApptTimeSlotLst.toArray(new SRApptTS[rApptTimeSlotLst.size()]);
        }
    }

    
    public SRApptTS[] getApptTSAry()
    {
        return (apptTSAry);
    }
    
    
    public void sortApptTimeSlotAry() 
    {
        if (apptTSAry.length <= 0) return;
        
        Arrays.sort(apptTSAry,
                new Comparator<SRApptTS>()
                {
                    public int compare(SRApptTS rA, SRApptTS rB)
                    {
                        int             rx;

                        /*
                            Sort the available appointment timeslot by: 
                            
                            (1) ApptDate ==> [YYYYMMDD]
                            (2) ApptDtTime().substring(7) ==> [HH-HH]
                        */
                        rx = rA.getApptDate().compareTo(rB.getApptDate());
                        if (rx == 0) rx = rA.getApptDT().substring(7).compareTo(rB.getApptDT().substring(7));
                        
                        return (rx);
                    }
                }
            );
    }

}
