package com.pccw.myhkt.fragment;

import java.lang.Thread.UncaughtExceptionHandler;
import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.graphics.Point;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.fragment.app.FragmentTransaction;
import androidx.viewpager.widget.ViewPager;
import androidx.viewpager.widget.ViewPager.OnPageChangeListener;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.pccw.dango.shared.cra.PlanLtsCra;
import com.pccw.myhkt.APIsManager;
import com.pccw.myhkt.APIsResponse;
import com.pccw.myhkt.ClnEnv;
import com.pccw.myhkt.DialogHelper;
import com.pccw.biz.myhkt.R;
import com.pccw.myhkt.Utils;
import com.pccw.myhkt.fragment.IDDRatesFragment.OnIDDRatesListener;
import com.pccw.myhkt.fragment.PlanLTSMainFragment.OnPlanLtsListener;
import com.pccw.myhkt.lib.ui.AAQuery;
import com.pccw.myhkt.pageindicator.CirclePageIndicator;

/************************************************************************
 * File : PlanLTSFragment.java
 * Desc : LTS Plan
 * Name : PlanLTSFragment
 * by 	: Andy Wong
 * Date : 25/1/2015
 * 
 * Change History:
 * Date       Modified By		Description
 * ---------- ----------------	-------------------------------
 * 18/1/2015  Andy Wong 		-First draft
 *************************************************************************/

public class PlanLTSFragment extends BaseServiceFragment implements OnPlanLtsListener,OnIDDRatesListener {
	private PlanLTSFragment me;
	private AAQuery aq;

	private static final String TAG = "PlanLTSFragment";

	private int deviceWidth ;
	private int extralinespace;
	private int activeSubView =	R.string.CONST_SELECTEDVIEW_LTSPLAN_MAIN;

	private PlanLtsCra 			planLtsCra;

	private PlanLTSMainFragment planLTSMainFragment;
	public IDDRatesFragment		iddRatesFragment;
	public List<Fragment> 		fragmentList ;

	private FragmentTransaction ft;

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		try {
			callback_main = (OnServiceListener) activity;
		} catch (ClassCastException e) {
			throw new ClassCastException(activity.toString() + " must implement OnServiceListener");
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		me = this;
		View fragmentLayout = inflater.inflate(R.layout.fragment_plan_lts, container, false);
		aq = new AAQuery(fragmentLayout);
		initData();
		planLTSMainFragment = new PlanLTSMainFragment();
		return fragmentLayout;
	}

	@Override
	public void onStart() {
		super.onStart();
		//Screen Tracker
	}

	protected void initData(){
		super.initData();
	}

	protected void initUI(){	
		displayChildview(0);
	}

	public void displayChildview(int type) {
		ft = getChildFragmentManager().beginTransaction();
		if (type == 1) {
			ft.setCustomAnimations(R.anim.right_slide_in, R.anim.left_slide_out);	
		} else if (type ==2){
			ft.setCustomAnimations(R.anim.left_slide_in, R.anim.right_slide_out);
		} 
		//		else {
		//			ft.setCustomAnimations(0, 0);
		//		}
		switch (activeSubView) {
		case R.string.CONST_SELECTEDVIEW_LTSPLAN_MAIN:
		case R.string.CONST_SELECTEDVIEW_LTSPLAN_MAIN1:
			//			if (planLTSMainFragment == null) planLTSMainFragment = new PlanLTSMainFragment();
			//			 planLTSMainFragment = new PlanLTSMainFragment();
			ft.replace(R.id.fragment_lts_frameLayout, planLTSMainFragment);		
			//					ft.addToBackStack(null);
			break;		
		case R.string.CONST_SELECTEDVIEW_IDDRATES:
			iddRatesFragment = new IDDRatesFragment();
			ft.replace(R.id.fragment_lts_frameLayout, iddRatesFragment);
			iddRatesFragment.refreshData();
			//					ft.addToBackStack(null);
			break;
		}
		ft.commit();
		setModuleId();
		if (debug) Log.i(TAG, "R2.5");
	}


	public void setModuleId() {
		if (callback_main.getActiveSubview() == R.string.CONST_SELECTEDVIEW_LTSPLAN) {
			if (activeSubView ==  R.string.CONST_SELECTEDVIEW_LTSPLAN_MAIN || activeSubView ==  R.string.CONST_SELECTEDVIEW_LTSPLAN_MAIN1) {
				callback_livechat.setModuleId(getResString(R.string.MODULE_LTS_PLAN));
			} else {
				callback_livechat.setModuleId(getResString(R.string.MODULE_LTS_IDD));
			}
		}
	}
	//Refresh the data only when it is on current page
	public void refreshData(){
//		if (callback_main.getActiveSubview() != R.string.CONST_SELECTEDVIEW_LTSPLAN) {
//			activeSubView = R.string.CONST_SELECTEDVIEW_LTSPLAN_MAIN;
//			displayChildview(0);
//		}
	}

	public void refresh() {
		super.refresh();
		if (debug) Log.i(TAG, "R1");
		activeSubView = R.string.CONST_SELECTEDVIEW_LTSPLAN_MAIN;
		if (debug) Log.i(TAG, "R2");
		displayChildview(0);
		if (debug) Log.i(TAG, "R3 ");
		planLTSMainFragment.refresh();
	}
 
	public void updateUI(){

	}

	protected void cleanupUI() {

	}

	@Override
	public int getCurrentPlanPage() {
		return activeSubView;
	}

	@Override
	public String getSrvNum() {
		return callback_main.getAcctAgent().getSrvNum();
	}

	@Override
	public String getCustNum() {
		return callback_main.getAcctAgent().getCusNum();
	}

	@Override
	public void setActiveSubView(int view) {
		activeSubView = view;
	}
}

