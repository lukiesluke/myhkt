/*
    Crate LTSA Misc
    
    Keywords
    --------
    $URL: $
    $Rev: $
    $Date: $
    $Author: $
*/

package com.pccw.dango.shared.cra;

import java.io.Serializable;

import com.pccw.dango.shared.entity.Account;
import com.pccw.dango.shared.entity.CustRec;
import com.pccw.dango.shared.entity.SubnRec;

public class LtsAMiscCra extends BaseCraEx implements Serializable
{
    private static final long serialVersionUID = 1974077264314476353L;
    
    private String                  iLoginId;           /* Login ID                                      */
    private CustRec                 iCustRec;           /* Customer Information                          */
    private Account                 iAcct;              /* Account Information                           */
    private SubnRec                 iSubnRec;           /* Subscription Record                           */
    
    private String                  oPonCvg;            /* PON Coverage                                  */
    private String                  oDOB;               /* Date of Birth                                 */
    private String                  oIAdr;              /* IA                                            */

    
    public static void main(String rArg[])
    {
        System.out.println(getVer());
        return;
    }
    
    
    public static String getVer()
    {
        return ("$URL: $, $Rev: $");
    }

    
    public LtsAMiscCra()
    {
        initAndClear();
    }


    protected void init()
    {
        super.init();
    }


    public void clear()
    {
        super.clear();
        
        clearILoginId();
        clearICustRec();
        clearIAcct();
        clearISubnRec();
        clearOPonCvg();
        clearODOB();
        clearOIAdr();
    }


    public LtsAMiscCra copyFrom(LtsAMiscCra rSrc)
    {
        super.copyFrom(rSrc);
        
        setILoginId(rSrc.getILoginId());
        setICustRec(rSrc.getICustRec());
        setIAcct(rSrc.getIAcct());
        setISubnRec(rSrc.getISubnRec());
        setOPonCvg(rSrc.getOPonCvg());
        setODOB(rSrc.getODOB());
        setOIAdr(rSrc.getOIAdr());

        return (this);
    }


    public LtsAMiscCra copyTo(LtsAMiscCra rDes)
    {
        rDes.copyFrom(this);
        return (rDes);
    }
    
    
    public LtsAMiscCra copyMe()
    {
        LtsAMiscCra                 rDes;

        rDes = new LtsAMiscCra();
        rDes.copyFrom(this);
        return (rDes);
    }


    public void clearILoginId()
    {
        iLoginId = "";
    }


    public void setILoginId(String rArg)
    {
        iLoginId = rArg;
    }


    public String getILoginId()
    {
        return (iLoginId);
    }


    public void clearICustRec()
    {
        iCustRec = new CustRec();
    }


    public void setICustRec(CustRec rArg)
    {
        iCustRec = rArg;
    }


    public CustRec getICustRec()
    {
        return (iCustRec);
    }


    public void clearIAcct()
    {
        iAcct = new Account();
    }


    public void setIAcct(Account rArg)
    {
        iAcct = rArg;
    }


    public Account getIAcct()
    {
        return (iAcct);
    }


    public void clearISubnRec()
    {
        iSubnRec = new SubnRec();
    }


    public void setISubnRec(SubnRec rArg)
    {
        iSubnRec = rArg;
    }


    public SubnRec getISubnRec()
    {
        return (iSubnRec);
    }


    public void clearOPonCvg()
    {
        oPonCvg = "";
    }


    public void setOPonCvg(String rArg)
    {
        oPonCvg = rArg;
    }


    public String getOPonCvg()
    {
        return (oPonCvg);
    }


    public void clearODOB()
    {
        oDOB = "";
    }


    public void setODOB(String rArg)
    {
        oDOB = rArg;
    }


    public String getODOB()
    {
        return (oDOB);
    }


    public void clearOIAdr()
    {
        oIAdr = "";
    }


    public void setOIAdr(String rArg)
    {
        oIAdr = rArg;
    }


    public String getOIAdr()
    {
        return (oIAdr);
    }
}
