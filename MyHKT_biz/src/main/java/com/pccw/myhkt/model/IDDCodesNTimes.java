package com.pccw.myhkt.model;

import java.io.Serializable;
import java.util.List;

public class IDDCodesNTimes implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -8366374455308079063L;
	
	private String countryId;
	private String countryName;
	private String countryCode;
	private String areaCode;
	private String timeDiff;
	private String time;
	private String countryName2;
	private String hr;
	private String min;
	private String timeDiff_dest;
	private String timeDiff_local;
	private String otherInfo;

	public IDDCodesNTimes() {
		super();
		// TODO Auto-generated constructor stub
	}

	public String getCountryId() {
		return countryId;
	}

	public void setCountryId(String countryId) {
		this.countryId = countryId;
	}

	public String getCountryName() {
		return countryName;
	}

	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public String getAreaCode() {
		return areaCode;
	}

	public void setAreaCode(String areaCode) {
		this.areaCode = areaCode;
	}

	public String getTimeDiff() {
		return timeDiff;
	}

	public void setTimeDiff(String timeDiff) {
		this.timeDiff = timeDiff;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public String getCountryName2() {
		return countryName2;
	}

	public void setCountryName2(String countryName2) {
		this.countryName2 = countryName2;
	}

	public String getHr() {
		return hr;
	}

	public void setHr(String hr) {
		this.hr = hr;
	}

	public String getMin() {
		return min;
	}

	public void setMin(String min) {
		this.min = min;
	}

	public String getTimeDiff_dest() {
		return timeDiff_dest;
	}

	public void setTimeDiff_dest(String timeDiff_dest) {
		this.timeDiff_dest = timeDiff_dest;
	}

	public String getTimeDiff_local() {
		return timeDiff_local;
	}

	public void setTimeDiff_local(String timeDiff_local) {
		this.timeDiff_local = timeDiff_local;
	}

	public String getOtherInfo() {
		return otherInfo;
	}

	public void setOtherInfo(String otherInfo) {
		this.otherInfo = otherInfo;
	}
}