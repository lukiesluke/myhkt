package com.pccw.dango.shared.g3entity;

import java.io.Serializable;


public class G3AcctBomNextBillDateDTO  implements Serializable 
{
    private static final long serialVersionUID = 2159727623917465396L;
    
    private String acctNum;
    private String bomNextBillDate;
    private G3ResultDTO result;
    private String bomStartBillDate;
    
	public String getAcctNum() {
		return acctNum;
	}
	public void setAcctNum(String acctNum) {
		this.acctNum = acctNum;
	}
	public String getBomNextBillDate() {
		return bomNextBillDate;
	}
	public void setBomNextBillDate(String bomNextBillDate) {
		this.bomNextBillDate = bomNextBillDate;
	}
	public G3ResultDTO getResult() {
		return result;
	}
	public void setResult(G3ResultDTO result) {
		this.result = result;
	}
	public String getBomStartBillDate() {
		return bomStartBillDate;
	}
	public void setBomStartBillDate(String bomStartBillDate) {
		this.bomStartBillDate = bomStartBillDate;
	}
    
    
}
