package com.pccw.myhkt.util;

import static com.pccw.myhkt.util.Constant.FILENAME_APP_CONFIG;

import android.content.Context;
import android.util.Log;

import com.pccw.myhkt.R;
import com.pccw.myhkt.model.FileInfo;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import okhttp3.ResponseBody;

public class FileManager {

    // Save string message to a file
    public static boolean fileWriter(Context context, String dataToWrite, String fileName) {
        FileOutputStream outputStream = null;
        boolean fileSave = false;

        try {
            outputStream = context.openFileOutput(fileName, Context.MODE_PRIVATE);
            outputStream.write(dataToWrite.getBytes());
            outputStream.close();
            fileSave = true;

            return fileSave;
        } catch (IOException e) {
            e.printStackTrace();
            try {
                assert outputStream != null;
                outputStream.close();
            } catch (IOException ex) {
                throw new RuntimeException(ex);
            }
            return fileSave;
        }
    }

    public static String getAppConfigJson(Context context) throws IOException {
        return fileReader(context, FILENAME_APP_CONFIG);
    }

    // Get the string values on the file directory and return na the value
    // If file don't exist default value will return
    public static String fileReader(Context context, String fileName) {
        try {
            StringBuffer filePath = new StringBuffer().append(context.getFilesDir().getPath()).append("/").append(fileName);

            File myFileDir = new File(filePath.toString());
            if (!myFileDir.exists()) {
                // If file don't exist return default string for appConfig.json
                return context.getString(R.string.appConfig);
            }

            // Read the file appConfig.json on the file path and return the string value
            InputStream in = new BufferedInputStream(new FileInputStream(myFileDir));
            int size = in.available();

            byte[] buffer = new byte[size];
            in.read(buffer);
            in.close();
            return new String(buffer, StandardCharsets.UTF_8);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public boolean fileImage(Context context, String storagePath, String fileName, String headerLastModified) throws ParseException {
        StringBuffer pathFile = new StringBuffer().append(context.getFilesDir().getPath().replace("files", "")).append("images/en/").append(fileName);

        File file = new File(storagePath + fileName);
        if (file.exists()) {
            SimpleDateFormat format = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss zzz", Locale.US);
            Date serverFileLastModified = format.parse(headerLastModified);

            Date fileLastModified = new Date(file.lastModified());

            Log.d("lwg", "fileLastModified.compareTo: " + fileLastModified.compareTo(serverFileLastModified));
            return fileLastModified.compareTo(serverFileLastModified) != 0;
        } else {
            return false;
        }
    }

    public static boolean writeResponseBodyToDisk(ResponseBody body, String path, FileInfo fileInfo) {
        try {
            File dir = new File(path);
            if (!dir.exists())
                dir.mkdirs();

            File file = new File(path, fileInfo.getFilename());
            InputStream inputStream = null;
            OutputStream outputStream = null;

            try {
                byte[] fileReader = new byte[4096];
                long fileSize = body.contentLength();
                long fileSizeDownloaded = 0;

                inputStream = body.byteStream();
                outputStream = new FileOutputStream(file);

                while (true) {
                    int read = inputStream.read(fileReader);

                    if (read == -1) {
                        break;
                    }
                    outputStream.write(fileReader, 0, read);
                    fileSizeDownloaded += read;
                }
                outputStream.flush();
                if (file.setLastModified(fileInfo.getApiDate().getTime())) {
                    Log.i("lwg", "save file and setLastModified: " + fileInfo.getFilename());
                }
                return true;
            } catch (IOException e) {
                e.printStackTrace();
                return false;
            } finally {
                if (inputStream != null) {
                    inputStream.close();
                }
                if (outputStream != null) {
                    outputStream.close();
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }
}
