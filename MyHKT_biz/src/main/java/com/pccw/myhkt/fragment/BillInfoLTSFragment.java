package com.pccw.myhkt.fragment;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.DialogInterface;
import android.graphics.Point;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;
import androidx.viewpager.widget.ViewPager.OnPageChangeListener;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;

import com.pccw.dango.shared.cra.BiifCra;
import com.pccw.myhkt.DialogHelper;
import com.pccw.biz.myhkt.R;
import com.pccw.myhkt.Utils;
import com.pccw.myhkt.lib.ui.AAQuery;
import com.pccw.myhkt.lib.ui.HKTButton;
import com.pccw.myhkt.pageindicator.CirclePageIndicator;

public class BillInfoLTSFragment extends BaseServiceFragment{
	private BillInfoLTSFragment me;
	private View myView;
	private AAQuery aq;
	
	private int 		colWidth ;
	private int 		buttonPadding;
	private int 		pagePos = 0;
	public 	int 		activeSubView = R.string.CONST_SELECTEDVIEW_BILLINFOLTS_CHILD1;
	private BiifCra		biifCra;
	public Boolean		isZombie = false;

	//	private OnUsageListener callback_usage;
	private ArrayList<Fragment> 	fragmentList;	
	private BillInfoLTSChild1Fragment billInfoLTSChild1Fragment;
	//private BillInfoLTSChild2Fragment billInfoLTSChild2Fragment; ////CA: Remove to disbale the swiping
	
	private ViewPager 				viewPager;
	private CirclePageIndicator		mIndicator;
	private BillInfoLTSPagerAdapter billInfoLTSPagerAdapter;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {	
		super.onCreate(savedInstanceState);
		refreshRunOnResume = false;

		activeSubView = ((BillFragment)getParentFragment()).billInfoLTSSubView;
		pagePos = activeSubView == R.string.CONST_SELECTEDVIEW_BILLINFOLTS_CHILD1 ? 0 : 1;
		fragmentList = new ArrayList<Fragment>();	
		//Zombie will now show pay method
		isZombie = callback_main.IsZombie();
		if (savedInstanceState != null) {
			billInfoLTSChild1Fragment = (BillInfoLTSChild1Fragment) getChildFragmentManager().getFragment(savedInstanceState, "Frag1");
			//if (!isZombie) { billInfoLTSChild2Fragment = (BillInfoLTSChild2Fragment) getChildFragmentManager().getFragment(savedInstanceState, "Frag2");}
		} else {
			billInfoLTSChild1Fragment = new BillInfoLTSChild1Fragment();
			//if (!isZombie) { billInfoLTSChild2Fragment = new BillInfoLTSChild2Fragment(); }
		}
		fragmentList.add(billInfoLTSChild1Fragment);
		//if (!isZombie) { fragmentList.add(billInfoLTSChild2Fragment);}	
		//View pager part
		billInfoLTSPagerAdapter = new BillInfoLTSPagerAdapter(getChildFragmentManager(), fragmentList);
	}

	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		me = this;		
		View fragmentLayout = inflater.inflate(R.layout.fragment_billinfolts, container, false);
		myView = fragmentLayout;
		initData();		
		return fragmentLayout;
	}

	@Override
	public void onStart() {
		super.onStart();
	}

	protected void initData() {
		super.initData();
		aq = new AAQuery(myView);	
		buttonPadding = (int) getResources().getDimension(R.dimen.reg_logo_padding);
		colWidth = (deviceWidth - basePadding * 4- buttonPadding*2)/2;
	}

	protected void initUI(){	

		viewPager = (ViewPager) aq.id(R.id.billinfolts_viewpager).getView();
		viewPager.setAdapter(billInfoLTSPagerAdapter);		
		if (!isZombie) { 
			mIndicator = (CirclePageIndicator) aq.id(R.id.billinfolts_indicator).getView();
			mIndicator.setViewPager(viewPager);
			mIndicator.setCurrentItem(pagePos);
			activeSubView = pagePos == 0 ? R.string.CONST_SELECTEDVIEW_BILLINFOLTS_CHILD1 : R.string.CONST_SELECTEDVIEW_BILLINFOLTS_CHILD2 ;
			setModuleId();
			mIndicator.setOnPageChangeListener(new OnPageChangeListener(){

				@Override
				public void onPageScrollStateChanged(int arg0) {

				}

				@Override
				public void onPageScrolled(int arg0, float arg1, int arg2) {

				}

				@Override
				public void onPageSelected(int position) {
					pagePos = position;
					activeSubView = pagePos == 0 ? R.string.CONST_SELECTEDVIEW_BILLINFOLTS_CHILD1 : R.string.CONST_SELECTEDVIEW_BILLINFOLTS_CHILD2 ;
					callback_livechat.setModuleId(pagePos ==0 ? getResources().getString(R.string.MODULE_LTS_BILL) : getResources().getString(R.string.MODULE_LTS_EBILL));
					Utils.closeSoftKeyboard(getActivity());
					//Store the active subview in billFragment
					((BillFragment)getParentFragment()).billInfoLTSSubView = activeSubView;
					((BaseServiceFragment)fragmentList.get(position)).refresh();
					setBtns();
					setModuleId();
				}
			});
			mIndicator.setVisibility(View.GONE);
		} else {
			mIndicator.setVisibility(View.GONE);
		}
		
		aq.id(R.id.billinfolts_btn_right).visibility(View.VISIBLE);

		aq.id(R.id.billinfolts_btn_left).clicked(this , "onClick").width(colWidth, false);
		aq.marginpx(R.id.billinfolts_btn_left, 0, 0, buttonPadding, 0);
		aq.id(R.id.billinfolts_btn_right).clicked(this , "onClick").width(colWidth, false);
		aq.marginpx(R.id.billinfolts_btn_right, buttonPadding, 0, 0, 0);


		setBtns();
	}

	private void setBtns() {
		aq.id(R.id.billinfolts_btn_left).text(getResString(R.string.MYHKT_BTN_BACK));
		aq.id(R.id.billinfolts_btn_right).text(pagePos == 0 ? getResString(R.string.MYHKT_BTN_UPDATE) : getResString(R.string.MYHKT_BTN_UPDATE));
	}

	public void onClick (View v) {
		switch (v.getId()) {
		case R.id.billinfolts_btn_left:
			((BillFragment)getParentFragment()).openBillSumFrag();
			break;
		case R.id.billinfolts_btn_right:
			if (activeSubView == R.string.CONST_SELECTEDVIEW_BILLINFOLTS_CHILD1) {
				((BillInfoLTSChild1Fragment)fragmentList.get(0)).updateBillInfo();
			} else {
				((BillInfoLTSChild2Fragment)fragmentList.get(1)).doGetPaymentMethodURL();
			}

			break;

		}
	}

	public void setRightBtnName(String text) {
		aq.id(R.id.billinfolts_btn_right).text(text);
	}

	@Override
	public final void refreshData() {
		if (callback_main.getActiveSubview() == R.string.CONST_SELECTEDVIEW_BILLSUMMARY) {
			if (!((BillFragment)getParentFragment()).isBillSum) {
				super.refreshData();
			}
		}
	}

	protected void setModuleId() {
		if (pagePos == 0 ) {
			callback_livechat.setModuleId(getResString(R.string.MODULE_LTS_BILL));	
		} else {
			callback_livechat.setModuleId(getResString(R.string.MODULE_LTS_EBILL));			
		}		
	}
	
	@Override
	public void refresh() {
		super.refresh();
		activeSubView = ((BillFragment)getParentFragment()).billInfoLTSSubView;
		pagePos = activeSubView == R.string.CONST_SELECTEDVIEW_BILLINFOLTS_CHILD1 ? 0 : 1;
		viewPager.setCurrentItem(pagePos);
		//		((BaseServiceFragment)fragmentList.get(pagePos)).refresh();	
	}

	public void hideRightBtn(int pagePos){
		if (debug) Log.i(TAG, "Hide page: " + (pagePos == R.string.CONST_SELECTEDVIEW_BILLINFOLTS_CHILD1 ? 0 : 1));
		if (activeSubView == pagePos){
			aq.id(R.id.billinfolts_btn_right).visibility(View.GONE); //CA: Hide
		}
	}

	public void showRightBtn(int pagePos){
		if (debug) Log.i(TAG, "Show page: " + (pagePos == R.string.CONST_SELECTEDVIEW_BILLINFOLTS_CHILD1 ? 0 : 1));
		if (debug) Log.i(TAG, activeSubView  + "/"+ pagePos);
		if (activeSubView == pagePos){
			aq.id(R.id.billinfolts_btn_right).visibility(View.VISIBLE); //CA: Hide
		}
	}

	// View Pager Class
	private class BillInfoLTSPagerAdapter extends FragmentPagerAdapter {
		private List<Fragment> fragmentList;
		public BillInfoLTSPagerAdapter(FragmentManager fragmentManager ,List<Fragment> fragments) {
			super(fragmentManager);
			fragmentList = fragments;
		}

		@Override
		public int getCount() {
			return fragmentList.size();
		}

		// Returns the fragment to display for that page
		@Override
		public Fragment getItem(int position) {
			switch (position) {
			case 0: // Fragment # 0 - This will show FirstFragment
				//				billInfoLTSChild1Fragment = new BillInfoLTSChild1Fragment();
				//				return billInfoLTSChild1Fragment;
				return fragmentList.get(position);
			case 1: // Fragment # 1 - This will show FirstFragment different title
				return fragmentList.get(position);
				//				billInfoLTSChild2Fragment = new BillInfoLTSChild2Fragment();
				//				return billInfoLTSChild2Fragment;
			default:
				return null;				
			}
		}
	}

	public int getActiveSubView() {
		return activeSubView;
	}	

	@Override
	public void onSaveInstanceState(Bundle outState) {
		if (billInfoLTSChild1Fragment != null) {
			getChildFragmentManager().putFragment(outState, "Frag1",  billInfoLTSChild1Fragment);
		}

//		if (billInfoLTSChild2Fragment != null) {
//			if (!isZombie) {  getChildFragmentManager().putFragment(outState, "Frag2", billInfoLTSChild2Fragment); }
//		}
		super.onSaveInstanceState(outState);
	}
}
