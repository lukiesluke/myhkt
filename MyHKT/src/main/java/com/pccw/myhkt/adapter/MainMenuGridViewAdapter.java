package com.pccw.myhkt.adapter;

/***********************************************************************
 File       : IDDDestListViewAdapter.java
 Desc       : Destination list for IDD Rates search function
 Name       : IDDDestListViewAdapter
 Created by : Derek Tsui
 Date       : 23/01/2014

 Change History:
 Date       Modified By        	Description
 ---------- ----------------   	-------------------------------
 23/01/2014 Derek Tsui       	- First draft
 */

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.pccw.myhkt.ClnEnv;
import com.pccw.myhkt.R;
import com.pccw.myhkt.Utils;
import com.pccw.myhkt.lib.ui.HomeImageButton;
import com.pccw.myhkt.model.HomeButtonItem;

import java.util.List;

public class MainMenuGridViewAdapter extends BaseAdapter {
    private final boolean isZh;
    public Context context;
    public LayoutInflater inflater;
    public List<HomeButtonItem> homeItems;
    private final int itemHeight;
    private Boolean isPremier = false;

    public MainMenuGridViewAdapter(Context context, List<HomeButtonItem> homeItems, int itemHeight) {
        super();
        this.context = context;
        this.itemHeight = itemHeight;
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.homeItems = homeItems;

        // Language indicator
        isZh = "zh".equalsIgnoreCase(Utils.getString(context, R.string.myhkt_lang));
        isPremier = ClnEnv.getSessionPremierFlag();
    }

    public Boolean getIsPremier() {
        return isPremier;
    }

    public void setIsPremier(Boolean isPremier) {
        this.isPremier = isPremier;
    }

    @Override
    public int getCount() {
        if (homeItems == null) return 0;
        return homeItems.size();
    }

    @Override
    public Object getItem(int position) {
        if (position >= 0 && position < homeItems.size()) {
            return homeItems.get(position);
        } else {
            return null;
        }
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        HomeImageButton homeButton = new HomeImageButton(parent.getContext());
        homeButton.initViews(parent.getContext(), homeItems.get(position).image, homeItems.get(position).text, homeItems.get(position).textSize, homeItems.get(position).txtColor, homeItems.get(position).bgColor, itemHeight, isZh, isPremier);

        homeButton.setEnabled(homeItems.get(position).image == -1);
        homeButton.setFocusable(homeItems.get(position).image == -1);
        homeButton.setEnabled(homeItems.get(position).image == -1);

        // Comment out this code below to use only background color
        if (homeItems.get(position).type.equals(HomeButtonItem.MAINMENU.THECLUB) && !isPremier) {
            homeButton.setBg(R.drawable.the_club_bg);
        }

        if (homeItems.get(position).type.equals(HomeButtonItem.MAINMENU.AR_LENS) && !isPremier) {
            homeButton.setBg(R.drawable.ar_lens_bg);
        }

        if (homeItems.get(position).type.equals(HomeButtonItem.MAINMENU.TRAVEL_GUIDE) && !isPremier) {
            homeButton.setBg(R.drawable.travel_guide_bg);
        }

        if (homeItems.get(position).type.equals(HomeButtonItem.MAINMENU.TRAVEL_ZONE) && !isPremier) {
            homeButton.setBg(R.drawable.travel_zone_bg);
        }

        if (homeItems.get(position).type.equals(HomeButtonItem.MAINMENU.DRGO) && !isPremier) {
            homeButton.setBg(R.drawable.home_drgo_bg);
        }
        return homeButton;
    }
}
