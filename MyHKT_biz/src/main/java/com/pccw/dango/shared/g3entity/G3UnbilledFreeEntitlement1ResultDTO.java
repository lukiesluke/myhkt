package com.pccw.dango.shared.g3entity;

import java.io.Serializable;

public class G3UnbilledFreeEntitlement1ResultDTO implements Serializable 
{
    private static final long serialVersionUID = -4585990231766917397L;
    
    private String iccid; //MrbUnbilledFreeEntitlementResultDTO
    private String lastUpdDate; //MrbUnbilledFreeEntitlementResultDTO
    private String msisdn; //MrbUnbilledFreeEntitlementResultDTO    
    private String subId; //MrbUnbilledFreeEntitlementResultDTO
    
    private G3UnbilledFreeEntitlement1DTO[] listUnbilledFreeEntitlement;
    private G3UnbilledResultDTO result;    
	
    public String getIccid() {
		return iccid;
	}
	public void setIccid(String iccid) {
		this.iccid = iccid;
	}
	public String getLastUpdDate() {
		return lastUpdDate;
	}
	public void setLastUpdDate(String lastUpdDate) {
		this.lastUpdDate = lastUpdDate;
	}
	public String getMsisdn() {
		return msisdn;
	}
	public void setMsisdn(String msisdn) {
		this.msisdn = msisdn;
	}
	public String getSubId() {
		return subId;
	}
	public void setSubId(String subId) {
		this.subId = subId;
	}
	public G3UnbilledResultDTO getResult() {
		return result;
	}
	public void setResult(G3UnbilledResultDTO result) {
		this.result = result;
	}
	public G3UnbilledFreeEntitlement1DTO[] getListUnbilledFreeEntitlement() {
		return listUnbilledFreeEntitlement;
	}
	public void setListUnbilledFreeEntitlement(
			G3UnbilledFreeEntitlement1DTO[] listUnbilledFreeEntitlement) {
		this.listUnbilledFreeEntitlement = listUnbilledFreeEntitlement;
	}	
	
}
