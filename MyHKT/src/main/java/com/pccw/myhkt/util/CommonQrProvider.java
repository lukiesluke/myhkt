package com.pccw.myhkt.util;

import com.pccwmobile.tlv.CommonQR;
import com.pccwmobile.tlv.TlvException;

import model.AdditionalDataFieldTemplate;
import model.MerchantAccountInfo;
import model.MerchantAccountInfoFPS;

public class CommonQrProvider {

    public static final String DEFAULT_MERCHANT_CATEGORY_CODE = "0000";
    public static final String PAY_INITIATOR = "INPUTAMT";
    public static final String TAP_AND_GO_IDENTIFIER = "hk.com.tapngo";
    public static final String Merchant_Category_Code = "0000";
    public static final String Country_Code = "HK";
    public static final String Merchant_City = "HK";
    public static final String Postal_Code = "000000";
    public static final String Language_Preference = "ZH";
    public static final String Transaction_Currency = "344";
    public static final String FPS_GLOBALLY_UNIQUE_IDENTIFIER = "hk.com.hkicl";
    public static final String FPS_CLEARING_CODE = "935";


    public static CommonQR getInstance() throws TlvException {

        CommonQR qr = new CommonQR();
        qr.setPayloadFormatIndictor("01");
        qr.setPointOfInitationMethod("12");
        qr.setMerchantCategoryCode(Merchant_Category_Code);

        MerchantAccountInfoFPS fps = new MerchantAccountInfoFPS();
        fps.setGloballyUniqueIdentifier(FPS_GLOBALLY_UNIQUE_IDENTIFIER);
        fps.setClearingCode(FPS_CLEARING_CODE);
        qr.setMerchantAccountInfoFPS(fps);
        MerchantAccountInfo merchantAccountInfo = new MerchantAccountInfo();
        merchantAccountInfo.setGloballyUniqueIdentifier(TAP_AND_GO_IDENTIFIER);
        merchantAccountInfo.setPayInitiator(PAY_INITIATOR);
        qr.addMerchantAccountInfo(merchantAccountInfo);
        AdditionalDataFieldTemplate additionalDataFieldTemplate = new AdditionalDataFieldTemplate();
        qr.setAdditionalDataFieldTemplate(additionalDataFieldTemplate);
        qr.setCountryCode(Country_Code);
        qr.setMerchantCity(Merchant_City);
        qr.setPostalCode(Postal_Code);
        qr.setMerchantName("NA");
        qr.setTransactionCurrency(Transaction_Currency);
        return qr;
    }
}
