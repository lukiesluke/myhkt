package com.pccw.myhkt.fragment;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.os.Parcelable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.webkit.WebView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;

import com.pccw.dango.shared.entity.SveeRec;
import com.pccw.dango.shared.tool.RC;
import com.pccw.myhkt.APIsManager;
import com.pccw.myhkt.APIsResponse;
import com.pccw.myhkt.ClnEnv;
import com.pccw.myhkt.DialogHelper;
import com.pccw.myhkt.R;
import com.pccw.myhkt.activity.BaseActivity;
import com.pccw.myhkt.lib.ui.AAQuery;
import com.pccw.myhkt.lib.ui.popover.ActionItem;
import com.pccw.myhkt.lib.ui.popover.QuickAction;
import com.pccw.myhkt.model.AcctAgent;
import com.pccw.myhkt.model.Destination;
import com.pccw.myhkt.model.Rate;
import com.pccw.myhkt.model.SRPlan;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class IDDRateServiceFragment extends BaseFragment{

	private String TAG = "IDDRatesFragment";
	private IDDRateServiceFragment me;
	private AAQuery aq;

	private SveeRec sveeRec = null;

	// CallBack
	private OnIDDRateServiceListener callback;

	private Boolean 				isZh;
	private Boolean 				isResultShown;
	private int 					extralinespace;
	private int 					textViewHeight;
	private int 					countryPos = -1;
	private ArrayList<Rate> 		rateList = new ArrayList<Rate>();
	private SRPlan 					srPlan;
	private WebView					webView;
	private AcctAgent 				acctAgent;

	public interface OnIDDRateServiceListener {
		public void goToIddRateFragment();
		public void setAcctAgent(AcctAgent acctAgent);
		public AcctAgent getAcctAgent();
	}

	@Override
	public void onAttach(@NonNull Activity activity) {
		super.onAttach(activity);
		try {
			callback = (OnIDDRateServiceListener) activity;
		} catch (ClassCastException e) {
			throw new ClassCastException(activity.toString() + " must implement OnIDDRateServiceListener");
		}
	}

	public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		me = this;		
		View fragmentLayout = inflater.inflate(R.layout.fragment_iddrateservice, container, false);

		// Language indicator
		if ("zh".equalsIgnoreCase( getResString(R.string.myhkt_lang))) {
			isZh = true;
		} else {
			isZh = false;
		}
		aq = new AAQuery(fragmentLayout);

		initData();
		initUI();
		return fragmentLayout;
	}

	protected void initUI() {	
		aq.id(R.id.fragment_iddrateservice).clicked(this, "onClick");
	}


	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.fragment_iddrateservice:
			callback.goToIddRateFragment();
			break;	
		}
	}
	private SRPlan srPlan1;
	private SRPlan srPlan2;

	protected void initData() {
		isResultShown = true;
		extralinespace = (int) getResources().getDimension(R.dimen.extralinespace);
		textViewHeight = (int) getResources().getDimension(R.dimen.textviewheight);
		acctAgent = callback.getAcctAgent();
	}

	private static Destination[]									destinations;
	private static Destination[]									currDestinations;
	private String[]												engNameList;
	private String[]												chiNameList;
	// IDD 0060
	private static ArrayList<Parcelable>							parcDestinationList;
	private static ArrayList<Destination>							destinationList;
	/**********************************
	 * 
	 * Destination part
	 * 
	 ***********************************/
	private final void doneGetDestinationList(List<Parcelable> parcDestinationList) {
		destinationList = new ArrayList<Destination>();
		for (int i = 0; i < parcDestinationList.size(); i++) {
			destinationList.add((Destination) parcDestinationList.get(i));
		}
		destinations = destinationList.toArray(new Destination[0]);
		currDestinations = destinations;

		engNameList = new String[currDestinations.length];
		chiNameList = new String[currDestinations.length];
		// prepare string[] for engDest
		for (int i = 0; i < currDestinations.length; i++) {
			engNameList[i] = currDestinations[i].engDestName;
			chiNameList[i] = currDestinations[i].chiDestName;
		}
	}
	QuickAction mQuickAction;
	private final void openPicker(){
		ActionItem[] actionItem = new ActionItem[3];
		actionItem[0] = new ActionItem(3, getResources().getString(R.string.myhkt_settings_appt_3days));
		actionItem[1] = new ActionItem(5, getResources().getString(R.string.myhkt_settings_appt_5days));
		actionItem[2] = new ActionItem(7, getResources().getString(R.string.myhkt_settings_appt_7days));
		WindowManager wm = (WindowManager) this.getActivity().getSystemService(Context.WINDOW_SERVICE);
		int screenWidth 	= wm.getDefaultDisplay().getWidth();
		mQuickAction 	= new QuickAction(this.getActivity(), screenWidth/2, 0);
		for (ActionItem item : actionItem) {
			mQuickAction.addActionItem(item);
		}

		//setup the action item click listener
		mQuickAction.setOnActionItemClickListener(new QuickAction.OnActionItemClickListener() {
			@Override
			public void onItemClick(QuickAction quickAction, int pos, int actionId) {
				ActionItem actionItem = quickAction.getActionItem(pos);

			}
		});
		mQuickAction.show(aq.id(R.id.fragment_iddrates_country_layout).getView(), aq.id(R.id.fragment_iddrates_country_layout).getView());
	}

	/**********************************
	 * 
	 * Result part
	 * 
	 ***********************************/
	public void doSearch(int position) {
		HashMap<String, String> hmSelectedDestDetail = new HashMap<String, String>();
		hmSelectedDestDetail.put("dest", currDestinations[position].engDestName);
		hmSelectedDestDetail.put("destext", currDestinations[position].engDestNameExt);
		hmSelectedDestDetail.put("customernum", acctAgent.getSubnRec().cusNum);
		hmSelectedDestDetail.put("servicenum", acctAgent.getSubnRec().srvNum);
		hmSelectedDestDetail.put("srvtype", "MOB");
		APIsManager.doGetIDRateResult(me, hmSelectedDestDetail);
	}

	public void setResult(){
		((LinearLayout)aq.id(R.id.fragment_iddrates_result_frame).getView()).removeAllViews();
		//		rateList = srPlan.rateList;

		if (rateList !=null && rateList.size()>0){
			for (int rx = 0; rx < rateList.size(); rx++) {
				LayoutInflater layoutInflater = (LayoutInflater) this.getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);  
				RelativeLayout view = (RelativeLayout) layoutInflater.inflate(R.layout.item_srplan, null, false);
				view.setBackgroundColor(this.getResources().getColor(R.color.idd_grey));

				String name = isZh ? rateList.get(rx).chiTimeZone : rateList.get(rx).timeZone ;
				String desc = isZh ? rateList.get(rx).chiTimeZoneRemarks : rateList.get(rx).timeZoneRemarks ;
				String rates = "HK $" + rateList.get(rx).rate;
				AAQuery a = new AAQuery(view);
				a.normTextGrey(R.id.item_srplan_title, name);
				a.normTextGrey(R.id.item_srplan_rate, rates);
				a.normTextGrey(R.id.item_srplan_time, desc);

				((LinearLayout)aq.id(R.id.fragment_iddrates_result_frame).getView()).addView(view);
				LinearLayout.LayoutParams params = (LayoutParams) view.getLayoutParams();
				params.topMargin = extralinespace;
				view.setLayoutParams(params);
				view.setPadding(extralinespace, extralinespace, extralinespace, extralinespace);
			}
		}
	}

	@Override
	public void onFail(APIsResponse response) {
		// General Error Message
		if (!"".equals(response.getMessage()) && response.getMessage() != null) {
			DialogHelper.createSimpleDialog(getActivity(),  response.getMessage());
		} else if (RC.TCSESS_MM.equalsIgnoreCase(response.getReply().getCode())) {
			BaseActivity.ivSessDialog();
		} else {
			DialogHelper.createSimpleDialog(getActivity(),  ClnEnv.getRPCErrMsg(getActivity(), response.getReply().getCode()));
		}		
	}

	@Override
	public void onStart() {
		super.onStart();
	}

	public void onResume(){
		super.onResume();	
	}

	@Override
	public void onSuccess(APIsResponse response) {
		SRPlan srPlan = (SRPlan) response.getCra();
		rateList = srPlan.rateList;
		setResult();
	}
}
