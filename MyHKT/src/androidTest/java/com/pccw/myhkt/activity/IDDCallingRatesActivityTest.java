package com.pccw.myhkt.activity;

import android.app.ActivityManager;
import android.content.Context;
import android.content.SharedPreferences;
import androidx.test.espresso.intent.Intents;
import androidx.test.rule.ActivityTestRule;
import androidx.test.rule.GrantPermissionRule;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import com.pccw.myhkt.AccountHelper;
import com.pccw.myhkt.R;
import com.pccw.myhkt.base.BaseTest;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.List;

import static androidx.test.platform.app.InstrumentationRegistry.getInstrumentation;

@Ignore
@RunWith(AndroidJUnit4.class)
public class IDDCallingRatesActivityTest extends BaseTest {
    private Context context;
    SharedPreferences shared;

    @Rule
    public ActivityTestRule<MainMenuActivity> activityTestRule = new ActivityTestRule<>(MainMenuActivity.class);
    @Rule
    public GrantPermissionRule permissionRule = GrantPermissionRule.grant(android.Manifest.permission.WRITE_EXTERNAL_STORAGE);

    @Before
    public void setUp(){
        context = getInstrumentation().getTargetContext();
        shared = context.getSharedPreferences(
                "myhkt",
                Context.MODE_PRIVATE);
        Intents.init();
        clickLeftNavBar();
        clickString(context, R.string.myhkt_LGIF_LOGIN);
    }

    @After
    public void tearDown() {
        executeLogout(context);
        Intents.release();
    }

    @Test
    public void testCheckIDDCallingRatesDisplay() {
        loginPRD();
        checkStringIsDisplayed(context,R.string.MYHKT_IDD_TITLE);
        clickString(context, R.string.MYHKT_IDD_BTN_CALL_RATE);
        checkStringIsDisplayed(context, R.string.myhkt_btn_alias);
    }

    @Test
    public void testSetDisplayName(){
        loginPRD();
        changeDisplayNameBaseTest(context, 0, "Home", R.id.fragment_servlist_listview,
                R.string.MYMOB_BTN_ALIAS, R.id.adapter_servicelist_edit,
                R.id.adapter_servicelist_header_alias, waitForText(btn_OK));
    }

    @Test
    public void testCheckSetDisplayName() {
        loginPRD();
        clickString(context, R.string.MYHKT_IDD_BTN_CALL_RATE);
        clickString(context, R.string.myhkt_btn_alias);
    }

    @Test
    public void testClickAccount(){
        loginPRD();
        delay(TestTimeouts.API_TIMEOUTS,waitForIddServiceList());
        clickString(context, R.string.MYHKT_IDD_BTN_CALL_RATE);
        clickViewInAdapter(R.id.fragment_servlist_listview,0);
        delay(TestTimeouts.ACTIVITY_LOAD_TIMEOUTS);
        checkViewIsNotDisplayed(R.id.fragment_iddrates_remark);
        checkViewIsNotDisplayed(R.id.fragment_iddrates_result_scrollview);
        clickView(R.id.fragment_iddrates_country_layout);
        delay(TestTimeouts.DIALOG_TIMEOUTS);
        swipeUpView(R.id.wheelLeft);
        delay(TestTimeouts.API_TIMEOUTS, waitingForCountryList()); // waiting API response

        checkViewIsDisplayed(R.id.fragment_iddrates_remark);
        checkViewIsDisplayed(R.id.fragment_iddrates_result_scrollview);

        clickView(R.id.fragment_iddrates_country_layout);
        swipeDownView(R.id.wheelLeft);
        delay(TestTimeouts.DIALOG_TIMEOUTS);
        pressBackView(R.id.wheelLeft);

        clickView(R.id.fragment_iddrates_country_layout);
        swipeDownView(R.id.wheelLeft);
        delay(TestTimeouts.DIALOG_TIMEOUTS);
        pressBackView(R.id.wheelLeft);

        checkViewIsNotDisplayed(R.id.fragment_iddrates_remark);
        checkViewIsNotDisplayed(R.id.fragment_iddrates_result_scrollview);
    }

    @Test
    public void testTabs(){
        loginUAT();
        clickString(context, R.string.MYHKT_IDD_BTN_CALL_RATE);
        checkViewIsDisplayed(R.id.fragment_servlist_btn_layout);

        clickString(context, R.string.MYHKT_IDD_BTN_NEWS);
        checkStringIsDisplayed(context, R.string.MYHKT_IDD_NEWS_GLANCE);
    }

    @Test
    public void testLiveChat(){
        loginUAT();
        checkLiveChat(context,R.string.LIVE_CHAT_DISCLAIMER,
                R.id.fragment_servlist_btn_layout);
    }

    @Test
    public void testBackBtn(){
        loginUAT();
        checkBackBtn(MainMenuActivity.class.getName(),11);
    }

    @Test
    public void testEmptyService(){
        loginUAT();
        delay(TestTimeouts.API_TIMEOUTS,waitForIddServiceList());
        checkStringIsDisplayed(context,R.string.MYHKT_NO_SUBSCRIPTION);
    }

    public void loginUAT(){
        loginMyCredentialsUAT(AccountHelper.getUATAccount7().getUsername(), AccountHelper.getUATAccount7().getPassword(), shared);
        gotoMainMenuItem(11);
    }

    public void loginPRD(){
        loginMyCredentialsPRD(AccountHelper.getPRDAccount3().getUsername(),
                    AccountHelper.getPRDAccount3().getPassword(), shared);
        gotoMainMenuItem(6);
    }

    public Runnable waitForIddServiceList(){
        return new Runnable(){
            @Override
            public void run(){
                try {
                    ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
                    List<ActivityManager.RunningTaskInfo> tasks = am.getRunningTasks(1);
                    isStop = tasks.get(0).topActivity.getClassName().contains(IDDActivity.class.getSimpleName());
                }catch (Exception e){
                    isStop = false;
                }
            }
        };
    }

    public Runnable waitingForCountryList(){
        return new Runnable(){
            @Override
            public void run(){
                try {
                    checkViewIsDisplayed(R.id.fragment_iddrates_remark);
                }catch (Exception e){
                    isStop = false;
                }
            }
        };
    }

}