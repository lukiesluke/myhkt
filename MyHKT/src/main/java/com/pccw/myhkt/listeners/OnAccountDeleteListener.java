package com.pccw.myhkt.listeners;

public interface OnAccountDeleteListener {
    void onAccountDeleted();
    void onFailedDeleted();
}
