package com.pccw.myhkt.activity;

import android.app.PendingIntent;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.View;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.pccw.dango.shared.entity.ShopRec;
import com.pccw.myhkt.ClnEnv;
import com.pccw.myhkt.R;
import com.pccw.myhkt.fragment.BaseShopFragment.OnShopListener;
import com.pccw.myhkt.fragment.ShopDetailFragment;
import com.pccw.myhkt.fragment.ShopDistrictFragment;
import com.pccw.myhkt.fragment.ShopMyLocFragment;
import com.pccw.myhkt.fragment.ShopTypeFragment;
import com.pccw.myhkt.lib.ui.AAQuery;
import com.pccw.myhkt.mapviewballoons.MapFragmentD;
import com.pccw.myhkt.util.Constant;
import com.pccw.myhkt.util.RuntimePermissionUtil;

/************************************************************************
 File       : ShopActivity.java
 Desc       : Shop Screen
 Name       : ShopActivity
 Created by : Derek Tsui
 Date       : 7/12/2015

 Change History:
 Date       Modified By			Description
 ---------- ----------------	-------------------------------
 7/12/2015 Derek Tsui			- First draft
 *************************************************************************/

public class ShopActivity extends BaseActivity implements OnShopListener, MapFragmentD.OnMapEventListener {
    private AAQuery aq;
    private FragmentManager fragmentManager;
    protected int activeSubview = R.string.CONST_SELECTEDFRAG_SHOPTYPE;    // Initial default subview
    private Fragment ShopTypeFragment = null;
    private Fragment shopDistrictFragment = null;
    private Fragment shopMyLocFragment = null;
    private Fragment shopDetailFragment = null;
    private int shopType = -1;
    private ShopRec shopRec = null;
    private ShopRec[] shopRecAry = null;
    private boolean isPause = false;

    @Override
    public final void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        me = this;
        isLiveChatShown = false;
        ClnEnv.updateUILocale(getBaseContext(), ClnEnv.getAppLocale(getBaseContext()));
        setContentView(R.layout.activity_shop);

    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        ClnEnv.updateUILocale(getBaseContext(),
                ClnEnv.getAppLocale(getBaseContext()));
    }

    @Override
    protected final void onStart() {
        super.onStart();
        initUI();
        displaySubview();
    }

    @Override
    protected final void onPause() {
        super.onPause();
        isPause = true;
    }

    public final void onBackPressed() {
        if (fragmentManager.getBackStackEntryCount() > 1) {
            popBackStack();
        } else {
            finish();
            overridePendingTransition(R.anim.left_slide_in, R.anim.right_slide_out);
        }
    }

    private final void initUI() {
        fragmentManager = getSupportFragmentManager();
        aq = new AAQuery(this);

        //navbar style
        aq.navBarBaseLayout(R.id.navbar_base_layout);
        aq.navBarTitle(R.id.navbar_title, getResources().getString(R.string.myhkt_shop_title));
        aq.navBarButton(R.id.navbar_button_left, R.drawable.btn_back);
        aq.navBarButton(R.id.navbar_button_right, R.drawable.livechat_small);

        aq.id(R.id.navbar_button_left).clicked(this, "onClick");
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.navbar_button_left:
                onBackPressed();
                break;
        }
    }

    public final void displaySubview() {
        fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.setCustomAnimations(R.anim.right_slide_in, R.anim.left_slide_out, R.anim.left_slide_in, R.anim.right_slide_out);
        if (!isPause) {
            switch (activeSubview) {
                case R.string.CONST_SELECTEDFRAG_SHOPTYPE:
                    moduleId = getResources().getString(R.string.MODULE_SHOP);
                    ShopTypeFragment = new ShopTypeFragment();
                    transaction.replace(R.id.shop_commonview, ShopTypeFragment);
                    transaction.addToBackStack(null);
                    break;
                case R.string.CONST_SELECTEDFRAG_SHOPDISTRICT:
                    moduleId = getResources().getString(R.string.MODULE_SHOP_LIST);
                    shopDistrictFragment = new ShopDistrictFragment();
                    transaction.replace(R.id.shop_commonview, shopDistrictFragment);
                    transaction.addToBackStack(null);
                    break;
                case R.string.CONST_SELECTEDFRAG_SHOPMYLOC:
                    moduleId = getResources().getString(R.string.MODULE_SHOP_MY_LOCATION);
                    shopMyLocFragment = new ShopMyLocFragment();
                    transaction.replace(R.id.shop_commonview, shopMyLocFragment);
                    transaction.addToBackStack(null);
                    break;
                case R.string.CONST_SELECTEDFRAG_SHOPDETAIL:
                    moduleId = getResources().getString(R.string.MODULE_SHOP_DETAIL);
                    shopDetailFragment = new ShopDetailFragment();
                    transaction.replace(R.id.shop_commonview, shopDetailFragment);
                    transaction.addToBackStack(null);
                    break;
            }
        }
        isPause = false;
        transaction.commit();
    }

    public int getActiveSubview() {
        return activeSubview;
    }

    public void setActiveSubview(int activeSubview) {
        this.activeSubview = activeSubview;
    }

    @Override
    public void onFirstFix() {
    }

    @Override
    public void onShopDetail(ShopRec shop) {
        setShopRec(shop);
        setActiveSubview(R.string.CONST_SELECTEDFRAG_SHOPDETAIL);
        displaySubview();
    }

    public int getShopType() {
        return shopType;
    }

    public void setShopType(int shopType) {
        this.shopType = shopType;
    }

    public ShopRec[] getShopRecAry() {
        return shopRecAry;
    }

    public void setShopRecAry(ShopRec[] shopRecAry) {
        this.shopRecAry = shopRecAry;
    }

    public ShopRec getShopRec() {
        return shopRec;
    }

    public void setShopRec(ShopRec shopRec) {
        this.shopRec = shopRec;
    }

    @Override
    public void popBackStack() {
        fragmentManager.popBackStack();
    }

    public boolean isPause() {
        return isPause;
    }

    public void setPause(boolean isPause) {
        this.isPause = isPause;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == Constant.REQUEST_LOCATION_PERMISSION) {
            //Permission is granted
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Intent intent = new Intent();
                intent.setAction(Constant.ACTION_LOCATION_REQUEST_GRANTED);
                intent.setPackage(getPackageName());
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.S) {
                    PendingIntent.getBroadcast(this, 0, intent, PendingIntent.FLAG_IMMUTABLE | PendingIntent.FLAG_UPDATE_CURRENT);
                } else {
                    PendingIntent.getBroadcast(this, 0, intent, PendingIntent.FLAG_CANCEL_CURRENT | PendingIntent.FLAG_IMMUTABLE);
                }
                sendBroadcast(intent);
            } else {
                displayDialog(this, getString(R.string.permission_denied));

                //This will return false if the user tick the Don't ask again checkbox
                if (!RuntimePermissionUtil.shouldShowLocationRequestPermission(this)) {
                    ClnEnv.setPref(getApplicationContext(), Constant.LOCATION_PERMISSION_DENIED, true);
                }
            }
        }
    }
}
