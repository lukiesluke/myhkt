/*
    Billing Information
    
    Keywords
    --------
    $URL: $
    $Rev: $
    $Date: $
    $Author: $
*/

package com.pccw.dango.shared.entity;

import java.io.Serializable;

public class Biif implements Serializable
{
    private static final long serialVersionUID = 3651535716806028445L;
    
    private String                  billNm;             /* Billing Name (for H use)                      */
    private String                  billAdr1;           /* Billing Address 1 (for H use)                 */
    private String                  billAdr2;           /* Billing Address 2 (for H use)                 */
    private String                  billAdr3;           /* Billing Address 3 (for H use)                 */
    private String                  billAdr4;           /* Billing Address 4 (for H use)                 */
    private String                  billAdr5;           /* Billing Address 5 (for H use)                 */
    private String                  billAdr6;           /* Billing Address 6 (for H use)                 */
    private String                  billEmail;          /* Billing Email (for H use)                     */
    private String                  billMedia;          /* Billing Media (for H use)                     */
    private String                  billLang;           /* Billing Language (for H use)                  */
    private String                  smsNtfn;            /* SMS Notification MSISDN (for H, LTS use)      */
    private String                  waivePbChrg;        /* Waive PaperBill Charge (for H, LTS use)       */
    private String                  payMeth;            /* Payment Method  (for H, LTS use)              */
    private String                  tok4Pan;            /* CEKS Token (for H, LTS use)                   */
    private String                  tokExpr;            /* CEKS Token Expiry Date (for H, LTS use)       */
    private String                  payDocTy;           /* Payer DocTy (for H, LTS use)                  */
    private String                  payDocNum;          /* Payer DocNum (for H, LTS use)                 */
    private String                  payName;            /* Payer Name (for H, LTS use)      			 */
    private String                  unit;               /* Unit (for C use)                              */
    private String                  flat;               /* Flat (for C use)                              */
    private String                  floor;              /* Floor (for C use)                             */
    private String                  block;              /* Block (for C use)                             */
    private String                  building;           /* Building (for C use)                          */
    private String                  section;            /* Section (for C use)                           */
    private String                  streetNum;          /* Street Number (for C use)                     */
    private String                  streetNm;           /* Street Name (for C use)                       */
    private String                  streetTy;           /* Street Type (for C use)                       */
    private String                  district;           /* District (for C use)                          */
    private String                  area;               /* Area (for C use)                              */
    private String                  postalNm;           /* Postal Name (for C use)                       */
    private String                  postalBx;           /* Postal Box (for C use)                        */
    private boolean                 pox;                /* Is PO Box address? (for C use)                */


    public static void main(String rArg[])
    {
        System.out.println(getVer());
        return;
    }
    
    
    public static String getVer()
    {
        return ("$URL: $, $Rev: $");
    }

    
    public Biif()
    {
        initAndClear();
    }

    
    final void initAndClear()
    {
        init();
        clear();
    }


    protected void init()
    {
    }


    public void clear()
    {
        clearBillNm();
        clearBillAdr1();
        clearBillAdr2();
        clearBillAdr3();
        clearBillAdr4();
        clearBillAdr5();
        clearBillAdr6();
        clearBillEmail();
        clearBillMedia();
        clearBillLang();
        clearSmsNtfn();
        clearWaivePbChrg();
        clearPayMeth();
        clearTok4Pan();
        clearTokExpr();
        clearPayDocTy();
        clearPayDocNum();
        clearPayName();
        clearUnit();
        clearFlat();
        clearFloor();
        clearBlock();
        clearBuilding();
        clearSection();
        clearStreetNum();
        clearStreetNm();
        clearStreetTy();
        clearDistrict();
        clearArea();
        clearPostalNm();
        clearPostalBx();
        clearPox();
    }


    public Biif copyFrom(Biif rSrc)
    {
        setBillNm(rSrc.getBillNm());
        setBillAdr1(rSrc.getBillAdr1());
        setBillAdr2(rSrc.getBillAdr2());
        setBillAdr3(rSrc.getBillAdr3());
        setBillAdr4(rSrc.getBillAdr4());
        setBillAdr5(rSrc.getBillAdr5());
        setBillAdr6(rSrc.getBillAdr6());
        setBillEmail(rSrc.getBillEmail());
        setBillMedia(rSrc.getBillMedia());
        setBillLang(rSrc.getBillLang());
        setSmsNtfn(rSrc.getSmsNtfn());
        setWaivePbChrg(rSrc.getWaivePbChrg());
        setPayMeth(rSrc.getPayMeth());
        setTok4Pan(rSrc.getTok4Pan());
        setTokExpr(rSrc.getTokExpr());
        setPayDocTy(rSrc.getPayDocTy());
        setPayDocNum(rSrc.getPayDocNum());
        setPayName(rSrc.getPayName());
        setUnit(rSrc.getUnit());
        setFlat(rSrc.getFlat());
        setFloor(rSrc.getFloor());
        setBlock(rSrc.getBlock());
        setBuilding(rSrc.getBuilding());
        setSection(rSrc.getSection());
        setStreetNum(rSrc.getStreetNum());
        setStreetNm(rSrc.getStreetNm());
        setStreetTy(rSrc.getStreetTy());
        setDistrict(rSrc.getDistrict());
        setArea(rSrc.getArea());
        setPostalNm(rSrc.getPostalNm());
        setPostalBx(rSrc.getPostalBx());
        setPox(rSrc.isPox());

        return (this);
    }


    public Biif copyTo(Biif rDes)
    {
        rDes.copyFrom(this);
        return (rDes);
    }
    
    
    public Biif copyMe()
    {
        Biif                        rDes;

        rDes = new Biif();
        rDes.copyFrom(this);
        return (rDes);
    }


    public void clearBillNm()
    {
        billNm = "";
    }


    public void setBillNm(String rArg)
    {
        billNm = rArg;
    }


    public String getBillNm()
    {
        return (billNm);
    }


    public void clearBillAdr1()
    {
        billAdr1 = "";
    }


    public void setBillAdr1(String rArg)
    {
        billAdr1 = rArg;
    }


    public String getBillAdr1()
    {
        return (billAdr1);
    }


    public void clearBillAdr2()
    {
        billAdr2 = "";
    }


    public void setBillAdr2(String rArg)
    {
        billAdr2 = rArg;
    }


    public String getBillAdr2()
    {
        return (billAdr2);
    }


    public void clearBillAdr3()
    {
        billAdr3 = "";
    }


    public void setBillAdr3(String rArg)
    {
        billAdr3 = rArg;
    }


    public String getBillAdr3()
    {
        return (billAdr3);
    }


    public void clearBillAdr4()
    {
        billAdr4 = "";
    }


    public void setBillAdr4(String rArg)
    {
        billAdr4 = rArg;
    }


    public String getBillAdr4()
    {
        return (billAdr4);
    }


    public void clearBillAdr5()
    {
        billAdr5 = "";
    }


    public void setBillAdr5(String rArg)
    {
        billAdr5 = rArg;
    }


    public String getBillAdr5()
    {
        return (billAdr5);
    }


    public void clearBillAdr6()
    {
        billAdr6 = "";
    }


    public void setBillAdr6(String rArg)
    {
        billAdr6 = rArg;
    }


    public String getBillAdr6()
    {
        return (billAdr6);
    }


    public void clearBillEmail()
    {
        billEmail = "";
    }


    public void setBillEmail(String rArg)
    {
        billEmail = rArg;
    }


    public String getBillEmail()
    {
        return (billEmail);
    }


    public void clearBillMedia()
    {
        billMedia = "";
    }


    public void setBillMedia(String rArg)
    {
        billMedia = rArg;
    }


    public String getBillMedia()
    {
        return (billMedia);
    }


    public void clearBillLang()
    {
        billLang = "";
    }


    public void setBillLang(String rArg)
    {
        billLang = rArg;
    }


    public String getBillLang()
    {
        return (billLang);
    }
    
    public void clearSmsNtfn()
    {
        smsNtfn = "";
    }


    public void setSmsNtfn(String rArg)
    {
        smsNtfn = rArg;
    }


    public String getSmsNtfn()
    {
        return (smsNtfn);
    }


    public void clearWaivePbChrg()
    {
        waivePbChrg = "";
    }


    public void setWaivePbChrg(String rArg)
    {
        waivePbChrg = rArg;
    }


    public String getWaivePbChrg()
    {
        return (waivePbChrg);
    }


    public void clearPayMeth()
    {
        payMeth = "";
    }


    public void setPayMeth(String rArg)
    {
        payMeth = rArg;
    }


    public String getPayMeth()
    {
        return (payMeth);
    }


    public void clearTok4Pan()
    {
        tok4Pan = "";
    }


    public void setTok4Pan(String rArg)
    {
        tok4Pan = rArg;
    }


    public String getTok4Pan()
    {
        return (tok4Pan);
    }


    public void clearTokExpr()
    {
        tokExpr = "";
    }


    public void setTokExpr(String rArg)
    {
        tokExpr = rArg;
    }


    public String getTokExpr()
    {
        return (tokExpr);
    }


    public void clearPayDocTy()
    {
        payDocTy = "";
    }


    public void setPayDocTy(String rArg)
    {
        payDocTy = rArg;
    }


    public String getPayDocTy()
    {
        return (payDocTy);
    }


    public void clearPayDocNum()
    {
        payDocNum = "";
    }


    public void setPayDocNum(String rArg)
    {
        payDocNum = rArg;
    }


    public String getPayDocNum()
    {
        return (payDocNum);
    }


    public void clearPayName()
    {
        payName = "";
    }


    public void setPayName(String rArg)
    {
        payName = rArg;
    }


    public String getPayName()
    {
        return (payName);
    }



    public void clearUnit()
    {
        unit = "";
    }


    public void setUnit(String rArg)
    {
        unit = rArg;
    }


    public String getUnit()
    {
        return (unit);
    }


    public void clearFlat()
    {
        flat = "";
    }


    public void setFlat(String rArg)
    {
        flat = rArg;
    }


    public String getFlat()
    {
        return (flat);
    }


    public void clearFloor()
    {
        floor = "";
    }


    public void setFloor(String rArg)
    {
        floor = rArg;
    }


    public String getFloor()
    {
        return (floor);
    }


    public void clearBlock()
    {
        block = "";
    }


    public void setBlock(String rArg)
    {
        block = rArg;
    }


    public String getBlock()
    {
        return (block);
    }


    public void clearBuilding()
    {
        building = "";
    }


    public void setBuilding(String rArg)
    {
        building = rArg;
    }


    public String getBuilding()
    {
        return (building);
    }


    public void clearSection()
    {
        section = "";
    }


    public void setSection(String rArg)
    {
        section = rArg;
    }


    public String getSection()
    {
        return (section);
    }


    public void clearStreetNum()
    {
        streetNum = "";
    }


    public void setStreetNum(String rArg)
    {
        streetNum = rArg;
    }


    public String getStreetNum()
    {
        return (streetNum);
    }


    public void clearStreetNm()
    {
        streetNm = "";
    }


    public void setStreetNm(String rArg)
    {
        streetNm = rArg;
    }


    public String getStreetNm()
    {
        return (streetNm);
    }


    public void clearStreetTy()
    {
        streetTy = "";
    }


    public void setStreetTy(String rArg)
    {
        streetTy = rArg;
    }


    public String getStreetTy()
    {
        return (streetTy);
    }


    public void clearDistrict()
    {
        district = "";
    }


    public void setDistrict(String rArg)
    {
        district = rArg;
    }


    public String getDistrict()
    {
        return (district);
    }


    public void clearArea()
    {
        area = "";
    }


    public void setArea(String rArg)
    {
        area = rArg;
    }


    public String getArea()
    {
        return (area);
    }


    public void clearPostalNm()
    {
        postalNm = "";
    }


    public void setPostalNm(String rArg)
    {
        postalNm = rArg;
    }


    public String getPostalNm()
    {
        return (postalNm);
    }


    public void clearPostalBx()
    {
        postalBx = "";
    }


    public void setPostalBx(String rArg)
    {
        postalBx = rArg;
    }


    public String getPostalBx()
    {
        return (postalBx);
    }


    public void clearPox()
    {
        pox = false;
    }


    public void setPox(boolean rArg)
    {
        pox = rArg;
    }


    public boolean isPox()
    {
        return (pox);
    }
}
