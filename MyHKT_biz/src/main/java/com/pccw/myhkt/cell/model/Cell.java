package com.pccw.myhkt.cell.model;

import android.graphics.Typeface;
import android.graphics.drawable.Drawable;

import com.pccw.biz.myhkt.R;
import com.pccw.dango.shared.entity.Bill;

import java.util.List;

public class Cell {
//	public final static int BIGTEXT = 0;
//	public final static int SMALLTEXT = 1;
	public final static int BTNS3 = 2;
	public final static int IMGBTNS2 = 3;
	public final static int DETAIL_BTN = 4;
	public final static int LINE = 5;
	public final static int ICONTEXT = 6;
	public final static int ARROWTEXT = 7;
	public final static int COL3 = 8;
	public final static int ENQBOX = 9;
	public final static int TWOBTN = 10;
//	public final static int XSMALLTEXT = 11;
	public final static int CIRCLEVIEW = 12;
	public final static int SMALLTEXT1 = 14;
	public final static int BIGTEXT1 = 13;	
	public final static int TEXTBTN = 15;
	public final static int SPINNERTEXT = 16;
	public final static int SINGLEBTN = 17;
	public final static int WEBVIEW = 18;
	public final static int LINETEST = 19;
	public final static int IMAGEVIEW = 20;
	public final static int TITLESPINNER = 21;
	public final static int TEXTICON = 22;
	public final static int TEXTIMGBTN = 23;
	public final static int TWOTEXTBTN = 24;
	public final static int BTNS9 = 25;
	// Bundle name
	protected int type;
	protected int titleTypeface  = Typeface.NORMAL; //e.g. normal, bold, italic..
	protected int bgColorId = R.color.white;
	protected int titleColorId;	
	protected int contentColorId;
	protected int titleSizeDelta;
	protected int contentSizeDelta;
	protected int bottompadding;
	protected String title;
	protected String content;
	protected String[] clickArray;	
	protected String[] textArray;
	protected List<Bill> billList;
	protected String url;
	protected int[] imageArray; //
	protected int cellHeight = 0;
	protected boolean isTaller = false;
	
	private int[] widthArray;
	protected int draw;
	protected Drawable drawable = null;
	protected Boolean isVisible = true;
	public Boolean getIsVisible() {
		return isVisible;
	}
	public void setIsVisible(Boolean isVisible) {
		this.isVisible = isVisible;
	}
	public Drawable getDrawable() {
		return drawable;
	}
	public void setDrawable(Drawable drawable) {
		this.drawable = drawable;
	}
	private int[] resArray;
	private int textSize;

	private boolean isBotCell = false;
	private boolean expandText = false;	
	protected boolean isArrowShown = false;
	private int bgId = -1; 
	
	
	public int getBgId() {
		return bgId;
	}
	public void setBgId(int bgId) {
		this.bgId = bgId;
	}
	// For margin and padding use default value if it is -1 
	protected int leftMargin = -1;	
	protected int rightMargin = -1;
	protected int topMargin = -1;
	protected int botMargin = -1;
	
	protected int leftPadding = -1;
	protected int rightPadding = -1;
	protected int topPadding = -1;
	protected int botPadding = -1;


	
	

	//Will delete later
	@Deprecated
	private int toppadding;
	@Deprecated
	private int linedraw;
	@Deprecated
	private boolean isbotline = false;
	@Deprecated
	private boolean istopline = false;

	public Cell(){
		
	}
	// BIGTEXT, SMALLTEXT, XSMALLTEXT
	public Cell(int type, String title, String content, boolean isBotCell, boolean expandText) {
		super();
		this.type = type;
		this.title = title;
		this.content = content;
		this.isBotCell = isBotCell;
		this.expandText  = expandText;
	}

	// DETAIL_BTN
	public Cell(int type, String title, String content, int drawRight, int bgcolorId, String[] clickArray) {
		super();
		this.type = type;
		this.title = title;
		this.content = content;
		this.draw = drawRight;
		this.bgColorId = bgcolorId;
		this.clickArray = clickArray;

	}

	// IMGBTNS2
	public Cell(int type, String[] clickArray, int[] resArray) {
		super();
		this.type = type;
		this.clickArray = clickArray;
		this.resArray = resArray;
	}	
	
	//BTNS2
	public Cell(int type, String[] clickArray, String[] textArry) {
		super();
		this.type = type;
		this.clickArray = clickArray;
		this.textArray = textArry;
	}	
	
	@Deprecated
	// BTNS3
	public Cell(int type, String[] clickArray, String[] textArray, int bgcolor, int toppadding, int bottompadding) {
		super();
		this.type = type;
		this.clickArray = clickArray;
		this.textArray = textArray;
		this.bgColorId = bgcolor;
		this.toppadding = toppadding;
		this.bottompadding = bottompadding;
	}

	// BTNS3
	public Cell(int type, String[] clickArray, String[] textArray, int bgcolorId, int[] imageArray) {
		super();
		this.type = type;
		this.clickArray = clickArray;
		this.textArray = textArray;
		this.bgColorId = bgcolorId;			
		this.imageArray = imageArray;
	}

	// BTNS9
	public Cell(int type, int bgcolorId, int[] imageArray, List<Bill> billList) {
		super();
		this.type = type;
		this.bgColorId = bgcolorId;
		this.imageArray = imageArray;
		this.billList = billList;
	}

	// LINE
	public Cell(int type) {
		super();
		this.type = type;
	}

	// ICONTEXT
	public Cell(int type, int draw, String title) {
		super();
		this.type = type;
		this.draw = draw; 
		this.title = title;
	}

	//CellX
	public Cell(int type, int draw, String title, String[] textArray) {
		super();
//		if (textArray.length > 2) Log.i(""," Support max of 2 cols");
		this.type = type;
		this.title = title;
		this.textArray = textArray;
		this.draw = draw;
	}
	
	@Deprecated
	//CellY
	public Cell(int type, String title, String[] textArray, int[] widthArray,int bgcolor) {
		super();
//		if (textArray.length > 2) Log.i(""," Support max of 2 cols");
		this.type = type;
		this.title = title;
		this.textArray = textArray;
		this.bgColorId = bgcolor;		
		this.widthArray = widthArray;
	}
	
	//ENQBOX
	public Cell(int type, String btnContent, int btnDraw, String url, int[] widthAry) {
		super();
		this.type = type;
		this.content = btnContent;
		this.draw = btnDraw;
		this.url = url;
		this.widthArray = widthAry;
	}
	

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public int getDraw() {
		return draw;
	}

	public void setDraw(int draw) {
		this.draw = draw;
	}

	public String[] getClickArray() {
		return clickArray;
	}

	public void setClickArray(String[] clickArray) {
		this.clickArray = clickArray;
	}

	public int[] getResArray() {
		return resArray;
	}

	public void setResArray(int[] resArray) {
		this.resArray = resArray;
	}
	@Deprecated
	public boolean getIsbotline() {
		return isbotline;
	}
	@Deprecated
	public void setIsbotline(boolean isbotline) {
		this.isbotline = isbotline;
	}
	@Deprecated
	public boolean getIstopline() {
		return istopline;
	}
	@Deprecated
	public void setIstopline(boolean istopline) {
		this.istopline = istopline;
	}

	public int getBgcolorId() {
		return bgColorId;
	}

	public void setBgcolorId(int bgcolorId) {
		this.bgColorId = bgcolorId;
	}

	public String[] getTextArray() {
		return textArray;
	}

	public void setTextArray(String[] textArray) {
		this.textArray = textArray;
	}

	public int getBottompadding() {
		return bottompadding;
	}

	public void setBottompadding(int bottompadding) {
		this.bottompadding = bottompadding;
	}
	public boolean isBotCell() {
		return isBotCell;
	}

	public void setBotCell(boolean isBotCell) {
		this.isBotCell = isBotCell;
	}
	public int[] getWidthArray() {
		return widthArray;
	}

	public void setWidthArray(int[] widthArray) {
		this.widthArray = widthArray;
	}
	public int getContentColorId() {
		return contentColorId;
	}
	public void setContentColorId(int contentColorId) {
		this.contentColorId = contentColorId;
	}
	public int getTitleSizeDelta() {
		return titleSizeDelta;
	}
	public void setTitleSizeDelta(int titleSizeDelta) {
		this.titleSizeDelta = titleSizeDelta;
	}
	public int getContentSizeDelta() {
		return contentSizeDelta;
	}
	public void setContentSizeDelta(int contentSizeDelta) {
		this.contentSizeDelta = contentSizeDelta;
	}
	public boolean isExpandText() {
		return expandText;
	}
	public void setExpandText(boolean expandText) {
		this.expandText = expandText;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public int getBgColorId() {
		return bgColorId;
	}
	public void setBgColorId(int bgColorId) {
		this.bgColorId = bgColorId;
	}
	public int getTitleColorId() {
		return titleColorId;
	}
	public void setTitleColorId(int titleColorId) {
		this.titleColorId = titleColorId;
	}
	public boolean isArrowShown() {
		return isArrowShown;
	}
	public void setArrowShown(boolean isArrowShown) {
		this.isArrowShown = isArrowShown;
	}
	public int getTitleTypeface() {
		return titleTypeface;
	}
	public void setTitleTypeface(int titleTypeface) {
		this.titleTypeface = titleTypeface;
	}
	public void setMargin(int margin) {
		this.leftMargin = margin;
		this.rightMargin = margin;
		this.topMargin = margin;
		this.botMargin = margin;
	}
	public int getLeftMargin() {
		return leftMargin;
	}
	public void setLeftMargin(int leftMargin) {
		this.leftMargin = leftMargin;
	}
	public int getRightMargin() {
		return rightMargin;
	}
	public void setRightMargin(int rightMargin) {
		this.rightMargin = rightMargin;
	}
	public int getTopMargin() {
		return topMargin;
	}
	public void setTopMargin(int topMargin) {
		this.topMargin = topMargin;
	}
	public int getBotMargin() {
		return botMargin;
	}
	public void setBotMargin(int botMargin) {
		this.botMargin = botMargin;
	}
	public void setPadding(int padding) {
		this.leftPadding = padding;
		this.rightPadding = padding;
		this.topPadding = padding;
		this.botPadding = padding;
	}
	public int getLeftPadding() {
		return leftPadding;
	}
	public void setLeftPadding(int leftPadding) {
		this.leftPadding = leftPadding;
	}
	public int getRightPadding() {
		return rightPadding;
	}
	public void setRightPadding(int rightPadding) {
		this.rightPadding = rightPadding;
	}
	public int getTopPadding() {
		return topPadding;
	}
	public void setTopPadding(int topPadding) {
		this.topPadding = topPadding;
	}
	public int getBotPadding() {
		return botPadding;
	}
	public void setBotPadding(int botPadding) {
		this.botPadding = botPadding;
	}
	public int[] getImageArray() {
		return imageArray;
	}
	public void setImageArray(int[] imageArray) {
		this.imageArray = imageArray;
	}
	public int getCellHeight() {
		return cellHeight;
	}
	public void setCellHeight(int cellHeight) {
		this.cellHeight = cellHeight;
	}
	public boolean isTaller() {
		return isTaller;
	}
	public void setTaller(boolean isTaller) {
		this.isTaller = isTaller;
	}

	public List<Bill> getBillList() {
		return billList;
	}
}