package com.pccw.myhkt.mymob.fragment;

import android.app.Activity;

import androidx.annotation.NonNull;

import com.pccw.dango.shared.cra.AddOnCra;
import com.pccw.dango.shared.entity.SubnRec;
import com.pccw.myhkt.APIsResponse;
import com.pccw.myhkt.fragment.BaseFragment;
import com.pccw.myhkt.fragment.BaseServiceFragment.OnLiveChatListener;
import com.pccw.myhkt.model.AcctAgent;
import com.pccw.myhkt.mymob.model.EditCra;

public class BaseMyMobFragment extends BaseFragment {
	// CallBack
	protected OnMyMobListener		callback_main;

	@Override
	public void onAttach(@NonNull Activity activity) {
		super.onAttach(activity);

		try {
			callback_main = (OnMyMobListener) activity;
		} catch (ClassCastException e) {
			throw new ClassCastException(activity.toString() + " must implement OnMyMobListener");
		}
	}
	
	//Interface
	public interface OnMyMobListener {
		public void 	setActiveSubview(int index);
		public int 	getActiveSubview();
		public void 	displaySubview();
		public void 	popBackStack();
		public void 	fragmentBack();
		public void 	onActivityBackPress();
		public boolean isEmptyList();
		public void setAutoLogin(boolean isAutoLogin);
		public boolean isAutoLogin();
		public void setSelectedAcctAgent(AcctAgent acctAgent);
		public AcctAgent getSelectedAcctAgent();
		public AddOnCra getAddOnCra();
		public void setAddOnCra(AddOnCra addOnCra);
		public EditCra getEditCra();
		public void setEditCra(EditCra editCra);
		public boolean isEditMode();
		public void setEditMode(boolean isEditMode);
		public AcctAgent getRecallAcctAgent();

		public void setRecallAcctAgent(AcctAgent recallAcctAgent);

		public boolean isSavePwd();

		public void setSavePwd(boolean isSavePwd);

		public boolean isPwdDlgIsSavePwd();

		public void setPwdDlgIsSavePwd(boolean pwdDlgIsSavePwd);

		public String getPwdDlgPwd();

		public void setPwdDlgPwd(String pwdDlgPwd);

		public boolean isPwHintUpdated();

		public void setPwHintUpdated(boolean isPwHintUpdated);
		public String getActionTy();
		public void setActionTy(String actionTy);
		public SubnRec getIoSubscriptionRec();

		public void setIoSubscriptionRec(SubnRec ioSubscriptionRec);
		public void killActivity();
		
		public void hideLiveChatIcon();
		public void showLiveChatIcon();
	}
	
	@Override
	public void onSuccess(APIsResponse response) throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onFail(APIsResponse response) throws Exception {
		// TODO Auto-generated method stub
		
	}

}
