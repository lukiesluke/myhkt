package com.pccw.myhkt.fragment;

import android.app.Activity;
import android.os.Bundle;

import androidx.annotation.NonNull;

import com.pccw.dango.shared.entity.ShopRec;
import com.pccw.myhkt.APIsResponse;

public class BaseShopFragment extends BaseFragment{
		// CallBack
		protected OnShopListener		callback_main;
		
		@Override
		public void onAttach(@NonNull Activity activity) {
			super.onAttach(activity);

			try {
				callback_main = (OnShopListener) activity;
			} catch (ClassCastException e) {
				throw new ClassCastException(activity.toString() + " must implement OnShopListener");
			}
		}
		
		@Override
		public void onActivityCreated(Bundle savedInstanceState) {
			super.onActivityCreated(savedInstanceState);
			
		}

		//Interface
		public interface OnShopListener {
			public int getActiveSubview();
			public void setActiveSubview(int activeSubview);
			public void displaySubview();
			public void popBackStack();
			public int getShopType();
			public void setShopType(int shopType);
			public ShopRec[] getShopRecAry();
			public void setShopRecAry(ShopRec[] shopRecAry);
			public ShopRec getShopRec();
			public void setShopRec(ShopRec shopRec);
			public boolean isPause();
			public void setPause(boolean isPause);
		}
		
		public void initPonBanner() {
			
		}
		
		@Override
		public void onSuccess(APIsResponse response) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void onFail(APIsResponse response) {
			// TODO Auto-generated method stub
			
		}

	}

