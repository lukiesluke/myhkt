package com.pccw.dango.shared.g3entity;

import java.io.Serializable;

public class G3BoostUpOffer1DTO implements Serializable 
{
    private static final long serialVersionUID = -6259318186594124130L;
    
    private String boostUpOfferId;
	private String rechargeValue;
	private String rechargeValueInMB;
	private String rechargeAmount;
	private String rechargeAmountDisplay;
	private String engDescription;
	private String chiDescription;
	private String displayDescription;
	private String quotaName;
	private String valPeriod;
	
	public String getBoostUpOfferId() {
		return boostUpOfferId;
	}
	public void setBoostUpOfferId(String boostUpOfferId) {
		this.boostUpOfferId = boostUpOfferId;
	}
	public String getRechargeValue() {
		return rechargeValue;
	}
	public void setRechargeValue(String rechargeValue) {
		this.rechargeValue = rechargeValue;
	}
	public String getRechargeAmount() {
		return rechargeAmount;
	}
	public String getQuotaName() {
		return quotaName;
	}
	public void setQuotaName(String quotaName) {
		this.quotaName = quotaName;
	}
	public String getValPeriod() {
		return valPeriod;
	}
	public void setValPeriod(String valPeriod) {
		this.valPeriod = valPeriod;
	}
	public void setRechargeAmount(String rechargeAmount) {
		this.rechargeAmount = rechargeAmount;
	}
	public String getEngDescription() {
		return engDescription;
	}
	public void setEngDescription(String engDescription) {
		this.engDescription = engDescription;
	}
	public String getChiDescription() {
		return chiDescription;
	}
	public void setChiDescription(String chiDescription) {
		this.chiDescription = chiDescription;
	}
	public String getRechargeValueInMB() {
		return rechargeValueInMB;
	}
	public void setRechargeValueInMB(String rechargeValueInMB) {
		this.rechargeValueInMB = rechargeValueInMB;
	}
	public String getRechargeAmountDisplay() {
		return rechargeAmountDisplay;
	}
	public void setRechargeAmountDisplay(String rechargeAmountDisplay) {
		this.rechargeAmountDisplay = rechargeAmountDisplay;
	}
	public String getDisplayDescription() {
		return displayDescription;
	}
	public void setDisplayDescription(String displayDescription) {
		this.displayDescription = displayDescription;
	}

}
