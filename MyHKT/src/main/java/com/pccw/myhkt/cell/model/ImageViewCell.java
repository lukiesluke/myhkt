package com.pccw.myhkt.cell.model;

import com.pccw.myhkt.R;

import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.View.OnClickListener;

public class ImageViewCell extends Cell{
	
	public String url;	
	private OnClickListener onClickListener = null;
	
	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public ImageViewCell(int draw, String url, String[] clickArray) {
		super();
		//Default setting
		type = Cell.IMAGEVIEW;
	
		this.draw = draw;
		this.url = url;
		this.clickArray = clickArray;	
	}

	public ImageViewCell(Drawable drawable) {
		super();
		type = Cell.IMAGE_VIEW_DRAWABLE;
		this.drawable = drawable;
	}
	
	public OnClickListener getOnClickListener() {
		return onClickListener;
	}
	public void setOnClickListener(OnClickListener onClickListener) {
		this.onClickListener = onClickListener;
	}
}