package com.pccw.myhkt.activity;

import android.content.Context;
import android.content.SharedPreferences;
import androidx.test.espresso.intent.Intents;
import androidx.test.rule.ActivityTestRule;
import androidx.test.ext.junit.runners.AndroidJUnit4;
import android.view.View;

import com.pccw.myhkt.AccountHelper;
import com.pccw.myhkt.ClnEnv;
import com.pccw.myhkt.R;
import com.pccw.myhkt.base.BaseTest;
import com.pccw.myhkt.mymob.activity.MyMobileActivity;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.Calendar;

import static androidx.test.platform.app.InstrumentationRegistry.getInstrumentation;

@Ignore
@RunWith(AndroidJUnit4.class)
public class PremiumDashboardActivityTest extends BaseTest {
    private Context context;
    private SharedPreferences shared;

    @Rule
    public ActivityTestRule<MainMenuActivity> activityTestRule =
            new ActivityTestRule<>(MainMenuActivity.class);

    @Before
    public void setUp() {
        context = getInstrumentation().getTargetContext();
        shared = context.getSharedPreferences(
                "myhkt",
                Context.MODE_PRIVATE);
        if(ClnEnv.isLoggedIn()){
            executeLogout(context);
        }
        Intents.init();

        clickLeftNavBar();
        clickString(context, R.string.myhkt_LGIF_LOGIN);
    }

    @After
    public void tearDown(){
        if(ClnEnv.isLoggedIn()){
            executeLogout(context);
        }
        Intents.release();
    }

    @Test
    public void testPremiumDisplayUI(){
        loginMyCredentialsUAT(AccountHelper.getUATAccount6().getUsername(), AccountHelper.getUATAccount6().getPassword(), shared);
        checkIntendedActivity(MainMenuActivity.class.getName());
        checkStringIsDisplayed(context, R.string.app_name);
        checkViewVisibility(R.id.navbar_button_left,
                R.id.navbar_base_layout, View.VISIBLE);
        checkViewVisibility(R.id.navbar_message_icon,
                R.id.navbar_base_layout, View.VISIBLE);
        checkViewVisibility(R.id.mainmenu_hktslogun_line_1, View.VISIBLE);
        checkViewVisibility(R.id.mainmenu_hktslogun_line_2, View.VISIBLE);
        checkViewVisibility(R.id.mainmenu_hktslogun_line_3, View.VISIBLE);
        checkViewVisibility(R.id.mainmenu_btn_info, View.VISIBLE);
        checkViewVisibility(R.id.mainmenu_pccwlogo, View.VISIBLE);
        checkViewVisibility(R.id.mainmenu_pccwlogo1, View.VISIBLE);
        checkViewStartsWith(R.id.mainmenu_label_welcome, determineGreetingTime());
        checkViewVisibility(R.id.mainmenu_gridlayout, View.VISIBLE);
    }

    @Test
    public void testPremiumMyAccount() {
        loginMyCredentialsUAT(AccountHelper.getUATAccount6().getUsername(), AccountHelper.getUATAccount6().getPassword(), shared);
        executeMainMenu(0, ServiceListActivity.class.getName());
    }

    @Test
    public void testPremiumMyLineTest() {
        loginMyCredentialsUAT(AccountHelper.getUATAccount6().getUsername(), AccountHelper.getUATAccount6().getPassword(), shared);
        executeMainMenu(1, ServiceListActivity.class.getName());
    }

    @Test
    public void testPremiumMyAppointment() {
        loginMyCredentialsUAT(AccountHelper.getUATAccount6().getUsername(), AccountHelper.getUATAccount6().getPassword(), shared);
        gotoMainMenuItem(2);
        // TODO: Remove this code if using a PRD premier account
        delay(TestTimeouts.API_TIMEOUTS,waitForText(btn_OK));
        clickOK();
        // TODO: remove until here
        checkIntendedActivity(MyApptActivity.class.getName());
    }

    @Test
    public void testPremiumProfileSettings() {
        loginMyCredentialsUAT(AccountHelper.getUATAccount6().getUsername(), AccountHelper.getUATAccount6().getPassword(), shared);
        executeMainMenu(3, MyProfileActivity.class.getName());
    }

    @Test
    public void testPremiumMyMobile() {
        loginMyCredentialsUAT(AccountHelper.getUATAccount6().getUsername(), AccountHelper.getUATAccount6().getPassword(), shared);
        executeMainMenu(4, MyMobileActivity.class.getName());
    }

    @Test
    public void testPremiumPremierHomeSolutions() {
        loginMyCredentialsUAT(AccountHelper.getUATAccount6().getUsername(), AccountHelper.getUATAccount6().getPassword(), shared);
        executeMainMenu(5, BrowserActivity.class.getName());
    }

//    @Test
//    public void testPremiumPremierNews() {
//        loginMyCredentialsUAT(AccountHelper.getUATAccount6().getUsername(), AccountHelper.getUATAccount6().getPassword(), shared);
//        // NOTE: this button forwards to a browser
//    }

    @Test
    public void testPremiumTheClub() {
        loginMyCredentialsUAT(AccountHelper.getUATAccount6().getUsername(), AccountHelper.getUATAccount6().getPassword(), shared);
        gotoMainMenuItem(7);
        clickCANCEL();
//        checkStringIsDisplayed("OK").perform(click());
//        intended(allOf(
//                hasAction(Intent.ACTION_VIEW),
//                hasData(Uri.parse("http://play.google.com/store/apps/details?id=com.pccw.theclub"))
//        ));
//        UiDevice.getInstance(getInstrumentation()).pressBack();
    }

    @Test
    public void testPremiumTapNGo() {
        loginMyCredentialsUAT(AccountHelper.getUATAccount6().getUsername(), AccountHelper.getUATAccount6().getPassword(), shared);
        gotoMainMenuItem(8);
        clickCANCEL();
//        checkStringIsDisplayed("OK").perform(click());
//        intended(allOf(
//                hasAction(Intent.ACTION_VIEW),
//                hasData(Uri.parse("http://play.google.com/store/apps/details?id=com.hktpayment.tapngo"))
//        ));
//        UiDevice.getInstance(getInstrumentation()).pressBack();
    }

    @Test
    public void testPremiumHKTShop() {
        loginMyCredentialsUAT(AccountHelper.getUATAccount6().getUsername(), AccountHelper.getUATAccount6().getPassword(), shared);
        gotoMainMenuItem(9);
        clickCANCEL();
//        checkStringIsDisplayed("OK").perform(click());
//        intended(allOf(
//                hasAction(Intent.ACTION_VIEW),
//                hasData(Uri.parse("http://play.google.com/store/apps/details?id=com.hkt.android.hktshop"))
//        ));
//        UiDevice.getInstance(getInstrumentation()).pressBack();
    }

    @Test
    public void testPremiumDirectoryInquiries() {
        loginMyCredentialsUAT(AccountHelper.getUATAccount6().getUsername(), AccountHelper.getUATAccount6().getPassword(), shared);
        gotoMainMenuItem(10);
        checkIntendedActivity(DirectoryInquiryActivity.class.getName());
        delay(TestTimeouts.API_TIMEOUTS);
    }

    @Test
    public void testPremiumIDD0060() {
        loginMyCredentialsUAT(AccountHelper.getUATAccount6().getUsername(), AccountHelper.getUATAccount6().getPassword(), shared);
        executeMainMenu(11, IDDActivity.class.getName());
    }

    @Test
    public void testPremiumShops() {
        loginMyCredentialsUAT(AccountHelper.getUATAccount6().getUsername(), AccountHelper.getUATAccount6().getPassword(), shared);
        executeMainMenu(12, ShopActivity.class.getName());
    }

    @Test
    public void testPremiumContactUs()  {
        loginMyCredentialsUAT(AccountHelper.getUATAccount6().getUsername(), AccountHelper.getUATAccount6().getPassword(), shared);
        executeMainMenu(13, ContactUsNewActivity.class.getName());
    }

    @Test
    public void testPremiumBannerClick()  {
        loginMyCredentialsUAT(AccountHelper.getUATAccount6().getUsername(), AccountHelper.getUATAccount6().getPassword(), shared);
        clickView(R.id.mainmenu_btn_info);
        delay(TestTimeouts.LIVECHAT_TIMEOUTS);
        checkIntendedActivity(BrowserActivity.class.getName());
    }

    @Test
    public void testPremiumMyMessagesNavigation()  {
        loginMyCredentialsUAT(AccountHelper.getUATAccount6().getUsername(), AccountHelper.getUATAccount6().getPassword(), shared);
        delay(TestTimeouts.ACTIVITY_LOAD_TIMEOUTS);
        clickView(R.id.navbar_message_icon, R.id.navbar_base_layout);
        delay(TestTimeouts.API_TIMEOUTS,waitForText(context, R.string.title_inbox));
        checkIntendedActivity(InboxListActivity.class.getName());
    }

    @Test
    public void testPremiumMenuClick()  {
        loginMyCredentialsUAT(AccountHelper.getUATAccount6().getUsername(), AccountHelper.getUATAccount6().getPassword(), shared);
        clickLeftNavBar();
        checkStringIsDisplayed(context, R.string.SHLF_LOGOUT);
        checkStringIsDisplayed(context, R.string.myhkt_settings);
        checkStringIsDisplayed(context, R.string.myhkt_about);
        checkStringIsDisplayed(context, R.string.myhkt_menu_tnc);
        checkStringIsDisplayed(context, R.string.title_inbox);
    }

    @Test
    public void testPremiumMyMessagesHamburger()  {
        loginMyCredentialsUAT(AccountHelper.getUATAccount6().getUsername(), AccountHelper.getUATAccount6().getPassword(), shared);
        clickLeftNavBar();
        clickString(context, R.string.title_inbox);
    }

    @Test
    public void testPremiumSettings()  {
        loginMyCredentialsUAT(AccountHelper.getUATAccount6().getUsername(), AccountHelper.getUATAccount6().getPassword(), shared);
        clickLeftNavBar();
        clickString(context, R.string.myhkt_settings);
    }

    @Test
    public void testPremiumAboutMyHKT()  {
        loginMyCredentialsUAT(AccountHelper.getUATAccount6().getUsername(), AccountHelper.getUATAccount6().getPassword(), shared);
        clickLeftNavBar();
        clickString(context, R.string.myhkt_about);
        delay(TestTimeouts.DIALOG_TIMEOUTS,waitForText(btn_OK));
        clickOK();
    }

    @Test
    public void testPremiumTermsAndConditions()  {
        loginMyCredentialsUAT(AccountHelper.getUATAccount6().getUsername(), AccountHelper.getUATAccount6().getPassword(), shared);
        clickLeftNavBar();
        clickString(context, R.string.myhkt_menu_tnc);
        delay(TestTimeouts.DIALOG_TIMEOUTS,waitForText(btn_OK));
        clickOK();
    }

    @Test
    public void testPremiumLogout() {
        loginMyCredentialsUAT(AccountHelper.getUATAccount6().getUsername(),
                AccountHelper.getUATAccount6().getPassword(), shared);
        clickLeftNavBar();
        clickString(context, R.string.SHLF_LOGOUT);
        checkStringIsDisplayed(context, R.string.myhkt_confirmlogout);
        clickString(context, R.string.myhkt_btn_yes);
    }

    private String determineGreetingTime() {
        String greeting;
        Calendar cal = Calendar.getInstance();
        int rHr = cal.get(Calendar.HOUR_OF_DAY);
        if (rHr >= 5 && rHr < 12) {
            greeting = context.getResources().getString(R.string.SHLF_MORNING);
        } else {
            if (rHr >= 12 && rHr < 18) {
                greeting = context.getResources().getString(R.string.SHLF_AFTERNOON);
            } else {
                greeting = context.getResources().getString(R.string.SHLF_NIGHT);
            }
        }
        return greeting;
    }

    private void executeMainMenu(int position, String activityName) {
        gotoMainMenuItem(position);
        checkIntendedActivity(activityName);
        if(position == 2) {
            delay(TestTimeouts.API_TIMEOUTS);
        }
    }

}
