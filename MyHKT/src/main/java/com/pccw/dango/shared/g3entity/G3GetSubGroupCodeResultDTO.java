package com.pccw.dango.shared.g3entity;

import java.io.Serializable;

public class G3GetSubGroupCodeResultDTO implements Serializable 
{
    private static final long serialVersionUID = 3450171613677838656L;
    
    private String rtnCd;
	private String rtnMsg;
	private G3SubGroupCodeDTO g3SubGroupCodeDTO = new G3SubGroupCodeDTO();
	
	public String getRtnCd() {
		return rtnCd;
	}
	public void setRtnCd(String rtnCd) {
		this.rtnCd = rtnCd;
	}
	public String getRtnMsg() {
		return rtnMsg;
	}
	public void setRtnMsg(String rtnMsg) {
		this.rtnMsg = rtnMsg;
	}
	public G3SubGroupCodeDTO getG3SubGroupCodeDTO() {
		return g3SubGroupCodeDTO;
	}
	public void setG3SubGroupCodeDTO(G3SubGroupCodeDTO g3SubGroupCodeDTO) {
		this.g3SubGroupCodeDTO = g3SubGroupCodeDTO;
	}
	
}
