/*
    Class for Messages (to be shown in GWT)

    This class holds the TAG to be mapped for the 
    message display thru the Lightbox or Windows.alert().
    
    Keywords
    --------
    $URL: svn://10.87.120.207/dango/rel/v16.0/src/com/pccw/dango/shared/tool/Msgs.java $
    $Rev: 1123 $
    $Date: 2018-07-17 15:12:03 +0800 (¶g¤G, 17 ¤C¤ë 2018) $
    $Author: alpha.lau $
*/

package com.pccw.dango.shared.tool;

import java.io.Serializable;

import com.pccw.wheat.shared.tool.GwtTool;




public class Msgs implements Serializable
{
    private static final long serialVersionUID = 8514378236618068374L;
    
    /* Buttons */
    public static final String      CLOSE                   = "BTN_CLOSE";                  /* Close                                    */
    public static final String      CONFIRM                 = "BTN_CONFIRM";                /* Confirm                                  */
    public static final String      CANCEL                  = "BTN_CANCEL";                 /* Cancel                                   */
    public static final String      PROCEED                 = "BTN_PROCEED";                /* Proceed                                  */
    public static final String      SUBMIT                  = "BTN_SUBMIT";                 /* Submit                                   */
    public static final String      NEXT                    = "BTN_NEXT";                   /* Next                                     */
    public static final String      VERIFY                  = "BTN_VERIFY";                 /* Verify                                   */
    public static final String      UPDATE                  = "BTN_UPDATE";                 /* Update                                   */
    public static final String      EDIT                    = "BTN_EDIT";                   /* Edit                                     */
    public static final String      RETURN                  = "BTN_RETURN";                 /* Return                                   */
    
    
    /* Text assigned during runtime/dynamic */
    public static final String      PLS_SELECT              = "D_PLS_SELECT";               /* Please Select                            */
    public static final String      LOB_LTS                 = "D_LOB_LTS";                  /* Fixed Network                            */
    public static final String      LOB_LTS_TEL             = "D_LOB_LTS_TEL";              /* Fixed Network (for Comm)                 */
    public static final String      LOB_LTS_ONE             = "D_LOB_LTS_ONE";              /* Fixed Network & One (for Comm)           */
    public static final String      LOB_PCD                 = "D_LOB_PCD";                  /* PCD                                      */
    public static final String      LOB_IMS                 = "D_LOB_IMS";                  /* IMS (for Comm)                           */
    public static final String      LOB_TV                  = "D_LOB_TV";                   /* TV                                       */
    public static final String      LOB_MOB                 = "D_LOB_MOB";                  /* Mobile (H-SIM)                           */
    public static final String      LOB_IOI                 = "D_LOB_IOI";                  /* 1O1O (Migrated to H-SIM)                 */
    public static final String      LOB_NE                  = "D_LOB_NE";                   /* NE                                       */
    public static final String      LOB_O2F                 = "D_LOB_O2F";                  /* O2F (C-SIM)                              */
    public static final String      LOB_101                 = "D_LOB_101";                  /* 1O1O (C-SIM)                             */
    public static final String      XLOB_CSL                = "D_XLOB_CSL";                 /* Wildcard LOB (MOB + O2F)                 */
    public static final String      XLOB_101                = "D_XLOB_101";                 /* Wildcard LOB (101 + IOI)                 */

    public static final String      SVC_NUM                 = "D_SVC_NUM";                  /* Service Number                           */
    public static final String      SN_DESN_4LTS            = "D_SN_DESN_4LTS";             /* Service Number Description               */
    public static final String      SN_DESN_4PCD            = "D_SN_DESN_4PCD";
    public static final String      SN_DESN_4IMS            = "D_SN_DESN_4IMS";
    public static final String      SN_DESN_4TV             = "D_SN_DESN_4TV";
    public static final String      SN_DESN_4CSL            = "D_SN_DESN_4CSL";
    public static final String      SN_DESN_4NE             = "D_SN_DESN_4NE";
    public static final String      SN_DESN_4101            = "D_SN_DESN_4101";
    public static final String      SN_DESN_4LTS_TEL        = "D_SN_DESN_4LTS_TEL"; 
    public static final String      SN_DESN_4LTS_MOB        = "D_SN_DESN_4LTS_MOB";
    public static final String      SN_DESN_4LTS_ITS        = "D_SN_DESN_4LTS_ITS";
    public static final String      SN_DESN_4LTS_ONC        = "D_SN_DESN_4LTS_ONC";
    public static final String      SN_DESN_4LTS_ICF        = "D_SN_DESN_4LTS_ICF";
    
    
    public static final String      SN_BRIEF_4LTS           = "D_SN_BRIEF_4LTS";            /* Service Number Brief                     */
    public static final String      SN_BRIEF_4PCD           = "D_SN_BRIEF_4PCD";
    public static final String      SN_BRIEF_4IMS           = "D_SN_BRIEF_4IMS";
    public static final String      SN_BRIEF_4TV            = "D_SN_BRIEF_4TV";
    public static final String      SN_BRIEF_4CSL           = "D_SN_BRIEF_4CSL";
    public static final String      SN_BRIEF_4NE            = "D_SN_BRIEF_4NE";
    public static final String      SN_BRIEF_4101           = "D_SN_BRIEF_4101";
    
    public static final String      INDUST_OPT_ASSOC        = "D_INDUST_OPT_ASSOC";         /* Industry (for Comm)                      */ 
    public static final String      INDUST_OPT_BANK         = "D_INDUST_OPT_BANK";
    public static final String      INDUST_OPT_PROF_SRVCS   = "D_INDUST_OPT_PROF_SRVCS";
    public static final String      INDUST_OPT_CONSTRCT     = "D_INDUST_OPT_CONSTRCT";
    public static final String      INDUST_OPT_EDU_SRVCS    = "D_INDUST_OPT_EDU_SRVCS";
    public static final String      INDUST_OPT_INVSTMNT     = "D_INDUST_OPT_INVSTMNT";
    public static final String      INDUST_OPT_GOVT         = "D_INDUST_OPT_GOVT";
    public static final String      INDUST_OPT_HEALTH       = "D_INDUST_OPT_HEALTH";
    public static final String      INDUST_OPT_HOTEL        = "D_INDUST_OPT_HOTEL";
    public static final String      INDUST_OPT_INSURNCE     = "D_INDUST_OPT_INSURNCE";
    public static final String      INDUST_OPT_MANUF        = "D_INDUST_OPT_MANUF";
    public static final String      INDUST_OPT_PERSNL_SRVS  = "D_INDUST_OPT_PERSNL_SRVS";
    public static final String      INDUST_OPT_REAL_ESTATE  = "D_INDUST_OPT_REAL_ESTATE";
    public static final String      INDUST_OPT_RESTRNT      = "D_INDUST_OPT_RESTRNT";
    public static final String      INDUST_OPT_RETAIL       = "D_INDUST_OPT_RETAIL";
    public static final String      INDUST_OPT_TELECOM      = "D_INDUST_OPT_TELECOM";
    public static final String      INDUST_OPT_TRADE        = "D_INDUST_OPT_TRADE";
    public static final String      INDUST_OPT_TRANSPO_LOGC = "D_INDUST_OPT_TRANSPO_LOGC";
    public static final String      INDUST_OPT_TRAVEL_AGNCY = "D_INDUST_OPT_TRAVEL_AGNCY";
    public static final String      INDUST_OPT_UTILITY      = "D_INDUST_OPT_UTILITY";
    public static final String      INDUST_OPT_WHOLSALE     = "D_INDUST_OPT_WHOLSALE";
    public static final String      INDUST_OPT_OTHRS        = "D_INDUST_OPT_OTHRS";
    
    public static final String      RECALL_RCPT_EMAIL       = "D_RECALL_RCPT_EMAIL";        /* Recall Masked Email Rcpt                 */
    public static final String      RECALL_RCPT_MOB         = "D_RECALL_RCPT_MOB";          /* Recall Masked Mob Rcpt                   */
    public static final String      RECALL_LGI_ARRANGE      = "D_RECALL_LGI_ARRANGE";       /* Recall Login Arrangement                 */
    public static final String      RECALL_PWD_ARRANGE      = "D_RECALL_PWD_ARRANGE";       /* Recall Password Arrangement              */
    public static final String      CONTACT_HELP            = "D_CONTACT_HELP";             /* Contact Help                             */
    
    public static final String      ACCT_NUM                = "D_ACCT_NUM";
    public static final String      ACCT_NO                 = "D_ACCT_NO";
    
    public static final String      ZOMBIE_DESN             = "D_ZOMBIE_DESN";              /* Description of Zombie Account            */             
    
                                                                                            /* for Service Number Presentation          */
    public static final String      CALLING_CARD_PFX        = "D_CALLING_CARD_PFX";         /*   Prefix of Calling Card                 */
    
                                                                                            /* In Main Menu:                            */
    public static final String      ALL_ACCTS               = "D_ALL_ACCTS";                /*   All Accounts                           */
    public static final String      OPT_VWBILL              = "D_OPT_VWBILL";               /*   Option - View Bill                     */
    public static final String      OPT_MYPLAN              = "D_OPT_MYPLAN";               /*   Option - My Plan                       */
    public static final String      OPT_LNTEST              = "D_OPT_LNTEST";               /*   Option - Line Test                     */               
    public static final String      OPT_RBTMDM              = "D_OPT_RBTMDM";               /*   Option - Reboot Modem                  */
    public static final String      OPT_SUPP                = "D_OPT_SUPP";                 /*   Option - Support                       */
    public static final String      OPT_BIINFO              = "D_OPT_BIINFO";               /*   Option - Billing Info (for Csum)       */
    public static final String      OPT_ENQ_BIINFO          = "D_OPT_ENQ_BIINFO";           /*   Option - Enq Billing Info (for Comm)   */
    
    public static final String      PCD_PONCHK              = "D_PCD_PONCHK";               /* PON Coverage Check for PCD               */
    public static final String      TV_PONCHK               = "D_TV_PONCHK";                /* PON Coverage Check for TV                */
    public static final String      PCD_NO_CMMT             = "D_PCD_NO_CMMT";              /* No commitment for PCD                    */ 
    public static final String      PCD_HIDE_PLAN           = "D_PCD_HIDE_PLAN";            /* Hide Plan Info for PCD                   */
    public static final String      PCD_NO_VAS              = "D_PCD_NO_VAS";               /* No VAS for PCD                           */
    public static final String      PCD_NO_INCT             = "D_PCD_NO_INCT";              /* No incentive for PCD                     */
    public static final String      TV_NO_SRV_PLAN          = "D_TV_NO_SRV_PLAN";           /* No Service Plan for TV                   */
    public static final String      TV_NO_VAS               = "D_TV_NO_VAS";                /* No VAS for TV                            */
    public static final String      LTS_NO_CALL_PLAN        = "D_LTS_NO_CALL_PLAN";         /* No Call Plan for LTS                     */
    public static final String      LTS_CALL_PLAN_ENQ       = "D_LTS_CALL_PLAN_ENQ";        /* Call Plan Enquiry for LTS                */
    public static final String      LTS_CALL_PLAN_HDR       = "D_LTS_CALL_PLAN_HDR";        /* Call Plan Enquiry for LTS                */
    public static final String      LTS_NO_TERM_PLAN        = "D_LTS_NO_TERM_PLAN";         /* No Term Plan for LTS                     */
    public static final String      LTS_TP_LT3MTH           = "D_LTS_TP_LT3MTH";            /* Term Plan less than 3 mths               */
    public static final String      LTS_CP_LT3MTH           = "D_LTS_CP_LT3MTH";            /* Call Plan less than 3 mths               */
    
    public static final String      LBL_TV_CHL              = "D_LBL_TV_CHL";               /* Label - Channel                          */
    public static final String      LBL_CMMNCMNT_DATE       = "D_LBL_CMMNCMNT_DATE";        /* Label - Commencement Date                */
    public static final String      LBL_END_DATE            = "D_LBL_END_DATE";             /* Label - End date                         */
    public static final String      LBL_CMTMNT_PRD          = "D_LBL_CMTMNT_PRD";           /* Label - commitment period                */
    public static final String      LBL_TV_VAS_DESN         = "D_LBL_TV_VAS_DESN";          /* Label - TV VAS description               */
    public static final String      LBL_TV_VAS_START_DATE   = "D_LBL_TV_VAS_START_DATE";    /* Label - TV VAS Start Date                */
    public static final String      LBL_TV_VAS_END_DATE     = "D_LBL_TV_VAS_END_DATE";      /* Label - TV VAS End Date                  */
    public static final String      LBL_MNTHLY_RATE         = "D_LBL_MNTHLY_RATE";          /* Label - monthly rate                     */
    public static final String      LBL_MNTHLY_RATE_OVAS    = "D_LBL_MNTHLY_RATE_OVAS";     /* Label - monthly rate (for Optional VAS   */
    public static final String      LBL_ENTTLMNT_MNTH       = "D_LBL_ENTTLMNT_MNTH";        /* Label - entitlement                      */
    public static final String      LBL_MNTHLY_RATE_WN_CMNT_PRD
                                                            = "D_LBL_MNTHLY_RATE_WN_CMNT_PRD";
    public static final String      LBL_USAGE_MNTH          = "D_LBL_USAGE_MNTH";           /* Label - usage                            */
    public static final String      LBL_TTLMR               = "D_LBL_TTLMR";                /* Label - Mr.                              */
    public static final String      LBL_TTLMRS              = "D_LBL_TTLMRS";               /* Label - Mrs.                             */
    public static final String      LBL_TTLMS               = "D_LBL_TTLMS";                /* Label - Ms.                              */
    public static final String      LBL_ON                  = "D_LBL_ON";                   /* Label - On                               */
    public static final String      LBL_OFF                 = "D_LBL_OFF";                  /* Label - Off                              */
    
    public static final String      UNT_MTH                 = "D_UNT_MTH";                  /* Unit - Months                            */
    public static final String      UNT_MIN                 = "D_UNT_MIN";                  /* Unit - Mintues                           */
    public static final String      VAL_UNLIMIT             = "D_VAL_UNLIMIT";              /* Val - Unlimit                            */
    
    public static final String      MYBIL_AMNT_DUE_LTS      = "D_MYBIL_AMNT_DUE_LTS";
    public static final String      MYBIL_AMNT_DUE_MOB      = "D_MYBIL_AMNT_DUE_MOB";         
    public static final String      MYBIL_AMNT_DUE_O2F      = "D_MYBIL_AMNT_DUE_O2F";         
    public static final String      MYBIL_AMNT_DUE_101      = "D_MYBIL_AMNT_DUE_101";         
    public static final String      MYBIL_AMNT_DUE_IOI      = "D_MYBIL_AMNT_DUE_IOI";         
    public static final String      MYBIL_AMNT_DUE_PCD      = "D_MYBIL_AMNT_DUE_PCD";         
    public static final String      MYBIL_AMNT_DUE_TV       = "D_MYBIL_AMNT_DUE_TV";         
    public static final String      MYBIL_AMNT_DUE_IMS      = "D_MYBIL_AMNT_DUE_IMS";
    
    
    /* Images */
    public static final String      IMG_LTS                 = "IMG_LTS";
    public static final String      IMG_LTS_ONE             = "IMG_LTS_ONE";
    public static final String      IMG_LTS_W0060           = "IMG_LTS_W0060";
    public static final String      IMG_TEL                 = "IMG_TEL";
    public static final String      IMG_EYE                 = "IMG_EYE";
    public static final String      IMG_PCD                 = "IMG_PCD";
    public static final String      IMG_IMS                 = "IMG_IMS";
    public static final String      IMG_TV                  = "IMG_TV";
    public static final String      IMG_CSL                 = "IMG_CSL";
    public static final String      IMG_101                 = "IMG_101";
    public static final String      IMG_CSL_101             = "IMG_CSL_101";
    public static final String      IMG_NE                  = "IMG_NE";
    public static final String      IMG_PCD_TV              = "IMG_PCD_TV";

    public static final String      IMG_DVCE_PCD            = "IMG_DVCE_PCD";
    public static final String      IMG_DVCE_TV             = "IMG_DVCE_TV";

    /* Inbox */
    public static final String      IMG_INBX_CAT            = "IMG_INBX_CAT_%s";
    public static final String      IMG_INBX_ARROW          = "IMG_INBX_ARROW";

    
    
    /* Images and Anchors for Banner */
    public static final String      IMG_CSUM_BANNER         = "IMG_CSUM_BANNER";
    public static final String      IMG_COMM_BANNER         = "IMG_COMM_BANNER";

    public static final String      CSUM_BANNER_URL         = "D_CSUM_BANNER_URL";
    public static final String      COMM_BANNER_URL         = "D_COMM_BANNER_URL";
    

    /* Support */
    public static final String      SUPP_URL_LTS            = "D_SUPP_URL_LTS";             /* Support URL for LTS                      */
    public static final String      SUPP_URL_LTS_4COMM      = "D_SUPP_URL_LTS_4COMM";       /* Support URL for LTS (for Comm)           */
    public static final String      SUPP_URL_PCD            = "D_SUPP_URL_PCD";             /* Support URL for PCD                      */
    public static final String      SUPP_URL_PCD_4COMM      = "D_SUPP_URL_PCD_4COMM";       /* Support URL for PCD (for Comm)           */
    public static final String      SUPP_URL_TV             = "D_SUPP_URL_TV";              /* Support URL for TV                       */
    public static final String      SUPP_URL_MOB            = "D_SUPP_URL_MOB";             /* Support URL for MOB                      */
    public static final String      SUPP_URL_101            = "D_SUPP_URL_101";             /* Support URL for 1010                     */
    

    /* Show PPS for LTS */
    public static final String      PPS_URL                 = "D_PPS_URL";                  /* PPS URL                                  */
    
    
    /* Payment Method */
    public static final String      PAYMTHD_URL_LTS         = "D_PAYMTHD_URL_LTS";           /* Payment Method URL for LTS               */
    public static final String      PAYMTHD_URL_PCD         = "D_PAYMTHD_URL_PCD";           /* Payment Method URL for PCD               */
    public static final String      PAYMTHD_URL_TV          = "D_PAYMTHD_URL_TV";            /* Payment Method URL for TV                */
    public static final String      PAYMTHD_URL_MOB         = "D_PAYMTHD_URL_MOB";           /* Payment Method URL for MOB               */
    public static final String      PAYMTHD_URL_IOI         = "D_PAYMTHD_URL_IOI";           /* Payment Method URL for IOI               */
    public static final String      PAYMTHD_URL_O2F         = "D_PAYMTHD_URL_O2F";           /* Payment Method URL for O2F               */
    public static final String      PAYMTHD_URL_101         = "D_PAYMTHD_URL_101";           /* Payment Method URL for 1010              */
    
    
    /* Payment Note */
    public static final String      PAYNOTE_LTS             = "D_PAYNOTE_LTS";               /* Payment Note for LTS                     */
    public static final String      PAYNOTE_PCD             = "D_PAYNOTE_PCD";               /* Payment Note for PCD                     */
    public static final String      PAYNOTE_TV              = "D_PAYNOTE_TV";                /* Payment Note for TV                      */
    public static final String      PAYNOTE_MOB             = "D_PAYNOTE_MOB";               /* Payment Note for MOB                     */
    public static final String      PAYNOTE_IOI             = "D_PAYNOTE_IOI";               /* Payment Note for IOI                     */
    public static final String      PAYNOTE_O2F             = "D_PAYNOTE_O2F";               /* Payment Note for O2F                     */
    public static final String      PAYNOTE_101             = "D_PAYNOTE_101";               /* Payment Note for 1010                    */

    
    /* FAQ */
    public static final String      FAQ_URL_LTS             = "D_FAQ_URL_LTS";               /* FAQ URL for LTS                          */
    public static final String      FAQ_URL_PCD             = "D_FAQ_URL_PCD";               /* FAQ URL for PCD                          */
    public static final String      FAQ_URL_TV              = "D_FAQ_URL_TV";                /* FAQ URL for TV                           */
    public static final String      FAQ_URL_CSL             = "D_FAQ_URL_CSL";               /* FAQ URL for CSL                          */
    
    
    /* SignUp */
    public static final String      REGF_ACPT_PICS          = "D_REGF_ACPT_PICS";
    public static final String      REGF_ACPT_PICS_COMM     = "D_REGF_ACPT_PICS_COMM";
    public static final String      REGF_DOC_NUM            = "D_REGF_DOC_NUM";
    public static final String      REGF_DOC_NUM_COMM       = "D_REGF_DOC_NUM_COMM";
    public static final String      REGF_FB_DOC_NUM         = "D_REGF_FB_DOC_NUM";
    public static final String      REGF_FB_DOC_NUM_COMM    = "D_REGF_FB_DOC_NUM_COMM";
    public static final String      REGF_FB_ASO_LOB         = "D_REGF_FB_ASO_LOB";
    public static final String      REGF_FB_ASO_LOB_COMM    = "D_REGF_FB_ASO_LOB_COMM";
    public static final String      REGF_PICS               = "D_REGF_PICS";
    public static final String      REGF_PICS_COMM          = "D_REGF_PICS_COMM";
    public static final String      REGF_PICS_ECLSN         = "D_REGF_PICS_ECLSN";
    public static final String      REGF_PICS_ECLSN_        = "D_REGF_PICS_ECLSN_%s";
    public static final String      REGF_TITLE              = "D_REGF_TITLE";
    public static final String      REGF_TITLE_ECLSN        = "D_REGF_TITLE_ECLSN";
    public static final String      REGF_TITLE_ECLSN_       = "D_REGF_TITLE_ECLSN_%s";
    
    public static final String      REGF_ITO_ATH            = "D_REGF_ITO_ATH";
    public static final String      REGF_ITO_ATH_COMM       = "D_REGF_ITO_ATH_COMM";
    
    public static final String      REGF_PROMO_OPT          = "D_REGF_PROMO_OPT";
    
    
    /* Alias */
    public static final String      ALS_DEF                 = "D_ALS_DEF";
    
    
    /* LTS Payment Method */
    public static final String      LTS_PAYMETH_CASH        = "D_LTS_PAYMETH_CASH";
    public static final String      LTS_PAYMETH_CRCARD      = "D_LTS_PAYMETH_CRCARD";
    public static final String      LTS_PAYMETH_AUTOPAY     = "D_LTS_PAYMETH_AUTOPAY";

    
    /* My Profile */
    public static final String      MYPRF_BSC_HDR           = "D_MYPRF_BSC_HDR";
    public static final String      MYPRF_LOGIN_ID          = "D_MYPRF_LOGIN_ID";
    public static final String      MYPRF_PWD               = "D_MYPRF_PWD";
    public static final String      MYPRF_NICK_NAME         = "D_MYPRF_NICK_NAME";
    public static final String      MYPRF_CT_MAIL           = "D_MYPRF_CT_MAIL";
    public static final String      MYPRF_CT_MOB            = "D_MYPRF_CT_MOB";
    public static final String      MYPRF_LANG              = "D_MYPRF_LANG";
    public static final String      MYPRF_BSC_DESN          = "D_MYPRF_BSC_DESN";
    public static final String      MYPRF_UDT_BSC_HDR       = "D_MYPRF_UDT_BSC_HDR";
    public static final String      MYPRF_CURR_PWD          = "D_MYPRF_CURR_PWD";
    public static final String      MYPRF_NEW_PWD           = "D_MYPRF_NEW_PWD";
    public static final String      MYPRF_CFM_PWD           = "D_MYPRF_CFM_PWD";
    public static final String      MYPRF_REMARK1           = "D_MYPRF_REMARK1";
    public static final String      MYPRF_REMARK2           = "D_MYPRF_REMARK2";
    public static final String      MYPRF_REMARK3           = "D_MYPRF_REMARK3";
    public static final String      MYPRF_REMARK4           = "D_MYPRF_REMARK4";
    public static final String      MYPRF_REMARK5           = "D_MYPRF_REMARK5";
    public static final String      MYPRF_LANG_ZH           = "D_MYPRF_LANG_ZH";
    public static final String      MYPRF_LANG_EN           = "D_MYPRF_LANG_EN";
    public static final String      MYPRF_COMP_HDR          = "D_MYPRF_COMP_HDR";
    public static final String      MYPRF_COMP_NM           = "D_MYPRF_COMP_NM";
    public static final String      MYPRF_COMP_IND          = "D_MYPRF_COMP_IND";
    public static final String      MYPRF_COMP_NO_STF       = "D_MYPRF_COMP_NO_STF";
    public static final String      MYPRF_COMP_NO_BRC       = "D_MYPRF_COMP_NO_BRC";
    public static final String      MYPRF_COMP_CROSS        = "D_MYPRF_COMP_CROSS";
    public static final String      MYPRF_COMPRI_CTC_HDR    = "D_MYPRF_COMPRI_CTC_HDR";
    public static final String      MYPRF_COMPRI_NM         = "D_MYPRF_COMPRI_NM";
    public static final String      MYPRF_COMPRI_EML        = "D_MYPRF_COMPRI_EML";
    public static final String      MYPRF_COMPRI_TTL        = "D_MYPRF_COMPRI_TTL";
    public static final String      MYPRF_COMPRI_JT         = "D_MYPRF_COMPRI_JT";
    public static final String      MYPRF_COMPRI_TNO        = "D_MYPRF_COMPRI_TNO";
    public static final String      MYPRF_COMPRI_MNO        = "D_MYPRF_COMPRI_MNO";
    public static final String      MYPRF_COMSEC_CTC_HDR    = "D_MYPRF_COMSEC_CTC_HDR";
    public static final String      MYPRF_COMSEC_NM         = "D_MYPRF_COMSEC_NM";
    public static final String      MYPRF_COMSEC_EML        = "D_MYPRF_COMSEC_EML";
    public static final String      MYPRF_COMSEC_TTL        = "D_MYPRF_COMSEC_TTL";
    public static final String      MYPRF_COMSEC_JT         = "D_MYPRF_COMSEC_JT";
    public static final String      MYPRF_COMSEC_TNO        = "D_MYPRF_COMSEC_TNO";
    public static final String      MYPRF_COMSEC_MNO        = "D_MYPRF_COMSEC_MNO";
    public static final String      MYPRF_NO                = "D_MYPRF_NO";
    public static final String      MYPRF_YES               = "D_MYPRF_YES";
    public static final String      MYPRF_SHOW_LTS          = "D_MYPRF_SHOW_LTS";
    public static final String      MYPRF_SHOW_IMS          = "D_MYPRF_SHOW_IMS";
    public static final String      MYPRF_SHOW_MOB          = "D_MYPRF_SHOW_MOB";
    public static final String      MYPRF_SHOW_LTS_COMM     = "D_MYPRF_SHOW_LTS_COMM";
    public static final String      MYPRF_SHOW_IMS_COMM     = "D_MYPRF_SHOW_IMS_COMM";
    public static final String      MYPRF_SHOW_MOB_COMM     = "D_MYPRF_SHOW_MOB_COMM";

    public static final String      CTAF_IM_MOBPH           = "D_CTAF_IM_MOBPH";
    public static final String      CTAF_IM_OFCPH           = "D_CTAF_IM_OFCPH";
    public static final String      CTAF_HOMEPH             = "D_CTAF_HOMEPH";
    public static final String      CTAF_IM_EMAIL           = "D_CTAF_IM_EMAIL";
    public static final String      CTAF_CSIM_CTC           = "D_CTAF_CSIM_CTC";

    public static final String      ISPR_LBL_DISP_ACCT_INFO = "D_ISPR_LBL_DISP_ACCT_INFO"; 
    public static final String      ISPR_LBL_DISP_ACCT_NOTE = "D_ISPR_LBL_DISP_ACCT_NOTE";
    public static final String      ISPR_ALL_ACCNT          = "D_ISPR_ALL_ACCNT";

    public static final String      NOW_DOL_ASOF            = "D_NOW_DOL_ASOF";

    
    /* CARE */
    public static final String      CARE_ACP                = "D_CARE_ACP";
    public static final String      CARE_DCL                = "D_CARE_DCL";
    public static final String      CARE_IGUARD_URL         = "D_CARE_IGUARD_URL";
    
    
    /* Appointment */
    public static final String      APPTF_SRVO_APPT_DT      = "D_APPTF_SRVO_APPT_DT";
    public static final String      APPTF_SRVO_SRV          = "D_APPTF_SRVO_SRV";
    public static final String      APPTF_SRVO_ADDR         = "D_APPTF_SRVO_ADDR";
    public static final String      APPTF_MAINT_APPT_DT     = "D_APPTF_MAINT_APPT_DT";
    public static final String      APPTF_MAINT_ADDR        = "D_APPTF_MAINT_ADDR";
    public static final String      APPTF_MAINT_SRV         = "D_APPTF_MAINT_SRV";
    public static final String      APPTF_MAINT_CTC         = "D_APPTF_MAINT_CTC";
    public static final String      APPTF_MAINT_CTC_NO      = "D_APPTF_MAINT_CTC_NO";
    public static final String      APPTF_MAINT_CHG_BTN     = "D_APPTF_MAINT_CHG_BTN";
    public static final String      APPTF_MAINT_DEL_BTN     = "D_APPTF_MAINT_DEL_BTN";

    
    /* Bill */
    public static final String      BILL_NOBILL             = "D_BILL_NOBILL";               
    
    
    /* Plan */
    public static final String      PLNMBF_HDR_LDATA        = "D_PLNMBF_HDR_LDATA";
    public static final String      PLNMBF_ASOF             = "D_PLNMBF_ASOF";
    public static final String      PLNMBF_HDR_LVAS         = "D_PLNMBF_HDR_LVAS";
    public static final String      PLNMBF_HDR_RDATA        = "D_PLNMBF_HDR_RDATA";
    public static final String      PLNMBF_HDR_RVAS         = "D_PLNMBF_HDR_RVAS";
    public static final String      PLNMBF_NRF              = "D_PLNMBF_NRF";
    public static final String      PLNMBF_MSTMRT           = "D_PLNMBF_MSTMRT";
    public static final String      PLNMBF_GRP_DESN         = "D_PLNMBF_GRP_DESN";
    public static final String      PLNMBF_GRP_ETT_HDR      = "D_PLNMBF_GRP_ETT_HDR";
    public static final String      PLNMBF_GRP_USG_HDR      = "D_PLNMBF_GRP_USG_HDR";
    
    
    public static final String      PLNMBF_LVCE_LASTUPD     = "D_PLNMBF_LVCE_LASTUPD";
    public static final String      PLNMBF_LVDO_LASTUPD     = "D_PLNMBF_LVDO_LASTUPD";
    public static final String      PLNMBF_SMSMOOV_LASTUPD  = "D_PLNMBF_SMSMOOV_LASTUPD";
    public static final String      PLNMBF_MMS_LASTUPD      = "D_PLNMBF_MMS_LASTUPD";
    public static final String      PLNMBF_BB_LASTUPD       = "D_PLNMBF_BB_LASTUPD";
    public static final String      PLNMBF_GDATA_LASTUPD    = "D_PLNMBF_GDATA_LASTUPD";
    
    
    /* Line Test */
    public static final String      LNTT_HDR                = "D_LNTT_HDR";
    
    /* Reboot Modem */
    public static final String      RBMD_HDR                = "D_RBMD_HDR";
    
    /* Inbox */
    public static final String      INBX_EXP_UNT            = "D_INBX_EXP_UNT";
    
    
    /* Messages or Text or HTML */
    public static final String      PAGE_INEFF              = "M_PAGE_INEFF";               /* Page is ineffective                      */
    public static final String      PAGE_INEFF_NOSESS       = "M_PAGE_INEFF_NOSESS";        /* Page is ineffective (No Sess)            */
    public static final String      SVR_ERROR               = "M_SVR_ERROR";                /* Server Error (generic)                   */

    public static final String      BUSY                    = "M_BUSY";                     /* System Busy                              */
    public static final String      ALT                     = "M_ALT";                      /* Data is altered by other users           */
    
    public static final String      IV_DOCTY                = "M_IV_DOCTY";                 /* Invalid Document Type                    */
    public static final String      IV_DOCNUM               = "M_IV_DOCNUM";                /* INvalid Document Number                  */
    
    public static final String      IV_LOGIN_ID             = "M_IV_LOGIN_ID";              /* Invalid Login Id                         */
    public static final String      IV_LOGIN_ID_FMT         = "M_IV_LOGIN_ID_FMT";          /* Invalid Login Id Format                  */
    public static final String      IV_PWD                  = "M_IV_PWD";                   /* Invalid Password                         */
    public static final String      IV_PWDCBN               = "M_IV_PWDCBN";                /* Invalid Pwd Combination                  */
    public static final String      IV_NICKNAME             = "M_IV_NICKNAME";              /* Invalid Nick Name                        */
    public static final String      RESV_NICKNAME           = "M_RESV_NICKNAME";            /* Reserved Nick Name                       */
    public static final String      IV_CTMAIL               = "M_IV_CTMAIL";                /* Invalid Contact Email                    */
    public static final String      IV_CTMAIL_FMT           = "M_IV_CTMAIL_FMT";            /* Invalid Contact Email Format             */
    public static final String      IV_CTMOB                = "M_IV_CTMOB";                 /* Invalid Contact Mobile                   */
    public static final String      IV_LANG                 = "M_IV_LANG";                  /* Invalid Language                         */
    public static final String      IV_AMT                  = "M_IV_AMT";                   /* Invalid Amount                           */
    public static final String      NIL_AMT                 = "M_NIL_AMT";                  /* Nil Amount                               */
    public static final String      CFMPWDMM                = "M_CFMPWDMM";                 /* Re-type Password Mismatch                */
   
    public static final String      IV_LOB                  = "M_IV_LOB";                   /* Invalid LOB                              */
    public static final String      IV_SN_LTS               = "M_IV_SN_LTS";                /* Invalid LTS Service Number               */
    public static final String      IV_SN_MOB               = "M_IV_SN_MOB";                /* Invalid MOB Service Number               */
    public static final String      IV_SN_PCD               = "M_IV_SN_PCD";                /* Invalid PCD Service Number               */
    public static final String      IV_AN_TV                = "M_IV_AN_TV";                 /* Invalid TV Account Number                */
    
    public static final String      IV_CONAME               = "M_IV_CONAME";                /* Invalid Company Name                     */
    public static final String      IV_INDUST               = "M_IV_INDUST";                /* Invalid Industry                         */
    public static final String      IV_NUSTFF               = "M_IV_NUSTFF";                /* Invalid Nu Of Staff                      */
    public static final String      IV_NUPRSN               = "M_IV_NUPRSN";                /* Invalid Nu Of Presence                   */
    public static final String      IV_XBRDR                = "M_IV_XBRDR";                 /* Invalid Cross Border                     */
    public static final String      IV_PRICTNM              = "M_IV_PRICTNM";               /* Invalid Pri CT Name                      */
    public static final String      IV_PRICTTL              = "M_IV_PRICTTL";               /* Invalid Pri CT Title                     */
    public static final String      IV_PRICTEM              = "M_IV_PRICTEM";               /* Invalid Pri CT Email                     */
    public static final String      IV_PRICTJT              = "M_IV_PRICTJT";               /* Invalid Pri CT Job Title                 */
    public static final String      IV_PRICTTEL             = "M_IV_PRICTTEL";              /* Invalid Pri CT Tel                       */
    public static final String      IV_PRICTMOB             = "M_IV_PRICTMOB";              /* Invalid Pri CT Mob                       */
    public static final String      IV_SECCTNM              = "M_IV_SECCTNM";               /* Invalid Sec CT Name                      */
    public static final String      IV_SECCTTL              = "M_IV_SECCTTL";               /* Invalid Sec CT Title                     */
    public static final String      IV_SECCTEM              = "M_IV_SECCTEM";               /* Invalid Sec CT Email                     */
    public static final String      IV_SECCTJT              = "M_IV_SECCTJT";               /* Invalid Sec CT Job Title                 */
    public static final String      IV_SECCTTEL             = "M_IV_SECCTTEL";              /* Invalid Sec CT Tel                       */
    public static final String      IV_SECCTMOB             = "M_IV_SECCTMOB";              /* Invalid Sec CT Mob                       */
    
    public static final String      NOT_ACPTTNC             = "M_NOT_ACPTTNC";              /* Not Accepted TNC                         */
    
    public static final String      NO_CUSTOMER             = "M_NO_CUSTOMER";              /* No Such Customer                         */
    public static final String      LOGIN_ID_EXIST          = "M_LOGIN_ID_EXIST";           /* Login Id Already Exist                   */
    public static final String      INACTIVE_CUST           = "M_INACTIVE_CUST";            /* Inactive Customer                        */
    public static final String      INACTIVE_SVEE_CUST      = "M_INACTIVE_SVEE_CUST";       /* Inactive Customer or Login Id            */
    public static final String      NOT_SUPP_SEG            = "M_NOT_SUPP_SEG";             /* Not Supported Customer Segment           */
    public static final String      CUS_ALDY_REG            = "M_CUS_ALDY_REG";             /* Customer Already Registered              */
    public static final String      NO_SUBN_FND             = "M_NO_SUBN_FND";              /* No Subscription Found                    */
    public static final String      CUS_MISMATCH            = "M_CUS_MISMATCH";             /* Supplied Customer Id mismatch w/ DB      */
    public static final String      LOGIN_FAIL              = "M_LOGIN_FAIL";               /* Login Failed                             */
    
    public static final String      PWD_NO_CHG              = "M_PWD_NO_CHG";               /* The Password has no change               */
    
    public static final String      CFM_LOST_EDIT           = "M_CFM_LOST_EDIT";            /* Confirm Lost Edited Content              */
    public static final String      CFM_CANCEL              = "M_CFM_CANCEL";               /* Confirm to Cancel                        */

    public static final String      ACTCD_REGENED           = "M_ACTCD_REGENED";            /* ActCode Regenerated                      */
    public static final String      ACTCD_REGEN_EXCEED      = "M_ACTCD_REGEN_EXCEED";       /* Too Many ActCode Regeneration            */
    public static final String      LOGIN_ID_ACT_DONE       = "M_LOGIN_ID_ACT_DONE";        /* Login Id is Activated                    */
    public static final String      ACTCD_CNB_NIL           = "M_ACTCD_CNB_NIL";            /* ActCode cannot be Nil                    */
    public static final String      ACTCD_MM                = "M_ACTCD_MM";                 /* ActCode Mismatch                         */
    public static final String      ACT_NOT_REQU            = "M_ACT_NOT_REQU";             /* Activation is Not Required               */
    
    public static final String      NO_RECALL_OPT           = "M_NO_RECALL_OPT";            /* No Recall Option                         */
    public static final String      UNABLE_IDENT_CUST       = "M_UNABLE_IDENT_CUST";        /* Unable Identified Customer               */
    public static final String      RECALL_DONE             = "M_RECALL_DONE";              /* Recall is Done                           */
    public static final String      FILL_SVEE_IVSTATE       = "M_FILL_SVEE_IVSTATE";        /* Account not activated                    */
    public static final String      ACTN_FAIL               = "M_ACTN_FAIL";                /* Action Failed                            */
    
    public static final String      REVERIFY_FAIL           = "M_REVERIFY_FAIL";            /* Reverify Failed                          */
    public static final String      PWD_HB_RESET            = "M_PWD_HB_RESET";             /* Password has been reset                  */
    
    public static final String      UPDATE_DONE             = "M_UPDATE_DONE";              /* Update is Done                           */
    
    public static final String      NA_ADR                  = "M_NA_ADR";                   /* Non-ASC Address                          */
    public static final String      NIL_ADR                 = "M_NIL_ADR";                  /* Nil Address                              */
    public static final String      IL_ADR                  = "M_IL_ADR";                   /* Invalid Length of Address                */
    public static final String      NIL_POX                 = "M_NIL_POX";                  /* Nil Postal Box                           */
    public static final String      NA_EMAIL                = "M_NA_EMAIL";                 /* Non-ASC Email                            */
    public static final String      IL_EMAIL                = "M_IL_EMAIL";                 /* Invalid Length of Email                  */
    public static final String      IV_EMAIL                = "M_IV_EMAIL";                 /* Invalid Email (format)                   */
    public static final String      IV_SMSNTF               = "M_IV_SMSNTF";                /* Invalid SMS Notification#                */
    
    public static final String      BI_NRDY                 = "M_BI_NRDY";                  /* Bill Not ready                           */
    
    public static final String      LNTT_TMOUT              = "M_LNTT_TMOUT";               /* Line Test - Timeout                      */
    
    public static final String      APPT_NA_UPD             = "M_APPT_NA_UPD";              /* Appointment - No update allowed          */
    public static final String      APPT_SR_UPD             = "M_APPT_SR_UPD";              /* Appointment - SR updated                 */
    public static final String      APPT_SR_CLS             = "M_APPT_SR_CLS";              /* Appointment - SR closed                  */
    public static final String      APPT_SR_CLS_CFM         = "M_APPT_SR_CLS_CFM";          /* Appointment - SR confirmation for closure*/
    public static final String      APPT_SR_PONCVG          = "M_APPT_SR_PONCVG";           /* Appointment - SR PON Coverage            */
    
    public static final String      PAM_DONE                = "M_PAM_DONE";                 /* Profile Account Maint - Update Done      */
    
    public static final String      WARN_CHG2_PAPERBILL     = "M_WARN_CHG2_PAPERBILL";      /* Warning of change to Paper Bill          */
    
    public static final String      SIP                     = "M_SIP";                      /* SIP                                      */
    public static final String      WIP                     = "M_WIP";                      /* WIP                                      */
    
    public static final String      UPD2_CRCARD             = "M_UPD2_CRCARD";              /* Update to Paid By Credit Card            */
    public static final String      UP_CRCARD_NO            = "M_UPD_CRCARD_NO";            /* Update Credit Card Number                */
    
    public static final String      NO_ASO                  = "M_NO_ASO";                   /* No Association                           */

    public static final String      PLNMB_TP_DENIED         = "M_PLNMB_TP_DENIED";
    public static final String      PLNMB_NOTAUT_GNL        = "M_PLNMB_NOTAUT_GNL";
    public static final String      PLNMB_NOTAUT_101        = "M_PLNMB_NOTAUT_101";
    public static final String      PLNMB_NO_BOID_SEL       = "M_PLNMB_NO_BOID_SEL";
    public static final String      PLNMB_TP_DUPL           = "M_PLNMB_TP_DUPL";
    public static final String      PLNMB_TP_SIMU           = "M_PLNMB_TP_SIMU";
    public static final String      PLNMB_TP_SUCC           = "M_PLNMB_TP_SUCC";
    public static final String      PLNMB_NO_ODATA          = "M_PLNMB_NO_ODATA";
    public static final String      PLNMB_NO_OVAS           = "M_PLNMB_NO_OVAS";
    public static final String      PLNMB_TPUP_EMTY_BOID    = "M_PLNMB_TPUP_EMTY_BOID";
    
    public static final String      BILL_BIERR              = "M_BILL_BIERR";
    public static final String      BILL_BIRDY              = "M_BILL_BIRDY";
    
    public static final String      BIIF_CFM                = "M_BIIF_CFM";
    public static final String      BIIF_LTS_UPD_DONE       = "M_BIIF_LTS_UPD_DONE";
    public static final String      BIIF_IMS_UPD_DONE       = "M_BIIF_IMS_UPD_DONE";
    public static final String      BIIF_MOBH_UPD_DONE      = "M_BIIF_MOBH_UPD_DONE";
    public static final String      BIIF_MOBC_UPD_DONE      = "M_BIIF_MOBC_UPD_DONE";
    
    public static final String      LNTT_ERR                = "M_LNTT_ERR";
    public static final String      LNTT_PCD_ALONE_DIALUP   = "M_LNTT_PCD_ALONE_DIALUP";
    public static final String      LNTT_PCD_GGDY           = "M_LNTT_PCD_GGDY";
    
    public static final String      APPT_MAINT_RPTSR_FAIL   = "M_APPT_MAINT_RPTSR_FAIL";
    public static final String      APPT_MAINT_IVNAME       = "M_APPT_MAINT_IVNAME";
    public static final String      APPT_MAINT_IVNUM        = "M_APPT_MAINT_IVNUM";
    public static final String      APPT_MAINT_EAT_UNSUCC   = "M_APPT_MAINT_EAT_UNSUCC";
    public static final String      APPT_MAINT_IVAPDT       = "M_APPT_MAINT_IVAPDT";
    public static final String      APPT_MAINT_IVAPTS       = "M_APPT_MAINT_IVAPTS";
    public static final String      APPT_MAINT_NONEN_NAME   = "M_APPT_MAINT_NONEN_NAME";
    public static final String      APPT_MAINT_NOT_CRT      = "M_APPT_MAINT_NOT_CRT";
    public static final String      APPT_MAINT_NOT_UPD      = "M_APPT_MAINT_NOT_UPD";
    public static final String      APPT_MAINT_NOCHG_UPD    = "M_APPT_MAINT_NOCHG_UPD";
    
    public static final String      CTAC_ILEMAIL            = "M_CTAC_ILEMAIL";
    public static final String      CTAC_IVEMAIL            = "M_CTAC_IVEMAIL";
    public static final String      CTAC_IVHMNUM            = "M_CTAC_IVHMNUM";
    public static final String      CTAC_IVMOB              = "M_CTAC_IVMOB";
    public static final String      CTAC_IVOFCNUM           = "M_CTAC_IVOFCNUM";
    public static final String      CTAC_REQ_OFCNUM         = "M_CTAC_REQ_OFCNUM";
    public static final String      CTAC_REQ_EMAIL          = "M_CTAC_REQ_EMAIL";
    public static final String      CTAC_REQ_HMNUM          = "M_CTAC_REQ_HMNUM";
    public static final String      CTAC_REQ_INPUT          = "M_CTAC_REQ_INPUT";
    public static final String      CTAC_REQ_MOB            = "M_CTAC_REQ_MOB";
    public static final String      CTAC_CFM                = "M_CTAC_CFM";
    
    public static final String      PROF_IVCURRPWD          = "M_PROF_IVCURRPWD";
    public static final String      PROF_ILPWD              = "M_PROF_ILPWD";
    public static final String      PROF_IVPWDCBN           = "M_PROF_IVPWDCBN";
    public static final String      PROF_ILNICKNAME         = "M_PROF_ILNICKNAME";
    public static final String      PROF_RESV_NCKNM         = "M_PROF_RESV_NCKNM";
    public static final String      PROF_NACTMAIL           = "M_PROF_NACTMAIL";
    public static final String      PROF_IVCTMAIL           = "M_PROF_IVCTMAIL";
    public static final String      PROF_IVMOB              = "M_PROF_IVMOB";
    public static final String      PROF_IVMOBALRT          = "M_PROF_IVMOBALRT";
    public static final String      PROF_IVLANG             = "M_PROF_IVLANG";
    public static final String      PROF_IVCFMPWD           = "M_PROF_IVCFMPWD";
    public static final String      PROF_NOCHGPWD           = "M_PROF_NOCHGPWD";
    public static final String      PROF_ILCUSTNM           = "M_PROF_ILCUSTNM";      
    public static final String      PROF_NLINDUST           = "M_PROF_NLINDUST";
    public static final String      PROF_NLNUSTFF           = "M_PROF_NLNUSTFF";
    public static final String      PROF_NLNUPRSN           = "M_PROF_NLNUPRSN";
    public static final String      PROF_IVXBRDR            = "M_PROF_IVXBRDR";
    public static final String      PROF_ILPRICTNM          = "M_PROF_ILPRICTNM";
    public static final String      PROF_IVPRICTTL          = "M_PROF_IVPRICTTL";
    public static final String      PROF_NLPRICTEM          = "M_PROF_NLPRICTEM";
    public static final String      PROF_IVPRICTEM          = "M_PROF_IVPRICTEM";
    public static final String      PROF_ILPRICTJT          = "M_PROF_ILPRICTJT";
    public static final String      PROF_NLPRICTTEL         = "M_PROF_NLPRICTTEL";
    public static final String      PROF_IVPRICTTEL         = "M_PROF_IVPRICTTEL";
    public static final String      PROF_NLPRICTMOB         = "M_PROF_NLPRICTMOB";
    public static final String      PROF_IVPRICTMOB         = "M_PROF_IVPRICTMOB";
    public static final String      PROF_ILSECCTNM          = "M_PROF_ILSECCTNM";
    public static final String      PROF_IVSECCTTL          = "M_PROF_IVSECCTTL";
    public static final String      PROF_NLSECCTEM          = "M_PROF_NLSECCTEM";
    public static final String      PROF_IVSECCTEM          = "M_PROF_IVSECCTEM";
    public static final String      PROF_ILSECCTJT          = "M_PROF_ILSECCTJT";
    public static final String      PROF_NLSECCTTEL         = "M_PROF_NLSECCTTEL";
    public static final String      PROF_IVSECCTTEL         = "M_PROF_IVSECCTTEL";
    public static final String      PROF_NLSECCTMOB         = "M_PROF_NLSECCTMOB";
    public static final String      PROF_IVSECCTMOB         = "M_PROF_IVSECCTMOB";

    
    /* Alias */
    public static final String      ALIAS_ILALIAS           = "M_ALIAS_ILALIAS";
    public static final String      ALIAS_UPD_DONE          = "M_ALIAS_UPD_DONE";

    
    /* CARE */
    public static final String      CARE_OPTOUT_REG         = "M_CARE_OPTOUT_REG";          /* CARE+BIPT Optout via Reg                 */
    public static final String      CARE_OPTOUT             = "M_CARE_OPTOUT";              /* CARE+BIPT Optout                         */
    public static final String      BIPT_OPTOUT_REG         = "M_BIPT_OPTOUT_REG";          /* CARE Optin BIPT Optout via Reg           */
    public static final String      BIPT_OPTOUT             = "M_BIPT_OPTOUT";              /* CARE Optin BIPT Optout                   */
    public static final String      BIPT_OPTIN_REG          = "M_BIPT_OPTIN_REG";           /* CARE+BIPT Optin via Reg                  */
    public static final String      BIPT_OPTIN              = "M_BIPT_OPTIN";               /* CARE+BIPT Optin                          */

    
    /* MIP Related */
    public static final String      CSIM_ACCT_IN_MIG        = "M_CSIM_ACCT_IN_MIG";         /* C-SIM (Account) in Migration             */
    public static final String      CSIM_IN_MIG             = "M_CSIM_IN_MIG";              /* C-SIM in Migration                       */
    
    
    /* Bill Agent MRT */
    public static final String      BA_MRT_BINQ             = "M_BA_MRT_BINQ";
    public static final String      BA_MRT_BIIF             = "M_BA_MRT_BIIF";

    
    /* Inbox */
    public static final String      INBX_NO_MSG             = "M_INBX_NO_MSG";
    

    /* Error Message to be shown when unrecoverable Error */
    public static final String      ERR_MSG                 = "We are sorry that our system is under maintenance at the moment and will resume normal soon. Please try again later.\n 對不起，我們的系統正進行維護，並會盡快恢復正常服務。請稍後再試。\nRef. Code/參考號碼:";  
    /* Destination URL after the unrecoverable error message is shown */
    public static final String      DES_ONERR               = "http://www.hkt.com";

    
    public static void main(String rArg[])
    {
        System.out.println(getVer());
        return;
    }
    
    
    public static String getVer()
    {
        return ("$URL: svn://10.87.120.207/dango/rel/v16.0/src/com/pccw/dango/shared/tool/Msgs.java $, $Rev: 1123 $");
    }
    
    
    public static void abortOnUnrecoverableError(String rNode, Throwable rEx) 
    {
        String                      rRMsg;
        
        /*
            @@ 2017-06-02
            
            Both getLocalizedMessage() and toString() return meaningless 
            description, hence, only the default message + input node will 
            be shown.
            
            rRMsg = rMsg + " (" + rEx.getLocalizedMessage() + ")";
            
            
            @@ 2018-01-30
            
            This method originally resident in Rpcs.java. Now move here
            to serve the generic message when unrecoverable error (e.g.
            error CliEnv before setup).
        */
        
        rRMsg = ERR_MSG + " (" + rNode + ")";
        GwtTool.showMsg(rRMsg);
        GwtTool.replaceMe(DES_ONERR);
    }
}    
