package com.pccw.myhkt.fragment;

import kankan.wheel.widget.OnWheelChangedListener;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentTransaction;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;

import com.pccw.dango.shared.cra.ApptCra;
import com.pccw.dango.shared.entity.ApptInfo;
import com.pccw.dango.shared.entity.SRApptTS;
import com.pccw.dango.shared.entity.SrvReq;
import com.pccw.dango.shared.entity.SubnRec;
import com.pccw.dango.shared.tool.RC;
import com.pccw.myhkt.APIsManager;
import com.pccw.myhkt.APIsResponse;
import com.pccw.myhkt.ClnEnv;
import com.pccw.myhkt.DialogHelper;
import com.pccw.myhkt.FAWrapper;
import com.pccw.myhkt.InterpretRCManager;
import com.pccw.myhkt.R;
import com.pccw.myhkt.Utils;
import com.pccw.myhkt.activity.BaseActivity;
import com.pccw.myhkt.fragment.BaseServiceFragment.OnLiveChatListener;
import com.pccw.myhkt.lib.ui.AAQuery;
import com.pccw.myhkt.lib.ui.HKTButton;
import com.pccw.myhkt.lib.ui.popover.QuickAction;
import com.pccw.myhkt.lib.ui.popover.QuickAction.OnPickerScrollListener;
import com.pccw.myhkt.lib.ui.popover.QuickAction.OnPickerScrollSingleListener;
import com.pccw.wheat.shared.tool.Reply;

/************************************************************************
 * File : MyApptUpdateFragment.java 
 * Desc : my appointment update page
 * Name : MyApptUpdateFragment
 * by : Derek Tsui 
 * Date : 01/03/2016
 * 
 * Change History:
 * Date       Modified By		Description
 * ---------- ----------------	-------------------------------
 * 01/03/2016 Derek Tsui 		-First draft
 *************************************************************************/

public class MyApptUpdateFragment extends BaseMyApptFragment  {
	private MyApptUpdateFragment me;
	private AAQuery aq;
	private FragmentTransaction ft;
	private View myView;
	
	private SrvReq										srvReq;
	private ApptInfo									srApptInfo			= null;
	private String[]									timeslotAry			= null;
	
	private String[]				timeAry;
	private int 		 			timePos = 0;

	//wheel action
	private OnWheelChangedListener mWheelChangedListener;
	private OnPickerScrollListener mPickerScrollListener; //for standard 2 wheels
	private OnPickerScrollSingleListener mPickerScrollSingleListener; //for single wheel
	private OnLiveChatListener callback_livechat;
	@Override
	public void onAttach(@NonNull Activity activity) {
		super.onAttach(activity);
		try {
			callback_livechat = (OnLiveChatListener) activity;
		} catch (ClassCastException e) {
			throw new ClassCastException(activity.toString() + " must implement OnLiveChatListener");
		}

	}
	
	public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		me = this;
		View fragmentLayout = inflater.inflate(R.layout.fragment_myapptupdate, container, false);
		myView = fragmentLayout;
		initData();
		return fragmentLayout;
	}
	


	@Override
	public void onStart() {
		super.onStart();
	}

	@Override
	public void onResume(){
		super.onResume();
		// Screen Tracker
		FAWrapper.getInstance().sendFAScreen(getActivity(), R.string.CONST_SCRN_APPOINTMENTUPDATE, false);
	
	}
	
	@Override
	public void setUserVisibleHint(boolean isFragmentVisible) {
	    super.setUserVisibleHint(isFragmentVisible);
	}
	
	protected void initData() {    		
	}
	
	protected final void refreshData() {
		super.refreshData();
		srvReq = callback_main.getSrvReq();
		srApptInfo = callback_main.getApptInfo();
		prepareMyApptUpdateUI();
	}
	
	protected void initUI(){
		aq = new AAQuery(myView);
		int extralinespace = (int) getActivity().getResources().getDimension(R.dimen.extralinespace);
		int buttonPadding =  (int) getResources().getDimension(R.dimen.reg_logo_padding);
		int basePadding = (int) getActivity().getResources().getDimension(R.dimen.basePadding);
		int padding_screen = (int) getActivity().getResources().getDimension(R.dimen.padding_screen);
		int lob_box_width = (int) getActivity().getResources().getDimension(R.dimen.lob_box_width);
		aq.marginpx(R.id.myapptupd_layout2, basePadding, 0, basePadding, 0);
//		aq.marginpx(R.id.myapptupd_etxtlayout1, 0,0,0,buttonPadding);
		aq.marginpx(R.id.myapptupd_etxtlayout2, 0,0,0,buttonPadding);
		aq.marginpx(R.id.myapptupd_btn_layout, buttonPadding, 0, buttonPadding, 0);
		
		aq.id(R.id.myapptupd_header).getTextView().setCompoundDrawablesWithIntrinsicBounds(R.drawable.icon_apptx3, 0,0,0);
		aq.normText(R.id.myapptupd_header, " " + getString(R.string.MYHKT_SR_HDR_EDIT));
		aq.marginpx(R.id.myapptupd_header, 0, 0, 0, 0);
		
//		aq.id(R.id.myapptupd_logo_frame).width(lob_box_width);
//		aq.id(R.id.myapptupd_logo_frame).height(lob_box_width);
//		aq.normTextGrey(R.id.myapptupd_loginid_desc, "ACCOUNT NO.: ");
//		aq.normTextGrey(R.id.myapptupd_time_desc, " Select A Timeslot");
		aq.id(R.id.myapptupd_time_desc).textSize(getResources().getInteger(R.integer.textsize_default_int));
		aq.id(R.id.myapptupd_time_desc).getTextView().setCompoundDrawablesWithIntrinsicBounds(R.drawable.rightarrow_small, 0, 0, 0);
		aq.id(R.id.myapptupd_time_desc).getTextView().setCompoundDrawablePadding(basePadding);
		aq.marginpx(R.id.myapptupd_time_desc, 0, padding_screen, 0, basePadding);
		
//		aq.normTextGrey(R.id.myapptupd_contact_desc, " Contact Person" );
		aq.id(R.id.myapptupd_contact_desc).textSize(getResources().getInteger(R.integer.textsize_default_int));
		aq.id(R.id.myapptupd_contact_desc).getTextView().setCompoundDrawablesWithIntrinsicBounds(R.drawable.rightarrow_small, 0, 0, 0);
		aq.id(R.id.myapptupd_contact_desc).getTextView().setCompoundDrawablePadding(basePadding);
		aq.marginpx(R.id.myapptupd_contact_desc, 0, padding_screen, 0, 0);

		
		aq.line(0, "", R.id.myapptupd_line1,R.id.myapptupd_line2, R.id.myapptupd_line3, R.id.myapptupd_line4, R.id.myapptupd_line5);
//		timePopover = aq.popOverInputView(R.id.myapptupd_popover, "02-Mar 14:00 - 16:00");
//		aq.id(R.id.myapptupd_popover).clicked(this, "onClick");
		
//		aq.gravity(R.id.myapptupd_txt_time, Gravity.CENTER_VERTICAL | Gravity.RIGHT);
		aq.spinText(R.id.myapptupd_spinner , "");	
		aq.id(R.id.myapptupd_spinner).clicked(this, "onClick").background(0);
		
		
		//contact person
		aq.gravity(R.id.myapptupd_txt_contperson, Gravity.CENTER_VERTICAL | Gravity.RIGHT);
		aq.normEditText(R.id.myapptupd_etxt_contperson, "", "");
		EditText editTextContPerson = aq.id(R.id.myapptupd_etxt_contperson).getEditText();
		editTextContPerson.setTextColor(getActivity().getResources().getColor(R.color.hkt_textcolor));
		editTextContPerson.setBackgroundResource(0);
		editTextContPerson.setPadding(basePadding, 0, basePadding, 0);
		editTextContPerson.setSingleLine();
		
		//contact number
		aq.gravity(R.id.myapptupd_txt_contnum, Gravity.CENTER_VERTICAL | Gravity.RIGHT);
		aq.normEditText(R.id.myapptupd_etxt_contnum, "", "");
		EditText editTextContNum = aq.id(R.id.myapptupd_etxt_contnum).getEditText();
		editTextContNum.setTextColor(getActivity().getResources().getColor(R.color.hkt_textcolor));
		editTextContNum.setBackgroundResource(0);
		editTextContNum.setPadding(basePadding, 0, basePadding, 0);
		editTextContNum.setSingleLine();
		
		//buttons
		aq.norm2TxtBtns(R.id.myapptupd_btn_cancel, R.id.myapptupd_btn_confirm, getResString(R.string.MYHKT_SR_BTN_CANCEL) ,getResString(R.string.MYHKT_BTN_CONFIRM));
		aq.id(R.id.myapptupd_btn_cancel).clicked(this, "onClick");
		aq.id(R.id.myapptupd_btn_confirm).clicked(this, "onClick");
//		aq.normTxtBtn(R.id.myapptupd_btn_return, getResString(R.string.MYHKT_BTN_BACK), 0);
//		loginBtn  =  (HKTButton) aq.id(R.id.login_login_btn).getView();
		((HKTButton) aq.id(R.id.myapptupd_btn_return).getView()).initViews(getActivity(), getResources().getString(R.string.MYHKT_BTN_BACK), LayoutParams.MATCH_PARENT);
		aq.marginpx(R.id.myapptupd_btn_return, extralinespace, extralinespace, extralinespace, extralinespace);
		aq.id(R.id.myapptupd_btn_return).clicked(this, "onClick");
		

//		aq.layoutWeight(R.id.myapptupd_btn_return, 1);	
	}
	
	protected void prepareMyApptUpdateUI(){
		int basePadding = (int) getActivity().getResources().getDimension(R.dimen.basePadding);
		//derek
		prepareTimeSlot();
		if (srvReq != null) {
			// Preload Original SR Info
			int lob_eye_lts_plain = getSrvLogo(srvReq);
			if(R.drawable.lob_eye_lts_plain == lob_eye_lts_plain){
				aq.id(R.id.myapptupd_logo).getImageView().setScaleType(ImageView.ScaleType.FIT_CENTER);
			} else {
				aq.id(R.id.myapptupd_logo).getImageView().setScaleType(ImageView.ScaleType.CENTER_INSIDE);
			}
			aq.id(R.id.myapptupd_logo).getImageView().setImageResource(lob_eye_lts_plain);
			String srvLabel = "";
			if (SrvReq.OUT_PROD_EYE.equalsIgnoreCase(srvReq.getOutProd()) || SrvReq.OUT_PROD_VCE.equalsIgnoreCase(srvReq.getOutProd())) {
				srvLabel = getString(R.string.MYHKT_SR_LBL_LTS_ACC);
			} else if (SrvReq.OUT_PROD_PCD.equalsIgnoreCase(srvReq.getOutProd())) {
				srvLabel = getString(R.string.MYHKT_SR_LBL_PCD_ACC);
			} else if (SrvReq.OUT_PROD_VI.equalsIgnoreCase(srvReq.getOutProd())) {
				srvLabel = getString(R.string.MYHKT_SR_LBL_TV_ACC);
			}

			aq.marginpx(R.id.myapptupd_loginid_layout, basePadding,0,0,0);
			aq.id(R.id.myapptupd_loginid_label).text(srvLabel);
			aq.id(R.id.myapptupd_loginid_txt).textColorId(R.color.hkt_textcolor);
			aq.id(R.id.myapptupd_loginid_txt).text(srvReq.getAssocSubnRec().srvNum);
			
			
			aq.id(R.id.myapptupd_spinner).text(timeslotAry[timePos]);
			aq.id(R.id.myapptupd_etxt_contperson).text(srvReq.getCtNm());
			aq.id(R.id.myapptupd_etxt_contnum).text(srvReq.getCtNum());
			
			// Disable Update and Cancel Button
			if (!"Y".equalsIgnoreCase(srvReq.getAllowUpdInd())) {
				aq.id(R.id.myapptupd_spinner).getTextView().setEnabled(false);
				aq.id(R.id.myapptupd_spinner).getTextView().setAlpha(0.5f);
	
				aq.id(R.id.myapptupd_etxt_contperson).getEditText().setEnabled(false);
				aq.id(R.id.myapptupd_etxt_contperson).getEditText().setCursorVisible(false);
				aq.id(R.id.myapptupd_etxt_contperson).getEditText().setFocusable(false);
				aq.id(R.id.myapptupd_etxt_contperson).getEditText().setFocusableInTouchMode(false);
				aq.id(R.id.myapptupd_etxt_contperson).getEditText().setAlpha(0.5f);
	
				aq.id(R.id.myapptupd_etxt_contnum).getEditText().setEnabled(false);
				aq.id(R.id.myapptupd_etxt_contnum).getEditText().setCursorVisible(false);
				aq.id(R.id.myapptupd_etxt_contnum).getEditText().setFocusable(false);
				aq.id(R.id.myapptupd_etxt_contnum).getEditText().setFocusableInTouchMode(false);
				aq.id(R.id.myapptupd_etxt_contnum).getEditText().setAlpha(0.5f);
	
				aq.id(R.id.myapptupd_btn_cancel).visibility(View.GONE);
				aq.id(R.id.myapptupd_btn_confirm).visibility(View.GONE);
			}
		}
	}
	
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.myapptupd_btn_return:
			//TODO
//			if (viewholder.appointmentupdate_spinner.getSelectedItemPosition() != 0 || !viewholder.appointmentupdate_edittext_name.getText().toString().trim().equalsIgnoreCase(srvReq.getCtName().trim()) || !viewholder.appointmentupdate_edittext_number.getText().toString().trim().equalsIgnoreCase(srvReq.getCtNum().trim())) {
//				displayConfirmToLeaveDialog();
//			} else {
				callback_main.setActiveSubview(R.string.CONST_SELECTEDFRAG_MYAPPTLIST);
				callback_main.fragmentBack();
//			}
			break;
		case R.id.myapptupd_spinner:
			final QuickAction pickerWheelAction = new QuickAction(getActivity(), 0, 0);

			pickerWheelAction.initPickerWheelSingle(timeslotAry, timePos);
	        pickerWheelAction.setOnPickerScrollSingleListener(new QuickAction.OnPickerScrollSingleListener() {
				@Override
				public void onPickerScroll(String leftResult, int pos) {
					timePos = pos;
					setSelectedTime();
				}
			});
			pickerWheelAction.showPicker(v, aq.id(R.id.myapptupd_layout).getView());
			break;
		case R.id.myapptupd_btn_confirm:
			Reply reply;
			// Validation
			boolean isChangeTimeSlot = false;
			boolean isChangeCtInfo = false;

			SrvReq rSrvReq = srvReq.copyMe();
			// avoid the timeslot does not trimmed
			rSrvReq.setApptTS(srvReq.getApptTS().copyMe());

			if (isZh) {
				rSrvReq.setSmsLang("zh");
			} else {
				rSrvReq.setSmsLang("en");
			}

			// Check TimeSlot Change
			if (timePos > 0 && srApptInfo != null) {
				SRApptTS rApptTS = srApptInfo.getApptTSAry()[timePos - 1].copyMe();
				rSrvReq.setApptTS(rApptTS);
				isChangeTimeSlot = true;
			}
			
			// Check Name Change
			if (!aq.id(R.id.myapptupd_etxt_contperson).getText().toString().trim().equalsIgnoreCase(srvReq.getCtNm().trim())) {
				rSrvReq.setCtNm(aq.id(R.id.myapptupd_etxt_contperson).getText().toString().trim());
				isChangeCtInfo = true;
			}

			// Check Number Change
			if (!aq.id(R.id.myapptupd_etxt_contnum).getText().toString().trim().equalsIgnoreCase(srvReq.getCtNum().trim())) {
				rSrvReq.setCtNum(aq.id(R.id.myapptupd_etxt_contnum).getText().toString().trim());
				isChangeCtInfo = true;
			}

			// Fill Update Type
			if (isChangeTimeSlot && isChangeCtInfo) {
				rSrvReq.setUpdTy(SrvReq.UP_TY_BOTH);
			} else if (isChangeTimeSlot) {
				rSrvReq.setUpdTy(SrvReq.UP_TY_APPT);
			} else if (isChangeCtInfo) {
				rSrvReq.setUpdTy(SrvReq.UP_TY_CONTACT);
			}
			
			//TODO validate and call update API (check and make sure it works)
			reply = new Reply();
			reply = rSrvReq.verify4UpdateSR();

			if (!reply.isSucc()) {
				DialogHelper.createSimpleDialog(getActivity(), InterpretRCManager.interpretRC_SRMdu(getActivity(), reply.getCode()));
			} else {
				doUpdateSR(rSrvReq);
			}
			break;
		case R.id.myapptupd_btn_cancel:
			DialogHelper.createSimpleDialog(getActivity(), getString(R.string.MYHKT_SR_MSG_CONFIRM_TO_CANCEL), getString(R.string.myhkt_btn_yes), new OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					doCancelSR();
					dialog.dismiss();
				}
			},  getString(R.string.MYHKT_BTN_CANCEL));
			
			
//			DialogHelper.createSimpleDialog(getActivity(), getString(R.string.MYHKT_SR_MSG_CONFIRM_TO_CANCEL), getString(R.string.myhkt_btn_yes), new OnClickListener() {
//				@Override
//				public void onClick(DialogInterface dialog, int which) {
//					doCancelSR();
//					dialog.dismiss();
//				}
//			});
			break;
		}
	}
	
	private void prepareTimeSlot() {
		if (srvReq != null) { //this line is unnecessary if APIs are running normally
			ArrayAdapter<String> aa;
			if ("Y".equalsIgnoreCase(srvReq.getAllowUpdInd()) && srApptInfo != null) {
				// Prepared String Array for showing TimeSlot
				// dd-MMM HH:mm - HH:mm (original)
				SRApptTS[] apptTimeSlotAry = srApptInfo.getApptTSAry();
				timeslotAry = new String[apptTimeSlotAry.length + 1];
				timeslotAry[0] = String.format("%s %s", Utils.getTimeSlotDateTime1(getActivity(), srvReq.getApptTS().getApptDate(), srvReq.getApptTS().getApptTmslot()), getString(R.string.MYHKT_APPT_LBL_SELECTED_TIMESLOT));
	
				for (int i = 0; i < apptTimeSlotAry.length; i++) {
					SRApptTS rSRApptTS = apptTimeSlotAry[i];
					timeslotAry[i + 1] = Utils.getTimeSlotDateTime1(getActivity(), rSRApptTS.getApptDate(), rSRApptTS.getApptTmslot());
				}
			} else {
				// dd-MMM HH:mm - HH:mm (original)
				timeslotAry = new String[] { String.format("%s %s", Utils.getTimeSlotDateTime1(getActivity(), srvReq.getApptTS().getApptDate(), srvReq.getApptTS().getApptTmslot()), getString(R.string.MYHKT_APPT_LBL_SELECTED_TIMESLOT)) };
			}
		}
//		timeslotAry = new String[] {"02-Mar 14:00 - 16:00", "02-Mar 16:00 - 18:00", "02-Mar 18:00 - 20:00", "02-Mar 20:00 - 22:00", "03-Mar 10:00 - 13:00", "03-Mar 14:00 - 16:00"}; //testing
	}
	
	private void setSelectedTime() {
//		timePopover.setText(timeAry[timePos]);
		aq.id(R.id.myapptupd_spinner).text(timeslotAry[timePos]);
	}
	
	private final int getSrvLogo(SrvReq mySRappt) {
		boolean isLTS = false;
		boolean isEYE = false;
		boolean isPCD = false;
		boolean isTV = false;
		int logo = 0;

		if (SrvReq.OUT_PROD_VCE.equalsIgnoreCase(mySRappt.getOutProd())) {
			isLTS = true;
		} else if (SrvReq.OUT_PROD_PCD.equalsIgnoreCase(mySRappt.getOutProd())) {
			isPCD = true;
		} else if (SrvReq.OUT_PROD_EYE.equalsIgnoreCase(mySRappt.getOutProd())) {
			isEYE = true;
		} else if (SrvReq.OUT_PROD_VI.equalsIgnoreCase(mySRappt.getOutProd())) {
			isTV = true; // refer to csp website
		}
		
		if (isLTS) {
			logo = R.drawable.lob_eye_lts_plain; 	 	// LTS
		} else if (isEYE) {
			logo = R.drawable.lob_eye_lts_plain;		// EYE
		} else if (isPCD) {
			logo = R.drawable.lob_pcd_plain; 	 	// PCD
		} else if (isTV){
			logo = R.drawable.lob_tv_plain; 	 	// TV
		}

		return logo;
	}
	
	private void doCancelSR() {
		//Cancel SR API
		ApptCra rApptCra = new ApptCra();
		rApptCra.setILoginId(ClnEnv.getQualSvee().getSveeRec().loginId);
		rApptCra.setICustRec(ClnEnv.getQualSvee().getCustRec().copyMe());
		rApptCra.setISrvReq(srvReq.copyMe());
		rApptCra.getISrvReq().setSmsLang(isZh ? "zh" : "en");

		APIsManager.doCancelSR(me, rApptCra);
	}
	
	private void doUpdateSR(SrvReq srvReq) {
		//Update SR API
		ApptCra rApptCra = new ApptCra();
		rApptCra.setILoginId(ClnEnv.getQualSvee().getSveeRec().loginId);
		rApptCra.setICustRec(ClnEnv.getQualSvee().getCustRec().copyMe());
		rApptCra.setISrvReq(srvReq);

		APIsManager.doUpdateSR(me, rApptCra);
	}
	
    protected void setModuleId(){
    	String lob = "" ;
    	if (srvReq != null) {
    		lob = srvReq.getAssocSubnRec().lob;  
    	} 	
		if (SubnRec.LOB_LTS.equals(lob)) { callback_livechat.setModuleId(getResString(R.string.MODULE_LTS_APPTUP)); }
		if (SubnRec.LOB_PCD.equals(lob)) { callback_livechat.setModuleId(getResString(R.string.MODULE_PCD_APPTUP)); }
		if (SubnRec.LOB_TV.equals(lob)) { callback_livechat.setModuleId(getResString(R.string.MODULE_TV_APPTUP)); }
			
		super.setModuleId();
	}
    
	@Override
	public void onSuccess(APIsResponse response) {		
		if (APIsManager.SR_UPD.equals(response.getActionTy())) {
			ApptCra apptcra = (ApptCra) response.getCra();

			OnClickListener onPositiveClickListener = new OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					dialog.dismiss();
					callback_main.fragmentBack();
				}
			};
			DialogHelper.createSimpleDialog(getActivity(), getString(R.string.MYHKT_SR_MSG_UPDATE_SUCCESS), getString(R.string.btn_ok), onPositiveClickListener);
		} else if (APIsManager.SR_CLS.equals(response.getActionTy())) {
			ApptCra apptcra = (ApptCra) response.getCra();

			OnClickListener onPositiveClickListener = new OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					dialog.dismiss();
					callback_main.fragmentBack();
				}
			};
			DialogHelper.createSimpleDialog(getActivity(), getString(R.string.MYHKT_SR_MSG_CANCEL_SUCCESS), getString(R.string.btn_ok), onPositiveClickListener);
		}

	}

	@Override
	public void onFail(APIsResponse response) {
		// General Error Message
		if (!"".equals(response.getMessage()) && response.getMessage() != null) {
			DialogHelper.createSimpleDialog(getActivity(),  response.getMessage());
		} else if (RC.TCSESS_MM.equalsIgnoreCase(response.getReply().getCode())) {
			BaseActivity.ivSessDialog();
		} else {
			DialogHelper.createSimpleDialog(getActivity(),  ClnEnv.getRPCErrMsg(getActivity(), response.getReply().getCode()));
		}		
	}

}
