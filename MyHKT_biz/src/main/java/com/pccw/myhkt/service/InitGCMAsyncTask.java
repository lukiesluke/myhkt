package com.pccw.myhkt.service;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.pccw.myhkt.ClnEnv;

public class InitGCMAsyncTask extends AsyncTask<Context, Void, String> {
	private final Context		cxt;
	private Handler				handler;
	public static String		asyncid			= "INITGCM";
	
	private GoogleCloudMessaging gcm;
	private String regId;		//registration ID from GCM
	
	public InitGCMAsyncTask(Context cxt, Handler handler) {
		super();
		this.cxt = cxt;
		this.handler = handler;
	}
	
	@Override
	protected String doInBackground(Context... params) {
		String msg = "";
		try{
            if (gcm == null) {
                gcm = GoogleCloudMessaging.getInstance(params[0]);
            }
            regId = gcm.register(ClnEnv.GCM_SENDER_ID);
            msg = "Device registered, registration ID=" + regId;
            return regId;
		}catch(Exception e){
			  Log.e("GCMRegistrar", e.toString());
			  return "";
		}
	}
	
	@Override
	protected void onPostExecute(String regId) {
		Bundle bundle = new Bundle();
		bundle.putString("CALLER", asyncid);
		bundle.putString("REGID", regId);
		Message msg = new Message();
		msg.setData(bundle);

		handler.sendMessage(msg);
	}
	

}