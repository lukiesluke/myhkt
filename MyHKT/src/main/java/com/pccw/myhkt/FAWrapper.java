package com.pccw.myhkt;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.pccw.myhkt.util.Constant;

public class FAWrapper {

    private static FirebaseAnalytics mFirebaseAnalytics;
    private static FAWrapper mFAWrapper;

    public static synchronized FAWrapper getInstance() {
        if (mFAWrapper == null){
            mFAWrapper = new FAWrapper();
        }
        return mFAWrapper;
    }

    public void sendFAScreen(Activity activity, int screenName, boolean isPPS) {
        if(mFirebaseAnalytics == null) {
            mFirebaseAnalytics = FirebaseAnalytics.getInstance(activity);
        }

        mFirebaseAnalytics.setCurrentScreen(activity, Utils.getString(activity, screenName), null);
    }

    public void setPRDProperty(Activity activity) {
        if(mFirebaseAnalytics == null) {
            mFirebaseAnalytics = FirebaseAnalytics.getInstance(activity);
        }

        mFirebaseAnalytics.setUserProperty("Environment", "PRD");
    }

    public void setUATProperty(Activity activity) {
        if(mFirebaseAnalytics == null) {
            mFirebaseAnalytics = FirebaseAnalytics.getInstance(activity);
        }

        mFirebaseAnalytics.setUserProperty("Environment", "UAT");
    }

    public void sendFAEvents(Activity activity, int category, int action, int label,
                             boolean isPPS) {
        if(mFirebaseAnalytics == null) {
            mFirebaseAnalytics = FirebaseAnalytics.getInstance(activity);
        }
        String item_name = Utils.getString(activity, label);
        Bundle param = new Bundle();
        Log.d(this.getClass().getSimpleName() + ".sendFAEvents", FirebaseAnalytics.Param.ITEM_NAME + ", " + item_name);
        if(item_name != null) {
            param.putString(FirebaseAnalytics.Param.ITEM_NAME, item_name);
            mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, param);
        }
    }

    public void sendFAEvents(Activity activity, int category, int action, int label, String params,
                             boolean isPPS) {
        if(mFirebaseAnalytics == null) {
            mFirebaseAnalytics = FirebaseAnalytics.getInstance(activity);
        }
        String item_name = Utils.getString(activity, label) + " " + params;
        Bundle param = new Bundle();
        Log.d(this.getClass().getSimpleName() + ".sendFAEvents", FirebaseAnalytics.Param.ITEM_NAME + ", " + item_name);
        param.putString(FirebaseAnalytics.Param.ITEM_NAME, item_name);
        mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, param);
    }

    public void sendFAEvents(Activity activity, String userType) {
        if (mFirebaseAnalytics == null) {
            mFirebaseAnalytics = FirebaseAnalytics.getInstance(activity);
        }
        Bundle param = new Bundle();
        if (Constant.USER_TYPE_UAT.equalsIgnoreCase(userType)) {
            param.putString("Environment", Constant.USER_TYPE_UAT);
        } else if (Constant.USER_TYPE_PRD.equalsIgnoreCase(userType)) {
            param.putString("Environment", Constant.USER_TYPE_PRD);
        }

        param.putString("OS", Constant.USER_TYPE_OS);
        mFirebaseAnalytics.logEvent("Environment", param);
    }
}
