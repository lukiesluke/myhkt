/*
    Class for Extra Tool
    
    Keywords
    --------
    $URL: svn://10.87.120.207/dango/rel/v16.0/src/com/pccw/dango/shared/tool/MyTool.java $
    $Rev: 1123 $
    $Date: 2018-07-17 15:12:03 +0800 (¶g¤G, 17 ¤C¤ë 2018) $
    $Author: alpha.lau $
*/

package com.pccw.dango.shared.tool;

import java.io.Serializable;
import java.util.Date;

import com.pccw.dango.shared.entity.BiTx;
import com.pccw.dango.shared.entity.IbxMsgRec;
import com.pccw.dango.shared.entity.SubnRec;
import com.pccw.dango.shared.entity.SveeRec;
import com.pccw.wheat.shared.tool.Alma;
import com.pccw.wheat.shared.tool.Const;
import com.pccw.wheat.shared.tool.Tool;


public class MyTool implements Serializable
{
    private static final long serialVersionUID = -4920017781342401603L;


    public static void main(String rArg[])
    {
        System.out.println(getVer());
        return;
    }
    
    
    public static String getVer()
    {
        return ("$URL: svn://10.87.120.207/dango/rel/v16.0/src/com/pccw/dango/shared/tool/MyTool.java $, $Rev: 1123 $");
    }
    
    
    public static boolean isVaMob(String rMob)
    {
        return (DirNum.isValidMob(rMob));
    }
    
    
    public static boolean isVaTel(String rTel)
    {
        return (DirNum.isValidTel(rTel));
    }
    
    
    public static boolean isVaDirNum(String rDN)
    {
        return (DirNum.isVaDirNum(rDN));
    }
    
    
    public static boolean isValidPwdCombn(String rPwd)
    {
        char                        rC;
        int                         rUAlp;
        int                         rLAlp;
        int                         rNum;
        
        int                         rx, ri, rl;
        
        rl = rPwd.length();
        
        rUAlp = 0;
        rLAlp = 0;
        rNum  = 0;
        
        for (rx=0; rx<rl; rx++) {
            rC = rPwd.charAt(rx);
            
            if (rC >= 'A' && rC <= 'Z') rUAlp++;
            else if (rC >= 'a' && rC <= 'z') rLAlp++;
            else if (rC >= '0' && rC <= '9') rNum++;
            else return (false);
        }
        
        if (rUAlp > 0 && rLAlp > 0 && rNum > 0) return (true);
        
        return (false);
    }
    
    
    public static boolean isYOrN(String rYOrN)
    {
        return (Tool.isInParm(rYOrN, Const.Y, Const.N));
    }
    
    
    public static boolean isYesOrNo(String rYesOrNo)
    {
        return (Tool.isInParm(rYesOrNo, Const.YES, Const.NO));
    }
    
    
    public static boolean isActiveOrInactive(String rStatus)
    {
        return (Tool.isInParm(rStatus, Const.ACTIVE, Const.INACTIVE));
    }
    
    
    public static boolean isVaLang(String rLang)
    {
        return (BiTx.isValidLang(rLang));
    }
    
    
    public static boolean isVaEmail(String rEmail)
    {
        return (Tool.isEmailFmt(rEmail, SveeRec.MIN_DOMAIN_LVL));
    }
    
    
    public static boolean isValidDate(String rVar)
    {
        /*
            Validate a String, return true if it is a valid date.
            NOTE, rVar should be in the format of Alma (Timestamp)
        */
    
        return (Alma.isValid(rVar));
    }
    
    
    public static String getLastToken(String rStr, String rSep)
    {
        String                      rAry[];
        int                         rx, ri, rl;
        
        rAry = Tool.break2Ary(rStr, rSep);
        if ((rx=rAry.length) > 0) {
            return (rAry[rx-1]);
        }
        
        return ("");
    }
    
    
    public static String slashDMY2YMD(String rDMY)
    {
        /* format DD/MM/YYYY to Date YYYYMMDD */
        
        String                      rDD;
        String                      rMM;
        String                      rYYYY;
        String                      rYMD;
        
        if (rDMY.length() == 10) {
            rDD   = rDMY.substring(0, 2);
            rMM   = rDMY.substring(3, 5);
            rYYYY = rDMY.substring(6, 10);
            
            rYMD = rYYYY + rMM + rDD;
            if (isValidDate(rYMD + "000000")) {
                return (rYMD);
            }
        }
        
        return ("");
    }


    //derek
//    public static String formatDate(String rStr)
//    {
//        return (formatDate(rStr, Pictures.SLASH_DATE));
//    }
//
//
//    public static String formatDate(String rStr, String rPic)
//    {
//
//        Date                        rDate;
//        String                      rRes;
//
//        try {
//            if (rStr.equals("00000000000000")) {
//                rRes = Pictures.DASH;
//            }
//            else {
//
//                if (rStr.length() == 8)
//                    rStr = rStr + "000000";
//
//                rDate = Alma.toDate(rStr);
//                rRes  = DateTimeFormat.getFormat(rPic).format(rDate);
//            }
//        }
//        catch (Exception rEX) {
//            rRes = Pictures.DASH;
//        }
//
//        return (rRes);
//    }


//    public static String formatDollar(String rStr, String rFmt)
//    {
//        double                      rVal;
//
//        try {
//            if (!Tool.isNil(rStr))
//            {
//                rVal = Double.parseDouble(rStr);
//                if (rVal >= 0)
//                    rStr = Pictures.DOLLAR_SIGN + NumberFormat.getFormat(rFmt).format(rVal);
//                else {
//                    rVal = rVal * -1;
//                    rStr = "-" + Pictures.DOLLAR_SIGN + NumberFormat.getFormat(rFmt).format(rVal);
//                }
//
//            }
//        }
//        catch (Exception rEX) {
//        }
//        return rStr;
//    }
//

//    public static String formatNum(String rStr, String rFmt)
//    {
//        double                      rVal;
//
//        try {
//            if (!Tool.isNil(rStr))
//            {
//                rVal = Double.parseDouble(rStr);
//                rStr = NumberFormat.getFormat(rFmt).format(rVal);
//            }
//        }
//        catch (Exception rEX) {
//        }
//        return rStr;
//    }


    public static String formatUnt(String rStr, String rFmt)
    {
        try {
            if (!Tool.isNil(rStr))
            {
                rStr = Tool.format("%s %s", rStr, rFmt);
            }
            else rStr = Pictures.DASH;
        }
        catch (Exception rEX) {
        }
        return rStr;
    }

    
    public static String format4Nil(String rStr)
    {
        return format4Nil(rStr, Pictures.DASH + Pictures.DASH);
    }
    
    
    public static String format4Nil(String rStr, String rPic)
    {
        if (Tool.isNil(rStr)) {
            /* Avoid rStr == null */
            return (rPic);
        }
        else {
            if (Tool.isNil(rStr.trim())) {
                return (rPic);
            }
        }
        
        return (rStr);
    }


    public static String maskTail(String rVar, char rMask, int rShowLen)
    {
        StringBuilder               rSB;
        int                         rx, ri, rl;
        
        rl = rVar.length();
        if (rl < rShowLen) return (rVar);
        
        rSB = Tool.getDefaultSB(rl);
        for (rx=0; rx<rl; rx++) {
            if (rx < rShowLen) rSB.append(rVar.charAt(rx));
            else rSB.append(rMask);
        }
        
        return (rSB.toString());
    }
    
    
    public static String maskHead(String rVar, char rMask, int rShowLen)
    {
        StringBuilder               rSB;
        int                         rx, ri, rl;
        
        rl = rVar.length();
        if (rl < rShowLen) return (rVar);
        
        ri = rl - rShowLen;
        rSB = Tool.getDefaultSB(rl);
        for (rx=0; rx<rl; rx++) {
            if (rx < ri) rSB.append(rMask);
            else rSB.append(rVar.charAt(rx));
        }
        
        return (rSB.toString());
    }


//    public static void doShowFAQ(SubnRec rSubnRec, boolean rIsComm)
//    {
//        String                      rUrl;
//
//        if (rSubnRec.systy.equals(SubnRec.SYSTY_LTS)) {
//
//            if (rIsComm)
//                rUrl = ClnEnv.getBiTxMap().get(Msgs.SUPP_URL_LTS_4COMM);
//            else
//                rUrl = ClnEnv.getBiTxMap().get(Msgs.SUPP_URL_LTS);
//        }
//        else if (rSubnRec.systy.equals(SubnRec.SYSTY_IMS)) {
//
//            if (rSubnRec.lob.equals(SubnRec.LOB_TV))
//                rUrl = ClnEnv.getBiTxMap().get(Msgs.SUPP_URL_TV);
//            else {
//
//                if (rIsComm)
//                    rUrl = ClnEnv.getBiTxMap().get(Msgs.SUPP_URL_PCD_4COMM);
//                else
//                    rUrl = ClnEnv.getBiTxMap().get(Msgs.SUPP_URL_PCD);
//            }
//        }
//        else if (rSubnRec.systy.equals(SubnRec.SYSTY_MOB)) {
//
//            if (rSubnRec.lob.equals(SubnRec.LOB_IOI))
//                rUrl = ClnEnv.getBiTxMap().get(Msgs.SUPP_URL_101);
//            else
//                rUrl = ClnEnv.getBiTxMap().get(Msgs.SUPP_URL_MOB);
//
//        }
//        else if (rSubnRec.systy.equals(SubnRec.SYSTY_CSL)) {
//
//            if (rSubnRec.lob.equals(SubnRec.LOB_O2F))
//                rUrl = ClnEnv.getBiTxMap().get(Msgs.SUPP_URL_MOB);
//            else
//                rUrl = ClnEnv.getBiTxMap().get(Msgs.SUPP_URL_101);
//
//        }
//        else {
//
//            return;
//        }
//
//        MyGwtTool.launchBrowser(rUrl);
//    }




    public static String getUsgBarCol(int rRemain)
    {
        String                      rCol;
        
        if (rRemain >= 21) rCol = "#3184D7"; /* Blue */
        else if (rRemain >= 10) rCol = "#FFCC00"; /* Yellow */
        else rCol = "#FF3300"; /* Red */

        return rCol;
    }
    
    
    public static int getIbxUnrdCnt(IbxMsgRec[] rAry)
    {
        int                         rx, ri, rl;

        ri = 0;
        
        if (rAry == null) return ri;
        
        for (rx=0; rx<rAry.length; rx++) {
         
            if (!Tool.isOn(rAry[rx].readSts)) ri++;
        }
        
        return (ri);
    }
}    
