package com.pccw.myhkt.fragment;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.Thread.UncaughtExceptionHandler;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import android.app.Activity;
import android.graphics.Point;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.fragment.app.FragmentTransaction;
import androidx.viewpager.widget.ViewPager;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.pccw.dango.shared.cra.AddOnCra;
import com.pccw.dango.shared.cra.PlanMobCra;
import com.pccw.dango.shared.entity.SubnRec;
import com.pccw.dango.shared.g3entity.G3DisplayServiceItemDTO;
import com.pccw.dango.shared.g3entity.G3DisplayServiceItemResultDTO;
import com.pccw.dango.shared.tool.RC;
import com.pccw.myhkt.APIsManager;
import com.pccw.myhkt.APIsResponse;
import com.pccw.myhkt.ClnEnv;
import com.pccw.myhkt.DialogHelper;
import com.pccw.myhkt.InterpretRCManager;
import com.pccw.biz.myhkt.R;
import com.pccw.myhkt.Utils;
import com.pccw.myhkt.activity.BaseActivity;
import com.pccw.myhkt.fragment.BaseServiceFragment.OnServiceListener;
import com.pccw.myhkt.fragment.UsageDataChild1Fragment.OnUsageDataListener;
import com.pccw.myhkt.fragment.UsageDataChild1Fragment.OnUsageListener;
import com.pccw.myhkt.lib.ui.AAQuery;
import com.pccw.myhkt.model.AcctAgent;
import com.pccw.myhkt.pageindicator.CirclePageIndicator;

public class UsageDataFragment extends BaseServiceFragment  implements OnUsageDataListener{
	private UsageDataFragment me;
	private View myView;
	private AAQuery aq;
	private int deviceWidth ;
	private int extralinespace;
	private int colWidth ;
	private OnUsageListener callback_usage;
	private UsageDataChild1Fragment usageDataChild1Fragment;
	private UsageDataChild2Fragment usageDataChild2Fragment;
	private ViewPager viewPager;
	private PlanMobCra planMobCra; 
	private AddOnCra addOnCra;
	private List<G3DisplayServiceItemDTO> localServiceList;
	private List<G3DisplayServiceItemDTO> romServiceList;
	private List<G3DisplayServiceItemDTO> otherLocalServiceList;
	private List<G3DisplayServiceItemDTO> otherRommaServiceList;
	private CirclePageIndicator mIndicator;
	private UsageDataPagerAdapter usageDataPagerAdapter;
	private List<Fragment> fragmentList;
	private Boolean isMob = true;
	private Boolean isCSimPrim = true;
	private Boolean isCSimProgressShown = true;
	private Boolean isLocal = true;
	private String TAG = "UsageDataFragment";

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		try {
			callback_usage = (OnUsageListener)getParentFragment();
		} catch (ClassCastException e) {
			throw new ClassCastException(getParentFragment().toString() + " must implement OnUsageListener");
		}	

	}
	@Override
	public void onCreate(Bundle savedInstanceState) {	
		super.onCreate(savedInstanceState);
		try {
			callback_main = (OnServiceListener) this.getActivity();
		} catch (ClassCastException e) {
			throw new ClassCastException(this.getActivity() + " must implement OnServiceListener");
		}
//		isMob = true; //derek test
		isMob = (SubnRec.LOB_MOB.equals(callback_main.getAcctAgent().getSubnRec().lob) || SubnRec.LOB_IOI.equals(callback_main.getAcctAgent().getSubnRec().lob) || SubnRec.WLOB_XMOB.equals(callback_main.getAcctAgent().getSubnRec().lob));
		isLocal = callback_usage.getIsLocal();
		fragmentList = new ArrayList<Fragment>();		
		usageDataChild1Fragment = new UsageDataChild1Fragment();
		usageDataChild2Fragment = new UsageDataChild2Fragment();
		fragmentList.add(usageDataChild1Fragment);
		fragmentList.add(usageDataChild2Fragment);
	}

	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		me = this;		
		View fragmentLayout = inflater.inflate(R.layout.fragment_usagedata, container, false);
		myView = fragmentLayout;
		initData();		
		initUI();
		return fragmentLayout;
	}

	@Override
	public void onStart() {
		super.onStart();
//		// Screen Tracker
//		Tracker tracker = GoogleAnalytics.getInstance(getActivity()).getTracker(ClnEnv.getTrackerId(getActivity(), false));
//		tracker.send(MapBuilder.createAppView().set(Fields.SCREEN_NAME, Utils.getString(getActivity(), R.string.CONST_SCRN_MOBUSAGE)).build());
//		UncaughtExceptionHandler myHandler = new ExceptionReporter(tracker, GAServiceManager.getInstance(), Thread.getDefaultUncaughtExceptionHandler(), getActivity().getBaseContext());
//		Thread.setDefaultUncaughtExceptionHandler(myHandler);
	}


	protected void initData() {
		aq = new AAQuery(myView);	
		Display display = me.getActivity().getWindowManager().getDefaultDisplay();
		Point size = new Point();
		display.getSize(size);
		deviceWidth = size.x;
		extralinespace = (int) getResDimen(R.dimen.extralinespace);
		basePadding = (int) getResources().getDimension(R.dimen.basePadding);
		colWidth = (deviceWidth - basePadding *2 )/4;
	}

	protected void initUI(){
		
		usageDataPagerAdapter = new UsageDataPagerAdapter(getChildFragmentManager(), fragmentList);
		viewPager = (ViewPager) aq.id(R.id.fragment_usage_viewpager).getView();
		viewPager.setAdapter(usageDataPagerAdapter);		
		//		if (isLocal) {
		mIndicator = (CirclePageIndicator) aq.id(R.id.fragment_usage_indicator).getView();
		mIndicator.setViewPager(viewPager);
		mIndicator.setCurrentItem(0);
		//			mIndicator.setVisibility(View.VISIBLE);
		//		} else {
		//			mIndicator.setVisibility(View.GONE);
		//		}
	}

	private void setUI(){
//		SimpleDateFormat nextDf = new SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH);
//		Date nextDate = Calendar.getInstance().getTime();
//		//		try {
//		//			nextDate = nextDf.parse(planMobCra.getOMobUsage().getG3AcctBomNextBillDateDTO().getBomNextBillDate());
//		//		} catch (ParseException e) {
//		//			Log.i(TAG, "Set 21A" + e.toString());
//		//			e.printStackTrace();
//		//		}
//		SimpleDateFormat df = new SimpleDateFormat(Utils.getString(me.getActivity(), R.string.usage_date_format), Locale.ENGLISH);
//		String nextBillDate = df.format(nextDate);
//
//		//Detail part
//		String detail ="";
//		if (isMob) {
//			detail = getResString(R.string.csl_usage_subscribed_plan) + Utils.convertStringToPrice1dp(planMobCra.getOMobUsage().getRatePlan().getFee()) + 
//					getResString(R.string.csl_usage_per_month) + "\n"+getResString(R.string.usage_next_bill_date) + " " +nextBillDate;
//		} else {
//			//			Utils.toDateString(planMobCra.getOMobUsage().getXMobBillBalDetail().getG3OutstandingBalanceDTO().getContractEndDate().toString(), "yyyy-MM-dd hh:mm:ss.S", "dd/MM/yyyy")
//			detail =  getResString(R.string.csl_usage_contract_end_date) + "--/--/----" +
//					"\n"+getResString(R.string.usage_next_bill_date) + " " +nextBillDate;
//		}
//
//		aq.id(R.id.fragment_usage_bot_layout).backgroundColorId(R.color.cell_bg_grey);
//		aq.padding(R.id.fragment_usage_bot_layout, extralinespace, extralinespace, extralinespace, extralinespace);
//		aq.normText(R.id.fragment_usage_detail , detail, -2);
//		aq.gravity(R.id.fragment_usage_detail, Gravity.CENTER|Gravity.LEFT);
//		aq.id(R.id.fragment_usage_detail).height(LinearLayout.LayoutParams.WRAP_CONTENT).backgroundColorId(R.color.cell_bg_grey);
//		aq.id(R.id.fragment_usage_remark).visibility(isMob? View.GONE : View.VISIBLE);
//		Log.i(TAG, "Set 3A-" + fragmentList.size());

		usageDataPagerAdapter = new UsageDataPagerAdapter(getChildFragmentManager(), fragmentList);

		viewPager = (ViewPager) aq.id(R.id.fragment_usage_viewpager).getView();
//		FragmentTransaction ft = getChildFragmentManager().beginTransaction();  
//		ft.remove(usageDataChild1Fragment);
//		ft.remove(usageDataChild2Fragment);
//		ft.detach(usageDataChild1Fragment);
//		ft.detach(usageDataChild2Fragment);
//		ft.commit();
		if (debug) Log.i(TAG, "Set 4A"+usageDataPagerAdapter.getCount());
		//		if (viewPager.getAdapter() == null) {
//		viewPager.removeAllViews();
		viewPager.setAdapter(usageDataPagerAdapter);
		//		}
		if (((UsageDataPagerAdapter)viewPager.getAdapter()).getItem(0) instanceof UsageDataChild1Fragment){
			if (debug) Log.i(TAG, "Set 5A"+ true);
		} else {
			if (debug) Log.i(TAG, "Set 5A"+ false);   
		}

		usageDataPagerAdapter.notifyDataSetChanged();
		((BaseServiceFragment)fragmentList.get(0)).refresh();
		mIndicator = (CirclePageIndicator) aq.id(R.id.fragment_usage_indicator).getView();
		mIndicator.setViewPager(viewPager);
		mIndicator.setCurrentItem(0);
	}

	// call on onResume , we have to check if it is on currentPage
	public void refreshData(){
		super.refreshData();
		if ((isLocal && callback_usage.getActiveChildview() == R.string.CONST_SELECTEDFRAG_USAGEDATA && (callback_main.getActiveSubview() == R.string.CONST_SELECTEDVIEW_MOBUSAGE || callback_main.getActiveSubview() == R.string.CONST_SELECTEDVIEW_1010USAGE)) 
				|| (!isLocal && callback_usage.getActiveChildview() == R.string.CONST_SELECTEDFRAG_ROAMING && (callback_main.getActiveSubview() == R.string.CONST_SELECTEDVIEW_MOBROAMING || callback_main.getActiveSubview() == R.string.CONST_SELECTEDVIEW_1010ROAMING))) {
			if (debug) Log.i(TAG, "Refresh1");
			PlanMobCra sMlanMobcra = new PlanMobCra();
			sMlanMobcra.setILoginId(ClnEnv.isLoggedIn() ? ClnEnv.getQualSvee().getSveeRec().loginId : "");
			sMlanMobcra.setISubnRec(callback_main.getAcctAgent().getSubnRec());
			APIsManager.doGetPlanMob(me, sMlanMobcra);		
			//			}
		} 		
	}

	public void refresh(){
		super.refresh();
		PlanMobCra sMlanMobcra = new PlanMobCra();
		sMlanMobcra.setILoginId(ClnEnv.isLoggedIn() ? ClnEnv.getQualSvee().getSveeRec().loginId : "");
		sMlanMobcra.setISubnRec(callback_main.getAcctAgent().getSubnRec());
		APIsManager.doGetPlanMob(me, sMlanMobcra);
	}

	// View Pager Class
	private class UsageDataPagerAdapter extends FragmentPagerAdapter {
		private List<Fragment> fragmentList;
		private FragmentManager fragmentManager;
		public UsageDataPagerAdapter(FragmentManager fragmentManager ,List<Fragment> fragments) {
			super(fragmentManager);
			this.fragmentManager = fragmentManager;
			fragmentList = fragments;
		}

		@Override
		public int getCount() {
			return fragmentList.size();
		}
//		@Override
//		public void destroyItem(ViewGroup viewPager, int position, Object object) {
//			viewPager.removeView((View) object);
//		}
		// Returns the fragment to display for that page
		@Override
		public Fragment getItem(int position) {
			switch (position) {
			case 0: // Fragment # 0 - This will show FirstFragment
				return fragmentList.get(position);
			case 1: // Fragment # 1 - This will show FirstFragment different title
				return fragmentList.get(position);
			case 2: // Fragment # 1 - This will show FirstFragment different title
				return fragmentList.get(position);
			default:
				return null;				
			}
		}
//		@Override  
//		public boolean isViewFromObject(View arg0, Object arg1) {  
//			// TODO Auto-generated method stub  
//			return arg0 == arg1;   
//		}  

		@Override  
		public Object instantiateItem(ViewGroup container, int position) {  
			if (debug) Log.i(TAG, "instantiateItem:"+position);  
			Fragment cacheFragment = (Fragment) super.instantiateItem(container, position);  
			Fragment newFragment  = getItem(position);  
			String fragmentTag = cacheFragment.getTag();  
			if (cacheFragment != newFragment) {
				FragmentTransaction ft = fragmentManager.beginTransaction();  
				ft.remove(cacheFragment);  
				ft.remove(newFragment);  
				ft.add(container.getId(), newFragment, fragmentTag);
				ft.attach(newFragment);  
				ft.commit();
			}
			return newFragment;  

		}  

	}


	protected void cleanupUI(){

	}

	private void processList(G3DisplayServiceItemResultDTO g3DisplayServiceItemResultDTO) {
		List<G3DisplayServiceItemDTO> displayServiceItemList = g3DisplayServiceItemResultDTO.getDisplayServiceItemList();
		localServiceList = new ArrayList<G3DisplayServiceItemDTO>(); 
		romServiceList = new ArrayList<G3DisplayServiceItemDTO>();
		otherLocalServiceList = new ArrayList<G3DisplayServiceItemDTO>();
		otherRommaServiceList = new ArrayList<G3DisplayServiceItemDTO>();		
		if (displayServiceItemList != null) {
			for (G3DisplayServiceItemDTO item : displayServiceItemList) {
				if ("MUP Individual Usage".equals(item.getServiceType())) item.setShowInMainPage("Y");
				if ("Y".equalsIgnoreCase(item.getShowInMainPage())) {
					if ("LOCAL".equalsIgnoreCase(item.getRegion())) {
						localServiceList.add(item);
					} else if ("ROAMING".equalsIgnoreCase(item.getRegion())) {
						romServiceList.add(item);
					}
				} else {
					if ("LOCAL".equalsIgnoreCase(item.getRegion())) {
						otherLocalServiceList.add(item);
					} else if ("ROAMING".equalsIgnoreCase(item.getRegion())) {
						otherRommaServiceList.add(item);
					}
				}
			}
		}
	}

	private void apiResultHandle(PlanMobCra planMobCra){
		callback_usage.setPlanMobCra(planMobCra);
		UsageDataChild1Fragment usageDataChild1Fragment = new UsageDataChild1Fragment();
		UsageDataChild2Fragment usageDataChild2Fragment = new UsageDataChild2Fragment();
		UsageDataChild3Fragment usageDataChild3Fragment = new UsageDataChild3Fragment();
		fragmentList = new ArrayList<Fragment>();
        // Progress Bar show
        isCSimProgressShown = planMobCra.getOMobUsage().getG3DisplayServiceItemResultDTO() != null &&
                planMobCra.getOMobUsage().getG3DisplayServiceItemResultDTO().getDisplayServiceItemList() != null &&
                !planMobCra.getOMobUsage().getG3DisplayServiceItemResultDTO().getDisplayServiceItemList().isEmpty() &&
                planMobCra.getOMobUsage().getG3DisplayServiceItemResultDTO().getDisplayServiceItemList().get(0) != null;
		processList(planMobCra.getOMobUsage().getG3DisplayServiceItemResultDTO());
		if (isLocal) {
			if (isMob) {		
				fragmentList.add(usageDataChild1Fragment);
				fragmentList.add(usageDataChild2Fragment);			
			} else {			
				//NON VBP , Secordary MRT , DisplayServiceItemList null (isCsimProgressShown) 
				//will not shown the first page--TopUp Page
				if (!planMobCra.getReply().getCode().equals(RC.CVI_NON_VBP) && !planMobCra.getReply().getCode().equals(RC.CVI_SEC_MRT) && isCSimProgressShown ) {
					fragmentList.add(usageDataChild1Fragment);	
				} else {
				}			
				fragmentList.add(usageDataChild2Fragment);
			}
			if (otherLocalServiceList != null && otherLocalServiceList.size() > 0) {
				fragmentList.add(usageDataChild3Fragment);
			}
		} else {
			if (isMob) {		
				fragmentList.add(usageDataChild1Fragment);
			} else {
//				if (isCSimProgressShown) {
//					fragmentList.add(usageDataChild1Fragment);	
//				}				
				fragmentList.add(usageDataChild2Fragment);
			}
			if (otherRommaServiceList != null && otherRommaServiceList.size() > 0) {
				fragmentList.add(usageDataChild3Fragment);
			}
		}
		mIndicator.setVisibility(fragmentList.size() > 1 ? View.VISIBLE : View.GONE);
		setUI();
	}
	@Override
	public void onSuccess(APIsResponse response) {	
		if (debug) Log.i(TAG, "Success");
		if (APIsManager.PLAN_MOB.equals(response.getActionTy())){
			planMobCra = (PlanMobCra) response.getCra();
			Gson gson = new Gson();	
			if (debug) Utils.showLog(TAG,  gson.toJson(planMobCra));
			//			planLtsCra =  ClnEnv.getJSONObject(this.getActivity(), PlanLtsCra.class, "json/plan_lts.json");
			//			Log.i(TAG, "LTS2:" + gson.toJson(planLtsCra));
			apiResultHandle(planMobCra);		
			
		} else if (APIsManager.AO_AUTH.equals(response.getActionTy())) {
			addOnCra = (AddOnCra) response.getCra();
			addOnCra.getOSubnRec().ivr_pwd = addOnCra.getISubnRec().ivr_pwd;
			
			callback_main.setSubscriptionRec(addOnCra.getOSubnRec());
			callback_main.getAcctAgent().setSubnRec(addOnCra.getOSubnRec());

			PlanMobCra sMlanMobcra = new PlanMobCra();
			sMlanMobcra.setILoginId(ClnEnv.isLoggedIn() ? ClnEnv.getQualSvee().getSveeRec().loginId : "");
			sMlanMobcra.setISubnRec(callback_main.getAcctAgent().getSubnRec());
			APIsManager.doGetPlanMob(me, sMlanMobcra);
		}
	}

	@Override
	public void onFail(APIsResponse response) {	
		if (debug) Log.i(TAG, "onFail");
		if (APIsManager.PLAN_MOB.equals(response.getActionTy()) && response.getCra() != null){
			//TODO 
			//Need to handle the vsdr for Primary and secondary
			planMobCra = (PlanMobCra) response.getCra();
			Gson gson = new Gson();	
			if (debug) Utils.showLog(TAG,  gson.toJson(planMobCra));
			if (!isMob) {
				planMobCra = (PlanMobCra) response.getCra();
				if (planMobCra.getReply().getCode().equals(RC.CVI_NON_VBP) || planMobCra.getReply().getCode().equals(RC.CVI_SEC_MRT)) {
					if (debug) Log.i(TAG, "Error code:" + planMobCra.getReply().getCode());
					apiResultHandle(planMobCra);
				}else {
					if (debug) Log.i(TAG, "Error1");
					if (!"".equals(response.getMessage()) && response.getMessage() != null) {
						DialogHelper.createSimpleDialog(getActivity(),  response.getMessage());
					} else if (RC.TCSESS_MM.equalsIgnoreCase(response.getReply().getCode())) {
						BaseActivity.ivSessDialog();
					} else if (RC.AO_PSPH_FAIL.equalsIgnoreCase(response.getReply().getCode())) {
						recallDoAuth(me, callback_main.getAcctAgent().getSubnRec());
					} else {
						DialogHelper.createSimpleDialog(getActivity(),  InterpretRCManager.interpretRC_BinqMobMdu(getActivity(), response.getReply().getCode(), callback_main.getLob()));
					}	
				}				
			} else {
				//general error
				if (!"".equals(response.getMessage()) && response.getMessage() != null) {
					DialogHelper.createSimpleDialog(getActivity(),  response.getMessage());
				} else if (RC.TCSESS_MM.equalsIgnoreCase(response.getReply().getCode())) {
					BaseActivity.ivSessDialog();
				} else if (RC.AO_PSPH_FAIL.equalsIgnoreCase(response.getReply().getCode())) {
					recallDoAuth(me, callback_main.getAcctAgent().getSubnRec());
				} else {
					DialogHelper.createSimpleDialog(getActivity(),  ClnEnv.getRPCErrMsg(me.getActivity(), response.getReply().getCode()));
				}	
				apiResultHandle(planMobCra);		
			}
		} else {
			// General Error Message
			if (!"".equals(response.getMessage()) && response.getMessage() != null) {
				DialogHelper.createSimpleDialog(getActivity(),  response.getMessage());
			} else if (RC.TCSESS_MM.equalsIgnoreCase(response.getReply().getCode())) {
				BaseActivity.ivSessDialog();
			} else if (RC.AO_PSPH_FAIL.equalsIgnoreCase(response.getReply().getCode())) {
				recallDoAuth(me, callback_main.getAcctAgent().getSubnRec());
			} else {
				DialogHelper.createSimpleDialog(getActivity(),  ClnEnv.getRPCErrMsg(me.getActivity(), response.getReply().getCode()));
			}	
		}
	}



	@Override
	public PlanMobCra getPlanMobCra() {
		return planMobCra;
	}

	@Override
	public void setPlanMobCra(PlanMobCra mplanMobcra) {
		planMobCra = mplanMobcra;
	}
	@Override
	public List<G3DisplayServiceItemDTO> getLocalServiceList() {
		return localServiceList;
	}

	@Override
	public List<G3DisplayServiceItemDTO> getRommaServiceList() {
		return romServiceList;
	}

	@Override
	public List<G3DisplayServiceItemDTO> getOtherLocalServiceList() {
		return otherLocalServiceList;
	}
	@Override
	public List<G3DisplayServiceItemDTO> getOtherRommaServiceList() {
		return otherRommaServiceList;
	}


	@Override
	public AcctAgent getAcctAgent() {
		return callback_main.getAcctAgent();
	}
	

}
