package com.pccw.myhkt.model;

import java.util.ArrayList;

import android.os.Parcel;
import android.os.Parcelable;

public class SRPlan implements Parcelable{

    public String serviceType;
    public String engDestName;
    public String engDestNameExt;
    public String chiDestName;
    public String chiDestNameExt;
    public ArrayList<Rate> rateList = new ArrayList<Rate>();
	
	public SRPlan() { }
	
    public SRPlan(Parcel in) {
        this.serviceType = in.readString();
        this.engDestName = in.readString();
        this.engDestNameExt = in.readString();
        this.chiDestName = in.readString();
        this.chiDestNameExt = in.readString();
        this.rateList = in.readArrayList(Rate.class.getClassLoader());
    }

    public static final SRPlan.Creator CREATOR = new SRPlan.Creator() {
        public SRPlan createFromParcel(Parcel in) {
            return new SRPlan(in);
        }

        public SRPlan[] newArray(int size) {
            return new SRPlan[size];
        }
    };
    
	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.serviceType);
        dest.writeString(this.engDestName);
        dest.writeString(this.engDestNameExt);
        dest.writeString(this.chiDestName);
        dest.writeString(this.chiDestNameExt);
        dest.writeList(this.rateList);
	}

}
