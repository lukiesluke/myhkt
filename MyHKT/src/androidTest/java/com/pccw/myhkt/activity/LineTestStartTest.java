package com.pccw.myhkt.activity;

import android.content.Context;
import android.content.SharedPreferences;
import androidx.test.espresso.intent.Intents;
import androidx.test.rule.ActivityTestRule;
import androidx.test.rule.GrantPermissionRule;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import com.pccw.myhkt.AccountHelper;
import com.pccw.myhkt.R;
import com.pccw.myhkt.base.BaseTest;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static androidx.test.platform.app.InstrumentationRegistry.getInstrumentation;
import static androidx.test.espresso.Espresso.onData;
import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.RootMatchers.isDialog;
import static org.hamcrest.CoreMatchers.anything;

@Ignore
@RunWith(AndroidJUnit4.class)
public class LineTestStartTest extends BaseTest {
    private Context context;
    SharedPreferences shared;


    @Rule
    public ActivityTestRule<MainMenuActivity> activityTestRule = new ActivityTestRule<>(MainMenuActivity.class);
    @Rule
    public GrantPermissionRule permissionRule = GrantPermissionRule.grant(android.Manifest.permission.WRITE_EXTERNAL_STORAGE);

    @Before
    public void setUp(){
        context = getInstrumentation().getTargetContext();
        shared = context.getSharedPreferences(
                "myhkt",
                Context.MODE_PRIVATE);
        Intents.init();
        clickLeftNavBar();
        findStringContaining(context, R.string.myhkt_LGIF_LOGIN).perform(click());
        loginMyCredentialsUAT(AccountHelper.getUATAccount6().getUsername(),
                AccountHelper.getUATAccount6().getPassword(), shared);
        gotoMainMenuItem(1);
    }

    @After
    public void tearDown() {
        executeLogout(context);
        Intents.release();
    }

    @Test
    public void testMyLineTestFixedLineEye0060DisplayUI() {
        clickViewInAdapter(R.id.servicelist_listView, 0);
        delay(10000);
        findComponentByID(R.id.navbar_leftdraw);
        findStringContaining("34104068");
        findStringContaining(context, R.string.myhkt_lts_eye);
        findStringContaining(context, R.string.LTTF_ITO);
        findStringContaining(context, R.string.LTTF_TEST);
        checkLiveChatBtnIsDisplayed();
        clickLeftNavBar();
    }

    @Test
    public void testMyLineTestNetvigatorDisplayUI() {
        clickViewInAdapter(R.id.servicelist_listView, 6);
        delay(10000);
        findComponentByID(R.id.navbar_leftdraw);
        findStringContaining("a60058422");
        findStringContaining(context, R.string.LTTF_ITO);
        findStringContaining(context, R.string.LTTF_TEST);
        checkLiveChatBtnIsDisplayed();
        clickLeftNavBar();
    }

    @Test
    public void testMyLineTestNowTVDisplayUI() {
        clickViewInAdapter(R.id.servicelist_listView, 13);
        delay(10000);
        findComponentByID(R.id.navbar_leftdraw);
        findStringContaining("1068249781");
        findStringContaining(context, R.string.LTTF_ITO);
        findStringContaining(context, R.string.LTTF_TEST);
        checkLiveChatBtnIsDisplayed();
        clickLeftNavBar();
    }

    @Test
    public void testMyLineTestFixedLineEye0060LiveChat() {
        clickViewInAdapter(R.id.servicelist_listView, 0);
        delay(10000);
        checkLiveChat(context, R.string.LIVE_CHAT_DISCLAIMER, R.id.navbar_button_left);
        clickLeftNavBar();
    }

    @Test
    public void testMyLineTestNetvigatorLiveChat() {
        clickViewInAdapter(R.id.servicelist_listView, 6);
        delay(10000);
        checkLiveChat(context, R.string.LIVE_CHAT_DISCLAIMER, R.id.navbar_button_left);
        clickLeftNavBar();
    }

    @Test
    public void testMyLineTestNowTVLiveChat() {
        clickViewInAdapter(R.id.servicelist_listView, 13);
        delay(10000);
        checkLiveChat(context, R.string.LIVE_CHAT_DISCLAIMER, R.id.navbar_button_left);
        clickLeftNavBar();
    }

    @Test
    public void testMyLineTestStartTheTest() {
        clickViewInAdapter(R.id.servicelist_listView, 0);
        delay(10000);
        findStringContaining(context, R.string.LTTF_TEST).perform(click());
        findStringContaining("OK").inRoot(isDialog()).perform(click());
    }

}