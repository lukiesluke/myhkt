package com.pccw.myhkt.fragment;

import java.lang.Thread.UncaughtExceptionHandler;

import android.app.Activity;
import android.os.Bundle;
import androidx.fragment.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.MapsInitializer;
import com.pccw.dango.shared.cra.ShopCra;
import com.pccw.dango.shared.entity.ShopRec;
import com.pccw.dango.shared.tool.RC;
import com.pccw.myhkt.APIsManager;
import com.pccw.myhkt.APIsResponse;
import com.pccw.myhkt.ClnEnv;
import com.pccw.myhkt.DialogHelper;
import com.pccw.biz.myhkt.R;
import com.pccw.myhkt.Utils;
import com.pccw.myhkt.activity.BaseActivity;
import com.pccw.myhkt.lib.ui.AAQuery;
import com.pccw.myhkt.lib.ui.HKTButton;
import com.pccw.myhkt.mapviewballoons.MapFragmentD;

public class ShopMyLocFragment extends BaseShopFragment {
	private ShopMyLocFragment me;
	private AAQuery aq;
	
	private HKTButton myLocBtn;
	
	// Map v2
	private MapFragmentD								mFRaFragment					= null;
//	private static double								myLat;
//	private static double								myLng;
	
	private ShopRec[] shopRecAry = null;

	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		me = this;

		TAG = "ShopMyLocFragment";

		
		View fragmentLayout = inflater.inflate(R.layout.fragment_shopmyloc, container, false);
		return fragmentLayout;
	}
	
	@Override
	public void onStart() {
		super.onStart();
		//Screen Tracker
		
		init();
	}
	
	@Override
	public void onResume(){
		super.onResume();
		int googlePlayServiceState = GooglePlayServicesUtil.isGooglePlayServicesAvailable(getActivity().getApplicationContext());

		if (googlePlayServiceState == ConnectionResult.SUCCESS) {
			try {
				// Initialize Map
				if (mFRaFragment == null) {

					LinearLayout shopmyloc_mapview = getActivity().findViewById(R.id.shopmyloc_mapview);
					shopmyloc_mapview.removeAllViews();

					FragmentTransaction mTransaction = getChildFragmentManager().beginTransaction();
					mFRaFragment = new MapFragmentD();

					Bundle args = new Bundle();
					args.putBoolean("myLoc", true);
					args.putBoolean("isZh", isZh);
					mFRaFragment.setArguments(args);

					mTransaction.add(R.id.shopmyloc_mapview, mFRaFragment);
					mTransaction.commit();
				}
			} catch (Exception e) {
				// Error adding MapView, we cannot proceed
				e.printStackTrace();
				callback_main.popBackStack();
			}

			try {
				MapsInitializer.initialize(getActivity());
			} catch (Exception e) {
				e.printStackTrace();
			}

			try {
				if (shopRecAry != null) {
					initPOIs();
				} else {
					fetchShopList();
				}
			} catch (Exception e) {
				// do nothing
			}
		} else {
			// google play service not available, displaying suitable error message
			getActivity().findViewById(R.id.shopmyloc_button_mylocation).setVisibility(View.GONE);

			// ConnectionResult.SERVICE_MISSING,
			// ConnectionResult.SERVICE_VERSION_UPDATE_REQUIRED,
			// ConnectionResult.SERVICE_DISABLED,
			// ConnectionResult.SERVICE_INVALID
			GooglePlayServicesUtil.getErrorDialog(googlePlayServiceState, getActivity(), -1).show();
		}
	}

	private void init() {
		aq = new AAQuery(getActivity());
		
		aq.id(R.id.shopmyloc_button_mylocation).clicked(this, "onClick");
	}
	
	public void onClick(View v) {
		switch (v.getId()) {
			case R.id.shopmyloc_button_mylocation:
				try {
					if (mFRaFragment != null) {
						mFRaFragment.movetomyloc();
					}
					if (shopRecAry == null) {
						me.fetchShopList();
					}
				} catch (Exception e) {
					e.printStackTrace();
					Toast.makeText(getActivity().getApplicationContext(), getResources().getString(R.string.myhkt_shop_mylocerr), Toast.LENGTH_SHORT).show();
				}
				break;
		}
	}
	
	// Passing shop list info to the map for Pin generation
	private final void initPOIs() {
		if (mFRaFragment != null) {
			mFRaFragment.fetchShopList(shopRecAry);
		}
	}
	
	// Fetching Shop List from CSP API
	private final void fetchShopList() {
//		shopRecAry = callback.getShopRecAry();
		doGetShopByGC();
	}

	
	@Override
	public void onSuccess(APIsResponse response) {
		// TODO Auto-generated method stub
		ShopCra shopcra = (ShopCra) response.getCra();
		shopRecAry = shopcra.getOShopRecAry();
		initPOIs();
	}

	@Override
	public void onFail(APIsResponse response) {
		// General Error Message
		if (!"".equals(response.getMessage()) && response.getMessage() != null) {
			DialogHelper.createSimpleDialog(getActivity(),  response.getMessage());
		} else if (RC.TCSESS_MM.equalsIgnoreCase(response.getReply().getCode())) {
			BaseActivity.ivSessDialog();
		} else {
			DialogHelper.createSimpleDialog(getActivity(),  ClnEnv.getRPCErrMsg(getActivity(), response.getReply().getCode()));
		}		
	}

	
	private void doGetShopByGC() {
		ShopCra shopCra = new ShopCra();
		shopCra.setILatitude(0.0);
		shopCra.setILongitude(0.0);
		
		APIsManager.doGetShopByGC(me, shopCra);
	}

}
