package com.pccw.myhkt.fragment;

import static com.pccw.myhkt.Utils.setCharLimit;
import static com.pccw.myhkt.util.Constant.KEY_NAME_LOB_TYPE;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;

import androidx.annotation.NonNull;

import com.pccw.dango.shared.cra.AddOnCra;
import com.pccw.dango.shared.cra.BiifCra;
import com.pccw.dango.shared.tool.RC;
import com.pccw.myhkt.APIsManager;
import com.pccw.myhkt.APIsResponse;
import com.pccw.myhkt.ClnEnv;
import com.pccw.myhkt.DialogHelper;
import com.pccw.myhkt.FAWrapper;
import com.pccw.myhkt.InterpretRCManager;
import com.pccw.myhkt.R;
import com.pccw.myhkt.Utils;
import com.pccw.myhkt.activity.BaseActivity;
import com.pccw.myhkt.lib.ui.AAQuery;
import com.pccw.myhkt.lib.ui.LRTabButton;
import com.pccw.myhkt.lib.ui.PopOverInputView;
import com.pccw.myhkt.lib.ui.popover.ActionItem;
import com.pccw.myhkt.lib.ui.popover.QuickAction;
import com.pccw.wheat.shared.tool.Reply;

import java.util.Objects;

/************************************************************************
 * File : BillInfoFragment.java
 * Desc : Bill information Fragment
 * Name : BillInfoFragment
 * by 	: Derek Tsui
 * Date : 28/01/2016
 *
 * Change History:
 * Date       Modified By		Description
 * ---------- ----------------	-------------------------------
 * 28/01/2015 Derek Tsui 		-First draft
 * 29/06/2016 Andy Wong			-Update Validation logic
 *************************************************************************/

public class BillInfoFragment extends BaseServiceFragment {
    private BillInfoFragment me;
    private View myView;
    private AAQuery aq;

    private QuickAction areaQuickAction;
    private QuickAction unitQuickAction;
    private QuickAction distQuickAction;
    private QuickAction langQuickAction;
    private QuickAction mediaQuickAction;

    private PopOverInputView unitPopover;
    private PopOverInputView langPopover;
    private PopOverInputView areaPopover;
    private PopOverInputView mediaPopover;
    private LRTabButton lrTabBtn;

    private boolean isLoaded = false;
    private BiifCra biifCra;
    private AddOnCra addOnCra;
    private int lobType = -1;

    @Override
    public void onAttach(@NonNull Activity activity) {
        super.onAttach(activity);
    }

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        me = this;
        View fragmentLayout = inflater.inflate(R.layout.fragment_billinfo, container, false);
        myView = fragmentLayout;
        if (getArguments() != null) {
            lobType = getArguments().getInt(KEY_NAME_LOB_TYPE);
        }
        initData();
        return fragmentLayout;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (callback_main != null && isLoaded) {
            switch (callback_main.getLob()) {
                case R.string.CONST_LOB_LTS:
                case R.string.CONST_LOB_MOB:
                case R.string.CONST_LOB_IOI:
                case R.string.CONST_LOB_CSP:
                    //tracker
                    if (callback_main.getLob() == R.string.CONST_LOB_LTS) {
                        FAWrapper.getInstance().sendFAScreen(getActivity(), R.string.CONST_SCRN_LTSBILLINFO, false);
                    } else if (callback_main.getLob() == R.string.CONST_LOB_CSP) {
                        FAWrapper.getInstance().sendFAScreen(getActivity(), R.string.CONST_SCRN_CSPBILLINFO, false);
                    } else {
                        FAWrapper.getInstance().sendFAScreen(getActivity(), R.string.CONST_SCRN_MOBBILLINFO, false);
                    }
                    break;
                case R.string.CONST_LOB_O2F:
                case R.string.CONST_LOB_1010:
                    //tracker
                    FAWrapper.getInstance().sendFAScreen(getActivity(),
                            R.string.CONST_SCRN_MOBBILLINFO, false);
                    break;
                case R.string.CONST_LOB_PCD:
                case R.string.CONST_LOB_TV:
                    //tracker
                    if (callback_main.getLob() == R.string.CONST_LOB_PCD)
                        FAWrapper.getInstance().sendFAScreen(getActivity(),
                                R.string.CONST_SCRN_PCDBILLINFO, false);
                    else
                        FAWrapper.getInstance().sendFAScreen(getActivity(),
                                R.string.CONST_SCRN_TVBILLINFO, false);
                    break;
            }
        }
    }

    protected void initUI() {
        aq = new AAQuery(myView);
        aq.id(R.id.billinfo_layout).backgroundColorId(R.color.white);

        int imageRes = Utils.theme(R.drawable.billinfo_icon, callback_main.getLob());
        int rImageRes = R.drawable.ios7_question;
        aq.id(R.id.billinfo_header).getTextView().setCompoundDrawablesWithIntrinsicBounds(imageRes, 0, rImageRes, 0);
        aq.id(R.id.billinfo_header).clicked(this, "onClick");

        aq.padding(R.id.billinfo_header_layout, 0, basePadding, 0, basePadding);

        //option tabs
        lrTabBtn = (LRTabButton) aq.id(R.id.billinfo_lrtabbtn).getView();
        lrTabBtn.initViews(me.getActivity(), getString(R.string.csl_billinfo_opt1), getString(R.string.csl_billinfo_opt2), 0, 0);
        aq.id(R.id.billinfo_lrtabbtn).height((int) getResources().getDimension(R.dimen.lr_button_height), false);
        lrTabBtn.setOnLeftClickLisener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Utils.closeSoftKeyboard(getActivity());
                aq.id(R.id.billinfo_layout_opt1).visibility(View.VISIBLE);
                aq.id(R.id.billinfo_layout_opt2).visibility(View.GONE);
            }
        });
        lrTabBtn.setOnRightClickLisener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Utils.closeSoftKeyboard(getActivity());
                aq.id(R.id.billinfo_layout_opt1).visibility(View.GONE);
                aq.id(R.id.billinfo_layout_opt2).visibility(View.VISIBLE);
            }
        });

        //Option 1
        aq.popOverInputView(R.id.billinfo_popover_unit, "Unit");
        aq.normEditText(R.id.billinfo_etxt_num, "", "");
        aq.normEditText(R.id.billinfo_etxt_flr, "", "");
        aq.normEditText(R.id.billinfo_etxt_blk, "", "");
        aq.normEditText(R.id.billinfo_etxt_blding, "", "");
        aq.normEditText(R.id.billinfo_etxt_section, "", "");
        aq.normEditText(R.id.billinfo_etxt_str, "", "");
        aq.normEditText(R.id.billinfo_etxt_strname, "", "");
        aq.normEditText(R.id.billinfo_etxt_dist, "", "");
        aq.normEditText(R.id.billinfo_etxt_email, "", "");

        //popover views
        WindowManager wm = (WindowManager) getActivity().getSystemService(Context.WINDOW_SERVICE);
        int screenWidth = wm.getDefaultDisplay().getWidth();

        //UNIT quickaction
        unitQuickAction = new QuickAction(getActivity(), screenWidth / 2, 0);
        unitPopover = aq.popOverInputView(R.id.billinfo_popover_unit, "----");
        String[] unitAry = {"----", "APT", "FLAT", "ROOM", "SHOP", "SUITE", "UNIT"};
        ActionItem[] unitItems = new ActionItem[unitAry.length];
        for (int i = 0; i < unitAry.length; i++) {
            unitItems[i] = new ActionItem(i, unitAry[i]);
            unitQuickAction.addActionItem(unitItems[i]);
        }
        unitQuickAction.setOnActionItemClickListener(new QuickAction.OnActionItemClickListener() {
            @Override
            public void onItemClick(QuickAction quickAction, int pos, int actionId) {
                ActionItem actionItem = quickAction.getActionItem(pos);
                unitPopover.setText(actionItem.getTitle());
            }
        });
        aq.id(R.id.billinfo_popover_unit).clicked(this, "onClick");

        //AREA quickaction
        areaQuickAction = new QuickAction(getActivity(), screenWidth / 2, 0);
        areaPopover = aq.popOverInputView(R.id.billinfo_popover_area, getResources().getString(R.string.billinfo_hk));
        //appointment alert
        String[] areaAry = {getResources().getString(R.string.billinfo_hk), getResources().getString(R.string.billinfo_kl), getResources().getString(R.string.billinfo_nt), getResources().getString(R.string.billinfo_lt)};
        ActionItem[] areaItems = new ActionItem[areaAry.length];
        for (int i = 0; i < areaAry.length; i++) {
            areaItems[i] = new ActionItem(i, areaAry[i]);
            areaQuickAction.addActionItem(areaItems[i]);
        }
        //setup the action item click listener
        areaQuickAction.setOnActionItemClickListener(new QuickAction.OnActionItemClickListener() {
            @Override
            public void onItemClick(QuickAction quickAction, int pos, int actionId) {
                ActionItem actionItem = quickAction.getActionItem(pos);
                areaPopover.setText(actionItem.getTitle());
            }
        });
        aq.id(R.id.billinfo_popover_area).clicked(this, "onClick");

        //LANGUAGE quickaction
        langQuickAction = new QuickAction(getActivity(), screenWidth / 2, 0);
        langPopover = aq.popOverInputView(R.id.billinfo_popover_lang, Utils.getString(me.requireActivity(), R.string.BUPLTF_BILANGEN));
        langPopover.setBorderColorPopOver(lobType);

        //Lang Items
        String[] langItems;
        if (callback_main.getLob() == R.string.CONST_LOB_1010 || callback_main.getLob() == R.string.CONST_LOB_O2F) {
            langItems = new String[]{Utils.getString(me.requireActivity(), R.string.BUPMBF_BILANGZH), Utils.getString(me.requireActivity(), R.string.BUPMBF_BILANGEN)};
        } else {
            langItems = new String[]{Utils.getString(me.requireActivity(), R.string.BUPLTF_BILANGEN), Utils.getString(me.requireActivity(), R.string.BUPLTF_BILANGZH), Utils.getString(me.requireActivity(), R.string.BUPLTF_BILANGBI)};
        }
        ActionItem[] langItem = new ActionItem[langItems.length];
        for (int i = 0; i < langItems.length; i++) {
            langItem[i] = new ActionItem(i, langItems[i]);
            langQuickAction.addActionItem(langItem[i]);
        }
        langQuickAction.setOnActionItemClickListener(new QuickAction.OnActionItemClickListener() {
            @Override
            public void onItemClick(QuickAction quickAction, int pos, int actionId) {
                ActionItem actionItem = quickAction.getActionItem(pos);
                langPopover.setText(actionItem.getTitle());
            }
        });
        aq.id(R.id.billinfo_popover_lang).clicked(this, "onClick");

        //Bill Media quickaction
        mediaQuickAction = new QuickAction(getActivity(), screenWidth / 2, 0);
        mediaPopover = aq.popOverInputView(R.id.billinfo_popover_billmedia, Utils.getString(me.requireActivity(), R.string.BUPLTF_MEDIA_P));

        //Bill Media Items
        String[] mediaItems;
        mediaItems = new String[]{Utils.getString(me.getActivity(), R.string.BUPLTF_MEDIA_P), Utils.getString(me.getActivity(), R.string.BUPLTF_MEDIA_E)};

        ActionItem[] mediaItem = new ActionItem[mediaItems.length];
        for (int i = 0; i < mediaItems.length; i++) {
            mediaItem[i] = new ActionItem(i, mediaItems[i]);
            mediaQuickAction.addActionItem(mediaItem[i]);
        }
        mediaQuickAction.setOnActionItemClickListener(new QuickAction.OnActionItemClickListener() {
            @Override
            public void onItemClick(QuickAction quickAction, int pos, int actionId) {
                ActionItem actionItem = quickAction.getActionItem(pos);
                mediaPopover.setText(actionItem.getTitle());

                switch (pos) {
                    case 0:
                        aq.id(R.id.billinfo_email_layout).visibility(View.GONE);
                        aq.id(R.id.billinfo_sms_layout).visibility(View.GONE);
                        break;
                    case 1:
                        aq.id(R.id.billinfo_email_layout).visibility(View.VISIBLE);
                        aq.id(R.id.billinfo_sms_layout).visibility(View.VISIBLE);
                        if (biifCra != null && biifCra.getOBiif4Hkt() != null && biifCra.getOBiif4Hkt().getBillMedia() != null && biifCra.getOBiif4Hkt().getBillMedia().equalsIgnoreCase(BiifCra.BILL_MEDIA_S)) {
                            aq.id(R.id.billinfo_etxt_email).text(biifCra.getOBiif4Hkt().getBillEmail());
                            //TODO Fill in the SMS Number
                            //						aq.id(R.id.billinfo_sms_layout).text(biifCra.getOBiif4Hkt().getBillSmsNtfn());
                        } else {
                            aq.id(R.id.billinfo_etxt_email).text(Objects.requireNonNull(ClnEnv.getQualSvee()).getSveeRec().ctMail);
                        }
                        break;
                }
            }
        });
        aq.id(R.id.billinfo_popover_billmedia).clicked(this, "onClick");

        //button layout
        aq.norm2TxtBtns(lobType, R.id.billinfo_btn_back, R.id.billinfo_btn_update, getResString(R.string.MYHKT_BTN_BACK), getResString(R.string.MYHKT_BTN_UPDATE));
        aq.id(R.id.billinfo_btn_back).clicked(this, "onClick");
        aq.id(R.id.billinfo_btn_update).clicked(this, "onClick");

        int charLimit = setCharLimit(callback_main.getLob());

        //Option 2
        aq.id(R.id.billinfo_opt2_custname).text(getResString(R.string.BUPLTF_BI_NAME));
        aq.normEditText(R.id.billinfo_opt2_etxt_addr1, "", "", charLimit);
        aq.normEditText(R.id.billinfo_opt2_etxt_addr2, "", "", charLimit);
        aq.normEditText(R.id.billinfo_opt2_etxt_addr3, "", "", charLimit);
        aq.normEditText(R.id.billinfo_opt2_etxt_addr4, "", "", charLimit);
        aq.normEditText(R.id.billinfo_opt2_etxt_addr5, "", "", charLimit);
        aq.normEditText(R.id.billinfo_opt2_etxt_addr6, "", "", charLimit);

        if (callback_main.getLob() == R.string.CONST_LOB_PCD || callback_main.getLob() == R.string.CONST_LOB_TV) {
            aq.setCharLimit(R.id.billinfo_opt2_etxt_addr1, 48);
            aq.setCharLimit(R.id.billinfo_opt2_etxt_addr2, 48);
            aq.setCharLimit(R.id.billinfo_opt2_etxt_addr3, 50);
            aq.setCharLimit(R.id.billinfo_opt2_etxt_addr4, 50);
            aq.setCharLimit(R.id.billinfo_opt2_etxt_addr5, 50);
            aq.setCharLimit(R.id.billinfo_opt2_etxt_addr6, 50);
        }

        if (callback_main.getLob() == R.string.CONST_LOB_LTS) {
            aq.id(R.id.billinfo_opt2_etxt_addr6).visibility(View.VISIBLE);
            aq.normEditText(R.id.billinfo_opt2_etxt_addr6, "", "");
        } else {
            aq.id(R.id.billinfo_opt2_etxt_addr6).visibility(View.GONE);
        }

        aq.normEditText(R.id.billinfo_etxt_email, "", "");
        aq.normEditText(R.id.billinfo_etxt_sms, "", "");
        aq.id(R.id.billinfo_sms_layout).visibility(View.GONE);

        // Screen Tracker
        //init by lob
        switch (callback_main.getLob()) {
            case R.string.CONST_LOB_LTS:
            case R.string.CONST_LOB_MOB:
            case R.string.CONST_LOB_IOI:
            case R.string.CONST_LOB_CSP:
                //tracker
                if (callback_main.getLob() == R.string.CONST_LOB_LTS) {
                    FAWrapper.getInstance().sendFAScreen(getActivity(),
                            R.string.CONST_SCRN_LTSBILLINFO, false);
                } else if (callback_main.getLob() == R.string.CONST_LOB_CSP) {
                    FAWrapper.getInstance().sendFAScreen(getActivity(),
                            R.string.CONST_SCRN_LTSBILLINFO, false);
                } else
                    FAWrapper.getInstance().sendFAScreen(getActivity(),
                            R.string.CONST_SCRN_MOBBILLINFO, false);

                aq.id(R.id.billinfo_lrtabbtn).visibility(View.GONE);
                aq.id(R.id.billinfo_layout_opt1).visibility(View.GONE);
                aq.id(R.id.billinfo_layout_opt2).visibility(View.VISIBLE);
                aq.id(R.id.billinfo_custname_layout).visibility(View.VISIBLE);
                aq.id(R.id.billinfo_opt2_etxt_addr1).getEditText().setHint("");
                aq.id(R.id.billinfo_opt2_etxt_addr2).getEditText().setHint("");
                aq.id(R.id.billinfo_opt2_etxt_addr3).getEditText().setHint("");
                aq.id(R.id.billinfo_opt2_etxt_addr4).visibility(View.VISIBLE);
                aq.id(R.id.billinfo_opt2_etxt_addr5).visibility(View.VISIBLE);
                aq.id(R.id.billinfo_lang_layout).visibility(View.VISIBLE);
                aq.id(R.id.billinfo_billmedia_layout).visibility(View.GONE);
                aq.id(R.id.billinfo_area_layout).visibility(View.GONE);
                break;
            case R.string.CONST_LOB_O2F:
            case R.string.CONST_LOB_1010:
                //tracker
                FAWrapper.getInstance().sendFAScreen(getActivity(),
                        R.string.CONST_SCRN_MOBBILLINFO, false);

                aq.id(R.id.billinfo_lrtabbtn).visibility(View.VISIBLE);
                aq.id(R.id.billinfo_layout_opt1).visibility(View.VISIBLE);
                aq.id(R.id.billinfo_layout_opt2).visibility(View.GONE);
                aq.id(R.id.billinfo_custname_layout).visibility(View.GONE);
                aq.id(R.id.billinfo_opt2_etxt_addr1).getEditText().setHint(Utils.getString(me.getActivity(), R.string.BUPMBCF_POX_NM));
                aq.id(R.id.billinfo_opt2_etxt_addr2).getEditText().setHint(Utils.getString(me.getActivity(), R.string.BUPMBCF_POX_NO));
                aq.id(R.id.billinfo_opt2_etxt_addr3).getEditText().setHint(Utils.getString(me.getActivity(), R.string.BUPMBCF_DIST));
                aq.id(R.id.billinfo_opt2_etxt_addr4).visibility(View.GONE);
                aq.id(R.id.billinfo_opt2_etxt_addr5).visibility(View.GONE);
                aq.id(R.id.billinfo_lang_layout).visibility(View.VISIBLE);
                aq.id(R.id.billinfo_billmedia_layout).visibility(View.GONE);
                aq.id(R.id.billinfo_area_layout).visibility(View.VISIBLE);
                break;
            case R.string.CONST_LOB_PCD:
            case R.string.CONST_LOB_TV:
                //tracker
                if (callback_main.getLob() == R.string.CONST_LOB_PCD)
                    FAWrapper.getInstance().sendFAScreen(getActivity(),
                            R.string.CONST_SCRN_PCDBILLINFO, false);
                else
                    FAWrapper.getInstance().sendFAScreen(getActivity(),
                            R.string.CONST_SCRN_TVBILLINFO, false);

                aq.id(R.id.billinfo_lrtabbtn).visibility(View.GONE);
                aq.id(R.id.billinfo_layout_opt1).visibility(View.GONE);
                aq.id(R.id.billinfo_layout_opt2).visibility(View.VISIBLE);
                aq.id(R.id.billinfo_custname_layout).visibility(View.VISIBLE);
                aq.id(R.id.billinfo_opt2_etxt_addr1).getEditText().setHint("");
                aq.id(R.id.billinfo_opt2_etxt_addr2).getEditText().setHint("");
                aq.id(R.id.billinfo_opt2_etxt_addr3).getEditText().setHint("");
                aq.id(R.id.billinfo_opt2_etxt_addr3).getEditText().setOnEditorActionListener(
                        (v, actionId, event) -> {
                            if (actionId == EditorInfo.IME_ACTION_NEXT
                                    && !aq.id(R.id.billinfo_etxt_email).getEditText().isEnabled()) {
                                Utils.closeSoftKeyboard(getActivity());
                            }
                            return false;
                        });
                aq.id(R.id.billinfo_opt2_etxt_addr4).visibility(View.GONE);
                aq.id(R.id.billinfo_opt2_etxt_addr5).visibility(View.GONE);
                aq.id(R.id.billinfo_lang_layout).visibility(View.GONE);
                aq.id(R.id.billinfo_billmedia_layout).visibility(View.GONE);
                aq.id(R.id.billinfo_area_layout).visibility(View.GONE);
                break;
        }
        isLoaded = true;

        aq.id(R.id.billinfo_btn_back).clicked(this, "onClick");

        disableAllEditingForZombieAccount();

    }

    private void displayBillInfoHeaderDailog() {
        switch (callback_main.getLob()) {
            case R.string.CONST_LOB_LTS:
                displayDialog(Utils.getString(me.requireActivity(), R.string.myhkt_BUPLTF_FB_BI_ADR));
                break;
            case R.string.CONST_LOB_MOB:
            case R.string.CONST_LOB_IOI:
            case R.string.CONST_LOB_O2F:
            case R.string.CONST_LOB_1010:
                displayDialog(Utils.getString(me.requireActivity(), R.string.myhkt_BUPMBF_FB_BI_ADR));
                break;
            case R.string.CONST_LOB_PCD:
            case R.string.CONST_LOB_TV:
                displayDialog(Utils.getString(me.requireActivity(), R.string.myhkt_BUPIMF_FB_BI_ADR));
                break;
        }
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.billinfo_header:
                displayBillInfoHeaderDailog();
                break;
            case R.id.billinfo_btn_back:
                assert getParentFragment() != null;
                ((BillFragment) getParentFragment()).openBillSumFrag();
                break;
            case R.id.billinfo_popover_area:
                areaQuickAction.show(v, aq.id(R.id.billinfo_layout).getView());
                areaQuickAction.setAnimStyle(QuickAction.ANIM_GROW_FROM_RIGHT);
                break;
            case R.id.billinfo_popover_unit:
                unitQuickAction.show(v, aq.id(R.id.billinfo_layout).getView());
                unitQuickAction.setAnimStyle(QuickAction.ANIM_GROW_FROM_RIGHT);
                break;
            case R.id.billinfo_popover_lang:
                langQuickAction.show(v, aq.id(R.id.billinfo_layout).getView());
                langQuickAction.setAnimStyle(QuickAction.ANIM_GROW_FROM_RIGHT);
                break;
            case R.id.billinfo_popover_billmedia:
                mediaQuickAction.show(v, aq.id(R.id.billinfo_layout).getView());
                mediaQuickAction.setAnimStyle(QuickAction.ANIM_GROW_FROM_RIGHT);
                break;
            case R.id.billinfo_btn_update:
                Utils.closeSoftKeyboard(getActivity(), v);
                updateBillInfo();
                break;
        }

    }

    protected final void refreshData() {
        super.refreshData();
        if (callback_main.getActiveSubview() == R.string.CONST_SELECTEDVIEW_BILLSUMMARY) {
            assert getParentFragment() != null;
            if (!((BillFragment) getParentFragment()).isBillSum) {
                doGetBillInfo();
            }
        }
    }

    private void doGetBillInfo() {
        BiifCra biifCra = new BiifCra();
        biifCra.setILoginId(ClnEnv.isLoggedIn() ? Objects.requireNonNull(ClnEnv.getQualSvee()).getSveeRec().loginId : "");
        biifCra.setISubnRec(callback_main.getSubnRec().copyMe());

        APIsManager.doGetBillInfo(me, biifCra);
    }

    public final void initBillInfo() {
        switch (callback_main.getLob()) {
            case R.string.CONST_LOB_LTS:
            case R.string.CONST_LOB_MOB:
            case R.string.CONST_LOB_IOI:
            case R.string.CONST_LOB_CSP:
                aq.id(R.id.billinfo_opt2_custname_txt).text(biifCra.getOBiif4Hkt().getBillNm());
                if (callback_main.getLob() == R.string.CONST_LOB_MOB || callback_main.getLob() == R.string.CONST_LOB_IOI || callback_main.getLob() == R.string.CONST_LOB_CSP) {

                    aq.id(R.id.billinfo_opt2_etxt_addr1).text(biifCra.getOBiif4Hkt().getBillAdr2().replace("\n", "").replace("\r", ""));
                    aq.id(R.id.billinfo_opt2_etxt_addr2).text(biifCra.getOBiif4Hkt().getBillAdr3().replace("\n", "").replace("\r", ""));
                    aq.id(R.id.billinfo_opt2_etxt_addr3).text(biifCra.getOBiif4Hkt().getBillAdr4().replace("\n", "").replace("\r", ""));
                    aq.id(R.id.billinfo_opt2_etxt_addr4).text(biifCra.getOBiif4Hkt().getBillAdr5().replace("\n", "").replace("\r", ""));
                    aq.id(R.id.billinfo_opt2_etxt_addr5).text(biifCra.getOBiif4Hkt().getBillAdr6().replace("\n", "").replace("\r", ""));
                }
                if (callback_main.getLob() == R.string.CONST_LOB_LTS) {

                    aq.id(R.id.billinfo_opt2_etxt_addr1).text(biifCra.getOBiif4Hkt().getBillAdr1().replace("\n", "").replace("\r", ""));
                    aq.id(R.id.billinfo_opt2_etxt_addr2).text(biifCra.getOBiif4Hkt().getBillAdr2().replace("\n", "").replace("\r", ""));
                    aq.id(R.id.billinfo_opt2_etxt_addr3).text(biifCra.getOBiif4Hkt().getBillAdr3().replace("\n", "").replace("\r", ""));
                    aq.id(R.id.billinfo_opt2_etxt_addr4).text(biifCra.getOBiif4Hkt().getBillAdr4().replace("\n", "").replace("\r", ""));
                    aq.id(R.id.billinfo_opt2_etxt_addr5).text(biifCra.getOBiif4Hkt().getBillAdr5().replace("\n", "").replace("\r", ""));
                    aq.id(R.id.billinfo_opt2_etxt_addr6).text(biifCra.getOBiif4Hkt().getBillAdr6().replace("\n", "").replace("\r", ""));

                    if (biifCra.getOBiif4Hkt().getBillMedia().equalsIgnoreCase(BiifCra.BILL_MEDIA_S)) {
                        mediaPopover.setText(Utils.getString(me.requireActivity(), R.string.BUPLTF_MEDIA_E));
                        aq.id(R.id.billinfo_email_layout).visibility(View.VISIBLE);
                        aq.id(R.id.billinfo_sms_layout).visibility(View.VISIBLE);
                        aq.id(R.id.billinfo_email_layout).text(biifCra.getOBiif4Hkt().getBillEmail());
                        //TODO Fill in the SMS Number
                        //aq.id(R.id.billinfo_sms_layout).text(biifCra.getOBiif4Hkt().getBillSmsNtfn());
                    } else {
                        mediaPopover.setText(Utils.getString(me.requireActivity(), R.string.BUPLTF_MEDIA_P));
                        aq.id(R.id.billinfo_email_layout).visibility(View.GONE);
                        aq.id(R.id.billinfo_sms_layout).visibility(View.GONE);
                    }
                }

                //TODO: Henry Payment mode : please delete this for payment mode
                //TODO: Please also delete android:visibility="gone" attribute value under billinfo_popover_billmedia
                aq.id(R.id.billinfo_email_layout).visibility(View.VISIBLE);

                //bill language h-sim
                if (biifCra.getOBiif4Hkt().getBillLang().equalsIgnoreCase(BiifCra.BI_LANG_ZH)) {
                    aq.popOverInputView(R.id.billinfo_popover_lang, Utils.getString(me.requireActivity(), R.string.BUPLTF_BILANGZH));
                } else if (biifCra.getOBiif4Hkt().getBillLang().equalsIgnoreCase(BiifCra.BI_LANG_EN)) {
                    aq.popOverInputView(R.id.billinfo_popover_lang, Utils.getString(me.requireActivity(), R.string.BUPLTF_BILANGEN));
                } else {
                    aq.popOverInputView(R.id.billinfo_popover_lang, Utils.getString(me.requireActivity(), R.string.BUPLTF_BILANGBI));
                }
                aq.id(R.id.billinfo_popover_lang).enabled(true);

                aq.id(R.id.billinfo_popover_billmedia).enabled(true);
                break;
            case R.string.CONST_LOB_O2F:
            case R.string.CONST_LOB_1010:
                if (debug) Log.i(TAG, "isPOX" + biifCra.getOBiif4Csl().isPox());
                if (biifCra.getOBiif4Csl().isPox()) {
                    lrTabBtn.selectRight();
                    aq.id(R.id.billinfo_layout_opt1).visibility(View.GONE);
                    aq.id(R.id.billinfo_layout_opt2).visibility(View.VISIBLE);
                    aq.id(R.id.billinfo_opt2_etxt_addr1).text(biifCra.getOBiif4Csl().getPostalNm().replace("\n", "").replace("\r", ""));
                    aq.id(R.id.billinfo_opt2_etxt_addr2).text(biifCra.getOBiif4Csl().getPostalBx().replace("\n", "").replace("\r", ""));
                    aq.id(R.id.billinfo_opt2_etxt_addr3).text(biifCra.getOBiif4Csl().getDistrict().replace("\n", "").replace("\r", ""));
                } else {
                    lrTabBtn.selectLeft();
                    aq.id(R.id.billinfo_layout_opt1).visibility(View.VISIBLE);
                    aq.id(R.id.billinfo_layout_opt2).visibility(View.GONE);
                    if (biifCra.getOBiif4Csl().getUnit().isEmpty()) {
                        unitPopover.setText("----");
                    } else if (biifCra.getOBiif4Csl().getUnit().equals("APT")) {
                        unitPopover.setText("APT");
                    } else if (biifCra.getOBiif4Csl().getUnit().equals("FLAT")) {
                        unitPopover.setText("FLAT");
                    } else if (biifCra.getOBiif4Csl().getUnit().equals("ROOM")) {
                        unitPopover.setText("ROOM");
                    } else if (biifCra.getOBiif4Csl().getUnit().equals("SHOP")) {
                        unitPopover.setText("SHOP");
                    } else if (biifCra.getOBiif4Csl().getUnit().equals("SUITE")) {
                        unitPopover.setText("SUITE");
                    } else if (biifCra.getOBiif4Csl().getUnit().equals("UNIT")) {
                        unitPopover.setText("UNIT");
                    }

                    aq.id(R.id.billinfo_etxt_num).text(biifCra.getOBiif4Csl().getFlat().replace("\n", "").replace("\r", ""));
                    aq.id(R.id.billinfo_etxt_flr).text(biifCra.getOBiif4Csl().getFloor().replace("\n", "").replace("\r", ""));
                    aq.id(R.id.billinfo_etxt_blk).text(biifCra.getOBiif4Csl().getBlock().replace("\n", "").replace("\r", ""));
                    aq.id(R.id.billinfo_etxt_blding).text(biifCra.getOBiif4Csl().getBuilding().replace("\n", "").replace("\r", ""));
                    aq.id(R.id.billinfo_etxt_section).text(biifCra.getOBiif4Csl().getSection().replace("\n", "").replace("\r", ""));
                    aq.id(R.id.billinfo_etxt_str).text(biifCra.getOBiif4Csl().getStreetNum().replace("\n", "").replace("\r", ""));
                    aq.id(R.id.billinfo_etxt_strname).text(biifCra.getOBiif4Csl().getStreetNm().replace("\n", "").replace("\r", ""));
                    aq.id(R.id.billinfo_etxt_dist).text(biifCra.getOBiif4Csl().getDistrict().replace("\n", "").replace("\r", ""));

                    //bill language c-sim
                    if (biifCra.getOBiif4Csl().getBillLang().equalsIgnoreCase(BiifCra.MOBC_BI_LANG_ZH)) {
                        aq.popOverInputView(R.id.billinfo_popover_lang, Utils.getString(me.requireActivity(), R.string.BUPLTF_BILANGZH));
                    } else if (biifCra.getOBiif4Csl().getBillLang().equalsIgnoreCase(BiifCra.MOBC_BI_LANG_EN)) {
                        aq.popOverInputView(R.id.billinfo_popover_lang, Utils.getString(me.requireActivity(), R.string.BUPLTF_BILANGEN));
                    }
                }
                aq.id(R.id.billinfo_popover_lang).enabled(true);
                //area
                if (biifCra.getOBiif4Csl().getArea().equalsIgnoreCase("HK")) {
                    areaPopover.setText(requireActivity().getString(R.string.billinfo_hk));
                } else if (biifCra.getOBiif4Csl().getArea().equalsIgnoreCase("KN")) {
                    areaPopover.setText(requireActivity().getString(R.string.billinfo_kl));
                } else if (biifCra.getOBiif4Csl().getArea().equalsIgnoreCase("NT")) {
                    areaPopover.setText(requireActivity().getString(R.string.billinfo_nt));
                } else if (biifCra.getOBiif4Csl().getArea().equalsIgnoreCase("LT")) {
                    areaPopover.setText(requireActivity().getString(R.string.billinfo_lt));
                } else {
                    areaPopover.setText(requireActivity().getString(R.string.billinfo_hk));
                }
                break;
            case R.string.CONST_LOB_PCD:
            case R.string.CONST_LOB_TV:
                aq.id(R.id.billinfo_opt2_custname_txt).text(biifCra.getOBiif4Hkt().getBillNm());
                aq.id(R.id.billinfo_opt2_etxt_addr1).text(biifCra.getOBiif4Hkt().getBillAdr1().replace("\n", "").replace("\r", ""));
                aq.id(R.id.billinfo_opt2_etxt_addr2).text(biifCra.getOBiif4Hkt().getBillAdr2().replace("\n", "").replace("\r", ""));
                aq.id(R.id.billinfo_opt2_etxt_addr3).text(biifCra.getOBiif4Hkt().getBillAdr3().replace("\n", "").replace("\r", ""));
                break;
        }

        if (callback_main.getLob() == R.string.CONST_LOB_1010 || callback_main.getLob() == R.string.CONST_LOB_O2F) {
            if (biifCra.getOBiif4Csl().getBillLang().equalsIgnoreCase(BiifCra.MOBC_BI_LANG_ZH)) {
                aq.popOverInputView(R.id.billinfo_popover_lang, Utils.getString(me.requireActivity(), R.string.BUPMBF_BILANGZH));
            } else if (biifCra.getOBiif4Csl().getBillLang().equalsIgnoreCase(BiifCra.MOBC_BI_LANG_EN)) {
                langPopover.setText(Utils.getString(me.requireActivity(), R.string.BUPMBF_BILANGEN));
            }
        } else {
            if (biifCra.getOBiif4Hkt().getBillLang().equalsIgnoreCase(BiifCra.BI_LANG_ZH)) {
                langPopover.setText(Utils.getString(me.requireActivity(), R.string.BUPLTF_BILANGZH));
            } else if (biifCra.getOBiif4Hkt().getBillLang().equalsIgnoreCase(BiifCra.BI_LANG_EN)) {
                langPopover.setText(Utils.getString(me.requireActivity(), R.string.BUPLTF_BILANGEN));
            } else {
                langPopover.setText(Utils.getString(me.requireActivity(), R.string.BUPLTF_BILANGBI));
            }
        }

        if (callback_main.getLob() == R.string.CONST_LOB_1010 || callback_main.getLob() == R.string.CONST_LOB_O2F) {
            aq.id(R.id.billinfo_etxt_email).text(biifCra.getOBiif4Csl().getBillEmail());
        } else {
            aq.id(R.id.billinfo_etxt_email).text(biifCra.getOBiif4Hkt().getBillEmail());
        }

        disableAllEditingForZombieAccount();
        setIMEOptionForAddressLine();
    }

    private void setIMEOptionForAddressLine() {
        // SoftKey Fixes
        switch (callback_main.getLob()) {
            case R.string.CONST_LOB_LTS:
            case R.string.CONST_LOB_MOB:
            case R.string.CONST_LOB_IOI:
                // insert code here for keyboard bug fixes
                aq.id(R.id.billinfo_opt2_etxt_addr5).getEditText().setImeOptions(
                        aq.id(R.id.billinfo_etxt_email).getEditText().isEnabled() ?
                                EditorInfo.IME_ACTION_NEXT : EditorInfo.IME_ACTION_DONE);
                break;
            case R.string.CONST_LOB_O2F:
            case R.string.CONST_LOB_1010:
                // insert code here for keyboard bug fixes
                break;
            case R.string.CONST_LOB_PCD:
            case R.string.CONST_LOB_TV:
                // insert code here for keyboard bug fixes
                break;
        }
    }

    private void disableAllEditingForZombieAccount() {
        //disable all editing for zombie account
        if (callback_main.IsZombie() || (callback_main.isMyMobAcct()) || biifCra == null) {
            disableBillAddressET();
            disableEmailAddressET();
            disablePopover();
            aq.id(R.id.billinfo_btn_update).visibility(View.INVISIBLE);
        } else {
            if (!(callback_main.getLob() == R.string.CONST_LOB_1010 || callback_main.getLob() == R.string.CONST_LOB_O2F)) {
                if (callback_main.getLob() == R.string.CONST_LOB_LTS) {
                    if (biifCra.getOBiif4Hkt().getBillMedia().equalsIgnoreCase(BiifCra.BILL_MEDIA_S)) {
                        enableEmailAddressET();
                    } else {
                        disableEmailAddressET();
                    }
                } else {
                    if (biifCra.getOBiif4Hkt().getBillMedia().equalsIgnoreCase(BiifCra.BILL_MEDIA_E)) {
                        enableEmailAddressET();
                    } else {
                        disableEmailAddressET();
                    }
                }
            }
            enableBillAddressET();

            aq.id(R.id.billinfo_popover_unit).enabled(true);
            aq.id(R.id.billinfo_popover_unit).getView().setAlpha(1f);
            aq.id(R.id.billinfo_popover_area).enabled(true);
            aq.id(R.id.billinfo_popover_area).getView().setAlpha(1f);
            aq.id(R.id.billinfo_popover_billmedia).enabled(true);
            aq.id(R.id.billinfo_popover_billmedia).getView().setAlpha(1f);
            aq.id(R.id.billinfo_popover_lang).enabled(true);
            aq.id(R.id.billinfo_popover_lang).getView().setAlpha(1f);
            aq.id(R.id.billinfo_btn_update).visibility(View.VISIBLE);
        }
    }

    public void disableBillAddressET() {
        aq.id(R.id.billinfo_etxt_num).enabled(false);
        aq.id(R.id.billinfo_etxt_flr).enabled(false);
        aq.id(R.id.billinfo_etxt_blk).enabled(false);
        aq.id(R.id.billinfo_etxt_blding).enabled(false);
        aq.id(R.id.billinfo_etxt_section).enabled(false);
        aq.id(R.id.billinfo_etxt_str).enabled(false);
        aq.id(R.id.billinfo_etxt_strname).enabled(false);
        aq.id(R.id.billinfo_etxt_dist).enabled(false);
        aq.id(R.id.billinfo_opt2_etxt_addr1).enabled(false);
        aq.id(R.id.billinfo_opt2_etxt_addr2).enabled(false);
        aq.id(R.id.billinfo_opt2_etxt_addr3).enabled(false);
        aq.id(R.id.billinfo_opt2_etxt_addr4).enabled(false);
        aq.id(R.id.billinfo_opt2_etxt_addr5).enabled(false);
        aq.id(R.id.billinfo_opt2_etxt_addr6).enabled(false);
        aq.id(R.id.billinfo_opt2_etxt_addr1).getView().setAlpha(0.5f);
        aq.id(R.id.billinfo_opt2_etxt_addr2).getView().setAlpha(0.5f);
        aq.id(R.id.billinfo_opt2_etxt_addr3).getView().setAlpha(0.5f);
        aq.id(R.id.billinfo_opt2_etxt_addr4).getView().setAlpha(0.5f);
        aq.id(R.id.billinfo_opt2_etxt_addr5).getView().setAlpha(0.5f);
        aq.id(R.id.billinfo_opt2_etxt_addr6).getView().setAlpha(0.5f);
    }

    public void enableBillAddressET() {
        aq.id(R.id.billinfo_etxt_num).enabled(true);
        aq.id(R.id.billinfo_etxt_flr).enabled(true);
        aq.id(R.id.billinfo_etxt_blk).enabled(true);
        aq.id(R.id.billinfo_etxt_blding).enabled(true);
        aq.id(R.id.billinfo_etxt_section).enabled(true);
        aq.id(R.id.billinfo_etxt_str).enabled(true);
        aq.id(R.id.billinfo_etxt_strname).enabled(true);
        aq.id(R.id.billinfo_etxt_dist).enabled(true);
        aq.id(R.id.billinfo_opt2_etxt_addr1).enabled(true);
        aq.id(R.id.billinfo_opt2_etxt_addr2).enabled(true);
        aq.id(R.id.billinfo_opt2_etxt_addr3).enabled(true);
        aq.id(R.id.billinfo_opt2_etxt_addr4).enabled(true);
        aq.id(R.id.billinfo_opt2_etxt_addr5).enabled(true);
        aq.id(R.id.billinfo_opt2_etxt_addr6).enabled(true);
        aq.id(R.id.billinfo_opt2_etxt_addr1).getView().setAlpha(1f);
        aq.id(R.id.billinfo_opt2_etxt_addr2).getView().setAlpha(1f);
        aq.id(R.id.billinfo_opt2_etxt_addr3).getView().setAlpha(1f);
        aq.id(R.id.billinfo_opt2_etxt_addr4).getView().setAlpha(1f);
        aq.id(R.id.billinfo_opt2_etxt_addr5).getView().setAlpha(1f);
        aq.id(R.id.billinfo_opt2_etxt_addr6).getView().setAlpha(1f);
    }

    public void disableEmailAddressET() {
        aq.id(R.id.billinfo_etxt_email).enabled(false);
        aq.id(R.id.billinfo_etxt_email).getView().setAlpha(0.5f);
    }

    public void enableEmailAddressET() {
        aq.id(R.id.billinfo_etxt_email).enabled(true);
        aq.id(R.id.billinfo_etxt_email).getView().setAlpha(1f);
    }

    public void disablePopover() {
        aq.id(R.id.billinfo_popover_unit).enabled(false);
        aq.id(R.id.billinfo_popover_unit).getView().setAlpha(0.5f);
        aq.id(R.id.billinfo_popover_area).enabled(false);
        aq.id(R.id.billinfo_popover_area).getView().setAlpha(0.5f);
        aq.id(R.id.billinfo_popover_billmedia).enabled(false);
        aq.id(R.id.billinfo_popover_billmedia).getView().setAlpha(0.5f);
        aq.id(R.id.billinfo_popover_lang).enabled(false);
        aq.id(R.id.billinfo_popover_lang).getView().setAlpha(0.5f);
    }

    public void disableOrEnableEmailAndBillAddressET() {
        switch (callback_main.getLob()) {
            case R.string.CONST_LOB_LTS:
                if (callback_main.IsZombie()) {
                    disableEmailAddressET();
                    disableBillAddressET();
                } else {
                    if (biifCra.getOBiif4Hkt().getBillMedia().equals(BiifCra.BILL_MEDIA_S)) {
                        enableEmailAddressET();
                    } else {
                        disableEmailAddressET();
                    }
                    if (callback_main.getLtsType() == R.string.CONST_LTS_ICFS ||
                            callback_main.getLtsType() == R.string.CONST_LTS_ONECALL ||
                            callback_main.getLtsType() == R.string.CONST_LTS_IDD0060 ||
                            callback_main.getLtsType() == R.string.CONST_LTS_CALLINGCARD) {
                        disableBillAddressET();
                    } else {
                        enableBillAddressET();
                    }
                }
                break;
            case R.string.CONST_LOB_MOB:
            case R.string.CONST_LOB_IOI:
            case R.string.CONST_LOB_PCD:
            case R.string.CONST_LOB_TV:
            case R.string.CONST_LOB_CSP:
                if (callback_main.IsZombie() || callback_main.isMyMobAcct()) {
                    disableEmailAddressET();
                    disableBillAddressET();
                } else {
                    if (biifCra.getOBiif4Hkt().getBillMedia().equals(BiifCra.BILL_MEDIA_E)) {
                        enableEmailAddressET();
                    } else {
                        disableEmailAddressET();
                    }
                    enableBillAddressET();
                }
                break;
            case R.string.CONST_LOB_O2F:
            case R.string.CONST_LOB_1010:
                if (callback_main.IsZombie()) {
                    disableEmailAddressET();
                    disableBillAddressET();
                } else {
                    enableEmailAddressET();
                    enableBillAddressET();
                }
                break;
        }
    }

    public final void updateBillInfo() {
        BiifCra biifCraUpd = biifCra.copyMe();
        biifCraUpd.setILoginId(Objects.requireNonNull(ClnEnv.getQualSvee()).getSveeRec().loginId);

        aq = new AAQuery(myView);

        //Set address
        switch (callback_main.getLob()) {
            case R.string.CONST_LOB_MOB:
            case R.string.CONST_LOB_IOI:
            case R.string.CONST_LOB_CSP:
                biifCraUpd.getIBiif4Hkt().setBillAdr2(aq.id(R.id.billinfo_opt2_etxt_addr1).getText().toString());
                biifCraUpd.getIBiif4Hkt().setBillAdr3(aq.id(R.id.billinfo_opt2_etxt_addr2).getText().toString());
                biifCraUpd.getIBiif4Hkt().setBillAdr4(aq.id(R.id.billinfo_opt2_etxt_addr3).getText().toString());
                biifCraUpd.getIBiif4Hkt().setBillAdr5(aq.id(R.id.billinfo_opt2_etxt_addr4).getText().toString());
                biifCraUpd.getIBiif4Hkt().setBillAdr6(aq.id(R.id.billinfo_opt2_etxt_addr5).getText().toString());
                break;
            case R.string.CONST_LOB_O2F:
            case R.string.CONST_LOB_1010:
                if ("----".equalsIgnoreCase(unitPopover.getText())) {
                    biifCraUpd.getIBiif4Csl().setUnit("");
                } else {
                    biifCraUpd.getIBiif4Csl().setUnit(unitPopover.getText());
                }
                if (lrTabBtn.isLeftClicked()) {
                    biifCraUpd.getIBiif4Csl().setPox(false);
                    biifCraUpd.getIBiif4Csl().setFlat(aq.id(R.id.billinfo_etxt_num).getText().toString());
                    biifCraUpd.getIBiif4Csl().setFloor(aq.id(R.id.billinfo_etxt_flr).getText().toString());
                    biifCraUpd.getIBiif4Csl().setBlock(aq.id(R.id.billinfo_etxt_blk).getText().toString());
                    biifCraUpd.getIBiif4Csl().setBuilding(aq.id(R.id.billinfo_etxt_blding).getText().toString());
                    biifCraUpd.getIBiif4Csl().setSection(aq.id(R.id.billinfo_etxt_section).getText().toString());
                    biifCraUpd.getIBiif4Csl().setStreetNum(aq.id(R.id.billinfo_etxt_str).getText().toString());
                    biifCraUpd.getIBiif4Csl().setStreetNm(aq.id(R.id.billinfo_etxt_strname).getText().toString());
                    biifCraUpd.getIBiif4Csl().setDistrict(aq.id(R.id.billinfo_etxt_dist).getText().toString());
                } else {
                    biifCraUpd.getIBiif4Csl().setPox(true);
                    biifCraUpd.getIBiif4Csl().setPostalNm(aq.id(R.id.billinfo_opt2_etxt_addr1).getText().toString());
                    biifCraUpd.getIBiif4Csl().setPostalBx(aq.id(R.id.billinfo_opt2_etxt_addr2).getText().toString());
                    biifCraUpd.getIBiif4Csl().setDistrict(aq.id(R.id.billinfo_opt2_etxt_addr3).getText().toString());
                }
                biifCraUpd.getIBiif4Csl().setArea(getSelectedArea());
                break;
            case R.string.CONST_LOB_PCD:
            case R.string.CONST_LOB_TV:
                biifCraUpd.getIBiif4Hkt().setBillAdr1(aq.id(R.id.billinfo_opt2_etxt_addr1).getText().toString());
                biifCraUpd.getIBiif4Hkt().setBillAdr2(aq.id(R.id.billinfo_opt2_etxt_addr2).getText().toString());
                biifCraUpd.getIBiif4Hkt().setBillAdr3(aq.id(R.id.billinfo_opt2_etxt_addr3).getText().toString());
                break;
        }

        //Set Email And Language
        if (callback_main.getLob() == R.string.CONST_LOB_1010 || callback_main.getLob() == R.string.CONST_LOB_O2F) {
            biifCraUpd.getIBiif4Csl().setBillEmail(aq.id(R.id.billinfo_etxt_email).getText().toString());
            biifCraUpd.getIBiif4Csl().setBillLang(getSelectedLangCode());
        } else if (callback_main.getLob() == R.string.CONST_LOB_PCD || callback_main.getLob() == R.string.CONST_LOB_TV) {
            biifCraUpd.getIBiif4Hkt().setBillLang("");
            biifCraUpd.getIBiif4Hkt().setBillEmail(aq.id(R.id.billinfo_etxt_email).getText().toString());
        } else {
            biifCraUpd.getIBiif4Hkt().setBillEmail(aq.id(R.id.billinfo_etxt_email).getText().toString());
            biifCraUpd.getIBiif4Hkt().setBillLang(getSelectedLangCode());
        }

        //Validation
        final BiifCra biifCraUpdFinal = biifCraUpd.copyMe();
        Reply reply = new Reply();
        String dialogTitle = "";
        String dialogMsg = "";
        String errorMsg = "";
        switch (callback_main.getLob()) {
            case R.string.CONST_LOB_MOB:
            case R.string.CONST_LOB_IOI:
            case R.string.CONST_LOB_CSP:
                dialogTitle = getString(R.string.BUPIMM_CFMHDR);
                dialogMsg = getString(R.string.BUPIMM_CFMMSG);
                // To handle both NE and MOB Case get propagate the getLob directly
                reply = BiifCra.validAdrLen(callback_main.getLobString(), biifCraUpd.getIBiif4Hkt().getBillAdr1(), biifCraUpd.getIBiif4Hkt().getBillAdr2(), biifCraUpd.getIBiif4Hkt().getBillAdr3(), biifCraUpd.getIBiif4Hkt().getBillAdr4(), biifCraUpd.getIBiif4Hkt().getBillAdr5(), biifCraUpd.getIBiif4Hkt().getBillAdr6());
                if (reply.isSucc())
                    reply = BiifCra.validAdrChr(biifCraUpd.getIBiif4Hkt().getBillAdr1(), biifCraUpd.getIBiif4Hkt().getBillAdr2(), biifCraUpd.getIBiif4Hkt().getBillAdr3(), biifCraUpd.getIBiif4Hkt().getBillAdr4(), biifCraUpd.getIBiif4Hkt().getBillAdr5(), biifCraUpd.getIBiif4Hkt().getBillAdr6());
                if (reply.isSucc() && biifCraUpd.getOBiif4Hkt().getBillMedia().equalsIgnoreCase(BiifCra.BILL_MEDIA_E))
                    reply = biifCraUpd.validEmail();
                errorMsg = InterpretRCManager.interpretRC_BinqMobMdu(getActivity(), reply.getCode(), callback_main.getLob());
                break;
            case R.string.CONST_LOB_O2F:
            case R.string.CONST_LOB_1010:
                dialogTitle = getString(R.string.BUPIMM_CFMHDR);
                dialogMsg = getString(R.string.BUPIMM_CFMMSG);
                reply = biifCraUpd.validate4Csl();
                reply.setCode(RC.SUCC);
                errorMsg = InterpretRCManager.interpretRC_BinqMobMdu(getActivity(), reply.getCode(), callback_main.getLob());
                break;
            case R.string.CONST_LOB_PCD:
            case R.string.CONST_LOB_TV:
                dialogTitle = getString(R.string.BUPIMM_CFMHDR);
                dialogMsg = getString(R.string.BUPIMM_CFMMSG);
                reply = BiifCra.validAdrLen(callback_main.getLobString(), biifCraUpd.getIBiif4Hkt().getBillAdr1(), biifCraUpd.getIBiif4Hkt().getBillAdr2(), biifCraUpd.getIBiif4Hkt().getBillAdr3());
                if (reply.isSucc())
                    reply = BiifCra.validAdrChr(biifCraUpd.getIBiif4Hkt().getBillAdr1(), biifCraUpd.getIBiif4Hkt().getBillAdr2(), biifCraUpd.getIBiif4Hkt().getBillAdr3());
                if (reply.isSucc() && biifCraUpd.getOBiif4Hkt().getBillMedia().equalsIgnoreCase(BiifCra.BILL_MEDIA_E))
                    reply = biifCraUpd.validEmail();
                errorMsg = InterpretRCManager.interpretRC_BinqImsMdu(getActivity(), reply.getCode());
                break;
        }

        if (!reply.isSucc()) {
            //Show Error dialog
            displayDialog(errorMsg);
        } else {
            //
            DialogInterface.OnClickListener onPosClick = new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                    APIsManager.doUpdBillinfo(me, biifCraUpdFinal);
                }
            };
            DialogInterface.OnClickListener onNegClick = new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                    displayDialog(getResString(R.string.CFMM_DISCARD));
                    initBillInfo();
                }
            };
            DialogHelper.createTitleDialog(me.getActivity(), dialogTitle, dialogMsg,
                    getString(R.string.CFMF_CFM), onPosClick,
                    getString(R.string.CFMF_NOTCFM), onNegClick);
        }
    }

    private String getSelectedLangCode() {
        if (callback_main.getLob() == R.string.CONST_LOB_1010 || callback_main.getLob() == R.string.CONST_LOB_O2F) {
            if (langPopover.getText().equalsIgnoreCase(requireActivity().getString(R.string.BUPLTF_BILANGZH))) {
                return BiifCra.MOBC_BI_LANG_ZH;
            } else if (langPopover.getText().equalsIgnoreCase(requireActivity().getString(R.string.BUPLTF_BILANGEN))) {
                return BiifCra.MOBC_BI_LANG_EN;
            }
        } else {
            if (langPopover.getText().equalsIgnoreCase(requireActivity().getString(R.string.BUPLTF_BILANGZH))) {
                return BiifCra.BI_LANG_ZH;
            } else if (langPopover.getText().equalsIgnoreCase(requireActivity().getString(R.string.BUPLTF_BILANGEN))) {
                return BiifCra.BI_LANG_EN;
            } else if (langPopover.getText().equalsIgnoreCase(requireActivity().getString(R.string.BUPLTF_BILANGBI))) {
                return BiifCra.BI_LANG_BI;
            }
        }
        return "";
    }

    private String getSelectedArea() {
        if (requireActivity().getString(R.string.billinfo_hk).equalsIgnoreCase(areaPopover.getText())) {
            return "HK";
        } else if (requireActivity().getString(R.string.billinfo_kl).equalsIgnoreCase(areaPopover.getText())) {
            return "KN";
        } else if (requireActivity().getString(R.string.billinfo_nt).equalsIgnoreCase(areaPopover.getText())) {
            return "NT";
        } else if (requireActivity().getString(R.string.billinfo_lt).equalsIgnoreCase(areaPopover.getText())) {
            return "LT";
        } else {
            return "HK";
        }
    }

    private void resetLayout() {
        aq.id(R.id.billinfo_etxt_num).text("");
        aq.id(R.id.billinfo_etxt_flr).text("");
        aq.id(R.id.billinfo_etxt_blk).text("");
        aq.id(R.id.billinfo_etxt_blding).text("");
        aq.id(R.id.billinfo_etxt_section).text("");
        aq.id(R.id.billinfo_etxt_str).text("");
        aq.id(R.id.billinfo_etxt_strname).text("");
        aq.id(R.id.billinfo_etxt_dist).text("");
        aq.id(R.id.billinfo_opt2_etxt_addr1).text("");
        aq.id(R.id.billinfo_opt2_etxt_addr2).text("");
        aq.id(R.id.billinfo_opt2_etxt_addr3).text("");
        aq.id(R.id.billinfo_opt2_etxt_addr4).text("");
        aq.id(R.id.billinfo_opt2_etxt_addr5).text("");
        langPopover.setText("");
        areaPopover.setText("");
        unitPopover.setText("");
    }

    @Override
    public final void refresh() {
        super.refresh();
        if (callback_main.getActiveSubview() == R.string.CONST_SELECTEDVIEW_BILLSUMMARY) {
            doGetBillInfo();
        }
    }

    @Override
    public void onSuccess(APIsResponse response) {
        if (APIsManager.READ_BIIF.equals(response.getActionTy())) {
            biifCra = new BiifCra();
            biifCra = (BiifCra) response.getCra();
            resetLayout();
            initBillInfo();
            disableOrEnableEmailAndBillAddressET();

        } else if (APIsManager.UPD_BIIF.equals(response.getActionTy())) {
            biifCra = new BiifCra();
            biifCra = (BiifCra) response.getCra();
            if (callback_main.getLob() == R.string.CONST_LOB_1010 || callback_main.getLob() == R.string.CONST_LOB_O2F || callback_main.getLob() == R.string.CONST_LOB_MOB || callback_main.getLob() == R.string.CONST_LOB_IOI) {
                DialogHelper.createSimpleDialog(getActivity(), requireActivity().getString(R.string.BUPMBM_UPD_DONE));
            } else if (callback_main.getLob() == R.string.CONST_LOB_LTS) {
                DialogHelper.createSimpleDialog(getActivity(), requireActivity().getString(R.string.BUPLTM_UPD_DONE));
            } else if (callback_main.getLob() == R.string.CONST_LOB_PCD || callback_main.getLob() == R.string.CONST_LOB_TV) {
                DialogHelper.createSimpleDialog(getActivity(), requireActivity().getString(R.string.BUPIMM_UPD_DONE));
            } else {
                DialogHelper.createSimpleDialog(getActivity(), requireActivity().getString(R.string.BUPLTM_UPD_DONE));
            }
            doGetBillInfo();
        } else if (APIsManager.AO_AUTH.equals(response.getActionTy())) {
            addOnCra = (AddOnCra) response.getCra();
            addOnCra.getOSubnRec().ivr_pwd = addOnCra.getISubnRec().ivr_pwd;

            callback_main.setSubscriptionRec(addOnCra.getOSubnRec());
            callback_main.getAcctAgent().setSubnRec(addOnCra.getOSubnRec());

            doGetBillInfo();
        }
    }

    @Override
    public void onFail(APIsResponse response) {

        if (!"".equals(response.getMessage()) && response.getMessage() != null) {
            DialogHelper.createSimpleDialog(getActivity(), response.getMessage());
        } else if (RC.TCSESS_MM.equalsIgnoreCase(response.getReply().getCode())) {
            BaseActivity.ivSessDialog();
        } else if (RC.AO_PSPH_FAIL.equalsIgnoreCase(response.getReply().getCode())) {
            recallDoAuth(me, callback_main.getSubnRec());
        } else if (APIsManager.READ_BIIF.equals(response.getActionTy()) || APIsManager.UPD_BIIF.equals(response.getActionTy())) {
            if (callback_main.getLob() == R.string.CONST_LOB_LTS) {
                DialogHelper.createSimpleDialog(getActivity(), InterpretRCManager.interpretRC_BinqLtsMdu(getActivity(), response.getReply().getCode()));
            } else if (callback_main.getLob() == R.string.CONST_LOB_PCD || callback_main.getLob() == R.string.CONST_LOB_TV) {
                DialogHelper.createSimpleDialog(getActivity(), InterpretRCManager.interpretRC_BinqImsMdu(getActivity(), response.getReply().getCode()));
            } else if (callback_main.getLob() == R.string.CONST_LOB_1010 || callback_main.getLob() == R.string.CONST_LOB_O2F || callback_main.getLob() == R.string.CONST_LOB_MOB || callback_main.getLob() == R.string.CONST_LOB_IOI) {
                DialogHelper.createSimpleDialog(getActivity(), InterpretRCManager.interpretRC_BinqMobMdu(getActivity(), response.getReply().getCode(), callback_main.getLob()));
            } else {
                DialogHelper.createSimpleDialog(getActivity(), ClnEnv.getRPCErrMsg(getActivity(), response.getReply().getCode()));
            }
            if (APIsManager.UPD_BIIF.equals(response.getActionTy())) {
                doGetBillInfo();
            }
        }
        //Hide the update button when there is error for reading bill
        if (APIsManager.READ_BIIF.equals(response.getActionTy())) {
            biifCra = new BiifCra();
            biifCra = (BiifCra) response.getCra();
            resetLayout();
            initBillInfo();
            disableBillAddressET();
            disableEmailAddressET();
            disablePopover();
            aq.id(R.id.billinfo_btn_update).visibility(View.INVISIBLE);
        }
    }

}
