package com.pccw.myhkt.activity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.pccw.myhkt.ClnEnv;
import com.pccw.myhkt.DialogHelper;
import com.pccw.myhkt.FAWrapper;
import com.pccw.myhkt.R;
import com.pccw.myhkt.Utils;
import com.pccw.myhkt.lib.ui.HKTButton;
import com.pccw.myhkt.listeners.OnDialogSelectionListener;
import com.pccw.myhkt.util.Constant;

/************************************************************************
 * File : TouchIdWelcomeActivity.java 
 * Desc : Touch Id Welcome page contains the views to allow user to activate Touch Id
 * Name : TouchIdWelcomeActivity
 * by : Abdulmoiz Esmail 
 * Date : 19/10/2017
 *
 * Change History:
 * Date       Modified By		Description
 * ---------- ----------------	-------------------------------
 * 19/10/2017 Abdulmoiz Esmail  - First Draft
 * 13/02/2019 Abdulmoiz Esmail  - Added close button, with remember me dialog
 *************************************************************************/
public class TouchIdWelcomeActivity extends Activity implements OnDialogSelectionListener {

    HKTButton mActivateNowButton;
    Button mActivateLaterButton;
    Button mSkipButton;
    ImageButton mCloseButton;
    LinearLayout mViewItems;
    TextView mWelcomeMessage;

    TextView mTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_touch_id_welcome);

        mTitle = (TextView) findViewById(R.id.main_view_title);
        Typeface face = Typeface.createFromAsset(getAssets(),
                "Roboto-Light.ttf");
        mTitle.setTypeface(face);
        mActivateNowButton = (HKTButton) findViewById(R.id.ti_btn_activate_now);
        mActivateLaterButton = (Button) findViewById(R.id.ti_btn_activate_later);
        mSkipButton = (Button) findViewById(R.id.ti_btn_close);
        mCloseButton = (ImageButton) findViewById(R.id.touch_id_button_close);
        mViewItems = (LinearLayout) findViewById(R.id.view_steps);
        mWelcomeMessage = (TextView) findViewById(R.id.welcome_message);

        int textSize = (int) getResources().getDimension(R.dimen.textsize_xlarge);

        mActivateNowButton.initViews(this, HKTButton.TYPE_GREEN, getResources().getString(R.string.fp_welcome_btn_activate),
                textSize, LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        mActivateNowButton.setPadding(60, 5, 60, 5);


        if (Utils.isTouchIDLoginActivated(this)) {
            mActivateNowButton.setVisibility(View.GONE);
            mViewItems.setVisibility(View.GONE);
            if (Utils.isTouchIdUserEqualsToLoggedInUser(this)) {
                mWelcomeMessage.setText(getString(R.string.fp_welcome_description_enabled));
            } else {
                mWelcomeMessage.setText(getString(R.string.fp_welcome_description_duplicated));
            }
            mActivateLaterButton.setVisibility(View.GONE);
            mSkipButton.setVisibility(View.VISIBLE);
        }

        //If from auto-show (Login), visible the close and skip buttons
        if (getIntent().getBooleanExtra(Constant.TOUCH_ID_LOGIN_WELCOME_PARENT, false)) {
            mCloseButton.setVisibility(View.VISIBLE);
            mActivateLaterButton.setText(R.string.fp_welcome_btn_skip);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onResume() {
        super.onResume();
        //Screen Tracker
        FAWrapper.getInstance().sendFAScreen(this, R.string.CONST_SCRN_FP_WELCOME, false);
    }

    public void onActivateNowClicked(View view) {
        if (Utils.isTouchIDEnrolledByUser(this)) {
            Intent intent = new Intent(this, TouchIdLoginActivationActivity.class);
            startActivityForResult(intent, Constant.REQUEST_TOUCH_ID_ACTIVATION_PAGE);
            overridePendingTransition(R.anim.right_slide_in, R.anim.left_slide_out);
        } else {
            DialogHelper.createSimpleDialog(this, getString(R.string.fp_welcome_not_enrolled_alert_msg));
        }
    }

    //skip button clicked
    public void onDismissActivity(View view) {

        finish();
        overridePendingTransition(R.anim.left_slide_in, R.anim.right_slide_out);
    }

    //close button clicked
    public void onCloseWelcomeScreen(View view) {

        if (getIntent().getBooleanExtra(Constant.TOUCH_ID_LOGIN_WELCOME_PARENT, false)) {

            DialogHelper.createRememberDialog(this,
                    getString(R.string.fp_welcome_dismiss_alert_title),
                    getString(R.string.fp_welcome_dismiss_alert_content),
                    getString(R.string.fp_failure_lock_btn_confirm),
                    getString(R.string.fp_login_cancel),
                    this);
        } else {

            finish();
            overridePendingTransition(R.anim.left_slide_in, R.anim.right_slide_out);
        }


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Constant.REQUEST_TOUCH_ID_ACTIVATION_PAGE) {
            if (resultCode == RESULT_OK) {
                finish();
                overridePendingTransition(R.anim.left_slide_in, R.anim.right_slide_out);
            }
        }
    }

    @Override
    public void onBackPressed() {
        finish();
        overridePendingTransition(R.anim.left_slide_in, R.anim.right_slide_out);
        super.onBackPressed();
    }


    //Dialog Listeners
    @Override
    public void onPositiveClickListener(boolean isChecked) {


        if (!Utils.isTouchIDLoginActivated(getApplicationContext())) {
            Utils.clearTouchIdUserDetails(TouchIdWelcomeActivity.this);
        }

        if(isChecked) {
            ClnEnv.setPref(TouchIdWelcomeActivity.this, Constant.TOUCH_ID_WELCOME_TOUCH_ID_COUNTER, getResources().getInteger(R.integer.FINGERPRINT_WELCOME_SHOW_MAXIMUM));
        } else {
            //increment the counter if the checkbox is not checked
            int counter = ClnEnv.getPref(TouchIdWelcomeActivity.this, Constant.TOUCH_ID_WELCOME_TOUCH_ID_COUNTER, 0);
            counter++;
            ClnEnv.setPref(TouchIdWelcomeActivity.this, Constant.TOUCH_ID_WELCOME_TOUCH_ID_COUNTER, counter);
        }

        finish();
        overridePendingTransition(R.anim.left_slide_in, R.anim.right_slide_out);

    }

    @Override
    public void onCancelClickListener() {

    }
}
