/*
    Class for Reply Codes

    For those Codes remarked with (BWC*) means 
    - a new Code is introduced for DANGO
    - for Backward Compatible (with DONUT). Likely used by external API.
    
    Keywords
    --------
    $URL: $
    $Rev: $
    $Date: $
    $Author: $
*/

package com.pccw.dango.shared.tool;

import java.io.Serializable;

import com.pccw.wheat.shared.tool.BaseRC;


public class RC extends BaseRC implements Serializable
{
    private static final long serialVersionUID = -1672315436786673669L;
    
    // Ruler                       >123456789012345678901<    >123456789012345678901234<
    public final static String      UXPERR                  = "RC_UXPERR";              /* Unexpected Error                 */   
    public final static String      OUT_OF_RANGE            = "RC_OUT_OF_RANGE";        /* Out Of Range                     */
    
    public final static String      CALLIPC_ERR             = "RC_CALLIPC_ERR";         /* Error when invoking IPC          */
    public final static String      CALLHTTP_ERR            = "RC_CALLHTTP_ERR";        /* Error when invoking O/B HTTP     */      
    public final static String      CALLHTTP_NOTRUN         = "RC_CALLHTTP_NOTRUN";     /* HTTP readCtnt not invoked        */      
    
    public final static String      UNIDFY_IPC              = "RC_UNIDFY_IPC";          /* Unidentified IPC                 */
    public final static String      UXPIPC_ACTN             = "RC_UXPIPC_ACTN";         /* Unexpected IPC Action            */

    
    // Session Control
    public final static String      GWTGUD_MM               = "RC_GWTTOK_MM";           /* GWT Guard Mismatch               */
    public final static String      BRWGUD_MM               = "RC_BRWTOK_MM";           /* Browsing Session Guard Mismatch  */
    public final static String      TCSESS_MM               = "RC_TCSESS_MM";           /* Tomcat Session Mismatch          */
    public final static String      TCSESS_EXP              = "RC_TCSESS_EXP";          /* Tomcat Session Expired        	*/  //added privately by MyHKT mobile app team

    public final static String      UXP_SRCFRM              = "RC_UXP_SRCFRM";          /* Unexpected Src FrmId             */
    public final static String      FE_BLOCKED              = "RC_FE_BLOCKED";          /* F/E is Blocked                   */
    
    // Secure Operation
    public final static String      SOGUD_MM                = "RC_SOGUD_MM";            /* SoGud Mismatch                   */
    public final static String      DISABLED_FUNC           = "RC_DISABLED_FUNC";       /* Disabled Function                */
    public final static String      NOT_AUTH_PSNTY          = "RC_NOT_AUTH_PSNTY";      /* Not Authorized Person Type       */
    
    
    /* Chat Related */
    public final static String      CHAT_SIGN_MM            = "RC_CHAT_SIGN_MM";        /* Chat Signature Mismatch          */
    public final static String      CHAT_IVPARM             = "RC_CHAT_IVPARM";         /* Chat Invalid Parameters          */

    
    /* Generic for the Application */
    public final static String      NO_CUSTOMER             = "RC_NO_CUSTOMER";         /* Can't find CUST (BWC*)           */
    public final static String      NO_SUBN                 = "RC_NO_SUBN";             /* No SUBN found                    */
    public final static String      NO_LOGIN_ID             = "RC_NO_LOGIN_ID";         /* Login Id does not exist          */
    public final static String      LOGIN_ID_EXIST          = "RC_LOGIN_ID_EXIST";      /* Login Id does already exist      */
    public final static String      CUS_ALDY_REG            = "RC_CUS_ALDY_REG";        /* Customer already registered      */
    public final static String      NOT_YET_RCUS            = "RC_NOT_YET_RCUS";        /* Not Yet Registered (BWC*)        */
    public final static String      RCUS_IVLOGINID          = "RC_RCUS_IVLOGINID";      /* Invalid Login Id (BWC*)          */
    public final static String      RCUS_ILLOGINID          = "RC_RCUS_ILLOGINID";      /* IvLen Login Id (BWC*)            */
    public final static String      NOT_AUTH                = "RC_NOT_AUTH";            /* Action Not Authorized            */
    public final static String      API_NOTAUTH             = "RC_API_NOTAUTH";         /* API Not Authorized               */
    public final static String      NOT_SUPP_SEG            = "RC_NOT_SUPP_SEG";        /* Not Supported Cust Segment       */
    public final static String      NOT_SUPP_PHYLUM         = "RC_NOT_SUPP_PHYLUM";     /* Not Supported Cust Phylum        */
    public final static String      INACTIVE_SVEE           = "RC_INACTIVE_SVEE";       /* SVEE is inactive                 */
    public final static String      INACTIVE_CUST           = "RC_INACTIVE_CUST";       /* CUST is inactive                 */
    public final static String      LOGIN_FAIL              = "RC_LOGIN_FAIL";          /* Login Fail                       */
    public final static String      REVERIFY_FAIL           = "RC_REVERIFY_FAIL";       /* Reverify Fail (after 1st login)  */
    public final static String      PWD_MISMATCH            = "RC_PWD_MISMATCH";        /* Password Mismatch                */
    public final static String      PWD_NO_CHANGE           = "RC_PWD_NO_CHANGE";       /* Password is not changed          */
    public final static String      SECANS_MISMATCH         = "RC_SECANS_MISMATCH";     /* Security Answer Mismatch         */
    public final static String      NOT_SUPP_SESS           = "RC_NOT_SUPP_SESS";       /* Not Supported Session            */
    public final static String      NOT_SUPP_SVEE_STATE     = "RC_NOT_SUPP_SVEE_STATE"; /* Not Supported SVEE state         */      
    public final static String      CANT_IDFY_CUST          = "RC_CANT_IDFY_CUST";      /* Cannot Identify Customer         */
    public final static String      SAME_LOGIN_ID           = "RC_SAME_LOGIN_ID";       /* Login Ids are the Same           */
    public final static String      LOGGED_IN               = "RC_LOGGED_IN";           /* Login Id is now logged in        */
    public final static String      BOMCUS_NOTFND           = "RC_BOMCUS_NOTFND";       /* BOM CusNum Not Found             */
    public final static String      ACCT_NOTFND             = "RC_ACCT_NOTFND";         /* SysTy/Account Not Found          */
    public final static String      SIP                     = "RC_SIP";                 /* SIP                              */
    public final static String      WIP                     = "RC_WIP";                 /* WIP                              */
    public final static String      IV_LOB                  = "RC_IV_LOB";              /* Invalid LOB                      */
    public final static String      UXP_ACTN                = "RC_UXP_ACTN";            /* Unexpected Action                */
    public final static String      IV_SESS                 = "RC_IV_SESS";             /* Invalid Session                  */
    public final static String      NOT_SRV_ZOMBIE          = "RC_NOT_SRV_ZOMBIE";      /* Not Serve Zombie                 */
    public final static String      PCD_ALONE_DIALUP        = "RC_PCD_ALONE_DIALUP";    /* Standalone Dialup of PCD         */
    public final static String      ALDY_OPT_IN             = "RC_ALDY_OPT_IN";         /* Already Opt-In                   */
    public final static String      NOT_ELIG                = "RC_NOT_ELIG";            /* Not Eligible                     */
    public final static String      IV_OPTFOR               = "RC_IV_OPTFOR";           /* Invalid Opt In/Out flag          */
    
    
    /* For Club and Consolidated APIs (mainly for BWC*) */
    public final static String      SUBN_FND                = "RC_SUBN_FND";             /* Found Subscription              */
    public final static String      IV_TNC                  = "RC_IV_TNC";               /* Invalid TNC indicator           */
    public final static String      CTMAIL_SB_NIL           = "RC_CTMAIL_SB_NIL";        /* CtMail should be Nil            */
    public final static String      IL_CLUBLI               = "RC_IL_CLUBLI";            /* IvLen of Club Login Id          */
    public final static String      CUST_EXIST              = "RC_CUST_EXIST";           /* CUST does exist                 */
    public final static String      NO_MEMBER               = "RC_NO_MEMEBR";            /* Club Member does not exist      */
    public final static String      IN_USE                  = "RC_IN_USE";               /* Login Id In Use                 */
    public final static String      UX_CLUBRES              = "RC_UX_CLUBRES";           /* Unexpected Club API Response    */
    public final static String      IVSRCSYS                = "RC_IVSRCSYS";             /* Invalid Source System           */
    public final static String      NO_LOGINID              = "RC_NO_LOGINID";           /* No Login Id Specified           */
    public final static String      RCUS_IVPROMO            = "RC_RCUS_IVPROMO";         /* Invalid Promotion Opt (BWC*)    */
    public final static String      IV_4TNG                 = "RC_IV_4TNG";              /* Invalid For Tap&Go              */  
    
    
    /* Login State RRs */
    public final static String      RESET_SVEE              = "RC_RESET_SVEE";          /* Shld Reset the SVEE Pwd          */
    public final static String      INITPWD_SVEE            = "RC_INITPWD_SVEE";        /* Shld Init the SVEE Pwd           */
    public final static String      ACTV_SVEE               = "RC_ACTVSVEE";            /* Shld Activate the SVEE           */
    public final static String      FILL_SVEE               = "RC_FILL_SVEE";           /* Shld Fill the SVEE details       */
    public final static String      FILL_SVEE_NOTNC         = "RC_FILL_SVEE_NOTNC";     /* Shld Fill SVEE details w/o TNC   */
    public final static String      FILL_SVEE4CLUB          = "RC_FILL_SVEE4CLUB";      /* Shld Fill SVEE for Club          */
    public final static String      FILL_SVEE4CLBL          = "RC_FILL_SVEE4CLBL";      /* Shld Fill SVEE for CLBL          */
    public final static String      FILL_SVEE4CLBL_NOTNC    = "RC_FILL_SVEE4CLBL_NOTNC";/* Shld Fill SVEE4CLBL no Need TNC  */
    public final static String      FILL_SVEE4CLAV          = "RC_FILL_SVEE4CLAV";      /* Shld fill SVEE for ClubActv'ed   */
    public final static String      LOGIN_STERR             = "RC_LOGIN_STERR";         /* Unexpected State when Login      */
    
    
    /* Registration/Activation specific */
    public final static String      AIK_CORRUPTED           = "RC_AIK_CORRUPTED";       /* AIK is corrupted                 */
    public final static String      AIK_NRF                 = "RC_AIK_NRF";             /* CUST derived from AIK is NRF     */
    public final static String      AIK_MISMATCH            = "RC_AIK_MISMATCH";        /* Id Doc mismatch with AIK rec     */
    public final static String      ALNK_FAIL               = "RC_ALNK_FAIL";           /* Read ALNK failed                 */
    public final static String      ACTCODE_MISMATCH        = "RC_ACTCODE_MISMATCH";    /* ActCode mistmatch in ALNK        */
    public final static String      ACTCODE_REGEN_EXCEED    = "RC_ACTCODE_REGEN_EXCEED";/* ActCode regen exceed daily limit */
    public final static String      NOT_ACPTTNC             = "RC_NOT_ACPTTNC";         /* Not Accepted TNC                 */
    public final static String      NOT_ALLOW_COMM          = "RC_NOT_ALLOW_COMM";      /* Not Allow for Commercial         */
    public final static String      DUMM_DOCNUM             = "RC_DUMM_DOCNUM";         /* Dummy Document Number            */
    
    
    /* Eclosion specific */
    public final static String      CUS_MISMATCH            = "RC_CUS_MISMATCH";        /* Id Doc Mismatch                  */
    

    /* CUST */
    public final static String      CUST_IVDOCTY            = "RC_CUST_IVDOCTY";        /* Invalid Document Type            */        
    public final static String      CUST_DOCTYMMPH          = "RC_CUST_DOCTYMMPH";      /* DocType Mismatch w/ Phylum       */
    public final static String      CUST_NLDOCNUM           = "RC_CUST_NLDOCNUM";       /* NonASC/IvLen Document Number     */ 
    public final static String      CUST_ILDOCNUM           = "RC_CUST_ILDOCNUM";       /* IvLen Document Number (BWC*)     */
    public final static String      CUST_ICDOCNUM           = "RC_CUST_ICDOCNUM";       /* Incompatible Case of DocNum      */
    public final static String      CUST_IVDOCNUM           = "RC_CUST_IVDOCNUM";       /* Invalid DocNum (for ARQ, BWC*)   */
    public final static String      CUST_IVPREMIER          = "RC_CUST_IVPREMIER";      /* NonASC/IvLen Premier             */

    
    /* SVEE */
    public final static String      SVEE_NLLOGINID          = "RC_SVEE_NLLOGINID";      /* NonASC/IvLen Login Id            */
    public final static String      SVEE_ICLOGINID          = "RC_SVEE_ICLOGINID";      /* Incompatible Case of Login Id    */
    public final static String      SVEE_IVLOGINID          = "RC_SVEE_IVLOGINID";      /* Invalid Login Id                 */
    public final static String      SVEE_NLPWD              = "RC_SVEE_NLPWD";          /* NonASC/IvLen Password            */
    public final static String      SVEE_IVPWDCBN           = "RC_SVEE_IVPWDCBN";       /* Invalid Password Combination     */
    public final static String      SVEE_ILNICKNAME         = "RC_SVEE_ILNICKNAME";     /* Nickname IvLen                   */
    public final static String      SVEE_RESV_NCKNM         = "RC_SVEE_RESV_NCKNM";     /* Reserved Nickname                */
    public final static String      SVEE_NLCTMAIL           = "RC_SVEE_NLCTMAIL";       /* NonASC/IvLen CT Mail             */
    public final static String      SVEE_IVCTMAIL           = "RC_SVEE_IVCTMAIL";       /* Invalid CT Mail                  */
    public final static String      SVEE_ICCTMAIL           = "RC_SVEE_ICCTMAIL";       /* Incompatible Case of CT Mail     */
    public final static String      SVEE_IVCTMOB            = "RC_SVEE_IVCTMOB";        /* Invalid CT Mob                   */
    public final static String      SVEE_IVMOBALRT          = "RC_SVEE_IVMOBALTR";      /* Invalid CT Mob Alert             */
    public final static String      SVEE_IVLANG             = "RC_SVEE_IVLANG";         /* Invalid Language                 */
    public final static String      SVEE_IVSECQUS           = "RC_SVEE_IVSECQUS";       /* Invalid Sec Qus                  */
    public final static String      SVEE_NONEED_SECQA       = "RC_SVEE_NONEED_SECQA";   /* No Need Security QA              */
    public final static String      SVEE_IVSECANS           = "RC_SVEE_IVSECANS";       /* Invalid Sec Ans                  */
    public final static String      SVEE_NLSECANS           = "RC_SVEE_NLSECANS";       /* NonASC/IvLen Sec Ans             */
    public final static String      SVEE_NLSALESCHNL        = "RC_SVEE_NLSALESCHNL";    /* NonASC/IvLen Sales Channel       */
    public final static String      SVEE_NLTEAMCD           = "RC_SVEE_NLTEAMCD";       /* NonASC/IvLen Team Code           */
    public final static String      SVEE_NLSTAFFID          = "RC_SVEE_NLSTAFFID";      /* NonASC/IvLen Staff Id            */
    public final static String      SVEE_IVSTATUS           = "RC_SVEE_IVSTATUS";       /* Invalid Status                   */         
    public final static String      SVEE_IVSTATE            = "RC_SVEE_IVSTATE";        /* Invalid State                    */
    public final static String      SVEE_IVPROMOOPT         = "RC_SVEE_IVPROMOOPT";     /* Invalid PromoOpt                 */
    public final static String      SVEE_CFMPWDMM           = "RC_SVEE_CFMPWDMM";       /* Confirmed Pwd mismatch           */
    
    
    /* BCIF */
    public final static String      BCIF_ILCUSTNM           = "RC_BCIF_ILCUSTNM";       /* IvLen Company Name               */
    public final static String      BCIF_NLINDUST           = "RC_BCIF_ILINDUST";       /* NonASC/IvLen Industry            */
    public final static String      BCIF_NLNUSTFF           = "RC_BCIF_NLNUSTFF";       /* NonASC/IvLen Nu Of Staff         */
    public final static String      BCIF_NLNUPRSN           = "RC_BCIF_NLNUPRSN";       /* NonASC/IvLen Nu Of Presence      */
    public final static String      BCIF_IVXBRDR            = "RC_BCIF_IVXBRDR";        /* Invalid X-Border                 */
    public final static String      BCIF_ILPRICTNM          = "RC_BCIF_ILPRICTNM";      /* IvLen Pri Contact Name           */
    public final static String      BCIF_IVPRICTTL          = "RC_BCIF_IVPRICTTL";      /* Invalid Pri Title                */
    public final static String      BCIF_NLPRICTEM          = "RC_BCIF_NLPRICTEM";      /* NonASC/IvLen Pri Contact EMail   */
    public final static String      BCIF_IVPRICTEM          = "RC_BCIF_IVPRICTEM";      /* Invalid Pri Contact EMail        */
    public final static String      BCIF_ILPRICTJT          = "RC_BCIF_ILPRICTJT";      /* IvLen Pri Job Title              */
    public final static String      BCIF_NLPRICTTEL         = "RC_BCIF_NLPRICTTEL";     /* NonASC/IvLen Pri Contact Tel     */
    public final static String      BCIF_IVPRICTTEL         = "RC_BCIF_IVPRICTTEL";     /* Invalid Pri Contact Tel          */
    public final static String      BCIF_NLPRICTMOB         = "RC_BCIF_NLPRICTMOB";     /* NonASC/IvLen Pri Contact Mob     */
    public final static String      BCIF_IVPRICTMOB         = "RC_BCIF_IVPRICTMOB";     /* Invalid Pri Contact Mob          */
    public final static String      BCIF_NO_PRICTDN         = "RC_BCIF_NO_PRICTDN";     /* No Pri Ct Directory Number       */
    public final static String      BCIF_ILSECCTNM          = "RC_BCIF_ILSECCTNM";      /* IvLen Sec Contact Name           */
    public final static String      BCIF_IVSECCTTL          = "RC_BCIF_IVSECCTTL";      /* Invalid Sec Title                */
    public final static String      BCIF_NLSECCTEM          = "RC_BCIF_NLSECCTEM";      /* NonASC/IvLen Sec Contact EMail   */
    public final static String      BCIF_IVSECCTEM          = "RC_BCIF_IVSECCTEM";      /* Invalid Sec Contact EMail        */
    public final static String      BCIF_ILSECCTJT          = "RC_BCIF_ILSECCTJT";      /* IvLen Sec Job Title              */
    public final static String      BCIF_NLSECCTTEL         = "RC_BCIF_NLSECCTTEL";     /* NonASC/IvLen Sec Contact Tel     */
    public final static String      BCIF_IVSECCTTEL         = "RC_BCIF_IVSECCTTEL";     /* Invalid Sec Contact Tel          */
    public final static String      BCIF_NLSECCTMOB         = "RC_BCIF_NLSECCTMOB";     /* NonASC/IvLen Sec Contact Mob     */
    public final static String      BCIF_IVSECCTMOB         = "RC_BCIF_IVSECCTMOB";     /* Invalid Sec Contact Mob          */
    
    
    /* SUBN */
    public final static String      SUBN_OVERFLOW           = "RC_SUBN_OVERFLOW";       /* Supported SUBN overflow          */
    public final static String      SUBN_IVSYSTY            = "RC_SUBN_IVSYSTY";        /* Invalid SYSTY                    */
    public final static String      SUBN_IVLOB              = "RC_SUBN_IVLOB";          /* Invalid LOB                      */
    public final static String      SUBN_NLACCTNUM          = "RC_SUBN_NLACCTNUM";      /* NonASC/IvLen of AcctNum          */
    public final static String      SUBN_NONEED_ACCTNUM     = "RC_SUBN_NONEED_ACCTNUM"; /* No Need AcctNum                  */
    public final static String      SUBN_NONEED_SRVNUM      = "RC_SUBN_NONEED_SRVNUM";  /* No Need SrvNum                   */
    public final static String      SUBN_IVACCTNUM          = "RC_SUBN_IVACCTNUM";      /* Invalid AcctNum                  */
    public final static String      SUBN_IVAN_IMS           = "RC_SUBN_IVAN_IMS";       /* Invalid AcctNum for IMS          */
    public final static String      SUBN_IVAN_TV            = "RC_SUBN_IVAN_TV";        /* Invalid AcctNum for TV           */
    public final static String      SUBN_ILSRVNUM           = "RC_SUBN_NLSRVNUM";       /* NonASC/IvLen of SrvNum           */
    public final static String      SUBN_IVSRVNUM           = "RC_SUBN_IVSRVNUM";       /* Invalid SrvNum                   */
    public final static String      SUBN_NLFSA              = "RC_SUBN_NLFSA";          /* NonASC/IvLen of FSA              */
    public final static String      SUBN_IVSN_MOB           = "RC_SUBN_IVSN_MOB";       /* Invalid SrvNum for MOB           */
    public final static String      SUBN_IVSN_LTS           = "RC_SUBN_IVSN_LTS";       /* Invalid SrvNum for LTS           */
    public final static String      SUBN_IVSN_PCD           = "RC_SUBN_IVSN_PCD";       /* Invalid SrvNum for PCD           */
    public final static String      SUBN_ILALIAS            = "RC_SUBN_ILALIAS";        /* IvLen Alias                      */
    public final static String      SUBN_NO_ASO             = "RC_SUBN_NO_ASO";         /* No Association                   */
    
    
    /* SimpRegn */
    public final static String      SFRG_ILSALESCHNL        = "RC_SFRG_ILSALESCHNL";    /* SalesChnl IvLen                  */
    public final static String      SFRG_ILTEAMCODE         = "RC_SFRG_ILTEAMCODE";     /* TeamCode IvLen                   */
    public final static String      SFRG_ILSTAFFID          = "RC_SFRG_ILSTAFFID";      /* Staff Id IvLen                   */
    public final static String      SFRG_IVRMODE            = "RC_SFRG_IVRMODE";        /* Invalid Rmode                    */
    public final static String      SFRG_IVKEYTY            = "RC_SFRG_IVKEYTY";        /* Invalid Key Type                 */
    public final static String      SFRG_ILKEYVAL           = "RC_SFRG_ILKEYVAL";       /* KeyVal IvLen                     */

    
    /* IMSBGW */
    public final static String      IMSB_UNSUCC             = "RC_IMSB_UNSUCC";         /* IMSB unsuccess operation         */
    
    
    /* IMSCGW */
    public final static String      IMSC_UNSUCC             = "RC_IMSC_UNSUCC";         /* IMSC unsuccess operation         */
    public final static String      IMSC_EMTY_SUMMURL       = "RC_IMSC_EMTY_SUMMURL";   /* IMSC empty summary URL           */
    
    
    /* LTSAGW */
    public final static String      LTSA_UNSUCC             = "RC_LTSA_UNSUCC";         /* LTSA unsuccess operation         */
    public final static String      LTSA_CPLN_FOUT          = "RC_LTSA_CPLN_FOUT";      /* LTSA Call Plan Filtered-out      */
    public final static String      LTSA_GLG_FAIL           = "RC_LTSA_GLG_FAIL";       /* LTSA Get Language Fail           */
    public final static String      LTSA_UPLG_FAIL          = "RC_LTSA_UPLG_FAIL";      /* LTSA Update Language Fail        */
    public final static String      LTSA_GEM_FAIL           = "RC_LTSA_GEM_FAIL";       /* LTSA Get Email Fail              */
    public final static String      LTSA_GPI_FAIL           = "RC_LTSA_GPI_FAIL";       /* LTSA Get PayInfo Fail            */
    public final static String      LTSA_EMTY_EMAIL         = "RC_LTSA_EMTY_EMAIL";     /* LTSA Empty Email                 */
    public final static String      LTSA_UPEM_FAIL          = "RC_LTSA_UPEM_FAIL";      /* LTSA Update Email Fail           */
    public final static String      LTSA_UPPI_FAIL          = "RC_LTSA_UPPI_FAIL";      /* LTSA Update PayInfo Fail         */
    public final static String      LTSA_GBA_FAIL           = "RC_LTSA_GBA_FAIL";       /* LTSA Get Bill Address Fail       */
    public final static String      LTSA_UPBA_FAIL          = "RC_LTSA_UPBA_FAIL";      /* LTSA update Bill Address Fail    */
    public final static String      LTSA_GCIF_FAIL          = "RC_LTSA_GCIF_FAIL";      /* LTSA Get Contact Info Fail       */
    public final static String      LTSA_UPCIF_FAIL         = "RC_LTSA_UPCIF_FAIL";     /* LTSA Update Contact Info Fail    */
    public final static String      LTSA_GPON_FAIL          = "RC_LTSA_GPON_FAIL";      /* LTSA Get PON Fail                */
    public final static String      LTSA_GCUS_FAIL          = "RC_LTSA_GCUS_FAIL";      /* LTSA Get Customer Fail           */
    public final static String      LTSA_BUPD_NOUPD         = "RC_LTSA_BUPD_NOUPD";     /* LTSA Bill Info Upd not allowed   */
    public final static String      LTSA_CPLN_11_17		 	= "RC_LTSA_CPLN_11_17";    /* LTSA Call Plan Case 0011-0017   */
    
    /* IWPAGW */
    public final static String      IWPA_NORESP             = "RC_IWPA_NORESP";         /* IWPA No Response                 */


    /* MOBAGW */
    public final static String      MOBA_UNSUCC             = "RC_MOBA_UNSUCC";         /* MOBA unsuccess operation         */
    public final static String      MOBA_NO_SUBN            = "RC_MOBA_NO_SUBN";        /* MOBA no subscriber               */
    public final static String      MOBA_ALSB_IVPWD         = "RC_MOBA_ALSB_IVPWD";     /* MOBA invalid password by ALSB    */
    public final static String      MOBA_ALSB_SIP           = "RC_MOBA_ALSB_SIP";       /* MOBA SIP by ALSB                 */
    public final static String      MOBA_TPUP_SIMU          = "RC_MOBA_TPUP_SIMU";      /* MOBA Simultaneous Topup          */
    public final static String      MOBA_TPUP_DUPL          = "RC_MOBA_TPUP_DUPL";      /* MOBA Duplicate Topup             */
    
    
    /* MOBBGW */
    public final static String      MOBB_NO_UPD             = "RC_MOBB_NO_UPD";         /* MOBB No update allowed           */
    public final static String      MOBB_UNSUCC             = "RC_MOBB_UNSUCC";         /* MOBB unsuccess operation         */
    
    
    /* USRAGW */
    public final static String      USRA_UNSUCC             = "RC_USRA_UNSUCC";         /* USRA unsuccess operation         */
    public final static String      USRA_LNTT_MR_FAIL       = "RC_USRA_LNTT_MR_FAIL";   /* USRA Modem Reset fail            */
    public final static String      USRA_LNTT_MDM_NF        = "RC_USRA_LNTT_MDM_NF";    /* USRA Modem Info not found        */
    public final static String      USRA_LNTT_TXTG_NF       = "RC_USRA_LNTT_TXTG_NF";   /* USRA TXTG not found              */
    public final static String      USRA_LT_ERR             = "RC_USRA_LT_ERR";         /* Line Test - Error                */
    public final static String      USRA_LT_PREP            = "RC_USRA_LT_PREP";        /* Line Test - Prepare              */
    public final static String      USRA_LT_UXSTS           = "RC_USRA_LT_UXSTS";       /* Line Test - Unexpected status    */
    public final static String      USRA_LT_IV_RID          = "RC_USRA_LT_IV_RID";      /* Line Test - Invalid rid          */
    
    
    /* USRBGW */
    public final static String      USRB_UNSUCC             = "RC_USRB_UNSUCC";         /* USRB unsuccess operation         */
    public final static String      USRB_SR_EXIST           = "RC_USRB_SR_EXIST";       /* USRB SR exists                   */
    public final static String      USRB_PENDSR_FND         = "RC_USRB_PENDSR_FND";     /* USRB Pending SR found            */
    public final static String      USRB_NOEMPYSR           = "RC_USRB_NOEMPYSR";       /* USRB No Empty SR                 */
    public final static String      USRB_EAT_UNSUCC         = "RC_USRB_EAT_UNSUCC";     /* USRB Enquire Available Tm Fail   */
    
    
    /* CVI */
    public final static String      CVI_UNSUCC              = "RC_CVI_UNSUCC";           /* CVI unsuccess operation         */
    public final static String      CVI_IVCUST              = "RC_CVI_IVCUST";           /* CVI Invalid Cust                */
    public final static String      CVI_NON_VBP             = "RC_CVI_NON_VBP";          /* CVI Non VBP                     */
    public final static String      CVI_SEC_MRT             = "RC_CVI_SEC_MRT";          /* CVI Secondary MRT               */
    
    
    /* Add-on */
    public final static String      AO_MMP_NO_SUBN          = "RC_AO_MMP_NO_SUBN";       /* Add-On MMP No Subn              */
    public final static String      AO_MMP_IVPWD            = "RC_AO_MMP_IVPWD";         /* Add-On MMP Invalid Password     */
    public final static String      AO_MMP_UNSUCC           = "RC_AO_MMP_UNSUCC";        /* Add-On MMP unsuccess operation  */
    public final static String      AO_MMP_OA               = "RC_AO_MMP_OA";            /* Add-On MMP OA                   */
    public final static String      AO_MMP_UKNLOB           = "RC_AO_MMP_UKNLOB";        /* Add-On MMP Unknown LOB          */
    public final static String      AO_MMP_UKNPLF           = "RC_AO_MMP_UKNPLF";        /* Add-On MMP Unknown Platform     */
    public final static String      AO_EAI_IVPWD            = "RC_AO_EAI_IVPWD";         /* Add-On EAI Invalid Password     */
    public final static String      AO_IVSESS               = "RC_AO_IVSESS";            /* ADD-ON - Invalid Session        */
    public final static String      AO_SUBN_FND             = "RC_AO_SUBN_FND";          /* ADD-ON - Subscription Found     */
    public final static String      AO_INDIV_H              = "RC_AO_INDIV_H";           /* ADD-ON - Individual Account (H) */
    public final static String      AO_INDIV_C              = "RC_AO_INDIV_C";           /* ADD-ON - Individual Account (C) */
    public final static String      AO_TOO_MANY             = "RC_AO_TOO_MANY";          /* ADD-ON - Too many service added */
    public final static String      AO_PSPH_FAIL            = "RC_AO_PSPH_FAIL";         /* ADD-ON - Passphrase Failed      */
    
    /* System Peer */
    public final static String      INACTIVE_SYPR           = "RC_INACTIVE_SYPR";       /* SYPR is inactive                 */
    public final static String      INACTIVE_ROLE           = "RC_INACTIVE_ROLE";       /* ROLE is inactive                 */
    public final static String      SYPR_NLPWD              = "RC_SYPR_NLPWD";          /* NonASC/IvLen Pwd                 */
    public final static String      SYPR_IVPWDCBN           = "RC_SYPR_IVPWDCBN";       /* Invalid Password Combination     */    
    public final static String      SYPR_REQ_NEWPWD         = "RC_SYPR_REQ_NEWPWD";     /* Require a New Pwd                */
    
    
    /* ARQ */
    public final static String      ARQ_NAOBJ               = "RC_ARQ_NAOBJ";           /* Not Applicable Object            */
    public final static String      ARQ_NAAPITYPE           = "RC_ARQ_NAAPITYPE";       /* N/A API Type                     */
    public final static String      ARQ_NASYSID             = "RC_ARQ_NASYSID";         /* N/A System Id                    */
    public final static String      ARQ_NASYSPWD            = "RC_ARQ_NASYSPWD";        /* N/A System Pwd                   */
    public final static String      ARQ_NAUSERID            = "RC_ARQ_NAUSERID";        /* N/A User Id                      */
    public final static String      ARQ_NAPHYLUM            = "RC_ARQ_NAPHYLUM";        /* N/A Phylum                       */

    public final static String      ARQ_IVDOCNUM6DIG        = "RC_ARQ_IVDOCNUM6DIG";    /* Invalid DocNum for (No 6 Dig)    */
    public final static String      ARQ_DOCNUMMISMCH        = "RC_ARQ_DOCNUMMISMCH";    /* DocNum Mismatch                  */

    public final static String      ARQ_IVDIRNUM            = "RC_ARQ_IVDIRNUM";        /* Invalid Directory Number         */
    public final static String      ARQ_IVLANG              = "RC_ARQ_IVLANG";          /* Invalid Language                 */
    public final static String      ARQ_IVSMS               = "RC_ARQ_IVSMS";           /* Invalid SMS Content              */
    
    
    /* BIIF */
    public final static String      BIIF_IV_MEDIA           = "RC_BIIF_IV_MEDIA";       /* Invalid Bill Media               */
    public final static String      BIIF_IV_LANG            = "RC_BIIF_IV_LANG";        /* Invalid Language                 */
    public final static String      BIIF_NA_ADR             = "RC_BIIF_NA_ADR";         /* Non-Ascii for Address            */
    public final static String      BIIF_IL_ADR             = "RC_BIIF_IL_ADR";         /* Invalid Length for Address       */
    public final static String      BIIF_NIL_ADR            = "RC_BIIF_NIL_ADR";        /* Nil Address                      */
    public final static String      BIIF_NIL_POX            = "RC_BIIF_NIL_POX";        /* Nil Postal Box                   */
    public final static String      BIIF_NIL_EMAIL          = "RC_BIIF_NIL_EMAIL";        /* Nil Email                  */
    public final static String      BIIF_NA_EMAIL           = "RC_BIIF_NA_EMAIL";       /* Non-Ascii for Email              */
    public final static String      BIIF_IL_EMAIL           = "RC_BIIF_IL_EMAIL";       /* Invalid Length for Email         */
    public final static String      BIIF_IV_EMAIL           = "RC_BIIF_IV_EMAIL";       /* Invalid Email                    */
    public final static String      BIIF_IV_SMSNTF          = "RC_BIIF_IV_SMSNTF";      /* Invalid SMS Notifcation#         */
    
    
    /* BINQ */
    public final static String      BINQ_BINRDY             = "RC_BINQ_BINRDY";         /* Bill not ready                   */
    public final static String      BINQ_BIERR              = "RC_BINQ_BIERR";          /* Bill status error                */
    public final static String      BINQ_WITHHOLD           = "RC_BINQ_WITHHOLD";       /* Bill is withheld                 */

    
    /* SHOP */
    public final static String      SHOP_NO_SHOP            = "RC_SHOP_NO_SHOP";        /* No shop found                    */
    public final static String      SHOP_OVERFLOW           = "RC_SHOP_OVERFLOW";       /* Shop overflow                    */
    public final static String      SHOP_IVSHOPCD           = "RC_SHOP_IVSHOPCD";       /* Invalid Shop Code                */
    public final static String      SHOP_IVSHOPTY           = "RC_SHOP_IVSHOPTY";       /* Invalid Shop Type                */
    public final static String      SHOP_IVSHOPCAT          = "RC_SHOP_IVSHOPCAT";      /* Invalid Shop Cat                 */
    public final static String      SHOP_IVAREA             = "RC_SHOP_IVAREA";         /* Invalid Area                     */
    public final static String      SHOP_IVDIST_EN          = "RC_SHOP_IVDIST_EN";      /* Invalid District (EN)            */
    public final static String      SHOP_IVDIST_ZH          = "RC_SHOP_IVDIST_ZH";      /* Invalid District (ZH)            */
    public final static String      SHOP_IVADDR_EN          = "RC_SHOP_IVADDR_EN";      /* Invalid District (EN)            */
    public final static String      SHOP_IVADDR_ZH          = "RC_SHOP_IVADRR_ZH";      /* Invalid District (ZH)            */
    public final static String      SHOP_IVCT_NUM           = "RC_SHOP_IVCT_NUM";       /* Invalid Contact Number           */
    public final static String      SHOP_IVBZHR_EN          = "RC_SHOP_BZHR_EN";        /* Invalid Buzz Hour (EN)           */
    public final static String      SHOP_IVBZHR_ZH          = "RC_SHOP_BZHR_ZH";        /* Invalid Buzz Hour (ZH)           */
    public final static String      SHOP_IVLONG             = "RC_SHOP_IVLONG";         /* Invalid Longitude                */
    public final static String      SHOP_IVLAT              = "RC_SHOP_IVLAT";          /* Invalid Latitude                 */
    public final static String      SHOP_IVDESN_EN          = "RC_SHOP_DESN_EN";        /* Invalid Description (EN)         */
    public final static String      SHOP_IVDESN_ZH          = "RC_SHOP_DESN_ZH";        /* Invalid Description (EN)         */
    public final static String      SHOP_IVNAME_EN          = "RC_SHOP_NAME_EN";        /* Invalid Name (EN)                */
    public final static String      SHOP_IVNAME_ZH          = "RC_SHOP_NAME_ZH";        /* Invalid Name (ZH)                */
    
    
    /* SPNF - Smart Phone Notification */
    public final static String      SPNF_SENDFAIL           = "RC_SPNF_SENDFAIL";       /* Send SmartPhone Nfn Failed       */

    
    /* SPSS */
    public final static String      SPSS_IV_DID             = "RC_SPSS_IV_DID";          /* Invalid Device ID               */
    public final static String      SPSS_IV_DTY             = "RC_SPSS_IV_DTY";          /* Invalid Device Type             */
    public final static String      SPSS_IV_NIND            = "RC_SPSS_IV_NIND";         /* Invalid Notification Indicator  */
    public final static String      SPSS_IV_LANG            = "RC_SPSS_IV_LANG";         /* Invalid Language                */
    public final static String      SPSS_IV_CKSM            = "RC_SPSS_IV_CKSM";         /* Invalid Checksum                */
    
    
    /* SR */
    public final static String      SR_IVCTNUM              = "RC_SR_IVCTNUM";           /* Invalid contact number          */
    public final static String      SR_IVCTNAME             = "RC_SR_IVCTNAME";          /* Invalid contact name            */
    public final static String      SR_IVAPPTDT             = "RC_SR_IVAPPTDT";          /* Invalid appointment date        */
    public final static String      SR_IVAPPTTS             = "RC_SR_IVAPPTTS";          /* Invalid appointment timeslot    */
    public final static String      SR_IVENSPKR             = "RC_SR_IVENSPKR";          /* Invalid english speaker         */
    public final static String      SR_IVSMSLANG            = "RC_SR_IVSMSLANG";         /* Invalid SMS language            */
    public final static String      SR_IVUPDTY              = "RC_SR_IVUPDTY";           /* Invalid update type             */
    public final static String      SR_NONEN_NAME           = "RC_SR_NONENG_NAME";       /* non english character for name  */

    
    /* PLAN */
    public final static String      PLAN_TPUP_EMTY_BOID     = "RC_PLAN_TPUP_EMTY_BOID";  /* Plan - Empty BOID returned      */
    public final static String      PLAN_TPUP_NO_BOID_SEL   = "RC_PLAN_TPUP_NO_BOID_SEL";/* Plan - No BOID selected         */
    public final static String      PLAN_TPUP_DENIED        = "RC_PLAN_TPUP_DENIED";     /* Plan - Topup denied             */
    
    /* CTAC */
    public final static String      CTAC_REQ_INPUT          = "RC_CTAC_REQ_INPUT";       /* Ctac - Inputs missing           */
    public final static String      CTAC_REQ_OFCNUM         = "RC_CTAC_REQ_OFCNUM";      /* Ctac - office number missing    */
    public final static String      CTAC_IVOFCNUM           = "RC_CTAC_IVOFCNUM";        /* Ctac - invalid office number    */
    public final static String      CTAC_REQ_HMNUM          = "RC_CTAC_REQ_HMNUM";       /* Ctac - home number missing      */
    public final static String      CTAC_IVHMNUM            = "RC_CTAC_IVHMNUM";         /* Ctac - invalid home number      */
    public final static String      CTAC_REQ_MOB            = "RC_CTAC_REQ_MOB";         /* Ctac - mobile number missing    */
    public final static String      CTAC_IVMOB              = "RC_CTAC_IVMOB";           /* Ctac - invalid mobile number    */
    public final static String      CTAC_REQ_EMAIL          = "RC_CTAC_REQ_EMAIL";       /* Ctac - email missing            */
    public final static String      CTAC_ILCTMAIL           = "RC_CTAC_ILCTMAIL";        /* Ctac - invalid length of email  */
    public final static String      CTAC_IVCTMAIL           = "RC_CTAC_IVCTMAIL";        /* Ctac - invalid email format     */
    
    
    /* CARE */
    public final static String      CARE_NIL_INP            = "RC_CARE_NIL_INP";         /* CARE - invalid inputs           */
    public final static String      CARE_IVCUSTRID          = "RC_CARE_IVCUSTRID";       /* CARE - invalid CustRid          */
    public final static String      CARE_UPD_NALW           = "RC_CARE_UPD_NALW";        /* CARE - update not allowed       */
    public final static String      CARE_UPD_NALW2          = "RC_CARE_UPD_NALW2";       /* CARE - update not allowed       */
    public final static String      CARE_ILCARE             = "RC_CARE_ILCARE";          /* CARE - Invalid Length           */
    public final static String      CARE_IVCARE             = "RC_CARE_IVCARE";          /* CARE - Invalid Value            */
    public final static String      CARE_ILBIPT             = "RC_CARE_ILBIPT";          /* CARE - Invalid Length           */
    public final static String      CARE_IVBIPT             = "RC_CARE_IVBIPT";          /* CARE - Invalid Value            */
    public final static String      CARE_ILCUNM             = "RC_CARE_ILCUNM";          /* CARE - InvLen Cust Name         */
    public final static String      CARE_ILNKNM             = "RC_CARE_ILNKNM";          /* CARE - InvLen Nick Name         */
    public final static String      CARE_ILFORM             = "RC_CARE_ILFORM";          /* CARE - InvLen Form Number       */
    public final static String      CARE_ILCHNL             = "RC_CARE_ILCHNL";          /* CARE - InvLen Chnl Code         */
    public final static String      CARE_ILCTMA             = "RC_CARE_ILCTMA";          /* CARE - InvLen Contact Mail      */
    public final static String      CARE_ICCTMA             = "RC_CARE_ICCTMA";          /* CARE - InvCase Contact Mail     */
    public final static String      CARE_IVCTMA             = "RC_CARE_IVCTMA";          /* CARE - Invalid Contact Mail     */
    public final static String      CARE_ILCTPH             = "RC_CARE_ILCTPH";          /* CARE - InvLen Contact Phone     */
    public final static String      CARE_IVDOB              = "RC_CARE_IVDOB";           /* CARE - Invalid DOB              */
    public final static String      CARE_IVOPT4DM           = "RC_CARE_IVOPT4DM";        /* CARE - Invalid OptFor DM        */
    public final static String      CARE_IVLANG             = "RC_CARE_IVLANG";          /* CARE - Invalid Language         */
    public final static String      CARE_IVLTSN             = "RC_CARE_IVLTSN";          /* CARE - Invalid # of LTS Subn    */
    public final static String      CARE_IVMOBN             = "RC_CARE_IVMOBN";          /* CARE - Invalid # of MOB Subn    */
    public final static String      CARE_IVIMSN             = "RC_CARE_IVIMSN";          /* CARE - Invalid # of IMS Subn    */
    public final static String      CARE_IVTVN              = "RC_CARE_IVTVN";           /* CARE - Invalid # of TV Subn     */
    public final static String      CARE_IVVITS             = "RC_CARE_IVVITS";          /* CARE - Invalid Visit TS         */
    public final static String      CARE_HITCNT             = "RC_CARE_HITCNT";          /* CARE - reach max hit count      */
    public final static String      CARE_DOROOT             = "RC_CARE_DOROOT";          /* CARE - return root              */
    public final static String      CARE_REDIR              = "RC_CARE_REDIR";           /* CARE - redirect to i-Guard      */
    
    
    /* CEKI */
    public final static String      CEKI_IV_USR             = "RC_CEKI_IV_USR";          /* CEKI - Invalid User             */
    public final static String      CEKI_IV_STATE           = "RC_CEKI_IV_STATE";        /* CEKI - Invalid State            */
    public final static String      CEKI_EXPIRED            = "RC_CEKI_EXPIRED";         /* CEKI - Expired                  */
    public final static String      CEKI_OVERRIDDEN         = "RC_CEKI_OVERRIDDEN";      /* CEKI - Overridden               */
    
    
    /* MIP Related */
    public final static String      CSIM_IN_MIG             = "RC_CSIM_IN_MIG";          /* CSIM in Migration               */
    public final static String      NO_CATY                 = "RC_NO_CATY";              /* No corr CATY rec can be found   */
    
    
    /* BILL AGENT MRT */
    public final static String      BA_MRT_BIIF             = "RC_BA_MRT_BIIF";
    public final static String      BA_MRT_BINQ             = "RC_BA_MRT_BINQ";

    
    public final static String      ALT                		= "RC_ALT";              	 /* ALT  */
    
    public static void main(String rArg[])
    {
        System.out.println(getVer());
        return;
    }
    
    
    public static String getVer()
    {
        return ("$URL: $, $Rev: $");
    }
}    
