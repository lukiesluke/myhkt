package com.pccw.myhkt.model;

import com.pccw.dango.shared.entity.SubnRec;
import com.pccw.myhkt.R;

public class SubService {

    public SubService(int imageRes, String lob) {
        super();
        this.imageRes = imageRes;
        this.lob = lob;
    }

    public int getImageRes() {
        return imageRes;
    }

    public void setImageRes(int imageRes) {
        this.imageRes = imageRes;
    }

    public String getLob() {
        return lob;
    }

    public void setLob(String lob) {
        this.lob = lob;
    }

    private int imageRes;
    private String lob;

    public static int getResId(String lob) {
        if (lob.equals(SubnRec.LOB_PCD)) {
            return R.drawable.logo_netvigator;
        }
        if (lob.equals(SubnRec.LOB_LTS)) {
            return R.drawable.lob_lts_plain_idd;
        }
        if (lob.equals(SubnRec.LOB_TV)) {
            return R.drawable.logo_now;
        }
        if (lob.equals(SubnRec.WLOB_X101)) {
            return R.drawable.logo_1010;
        }
        if (lob.equals(SubnRec.WLOB_CSL)) {
            return R.drawable.logo_csl;
        }
        if (lob.equals(SubnRec.LOB_CSP)) {
            return R.drawable.lob_csp_plain;
        }
        return -1;
    }

    public static String getName(String lob) {
//		if (lob.equals(SubnRec.LOB_NE)) { return "NE";}
//		if (lob.equals(SubnRec.LOB_LTS)) { return R.drawable.logo_fixedline;}
//		if (lob.equals(SubnRec.LOB_TV)) { return R.drawable.logo_now;}
//		if (lob.equals(SubnRec.LOB_101)) { return R.drawable.logo_1010;}
//		if (lob.equals(SubnRec.LOB_MOB)) { return R.drawable.logo_csl;}
//		return -1;
        return lob;
    }
}
