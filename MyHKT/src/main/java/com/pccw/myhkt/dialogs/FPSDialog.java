package com.pccw.myhkt.dialogs;

import android.Manifest;
import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.Settings;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;
import androidx.documentfile.provider.DocumentFile;
import androidx.fragment.app.DialogFragment;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.WriterException;
import com.pccw.dango.shared.entity.SubnRec;
import com.pccw.myhkt.APIsManager;
import com.pccw.myhkt.ClnEnv;
import com.pccw.myhkt.R;
import com.pccw.myhkt.Tool;
import com.pccw.myhkt.Utils;
import com.pccw.myhkt.lib.ui.AAQuery;
import com.pccw.myhkt.qrcode.Contents;
import com.pccw.myhkt.qrcode.QRCodeEncoder;
import com.pccw.myhkt.util.CommonQrProvider;
import com.pccw.myhkt.util.Constant;
import com.pccw.myhkt.util.HmacSHAGenerator;
import com.pccw.myhkt.util.RuntimePermissionUtil;
import com.pccwmobile.tlv.CommonQR;
import com.pccwmobile.tlv.TLV;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Objects;

import model.AdditionalDataFieldTemplate;
import model.MerchantAccountInfo;
import model.MerchantAccountInfoFPS;

public class FPSDialog extends DialogFragment {

    private Bitmap mBmp;
    String mFpsId;
    String mBillNo;
    String mAmount;
    String mAppId;
    String mApiKey;
    String mAccNum;
    int mLob;
    protected AlertDialog alertDialog = null;

    //UAT
    private static final String APP_ID_TV_UAT = "4295818032";
    private static final String APP_ID_LTS_UAT = "4263234265";
    private static final String APP_ID_PCD_UAT = "4272500649";
    private static final String APP_ID_MOB_UAT = "4249398638";
    private static final String APP_ID_101_UAT = "4281327185";
    private static final String APP_ID_CSP_UAT = "4204802576";

    private static final String FPS_ID_TV_UAT = "1643329";
    private static final String FPS_ID_LTS_UAT = "1950765";
    private static final String FPS_ID_PCD_UAT = "1305218";
    private static final String FPS_ID_MOB_UAT = "1523489";
    private static final String FPS_ID_101_UAT = "1659382";
    private static final String FPS_ID_CSP_UAT = "799167473";

    private static final String API_KEY_TV_UAT = "d45UyiCgcfu2fdh4x7KL/UM6AG792oXhjUhaYVluV3qSgnPweLqsy51l1rSHm7eO5IgZEueOoiObbHJNXRKlBA==";
    private static final String API_KEY_LTS_UAT = "C/ikvKB4eas0YbQR6gvpLrhNV3cdVQkfUfG/D9f59El83XfbP9xp0NVlxmNyRFy1H7u2qzOOHhzHjcmRPAlJcw==";
    private static final String API_KEY_PCD_UAT = "xx3i66/Kl9viV7leMuA1HvWLqMrRJI7e6azN9+j56E3B3OlK55f+fKOmgRyAnNXus5vK73Wrvzo3fDsmgIgPWQ==";
    private static final String API_KEY_MOB_UAT = "EXc5y+45h+stbzoYtZM5GPpVlJTqdlYN6fIOiZ3BFNZV4+2/IgQ3KabzZPkVizK+FSYHPRH+2KQK5C+1k+Qx5A==";
    private static final String API_KEY_101_UAT = "/I7MZRqRVbYWCHL4t5j/ic4ojQG12TUQws4pM46cISuuWqYb7snrq3Rp04w6iU65RiXt+2tm46KtBmj8S9BIvQ==";
    private static final String API_KEY_CSP_UAT = "uMTLlM8gKRxSQZIzKQsR3aAXGPOge3ksWWHIAjaDy9FKpQeyiOByn04nhQvvfG8o2UWq85jZuZbiOvCMOhG6yA==";

    //PRD
    private static final String APP_ID_TV_PRD = "8325298587";
    private static final String APP_ID_LTS_PRD = "8090157862";
    private static final String APP_ID_PCD_PRD = "6069089080";
    private static final String APP_ID_MOB_PRD = "9502224525";
    private static final String APP_ID_101_PRD = "8152158845";
    private static final String APP_ID_CSP_PRD = "4870787234";

    private static final String API_KEY_TV_PRD =  "e5tCJWW/s+DwHZCwKpogcL1bFy8lddQXZN50AA0YmBeA1jtvO1b8J8pOLOS95LbMg/lPeEq8RSLz4mL9uRxyyA==";
    private static final String API_KEY_LTS_PRD = "Axbz3cK/hnK+2yDp3zM4KEUJYJJ2UcK0rWsmeoEFrKlBT6nHHkHwTtyqAGTGrNZJNRqF41XtWevjrfGMvz11Tw==";
    private static final String API_KEY_PCD_PRD = "PhDl4+iu3p3cUrj1oWMy08cWH58u1YpVGOvH4pcDth+Gsh8GpuI/Nb92TQumvDO8vcIaTv5CixkiI3XJf6cgvg==";
    private static final String API_KEY_MOB_PRD = "w5LBVAz8gh8p3+yJi1t4M7KyKsNpbVoYRfI9hZuM5OpjsV0nFbI7L5SR3GIv2EAFYEKpzOZ/LGIKuqA4/ioHVQ==";
    private static final String API_KEY_101_PRD = "YL910C5GDacbllB6EY03TGILzJdvU/1s2BSqX8MfnI19awRARva8Xy6ppopBZQJHX/K/jWtIszvrc4Xt/w6wHg==";
    private static final String API_KEY_CSP_PRD = "QD00CgMvhiPWxjKiodVHVNdiT5D/DKdJtriJqYXKisuf/M0Iq0l+nIOYk4S57VtBbd/j5/RmbSFW8DwS0Q4PKw==";

    private static final String FPS_ID_TV_PRD = "3101177";
    private static final String FPS_ID_LTS_PRD = "2986248";
    private static final String FPS_ID_PCD_PRD = "2695278";
    private static final String FPS_ID_MOB_PRD = "2626687";
    private static final String FPS_ID_101_PRD = "2686939";
    private static final String FPS_ID_CSP_PRD = "167612688";

    private static String API_KEY_LTS = API_KEY_LTS_PRD;
    private static String API_KEY_PCD = API_KEY_PCD_PRD;
    private static String API_KEY_TV = API_KEY_TV_PRD;
    private static String API_KEY_MOB = API_KEY_MOB_PRD;
    private static String API_KEY_101 = API_KEY_101_PRD;
    private static String API_KEY_CSP = API_KEY_CSP_PRD;

    private static String APP_ID_LTS = APP_ID_LTS_PRD;
    private static String APP_ID_PCD = APP_ID_PCD_PRD;
    private static String APP_ID_TV = APP_ID_TV_PRD;
    private static String APP_ID_MOB = APP_ID_MOB_PRD;
    private static String APP_ID_101 = APP_ID_101_PRD;
    private static String APP_ID_CSP = APP_ID_CSP_PRD;

    private static String FPS_ID_LTS = FPS_ID_LTS_PRD;
    private static String FPS_ID_PCD = FPS_ID_PCD_PRD;
    private static String FPS_ID_TV = FPS_ID_TV_PRD;
    private static String FPS_ID_MOB = FPS_ID_MOB_PRD;
    private static String FPS_ID_101 = FPS_ID_101_PRD;
    private static String FPS_ID_CSP = FPS_ID_CSP_PRD;

    private String mServiceType;
    private String mDisplayDate;
    private String mBillType;
    private double mAmnt;
    private int width;
    private int height;
    private Bitmap bitmap;
    private ConstraintLayout view;
    private AAQuery aq;

    /**
     * Create a new instance of MyDialogFragment, providing "num"
     * as an argument.
     */
    public static FPSDialog newInstance(boolean mobile, String fpsId, String accNum, String amount, int lob) {
        FPSDialog f = new FPSDialog();
        // Supply num input as an argument.
        Bundle args = new Bundle();
        args.putBoolean(Constant.MY_MOBILE_PARENT_ACTIVITY, mobile);
        args.putString("fpsId", fpsId);
        args.putString("accNum", accNum);
        args.putString("amount", amount);
        args.putInt("lob", lob);
        f.setArguments(args);
        return f;
    }

    private void setBlackStatusBar() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
            getActivity().getWindow().setStatusBarColor(getResources().getColor(R.color.black));
    }

    private void setOrangeStatusBar() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
            getActivity().getWindow().setStatusBarColor(getResources().getColor(R.color.hkt_edittextbgorange));
    }

    private void setBlueStatusBar() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
            getActivity().getWindow().setStatusBarColor(getResources().getColor(R.color.hkt_textcolor));
    }

    private void setGrayStatusBar() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
            getActivity().getWindow().setStatusBarColor(getResources().getColor(R.color.hkt_headingpremier));
    }

    private void setPurpleStatusBar() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
            getActivity().getWindow().setStatusBarColor(getResources().getColor(R.color.mymob_club_sim));
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        determineWindowColor(ClnEnv.getSessionPremierFlag(),
                getArguments().getBoolean(Constant.MY_MOBILE_IS_1010),
                getArguments().getString(Constant.MY_MOBILE_LOB_STRING), ClnEnv.isMyMobFlag());
        setCancelable(false);
        setStyle(DialogFragment.STYLE_NO_FRAME, R.style.TransparentBGDialog);
        mLob = getArguments().getInt("lob");
        mServiceType = getArguments().getString("serviceType");
        mDisplayDate = getArguments().getString("billDt");
        mAccNum = getArguments().getString("acctNum");
        mBillType = getArguments().getString("billType");
        mAmnt = getArguments().getDouble("amtdouble");

        genKeys(mLob);
        generateBillNo();


    }

    private void determineWindowColor(boolean sessionPremierFlag, boolean is1010,
                                      String lobString, boolean isMyMobileParentActivity) {
        if (isMyMobileParentActivity & is1010) setBlackStatusBar();
        else if (isMyMobileParentActivity && SubnRec.LOB_CSP.equalsIgnoreCase(lobString) || SubnRec.WLOB_XCSP.equalsIgnoreCase(lobString))
            setPurpleStatusBar();
        else if (isMyMobileParentActivity) setOrangeStatusBar();
        else if (sessionPremierFlag) setGrayStatusBar();
        else setBlueStatusBar();
    }

    private void generateBillNo() {
        String acctNum = Tool.formatAcctNum14(mAccNum);
        mBillNo = acctNum + "A" + mBillType;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        // retrieve display dimensions
        Rect displayRectangle = new Rect();
        Window window = requireActivity().getWindow();
        window.getDecorView().getWindowVisibleDisplayFrame(displayRectangle);

        inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View layout = inflater.inflate(R.layout.dialog_fps, container, false);

        width = (int) (displayRectangle.width() * 1f);
        height = (int) (displayRectangle.height() * 1f);
        Objects.requireNonNull(getDialog()).getWindow().setLayout(width, height);
        layout.setMinimumWidth(width);
        layout.setMinimumHeight(height);

        view = (ConstraintLayout) layout.findViewById(R.id.frs_details_layout);
        ImageView ivQr = (ImageView) layout.findViewById(R.id.dialog_7eleven_qrcode);
        ImageView ivLob = (ImageView) layout.findViewById(R.id.dialog_7eleven_lob);
        TextView title = (TextView) layout.findViewById(R.id.qr_message);
        TextView tvServiceType = (TextView) layout.findViewById(R.id.tv_servicetype_value);
        TextView tvBillDate = (TextView) layout.findViewById(R.id.tv_billdate_value);
        TextView tvAccNum = (TextView) layout.findViewById(R.id.tv_accnum_value);
        TextView tvBillType = (TextView) layout.findViewById(R.id.tv_billtype_value);
        TextView tvPaymentAmnt = (TextView) layout.findViewById(R.id.tv_paymentamnt_value);
        Button btnSaveQr = (Button) layout.findViewById(R.id.save_qr_btn);
        TextView tv_accnum_label = (TextView) layout.findViewById(R.id.tv_accnum_label);

        title.setText(getActivity().getResources().getString(R.string.qrcode_fps_title));
        ivLob.setImageResource(Utils.getLobIcon(mLob, 0));
        if (mLob == R.string.CONST_LOB_PCD) {
            ivLob.getLayoutParams().width = getResources().getDisplayMetrics().density >= 3.0 ? 450 : 300;
            ivLob.getLayoutParams().height = getResources().getDisplayMetrics().density >= 3.0 ? 160 : 130;
            ivLob.setScaleType(ImageView.ScaleType.FIT_START);
        }
        if (mLob == R.string.CONST_LOB_PCD || mLob == R.string.CONST_LOB_TV) {
            tv_accnum_label.setText(R.string.pcd_summary_recacct_num);
        } else {
            tv_accnum_label.setText(R.string.LTTF_TV_ACCTNUM);
        }

        ivQr.setMinimumWidth(400);
        ivQr.setMinimumHeight(400);

        tvServiceType.setText(mServiceType);
        tvBillDate.setText(mDisplayDate);
        tvAccNum.setText(Tool.formatAcctNum(mAccNum));
        tvBillType.setText(mBillType);
        String amt = Utils.convertStringToPrice(Double.toString(mAmnt));
        tvPaymentAmnt.setText(amt);

        String singleQr = genSingleQrc(mFpsId, mBillNo, Utils.convertDoubleToString(mAmnt, 2), mAppId, mApiKey);
        QRCodeEncoder qrCodeEncoder = new QRCodeEncoder(getContext(),
                "",
                singleQr,
                null,
                Contents.Type.TEXT,
                BarcodeFormat.QR_CODE.toString(),
                300);
        try {
            bitmap = qrCodeEncoder.encodeAsBitmap();
            ivQr.setImageBitmap(bitmap);

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
                ivQr.setBackground(Objects.requireNonNull(getContext()).getDrawable(R.drawable.shadow4));
            else
                ivQr.setBackground(ContextCompat.getDrawable(Objects.requireNonNull(getContext()), R.drawable.shadow4));
        } catch (WriterException e) {
            e.printStackTrace();
        }

        btnSaveQr.setOnClickListener(view -> {
            mBmp = bitmap;
            doSaveQrImage();
        });

        ImageView button = (ImageView) layout.findViewById(R.id.dialog_7eleven_close);
        button.setOnClickListener(layout1 -> {
            // When button is clicked, call up to owning activity.
            getDialog().dismiss();
        });
        return layout;
    }

    @Override
    public void onResume() {
        super.onResume();
        Window window = getDialog().getWindow();
        window.setLayout(width, height);
        window.setGravity(Gravity.CENTER);
    }

    public void doSaveQrImage() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            DocumentFile sourceFile = DocumentFile.fromSingleUri(Objects.requireNonNull(getContext()), saveQRCode());
            if (sourceFile != null && sourceFile.exists()) {
                displayDialog(requireActivity().getResources().getString(R.string.qr_saved_msg));
            }
        } else if (RuntimePermissionUtil.isWriteExternalStoragePermissionGranted(getActivity())) {
            File file = savebitmap();
            if (file.exists()) {
                displayDialog(requireActivity().getResources().getString(R.string.qr_saved_msg));
            }
        } else {
            if (ClnEnv.getPref(getActivity(), Constant.FILE_STORAGE_PERMISSION_DENIED, false)) {
                displaySettingsDialog(getActivity().getString(R.string.qr_permission_denied_settings));
            } else {
                requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        Constant.REQUEST_FILE_STORAGE_PERMISSION);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == Constant.REQUEST_FILE_STORAGE_PERMISSION || requestCode == Constant.REQUEST_LOG_FILE_STORAGE_PERMISSION) {
            //Permission is granted
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                if (requestCode == Constant.REQUEST_FILE_STORAGE_PERMISSION) {
                    savebitmap();
                }

            } else if (Build.VERSION.SDK_INT >= 23 && !shouldShowRequestPermissionRationale(permissions[0])) {
                displaySettingsDialog(getActivity().getString(R.string.qr_permission_denied_settings));
            } else {
                displayDialog(getActivity().getString(R.string.qr_permission_denied));
                //This will return false if the user tick the Don't ask again checkbox
                if (!RuntimePermissionUtil.shouldShowRequestPermission(getActivity())) {
                    ClnEnv.setPref(getActivity().getApplicationContext(), Constant.FILE_STORAGE_PERMISSION_DENIED, true);
                }
            }
        }
    }

    public final void displayDialog(String message) {
        if (alertDialog != null) {
            alertDialog.dismiss();
        }

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage(message);
        builder.setPositiveButton(Utils.getString(getActivity(), R.string.btn_ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                alertDialog = null;
            }
        });

        alertDialog = builder.create();
        alertDialog.show();
    }

    public final void displaySettingsDialog(String message) {
        if (alertDialog != null) {
            alertDialog.dismiss();
        }

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage(message);
        builder.setPositiveButton(Utils.getString(getActivity(), R.string.myhkt_settings), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent();
                intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                Uri uri = Uri.fromParts("package", getActivity().getPackageName(), null);
                intent.setData(uri);
                getActivity().startActivity(intent);
            }
        });

        builder.setNegativeButton(Utils.getString(getActivity(), R.string.btn_cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                alertDialog = null;
            }
        });

        alertDialog = builder.create();
        alertDialog.show();
    }

    private Uri saveQRCode() {
        Uri uri = null;
        view.setDrawingCacheEnabled(true);
        view.buildDrawingCache();
        Bitmap bitmap = Bitmap.createBitmap(view.getDrawingCache());
        view.setDrawingCacheEnabled(false); // clear drawing cache
        Canvas canvas = new Canvas(bitmap);
        //Get the view's background
        Drawable bgDrawable = view.getBackground();
        if (bgDrawable != null) {
            //has background drawable, then draw it on the canvas
            bgDrawable.draw(canvas);
        } else {
            //does not have background drawable, then draw white background on the canvas
            canvas.drawColor(Color.WHITE);
        }
        // draw the view on the canvas
        view.draw(canvas);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            ContentValues values = new ContentValues();
            values.put(MediaStore.Images.Media.DISPLAY_NAME, "QR_" + System.currentTimeMillis() + ".jpg");
            values.put(MediaStore.Images.Media.MIME_TYPE, "image/jpeg");
            values.put(MediaStore.Images.Media.RELATIVE_PATH, Environment.DIRECTORY_PICTURES);
            uri = requireActivity().getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);

            try {
                Log.d("FPSDebug", " uri.getPath(): " + Utils.getRealPathImageFromUri(getActivity(), uri));

                OutputStream imageOutStream = getActivity().getContentResolver().openOutputStream(uri);
                bitmap.compress(Bitmap.CompressFormat.JPEG, 60, imageOutStream);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
        return uri;
    }

    public File savebitmap() {
        view.setDrawingCacheEnabled(true);
        view.buildDrawingCache();
        Bitmap b = Bitmap.createBitmap(view.getDrawingCache());
        view.setDrawingCacheEnabled(false); // clear drawing cache
        Canvas canvas = new Canvas(b);
        //Get the view's background
        Drawable bgDrawable = view.getBackground();
        if (bgDrawable != null) {
            //has background drawable, then draw it on the canvas
            bgDrawable.draw(canvas);
        } else {
            //does not have background drawable, then draw white background on the canvas
            canvas.drawColor(Color.WHITE);
        }
        // draw the view on the canvas
        view.draw(canvas);

        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        b.compress(Bitmap.CompressFormat.JPEG, 60, bytes);
        File f = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS)
                + File.separator + "QR_" + System.currentTimeMillis() + ".jpg");
        try {
            f.createNewFile();
            FileOutputStream fo = new FileOutputStream(f);
            fo.write(bytes.toByteArray());
            fo.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        scanGallery(getContext(), f.getPath());
        return f;
    }

    private String genSingleQrc(String fpsId, String billNo, String amount, String appId, String apiKey) {
        CommonQR qr = null;
        try {
            qr = CommonQrProvider.getInstance();

            qr.setPointOfInitationMethod("12");
            qr.setPostalCode(null);

            if (!TextUtils.isEmpty(fpsId)) {
                MerchantAccountInfoFPS fps = qr.getMerchantAccountInfoFPS();
                fps.setFpsIdentifier(fpsId);
                fps.setClearingCode(null);
            }
            AdditionalDataFieldTemplate additionalDataFieldTemplate = qr.getAdditionalDataFieldTemplate();
            additionalDataFieldTemplate.setBillNumber(billNo);

            if (!TextUtils.isEmpty(appId)) {
                MerchantAccountInfo merchantAccountInfo = qr.getMerchantAccountInfos().get(0);
                merchantAccountInfo.setAppId(appId);
                String querystr = String.format("appId=%s&payInitiator=%s", appId, "INPUTAMT");
                if (!TextUtils.isEmpty(amount)) {
                    amount = String.format("%.2f", Double.parseDouble(amount));
                    querystr = String.format("am=%s&appId=%s&cu=%s&payInitiator=%s", amount, appId, "HKD", "INPUTAMT");
                }
                String sign = getSignature(apiKey, querystr);
                String signature = sign.substring(sign.length() - 22, sign.length() - 2);
                merchantAccountInfo.addTlv("08", new TLV("08", signature));
            }
            if (!TextUtils.isEmpty(amount)) {
                qr.setTransactionAmount(amount);
            }

            // todo add field 26-06
        } catch (Exception e) {
            Log.e("FPSDebug", "Failed to generate Common QR String by mobile, error={}", e);
            return null;
        }

        return qr.generateTlvString();
    }

    private String getSignature(String apiKey, String querystr) {
        Log.d("FPSDebug", String.format("apiKey=%s;querystr=%s", apiKey, querystr));
        String signature;
        try {
            HmacSHAGenerator signer = new HmacSHAGenerator(apiKey);
            signature = signer.getSignature(querystr);

            Log.d("FPSDebug", "signature=" + signature);
            return signature;

        } catch (Exception e) {
            Log.d("FPSDebug", "Failed to Signature");
            return "";
        }
    }

    public void genKeys(int lob) {
        Boolean isPRD = ClnEnv.getPref(getContext(), getString(R.string.CONST_PREF_DOMAIN), APIsManager.PRD_domain).equals(APIsManager.PRD_domain);
        API_KEY_LTS = isPRD ? API_KEY_LTS_PRD : API_KEY_LTS_UAT;
        API_KEY_PCD = isPRD ? API_KEY_PCD_PRD : API_KEY_PCD_UAT;
        API_KEY_TV = isPRD ? API_KEY_TV_PRD : API_KEY_TV_UAT;
        API_KEY_MOB = isPRD ? API_KEY_MOB_PRD : API_KEY_MOB_UAT;
        API_KEY_101 = isPRD ? API_KEY_101_PRD : API_KEY_101_UAT;
        API_KEY_CSP = isPRD ? API_KEY_CSP_PRD : API_KEY_CSP_UAT;

        APP_ID_LTS = isPRD ? APP_ID_LTS_PRD : APP_ID_LTS_UAT;
        APP_ID_PCD = isPRD ? APP_ID_PCD_PRD : APP_ID_PCD_UAT;
        APP_ID_TV = isPRD ? APP_ID_TV_PRD : APP_ID_TV_UAT;
        APP_ID_MOB = isPRD ? APP_ID_MOB_PRD : APP_ID_MOB_UAT;
        APP_ID_101 = isPRD ? APP_ID_101_PRD : APP_ID_101_UAT;
        APP_ID_CSP = isPRD ? APP_ID_CSP_PRD : APP_ID_CSP_UAT;

        FPS_ID_LTS = isPRD ? FPS_ID_LTS_PRD : FPS_ID_LTS_UAT;
        FPS_ID_PCD = isPRD ? FPS_ID_PCD_PRD : FPS_ID_PCD_UAT;
        FPS_ID_TV = isPRD ? FPS_ID_TV_PRD : FPS_ID_TV_UAT;
        FPS_ID_MOB = isPRD ? FPS_ID_MOB_PRD : FPS_ID_MOB_UAT;
        FPS_ID_101 = isPRD ? FPS_ID_101_PRD : FPS_ID_101_UAT;
        FPS_ID_CSP = isPRD ? FPS_ID_CSP_PRD : FPS_ID_CSP_UAT;

        //App ID
        if (lob == R.string.CONST_LOB_MOB || lob == R.string.CONST_LOB_O2F) {
            mAppId = APP_ID_MOB;
            mApiKey = API_KEY_MOB;
            mFpsId = FPS_ID_MOB;
        } else if (lob == R.string.CONST_LOB_PCD) {
            mAppId = APP_ID_PCD;
            mApiKey = API_KEY_PCD;
            mFpsId = FPS_ID_PCD;
        } else if (lob == R.string.CONST_LOB_TV) {
            mAppId = APP_ID_TV;
            mApiKey = API_KEY_TV;
            mFpsId = FPS_ID_TV;
        } else if (lob == R.string.CONST_LOB_IOI || lob == R.string.CONST_LOB_1010) {
            mAppId = APP_ID_101;
            mApiKey = API_KEY_101;
            mFpsId = FPS_ID_101;
        } else if (lob == R.string.CONST_LOB_LTS) {
            mAppId = APP_ID_LTS;
            mApiKey = API_KEY_LTS;
            mFpsId = FPS_ID_LTS;
        } else if (lob == R.string.CONST_LOB_CSP) {
            mAppId = APP_ID_CSP;
            mApiKey = API_KEY_CSP;
            mFpsId = FPS_ID_CSP;
        }
    }

    private File saveBitMap(Context context, View drawView) {
        File pictureFileDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), "Logicchip");
        if (!pictureFileDir.exists()) {
            boolean isDirectoryCreated = pictureFileDir.mkdirs();
            if (!isDirectoryCreated)
                Log.i("TAG", "Can't create directory to save the image");
            return null;
        }
        String filename = pictureFileDir.getPath() + File.separator + System.currentTimeMillis() + ".jpg";

        view.setDrawingCacheEnabled(true);
        view.buildDrawingCache();

        File pictureFile = new File(filename);
        Bitmap bitmap = getBitmapFromView(drawView);
        view.setDrawingCacheEnabled(false); // clear drawing cache
        try {
            pictureFile.createNewFile();
            FileOutputStream oStream = new FileOutputStream(pictureFile);
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, oStream);
            oStream.flush();
            oStream.close();
        } catch (IOException e) {
            e.printStackTrace();
            Log.i("TAG", "There was an issue saving the image.");
        }
        scanGallery(context, pictureFile.getAbsolutePath());
        return pictureFile;
    }

    //create bitmap from view and returns it
    private Bitmap getBitmapFromView(View view) {
        //Define a bitmap with the same size as the view
        Bitmap returnedBitmap = Bitmap.createBitmap(view.getWidth(), view.getHeight(), Bitmap.Config.ARGB_8888);
        //Bind a canvas to it
        Canvas canvas = new Canvas(returnedBitmap);
        //Get the view's background
        Drawable bgDrawable = view.getBackground();
        if (bgDrawable != null) {
            //has background drawable, then draw it on the canvas
            bgDrawable.draw(canvas);
        } else {
            //does not have background drawable, then draw white background on the canvas
            canvas.drawColor(Color.WHITE);
        }
        // draw the view on the canvas
        view.draw(canvas);
        //return the bitmap
        return returnedBitmap;
    }


    // used for scanning gallery
    private void scanGallery(Context cntx, String path) {
        try {
            MediaScannerConnection.scanFile(cntx, new String[]{path}, null, new MediaScannerConnection.OnScanCompletedListener() {
                public void onScanCompleted(String path, Uri uri) {
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            Log.i("TAG", "There was an issue scanning gallery.");
        }
    }

    @Override
    public void onDestroy() {
        determineWindowColor(ClnEnv.getSessionPremierFlag(),
                getArguments().getBoolean(Constant.MY_MOBILE_IS_1010), getArguments().getString(Constant.MY_MOBILE_LOB_STRING), ClnEnv.isMyMobFlag());
        super.onDestroy();
    }

}
