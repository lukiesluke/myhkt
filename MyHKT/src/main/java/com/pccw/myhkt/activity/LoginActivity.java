package com.pccw.myhkt.activity;

import static com.pccw.myhkt.APIsManager.WALLET_EW_JWT;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.app.NotificationManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.graphics.Point;
import android.os.Build;
import android.os.Bundle;
import android.text.InputType;
import android.text.TextUtils;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.crashlytics.FirebaseCrashlytics;
import com.google.gson.Gson;
import com.pccw.dango.shared.cra.LgiCra;
import com.pccw.dango.shared.cra.WalletCra;
import com.pccw.dango.shared.entity.BillList;
import com.pccw.dango.shared.entity.CustRec;
import com.pccw.dango.shared.entity.SrvReq;
import com.pccw.dango.shared.entity.SubnRec;
import com.pccw.dango.shared.entity.SveeRec;
import com.pccw.dango.shared.tool.RC;
import com.pccw.myhkt.APIsManager;
import com.pccw.myhkt.APIsManager.OnAPIsListener;
import com.pccw.myhkt.APIsResponse;
import com.pccw.myhkt.ClnEnv;
import com.pccw.myhkt.DialogHelper;
import com.pccw.myhkt.FAWrapper;
import com.pccw.myhkt.R;
import com.pccw.myhkt.SaveAccountHelper;
import com.pccw.myhkt.Utils;
import com.pccw.myhkt.lib.ui.AAQuery;
import com.pccw.myhkt.lib.ui.HKTButton;
import com.pccw.myhkt.lib.ui.RegInputItem;
import com.pccw.myhkt.model.AcctAgent;
import com.pccw.myhkt.model.HomeButtonItem.MAINMENU;
import com.pccw.myhkt.mymob.activity.MyMobileActivity;
import com.pccw.myhkt.service.APIClient;
import com.pccw.myhkt.service.CheckAppointmentIntentService;
import com.pccw.myhkt.service.InboxMessageIntentService;
import com.pccw.myhkt.touchId.OnTouchIdActivityListener;
import com.pccw.myhkt.touchId.TouchIDClickListener;
import com.pccw.myhkt.util.Constant;
import com.pccw.wheat.shared.tool.Reply;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/************************************************************************
 * File : LoginActivity.java
 * Desc : Login Page
 * Name : LoginActivity
 * by 	: Andy Wong
 * Date : 16/11/2015
 *
 * Change History:
 * Date       Modified By		Description
 * ---------- ----------------	-------------------------------
 * 16/11/2015 Andy Wong 		-First draft
 * 03/11/2017 AMoiz Esmail      -Added Touch Id capability
 * 08/03/2018 AMoiz Esmail      -Redownload the app config every changemode
 *************************************************************************/

public class LoginActivity extends BaseActivity implements OnAPIsListener, OnTouchIdActivityListener {
    private boolean debug = false;
    private String TAG = this.getClass().getName();
    private AAQuery aq;
    private final int colMaxNum = 3;
    private int deviceWidth = 0;
    private int colWidth = 0;
    private int extralinespace = 0;
    private int buttonPadding = 0;
    private int greylineHeight = 0;
    private boolean isRememberPw = true;
    private String userid;
    //View
    private Activity me;
    private HKTButton loginBtn;
    private HKTButton regBtn;
    private HKTButton touchIdBtn;
    private TextView textViewRememberPw;
    private TextView textViewPFPromoMessage;
    private com.kyleduo.switchbutton.SwitchButton switchButton;
    private LinearLayout forgetPwLayout;
    private RelativeLayout rememberPwLayout;
    private RegInputItem regInputUserID;
    private RegInputItem regInputPw;

    private MAINMENU parentOnClickId = MAINMENU.NULL;
    private SaveAccountHelper saveAccountHelper;            // SQLite database for a new bill checking

    //MyMobile redirection
    private AcctAgent recallAcctAgent; //AcctAgent that came from MyMobActivity for Redirect
    private String recallActionTy; // asyncId that came from MyMobActivity for redirect
    private boolean recallIsSavePwd; // savepassword checkbox setting that came from MyMobActivity for redirect
    private boolean recallIsLogin; // redirect from service list login page

    private boolean isTouchIdLogin = false;
    private boolean isStart = false;

    @Override
    public final void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        debug = getResources().getBoolean(R.bool.DEBUG);
        isLiveChatShown = false;
        APIsManager.requestAppConfigJSON(this, this);

        initData();
        // reload Data
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            parentOnClickId = (MAINMENU) bundle.getSerializable("CLICKBUTTON");
            if (parentOnClickId == null) parentOnClickId = MAINMENU.NULL;

            recallAcctAgent = (AcctAgent) bundle.getSerializable("SELECTACCTAGENT");
            recallActionTy = bundle.getString("ACTIONTY");
            recallIsSavePwd = bundle.getBoolean("ISSAVEPWD");
            recallIsLogin = bundle.getBoolean("ISMYMOBLOGIN");
        }

        ClnEnv.updateUILocale(getBaseContext(), ClnEnv.getAppLocale(getBaseContext()));

        //init MyMob flag if come from MyMob
        ClnEnv.setMyMobFlag(false);
        setContentView(R.layout.activity_login);
    }

    private void initData() {
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        deviceWidth = size.x;
        extralinespace = (int) getResources().getDimension(R.dimen.extralinespace);
        buttonPadding = (int) getResources().getDimension(R.dimen.reg_logo_padding);
        greylineHeight = (int) getResources().getDimension(R.dimen.greyline_height);
        userid = "";
        me = this;
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        ClnEnv.updateUILocale(getBaseContext(), ClnEnv.getAppLocale(getBaseContext()));
    }

    @Override
    protected final void onStart() {
        if (!getResources().getBoolean(R.bool.UATPRDSWITCH)) {
            setSharedPrefForPRD();
        }
        super.onStart();
        //Screen Tracker
        FAWrapper.getInstance().sendFAScreen(me, R.string.CONST_SCRN_LOGIN, false);
    }

    @Override
    protected final void onResume() {
        super.onResume();
        // Update Locale
        moduleId = getResources().getString(R.string.MODULE_LGIF);
        ClnEnv.updateUILocale(getBaseContext(), ClnEnv.getAppLocale(getBaseContext()));
    }

    protected void initUI2() {
        aq = new AAQuery(this);

        //navbar
        aq.navBarButton(R.id.navbar_button_left, R.drawable.btn_back);
        aq.navBarTitle(R.id.navbar_title, getResources().getString(R.string.myhkt_login_title));
        aq.id(R.id.navbar_button_left).clicked(this, "onClick");

        //UAT switch
        if (Utils.isUAT(me)) {
            aq.id(R.id.login_uat_switch_txt).visibility(View.VISIBLE);
        } else {
            aq.id(R.id.login_uat_switch_txt).visibility(View.INVISIBLE);
        }

        aq.id(R.id.login_container).getView().setPadding(buttonPadding, 0, buttonPadding, buttonPadding * 2);

        aq.normText(R.id.login_intro, getResString(R.string.myhkt_login_registerintro));
        aq.id(R.id.login_intro).textColorId(R.color.hkt_reg_grey);

        aq.id(R.id.login_intro).height(LinearLayout.LayoutParams.WRAP_CONTENT, false).getTextView().setPadding(0, buttonPadding, 0, buttonPadding);

        String password = "";
        if (ClnEnv.getPref(getApplicationContext(), getString(R.string.CONST_PREF_SAVELOGINID), false) || ClnEnv.getPref(getApplicationContext(), getString(R.string.CONST_PREF_SAVEPASSWORD), false)) {
            userid = ClnEnv.getPref(getApplicationContext(), getString(R.string.CONST_PREF_LOGINID), "");
        }
        //FRS: 3.10.1-Set the remember password to checked on fresh installed
        if (!TextUtils.isEmpty(ClnEnv.getSessionPassword()) ||
                !TextUtils.isEmpty(ClnEnv.getPref(getApplicationContext(), getString(R.string.CONST_PREF_LOGINID), "")) ||
                !TextUtils.isEmpty(ClnEnv.getPref(getApplicationContext(), Constant.TOUCH_ID_TEMP_USER_ID, "")) ||
                !ClnEnv.getPref(getApplicationContext(), getString(R.string.CONST_PREF_SAVEPASSWORD), true)) {

            if (ClnEnv.getPref(getApplicationContext(), getString(R.string.CONST_PREF_SAVEPASSWORD), false)) {
                password = ClnEnv.getEncPref(getApplicationContext(), ClnEnv.getPref(getApplicationContext(), getString(R.string.CONST_PREF_LOGINID), ""), getString(R.string.CONST_PREF_PASSWORD), "");
                isRememberPw = true;
            } else {
                isRememberPw = false;
            }

        } else {
            isRememberPw = true;
        }

        regInputUserID = (RegInputItem) aq.id(R.id.login_input_userid).getView();
        regInputUserID.initViews(this, getResString(R.string.myhkt_login_userid_txt), getResources().getString(R.string.myhkt_login_userid_hint), userid, InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);
        regInputUserID.setPadding(0, 0, 0, buttonPadding);

        regInputPw = (RegInputItem) aq.id(R.id.login_input_pw).getView();
        regInputPw.initViews(this, getResources().getString(R.string.myhkt_login_pw_txt), "", password, InputType.TYPE_TEXT_VARIATION_PASSWORD);
        regInputPw.setPadding(0, 0, 0, buttonPadding);
        regInputPw.setMaxLength(16);

        forgetPwLayout = (LinearLayout) aq.id(R.id.login_forget_pw_layout).getView();
        forgetPwLayout.setOrientation(LinearLayout.HORIZONTAL);
        forgetPwLayout.setGravity(Gravity.RIGHT | Gravity.CENTER_VERTICAL);
        aq.id(R.id.login_forget_pw_layout).clicked(this, "onClick");

        aq.normTextGrey(R.id.login_forget_pw_txt, getResString(R.string.myhkt_LGIF_FORGOT));
        aq.id(R.id.login_forget_pw_txt).height(LinearLayout.LayoutParams.WRAP_CONTENT, false).getTextView().setPadding(0, buttonPadding, buttonPadding / 3, buttonPadding);
        aq.id(R.id.login_forget_pw_txt).textColorId(R.color.hkt_buttonblue);

        aq.id(R.id.login_forget_pw_iv).image(R.drawable.btn_details);

        aq.line(R.id.login_line1, R.id.login_line2);
        aq.padding(R.id.login_line1, 0, 0, 0, 0);
        aq.padding(R.id.login_line2, 0, 0, 0, 0);

        rememberPwLayout = (RelativeLayout) aq.id(R.id.login_remember_pw_layout).getView();
        rememberPwLayout.setGravity(Gravity.RIGHT | Gravity.CENTER_VERTICAL);

        textViewRememberPw = aq.id(R.id.login_remember_pw_txt).getTextView();
        aq.normTextGrey(R.id.login_remember_pw_txt, getResString(R.string.myhkt_login_remember_pw_txt));
        aq.id(R.id.login_remember_pw_txt).height(LinearLayout.LayoutParams.WRAP_CONTENT, false);
        textViewRememberPw.setPadding(0, buttonPadding, extralinespace, buttonPadding);
        textViewRememberPw.setGravity(Gravity.CENTER_VERTICAL);

        switchButton = aq.switchButton(R.id.login_remember_pw_sb);
        switchButton.setChecked(isRememberPw);
        switchButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                ClnEnv.setPref(getApplicationContext(), getString(R.string.CONST_PREF_SAVEPASSWORD), isChecked);
                isRememberPw = isChecked;
            }
        });

        //init Btns
        loginBtn = (HKTButton) aq.id(R.id.login_login_btn).getView();
        loginBtn.initViews(this, getResources().getString(R.string.myhkt_login_login_txt), LayoutParams.MATCH_PARENT);
        aq.marginpx(R.id.login_login_btn, 0, buttonPadding * 2, 0, buttonPadding);
        aq.id(R.id.login_login_btn).clicked(this, "onClick");

        regBtn = (HKTButton) aq.id(R.id.login_reg_btn).getView();
        regBtn.initViews(this, HKTButton.TYPE_WHITE_BLUE, getResources().getString(R.string.myhkt_login_reg_txt), LayoutParams.MATCH_PARENT);
        aq.id(R.id.login_reg_btn).clicked(this, "onClick");

        //Touch ID button
        touchIdBtn = (HKTButton) aq.id(R.id.login_touch_id_btn).getView();
        touchIdBtn.initViews(this, getResources().getString(R.string.fp_login_button), LayoutParams.MATCH_PARENT);
        aq.marginpx(R.id.login_touch_id_btn, 0, 0, 0, buttonPadding);

        textViewPFPromoMessage = aq.id(R.id.login_pf_promo_message_txt).getTextView();
        aq.normTextGrey(R.id.login_pf_promo_message_txt, getString(R.string.login_bottom_des));
        aq.id(R.id.login_pf_promo_message_txt).height(LinearLayout.LayoutParams.WRAP_CONTENT, false).getTextView().setPadding(0, buttonPadding, 0, buttonPadding);
        aq.id(R.id.login_pf_promo_message_txt).textColorId(R.color.login_bottom_text_color);


        if (Build.VERSION.SDK_INT >= 23 && Utils.isTouchIDAvailableOnSystem(this) && Utils.isTouchIdLoginEnabled(this)) {
            touchIdBtn.setVisibility(View.VISIBLE);

            switchButton.setChecked(false);
            switchButton.setEnabled(false);

            ClnEnv.setPref(this, Constant.TOUCH_ID_FAILURE_COUNT, 0); //Clear failure count

            //Show the username saved during the Touch ID activation
            regInputUserID.initViews(this, getResString(R.string.myhkt_login_userid_txt),
                    getResources().getString(R.string.myhkt_login_userid_hint),
                    ClnEnv.getPref(this, Constant.TOUCH_ID_USER_ID_DEFAULT, ""));

            touchIdBtn.setOnClickListener(new TouchIDClickListener(this));

            if (Utils.isTouchIDEnrolledByUser(this)) {
                isStart = true;
                touchIdBtn.performClick();
            }

        } else {
            touchIdBtn.setVisibility(View.GONE);
        }
    }

    // Android Device Back Button Handling
    public final void onBackPressed() {
        //			clearNotificationData();
        Intent intent = new Intent(me.getApplicationContext(), MainMenuActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        me.startActivity(intent);
        overridePendingTransition(R.anim.left_slide_in, R.anim.right_slide_out);
        finish();
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.navbar_button_left:
                onBackPressed();
                break;
            case R.id.login_login_btn:
                //UAT PRD switch for UAT only, DO NOT USE AT POST RELEASE VERSION
                if (getResources().getBoolean(R.bool.UATPRDSWITCH) && "changemode".equals(regInputUserID.getInput())) {
                    ClnEnv.setAppConfigDownloaded(false);
                    DialogInterface.OnClickListener onPositiveClickListener = new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            setSharedPrefForUAT();
                            APIClient.resetAPIClientMyHKT();
                            aq.id(R.id.login_uat_switch_txt).visibility(View.VISIBLE);
                        }
                    };
                    DialogInterface.OnClickListener onNegativeClickListener = new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            setSharedPrefForPRD();
                            APIClient.resetAPIClientMyHKT();
                            aq.id(R.id.login_uat_switch_txt).visibility(View.INVISIBLE);
                        }
                    };
                    DialogHelper.createSimpleDialog(me, "Select host", "External UAT", onPositiveClickListener, "PRD (Pilot)", onNegativeClickListener);
                    break;
                }

                if ("".equals(regInputUserID.getInput())) {
                    displayDialog(this, Utils.getString(this, R.string.LGIM_IVLOGIN_ID));
                } else if ("".equals(regInputPw.getInput())) {
                    displayDialog(this, Utils.getString(this, R.string.LGIM_LOGIN_FAIL));
                } else {
                    doLogin();
                    // Enable Live Chat Disclaimer
                    Utils.setLiveChatDisclaimerFlag(true);
                }
                break;
            case R.id.login_reg_btn:
                Intent intent;
                intent = new Intent(me.getApplicationContext(), RegBasicActivity.class);
                startActivity(intent);
                overridePendingTransition(R.anim.right_slide_in, R.anim.left_slide_out);
                break;
            case R.id.login_forget_pw_layout:
                Intent intent_forgetPassword;
                intent_forgetPassword = new Intent(me.getApplicationContext(), ForgetPasswordActivity.class);
                startActivity(intent_forgetPassword);
                overridePendingTransition(R.anim.right_slide_in, R.anim.left_slide_out);
                break;
        }
    }

    private void doLogin() {
        SveeRec sveeRec = new SveeRec();
        sveeRec.loginId = regInputUserID.getInput();
        sveeRec.pwd = regInputPw.getInput();
        LgiCra lgiCra = new LgiCra();
        lgiCra.setIFingerpr(false);
        lgiCra.setISveeRec(sveeRec);
        isTouchIdLogin = false;
        APIsManager.doLoginTest(this, lgiCra, isRememberPw);
    }

    @Override
    protected final void onPause() {
        super.onPause();
        debugLog(TAG, "onPause...");
        userid = regInputUserID.getInput();
    }

    @Override
    protected final void onStop() {
        super.onStop();
    }

    public void onSuccess(APIsResponse response) {
        super.onSuccess(response);
        if (response != null) {
            if (APIsManager.HELO.equals(response.getActionTy())) {
                debugLog(TAG, "doHelo complete!!");
            } else if (APIsManager.LGI.equals(response.getActionTy())) {
                LgiCra lgiCra = (LgiCra) response.getCra();
                Gson gson = new Gson();
                String result = gson.toJson(lgiCra);
                FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
                crashlytics.setCrashlyticsCollectionEnabled(true);
                crashlytics.setUserId(lgiCra.getOQualSvee().getSveeRec().loginId);
                FirebaseAnalytics.getInstance(this).setUserId(lgiCra.getOQualSvee().getSveeRec().loginId);
                if (debug) Utils.showLog(TAG, result);

                if (lgiCra.getOQualSvee() != null && lgiCra.getOQualSvee().getCustRec() != null &&
                        !lgiCra.getOQualSvee().getCustRec().phylum.equals(CustRec.PH_CSUM)) {
                    displayDialog(this, getString(R.string.RC_NOT_SUPP_PHYLUM));        //Show error message if it's not consumer account
                } else {
                    NotificationManager nMgr = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                    nMgr.cancelAll();

                    //Get Inbox List using intent service
                    Intent intent = new Intent(me, InboxMessageIntentService.class);
                    startService(intent);

                    String lastTsSaved = ClnEnv.getPref(getApplicationContext(), getString(R.string.CONST_PREF_APPTIND_LAST_TS).concat("-").concat(regInputUserID.getInput()), "");

                    if (lastTsSaved.isEmpty() || Utils.isCurrentDay(this, lgiCra.getServerTS(), lastTsSaved, getString(R.string.input_datetime_format))) {
                        ClnEnv.setIsQueryApptServiceRunning(false);
                        Intent chkappt = new Intent(me, CheckAppointmentIntentService.class);
                        chkappt.putExtra("CONST_PREF_APPTIND_DAYS", ClnEnv.getPref(this, getString(R.string.CONST_PREF_APPTIND_DAYS), 3));
                        startService(chkappt);
                    }

                    ClnEnv.setLgiCra(lgiCra);
                    // Keeping the username and password in memory
                    ClnEnv.setSessionLoginID(regInputUserID.getInput());
                    ClnEnv.setSessionPassword(regInputPw.getInput());
                    ClnEnv.setSessionSavePw(isRememberPw);
                    // Save Account Type
                    ClnEnv.setPref(this, getString(R.string.CONST_PREF_PREMIER_FLAG), ClnEnv.getQualSvee().getCustRec().isPremier());
                    ClnEnv.setSessionPremierFlag(ClnEnv.getQualSvee().getCustRec().isPremier());

                    ClnEnv.setPref(this, getString(R.string.CONST_PREF_SAVELOGINID), true);
                    ClnEnv.setPref(this, getString(R.string.CONST_PREF_LOGINID), regInputUserID.getInput());
                    ClnEnv.setPref(this, getString(R.string.CONST_PREF_LOGINID), regInputUserID.getInput());
                    ClnEnv.setPref(getApplicationContext(), Constant.CONST_SHOW_SESS_TIMEOUT, false);

                    if (isRememberPw) {
                        // Saving password implies saving loginid
                        ClnEnv.setPref(me.getApplicationContext(), me.getString(R.string.CONST_PREF_SAVEPASSWORD), true);
                        ClnEnv.setEncPref(me.getApplicationContext(), regInputUserID.getInput(), me.getString(R.string.CONST_PREF_PASSWORD), regInputPw.getInput());
                    } else {
                        // Not saving password - remove the saved password
                        ClnEnv.setPref(me.getApplicationContext(), me.getString(R.string.CONST_PREF_SAVEPASSWORD), false);
                        ClnEnv.setEncPref(me.getApplicationContext(), "", me.getString(R.string.CONST_PREF_PASSWORD), "");
                    }

                    // New Bill Indicator Process
                    saveAccountHelper = SaveAccountHelper.getInstance(me);
                    // Removed all records if last bill date is one year or earlier than current Date
                    saveAccountHelper.RemoveRecordForInitial(lgiCra.getServerTS());
                    // make sure the BillAry Not null and empty Array
                    if (lgiCra.getOBillListAry() != null) {
                        for (BillList binqcra : lgiCra.getOBillListAry()) {
                            if (binqcra.getOBillAry() != null && binqcra.getOBillAry().length > 0) {
                                try {
                                    // Check Database only if the bill date returned
                                    if (!"".equalsIgnoreCase(binqcra.getOBillAry()[0].getInvDate())) {
                                        // Get lastBilldate from database
                                        String lastBillDate = saveAccountHelper.getDateByLoginIDAndAcctNum(ClnEnv.getSessionLoginID(), binqcra.getIAcct().getAcctNum());
                                        if (lastBillDate == null) {
                                            saveAccountHelper.insert(ClnEnv.getSessionLoginID(), binqcra.getIAcct().getAcctNum());
                                        } else if (CompareLastBillDate(lastBillDate, binqcra.getOBillAry()[0].getInvDate())) {
                                            saveAccountHelper.updateFlag(ClnEnv.getSessionLoginID(), binqcra.getIAcct().getAcctNum());
                                        }
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                    }

                    /*
                    comment out revise checking of appt
                     */

//                    // Appointment Indicator Process
//                    // Appt Alert Icon
//                    ClnEnv.setPref(me.getApplicationContext(), me.getString(R.string.CONST_PREF_APPTIND_FLAG), false);
//                    if (lgiCra.getOGnrlApptAry() != null) {
//                        for (int i = 0; i < lgiCra.getOGnrlApptAry().length; i++) {
//                            GnrlAppt gnrlAppt = lgiCra.getOGnrlApptAry()[i];
//                            if (Utils.CompareDateAdd3(me, gnrlAppt.getApptStDT(), lgiCra.getServerTS(), getString(R.string.input_datetime_format))) {
//                                // Have an appointment
//                                ClnEnv.setPref(me.getApplicationContext(), me.getString(R.string.CONST_PREF_APPTIND_FLAG), true);
//                                break;
//                            }
//                        }
//                    }
//
//                    if (lgiCra.getOSrvReqAry() != null) {
//                        for (int i = 0; i < lgiCra.getOSrvReqAry().length; i++) {
//                            SrvReq srvReq = lgiCra.getOSrvReqAry()[i];
//                            if (Utils.CompareDateAdd3(me, srvReq.getApptTS().getApptDate(), lgiCra.getServerTS(), getString(R.string.input_date_format))) {
//                                // Have an appointment
//                                ClnEnv.setPref(me.getApplicationContext(), me.getString(R.string.CONST_PREF_APPTIND_FLAG), true);
//                                break;
//                            }
//                        }
//                    }

                    // SR Array
                    if (lgiCra.getOSrvReqAry() != null) {
                        for (int i = 0; i < lgiCra.getOSrvReqAry().length; i++) {
                            SrvReq srvReq = lgiCra.getOSrvReqAry()[i];
                        }
                    }

                    //Clear Touch ID failures and trials
                    ClnEnv.setPref(this, Constant.TOUCH_ID_FAILURE_COUNT, 0);

                    //Check Touch ID is available in the device and if login through inputs
                    if (Utils.isToShowTouchID(this) && !TextUtils.isEmpty(regInputUserID.getInput())
                            && !TextUtils.isEmpty(regInputPw.getInput())) {
                        Utils.saveTempUserCredentials(getApplicationContext(), regInputUserID.getInput(), regInputPw.getInput());

                        //If the password is changed, then touch id password should be updated. Logged in user should be equal to Touch Id user before updating
                        if (Utils.isTouchIDLoginActivated(getApplicationContext()) && Utils.isTouchIdUserEqualsToLoggedInUser(getApplicationContext())) {
                            Utils.saveTempUserCredentialsAsTouchIdDefault(getApplicationContext());
                        }

                        //Clear Touch ID failures and retries
                        ClnEnv.setPref(this, Constant.TOUCH_ID_FAILURE_COUNT, 0);
                    }

                    //Check if first login success
//                    if (Utils.isToShowTouchIdWelcomePage(this)) {
//                        Intent welcomePage = new Intent(this, TouchIdWelcomeActivity.class);
//                        welcomePage.putExtra(Constant.TOUCH_ID_LOGIN_WELCOME_PARENT, true);
//                        startActivityForResult(welcomePage, Constant.REQUEST_TOUCH_ID_WELCOME_PAGE);
//                        overridePendingTransition(R.anim.right_slide_in, R.anim.left_slide_out);
//                    } else {
                    RedirectTargetScreen();
//                    }

                    int rl = ClnEnv.getQualSvee().getSubnRecAry().length;
                    int rx, rlz;
                    //live accounts
                    for (rx = 0; rx < rl; rx++) {
                        SubnRec subnRec = ClnEnv.getQualSvee().getSubnRecAry()[rx];
                        if (debug) Log.i("SubnRec", subnRec.lob);
                    }

                    //zombie accounts
                    rlz = ClnEnv.getQualSvee().getZmSubnRecAry().length;
                    for (rx = 0; rx < rlz; rx++) {
                        SubnRec subnRec = ClnEnv.getQualSvee().getZmSubnRecAry()[rx];
                        if (debug) Log.i("XSubnRec", subnRec.lob);
                    }
                }

            } else if (APIsManager.WALLET_EW_JWT.equals(response.getActionTy())) {
                String url = isZh ? Constant.URL_E_WALLET_CN_UAT : Constant.URL_E_WALLET_EN_UAT;
                WalletCra wallet = (WalletCra) response.getCra();

                Intent intent = new Intent(me.getApplicationContext(), BrowserActivity.class);
                url = url.replace(Constant.WALLET_TIMESTAMP, Utils.getTimeStamp());

                intent.putExtra(BrowserActivity.INTENTLINK, url);
                intent.putExtra(BrowserActivity.INTENTTITLE, Utils.getString(this, R.string.MYHKT_HOME_BTN_WALLET_WEB));
                intent.putExtra(BrowserActivity.AUTHORIZATION, wallet.getoJWT());
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                me.startActivity(intent);
                me.overridePendingTransition(R.anim.left_slide_in, R.anim.right_slide_out);
                finish();
            }
        }
    }

    public void onFail(APIsResponse response) {
        super.onFail(response);
        Intent intent;
        if (response != null) {
            if (APIsManager.LGI.equals(response.getActionTy())) {
                try {
                    LgiCra lgiCra = (LgiCra) response.getCra();
                    if (lgiCra.getReply().getCode().equals(RC.FILL_SVEE)
                            || lgiCra.getReply().getCode().equals(RC.FILL_SVEE4CLBL)
                            || lgiCra.getReply().getCode().equals(RC.FILL_SVEE4CLAV)
                            || lgiCra.getReply().getCode().equals(RC.FILL_SVEE4CLUB)
                            || lgiCra.getReply().getCode().equals(RC.FILL_SVEE_NOTNC)
                            || lgiCra.getReply().getCode().equals(RC.FILL_SVEE4CLBL_NOTNC)) {
                        // RC_FILL_SVEE
                        // Keeping the username and password in memory
                        theClubRedirectDialog(lgiCra, regInputUserID.getInput(), regInputPw.getInput(), isRememberPw, lgiCra.getReply().getCode());
                    } else if (lgiCra.getReply().getCode().equals(RC.ACTV_SVEE)) {
                        DialogHelper.createRedirectActivationDialog(this, regInputUserID.getInput(), regInputPw.getInput());
                    } else if (lgiCra.getReply().getCode().equals(RC.RESET_SVEE) || lgiCra.getReply().getCode().equals(RC.INITPWD_SVEE)) {
                        // RC_RESET_RCUS
                        Bundle rbundle = new Bundle();
                        rbundle.putString("RESETPWD", regInputPw.getInput());
                        rbundle.putBoolean("SAVEPWD", isRememberPw);
                        rbundle.putSerializable("SVEEREC", lgiCra.getOQualSvee().getSveeRec());
                        rbundle.putString("ERRCODE", lgiCra.getReply().getCode());
                        rbundle.putSerializable("LOGINCRA", lgiCra);
                        intent = new Intent(me.getApplicationContext(), ResetPwdActivity.class);
                        intent.putExtras(rbundle);
                        me.startActivity(intent);
                        me.overridePendingTransition(R.anim.right_slide_in, R.anim.left_slide_out);
                    } else if (lgiCra.getReply().getCode().equals(RC.LOGIN_FAIL) && isTouchIdLogin) {
                        displayDialog(this, getString(R.string.fp_login_err_invalid));
                    } else {
                        // General Error Message
                        if (!"".equals(response.getMessage()) && response.getMessage() != null) {
                            displayDialog(this, response.getMessage());
                        } else if (RC.TCSESS_MM.equalsIgnoreCase(response.getReply().getCode())) {
                            BaseActivity.ivSessDialog();
                        } else {
                            displayDialog(this, ClnEnv.getRPCErrMsg(this, response.getReply().getCode()));
                        }
                    }
                } catch (Exception e) { // to catch class cast exception when login receive 101.
                    // General Error Message
                    if (!"".equals(response.getMessage()) && response.getMessage() != null) {
                        displayDialog(this, response.getMessage());
                    } else if (RC.TCSESS_MM.equalsIgnoreCase(response.getReply().getCode())) {
                        BaseActivity.ivSessDialog();
                    } else {
                        displayDialog(this, ClnEnv.getRPCErrMsg(this, response.getReply().getCode()));
                    }
                }
            } else if (APIsManager.WALLET_EW_JWT.equals(response.getActionTy())) {
                MainMenuActivity.onErrorMessageRequestToken(response.getMessage());
                finish();
            } else {
                // General Error Message
                if (!"".equals(response.getMessage()) && response.getMessage() != null) {
                    displayDialog(this, response.getMessage());
                } else if (RC.TCSESS_MM.equalsIgnoreCase(response.getReply().getCode())) {
                    ivSessDialog();
                } else {
                    displayDialog(this, ClnEnv.getRPCErrMsg(this, response.getReply().getCode()));
                }
            }
        }
    }

    private boolean CompareLastBillDate(String lastBilldate, String newlastBilldate) {
        if (lastBilldate != null && lastBilldate.trim().isEmpty()) {
            return false;
        }
        SimpleDateFormat inputdf = new SimpleDateFormat("yyyyMMdd", Locale.US);
        Date lastBillDate, newlastBillDate;
        try {
            lastBillDate = inputdf.parse(lastBilldate);
            newlastBillDate = inputdf.parse(newlastBilldate);

            assert lastBillDate != null;
            if (lastBillDate.compareTo(newlastBillDate) < 0) {
                return true;
            }
        } catch (Exception e) {
            // fail to convert date
            e.printStackTrace();
        }
        return false;
    }

    // Redirect to Activation
    protected final void redirectDialog(String message, final LgiCra logincra) {
        AlertDialog.Builder builder = new Builder(LoginActivity.this);
        builder.setMessage(message);
        builder.setPositiveButton(Utils.getString(me, R.string.btn_ok), new OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                Intent intent = new Intent(getApplicationContext(), ActivationActivity.class);
                // Loading required information to Activation
                Bundle bundle = new Bundle();
                bundle.putString("LOGINID", regInputUserID.getInput());
                bundle.putString("PWD", regInputPw.getInput());
                intent.putExtras(bundle);
                startActivity(intent);
                overridePendingTransition(R.anim.right_slide_in, R.anim.left_slide_out);
                finish();
            }
        });
        builder.setOnCancelListener(new Dialog.OnCancelListener() {
            public void onCancel(DialogInterface dialog) {
                dialog.dismiss();
                Intent intent = new Intent(getApplicationContext(), ActivationActivity.class);
                // Loading required information to Activation
                Bundle bundle = new Bundle();
                bundle.putString("LOGINID", regInputUserID.getInput());
                bundle.putString("PWD", regInputPw.getInput());
                intent.putExtras(bundle);
                startActivity(intent);
                overridePendingTransition(R.anim.right_slide_in, R.anim.left_slide_out);
                finish();
            }
        });
        builder.create().show();
    }

    private final void RedirectTargetScreen() {
        Intent intent;
        Bundle bundle;
        // forward to the required screen
        switch (parentOnClickId) {
            case MYMOB:
                if (debug) Log.i("REDIRECTTARGETSCREEN", "REDIRECTTARGETSCREEN");
                intent = new Intent(me.getApplicationContext(), MyMobileActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                if (recallAcctAgent != null) {
                    bundle = new Bundle();
                    bundle.putSerializable("SELECTACCTAGENT", recallAcctAgent);
                    bundle.putString("ACTIONTY", recallActionTy);
                    bundle.putBoolean("ISSAVEPWD", recallIsSavePwd);
                    bundle.putBoolean("ISMYMOBLOGIN", recallIsLogin);
                    intent.putExtras(bundle);
                }
                me.startActivity(intent);
                me.overridePendingTransition(R.anim.right_slide_in, R.anim.left_slide_out);
                finish();
                break;
            case MYACCT:
                intent = new Intent(me.getApplicationContext(), ServiceListActivity.class);
                me.startActivity(intent);
                me.overridePendingTransition(R.anim.right_slide_in, R.anim.left_slide_out);
                finish();
                break;
            case MYLINETEST:
                intent = new Intent(me.getApplicationContext(), ServiceListActivity.class);
                Utils.ISMYLIENTEST = true;
                me.startActivity(intent);
                me.overridePendingTransition(R.anim.right_slide_in, R.anim.left_slide_out);
                finish();
                break;
            case MYAPPT:
                intent = new Intent(me.getApplicationContext(), MyApptActivity.class);
                me.startActivity(intent);
                me.overridePendingTransition(R.anim.right_slide_in, R.anim.left_slide_out);
                finish();
                break;
            case MYPROFILE:
                intent = new Intent(me.getApplicationContext(), MyProfileActivity.class);
                me.startActivity(intent);
                me.overridePendingTransition(R.anim.right_slide_in, R.anim.left_slide_out);
                finish();
                break;
            case DIRECTINQ:
                break;
            case IDD:
                intent = new Intent(me.getApplicationContext(), IDDActivity.class);
                bundle = new Bundle();
                bundle.putBoolean(IDDActivity.BUNDLEIS0060, true);
                intent.putExtras(bundle);
                me.startActivity(intent);
                me.overridePendingTransition(R.anim.right_slide_in, R.anim.left_slide_out);
                finish();
                break;
            case CONTACTUS:
                intent = new Intent(me.getApplicationContext(), ContactUsNewActivity.class);
                bundle = new Bundle();
                intent.putExtras(bundle);
                me.startActivity(intent);
                me.overridePendingTransition(R.anim.right_slide_in, R.anim.left_slide_out);
                finish();
                break;
            case SETTING:
                intent = new Intent(me.getApplicationContext(), SettingsActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                me.startActivity(intent);
                me.overridePendingTransition(R.anim.left_slide_in, R.anim.right_slide_out);
                finish();
                break;
            case MYWALLET:
                String iLogID = ClnEnv.getPref(getApplicationContext(), getString(R.string.CONST_PREF_LOGINID), "");

                WalletCra wallet = new WalletCra();
                wallet.setIApi(WALLET_EW_JWT);
                wallet.setIGwtGud(ClnEnv.getSessTok());
                wallet.setiLoginId(iLogID);
                wallet.setoJWT("");
                Reply rReply = new Reply();
                rReply.setCode(null);
                wallet.setReply(rReply);
                wallet.setServerTS(null);

                APIsManager.walletRequest(this, wallet);
                return;
            default:
                intent = new Intent(me.getApplicationContext(), MainMenuActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                me.startActivity(intent);
                me.overridePendingTransition(R.anim.left_slide_in, R.anim.right_slide_out);
                finish();
                break;
        }
        me.finish();
    }

    protected final void theClubRedirectDialog(LgiCra lgicra, String loginId, String pwd,
                                               boolean savepw, String errcode) {
        Intent intent;
        Bundle bundle;
        intent = new Intent(getApplicationContext(), RegAccInfoActivity.class);
        bundle = new Bundle();
        bundle.putString("LOGINID", loginId);
        bundle.putString("PWD", pwd);
        bundle.putBoolean("SAVEPW", savepw);
        bundle.putString("ERRCODE", errcode);
        bundle.putSerializable("LOGINCRA", lgicra);
        intent.putExtras(bundle);
        startActivity(intent);
        overridePendingTransition(R.anim.right_slide_in, R.anim.left_slide_out);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);

        if (requestCode == Constant.REQUEST_TOUCH_ID_WELCOME_PAGE) {
            RedirectTargetScreen();
        } else if (requestCode == Constant.REQUEST_TOUCH_ID_VERIFICATION_FAILED &&
                resultCode == Activity.RESULT_OK) {
            if (intent.getBooleanExtra(Constant.IS_TO_SHOW_LIVE_CHAT, false)) {
                //Open live chat with module LGIF_PA_LGIF
                openLiveChat(getResources().getString(R.string.MODULE_LGIF));
            } else {
                touchIdBtn.performClick();
            }
        }
    }

    @Override
    public void onTouchIdSuccess() {
        SveeRec sveeRec = new SveeRec();
        sveeRec.loginId = ClnEnv.getPref(this, Constant.TOUCH_ID_USER_ID_DEFAULT, "");
        sveeRec.pwd = ClnEnv.getEncPref(this, ClnEnv.getPref(this, Constant.TOUCH_ID_USER_ID_DEFAULT, ""),
                Constant.TOUCH_ID_PWD_DEFAULT, "");
        LgiCra lgiCra = new LgiCra();
        lgiCra.setIFingerpr(true);
        lgiCra.setISveeRec(sveeRec);

        isTouchIdLogin = true;

        if (!Utils.isTouchIdUserEqualsToLoggedInUser(getApplicationContext())) {
            Utils.saveDefaultTouchIDUserToTempUser(getApplicationContext());
        }

        Log.d("touchDebug", "isRememberPw:" + isRememberPw);
        APIsManager.doLoginTest(this, lgiCra, true);
    }

    @Override
    public void onTouchIdFailed(int failCount) {
        isStart = false;
        if (ClnEnv.getPref(getApplicationContext(), Constant.TOUCH_ID_FAILURE_COUNT, 0) == 3) {
            showRetryActivity();
        }
    }

    @Override
    public void onEnterPassword() {
        //Do Nothing
        isStart = false;
    }

    @Override
    public void onFingerprintLockout(String error) {
        //Show lock mode
        if (!isStart) {
            Intent intent = new Intent(this, TouchIdVerificationFailedActivity.class);
            intent.putExtra(Constant.TOUCH_ID_RETRY_COUNT, true);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivityForResult(intent, Constant.REQUEST_TOUCH_ID_VERIFICATION_FAILED);
            overridePendingTransition(R.anim.right_slide_in, R.anim.left_slide_out);
        }
        isStart = false;
    }

    @Override
    public void onFingerprintError(int messId) {
        // Do nothing
        // Dialog is dismissed
    }

    public void showRetryActivity() {
        Intent intent = new Intent(this, TouchIdVerificationFailedActivity.class);
        startActivityForResult(intent, Constant.REQUEST_TOUCH_ID_VERIFICATION_FAILED);
        overridePendingTransition(R.anim.right_slide_in, R.anim.left_slide_out);
    }

    private void setSharedPrefForPRD() {
        ClnEnv.setPref(me, getString(R.string.CONST_PREF_DOMAIN), APIsManager.PRD_domain);
        ClnEnv.setPref(me, getString(R.string.CONST_PREF_SHOPHOST), APIsManager.PRD_shop_host);
        ClnEnv.setPref(me, getString(R.string.CONST_PREF_SALT), APIsManager.PRD_salt);
        ClnEnv.setPref(me, getString(R.string.CONST_PREF_FTP), APIsManager.PRD_FTP);
    }

    private void setSharedPrefForUAT() {
        ClnEnv.setPref(me, getString(R.string.CONST_PREF_DOMAIN), APIsManager.UAT_domain);
        ClnEnv.setPref(me, getString(R.string.CONST_PREF_SHOPHOST), APIsManager.UAT_shop_host);
        ClnEnv.setPref(me, getString(R.string.CONST_PREF_SALT), APIsManager.UAT_salt);
        ClnEnv.setPref(me, getString(R.string.CONST_PREF_FTP), APIsManager.UAT_FTP);
    }

}