package com.pccw.myhkt;

import static com.pccw.myhkt.util.Constant.ACTION_BROADCAST_CONFIG_FINISH_DOWNLOAD;
import static com.pccw.myhkt.util.Constant.ACTION_BROADCAST_IMAGE_FINISH_DOWNLOAD;
import static com.pccw.myhkt.util.Constant.FILENAME_APP_CONFIG;
import static com.pccw.myhkt.util.Constant.FORMAT_MODIFIED_DATE;
import static com.pccw.myhkt.util.Constant.KEY_SHARED_APP_CONFIG;

import android.app.Activity;
import android.app.IntentService;
import android.app.PendingIntent;
import android.content.ActivityNotFoundException;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import androidx.core.content.FileProvider;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.multidex.BuildConfig;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.androidquery.util.XmlDom;
import com.google.firebase.crashlytics.FirebaseCrashlytics;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.pccw.dango.shared.cra.AcMainCra;
import com.pccw.dango.shared.cra.AddOnCra;
import com.pccw.dango.shared.cra.ApptCra;
import com.pccw.dango.shared.cra.BaseCraEx;
import com.pccw.dango.shared.cra.BiifCra;
import com.pccw.dango.shared.cra.BinqCra;
import com.pccw.dango.shared.cra.CApptCra;
import com.pccw.dango.shared.cra.CtacCra;
import com.pccw.dango.shared.cra.DeleteCra;
import com.pccw.dango.shared.cra.DeleteRequest;
import com.pccw.dango.shared.cra.DqryCra;
import com.pccw.dango.shared.cra.HeloCra;
import com.pccw.dango.shared.cra.InbxCra;
import com.pccw.dango.shared.cra.LgiCra;
import com.pccw.dango.shared.cra.LnttCra;
import com.pccw.dango.shared.cra.PlanImsCra;
import com.pccw.dango.shared.cra.PlanLtsCra;
import com.pccw.dango.shared.cra.PlanMobCra;
import com.pccw.dango.shared.cra.PlanTvCra;
import com.pccw.dango.shared.cra.ShopCra;
import com.pccw.dango.shared.cra.SpssCra;
import com.pccw.dango.shared.cra.WalletCra;
import com.pccw.dango.shared.entity.Bill;
import com.pccw.dango.shared.entity.SpssRec;
import com.pccw.dango.shared.tool.RC;
import com.pccw.myhkt.listeners.OnDownloadAttachmentListener;
import com.pccw.myhkt.model.AppConfig;
import com.pccw.myhkt.model.AppConfigImageSetting;
import com.pccw.myhkt.model.DQAreas;
import com.pccw.myhkt.model.Destinations;
import com.pccw.myhkt.model.FileInfo;
import com.pccw.myhkt.model.IDDCity;
import com.pccw.myhkt.model.IDDCodesNTimes;
import com.pccw.myhkt.model.IDDCountry;
import com.pccw.myhkt.model.Rate;
import com.pccw.myhkt.model.SRPlan;
import com.pccw.myhkt.model.VersionCheck;
import com.pccw.myhkt.service.APIClient;
import com.pccw.myhkt.service.GetDataService;
import com.pccw.myhkt.util.Constant;
import com.pccw.myhkt.util.FileManager;
import com.pccw.wheat.shared.tool.Reply;

import org.apache.http.HttpEntity;
import org.apache.http.cookie.Cookie;
import org.apache.http.entity.StringEntity;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Type;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class APIsManager {
    private static boolean debug;
    private static boolean debugGrq;
    private static boolean debugCra;

    private static final Gson gson = new Gson();
    private static final Gson gson2 = new GsonBuilder().serializeNulls().create();
    public static List<Cookie> mCookies;
    public static ProgressManager progressManager = null;
    // Error Code
    public static final String HTTPSTATUS_NOT_200 = "HTTPSTATUS_NOT_200";
    public static final String JSON_RETURN_NULL = "JSON_RETURN_NULL";
    public static final String NULL_OBJECT_CRA = "NULL_OBJECT_CRA";

    private static String domainURL = "https://cspuat.pccw.com/cs2";

    // IDD 0060
    public static String UAT_shop_host = "http://shop.pccwmobile.com/";
    public static String PRD_shop_host = "http://shop.pccwmobile.com/";
    // public static String PRD_shop_host = "http://sbreg.0060everywhere.com/";

    public static String UAT_domain = "https://cspuat.pccw.com/cs2";
    // public static String UAT_domain = "http://10.87.120.207:8080/dango"; //internal UAT
    public static String PRD_domain = "https://customerservice.pccw.com/myhkt"; //pilot version fake PRD

    public static String UAT_FTP = "https://cspuat.pccw.com";
    public static String PRD_FTP = "https://customerservice.pccw.com";

    //SALT (for checksum number)
    public static String UAT_salt = "1234567890";
    public static String PRD_salt = "134552de84";

    // public static String UAT_livechat = "https://hkt.softether.net/genesyslab/AllInOne/chat_mobile/"; // omni-chat
    public static String UAT_livechat = "https://livechathostuat.pccw.com/hkt/chat/"; // omni-chat
    public static String PRD_livechat = "https://livechathost.pccw.com/hkta/chat/"; // omni-chat
    // public static final String THECLUB_HOST = "https://www.theclub.com.hk";

    public static final String HELO = "HELO";               /* MA for Helo                          */
    public static final String LGI = "LGI";                /* MA for Login                         */
    public static final String LGO = "LGO";                /* MA for Logout                        */
    public static final String RG_ACTCD = "RG_ACTCD";           /* MA for Regenerate Act Code           */
    public static final String P4_RCALL = "P4_RCALL";           /* MA for Preparation for Recall        */
    public static final String RC_CRED = "RC_CRED";            /* MA for Recall Credential             */
    public static final String SIGNUP = "SIGNUP";             /* MA for Signup                        */
    public static final String SMSACT = "SMSACT";             /* MA for SMS Activation                */
    public static final String RESET = "RESET";              /* MA for Reset Password                */
    public static final String PLAN_LTS = "PLAN_LTS";           /* MA for MyPlan (LTS)                  */
    public static final String PLAN_IMS = "PLAN_IMS";           /* MA for MyPlan (IMS)                  */
    public static final String PLAN_TV = "PLAN_TV";            /* MA for MyPlan (TV)                   */
    public static final String PLAN_MOB = "PLAN_MOB";           /* MA for MyPlan (MOB)                  */
    public static final String READ_CTAC = "READ_CTAC";          /* MA for Read Contact Info             */
    public static final String UPD_CTAC = "UPD_CTAC";           /* MA for Update Contact Info           */
    public static final String DEL_ACCOUNT = "APITY_APP_DEL_LOGINID";           /* MA for Update Contact Info           */

    public static final String BILL = "BILL";               /* MA for Bill                          */
    public static final String CHK_BILL = "CHK_BILL";           /* MA for Check Bill                    */
    public static final String READ_BIIF = "READ_BIIF";          /* MA for Read Billing Info             */
    public static final String UPD_BIIF = "UPD_BIIF";           /* MA for Update Billing Info           */
    public static final String APPT = "APPT";               /* MA for Appointment                   */
    public static final String CAPPT = "CAPPT";               /* MA for Cache Appointment            */
    public static final String SHOP_BYA = "SHOP_BYA";           /* MA for Shop (Read By Area)           */
    public static final String SHOP_BYGC = "SHOP_BYGC";          /* MA for Shop (Read By GC)             */
    public static final String DQRY = "DQRY";               /* MA for Directory Query               */
    public static final String ACMAIN = "ACMAIN";             /* MA for Account Maintenance           */
    public static final String SVCASO = "SVCASO";             /* MA for Associated Account List       */
    public static final String SVCASO4SUBN = "SVCASO4SUBN";        /* MA for Associated Account List       */
    public static final String RTRT = "RTRT";                /* MA for Line Test (Reset purpose)     */
    public static final String SPSS_UPD = "SPSS_UPD";           /* MA for Smart Phone (Update)          */
    public static final String TPUP_HIST = "TPUP_HIST";          /* MA for Get Topup History             */
    public static final String TPUP = "TPUP";               /* MA for Get Topup                     */
    public static final String AO_ADDON = "AO_ADDON";           /* MA for Add-on Add on                 */
    public static final String AO_AUTH = "AO_AUTH";            /* MA for Add-on Authentication         */
    public static final String REQ_UPDTOK4LTSPI = "REQ_UPDTOK4LTSPI";    /* MA for Update LTS biifCra            */
    public static final String LNTT = "LNTT";               /* MA for Line Test                     */
    public static final String SR_CRT = "SR_CRT";             /* MA for SR Creation                   */
    public static final String SR_UPD = "SR_UPD";             /* MA for SR Update                     */
    public static final String SR_CLS = "SR_CLS";             /* MA for SR Closure                    */
    public static final String SR_ENQ_NP = "SR_ENQ_NP";          /* MA for SR Enquiry (No Pending)       */
    public static final String SR_AVA_TS = "SR_AVA_TS";          /* MA for SR Available Timeslot         */
    public static final String ECLOSION = "ECLOSION";
    public static final String RBMD = "RBMD";                   //Reboot modem

    //Custom make Action type
    public static final String IDDCTY = "IDDCTY";             /* MA for IDD Country list	            */
    public static final String IDDCODE = "IDDCODE";            /*    for IDD Code Result   			*/
    public static final String IDDRATEDEST = "IDDRATEDEST";
    public static final String IDDRATERESULT = "IDDRATERESULT";
    public static final String API_FINGERPRCFG = "FINGERPRCFG"; //ENDPT_FINGERPRCG
    public static final String API_APP_CONFIG = "APPCONFIG";
    //Inbox Action Type
    public static final String INBX_MSG = "INBX_MSG";               //Get inbox messages
    public static final String INBX_UPD_RDSTS = "INBX_UPD_RDSTS";   //Update inbox details status (read)
    public static final String INBX_DEL_MSG = "INBX_DEL_MSG";   //Update inbox details status (read)

    // Wallet
    public static final String WALLET_EW_JWT = "EW_JWT";

    public interface OnAPIsListener {
        void onSuccess(APIsResponse response) throws Exception;

        void onFail(APIsResponse response) throws Exception;
    }

    private static void initDebugMode(Context context) {
        debug = context.getResources().getBoolean(R.bool.DEBUG);
        debugGrq = context.getResources().getBoolean(R.bool.DEBUGGRQ);
        debugCra = context.getResources().getBoolean(R.bool.DEBUGCRA);
    }

    /*
    public static void doLogin(final FragmentActivity activity, LgiCra rLgiCra, boolean isSavePwd) {
        final String TAGNAME = "doLogin";
        AQuery aq = new AQuery(activity);
        domainURL = ClnEnv.getPref(activity, activity.getString(R.string.CONST_PREF_DOMAIN), APIsManager.PRD_domain);
        String url = domainURL + "/ma/lgi";
        LgiCra lgicra = rLgiCra.copyMe();
        lgicra.setIApi(LGI);
        lgicra.setIGwtGud(ClnEnv.getSessTok());
        lgicra.getISpssRec().devTy = SpssRec.TY_ANDROID;
        lgicra.getISpssRec().devId = ClnEnv.getPref(activity, activity.getString(R.string.CONST_PREF_GCM_REGID), "");

        lgicra.getISpssRec().lang = ClnEnv.getAppLocale(activity);
        lgicra.getISpssRec().gni = ClnEnv.getPref(((Context) activity).getApplicationContext(), activity.getString(R.string.CONST_PREF_GENNOTI_FLAG), "Y");
        lgicra.getISpssRec().bni = (isSavePwd) ? "Y" : "N";

        String jstr = gson.toJson(lgicra);
        if (debugGrq) ClnEnv.toLogFile(String.format("%s %s", TAGNAME, "request sent"), jstr);
        if (debugCra) Log.d(TAGNAME, String.format("%s %s", TAGNAME, "request sent:") + jstr);

        try {
            HttpEntity entity = new StringEntity(jstr, "UTF-8");
            Map<String, Object> params = new HashMap<>();
            params.put(AQuery.POST_ENTITY, entity);

            AjaxCallback<JSONObject> ajaxCallback = new AjaxCallback<JSONObject>() {
                OnAPIsListener callback;
                APIsResponse response;
                LgiCra cra;

                @Override
                public void callback(String url, JSONObject json, AjaxStatus status) {
                    //        	          cookies = status.getCookies(); // Save the cookies in every requests you made
                    try {
                        try {
                            callback = (OnAPIsListener) activity;
                        } catch (ClassCastException e) {
                            throw new ClassCastException(activity.toString() + " must implement OnAPIsListener");
                        }

                        response = new APIsResponse();
                        response.setActionTy(LGI);
                        cra = new LgiCra();
                        if (status.getCode() != 200) {
                            // TODO: return ERROR Message for HTTPSTATUS Not 200
                            if (debugCra)
                                Log.d(TAGNAME, "HTTPSTATUS_NOT_200 : Status : " + status.getCode() + ", Error : " + status.getError());
                            response.setReply(HTTPSTATUS_NOT_200);
                            response.setMessage(ClnEnv.getRPCErrMsg(activity, status.getCode(), status.getMessage()));
                            //	            		//derek force success for testing only START
                            //	            		String resultStr = "{\"serverTS\":\"20151130155041\",\"iGwtGud\":\"\",\"iOrigPwd\":\"\",\"oDuct\":null,\"oSrvReqAry\":[],\"iRecallLgiId\":false,\"oTaSubnRec\":{\"rev\":0,\"createTs\":\"\",\"netLoginId\":\"\",\"custRid\":0,\"systy\":\"\",\"instAdr\":\"\",\"eyeGrp\":\"\",\"sipSubr\":\"\",\"wipCust\":\"\",\"storeTy\":\"\",\"lastupdTs\":\"\",\"datCd\":\"\",\"indTv\":\"\",\"lastupdPsn\":\"\",\"lob\":\"\",\"tos\":\"\",\"indPcd\":\"\",\"auth_ph\":null,\"tariff\":\"\",\"indEye\":\"\",\"createPsn\":\"\",\"cusNum\":\"\",\"assoc\":\"\",\"fsa\":\"\",\"acct_ty\":null,\"alias\":\"\",\"acctNum\":\"\",\"ind2l2b\":\"\",\"domainTy\":\"\",\"srvNum\":\"\",\"bsn\":\"\",\"rid\":0,\"refFsa2l2b\":\"\",\"srvId\":\"\",\"priMob\":\"\",\"aloneDialup\":\"\",\"ivr_pwd\":null},\"oBomCustAry\":[{\"cusNum\":\"9500602\",\"lob\":\"PCD\",\"acctNum\":\"1200094879\",\"sysTy\":\"IMS\"}],\"iSveeRec\":{\"loginId\":\"\",\"rev\":0,\"staffId\":\"\",\"secAns\":\"\",\"ctMail\":\"\",\"status\":\"\",\"nickname\":\"\",\"createTs\":\"\",\"teamCd\":\"\",\"state\":\"\",\"custRid\":0,\"lastupdTs\":\"\",\"lang\":\"\",\"lastupdPsn\":\"\",\"ctMob\":\"\",\"salesChnl\":\"\",\"pwdEff\":\"\",\"pwd\":\"\",\"promoOpt\":\"\",\"regnActn\":\"\",\"rid\":0,\"stateUpTs\":\"\",\"secQus\":0,\"mobAlrt\":\"\",\"createPsn\":\"\"},\"oQualSvee\":{\"zmSubnRecAry\":[],\"subnRecAry\":[{\"rev\":0,\"createTs\":\"20151127164938\",\"netLoginId\":\"b60070059\",\"custRid\":7503,\"systy\":\"IMS\",\"instAdr\":\"\",\"eyeGrp\":\" \",\"sipSubr\":\" \",\"wipCust\":\"N\",\"storeTy\":\"\",\"lastupdTs\":\"20151127164938\",\"datCd\":\" \",\"indTv\":\"N\",\"lastupdPsn\":\"dldsbat\",\"lob\":\"PCD\",\"tos\":\"IMS\",\"indPcd\":\"Y\",\"auth_ph\":null,\"tariff\":\" \",\"indEye\":\"N\",\"createPsn\":\"dldsbat\",\"cusNum\":\"9500602\",\"assoc\":\"Y\",\"fsa\":\"60070059\",\"acct_ty\":null,\"alias\":\" \",\"acctNum\":\"1200094879\",\"ind2l2b\":\"N\",\"domainTy\":\"N\",\"srvNum\":\"b60070059\",\"bsn\":\" \",\"rid\":44775,\"refFsa2l2b\":\" \",\"srvId\":\"60070059\",\"priMob\":\" \",\"aloneDialup\":\"N\",\"ivr_pwd\":null},{\"rev\":0,\"createTs\":\"20151127164938\",\"netLoginId\":\"a60069429\",\"custRid\":7503,\"systy\":\"IMS\",\"instAdr\":\"\",\"eyeGrp\":\" \",\"sipSubr\":\" \",\"wipCust\":\"N\",\"storeTy\":\"\",\"lastupdTs\":\"20151127164938\",\"datCd\":\" \",\"indTv\":\"N\",\"lastupdPsn\":\"dldsbat\",\"lob\":\"PCD\",\"tos\":\"IMS\",\"indPcd\":\"Y\",\"auth_ph\":null,\"tariff\":\" \",\"indEye\":\"N\",\"createPsn\":\"dldsbat\",\"cusNum\":\"9500602\",\"assoc\":\"Y\",\"fsa\":\"60069429\",\"acct_ty\":null,\"alias\":\" \",\"acctNum\":\"1200101993\",\"ind2l2b\":\"N\",\"domainTy\":\"N\",\"srvNum\":\"a60069429\",\"bsn\":\" \",\"rid\":44776,\"refFsa2l2b\":\" \",\"srvId\":\"60069429\",\"priMob\":\" \",\"aloneDialup\":\"N\",\"ivr_pwd\":null},{\"rev\":0,\"createTs\":\"20151127164938\",\"netLoginId\":\"a60069005\",\"custRid\":7503,\"systy\":\"IMS\",\"instAdr\":\"\",\"eyeGrp\":\" \",\"sipSubr\":\" \",\"wipCust\":\"N\",\"storeTy\":\"\",\"lastupdTs\":\"20151127164938\",\"datCd\":\" \",\"indTv\":\"N\",\"lastupdPsn\":\"dldsbat\",\"lob\":\"PCD\",\"tos\":\"IMS\",\"indPcd\":\"Y\",\"auth_ph\":null,\"tariff\":\" \",\"indEye\":\"N\",\"createPsn\":\"dldsbat\",\"cusNum\":\"9500602\",\"assoc\":\"Y\",\"fsa\":\"60069005\",\"acct_ty\":null,\"alias\":\" \",\"acctNum\":\"1200102006\",\"ind2l2b\":\"N\",\"domainTy\":\"N\",\"srvNum\":\"a60069005\",\"bsn\":\" \",\"rid\":44777,\"refFsa2l2b\":\" \",\"srvId\":\"60069005\",\"priMob\":\" \",\"aloneDialup\":\"N\",\"ivr_pwd\":null},{\"rev\":0,\"createTs\":\"20151127164938\",\"netLoginId\":\"a60069009\",\"custRid\":7503,\"systy\":\"IMS\",\"instAdr\":\"\",\"eyeGrp\":\" \",\"sipSubr\":\" \",\"wipCust\":\"N\",\"storeTy\":\"\",\"lastupdTs\":\"20151127164938\",\"datCd\":\" \",\"indTv\":\"N\",\"lastupdPsn\":\"dldsbat\",\"lob\":\"PCD\",\"tos\":\"IMS\",\"indPcd\":\"Y\",\"auth_ph\":null,\"tariff\":\" \",\"indEye\":\"N\",\"createPsn\":\"dldsbat\",\"cusNum\":\"9500602\",\"assoc\":\"Y\",\"fsa\":\"60069009\",\"acct_ty\":null,\"alias\":\" \",\"acctNum\":\"1200121942\",\"ind2l2b\":\"N\",\"domainTy\":\"N\",\"srvNum\":\"a60069009\",\"bsn\":\" \",\"rid\":44778,\"refFsa2l2b\":\" \",\"srvId\":\"60069009\",\"priMob\":\" \",\"aloneDialup\":\"N\",\"ivr_pwd\":null},{\"rev\":0,\"createTs\":\"20151127164938\",\"netLoginId\":\"a60071827\",\"custRid\":7503,\"systy\":\"IMS\",\"instAdr\":\"\",\"eyeGrp\":\" \",\"sipSubr\":\" \",\"wipCust\":\"N\",\"storeTy\":\"\",\"lastupdTs\":\"20151127164938\",\"datCd\":\" \",\"indTv\":\"Y\",\"lastupdPsn\":\"dldsbat\",\"lob\":\"PCD\",\"tos\":\"IMS\",\"indPcd\":\"Y\",\"auth_ph\":null,\"tariff\":\" \",\"indEye\":\"N\",\"createPsn\":\"dldsbat\",\"cusNum\":\"9500602\",\"assoc\":\"Y\",\"fsa\":\"60071827\",\"acct_ty\":null,\"alias\":\" \",\"acctNum\":\"1200124349\",\"ind2l2b\":\"N\",\"domainTy\":\"N\",\"srvNum\":\"a60071827\",\"bsn\":\" \",\"rid\":44779,\"refFsa2l2b\":\" \",\"srvId\":\"60071827\",\"priMob\":\" \",\"aloneDialup\":\"N\",\"ivr_pwd\":null},{\"rev\":0,\"createTs\":\"20151127164938\",\"netLoginId\":\"a60071827\",\"custRid\":7503,\"systy\":\"IMS\",\"instAdr\":\"\",\"eyeGrp\":\" \",\"sipSubr\":\" \",\"wipCust\":\"N\",\"storeTy\":\"\",\"lastupdTs\":\"20151127164938\",\"datCd\":\" \",\"indTv\":\"Y\",\"lastupdPsn\":\"dldsbat\",\"lob\":\"TV\",\"tos\":\"IMS\",\"indPcd\":\"Y\",\"auth_ph\":null,\"tariff\":\" \",\"indEye\":\"N\",\"createPsn\":\"dldsbat\",\"cusNum\":\"9500602\",\"assoc\":\"Y\",\"fsa\":\"60071827\",\"acct_ty\":null,\"alias\":\" \",\"acctNum\":\"1200124352\",\"ind2l2b\":\"N\",\"domainTy\":\"N\",\"srvNum\":\"1200124352\",\"bsn\":\" \",\"rid\":44780,\"refFsa2l2b\":\" \",\"srvId\":\"60071827\",\"priMob\":\" \",\"aloneDialup\":\"N\",\"ivr_pwd\":null}],\"custRec\":{\"docNum\":\"88888891\",\"phylum\":\"CSUM\",\"rev\":0,\"status\":\"A\",\"rid\":7503,\"createTs\":\"20151127164938\",\"premier\":\" \",\"lastupdTs\":\"20151127164938\",\"docTy\":\"PASS\",\"createPsn\":\"dldsbat\",\"lastupdPsn\":\"dldsbat\"},\"bcifRec\":{\"priCtJobtitle\":\"\",\"rev\":0,\"custNm\":\"\",\"secCtMail\":\"\",\"createTs\":\"\",\"custRid\":0,\"secCtTel\":\"\",\"lastupdTs\":\"\",\"priCtMob\":\"\",\"priCtMail\":\"\",\"xborder\":\"\",\"lastupdPsn\":\"\",\"secCtMob\":\"\",\"nuStaff\":\"\",\"secCtName\":\"\",\"priCtTel\":\"\",\"nuPresence\":\"\",\"priCtName\":\"\",\"priCtTitle\":\"\",\"secCtTitle\":\"\",\"industry\":\"\",\"createPsn\":\"\",\"secCtJobtitle\":\"\"},\"sveeRec\":{\"loginId\":\"keith.kk.mak@pccw.com\",\"rev\":1,\"staffId\":\" \",\"secAns\":\"\",\"ctMail\":\"keith.kk.mak@pccw.com\",\"status\":\"A\",\"nickname\":\"KM\",\"createTs\":\"20151130103801\",\"teamCd\":\" \",\"state\":\"A\",\"custRid\":7503,\"lastupdTs\":\"20151130103824\",\"lang\":\"zh\",\"lastupdPsn\":\"!UNKN\",\"ctMob\":\"66230738\",\"salesChnl\":\" \",\"pwdEff\":\"Y\",\"pwd\":\"\",\"promoOpt\":\"N\",\"regnActn\":\"REGUSER\",\"rid\":3,\"stateUpTs\":\"20151130103824\",\"secQus\":0,\"mobAlrt\":\"Y\",\"createPsn\":\"!UNKN\"}},\"oGnrlApptAry\":[],\"iSpssRec\":{\"rev\":0,\"devTy\":\"\",\"dervRid\":0,\"rid\":0,\"bni\":\"\",\"sveeRid\":0,\"lastupdTs\":\"\",\"tcId\":\"\",\"gni\":\"\",\"devId\":\"\",\"tcSess\":\"\",\"lang\":\"\",\"lastupdPsn\":\"\"},\"oNxUrl\":\"\",\"iMyFrm\":\"\",\"iCustRec\":{\"docNum\":\"\",\"phylum\":\"\",\"rev\":0,\"status\":\"\",\"rid\":0,\"createTs\":\"\",\"premier\":\"\",\"lastupdTs\":\"\",\"docTy\":\"\",\"createPsn\":\"\",\"lastupdPsn\":\"\"},\"oHgg\":\"\",\"oAcctAry\":[{\"custRid\":7503,\"lob\":\"PCD\",\"sysTy\":\"IMS\",\"cusNum\":\"9500602\",\"live\":true,\"acctNum\":\"1200094879\"}],\"oChatTok\":\"e32ca1045577bcd7b0fc68ccb90e92f3d3d620d9a63b0a284a333fdce7073812\",\"reply\":{\"cargo\":null,\"code\":\"RC_SUCC\"},\"iApi\":\"\",\"oBillListAry\":[{\"oBillAry\":[],\"iAcct\":{\"custRid\":7503,\"lob\":\"PCD\",\"sysTy\":\"IMS\",\"cusNum\":\"9500602\",\"live\":true,\"acctNum\":\"1200094879\"}},{\"oBillAry\":[],\"iAcct\":{\"custRid\":7503,\"lob\":\"PCD\",\"sysTy\":\"IMS\",\"cusNum\":\"9500602\",\"live\":true,\"acctNum\":\"1200101993\"}},{\"oBillAry\":[],\"iAcct\":{\"custRid\":7503,\"lob\":\"PCD\",\"sysTy\":\"IMS\",\"cusNum\":\"9500602\",\"live\":true,\"acctNum\":\"1200102006\"}},{\"oBillAry\":[],\"iAcct\":{\"custRid\":7503,\"lob\":\"PCD\",\"sysTy\":\"IMS\",\"cusNum\":\"9500602\",\"live\":true,\"acctNum\":\"1200121942\"}},{\"oBillAry\":[],\"iAcct\":{\"custRid\":7503,\"lob\":\"PCD\",\"sysTy\":\"IMS\",\"cusNum\":\"9500602\",\"live\":true,\"acctNum\":\"1200124349\"}},{\"oBillAry\":[],\"iAcct\":{\"custRid\":7503,\"lob\":\"TV\",\"sysTy\":\"IMS\",\"cusNum\":\"9500602\",\"live\":true,\"acctNum\":\"1200124352\"}}],\"oSoGud\":\"\",\"iRecallPwd\":false,\"iNaSubnRec\":{\"rev\":0,\"createTs\":\"\",\"netLoginId\":\"\",\"custRid\":0,\"systy\":\"\",\"instAdr\":\"\",\"eyeGrp\":\"\",\"sipSubr\":\"\",\"wipCust\":\"\",\"storeTy\":\"\",\"lastupdTs\":\"\",\"datCd\":\"\",\"indTv\":\"\",\"lastupdPsn\":\"\",\"lob\":\"\",\"tos\":\"\",\"indPcd\":\"\",\"auth_ph\":null,\"tariff\":\"\",\"indEye\":\"\",\"createPsn\":\"\",\"cusNum\":\"\",\"assoc\":\"\",\"fsa\":\"\",\"acct_ty\":null,\"alias\":\"\",\"acctNum\":\"\",\"ind2l2b\":\"\",\"domainTy\":\"\",\"srvNum\":\"\",\"bsn\":\"\",\"rid\":0,\"refFsa2l2b\":\"\",\"srvId\":\"\",\"priMob\":\"\",\"aloneDialup\":\"\",\"ivr_pwd\":null}}";
                            //	            		Type collectionType = new TypeToken<LgiCra>() {}.getType();
                            //	            		cra = gson.fromJson(resultStr, collectionType);
                            //	            		ClnEnv.setLgiCra(cra);
                            //	            		response.setReply(cra.getReply());
                            //            			response.setCra(cra);
                            //	            			callback.onSuccess(response);
                            //	            		//derek force success for testing only END
                            callback.onFail(response);
                        } else if (json == null) {
                            // TODO: httppost response null Object from api
                            if (debugCra) Log.d(TAGNAME, JSON_RETURN_NULL);
                            response.setReply(JSON_RETURN_NULL);
                            response.setMessage(ClnEnv.getRPCErrMsg(activity, status.getCode(), status.getMessage()));
                            callback.onFail(response);
                        } else {
                            String resultStr = json.toString();
                            if (debugCra)
                                ClnEnv.toLogFile(String.format("%s %s %s", TAGNAME, cra.getClass().getSimpleName(), "response received"), resultStr);
                            if (debugCra)
                                Log.d(TAGNAME, String.format("%s %s %s", TAGNAME, cra.getClass().getSimpleName(), "response received:") + resultStr);

                            try {
                                Type collectionType = new TypeToken<LgiCra>() {
                                }.getType();
                                cra = gson.fromJson(json.toString(), collectionType);
                            } catch (Exception e) {
                                if (debug) Log.e(TAGNAME, "/gson.fromJson error:" + e.getMessage());
                                e.printStackTrace();
                            }

                            if (cra == null || !cra.getReply().isSucc()) {
                                if (cra == null) {
                                    cra = new LgiCra();
                                    cra.setReply(NULL_OBJECT_CRA);
                                }
                                response.setReply(cra.getReply());
                                response.setCra(cra);
                                callback.onFail(response);
                            } else {
                                ClnEnv.setLgiCra(cra);
                                response.setReply(cra.getReply());
                                response.setCra(cra);
                                callback.onSuccess(response);
                            }
                        }
                    } catch (Exception e) {
                        if (debugCra) Log.d(TAGNAME, e.toString());
                    }
                }
            };

            for (Cookie cookie : mCookies) {
                ajaxCallback.cookie(cookie.getName(), cookie.getValue());
                //Here we are setting the cookie info.
            }
            aq.ajax(url, params, JSONObject.class, ajaxCallback);
        } catch (Exception e) {
            e.toString();
        }
    }
    */

    public static void doLogout(final FragmentActivity activity) {
        domainURL = ClnEnv.getPref(activity, activity.getString(R.string.CONST_PREF_DOMAIN), APIsManager.PRD_domain);
        String url = domainURL + "/ma/lgo";
        LgiCra lgicra = new LgiCra();
        lgicra.setIApi(LGO);
        lgicra.setIGwtGud(ClnEnv.getSessTok());
        APIsRequest<LgiCra> apiRequest = new APIsRequest<>(url, LGO);
        apiRequest.setiCra(lgicra);
        doAction(activity, null, LgiCra.class, apiRequest);
    }

    public static void doRegister(final FragmentActivity activity, AcMainCra rAcMainCra, boolean isSavePwd) {
        domainURL = ClnEnv.getPref(activity, activity.getString(R.string.CONST_PREF_DOMAIN), APIsManager.PRD_domain);
        String url = domainURL + "/ma/signup";
        AcMainCra acMainCra = rAcMainCra.copyMe();
        acMainCra.setIApi(SIGNUP);
        acMainCra.setIGwtGud(ClnEnv.getSessTok());
        // acMainCra.getISpssRec().devTy = SpssRec.TY_ANDROID;
        // acMainCra.getISpssRec().devId = ClnEnv.getPref(cxt, cxt.getString(R.string.CONST_PREF_GCM_REGID), "");
        // acMainCra.getISpssRec().devId = ClnEnv.getDeviceID(activity);
        // acMainCra.getISpssRec().lang = ClnEnv.getAppLocale(activity);
        // acMainCra.getISpssRec().gni = ClnEnv.getPref(cxt.getApplicationContext(), cxt.getString(R.string.CONST_PREF_GENMSG_NOTICEFLAG), "Y");
        // acMainCra.getISpssRec().bni = (isSavePwd) ? "Y" : "N";
        APIsRequest<AcMainCra> apiRequest = new APIsRequest<>(url, SIGNUP);
        apiRequest.setiCra(acMainCra);

        // 2nd request
        progressManager = ProgressManager.getInstance(activity);
        progressManager.showProgressDialog(activity);
        doHelo(activity, null, AcMainCra.class, apiRequest);
    }

    public static void doPrepare4Recall(final FragmentActivity activity, LgiCra rLgiCra) {
        domainURL = ClnEnv.getPref(activity, activity.getString(R.string.CONST_PREF_DOMAIN), APIsManager.PRD_domain);
        String url = domainURL + "/ma/prepRecall";
        LgiCra lgicra = rLgiCra.copyMe();
        lgicra.setIApi(P4_RCALL);
        lgicra.setIGwtGud(ClnEnv.getSessTok());
        lgicra.getISpssRec().devTy = SpssRec.TY_ANDROID;
        lgicra.getISpssRec().devId = ClnEnv.getPref(activity, activity.getString(R.string.CONST_PREF_GCM_REGID), "");
        lgicra.getISpssRec().lang = ClnEnv.getAppLocale(activity);
        lgicra.getISpssRec().gni = ClnEnv.getPref(((Context) activity).getApplicationContext(), activity.getString(R.string.CONST_PREF_GENNOTI_FLAG), "Y");
        APIsRequest<LgiCra> apiRequest = new APIsRequest<>(url, P4_RCALL);
        apiRequest.setiCra(lgicra);

        // 2nd request
        progressManager = ProgressManager.getInstance(activity);
        progressManager.showProgressDialog(activity);
        doHelo(activity, null, LgiCra.class, apiRequest);
    }

    public static void doRecallIDOrPwd(final FragmentActivity activity, LgiCra rLgiCra) {
        domainURL = ClnEnv.getPref(activity, activity.getString(R.string.CONST_PREF_DOMAIN), APIsManager.PRD_domain);
        String url = domainURL + "/ma/recallCred";
        LgiCra lgicra = rLgiCra.copyMe();
        lgicra.setIApi(RC_CRED);
        lgicra.setIGwtGud(ClnEnv.getSessTok());

        APIsRequest<LgiCra> apiRequest = new APIsRequest<>(url, RC_CRED);
        apiRequest.setiCra(lgicra);
        // 2nd request
        progressManager = ProgressManager.getInstance(activity);
        progressManager.showProgressDialog(activity);
        doHelo(activity, null, LgiCra.class, apiRequest);
    }

    public static void doResetPwd(final FragmentActivity activity, AcMainCra rAcMainCra) {
        domainURL = ClnEnv.getPref(activity, activity.getString(R.string.CONST_PREF_DOMAIN), APIsManager.PRD_domain);
        String url = domainURL + "/ma/reset";

        AcMainCra acMainCra = rAcMainCra.copyMe();
        acMainCra.setIApi(RESET);
        acMainCra.setIBcifRec(null); //must be null for all acMainCra related API call
        APIsRequest<AcMainCra> apiRequest = new APIsRequest<>(url, RESET);
        apiRequest.setiCra(acMainCra);
        doHelo(activity, null, AcMainCra.class, apiRequest);
    }

    public static void doChangePwd(final Fragment fragment, AcMainCra rAcMainCra) {
        Context cxt = fragment.getActivity();
        domainURL = ClnEnv.getPref(cxt, Objects.requireNonNull(cxt).getString(R.string.CONST_PREF_DOMAIN), APIsManager.PRD_domain);
        String url = domainURL + "/ma/acMain";
        AcMainCra acMainCra = rAcMainCra.copyMe();
        acMainCra.setIApi(ACMAIN);
        acMainCra.setIGwtGud(ClnEnv.getSessTok());
        acMainCra.setIBcifRec(null); //must be null for all acMainCra related API call
        APIsRequest<AcMainCra> apiRequest = new APIsRequest<>(url, ACMAIN);
        apiRequest.setiCra(acMainCra);
        doAction(fragment.getActivity(), fragment, AcMainCra.class, apiRequest);
    }

    public static void doUpdProfile(final FragmentActivity activity, final Fragment fragment, AcMainCra rAcMainCra) {
        Context cxt = activity == null ? fragment.getActivity() : activity;
        domainURL = ClnEnv.getPref(cxt, Objects.requireNonNull(cxt).getString(R.string.CONST_PREF_DOMAIN), APIsManager.PRD_domain);
        String url = domainURL + "/ma/acMain";
        AcMainCra acMainCra = rAcMainCra.copyMe();
        acMainCra.setIApi(ACMAIN);
        acMainCra.setIGwtGud(ClnEnv.getSessTok());
        acMainCra.setIChgPwd(false);
        acMainCra.setIBcifRec(null); //must be null for all acMainCra related API call

        APIsRequest<AcMainCra> apiRequest = new APIsRequest<>(url, ACMAIN);
        apiRequest.setiCra(acMainCra);
        // 2nd request
        progressManager = ProgressManager.getInstance(cxt);
        progressManager.showProgressDialog(cxt);
        doAction(activity, fragment, AcMainCra.class, apiRequest);
    }

    public static void doUpdCtInfo(final Fragment fragment, CtacCra rCtacCra) {
        Context cxt = fragment.getActivity();
        domainURL = ClnEnv.getPref(cxt, Objects.requireNonNull(cxt).getString(R.string.CONST_PREF_DOMAIN), APIsManager.PRD_domain);
        String url = domainURL + "/ma/updCtac";
        CtacCra ctacCra = rCtacCra.copyMe();
        ctacCra.setIApi(UPD_CTAC);
        ctacCra.setIGwtGud(ClnEnv.getSessTok());

        APIsRequest<CtacCra> apiRequest = new APIsRequest<>(url, UPD_CTAC);
        apiRequest.setiCra(ctacCra);

        progressManager = ProgressManager.getInstance(cxt);
        progressManager.showProgressDialog(cxt);

        doAction(fragment.getActivity(), fragment, CtacCra.class, apiRequest);
    }

    public static void doReadCtInfo(final Fragment fragment, CtacCra rCtacCra) {
        Context cxt = fragment.getActivity();
        domainURL = ClnEnv.getPref(cxt, Objects.requireNonNull(cxt).getString(R.string.CONST_PREF_DOMAIN), APIsManager.PRD_domain);
        String url = domainURL + "/ma/readCtac";

        CtacCra ctacCra = rCtacCra.copyMe();
        ctacCra.setIApi(READ_CTAC);
        ctacCra.setIGwtGud(ClnEnv.getSessTok());

        APIsRequest<CtacCra> apiRequest = new APIsRequest<>(url, READ_CTAC);
        apiRequest.setiCra(ctacCra);

        progressManager = ProgressManager.getInstance(cxt);
        progressManager.showProgressDialog(cxt);

        doAction(fragment.getActivity(), fragment, CtacCra.class, apiRequest);
    }

    public static void doGetShop(final Fragment fragment, ShopCra shopcra) {
        Context cxt = fragment.getActivity();
        domainURL = ClnEnv.getPref(cxt, Objects.requireNonNull(cxt).getString(R.string.CONST_PREF_DOMAIN), APIsManager.PRD_domain);
        String url = domainURL + "/ma/shopByArea";
        shopcra.setIApi(SHOP_BYA);
        APIsRequest<ShopCra> apiRequest = new APIsRequest<>(url, SHOP_BYA);
        apiRequest.setiCra(shopcra);
        // 2nd request
        progressManager = ProgressManager.getInstance(fragment.getActivity());
        progressManager.showProgressDialog(fragment.getActivity());
        doHeloOptional(fragment.getActivity(), fragment, ShopCra.class, apiRequest);
    }

    public static void activateUserFingerPrint(final FragmentActivity activity, LgiCra rLgiCra) {
        domainURL = ClnEnv.getPref(activity, activity.getString(R.string.CONST_PREF_DOMAIN), APIsManager.PRD_domain);
        String url = domainURL + "/ma/fingerprCfg";
        LgiCra lgicra = rLgiCra.copyMe();
        lgicra.setIApi(API_FINGERPRCFG);
        lgicra.setIGwtGud(ClnEnv.getSessTok());

        APIsRequest<LgiCra> apiRequest = new APIsRequest<>(url, API_FINGERPRCFG);
        apiRequest.setiCra(lgicra);

        // 2nd request
        progressManager = ProgressManager.getInstance(activity);
        progressManager.showProgressDialog(activity);
        doAction(activity, null, LgiCra.class, apiRequest);
    }

    public static void doLoginTest(final FragmentActivity activity, LgiCra rLgiCra, boolean isSavePwd) {
        domainURL = ClnEnv.getPref(activity, activity.getString(R.string.CONST_PREF_DOMAIN), APIsManager.PRD_domain);
        String url = domainURL + "/ma/lgi";
        LgiCra lgicra = rLgiCra.copyMe();
        lgicra.setIApi(LGI);
        lgicra.setIGwtGud(ClnEnv.getSessTok());
        lgicra.getISpssRec().devTy = SpssRec.TY_ANDROID;
        lgicra.getISpssRec().devId = ClnEnv.getPref(activity, activity.getString(R.string.CONST_PREF_GCM_REGID), "");
        lgicra.getISpssRec().lang = ClnEnv.getAppLocale(activity);
        lgicra.getISpssRec().gni = ClnEnv.getPref(activity, activity.getString(R.string.CONST_PREF_GENNOTI_FLAG), "Y");
        lgicra.getISpssRec().bni = (isSavePwd) ? "Y" : "N";

        APIsRequest<LgiCra> apiRequest = new APIsRequest<>(url, LGI);
        apiRequest.setiCra(lgicra);

        // 2nd request
        progressManager = ProgressManager.getInstance(activity);
        progressManager.showProgressDialog(activity);
        doHelo(activity, null, LgiCra.class, apiRequest);
    }

    /*
    public static final void doRegen(final Fragment frag, LgiCra rLgiCra) {
        Context cxt = frag.getActivity();
        domainURL = ClnEnv.getPref(cxt, cxt.getString(R.string.CONST_PREF_DOMAIN), APIsManager.PRD_domain);
        String url = domainURL + "/ma/regenActCd";
        LgiCra lgicra = rLgiCra.copyMe();
        lgicra.setIApi(RG_ACTCD);
        lgicra.setIGwtGud(ClnEnv.getSessTok());
        lgicra.getISpssRec().devTy = SpssRec.TY_ANDROID;
        lgicra.getISpssRec().devId = ClnEnv.getPref(cxt, cxt.getString(R.string.CONST_PREF_GCM_REGID), "");
        //lgicra.getISpssRec().devId = ClnEnv.getDeviceID(activity);
        lgicra.getISpssRec().lang = ClnEnv.getAppLocale(frag.getActivity());

        APIsRequest<LgiCra> apiRequest = new APIsRequest<LgiCra>(url, LGI);
        apiRequest.setiCra(lgicra);

        doHelo(frag.getActivity(), frag, LgiCra.class, apiRequest);
    }
    */

    /**
     * Inbox
     */
    public static void doGetInboxMessages(IntentService service, InbxCra inbxCra) {
        domainURL = ClnEnv.getPref(service, service.getString(R.string.CONST_PREF_DOMAIN), APIsManager.PRD_domain);
        String url = domainURL + "/ma/ibxMsg";
        InbxCra apptCra = inbxCra.copyMe();
        apptCra.setIApi(INBX_MSG);
        apptCra.setIGwtGud(ClnEnv.getSessTok());
        APIsRequest<InbxCra> apiRequest = new APIsRequest<>(url, INBX_MSG);
        apiRequest.setiCra(apptCra);
        doAction(null, null, service, InbxCra.class, apiRequest, true);
    }

    public static void doUpdateInboxStatus(final FragmentActivity activity, InbxCra inbxCra) {
        domainURL = ClnEnv.getPref(activity, activity.getString(R.string.CONST_PREF_DOMAIN), APIsManager.PRD_domain);
        String url = domainURL + "/ma/updRdSts";
        InbxCra apptCra = inbxCra.copyMe();
        apptCra.setIApi(INBX_UPD_RDSTS);
        apptCra.setIGwtGud(ClnEnv.getSessTok());

        APIsRequest<InbxCra> apiRequest = new APIsRequest<>(url, INBX_UPD_RDSTS);
        apiRequest.setiCra(apptCra);

        doAction(activity, null, null, InbxCra.class, apiRequest, true);
    }

    public static void doDeleteMessageFromInbox(final FragmentActivity activity, InbxCra inbxCra) {
        domainURL = ClnEnv.getPref(activity, activity.getString(R.string.CONST_PREF_DOMAIN), APIsManager.PRD_domain);
        String url = domainURL + "/ma/delIbxMsg";
        InbxCra apptCra = inbxCra.copyMe();
        apptCra.setIApi(INBX_DEL_MSG);
        apptCra.setIGwtGud(ClnEnv.getSessTok());

        APIsRequest<InbxCra> apiRequest = new APIsRequest<>(url, INBX_DEL_MSG);
        apiRequest.setiCra(apptCra);

        doAction(activity, null, null, InbxCra.class, apiRequest, true);
    }

    /*******************************
     * DQ
     */
    public static void doReadDistrict(FragmentActivity activity) {
        domainURL = ClnEnv.getPref(activity, activity.getString(R.string.CONST_PREF_DOMAIN), APIsManager.PRD_domain);
        String url = String.format("%s%s", "https://customerservice.pccw.com/", "mba/data/dq_districts.txt");

        DQAreas dqAreas = new DQAreas();
        APIsRequest<DQAreas> apiRequest = new APIsRequest<>(url, LGI);
        apiRequest.setiCra(dqAreas);

        progressManager = ProgressManager.getInstance(activity);
        progressManager.showProgressDialog(activity);

        doHeloOptional(activity, null, DQAreas.class, apiRequest);
    }

    public static void doDearDqry(FragmentActivity activity, DqryCra mDqryCra) {
        domainURL = ClnEnv.getPref(activity, activity.getString(R.string.CONST_PREF_DOMAIN), APIsManager.PRD_domain);
        String url = domainURL + "/ma/dqry";
        DqryCra dqryCra = mDqryCra.copyMe();
        dqryCra.setIApi(DQRY);

        APIsRequest<DqryCra> apiRequest = new APIsRequest<>(url, DQRY);
        apiRequest.setiCra(dqryCra);

        progressManager = ProgressManager.getInstance(activity);
        progressManager.showProgressDialog(activity);

        doHeloOptional(activity, null, DqryCra.class, apiRequest);
    }

    // get XML Response
    // IDD
    // Method 1: AQuery Method - Reference : http://code.google.com/p/android-query/wiki/AsyncAPI
    // light Weight version - may exist memory problem if XML too large
    public static void doGetIDDCountries(final Fragment frag) {
        final String countryUrl = "http://www.hkt.com/staticfiles/PCCWCorpsite/EForms/IDDCodes/countrylist.xml";
//		final String webviewurl = "http://www.pccw.com/Customer+Service/Directory+Inquiries/International+Telephone+Numbers/IDD+Codes+%26+Local+Times+Worldwide?language=zh_HK";

        final AQuery aq = new AQuery(frag.getActivity());
        if (debug)
            Log.d("getCountryList", String.format("%s %s", "doGetIDDCountries2", "request sent:") + countryUrl);
        progressManager = ProgressManager.getInstance(frag.getActivity());

        final AjaxCallback<XmlDom> cb = new AjaxCallback<>() {
            @Override
            public void callback(String url, XmlDom xml, AjaxStatus status) {
                OnAPIsListener callback;
                APIsResponse response;
                boolean isFailCase = false;
                response = new APIsResponse();
                response.setActionTy(IDDCTY);
                try {
                    try {
                        callback = (OnAPIsListener) frag;
                    } catch (ClassCastException e) {
                        throw new ClassCastException(frag + " must implement OnAPIsListener");
                    }

                    if (status.getCode() != 200) {
                        Log.e("getCountryList", "status code not 200");
                        response.setReply(HTTPSTATUS_NOT_200);
                        response.setMessage(ClnEnv.getRPCErrMsg(frag.getActivity(), status.getCode(), status.getMessage()));
                        callback.onFail(response);
                        isFailCase = true;
                    } else {
                        // Return success check if conurty list is null or not
                        Log.d("getCountryList", "successfully get conurty list" + xml);
                        if (xml != null && xml.tags("country") != null && !xml.tags("country").isEmpty()) {
                            List<XmlDom> countries = xml.tags("country");
                            List<IDDCity> cities;
                            List<IDDCountry> countriesList = new ArrayList<>();
                            for (XmlDom country : countries) {
                                IDDCountry countryItem = new IDDCountry();
                                countryItem.setEname(country.attr("ename"));
                                countryItem.setCname(country.attr("cname"));
                                countryItem.setVal(country.attr("val"));
                                if (debug)
                                    Log.d("getCountryList", String.format("%s %s", "GETCOUNTRYLIST", "countryItem :" + countryItem.getEname()));

                                cities = new ArrayList<>();
                                for (XmlDom city : country.children("city")) {
                                    IDDCity cityItem = new IDDCity();
                                    cityItem.setEname(city.attr("ename"));
                                    cityItem.setCname(city.attr("cname"));
                                    cityItem.setVal(city.attr("val"));
                                    cities.add(cityItem);

                                    if (debug)
                                        Log.d("getCountryList", String.format("%s %s", "GETCOUNTRYLIST", "cityItem :" + cityItem.getEname()));
                                }
                                countryItem.setCity(cities);
                                countriesList.add(countryItem);
                            }
                            if (countriesList.isEmpty()) {
                                //if failed to retrieve country list, exit this page
                                iddCountryError(frag);
                            } else {
                                response.setReply(Reply.getSucc());
                                response.setCra(countriesList);
                                callback.onSuccess(response);
                            }
                        } else {
                            // if failed to retrieve country list, exit this page
                            iddCountryError(frag);
                        }
                    }
                    progressManager.dismissProgressDialog(isFailCase);
                } catch (Exception e) {
                    progressManager.dismissProgressDialog(true);
                    if (debugCra) Log.d("getCountryList", e.toString());
                }
            }
        };

        //Load webview first, on webview finish loading, call
        WebView webview = new WebView(frag.requireActivity());
        webview.getSettings().setJavaScriptEnabled(true);
//        webview.getSettings().setAppCacheEnabled(false);
        webview.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
        webview.setWebChromeClient(new WebChromeClient());
        webview.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                final WebView tView = view;
                Log.d("getCountryList", "onPageFinished with " + url);
                view.evaluateJavascript("(function() { return document.documentElement.outerHTML; })();", html -> {
                    Log.d("getCountryList", html);
                    if (!html.contains("window.location.reload()")) {
                        Log.d("getCountryList", "no reload, call ajax");
                        aq.ajax(countryUrl, XmlDom.class, 1, cb);
                        tView.clearCache(true);
                        tView.clearHistory();
                        tView.destroy();
                    }
                });
            }

            @Override
            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                super.onReceivedError(view, errorCode, description, failingUrl);
                Log.d("onReceivedError", String.format("errorCode:%d description:%s failingUrl:%s", errorCode, description, failingUrl));
                aq.ajax(countryUrl, XmlDom.class, 1, cb);
                view.clearCache(true);
                view.clearHistory();
                view.destroy();
            }
        });
        webview.loadUrl(countryUrl);
    }

    private static void iddCountryError(final Fragment frag) {
        DialogInterface.OnClickListener onPositiveClickListener = (dialog, which) -> {
            frag.requireActivity().finish();
            frag.requireActivity().overridePendingTransition(R.anim.left_slide_in, R.anim.right_slide_out);
        };
        progressManager.dismissProgressDialog(true);
        DialogHelper.createSimpleDialog(frag.getActivity(), frag.requireActivity().getString(R.string.myhkt_countrylist_failed), frag.requireActivity().getString(R.string.btn_ok), onPositiveClickListener);
    }

    public static void doGetIDDCodeResult(final Fragment frag, HashMap<String, String> hmSelectedCodeDetail) {
        Context cxt = frag.getActivity();
        domainURL = ClnEnv.getPref(cxt, Objects.requireNonNull(cxt).getString(R.string.CONST_PREF_DOMAIN), APIsManager.PRD_domain);
        String domain = "http://www.hkt.com/vgn-ext-templating/HKTCorpsite2/Template/IDDCodeResult.jsp?";
        String str = "";

        String local_v1 = Utils.isCountryTaiwan(Objects.requireNonNull(hmSelectedCodeDetail.get("local_v1")));//"China"; //country calling from
        String local_v2 = Utils.isCountryTaiwan(Objects.requireNonNull(hmSelectedCodeDetail.get("local_v2")));//"HONG KONG"; //city calling from
        String dest_v1 = Utils.isCountryTaiwan(Objects.requireNonNull(hmSelectedCodeDetail.get("dest_v1")));//"Denmark"; //country calling from
        String dest_v2 = Utils.isCountryTaiwan(Objects.requireNonNull(hmSelectedCodeDetail.get("dest_v2")));//"Copenhagen"; //city calling to

        String hr = hmSelectedCodeDetail.get("hr");//"11"; // hour calling from
        String min = hmSelectedCodeDetail.get("min");//"52"; // minute calling from
        str = str + "iddCodeDetail.local_v1=" + urlEncode(local_v1);
        str = str + "&iddCodeDetail.local_v2=" + urlEncode(local_v2);
        str = str + "&iddCodeDetail.dest_v1=" + urlEncode(dest_v1);
        str = str + "&iddCodeDetail.dest_v2=" + urlEncode(dest_v2);
        str = str + "&iddCodeDetail.hr=" + urlEncode(hr);
        str = str + "&iddCodeDetail.minute=" + urlEncode(min);

        String url = String.format("%s%s", domain, str);
        AQuery aq = new AQuery(frag.getActivity());

        if (debug)
            Log.d("doGetIDDCodeResult2", String.format("%s %s", "doGetIDDCodeResult2", "request sent:") + url);
        progressManager = ProgressManager.getInstance(frag.getActivity());
        progressManager.showProgressDialog(frag.getActivity());

        aq.ajax(url, XmlDom.class, new AjaxCallback<>() {
            @Override
            public void callback(String url, XmlDom xml, AjaxStatus status) {
                OnAPIsListener callback;
                APIsResponse response;
                boolean isFailCase = false;
                response = new APIsResponse();
                response.setActionTy(IDDCODE);
                try {
                    try {
                        callback = (OnAPIsListener) frag;

                    } catch (ClassCastException e) {
                        throw new ClassCastException(frag + " must implement OnAPIsListener");
                    }

                    if (status.getCode() != 200) {
                        response.setReply(HTTPSTATUS_NOT_200);
                        response.setMessage(ClnEnv.getRPCErrMsg(frag.getActivity(), status.getCode(), status.getMessage()));
                        callback.onFail(response);
                        isFailCase = true;
                    } else if (xml == null) {
                        response.setReply(JSON_RETURN_NULL);
                        response.setMessage(ClnEnv.getRPCErrMsg(frag.getActivity(), status.getCode(), status.getMessage()));
                        callback.onFail(response);
                        isFailCase = true;
                    } else {

                        IDDCodesNTimes codeNTime = new IDDCodesNTimes();
                        if (debug)
                            Log.d("doGetIDDCodeResult2", String.format("%s %s", "doGetIDDCodeResult2", "response receive2:") + xml);

                        codeNTime.setCountryId(xml.text("countryId"));
                        codeNTime.setCountryName(xml.text("countryName"));
                        codeNTime.setCountryCode(xml.text("countryCode"));
                        codeNTime.setAreaCode(xml.text("areaCode"));
                        codeNTime.setTimeDiff(xml.text("timeDiff"));
                        codeNTime.setTime(xml.text("time"));
                        codeNTime.setCountryName2(xml.text("countryName2"));
                        codeNTime.setHr(xml.text("hr"));
                        codeNTime.setMin(xml.text("min"));
                        codeNTime.setTimeDiff_dest(xml.text("timeDiff_dest"));
                        codeNTime.setTimeDiff_local(xml.text("timeDiff_local"));
                        codeNTime.setOtherInfo(xml.text("otherInfo"));
                        if (debug)
                            Log.d("doGetIDDCodeResult2", String.format("%s %s", "doGetIDDCodeResult2", "codeNTime :" + codeNTime.getAreaCode()));

                        response.setReply(Reply.getSucc());
                        response.setCra(codeNTime);
                        callback.onSuccess(response);
                    }
                    progressManager.dismissProgressDialog(isFailCase);
                } catch (Exception e) {
                    progressManager.dismissProgressDialog(true);
                    if (debugCra) Log.d("", e.toString());
                }
            }
        });
    }

    public static void requestAppConfigJSON(Context context, FragmentActivity fragmentActivity) {
        domainURL = ClnEnv.getPref(context, fragmentActivity.getString(R.string.CONST_PREF_FTP), APIsManager.PRD_FTP);

        APIsResponse apIsResponse = new APIsResponse();
        apIsResponse.setActionTy(API_APP_CONFIG);
        OnAPIsListener callback = (OnAPIsListener) fragmentActivity;

        GetDataService service = APIClient.getAppConfigInstance(domainURL.concat("/")).create(GetDataService.class);
        Call<AppConfig> call = service.getAppConfigJSON(domainURL.concat("/mba/data/app_config.json"));
        call.enqueue(new Callback<>() {
            @Override
            public void onResponse(Call<AppConfig> call, Response<AppConfig> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        try {
                            AppConfig appConfig = response.body();
                            String appConfigResponse = gson.toJson(appConfig);
                            if (FileManager.fileWriter(context, appConfigResponse, FILENAME_APP_CONFIG)) {
                                ClnEnv.setPref(context.getApplicationContext(), KEY_SHARED_APP_CONFIG, appConfigResponse);
                                apIsResponse.setCra(appConfig);
                                callback.onSuccess(apIsResponse);
                                ClnEnv.setPref(context.getApplicationContext(), "livechat_consumer_flag", "Y".equalsIgnoreCase(appConfig.getCommonConfig().getLivechat_consumer_flag()));
                                ClnEnv.setPref(context.getApplicationContext(), "livechat_premier_flag", "Y".equalsIgnoreCase(appConfig.getCommonConfig().getLivechat_premier_flag()));
                                if (appConfig.getCommonConfig().getShow_fingerprint_banner() != null) {
                                    ClnEnv.setPref(context.getApplicationContext(), Constant.IS_SERVER_TO_SHOW_FP_BANNER, appConfig.getCommonConfig().getShow_fingerprint_banner());
                                }
                                sendBroadCastMessageToReceiver(context, ACTION_BROADCAST_CONFIG_FINISH_DOWNLOAD);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<AppConfig> call, Throwable t) {

            }
        });
    }

    public static void requestDownloadImage(Context context, FragmentActivity fragmentActivity, AppConfig appConfig, String localRegion) {
        String HOST = ClnEnv.getPref(fragmentActivity, fragmentActivity.getString(R.string.CONST_PREF_FTP), APIsManager.PRD_FTP);
        GetDataService service = APIClient.getAppConfigInstance(HOST.concat("/")).create(GetDataService.class);
        for (AppConfigImageSetting image : appConfig.getImageList()) {
            String localPath = localRegion.equalsIgnoreCase("en") ? image.getLocalFilePathEn() : image.getLocalFilePathZh();
            if (image.getLocalFilePathEn().contains(localRegion)) {
                requestImageCallback(context, service, localPath, image.getLocalFileName(), image.getUrlFileNameEn());
            }
            if (image.getLocalFilePathZh().contains(localRegion)) {
                requestImageCallback(context, service, localPath, image.getLocalFileName(), image.getUrlFileNameZh());
            }
        }
    }

    private static void requestImageCallback(Context context, GetDataService service, String path, String localFileName, String urlFileName) {
        Call<ResponseBody> call = service.getImage(urlFileName);
        call.enqueue(new Callback<>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    File localFile = new File(context.getFilesDir().getPath() + path, localFileName);
                    try {
                        boolean downloadImage;
                        SimpleDateFormat format = new SimpleDateFormat(FORMAT_MODIFIED_DATE, Locale.US);
                        Date apiModifiedDate = format.parse(response.headers().get("Last-Modified"));

                        FileInfo fileInfo = new FileInfo();
                        fileInfo.setFilename(localFileName);
                        fileInfo.setApiDate(apiModifiedDate);

                        if (localFile.exists()) {
                            downloadImage = apiModifiedDate != null && apiModifiedDate.getTime() != localFile.lastModified();
                        } else {
                            downloadImage = true;
                        }

                        if (downloadImage) {
                            if (FileManager.writeResponseBodyToDisk(response.body(), context.getFilesDir().getPath() + path, fileInfo)) {
                                sendBroadCastMessageToReceiver(context, ACTION_BROADCAST_IMAGE_FINISH_DOWNLOAD);
                                Log.d("lwg", "sendBroadCastMessageToReceiver");
                            }
                        }
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }

    // Send broadcast message to receiver to MainActivity
    private static void sendBroadCastMessageToReceiver(Context context, String actionMessage) {
        Intent intent = new Intent();
        intent.setAction(actionMessage);
        intent.setPackage(context.getPackageName());
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.S) {
            PendingIntent.getBroadcast(context, 0, intent, PendingIntent.FLAG_IMMUTABLE | PendingIntent.FLAG_UPDATE_CURRENT);
        } else {
            PendingIntent.getBroadcast(context, 0, intent, PendingIntent.FLAG_CANCEL_CURRENT | PendingIntent.FLAG_IMMUTABLE);
        }
        context.sendBroadcast(intent);
    }

    public static void requestDestinationIDDRate(Fragment frag) {
        FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
        GetDataService service = APIClient.getRetrofitInstance().create(GetDataService.class);

        progressManager = ProgressManager.getInstance(frag.getActivity());
        progressManager.showProgressDialog(frag.getActivity());

        OnAPIsListener callback = (OnAPIsListener) frag;

        APIsResponse apIsResponse = new APIsResponse();
        apIsResponse.setActionTy(IDDRATEDEST);

        Call<Destinations> call = service.getDestinations();
        call.enqueue(new Callback<>() {

            @Override
            public void onResponse(Call<Destinations> call, Response<Destinations> response) {
                try {
                    if (response.isSuccessful()) {
                        if (response.body() != null) {
                            apIsResponse.setReply(Reply.getSucc());
                            apIsResponse.setCra(response.body().getDestination());
                            callback.onSuccess(apIsResponse);
                            Log.d("lwg", "\nOK: " + response.body().toString());
                        } else {
                            apIsResponse.setReply(JSON_RETURN_NULL);
                            apIsResponse.setMessage(ClnEnv.getRPCErrMsg(frag.getActivity(), response.code(), response.message()));
                            callback.onFail(apIsResponse);
                            setFirebaseKeysValuesErrorLogs(crashlytics, call.request().url().toString(), response.code(), response.message(), null);
                        }
                    } else {
                        apIsResponse.setReply(HTTPSTATUS_NOT_200);
                        apIsResponse.setMessage(ClnEnv.getRPCErrMsg(frag.getActivity(), response.code(), response.message()));
                        callback.onFail(apIsResponse);
                        setFirebaseKeysValuesErrorLogs(crashlytics, call.request().url().toString(), response.code(), response.message(), null);
                    }
                } catch (Exception exception) {
                    exception.printStackTrace();
                }
                progressManager.dismissProgressDialog(true);
            }

            @Override
            public void onFailure(Call<Destinations> call, Throwable t) {
                try {
                    Log.d("lwg", "onFailure: " + t.getMessage());
                    apIsResponse.setReply(HTTPSTATUS_NOT_200);
                    apIsResponse.setMessage(ClnEnv.getRPCErrMsg(frag.getActivity(), 500, t.getMessage()));
                    callback.onFail(apIsResponse);
                    setFirebaseKeysValuesErrorLogs(crashlytics, call.request().url().toString(), -1, t.getMessage() != null ? t.getMessage() : "", null);
                } catch (Exception exception) {
                    exception.printStackTrace();
                }
                progressManager.dismissProgressDialog(true);
            }
        });
    }

    /*
    public static void doGetIDDRateDest(final Fragment frag) {
        Context cxt = frag.getActivity();
        domainURL = ClnEnv.getPref(cxt, Objects.requireNonNull(cxt).getString(R.string.CONST_PREF_DOMAIN), APIsManager.PRD_domain);

        String domain = ClnEnv.getPref(cxt, cxt.getString(R.string.CONST_PREF_SHOPHOST), APIsManager.PRD_shop_host);
        String url = String.format("%s%s", domain, IDDRateDestEndpoint);
        AQuery aq = new AQuery(frag.getActivity());
        if (debug)
            Log.d("doGetIDDRateDest", String.format("%s %s", "doGetIDDRateDest", "request sent:") + url);

        progressManager = ProgressManager.getInstance(frag.getActivity());
        progressManager.showProgressDialog(frag.getActivity());
        aq.ajax(url, XmlDom.class, new AjaxCallback<XmlDom>() {
            @Override
            public void callback(String url, XmlDom xml, AjaxStatus status) {
                OnAPIsListener callback;
                APIsResponse response;
                boolean isFailCase = false;
                response = new APIsResponse();
                response.setActionTy(IDDRATEDEST);
                try {
                    try {
                        callback = (OnAPIsListener) frag;

                    } catch (ClassCastException e) {
                        throw new ClassCastException(frag.toString() + " must implement OnAPIsListener");
                    }

                    if (status.getCode() != 200) {
                        response.setReply(HTTPSTATUS_NOT_200);
                        response.setMessage(ClnEnv.getRPCErrMsg(frag.getActivity(), status.getCode(), status.getMessage()));
                        callback.onFail(response);
                        isFailCase = true;
                    } else if (xml == null) {
                        // TODO: httppost response null Object from api
                        response.setReply(JSON_RETURN_NULL);
                        response.setMessage(ClnEnv.getRPCErrMsg(frag.getActivity(), status.getCode(), status.getMessage()));
                        callback.onFail(response);
                        isFailCase = true;
                    } else {
                        if (debug)
                            Utils.showLog("doGetIDDRateDest", String.format("%s %s", "doGetIDDRateDest", "response receive:") + xml.toString());

                        List<XmlDom> nlDestination = xml.children("Destination");
                        List<Destination> destinationList = new ArrayList<>();
                        for (int i = 0; i < nlDestination.size(); i++) {
                            Destination destination = new Destination();
                            destination.engDestName = nlDestination.get(i).text("engDestName");
                            destination.engDestNameExt = nlDestination.get(i).text("engDestNameExt");
                            destination.chiDestName = nlDestination.get(i).text("chiDestName");
                            destination.chiDestNameExt = nlDestination.get(i).text("chiDestNameExt");
                            destinationList.add(destination);
                        }

                        if (destinationList == null) {
                            response.setReply(NULL_OBJECT_CRA);
                            response.setCra(destinationList);
                            callback.onFail(response);
                            isFailCase = true;
                        } else {
                            response.setReply(Reply.getSucc());
                            response.setCra(destinationList);
                            callback.onSuccess(response);
                        }
                    }
                    // dismiss
                    //	            	progressManager = ProgressManager.getInstance(activity);
                    progressManager.dismissProgressDialog(isFailCase);
                } catch (Exception e) {
                    progressManager.dismissProgressDialog(true);
                    if (debugCra) Log.d("", e.toString());
                }
            }
        });
    }
    */

    public static void doGetIDRateResult(final Fragment frag, HashMap<String, String> hmSelectedDestDetail) {
        String domain = ClnEnv.getPref(frag.getActivity(), frag.requireActivity().getString(R.string.CONST_PREF_SHOPHOST), APIsManager.PRD_shop_host);
        String str = "";
        String customernum = hmSelectedDestDetail.get("customernum");//"China"; //country calling from
        String servicenum = hmSelectedDestDetail.get("servicenum");//"HONG KONG"; //city calling from
        String srvtype = hmSelectedDestDetail.get("srvtype");//"Denmark"; //country calling from
        String dest = hmSelectedDestDetail.get("dest");//"Copenhagen"; //city calling to
        String destext = hmSelectedDestDetail.get("destext");//"11"; // hour calling from
        if (destext == null) {
            destext = "";
        }
        if (Utils.isUAT(frag.getActivity())) {
            customernum = "26125891";
            servicenum = "25587108";
        }

        String IDDRateResultEndpoint = "IDD0060WS/rs/idd0060/customer";
        StringBuilder builder = new StringBuilder().append(domain).append(IDDRateResultEndpoint).append("/").append(urlEncode(customernum)).append("/srps?servicenum=").append(urlEncode(servicenum)).append("&srvtype=").append(urlEncode(srvtype)).append("&dest=").append(urlEncode(dest)).append("&destext=").append(urlEncode(destext));

        str = str + "/" + urlEncode(customernum);
        str = str + "/srps?servicenum=" + urlEncode(servicenum);
        str = str + "&srvtype=" + urlEncode(srvtype);
        str = str + "&dest=" + urlEncode(dest);
        str = str + "&destext=" + urlEncode(destext);

        String url = String.format("%s%s%s", domain, IDDRateResultEndpoint, str);
//		String url = String.format("%s%s%s", UAT_domain,IDDRateResultEndpoint, str);
        AQuery aq = new AQuery(frag.getActivity());
        if (debug)
            Log.d("doGetIDRateResult", String.format("%s %s", "doGetIDRateResult", "request sent:") + url);

        progressManager = ProgressManager.getInstance(frag.getActivity());
        progressManager.showProgressDialog(frag.getActivity());
        try {
            aq.ajax(url, XmlDom.class, new AjaxCallback<>() {
                @Override
                public void callback(String url, XmlDom xml, AjaxStatus status) {
                    OnAPIsListener callback;
                    APIsResponse response;
                    boolean isFailCase = false;
                    response = new APIsResponse();
                    response.setActionTy(IDDRATERESULT);
                    try {
                        try {
                            callback = (OnAPIsListener) frag;
                        } catch (ClassCastException e) {
                            throw new ClassCastException(frag + " must implement OnAPIsListener");
                        }

                        if (status.getCode() != 200) {
                            response.setReply(HTTPSTATUS_NOT_200);
                            String msg;
                            int statusCode = status.getCode();
                            if (status.getError() != null) {
                                XmlDom xD = new XmlDom(status.getError());
                                statusCode = -1;
                                msg = xD.getElement().getElementsByTagName("errorCode").item(0).getTextContent();
                            } else {
                                msg = status.getMessage();
                            }
                            response.setMessage(ClnEnv.getRPCErrMsg(frag.getActivity(), statusCode, msg));
                            callback.onFail(response);

                            isFailCase = true;
                        } else if (xml == null) {
                            response.setReply(JSON_RETURN_NULL);
                            response.setMessage(ClnEnv.getRPCErrMsg(frag.getActivity(), status.getCode(), status.getMessage()));
                            callback.onFail(response);
                            isFailCase = true;
                        } else {
                            if (debug)
                                Log.d("doGetIDRateResult", String.format("%s %s", "doGetIDRateResult", "response receive:") + xml);

                            SRPlan srPlan = new SRPlan();
                            srPlan.serviceType = xml.text("serviceType");
                            srPlan.engDestName = xml.text("engDestName");
                            srPlan.engDestNameExt = xml.text("engDestNameExt");
                            srPlan.chiDestName = xml.text("chiDestName");
                            srPlan.chiDestNameExt = xml.text("chiDestNameExt");

                            List<XmlDom> rates = xml.tag("rates").children("Rate");
                            ArrayList<Rate> rateList = new ArrayList<>();
                            for (int i = 0; i < rates.size(); i++) {
                                Rate rate = new Rate();
                                rate.timeZone = rates.get(i).text("timeZone");
                                rate.chiTimeZone = rates.get(i).text("chiTimeZone");
                                rate.timeZoneRemarks = rates.get(i).text("timeZoneRemarks");
                                rate.chiTimeZoneRemarks = rates.get(i).text("chiTimeZoneRemarks");
                                rate.rate = rates.get(i).text("rate");
                                rateList.add(rate);
                            }
                            srPlan.rateList = rateList;

                            if (srPlan == null) {
                                response.setReply(NULL_OBJECT_CRA);
                                response.setCra(srPlan);
                                callback.onFail(response);
                                isFailCase = true;
                            } else {
                                response.setReply(Reply.getSucc());
                                response.setCra(srPlan);
                                callback.onSuccess(response);
                            }
                        }
                        progressManager.dismissProgressDialog(isFailCase);
                    } catch (Exception e) {
                        progressManager.dismissProgressDialog(true);
                        if (debugCra) Log.d("", e.toString());
                    }
                }
            });
        } catch (Exception e) {
            progressManager.dismissProgressDialog(true);
        }
    }

    public static void requestIDRateResult(final Fragment frag, HashMap<String, String> hmSelectedDestDetail) {
        FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
        GetDataService service = APIClient.getRetrofitInstance().create(GetDataService.class);

        OnAPIsListener callback = (OnAPIsListener) frag;
        APIsResponse apIsResponse = new APIsResponse();
        apIsResponse.setActionTy(IDDRATERESULT);

        String customernum = hmSelectedDestDetail.get("customernum");
        String servicenum = hmSelectedDestDetail.get("servicenum");
        String srvtype = hmSelectedDestDetail.get("srvtype");
        String dest = hmSelectedDestDetail.get("dest");
        String destext = hmSelectedDestDetail.get("destext");

        if (destext == null) {
            destext = "";
        }

        if (Utils.isUAT(frag.getActivity())) {
            customernum = "26125891";
            servicenum = "25587108";
        }

        progressManager = ProgressManager.getInstance(frag.getActivity());
        progressManager.showProgressDialog(frag.getActivity());

        Call<SRPlan> call = service.getSrpRate(customernum, servicenum, srvtype, dest, destext);
        call.enqueue(new Callback<>() {
            @Override
            public void onResponse(Call<SRPlan> call, Response<SRPlan> response) {
                try {
                    if (response.isSuccessful()) {
                        if (response.body() != null) {
                            SRPlan srPlan = response.body();
                            srPlan.rateList = response.body().getRates().rateList;

                            apIsResponse.setReply(Reply.getSucc());
                            apIsResponse.setCra(srPlan);
                            callback.onSuccess(apIsResponse);
                        } else {
                            apIsResponse.setReply(JSON_RETURN_NULL);
                            apIsResponse.setMessage(ClnEnv.getRPCErrMsg(frag.getActivity(), response.code(), response.message()));
                            callback.onFail(apIsResponse);
                            setFirebaseKeysValuesErrorLogs(crashlytics, call.request().url().toString(), response.code(), response.message(), null);
                        }
                    } else {
                        apIsResponse.setReply(HTTPSTATUS_NOT_200);
                        apIsResponse.setMessage(ClnEnv.getRPCErrMsg(frag.getActivity(), response.code(), response.message()));
                        callback.onFail(apIsResponse);
                        setFirebaseKeysValuesErrorLogs(crashlytics, call.request().url().toString(), response.code(), response.message(), null);
                    }
                } catch (Exception exception) {
                    exception.printStackTrace();
                }
                progressManager.dismissProgressDialog(true);
            }

            @Override
            public void onFailure(Call<SRPlan> call, Throwable t) {
                try {
                    apIsResponse.setReply(HTTPSTATUS_NOT_200);
                    apIsResponse.setMessage(ClnEnv.getRPCErrMsg(frag.getActivity(), 500, t.getMessage()));
                    callback.onFail(apIsResponse);
                    setFirebaseKeysValuesErrorLogs(crashlytics, call.request().url().toString(), -1, t.getMessage() != null ? t.getMessage() : "", null);
                } catch (Exception exception) {
                    exception.printStackTrace();
                    setFirebaseKeysValuesErrorLogs(crashlytics, call.request().url().toString(), -1, t.getMessage() != null ? t.getMessage() : "", exception);
                }
                progressManager.dismissProgressDialog(true);
            }
        });
    }

    private static String urlEncode(String url) {
        return URLEncoder.encode(url, StandardCharsets.UTF_8);
    }

    private static void setFirebaseKeysValuesErrorLogs(FirebaseCrashlytics crashlytics, String url, int code, String messageError, Exception exception) {
        crashlytics.setCustomKey("myhkt_app_version", BuildConfig.VERSION_NAME + " (" + BuildConfig.VERSION_CODE + ")");
        crashlytics.setCustomKey("service_api_url", url);

        crashlytics.setCustomKey("service_api_error_message", messageError.isEmpty() ? "getMessageError NULL" : messageError);
        crashlytics.setCustomKey("service_api_response_code", code);

        if (exception != null) {
            crashlytics.recordException(exception);
        } else {
            crashlytics.recordException(new Exception("error code: " + code + " \n error message: " + messageError));
        }
    }

    public static void doGetPlanMob(Fragment frag, PlanMobCra mPlanMobCra) {
        Context cxt = frag.getActivity();
        domainURL = ClnEnv.getPref(cxt, Objects.requireNonNull(cxt).getString(R.string.CONST_PREF_DOMAIN), APIsManager.PRD_domain);
        String url = domainURL + "/ma/planMob";
        PlanMobCra planMobCra = mPlanMobCra.copyMe();
        planMobCra.setIApi(PLAN_MOB);
        planMobCra.setIGwtGud(ClnEnv.getSessTok());
        APIsRequest<PlanMobCra> apiRequest = new APIsRequest<>(url, PLAN_MOB);
        apiRequest.setiCra(planMobCra);

        progressManager = ProgressManager.getInstance(frag.getActivity());
        progressManager.showProgressDialog(frag.getActivity());
        doAction(frag.getActivity(), frag, PlanMobCra.class, apiRequest);
    }

    public static void doGetCAppt(IntentService service, CApptCra mCApptCra) {
        domainURL = ClnEnv.getPref(service, service.getString(R.string.CONST_PREF_DOMAIN), APIsManager.PRD_domain);
        String url = domainURL + "/ma/cappt";
        CApptCra cApptCra = mCApptCra.copyMe();
        cApptCra.setIApi(CAPPT);
        APIsRequest<CApptCra> apiRequest = new APIsRequest<>(url, CAPPT);
        apiRequest.setiCra(cApptCra);
        doAction(null, null, service, CApptCra.class, apiRequest, true);
    }

    public static void doRegenActCode(FragmentActivity act, LgiCra mLgiCra) {
        domainURL = ClnEnv.getPref(act, act.getString(R.string.CONST_PREF_DOMAIN), APIsManager.PRD_domain);
        String url = domainURL + "/ma/regenActCd";
        LgiCra lgiCra = mLgiCra.copyMe();
        lgiCra.setIApi(RG_ACTCD);
        APIsRequest<LgiCra> apiRequest = new APIsRequest<>(url, RG_ACTCD);
        apiRequest.setiCra(lgiCra);
        progressManager = ProgressManager.getInstance(act);
        progressManager.showProgressDialog(act);
        doAction(act, null, LgiCra.class, apiRequest);
    }

    public static void doSMSActivation(FragmentActivity act, AcMainCra mAcMainCra) {
        domainURL = ClnEnv.getPref(act, act.getString(R.string.CONST_PREF_DOMAIN), APIsManager.PRD_domain);
        String url = domainURL + "/ma/smsAct";
        AcMainCra acMainCra = mAcMainCra.copyMe();
        acMainCra.setIApi(SMSACT);
        acMainCra.setIBcifRec(null); //must be null for all acMainCra related API call
        APIsRequest<AcMainCra> apiRequest = new APIsRequest<>(url, SMSACT);
        apiRequest.setiCra(acMainCra);
        progressManager = ProgressManager.getInstance(act);
        progressManager.showProgressDialog(act);
        doAction(act, null, AcMainCra.class, apiRequest);
    }

    public static void doTopupHist(Fragment frag, PlanMobCra mPlanMobCra) {
        Context cxt = frag.getActivity();
        domainURL = ClnEnv.getPref(cxt, Objects.requireNonNull(cxt).getString(R.string.CONST_PREF_DOMAIN), APIsManager.PRD_domain);
        String url = domainURL + "/ma/tpupHist";
        PlanMobCra planMobCra = mPlanMobCra.copyMe();
        planMobCra.setIApi(TPUP_HIST);
        planMobCra.setIGwtGud(ClnEnv.getSessTok());
        planMobCra.setITopupActn(PlanMobCra.TP_ACTN_I);
        APIsRequest<PlanMobCra> apiRequest = new APIsRequest<>(url, TPUP_HIST);
        apiRequest.setiCra(planMobCra);
        progressManager = ProgressManager.getInstance(frag.getActivity());
        progressManager.showProgressDialog(frag.getActivity());
        doAction(frag.getActivity(), frag, PlanMobCra.class, apiRequest);
    }

    public static void doTopupItem(Fragment frag, PlanMobCra mPlanMobCra) {
        Context cxt = frag.getActivity();
        domainURL = ClnEnv.getPref(cxt, Objects.requireNonNull(cxt).getString(R.string.CONST_PREF_DOMAIN), APIsManager.PRD_domain);
        String url = domainURL + "/ma/tpup";
        PlanMobCra planMobCra = mPlanMobCra.copyMe();
        planMobCra.setIApi(TPUP);
        planMobCra.setIGwtGud(ClnEnv.getSessTok());
        planMobCra.setITopupActn(PlanMobCra.TP_ACTN_I);
        APIsRequest<PlanMobCra> apiRequest = new APIsRequest<>(url, TPUP);
        apiRequest.setiCra(planMobCra);
        progressManager = ProgressManager.getInstance(frag.getActivity());
        progressManager.showProgressDialog(frag.getActivity());
        doAction(frag.getActivity(), frag, PlanMobCra.class, apiRequest);
    }

    public static void doTopup(Fragment frag, PlanMobCra mPlanMobCra) {
        Context cxt = frag.getActivity();
        domainURL = ClnEnv.getPref(cxt, Objects.requireNonNull(cxt).getString(R.string.CONST_PREF_DOMAIN), APIsManager.PRD_domain);
        String url = domainURL + "/ma/tpup";
        PlanMobCra planMobCra = mPlanMobCra.copyMe();
        planMobCra.setIApi(TPUP);
        planMobCra.setIGwtGud(ClnEnv.getSessTok());
        planMobCra.setITopupActn(PlanMobCra.TP_ACTN_C);
        APIsRequest<PlanMobCra> apiRequest = new APIsRequest<>(url, TPUP);
        apiRequest.setiCra(planMobCra);

        progressManager = ProgressManager.getInstance(frag.getActivity());
        progressManager.showProgressDialog(frag.getActivity());

        doAction(frag.getActivity(), frag, PlanMobCra.class, apiRequest);
    }

    /****************************
     * Plan
     ****************************/
    public static void doGetPlanIMS(Fragment frag, PlanImsCra mPlanImsCra) {
        Context cxt = frag.getActivity();
        domainURL = ClnEnv.getPref(cxt, Objects.requireNonNull(cxt).getString(R.string.CONST_PREF_DOMAIN), APIsManager.PRD_domain);
        String url = domainURL + "/ma/planIms";
        PlanImsCra planImsCra = mPlanImsCra.copyMe();
        planImsCra.setIApi(PLAN_IMS);
        planImsCra.setIGwtGud(ClnEnv.getSessTok());

        APIsRequest<PlanImsCra> apiRequest = new APIsRequest<>(url, PLAN_IMS);
        apiRequest.setiCra(planImsCra);

        progressManager = ProgressManager.getInstance(frag.getActivity());
        progressManager.showProgressDialog(frag.getActivity());

        doAction(frag.getActivity(), frag, PlanImsCra.class, apiRequest);
    }

    public static void doGetPlanLts(Fragment frag, PlanLtsCra mPlanLtsCra) {
        Context cxt = frag.getActivity();
        domainURL = ClnEnv.getPref(cxt, Objects.requireNonNull(cxt).getString(R.string.CONST_PREF_DOMAIN), APIsManager.PRD_domain);
        String url = domainURL + "/ma/planLts";
        PlanLtsCra planLtsCra = mPlanLtsCra.copyMe();
        planLtsCra.setIApi(PLAN_LTS);
        planLtsCra.setIGwtGud(ClnEnv.getSessTok());

        APIsRequest<PlanLtsCra> apiRequest = new APIsRequest<>(url, PLAN_LTS);
        apiRequest.setiCra(planLtsCra);

        progressManager = ProgressManager.getInstance(frag.getActivity());
        progressManager.showProgressDialog(frag.getActivity());

        doAction(frag.getActivity(), frag, PlanLtsCra.class, apiRequest);
    }

    public static void doEnquireSR4NoPend(Fragment frag, ApptCra mApptCra) {
        Context cxt = frag.getActivity();
        domainURL = ClnEnv.getPref(cxt, Objects.requireNonNull(cxt).getString(R.string.CONST_PREF_DOMAIN), APIsManager.PRD_domain);
        String url = domainURL + "/ma/srEnqNP";
        ApptCra apptCra = mApptCra.copyMe();
        apptCra.setIApi(SR_ENQ_NP);
        apptCra.setIGwtGud(ClnEnv.getSessTok());

        APIsRequest<ApptCra> apiRequest = new APIsRequest<>(url, SR_ENQ_NP);
        apiRequest.setiCra(apptCra);

        progressManager = ProgressManager.getInstance(frag.getActivity());
        progressManager.showProgressDialog(frag.getActivity());

        doAction(frag.getActivity(), frag, ApptCra.class, apiRequest);
    }

    /*************************************
     * Line Test
     * ***********************************/
    public static void doSubmitLntt(IntentService service, LnttCra mLnttCra) {
        domainURL = ClnEnv.getPref(service, service.getString(R.string.CONST_PREF_DOMAIN), APIsManager.PRD_domain);
        String url = domainURL + "/ma/lntt";
        LnttCra lnttCra = mLnttCra.copyMe();
        lnttCra.setIApi(LNTT);
        lnttCra.setIGwtGud(ClnEnv.getSessTok());

        APIsRequest<LnttCra> apiRequest = new APIsRequest<>(url, LNTT);
        apiRequest.setiCra(lnttCra);

        doAction(null, null, service, LnttCra.class, apiRequest, true);
    }

    public static void doResetModem(IntentService service, LnttCra mLnttCra) {
        domainURL = ClnEnv.getPref(service, service.getString(R.string.CONST_PREF_DOMAIN), APIsManager.PRD_domain);
        String url = domainURL + "/ma/rtrt";
        LnttCra lnttCra = mLnttCra.copyMe();
        lnttCra.setIApi(RTRT);
        lnttCra.setIGwtGud(ClnEnv.getSessTok());

        APIsRequest<LnttCra> apiRequest = new APIsRequest<>(url, RTRT);
        apiRequest.setiCra(lnttCra);

        doAction(null, null, service, LnttCra.class, apiRequest, true);
    }


    public static void doRebootModem(Fragment fragment, LnttCra mLnttCra) {
        Context cxt = fragment.getActivity();
        domainURL = ClnEnv.getPref(cxt, Objects.requireNonNull(cxt).getString(R.string.CONST_PREF_DOMAIN), APIsManager.PRD_domain);
        String url = domainURL + "/ma/rbmd";
        LnttCra lnttCra = mLnttCra.copyMe();
        lnttCra.setIApi(RBMD);
        lnttCra.setIGwtGud(ClnEnv.getSessTok());
        lnttCra.setILoginId(ClnEnv.getLoginId());

        APIsRequest<LnttCra> apiRequest = new APIsRequest<>(url, RBMD);
        apiRequest.setiCra(lnttCra);

        progressManager = ProgressManager.getInstance(cxt);
        progressManager.showProgressDialog(cxt);

        doAction(null, fragment, null, LnttCra.class, apiRequest, true);
    }

    public static void doGetAvailableTimeSlot(final Fragment frag, ApptCra mApptCra) {
        Context cxt = frag.getActivity();
        domainURL = ClnEnv.getPref(cxt, Objects.requireNonNull(cxt).getString(R.string.CONST_PREF_DOMAIN), APIsManager.PRD_domain);
        String url = domainURL + "/ma/srAvaTs";
        ApptCra apptCra = mApptCra.copyMe();
        apptCra.setIApi(SR_AVA_TS);
        apptCra.setIGwtGud(ClnEnv.getSessTok());

        APIsRequest<ApptCra> apiRequest = new APIsRequest<>(url, SR_AVA_TS);
        apiRequest.setiCra(apptCra);

        progressManager = ProgressManager.getInstance(frag.getActivity());
        progressManager.showProgressDialog(frag.getActivity());

        doAction(frag.getActivity(), frag, ApptCra.class, apiRequest);
    }

    public static void doCheckUpdate(final IntentService service) {
        domainURL = ClnEnv.getPref(service, service.getString(R.string.CONST_PREF_FTP), APIsManager.PRD_FTP);
        String url = String.format("%s%s", domainURL, "/mba/data/myhkt_version_android.txt");
        AQuery aq = new AQuery(service.getApplicationContext());
        try {
            AjaxCallback<JSONObject> ajaxCallback = new AjaxCallback<>() {
                OnAPIsListener callback;
                APIsResponse response;
                VersionCheck cra;

                @Override
                public void callback(String url, JSONObject json, AjaxStatus status) {
                    try {
                        try {
                            callback = (OnAPIsListener) service;
                        } catch (ClassCastException e) {
                            throw new ClassCastException(service + " must implement OnAPIsListener");
                        }
                        response = new APIsResponse();
                        if (status.getCode() != 200) {
                            response.setReply(HTTPSTATUS_NOT_200);
                            response.setMessage(ClnEnv.getRPCErrMsg(service.getBaseContext(), status.getCode(), status.getMessage()));
                            callback.onFail(response);
                        } else if (json == null) {
                            // TODO: httppost response null Object from api
                            response.setReply(JSON_RETURN_NULL);
                            response.setMessage(ClnEnv.getRPCErrMsg(service.getBaseContext(), status.getCode(), status.getMessage()));
                            callback.onFail(response);
                        } else {
                            String resultStr = json.toString();
                            if (debugCra)
                                ClnEnv.toLogFile(String.format("%s %s", "VersionCheck", "response received"), resultStr);
                            if (debugCra)
                                Log.d("VersionCheck", String.format("%s %s", "VersionCheck", "response received:") + resultStr);
                            try {
                                cra = gson.fromJson(resultStr, VersionCheck.class);
                                response.setCra(cra);
                                callback.onSuccess(response);
                            } catch (Exception e) {
                                if (debug)
                                    Log.e("VersionCheck", "/gson.fromJson error: " + e.getMessage());
                                response.setReply("Unexpected Error");
                                response.setMessage(ClnEnv.getRPCErrMsg(service.getBaseContext(), "Unexpected Error"));
                                callback.onFail(response);
                                e.printStackTrace();
                            }
                        }
                    } catch (Exception e) {
                        progressManager.dismissProgressDialog(true);
                        if (debugCra) Log.d("", e.toString());
                    }
                }
            };
            /* //Here we are setting the cookie info.
            for (Cookie cookie : mCookies) {
                ajaxCallback.cookie(cookie.getName(), cookie.getValue());
            }
            */
            aq.ajax(url, null, JSONObject.class, ajaxCallback);
        } catch (Exception e) {
            e.printStackTrace();
            OnAPIsListener callback;
            try {
                if (service != null) {
                    callback = (OnAPIsListener) service;

                    APIsResponse response;
                    response = new APIsResponse();
                    response.setReply("Unexpected Error");
                    response.setMessage(ClnEnv.getRPCErrMsg(service.getBaseContext(), "Unexpected Error"));
                    callback.onFail(response);
                }
            } catch (ClassCastException e2) {
                throw new ClassCastException(service + " must implement OnAPIsListener");
            } catch (Exception e3) {
                e3.printStackTrace();
            }
        }
    }

    public static void doAppendUserInfo(FragmentActivity act, AcMainCra mAcMainCra) {
        domainURL = ClnEnv.getPref(act, act.getString(R.string.CONST_PREF_DOMAIN), APIsManager.PRD_domain);
        String url = domainURL + "/ma/eclosion";
        AcMainCra acMainCra = mAcMainCra.copyMe();
        acMainCra.setIApi(ECLOSION);
        acMainCra.setIGwtGud(ClnEnv.getSessTok());
        acMainCra.setIBcifRec(null); //must be null for all acMainCra related API call
        APIsRequest<AcMainCra> apiRequest = new APIsRequest<>(url, ECLOSION);
        apiRequest.setiCra(acMainCra);
        doAction(act, null, AcMainCra.class, apiRequest);
    }

    public static void walletRequest(final FragmentActivity activity, WalletCra wallet) {
        domainURL = ClnEnv.getPref(activity, activity.getString(R.string.CONST_PREF_DOMAIN), APIsManager.PRD_domain);
        String url = domainURL + "/ma/eWalletJWT";
        APIsRequest<WalletCra> apiRequest = new APIsRequest<>(url, WALLET_EW_JWT);
        apiRequest.setiCra(wallet);
        doWallet(activity, null, apiRequest);
    }

    private static void doWallet(FragmentActivity activity, Fragment frag, APIsRequest<WalletCra> apiRequest) {
        Context cxt = activity != null ? activity : frag.getActivity();
        final AQuery aq = new AQuery(activity);
        final String TAGNAME = "wallet";

        try {
            progressManager.showProgressDialog(activity);
            WalletCra wallet = apiRequest.getiCra();
            String walletParams = gson.toJson(wallet);

            if (debugGrq)
                ClnEnv.toLogFile(String.format("%s %s", TAGNAME, "request sent"), walletParams);
            if (debugCra)
                Log.d(TAGNAME, String.format("%s %s", TAGNAME, "request sent:") + walletParams);

            HttpEntity entity = new StringEntity(walletParams, "UTF-8");
            Map<String, Object> params = new HashMap<>();
            params.put(AQuery.POST_ENTITY, entity);

            AjaxCallback<JSONObject> ajaxCallback = new AjaxCallback<>() {
                boolean isFailCase = false;
                OnAPIsListener callback;
                APIsResponse response;
                WalletCra cra;

                @Override
                public void callback(String url, JSONObject json, AjaxStatus status) {
                    try {
                        try {
                            if (frag != null) {
                                callback = (OnAPIsListener) frag;
                            } else {
                                callback = (OnAPIsListener) activity;
                            }
                        } catch (ClassCastException e) {
                            throw new ClassCastException(activity.toString() + " must implement OnAPIsListener");
                        }

                        response = new APIsResponse();
                        response.setActionTy(WALLET_EW_JWT);
                        cra = new WalletCra();

                        if (status.getCode() != 200 || json == null) {
                            response.setReply(HTTPSTATUS_NOT_200);
                            response.setMessage(Objects.requireNonNull(cxt).getString(R.string.EW_JWT_ERR));
                            callback.onFail(response);
                            isFailCase = true;
                        } else {
                            String resultStr = json.toString();
                            if (debugCra)
                                ClnEnv.toLogFile(String.format("%s %s %s", TAGNAME, cra.getClass().getSimpleName(), "response received"), resultStr);
                            if (debugCra)
                                Log.d(TAGNAME, String.format("%s %s %s", TAGNAME, cra.getClass().getSimpleName(), "response received:") + resultStr);

                            try {
                                Type collectionType = new TypeToken<WalletCra>() {
                                }.getType();
                                cra = gson.fromJson(json.toString(), collectionType);

                                response.setActionTy(WALLET_EW_JWT);
                                response.setReply(cra.getReply());
                                response.setCra(cra);
                                callback.onSuccess(response);
                            } catch (Exception e) {
                                if (debug)
                                    Log.e(TAGNAME, "/gson.fromJson error: " + e.getMessage());
                                e.printStackTrace();
                            }

                            if (cra == null || cra.getReply() == null) {
                                if (cra == null) {
                                    cra = new WalletCra();
                                    cra.setReply(NULL_OBJECT_CRA);
                                }
                                response.setReply(cra.getReply());
                                response.setCra(cra);
                                callback.onFail(response);
                                isFailCase = true;
                            }
                        }
                        progressManager.dismissProgressDialog(isFailCase);
                    } catch (Exception e) {
                        if (progressManager != null) {
                            progressManager.dismissProgressDialog(true);
                        }
                    }
                }
            };

            // Here we are setting the cookie info.
            if (mCookies != null && !mCookies.isEmpty()) {
                for (Cookie cookie : mCookies) {
                    ajaxCallback.cookie(cookie.getName(), cookie.getValue());
                }
            }
            ajaxCallback.timeout(60000);
            aq.ajax(apiRequest.getPath(), params, JSONObject.class, ajaxCallback);

        } catch (Exception e1) {
            e1.printStackTrace();
            progressManager.dismissProgressDialog(true);
        }
    }

    public static <T extends BaseCraEx> void doHeloOptional(final FragmentActivity activity, final Fragment frag, final Class<T> typeClass, final APIsRequest<T> apiRequest) {
        if (ClnEnv.getSessTok() != null && !"".equals(ClnEnv.getSessTok())) {
            doAction(activity, frag, typeClass, apiRequest);
        } else {
            doHelo(activity, frag, typeClass, apiRequest);
        }
    }

    public static <T extends BaseCraEx> void doHeloOptional(final FragmentActivity activity, final Fragment frag, final Class<T> typeClass, final APIsRequest<T> apiRequest, final Boolean isLastInLoop) {
        if (ClnEnv.getSessTok() != null && !"".equals(ClnEnv.getSessTok())) {
            doAction(activity, frag, typeClass, apiRequest, isLastInLoop);
        } else {
            doHelo(activity, frag, typeClass, apiRequest, isLastInLoop);
        }
    }

    public static <T extends BaseCraEx> void doAction(final FragmentActivity activity, final Fragment frag, final Class<T> typeClass, final APIsRequest<T> apiRequest) {
        doAction(activity, frag, null, typeClass, apiRequest, true);
    }

    public static <T extends BaseCraEx> void doAction(final FragmentActivity activity, final Fragment frag, final Class<T> typeClass, final APIsRequest<T> apiRequest, final Boolean isLastInLoop) {
        doAction(activity, frag, null, typeClass, apiRequest, isLastInLoop);
    }

    private static <T extends BaseCraEx> void doAction(final FragmentActivity activity, final Fragment frag, final IntentService service, final Class<T> typeClass, final APIsRequest<T> apiRequest, final Boolean isLastinLoop) {
        if (service != null) {
            initDebugMode(service.getApplicationContext());
        } else if (frag != null) {
            initDebugMode(frag.requireActivity());
        } else {
            initDebugMode(activity);
        }
        AQuery aq = new AQuery(service != null ? service.getApplicationContext() : activity);
        String url = apiRequest.getPath();

        T icra = apiRequest.getiCra();
        icra.setIGwtGud(ClnEnv.getSessTok());
        String jstr;
        if (icra instanceof CtacCra || icra instanceof AcMainCra) {
            jstr = gson2.toJson(icra);
        } else {
            jstr = gson.toJson(icra);
        }
        Gson gsonPrint = new GsonBuilder().setPrettyPrinting().create();
        if (progressManager != null) {
            progressManager.checkNull();
        }
//        if (debugGrq)
//            ClnEnv.toLogFile(String.format("%s %s", apiRequest.getActionTy(), "request sent"), icra.toString());
        if (debugCra)
            Utils.showLog((service != null ? service.getClass().toString() : (frag != null ? frag.getClass().toString() : activity.getClass().toString())) + ">>" + apiRequest.getActionTy(), url + "/" + String.format("%s %s", apiRequest.getActionTy(), "request sent:") + gsonPrint.toJson(icra));

        try {
            HttpEntity entity = new StringEntity(jstr, "UTF-8");
            Map<String, Object> params = new HashMap<>();
            params.put(AQuery.POST_ENTITY, entity);
            if (progressManager != null) {
                progressManager.checkNull();
            }
            AjaxCallback<JSONObject> ajaxCallback = new AjaxCallback<>() {
                OnAPIsListener callback;
                APIsResponse response;
                T cra;

                @Override
                public void callback(String url, JSONObject json, AjaxStatus status) {
                    boolean isFailCase = false;
                    Context cxt = null;
                    if (progressManager != null) {
                        progressManager.checkNull();
                    }

                    try {
                        try {
                            if (service != null) {
                                callback = (OnAPIsListener) service;
                                cxt = service.getBaseContext();
                            } else if (frag != null) {
                                callback = (OnAPIsListener) frag;
                                cxt = frag.getActivity();
                            } else {
                                callback = (OnAPIsListener) activity;
                                cxt = activity;
                            }
                        } catch (ClassCastException e) {
                            throw new ClassCastException((service != null ? service.toString() : cxt.toString()) + " must implement OnAPIsListener");
                        }
                        Objects.requireNonNull(progressManager).checkNull();
                        response = new APIsResponse();
                        response.setActionTy(apiRequest.getActionTy());
                        try {
                            cra = typeClass.newInstance();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        if (status.getCode() != 200) {
                            response.setReply(HTTPSTATUS_NOT_200);
                            if (status.getCode() == 500) {
                                if (ClnEnv.isLoggedIn()) {
                                    if (debug) Log.i("APIManager", "Login 500");
                                    response.getReply().setCode(RC.TCSESS_MM);
                                } else {
                                    if (debug) Log.i("APIManager", "No Login 500");
                                    response.getReply().setCode(RC.TCSESS_EXP);
                                }
                                ClnEnv.setHeloCra(null);
                                isFailCase = true;
                                if (progressManager != null) {
                                    progressManager.dismissProgressDialog(isFailCase);
                                }
                                callback.onFail(response);
                            } else {
                                response.setMessage(ClnEnv.getRPCErrMsg(cxt, status.getCode(), status.getMessage()));
                                callback.onFail(response);
                                isFailCase = true;
                            }
                        } else if (json == null) {
                            response.setReply(JSON_RETURN_NULL);
                            response.setMessage(ClnEnv.getRPCErrMsg(cxt, status.getCode(), status.getMessage()));
                            callback.onFail(response);
                            isFailCase = true;
                        } else {
                            if (progressManager != null) {
                                progressManager.checkNull();
                            }
                            String resultStr = json.toString();
                            //for API testing, modify if needed.
                            resultStr = sampleData(cxt, apiRequest, resultStr);

                            //special string handling for DQ
                            if (DQRY.equalsIgnoreCase(apiRequest.getActionTy()))
                                resultStr = resultStr.replaceAll("簕", "滙").replaceAll("𥝲", "鰂").replaceAll("𡧛", "邨").replaceAll("𦲁", "劵");

//                            if (debugCra)
//                                ClnEnv.toLogFile(String.format("%s %s %s", apiRequest.getActionTy(), cra.getClass().getSimpleName(), "response received"), resultStr);
                            if (debugCra)
                                Utils.showLog(apiRequest.getActionTy(), String.format("%s %s %s", apiRequest.getActionTy(), cra.getClass().getSimpleName(), "response received:") + resultStr);

                            try {
                                cra = gson.fromJson(resultStr, typeClass); //debug
                                if (debugCra)
                                    Utils.showLog(apiRequest.getActionTy(), String.format("%s %s %s", apiRequest.getActionTy(), cra.getClass().getSimpleName(), "response received:") + gsonPrint.toJson(cra));
                            } catch (Exception e) {
                                if (debug)
                                    Log.e(apiRequest.getActionTy() + "/gson.fromJson error:", e.getMessage());
                                e.printStackTrace();
                            }

                            if (cra == null || !cra.getReply().isSucc()) {
                                if (cra == null) {
                                    try {
                                        cra = typeClass.newInstance();
                                        cra.setReply(NULL_OBJECT_CRA);
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }
                                /*
                                This business logic is not implemented in iOS.
                                SVN Revision : 744 - committed this revision.

                                //Except the following case , other need to turn RC_ALT to RC_RC_TCESS_MM
                                //Force Relogin
                                //1		.Read Profile contact
                                //2		.Update Profile contact
                                //3		.Update Profile / Change PWD
                                //4		.Update Alias /Service List on Profile Page
                                //5		.Update Alias on Service List
                                if (RC.ALT.equals(cra.getReply().getCode())) {
                                    if (cra.getIApi().equals(APIsManager.READ_CTAC) ||
                                            cra.getIApi().equals(APIsManager.UPD_CTAC) ||
                                            cra.getIApi().equals(APIsManager.ACMAIN) ||
                                            cra.getIApi().equals(APIsManager.SVCASO) ||
                                            cra.getIApi().equals(APIsManager.SVCASO4SUBN)) {

                                    } else {
                                        cra.setReply(RC.TCSESS_MM);
                                    }

                                }
                                */
                                response.setReply(cra.getReply());
                                response.setCra(cra);
                                callback.onFail(response);
                                isFailCase = true;
                            } else {
                                if (progressManager != null) {
                                    progressManager.checkNull();
                                }
                                response.setReply(cra.getReply());
                                response.setCra(cra);
                                callback.onSuccess(response);
                            }
                        }
                        if (progressManager != null) {
                            progressManager.checkNull();
                        }
                        if (APIsManager.AO_ADDON.equals(response.getActionTy()) && !isFailCase) {
                            if (progressManager != null) {
                                progressManager.popAsyncCount();
                            }
                        } else if (APIsManager.CHK_BILL.equals(response.getActionTy()) && isFailCase) {
                            if (isLastinLoop) {
                                if (progressManager != null) {
                                    progressManager.dismissProgressDialog(isFailCase);
                                }
                            } else {
                                if (progressManager != null) {
                                    progressManager.popAsyncCount();
                                }
                            }
                        } else {
                            if (progressManager != null) {
                                progressManager.dismissProgressDialog(isFailCase);
                            }
                        }
                    } catch (Exception e) {
                        if (progressManager != null) {
                            progressManager.dismissProgressDialog(true);
                        }
                        if (debugCra) Log.d("", e.toString());
                    }
                }
            };

            // Here we are setting the cookie info.
            if (mCookies != null && !mCookies.isEmpty()) {
                for (Cookie cookie : mCookies) {
                    ajaxCallback.cookie(cookie.getName(), cookie.getValue());
                }
            }
            ajaxCallback.timeout(60000);
            aq.ajax(url, params, JSONObject.class, ajaxCallback);
        } catch (Exception e) {
            if (progressManager != null) {
                progressManager.dismissProgressDialog(true);
            }
            OnAPIsListener callback;
            try {
                if (service != null) {
                    callback = (OnAPIsListener) service;
                } else if (frag != null) {
                    callback = (OnAPIsListener) frag;
                } else {
                    callback = (OnAPIsListener) activity;
                }
                APIsResponse response;
                response = new APIsResponse();
                response.setActionTy(apiRequest.getActionTy());
                response.setReply(e.toString());
                response.setMessage(ClnEnv.getRPCErrMsg(activity, e.toString()));
                callback.onFail(response);
            } catch (ClassCastException e2) {
                throw new ClassCastException((service != null ? service.toString() : activity.toString()) + " must implement OnAPIsListener");
            } catch (Exception e3) {
                if (debugCra) Log.d("", e3.toString());
            }
        }
    }

    public static <T extends BaseCraEx> void doHelo(final FragmentActivity activity, final Fragment frag, final Class<T> typeClass, final APIsRequest<T> apiRequest) {
        doHelo(activity, frag, typeClass, apiRequest, true);
    }

    public static void onDeleteAccountRequest(final Fragment fragment, String logId, int rId, String sessionToken, APIsManager.OnAPIsListener callback) {
        Context context = fragment.getContext();
        domainURL = ClnEnv.getPref(context, Objects.requireNonNull(context).getString(R.string.CONST_PREF_DOMAIN), APIsManager.PRD_domain);
        String url = domainURL + "/ma/delLoginId";

        DeleteRequest deleteRequest = new DeleteRequest();
        deleteRequest.setIApi(DEL_ACCOUNT);
        deleteRequest.setiLoginId(logId);
        deleteRequest.setiSveeRid(rId);
        deleteRequest.setIGwtGud(sessionToken);

        APIsRequest<DeleteRequest> apiRequest = new APIsRequest<>(url, DEL_ACCOUNT);
        apiRequest.setiCra(deleteRequest);

        deleteAccount(fragment, DeleteRequest.class, apiRequest, callback);
    }

    public static <T extends BaseCraEx> void deleteAccount(final Fragment fragment, final Class<T> typeClass, final APIsRequest<T> apiRequest, APIsManager.OnAPIsListener callback) {
        final AQuery aq = new AQuery(fragment.getActivity());
        FragmentActivity fragmentActivity = fragment.getActivity();

        try {
            progressManager.showProgressDialog(fragment.getActivity());
            String jstr = gson.toJson(apiRequest.getiCra());
            HttpEntity entity = new StringEntity(jstr, "UTF-8");
            Map<String, Object> params = new HashMap<>();
            params.put(AQuery.POST_ENTITY, entity);

            AjaxCallback<JSONObject> ajaxCallback = new AjaxCallback<>() {
                APIsResponse response;
                DeleteCra cra;

                @Override
                public void callback(String url, JSONObject object, AjaxStatus status) {
                    response = new APIsResponse();
                    response.setActionTy(DEL_ACCOUNT);
                    cra = new DeleteCra();
                    try {
                        cra = gson.fromJson(object.toString(), DeleteCra.class);
                        if (status.getCode() != 200) {
                            response.setReply(HTTPSTATUS_NOT_200);
                            response.setMessage(ClnEnv.getRPCErrMsg(fragmentActivity, status.getCode(), status.getMessage()));
                            callback.onFail(response);
                        } else {
                            if ("RC_SUCC".equalsIgnoreCase(cra.getReply().getCode())) {
                                response.setReply(cra.getReply());
                                response.setMessage(ClnEnv.getRPCErrMsg(fragmentActivity, status.getCode(), status.getMessage()));
                                callback.onSuccess(response);
                            } else {
                                response.setMessage(ClnEnv.getRPCErrMsg(fragmentActivity, cra.getReply().getCode()));
                                response.setReply(cra.getReply());
                                callback.onFail(response);
                            }
                        }
                        progressManager.dismissProgressDialog(true);
                    } catch (Exception e) {
                        e.printStackTrace();
                        Log.d("lwg", "Exception inner: " + e.getMessage());
                        try {
                            response.setMessage(ClnEnv.getRPCErrMsg(fragmentActivity, status.getCode(), status.getMessage()));
                            callback.onFail(response);
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }
                    }
                    progressManager.dismissProgressDialog(true);
                }
            };

            // Here we are setting the cookie info.
            for (Cookie cookie : mCookies) {
                ajaxCallback.cookie(cookie.getName(), cookie.getValue());
            }
            aq.ajax(apiRequest.getPath(), params, JSONObject.class, ajaxCallback);

        } catch (Exception e) {
            e.printStackTrace();
            Log.d("lwg", "Exception end: " + e.getMessage());
            progressManager.dismissProgressDialog(true);
        }
    }

    public static <T extends BaseCraEx> void doHelo(final FragmentActivity activity, final Fragment frag, final Class<T> typeClass, final APIsRequest<T> apiRequest, final Boolean isLastInLoop) {
        Context cxt = activity != null ? activity : frag.getActivity();
        final AQuery aq = new AQuery(activity);
        // doHello first
        final String TAGNAME = HELO;
        domainURL = ClnEnv.getPref(cxt, Objects.requireNonNull(cxt).getString(R.string.CONST_PREF_DOMAIN), APIsManager.PRD_domain);
        String url = domainURL + "/ma/helo";
        HeloCra heloCra = new HeloCra();
        heloCra.setIApi(HELO);

        // including spssCra on every doHello
        SpssCra spssCra = new SpssCra();

        spssCra.setICkSum(Utils.sha256(ClnEnv.getPref(cxt.getApplicationContext(), cxt.getString(R.string.CONST_PREF_GCM_REGID), "") + "A", ClnEnv.getPref(cxt, cxt.getString(R.string.CONST_PREF_SALT), APIsManager.PRD_salt)));
        if (ClnEnv.isLoggedIn() || typeClass == LgiCra.class) {
            if (typeClass == LgiCra.class) {
                LgiCra lgicra = (LgiCra) apiRequest.getiCra();
                spssCra.setISpssRec(lgicra.getISpssRec());
                spssCra.setILoginId(lgicra.getISveeRec().loginId);
            } else if (ClnEnv.isLoggedIn()) {
                spssCra.setISpssRec(ClnEnv.getLgiCra().getISpssRec());
                spssCra.setILoginId(ClnEnv.getLoginId());
            }
        } else {
            // if not doing login or save login
            spssCra.getISpssRec().devTy = SpssRec.TY_ANDROID;
            spssCra.getISpssRec().devId = ClnEnv.getPref(cxt, cxt.getString(R.string.CONST_PREF_GCM_REGID), "");

            spssCra.getISpssRec().lang = ClnEnv.getAppLocale(cxt);
            spssCra.getISpssRec().gni = ClnEnv.getPref(cxt, cxt.getString(R.string.CONST_PREF_GENNOTI_FLAG), "Y");
        }
        heloCra.setISpssCra(spssCra);

        String jstr = gson.toJson(heloCra);
        if (debugGrq) ClnEnv.toLogFile(String.format("%s %s", TAGNAME, "request sent"), jstr);
        if (debugCra) Log.d(TAGNAME, String.format("%s %s", TAGNAME, "request sent:") + jstr);

        try {
            // 1st request
            progressManager.showProgressDialog(activity);
            // 1st call doHello
            HttpEntity entity = new StringEntity(jstr, "UTF-8");
            Map<String, Object> params = new HashMap<>();
            params.put(AQuery.POST_ENTITY, entity);

            aq.ajax(url, params, JSONObject.class, new AjaxCallback<>() {

                OnAPIsListener callback;
                APIsResponse response;
                HeloCra cra;

                @Override
                public void callback(String url, JSONObject json, AjaxStatus status) {
                    boolean isFailCase = false;
                    mCookies = status.getCookies();
                    try {
                        try {
                            if (frag != null) {
                                callback = (OnAPIsListener) frag;
                            } else {
                                callback = (OnAPIsListener) activity;
                            }
                        } catch (ClassCastException e) {
                            throw new ClassCastException(Objects.requireNonNull(activity) + " must implement OnAPIsListener");
                        }

                        response = new APIsResponse();
                        response.setActionTy(HELO);
                        cra = new HeloCra();
                        if (status.getCode() != 200) {
                            response.setReply(HTTPSTATUS_NOT_200);
                            response.setMessage(ClnEnv.getRPCErrMsg(activity, status.getCode(), status.getMessage()));
                            callback.onFail(response);
                            isFailCase = true;
                        } else if (json == null) {
                            response.setReply(JSON_RETURN_NULL);
                            response.setMessage(ClnEnv.getRPCErrMsg(activity, status.getCode(), status.getMessage()));
                            callback.onFail(response);
                            isFailCase = true;
                        } else {
                            String resultStr = json.toString();
                            if (debugCra)
                                ClnEnv.toLogFile(String.format("%s %s %s", TAGNAME, cra.getClass().getSimpleName(), "response received"), resultStr);
                            if (debugCra)
                                Log.d(TAGNAME, String.format("%s %s %s", TAGNAME, cra.getClass().getSimpleName(), "response received:") + resultStr);

                            try {
                                Type collectionType = new TypeToken<HeloCra>() {
                                }.getType();
                                cra = gson.fromJson(json.toString(), collectionType);
                            } catch (Exception e) {
                                if (debug)
                                    Log.e(TAGNAME, "/gson.fromJson error: " + e.getMessage());
                                e.printStackTrace();
                            }

                            if (cra == null || !cra.getReply().isSucc()) {
                                if (cra == null) {
                                    cra = new HeloCra();
                                    cra.setReply(NULL_OBJECT_CRA);
                                }
                                response.setReply(cra.getReply());
                                response.setCra(cra);
                                callback.onFail(response);
                                isFailCase = true;
                            } else {
                                //set mobile number prefix
                                String mobPfx = cra.getOClnCfg().getProp().get("CLN_MOB_PFX");
                                String ltsPfx = cra.getOClnCfg().getProp().get("CLN_LTS_PFX");
                                if (mobPfx != null && !mobPfx.isEmpty())
                                    ClnEnv.setPref(activity, "mobPfx", mobPfx);
                                if (ltsPfx != null && !ltsPfx.isEmpty())
                                    ClnEnv.setPref(activity, "ltsPfx", ltsPfx);

                                ClnEnv.setHeloCra(cra);
                                // 2nd call
                                doAction(activity, frag, typeClass, apiRequest, isLastInLoop);
                            }
                        }
                        progressManager.dismissProgressDialog(isFailCase);
                    } catch (Exception e) {
                        progressManager.dismissProgressDialog(true);
                        if (debugCra) Log.d("", e.toString());
                    }
                }
            });
        } catch (Exception e1) {
            e1.printStackTrace();
            progressManager.dismissProgressDialog(true);
        }
    }

    // Settings
    public static void doUpdSmph(final FragmentActivity activity, SpssCra rSpssCra) {
        domainURL = ClnEnv.getPref(activity, activity.getString(R.string.CONST_PREF_DOMAIN), APIsManager.PRD_domain);
        String url = domainURL + "/ma/spssUpd";
        SpssCra spsscra = rSpssCra.copyMe();
        spsscra.setIApi(SPSS_UPD);

        spsscra.setIGwtGud(ClnEnv.getSessTok());
        spsscra.setILoginId(ClnEnv.isLoggedIn() ? ClnEnv.getSessionLoginID() : "");
        spsscra.setICkSum(Utils.sha256(ClnEnv.getPref(activity.getApplicationContext(), activity.getString(R.string.CONST_PREF_GCM_REGID), "") + "A", ClnEnv.getPref(activity, activity.getString(R.string.CONST_PREF_SALT), APIsManager.PRD_salt)));

        spsscra.getISpssRec().devTy = SpssRec.TY_ANDROID;
        spsscra.getISpssRec().devId = ClnEnv.getPref(activity, activity.getString(R.string.CONST_PREF_GCM_REGID), "");
        spsscra.getISpssRec().lang = ClnEnv.getAppLocale(activity);
        //please note that bni and gni are defined before this call.
        APIsRequest<SpssCra> apiRequest = new APIsRequest<>(url, SPSS_UPD);
        apiRequest.setiCra(spsscra);

        // 2nd request
        progressManager = ProgressManager.getInstance(activity);
        progressManager.showProgressDialog(activity);
        if (ClnEnv.isLoggedIn()) {
            doHeloOptional(activity, null, SpssCra.class, apiRequest);
        } else {
            doHelo(activity, null, SpssCra.class, apiRequest);
        }
    }

    // MyMob verify
    public static void doVerify(final Fragment frag, AddOnCra rAddonCra) {
        Context cxt = frag.getActivity();
        domainURL = ClnEnv.getPref(cxt, Objects.requireNonNull(cxt).getString(R.string.CONST_PREF_DOMAIN), APIsManager.PRD_domain);
        String url = domainURL + "/ma/aoAddOn";
        AddOnCra addoncra = rAddonCra.copyMe();
        addoncra.setIApi(AO_ADDON);

        APIsRequest<AddOnCra> apiRequest = new APIsRequest<>(url, AO_ADDON);
        apiRequest.setiCra(addoncra);

        if (debug) Log.i("ADD ACCOUNT", "ADD ACCOUNT");

        // 2nd request
        progressManager = ProgressManager.getInstance(cxt);
        progressManager.showProgressDialog(cxt);

        doHeloOptional(null, frag, AddOnCra.class, apiRequest);
    }

    // MyMob Authed
    public static void doAuthen(final Fragment frag, AddOnCra rAddonCra) {
        Context cxt = frag.getActivity();
        domainURL = ClnEnv.getPref(cxt, Objects.requireNonNull(cxt).getString(R.string.CONST_PREF_DOMAIN), APIsManager.PRD_domain);
        String url = domainURL + "/ma/aoAuth";
        AddOnCra addoncra = rAddonCra.copyMe();
        addoncra.setISms(false);
        addoncra.setIApi(AO_AUTH);

        APIsRequest<AddOnCra> apiRequest = new APIsRequest<>(url, AO_AUTH);
        apiRequest.setiCra(addoncra);

        if (debug) Log.i("AUTHEN", "AUTHEN");

        // 2nd request
        progressManager = ProgressManager.getInstance(cxt);
        progressManager.showProgressDialog(cxt);
        doHeloOptional(null, frag, AddOnCra.class, apiRequest);
    }

    public static void doSvcAso(final FragmentActivity activity, final Fragment fragment, AcMainCra racMainCra) {
        Context cxt = activity == null ? fragment.getActivity() : activity;
        domainURL = ClnEnv.getPref(cxt, Objects.requireNonNull(cxt).getString(R.string.CONST_PREF_DOMAIN), APIsManager.PRD_domain);
        String url = domainURL + "/ma/svcAso";
        AcMainCra acMainCra = racMainCra.copyMe();
        acMainCra.setIApi(SVCASO);
        acMainCra.setIGwtGud(ClnEnv.getSessTok());
        acMainCra.setIBcifRec(null); //must be null for all acMainCra related API call

        APIsRequest<AcMainCra> apiRequest = new APIsRequest<>(url, SVCASO);
        apiRequest.setiCra(acMainCra);

        // 2nd request
        progressManager = ProgressManager.getInstance(cxt);
        progressManager.showProgressDialog(cxt);
        doAction(activity, fragment, AcMainCra.class, apiRequest);
    }

    public static void doSvcAso4Subn(final FragmentActivity activity, final Fragment fragment, AcMainCra racMainCra) {
        Context cxt = activity == null ? fragment.getActivity() : activity;
        domainURL = ClnEnv.getPref(cxt, Objects.requireNonNull(cxt).getString(R.string.CONST_PREF_DOMAIN), APIsManager.PRD_domain);
        String url = domainURL + "/ma/svcAso4Subn";
        AcMainCra acMainCra = racMainCra.copyMe();
        acMainCra.setIApi(SVCASO4SUBN);
        acMainCra.setIGwtGud(ClnEnv.getSessTok());
        acMainCra.setIBcifRec(null); //must be null for all acMainCra related API call

        APIsRequest<AcMainCra> apiRequest = new APIsRequest<>(url, SVCASO4SUBN);
        apiRequest.setiCra(acMainCra);

        // 2nd request
        progressManager = ProgressManager.getInstance(cxt);
        progressManager.showProgressDialog(cxt);
        doAction(activity, fragment, AcMainCra.class, apiRequest);
    }

    public static void doGetBill(final Fragment fragment, BinqCra rbinqCra) {
        Context cxt = fragment.getActivity();
        domainURL = ClnEnv.getPref(cxt, Objects.requireNonNull(cxt).getString(R.string.CONST_PREF_DOMAIN), APIsManager.PRD_domain);
        String url = domainURL + "/ma/bill";
        BinqCra binqCra = rbinqCra.copyMe();
        binqCra.setIApi(BILL);
        binqCra.setIGwtGud(ClnEnv.getSessTok());

        APIsRequest<BinqCra> apiRequest = new APIsRequest<>(url, BILL);
        apiRequest.setiCra(binqCra);

        // 2nd request
        progressManager = ProgressManager.getInstance(cxt);
        progressManager.showProgressDialog(cxt);
        doAction(fragment.getActivity(), fragment, BinqCra.class, apiRequest);
    }

    public static void doGetBillInfo(final Fragment fragment, BiifCra rbiifCra) {
        Context cxt = fragment.getActivity();
        domainURL = ClnEnv.getPref(cxt, Objects.requireNonNull(cxt).getString(R.string.CONST_PREF_DOMAIN), APIsManager.PRD_domain);
        String url = domainURL + "/ma/readBiif";
        BiifCra biifCra = rbiifCra.copyMe();
        biifCra.setIApi(READ_BIIF);
        biifCra.setIGwtGud(ClnEnv.getSessTok());

        APIsRequest<BiifCra> apiRequest = new APIsRequest<>(url, READ_BIIF);
        apiRequest.setiCra(biifCra);

        progressManager = ProgressManager.getInstance(cxt);
        progressManager.showProgressDialog(cxt);
        doAction(fragment.getActivity(), fragment, BiifCra.class, apiRequest);
    }

    public static void doUpdatePayMeth(final Fragment fragment, BiifCra rbiifCra) {
        Context cxt = fragment.getActivity();
        domainURL = ClnEnv.getPref(cxt, Objects.requireNonNull(cxt).getString(R.string.CONST_PREF_DOMAIN), APIsManager.PRD_domain);
        String url = domainURL + "/ma/reqUpdTok4LtsPi";
        BiifCra biifCra = rbiifCra.copyMe();
        biifCra.setIApi(REQ_UPDTOK4LTSPI);
        biifCra.setIGwtGud(ClnEnv.getSessTok());

        APIsRequest<BiifCra> apiRequest = new APIsRequest<>(url, REQ_UPDTOK4LTSPI);
        apiRequest.setiCra(biifCra);

        progressManager = ProgressManager.getInstance(cxt);
        progressManager.showProgressDialog(cxt);
        doAction(fragment.getActivity(), fragment, BiifCra.class, apiRequest);
    }

    public static void doGetShopByGC(final Fragment fragment, ShopCra rShopCra) {
        Context cxt = fragment.getActivity();
        domainURL = ClnEnv.getPref(cxt, Objects.requireNonNull(cxt).getString(R.string.CONST_PREF_DOMAIN), APIsManager.PRD_domain);
        String url = domainURL + "/ma/shopByGC";
        ShopCra shopCra = rShopCra.copyMe();
        shopCra.setIApi(SHOP_BYGC);

        APIsRequest<ShopCra> apiRequest = new APIsRequest<>(url, SHOP_BYGC);
        apiRequest.setiCra(shopCra);

        progressManager = ProgressManager.getInstance(cxt);
        progressManager.showProgressDialog(cxt);
        doHeloOptional(fragment.getActivity(), fragment, ShopCra.class, apiRequest);
    }

    public static void doUpdBillinfo(final Fragment fragment, BiifCra rbiifCra) {
        Context cxt = fragment.getActivity();
        domainURL = ClnEnv.getPref(cxt, Objects.requireNonNull(cxt).getString(R.string.CONST_PREF_DOMAIN), APIsManager.PRD_domain);
        String url = domainURL + "/ma/updBiif";

        BiifCra biifCra = rbiifCra.copyMe();
        biifCra.setIApi(UPD_BIIF);
        biifCra.setIGwtGud(ClnEnv.getSessTok());

        APIsRequest<BiifCra> apiRequest = new APIsRequest<>(url, UPD_BIIF);
        apiRequest.setiCra(biifCra);

        progressManager = ProgressManager.getInstance(cxt);
        progressManager.showProgressDialog(cxt);
        doAction(fragment.getActivity(), fragment, BiifCra.class, apiRequest);
    }

    /*
    public static final void doShowBill(final Fragment fragment, BiifCra rbiifCra) {
        Context cxt = fragment.getActivity();
        domainURL = ClnEnv.getPref(cxt, cxt.getString(R.string.CONST_PREF_DOMAIN), APIsManager.PRD_domain);
    }
    */

    public static void doGetPlanTv(final Fragment fragment, PlanTvCra rplanTvCra) {
        Context cxt = fragment.getActivity();
        domainURL = ClnEnv.getPref(cxt, Objects.requireNonNull(cxt).getString(R.string.CONST_PREF_DOMAIN), APIsManager.PRD_domain);
        String url = domainURL + "/ma/planTv";
        PlanTvCra planTvCra = rplanTvCra.copyMe();
        planTvCra.setIApi(PLAN_TV);

        APIsRequest<PlanTvCra> apiRequest = new APIsRequest<>(url, PLAN_TV);
        apiRequest.setiCra(planTvCra);

        progressManager = ProgressManager.getInstance(cxt);
        progressManager.showProgressDialog(cxt);
        doHeloOptional(fragment.getActivity(), fragment, PlanTvCra.class, apiRequest);
    }

    public static void doCheckBillSts(final Fragment fragment, BinqCra rbinqCra, Boolean isLastInLoop) {
        Context cxt = fragment.getActivity();
        domainURL = ClnEnv.getPref(cxt, Objects.requireNonNull(cxt).getString(R.string.CONST_PREF_DOMAIN), APIsManager.PRD_domain);
        String url = domainURL + "/ma/chkBill";
        BinqCra binqCra = rbinqCra.copyMe();
        binqCra.setIApi(CHK_BILL);

        APIsRequest<BinqCra> apiRequest = new APIsRequest<>(url, CHK_BILL);
        apiRequest.setiCra(binqCra);

        progressManager = ProgressManager.getInstance(cxt);
        progressManager.showProgressDialog(cxt);
        doHeloOptional(fragment.getActivity(), fragment, BinqCra.class, apiRequest, isLastInLoop);
    }

    public static void doDownloadPDF(final Fragment fragment, BinqCra rbinqCra, String lobString, String cusNum, int clickBill, String lang) {
        final Context cxt = fragment.getActivity();
        domainURL = ClnEnv.getPref(cxt, Objects.requireNonNull(cxt).getString(R.string.CONST_PREF_DOMAIN), APIsManager.PRD_domain);
        String filePath;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            filePath = cxt.getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS).getPath();
        } else {
            filePath = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).getPath();
        }
        String endpoint = "/svlt/showBill";
        AQuery aq = new AQuery(fragment.getActivity());

        String urlFormat = "?p0=%s&p1=%s&p2=%s&p3=%s&p4=%s&p5=%s";
        String filenameFormat = "%s-%s.pdf";

        Bill[] billary = rbinqCra.getOBillList().getOBillAry();

        String iLobString = "".equals(lobString) ? " " : lobString;
        String systy = "".equals(billary[clickBill].getSysTy()) ? " " : billary[clickBill].getSysTy();
        String iCusNum = "".equals(cusNum) ? " " : cusNum;
        String iAcctNum = "".equals(billary[clickBill].getAcctNum()) ? " " : billary[clickBill].getAcctNum();
        String iInvDate = "".equals(billary[clickBill].getInvDate()) ? " " : billary[clickBill].getInvDate();
        String iLang = "".equals(lang) ? " " : lang;

        String url = String.format(urlFormat, iLobString, systy, iCusNum, iAcctNum, iInvDate, iLang);
        url = url.replaceAll(" ", "%20");
        url = String.format("%s%s%s", domainURL, endpoint, url);
        String filename = String.format(filenameFormat, billary[clickBill].getAcctNum(), billary[clickBill].getInvDate());

        // testing
        // dummy pdf data for testing
        // String 	url  = "http://www.orimi.com/pdf-test.pdf";
        // String filename = "test.pdf";
        // set saved file destination
        final File target = new File(filePath, filename);

        AjaxCallback<File> ajaxCallback = new AjaxCallback<>() {
            @Override
            public void callback(String url, File file, AjaxStatus status) {
                OnAPIsListener callback;
                APIsResponse response;
                try {
                    progressManager.showProgressDialog(cxt);
                    try {
                        callback = (OnAPIsListener) fragment;
                    } catch (ClassCastException e) {
                        throw new ClassCastException(fragment.getActivity().toString() + " must implement OnAPIsListener");
                    }

                    response = new APIsResponse();
                    if (status.getCode() != 200) {
                        if (status.getCode() == 500) {
                            response.setReply(HTTPSTATUS_NOT_200);
                            response.setMessage(ClnEnv.getRPCErrMsg(cxt, -1, "HTTP " + status.getCode()));
                            callback.onFail(response);
                        } else {
                            response.setReply(HTTPSTATUS_NOT_200);
                            response.setMessage(ClnEnv.getRPCErrMsg(cxt, status.getCode(), status.getMessage()));
                            callback.onFail(response);
                        }

                    } else {
                        if (file != null) {
                            // Open pdf
                            if (file.exists()) {
                                try {

                                    Uri uri = FileProvider.getUriForFile(fragment.requireContext(), "com.pccw.myhkt.provider", file);

                                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                                        ContentValues values = new ContentValues();
                                        values.put(MediaStore.Downloads.DISPLAY_NAME, filename);
                                        values.put(MediaStore.Downloads.MIME_TYPE, "application/pdf");
                                        values.put(MediaStore.Downloads.RELATIVE_PATH, Environment.DIRECTORY_DOWNLOADS);
                                        ContentResolver resolver = cxt.getContentResolver();

                                        Uri uriInsert = resolver.insert(MediaStore.Downloads.EXTERNAL_CONTENT_URI, values);
                                        OutputStream outputStream = resolver.openOutputStream(Objects.requireNonNull(uriInsert));
                                        InputStream sourceFile = new FileInputStream(file);

                                        copyFile(sourceFile, outputStream);

                                        String[] projection = new String[]{MediaStore.Files.FileColumns._ID, MediaStore.Downloads.DISPLAY_NAME, MediaStore.Downloads.DATA};

                                        Uri collection = MediaStore.Downloads.getContentUri(MediaStore.VOLUME_EXTERNAL);
                                        Cursor cursor = resolver.query(collection, projection, MediaStore.Files.FileColumns.DISPLAY_NAME, null, null);

                                        long fileId = 0;
                                        while (true) {
                                            assert cursor != null;
                                            if (!cursor.moveToNext()) break;
                                            int columnIndex = cursor.getColumnIndex(projection[0]);
                                            fileId = cursor.getLong(columnIndex);
                                        }
                                        cursor.close();
                                        uri = Uri.parse(collection + File.separator + fileId);
                                    }

                                    Intent intent = new Intent(Intent.ACTION_VIEW);
                                    intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY | Intent.FLAG_GRANT_READ_URI_PERMISSION);
                                    intent.setDataAndType(uri, "application/pdf");
                                    fragment.getActivity().startActivity(Intent.createChooser(intent, "Choose PDF File Viewer"));

                                } catch (ActivityNotFoundException e) {
                                    // Instruct the user to install a PDF reader here, or something
                                    DialogHelper.createSimpleDialog(fragment.getActivity(), fragment.getString(R.string.myhkt_nopdfapp));
                                }
                            } else {
                                DialogHelper.createSimpleDialog(fragment.getActivity(), fragment.getString(R.string.myhkt_pdfnf));
                            }
                        } else {
                            DialogHelper.createSimpleDialog(fragment.getActivity(), fragment.getString(R.string.myhkt_pdfnf));
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                if (progressManager != null) {
                    progressManager.dismissProgressDialog();
                }
            }
        };

        // Here we are setting the cookie info.
        for (Cookie cookie : mCookies) {
            ajaxCallback.cookie(cookie.getName(), cookie.getValue());
        }
        aq.download(url, target, ajaxCallback);
    }

    public static void copyFile(InputStream in, OutputStream out) throws IOException {
        // https://stackoverflow.com/questions/11398239/open-a-pdf-file-programmatically
        // Reference https://stackoverflow.com/questions/58864154/how-do-i-copy-a-pdf-into-my-internal-storage-android-studio
        byte[] buffer = new byte[1024];
        int read;
        while ((read = in.read(buffer)) != -1) {
            out.write(buffer, 0, read);
        }
    }

    public static void doDownloadImageAttachment(Context context, String url, final OnDownloadAttachmentListener listener, final boolean isFinal, String extention) {
        try {
            final String filePath = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).getPath();
            AQuery aq = new AQuery(context);

            String filename = System.currentTimeMillis() + "";
            filename = filename.replace(" ", "");

            // set saved file destination
            final File outputDir = context.getCacheDir(); // context being the Activity pointer
            final File target = File.createTempFile("prefix", "extension", outputDir);

            //AJAX callback
            AjaxCallback<File> ajaxCallback = new AjaxCallback<>() {
                @Override
                public void callback(String url, File file, AjaxStatus status) {
                    try {

                        if (status.getCode() != 200) {
                            listener.onFailedDownload();
                        } else {
                            if (file != null && (file.exists())) {
                                // if (readBytesFromFile(file)[0] != 0x47 && !isFinal){
                                byte b = readBytesFromFile(file)[0];
                                String s = "";
                                if (b == 0xFF) s = "jpeg";
                                else if (b == 0x89) s = ".png";
                                else if (b == 0x47) s = ".gif";
                                else if (b == 0x49 || b == 0x4D) s = ".tiff";
                                File newDestination = File.createTempFile("prefix", s, outputDir);
                                if (file.renameTo(newDestination)) {
                                    Log.d("File", "File renamed to " + newDestination.getAbsolutePath());
                                }
                                listener.onSuccessDownload(newDestination);
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            };

            // Here we are setting the cookie info.
            for (Cookie cookie : mCookies) {
                ajaxCallback.cookie(cookie.getName(), cookie.getValue());
            }
            aq.download(url, target, ajaxCallback);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static byte[] readBytesFromFile(File file) {
        FileInputStream fileInputStream = null;
        byte[] bytesArray = null;

        try {

            bytesArray = new byte[(int) file.length()];
            //read file into bytes[]
            fileInputStream = new FileInputStream(file);
            fileInputStream.read(bytesArray);

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (fileInputStream != null) {
                try {
                    fileInputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return bytesArray;
    }

    public static void doGetAppointment(final Fragment fragment, ApptCra rapptCra) {
        Context cxt = fragment.getActivity();
        domainURL = ClnEnv.getPref(cxt, Objects.requireNonNull(cxt).getString(R.string.CONST_PREF_DOMAIN), APIsManager.PRD_domain);
        if (debug) Log.i("API", "doGetAppointment");
        String url = domainURL + "/ma/appt";
        ApptCra apptCra = rapptCra.copyMe();
        apptCra.setIApi(APPT);

        APIsRequest<ApptCra> apiRequest = new APIsRequest<>(url, APPT);
        apiRequest.setiCra(apptCra);

        progressManager = ProgressManager.getInstance(cxt);
        progressManager.showProgressDialog(cxt);
        doHeloOptional(fragment.getActivity(), fragment, ApptCra.class, apiRequest);
    }

    public static void doGetAppointment(final IntentService service, ApptCra rapptCra) {
        domainURL = ClnEnv.getPref(service, service.getString(R.string.CONST_PREF_DOMAIN), APIsManager.PRD_domain);
        if (debug) Log.i("API", "doGetAppointment");
        String url = domainURL + "/ma/appt";
        ApptCra apptCra = rapptCra.copyMe();
        apptCra.setIApi(APPT);
        APIsRequest<ApptCra> apiRequest = new APIsRequest<>(url, APPT);
        apiRequest.setiCra(apptCra);
        doAction(null, null, service, ApptCra.class, apiRequest, true);
    }

    public static void doCreateSR(final Fragment fragment, ApptCra rapptCra) {
        Context cxt = fragment.getActivity();
        domainURL = ClnEnv.getPref(cxt, Objects.requireNonNull(cxt).getString(R.string.CONST_PREF_DOMAIN), APIsManager.PRD_domain);
        String url = domainURL + "/ma/srCrt";
        ApptCra apptCra = rapptCra.copyMe();
        apptCra.setIApi(SR_CRT);

        APIsRequest<ApptCra> apiRequest = new APIsRequest<>(url, SR_CRT);
        apiRequest.setiCra(apptCra);

        progressManager = ProgressManager.getInstance(cxt);
        progressManager.showProgressDialog(cxt);
        doHeloOptional(fragment.getActivity(), fragment, ApptCra.class, apiRequest);
    }

    public static void doUpdateSR(final Fragment fragment, ApptCra rapptCra) {
        Context cxt = fragment.getActivity();
        domainURL = ClnEnv.getPref(cxt, Objects.requireNonNull(cxt).getString(R.string.CONST_PREF_DOMAIN), APIsManager.PRD_domain);
        String url = domainURL + "/ma/srUpd";
        ApptCra apptCra = rapptCra.copyMe();
        apptCra.setIApi(SR_UPD);

        APIsRequest<ApptCra> apiRequest = new APIsRequest<>(url, SR_UPD);
        apiRequest.setiCra(apptCra);

        progressManager = ProgressManager.getInstance(cxt);
        progressManager.showProgressDialog(cxt);
        doHeloOptional(fragment.getActivity(), fragment, ApptCra.class, apiRequest);
    }

    public static void doCancelSR(final Fragment fragment, ApptCra rapptCra) {
        Context cxt = fragment.getActivity();
        domainURL = ClnEnv.getPref(cxt, Objects.requireNonNull(cxt).getString(R.string.CONST_PREF_DOMAIN), APIsManager.PRD_domain);
        String url = domainURL + "/ma/srCls";
        ApptCra apptCra = rapptCra.copyMe();
        apptCra.setIApi(SR_CLS);

        APIsRequest<ApptCra> apiRequest = new APIsRequest<>(url, SR_CLS);
        apiRequest.setiCra(apptCra);

        progressManager = ProgressManager.getInstance(cxt);
        progressManager.showProgressDialog(cxt);
        doHeloOptional(fragment.getActivity(), fragment, ApptCra.class, apiRequest);
    }

    private static <T extends BaseCraEx> String sampleData(Context ctx, APIsRequest<T> apiRequest, String result) {
        //dummy json for API testing

        // if(INBX_MSG.equalsIgnoreCase(apiRequest.getActionTy())) return MockData.INBOX_SAMPLE_RESPONSE();

        // if (PLAN_IMS.equalsIgnoreCase(apiRequest.getActionTy())) { return MockData.PLAN_IMS(); }

        //check bill
        //if (CHK_BILL.equalsIgnoreCase(apiRequest.getActionTy())) { return MockData.CHK_BILL(); }

        //if (LGI.equalsIgnoreCase(apiRequest.getActionTy())) {
        //    return MockData.LGI();
        //    return MockData.LGI_2();
        //    return MockData.LGI_3();
        //}

        //login with appointment icon
        //if(LGI.equalsIgnoreCase(apiRequest.getActionTy())) { return MockData.LGI_WITH_APPOINTMENT(); }

        //lts plan
        //if (PLAN_LTS.equalsIgnoreCase(apiRequest.getActionTy())) {
        // return MockData.PLAN_LTS();
        // return MockData.PLAN_LTS_GLOBAL();
        // return MockData.PLANT_LTS_2();
        // }

        //csim zombie
        //f (LGI.equalsIgnoreCase(apiRequest.getActionTy())) { return MockData.LGI_CSIM_ZOMBIE();}

        //if (PLAN_MOB.equalsIgnoreCase(apiRequest.getActionTy())) {
        //    return MockData.PLAN_MOB_X1();
        //    return MockData.PLAN_MOB_X2();
        //    return MockData.PLAN_MOB_X3();
        //    return MockData.PLAN_MOB_X4();
        //    return MockData.PLAN_MOB_X5();
        //}

        //if (PLAN_TV.equalsIgnoreCase(apiRequest.getActionTy()))  {
        // return MockData.PLAN_TV();
        // }

        //if (APPT.equalsIgnoreCase(apiRequest.getActionTy())) {
        // return MockData.APPT();
        // return MockData.APPT_2();
        // return MockData.APPT_3();
        // return MockData.APPT_4();
        // }


        //if (AO_AUTH.equalsIgnoreCase(apiRequest.getActionTy())) {
        // return MockData.AO_AUTH_SUCCESS();
        // return MockData.AUTH_H_INDIV();
        //	}

        //if (AO_ADDON.equalsIgnoreCase(apiRequest.getActionTy())) {
        // return MockData.ADD_SUCCESS();
        // return MockData.ADD_H_INDIV();
        //}


        // if (PLAN_MOB.equalsIgnoreCase(apiRequest.getActionTy())) {
        //	 return ClnEnv.getJSONString(ctx, "json/hsim_mobusage.json");
        // }
        //		//Appointment case (ltstest01@abc.com , 37551223);
        //		if (LNTT.equalsIgnoreCase(apiRequest.getActionTy())) {
        //			return ClnEnv.getJSONString(ctx, "json/lntt_rs_report.json");
        //		}
        //		if (SR_ENQ_NP.equalsIgnoreCase(apiRequest.getActionTy())) {
        //			return ClnEnv.getJSONString(ctx, "json/enq_sr.json");
        //		}
        //		if (SR_AVA_TS.equalsIgnoreCase(apiRequest.getActionTy())) {
        //			return ClnEnv.getJSONString(ctx, "json/sr_ava_ts.json");
        //		}
        //		if (SR_CRT.equalsIgnoreCase(apiRequest.getActionTy())) {
        //			return ClnEnv.getJSONString(ctx, "json/sr_crt.json");
        //		}
        //		if (PLAN_MOB.equalsIgnoreCase(apiRequest.getActionTy())) {
        //			return ClnEnv.getJSONString(ctx, "json/hsim_mobusage.json");}
        //		}

        //		if (SR_AVA_TS.equalsIgnoreCase(apiRequest.getActionTy())) {
        //			return MockData.SR_AVA_TS();
        //		}


        //		if (BILL.equalsIgnoreCase(apiRequest.getActionTy())) {
        //			//MOB
        //			if (result.contains("MOB"))
        //            return MockData.BILL_MOB();
        //			//PCD
        //			if (result.contains("TV") || result.contains("PCD"))
        //			return MockData.BILL_PCD();
        //			///101 / O2F
        //			if (result.contains("101") || result.contains("O2F"))
        //			return  BILL_101();
        //			return  BILL_101_2();
        //		}

        //Moiz: 6.18.18 - To test the the Global asia plan please uncomment the next line code
        //if (PLAN_MOB.equalsIgnoreCase(apiRequest.getActionTy())) { return MockData.PLAN_MOB_REGION_GLOBAL_J1();}
        int maxLogSize = 1000;
        for (int i = 0; i <= result.length() / maxLogSize; i++) {
            int start = i * maxLogSize;
            int end = (i + 1) * maxLogSize;
            end = end > result.length() ? result.length() : end;
            if (debug) Log.v("APISManager", result.substring(start, end));
        }
        return result;
    }

    public static void forceCloseProgressDialog(Context cxt) {
        progressManager = ProgressManager.getInstance(cxt);
        progressManager.dismissProgressDialog(true);
    }

    public static <T extends BaseCraEx> void doHelo(final Activity activity) {
        final AQuery aq = new AQuery(activity);
        // doHello first
        final String TAGNAME = HELO;
        domainURL = ClnEnv.getPref(activity, activity.getString(R.string.CONST_PREF_DOMAIN), APIsManager.PRD_domain);
        String url = domainURL + "/ma/helo";
        HeloCra heloCra = new HeloCra();
        heloCra.setIApi(HELO);

        // including spssCra on every doHello
        SpssCra spssCra = new SpssCra();
        spssCra.setICkSum(Utils.sha256(ClnEnv.getPref(activity.getApplicationContext(), activity.getString(R.string.CONST_PREF_GCM_REGID), "") + "A", ClnEnv.getPref(activity, activity.getString(R.string.CONST_PREF_SALT), APIsManager.PRD_salt)));

        if (ClnEnv.isLoggedIn()) {
            spssCra.setISpssRec(ClnEnv.getLgiCra().getISpssRec());
            spssCra.setILoginId(ClnEnv.getLoginId());
        } else {
            // if not doing login or save login
            spssCra.getISpssRec().devTy = SpssRec.TY_ANDROID;
            spssCra.getISpssRec().devId = ClnEnv.getPref(activity, activity.getString(R.string.CONST_PREF_GCM_REGID), "");
            spssCra.getISpssRec().lang = ClnEnv.getAppLocale(activity);
            spssCra.getISpssRec().gni = ClnEnv.getPref(activity.getApplicationContext(), activity.getString(R.string.CONST_PREF_GENNOTI_FLAG), "Y");
        }
        heloCra.setISpssCra(spssCra);

        String jstr = gson.toJson(heloCra);
        ClnEnv.toLogFile(String.format("%s %s", TAGNAME, "request sent"), jstr);
        if (debug) Log.d(TAGNAME, String.format("%s %s", TAGNAME, "request sent:") + jstr);

        try {
            HttpEntity entity = new StringEntity(jstr, "UTF-8");
            Map<String, Object> params = new HashMap<>();
            params.put(AQuery.POST_ENTITY, entity);
            aq.ajax(url, params, JSONObject.class, new AjaxCallback<>() {
                OnAPIsListener callback;
                APIsResponse response;
                HeloCra cra;

                @Override
                public void callback(String url, JSONObject json, AjaxStatus status) {
                    boolean isFailCase = false;
                    mCookies = status.getCookies();
                    try {
                        try {
                            callback = (OnAPIsListener) activity;
                        } catch (ClassCastException e) {
                            throw new ClassCastException(activity + " must implement OnAPIsListener");
                        }

                        response = new APIsResponse();
                        response.setActionTy(HELO);
                        cra = new HeloCra();
                        if (status.getCode() != 200) {
                            response.setReply(HTTPSTATUS_NOT_200);
                            response.setMessage(ClnEnv.getRPCErrMsg(activity, status.getCode(), status.getMessage()));
                            callback.onFail(response);
                            isFailCase = true;
                        } else if (json == null) {
                            response.setReply(JSON_RETURN_NULL);
                            response.setMessage(ClnEnv.getRPCErrMsg(activity, status.getCode(), status.getMessage()));
                            callback.onFail(response);
                            isFailCase = true;
                        } else {
                            String resultStr = json.toString();
                            if (debugCra)
                                ClnEnv.toLogFile(String.format("%s %s %s", TAGNAME, cra.getClass().getSimpleName(), "response received"), resultStr);
                            if (debugCra)
                                Log.d(TAGNAME, String.format("%s %s %s", TAGNAME, cra.getClass().getSimpleName(), "response received:") + resultStr);

                            try {
                                Type collectionType = new TypeToken<HeloCra>() {
                                }.getType();
                                cra = gson.fromJson(json.toString(), collectionType);
                            } catch (Exception e) {
                                if (debug)
                                    Log.e(TAGNAME, "/gson.fromJson error: " + e.getMessage());
                                e.printStackTrace();
                            }

                            if (cra == null || !cra.getReply().isSucc()) {
                                if (cra == null) {
                                    cra = new HeloCra();
                                    cra.setReply(NULL_OBJECT_CRA);
                                }
                                isFailCase = true;
                            } else {
                                // set mobile number prefix
                                String mobPfx = cra.getOClnCfg().getProp().get("CLN_MOB_PFX");
                                String ltsPfx = cra.getOClnCfg().getProp().get("CLN_LTS_PFX");
                                if (mobPfx != null && !mobPfx.isEmpty())
                                    ClnEnv.setPref(activity, "mobPfx", mobPfx);
                                if (ltsPfx != null && !ltsPfx.isEmpty())
                                    ClnEnv.setPref(activity, "ltsPfx", ltsPfx);
                                ClnEnv.setHeloCra(cra);
                                response.setReply(cra.getReply());
                                response.setCra(cra);
                                callback.onSuccess(response);
                                if (debug)
                                    Utils.showLog(response.getActionTy(), String.format("%s %s %s", response.getActionTy(), cra.getClass().getSimpleName(), "response received:") + resultStr);
                            }
                        }
                        progressManager = ProgressManager.getInstance(activity);
                        progressManager.dismissProgressDialog(isFailCase);

                    } catch (Exception e) {
                        e.printStackTrace();
                        // progressManager.dismissProgressDialog(true);
                        if (debugCra) Log.d(TAGNAME, e.toString());
                    }
                }
            });
        } catch (Exception e1) {
            e1.printStackTrace();
        }
    }
}
