package com.pccw.myhkt.activity;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import androidx.fragment.app.FragmentActivity;

import com.pccw.myhkt.APIsManager;
import com.pccw.myhkt.APIsResponse;
import com.pccw.myhkt.ClnEnv;
import com.pccw.myhkt.FAWrapper;
import com.pccw.myhkt.R;
import com.pccw.myhkt.Utils;
import com.pccw.myhkt.lib.ui.AAQuery;

/************************************************************************
 * File : TACSActivity.java
 * Change History:
 * Date       Modified By		Description
 * ---------- ----------------	-------------------------------
 * 03/07/2018 Abdulmoiz Esmail  - Download the app config right away
 *************************************************************************/
public class TACSActivity extends FragmentActivity implements APIsManager.OnAPIsListener {
    private AAQuery aq;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Status Bar Color
        Window window = getWindow();
        window.getDecorView().setFitsSystemWindows(false);
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.setStatusBarColor(getResources().getColor(R.color.hkt_textcolor));
        setContentView(R.layout.activity_tacs);
    }

    @Override
    protected final void onResume() {
        super.onResume();
        //Screen Tracker
        FAWrapper.getInstance().sendFAScreen(this, R.string.CONST_SCRN_TACS, false);
    }

    // Android Device Back Button Handling
    public final void onBackPressed() {
        finish();
    }


    @Override
    protected void onStart() {
        super.onStart();
        initUI();
    }

    protected final void initUI() {
        aq = new AAQuery(this);
        int extralinespace = (int) getResources().getDimension(R.dimen.extralinespace);
        int buttonPadding = (int) getResources().getDimension(R.dimen.reg_logo_padding);
        //navbar style
        aq.navBarBaseLayout(R.id.navbar_base_layout);
        aq.navBarTitle(R.id.navbar_title, getResources().getString(R.string.app_name));

        aq.id(R.id.tacs_heading).textColorId(R.color.hkt_textcolor);
        aq.id(R.id.tacs_heading).textSize(16f);
        aq.id(R.id.tacs_heading).getTextView().setTypeface(Typeface.MONOSPACE, Typeface.BOLD);
        aq.marginpx(R.id.tacs_heading, buttonPadding, buttonPadding, buttonPadding, buttonPadding);

        aq.id(R.id.tacs_frame).backgroundColorId(R.color.white);
        aq.marginpx(R.id.tacs_scrollview, buttonPadding, 0, buttonPadding, 0);
        aq.norm2TxtBtns(R.id.tacs_btn_decline, R.id.tacs_btn_accept, Utils.getString(this, R.string.btn_decline), Utils.getString(this, R.string.btn_accept));
        aq.marginpx(R.id.tacs_btn_layout, 0, extralinespace, 0, extralinespace);
        aq.id(R.id.tacs_btn_accept).clicked(this, "onClick");
        aq.id(R.id.tacs_btn_decline).clicked(this, "onClick");
    }

    public void onClick(View v) {
        Intent intent;
        switch (v.getId()) {
            case R.id.tacs_btn_accept:
                // Keep version of TACS accepted
                ClnEnv.setPref(getApplicationContext(), getString(R.string.CONST_PREF_TACSVERSION), ClnEnv.getAppVersion(getApplicationContext()));
                finish();
                intent = new Intent(getApplicationContext(), MainMenuActivity.class);
                startActivity(intent);
                break;
            case R.id.tacs_btn_decline:
                finish();
                break;
        }
    }

    @Override
    public void onSuccess(APIsResponse response) throws Exception {

    }

    @Override
    public void onFail(APIsResponse response) throws Exception {

    }
}
