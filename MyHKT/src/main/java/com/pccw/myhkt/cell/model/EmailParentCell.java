package com.pccw.myhkt.cell.model;

public class EmailParentCell extends Cell {

    private String parentTitle;

    public EmailParentCell(String title) {
        type = Cell.MY_MESSAGE_TITLE;
        parentTitle = title;
    }

    public String getParentTitle() {
        return parentTitle;
    }
}
