package com.pccw.myhkt.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.Point;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.LinearLayout.LayoutParams;

import androidx.fragment.app.FragmentActivity;

import com.google.gson.Gson;
import com.pccw.dango.shared.tool.DirNum;
import com.pccw.myhkt.APIsManager;
import com.pccw.myhkt.APIsManager.OnAPIsListener;
import com.pccw.myhkt.APIsResponse;
import com.pccw.myhkt.ClnEnv;
import com.pccw.myhkt.DialogHelper;
import com.pccw.myhkt.LiveChatHelper;
import com.pccw.myhkt.R;
import com.pccw.myhkt.Utils;
import com.pccw.myhkt.fragment.BaseServiceFragment.OnLiveChatListener;
import com.pccw.myhkt.lib.ui.AAQuery;
import com.pccw.myhkt.model.HomeButtonItem.MAINMENU;

/************************************************************************
 * File : BaseActivity.java 
 * Desc : Base Activity containing basic settings for all activity
 * Name : BaseActivity
 * by : Derek Tsui 
 * Date : 24/11/2015
 * Change History:
 * Date       Modified By		Description
 * ---------- ----------------	-------------------------------
 * 24/11/2015 Derek Tsui 		-First draft
 * 23/06/2017 Abdulmoiz Esmail  -Moved the checking of language on the onCreate method and on the setContentView method,
 *  							 checked if the locale  has changed to default English (due to webviews in Android 
 *  							 higher or equal 24), if does switch to Chinese.
 * 16/04/2018 Abdulmoiz Esmail  -Added isActivityActive that will validate before showing any dialog to prevent view leak
 *************************************************************************/

public class BaseActivity extends FragmentActivity implements OnAPIsListener, OnLiveChatListener {
    public static BaseActivity me;
    protected int basePadding;
    protected int extraLineSpace;
    protected int titleWidth;
    protected int detailBtnPadding;
    protected Boolean isLiveChatShown = true;
    protected FrameLayout frameContainer;
    protected AlertDialog alertDialog = null;
    protected String moduleId;
    protected Boolean isZh = false;
    protected Boolean callOnStart = true;
    protected Gson gson;
    private boolean debug = false;
    private AAQuery aq;
    private boolean doHelloFromChatMA = false;
    private boolean isActivityActive = false;

    public static void ivSessDialog() {
        DialogInterface.OnClickListener onPositiveClickListener = (dialog, which) -> {
            Intent intent = new Intent(me.getApplicationContext(), LoginActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            me.startActivity(intent);
            me.overridePendingTransition(R.anim.left_slide_in, R.anim.right_slide_out);
        };

        ClnEnv.setHeloCra(null);
        ClnEnv.setSessionLoginID(null);
        ClnEnv.setSessionPassword(null);
        DialogHelper.createSimpleDialog(me, Utils.getString(me, R.string.DLGM_ABORT_IVSESS), Utils.getString(me, R.string.btn_ok), onPositiveClickListener);
    }

    public String getModuleId() {
        return moduleId;
    }

    public void setModuleId(String moduleId) {
        if (debug) Log.d("MODULE ID", "BaseActivity set module id: " + moduleId);
        this.moduleId = moduleId;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        debug = getResources().getBoolean(R.bool.DEBUG);
        gson = new Gson();

        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        extraLineSpace = (int) getResources().getDimension(R.dimen.extralinespace);
        basePadding = (int) getResources().getDimension(R.dimen.basePadding);
        titleWidth = (int) getResources().getDimension(R.dimen.dq_title_width);
        detailBtnPadding = (int) getResources().getDimension(R.dimen.detail_btn_padding);
        ClnEnv.updateUILocale(getBaseContext(), ClnEnv.getAppLocale(getBaseContext()));
        me = this;
        gson = new Gson();
        //Language Indicator
        isZh = "zh".equalsIgnoreCase(getString(R.string.myhkt_lang));

        //For validation
        String mobPfx = ClnEnv.getPref(this, "mobPfx", getResString(R.string.mob_pfx));
        String ltsPfx = ClnEnv.getPref(this, "ltsPfx", "2,3,81,82,83");
        DirNum.getInstance(mobPfx, ltsPfx);

        //Status Bar Color
        Window window = getWindow();
        window.getDecorView().setFitsSystemWindows(false);
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        if (ClnEnv.isMyMobFlag()) {
            window.setStatusBarColor(getResources().getColor(R.color.hkt_edittextbgorange));
        } else {
            window.setStatusBarColor(getResources().getColor(ClnEnv.getSessionPremierFlag() ? R.color.hkt_headingpremier : R.color.hkt_textcolor));
        }

    }

    @Override
    public void setContentView(int layoutResID) {
        if (frameContainer != null) {
            frameContainer.removeAllViews();
        }
        frameContainer = new FrameLayout(this);
        LayoutParams layoutParam = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
        frameContainer.addView(getLayoutInflater().inflate(layoutResID, null), layoutParam);

        if (Build.VERSION.SDK_INT >= 24 && isZh && "en".equalsIgnoreCase(getString(R.string.myhkt_lang))) {
            Utils.switchToZhLocale(getApplicationContext());
        }
        super.setContentView(frameContainer, layoutParam);
    }

    @Override
    protected void onRestart() {
        if (debug) Log.i(this.getClass().toString(), "onRestart");
        callOnStart = false;
        super.onRestart();

    }

    @Override
    protected void onStart() {
        if (debug) Log.i(this.getClass().toString(), "onStart");
        super.onStart();

        isZh = "zh".equalsIgnoreCase(getResString(R.string.myhkt_lang));

        if (callOnStart) {
            initNavBar();
            initUI2();
        }
    }

    public final void initNavBar() {
        aq = new AAQuery(this);
        aq.navBarBaseLayout(R.id.navbar_base_layout);
        LiveChatHelper.getInstance(me, me);
        if (LiveChatHelper.isPause || isLiveChatShown) {
            aq.id(R.id.navbar_button_right).visibility(View.VISIBLE);
        } else {
            aq.id(R.id.navbar_button_right).visibility(View.INVISIBLE);
        }

        aq.id(R.id.navbar_button_right).clicked(v -> {
            //login required
            if (LiveChatHelper.isPause) {
                openLiveChat();
            } else if ((moduleId.equals(getResources().getString(R.string.MODULE_CONTACT_US_PRD)) || moduleId.equals(getResources().getString(R.string.MODULE_CONTACT_US_UAT))) && !ClnEnv.isLoggedIn()) {
                loginFirstDialog(MAINMENU.CONTACTUS);
            } else if (moduleId.equals(getResources().getString(R.string.MODULE_LTS_IDD)) && !ClnEnv.isLoggedIn()) {
                loginFirstDialog(MAINMENU.IDD);
            } else if (moduleId.equals(getResources().getString(R.string.MODULE_MM_MAIN)) && !ClnEnv.isLoggedIn()) {
                loginFirstDialog(MAINMENU.MYMOB);
            } else if (moduleId.equals(getResources().getString(R.string.MODULE_SETTING)) && !ClnEnv.isLoggedIn()) {
                loginFirstDialog(MAINMENU.SETTING);
            } else {
                openLiveChat();
            }
        });

        // change the theme according to the premier status
        ColorFilter filter;
        if (!ClnEnv.getSessionPremierFlag()) {
            filter = new PorterDuffColorFilter(Color.parseColor("#217EC0"), PorterDuff.Mode.MULTIPLY);
        } else {
            filter = new PorterDuffColorFilter(Color.parseColor("#FFFFFF"), PorterDuff.Mode.MULTIPLY);
        }
        aq.id(R.id.navbar_button_left).getImageView().setColorFilter(filter);
    }

    public void openLiveChat() {
        DialogHelper.createSimpleDialog(this, getString(R.string.contact_us_redirect_message), getString(R.string.btn_ok), (dialog, which) -> {
            Intent intentBrowser = new Intent(Intent.ACTION_VIEW, Uri.parse("https://wa.me/+85260331000"));
            startActivity(intentBrowser);
        }, getString(R.string.btn_cancel));
// Event Tracker
//        FAWrapper.getInstance().sendFAEvents(me, R.string.CONST_GA_CATEGORY_USERLV, R.string.CONST_GA_ACTION_BTN, R.string.CONST_GA_LABEL_OPEN_LIVECHAT, "<" + moduleId + ">", false);
//        if (debug) Log.d("MODULE ID", "openLiveChat: " + moduleId);
//        //open live chat
//        if (ClnEnv.getChatIsMaintenance() != null) {
//            if (!"Y".equalsIgnoreCase(ClnEnv.getChatIsMaintenance())) {
//                LiveChatHelper.getInstance(me, me).showWebView(me, moduleId);
//            } else {
//                DialogHelper.createSimpleDialog(me, getString(R.string.LIVECHAT_UNAVAILABLE));
//            }
//        } else {
//            doHelloFromChatMA = true;
//            APIsManager.doHelo(me);
//        }
    }

    public void openLiveChat(String tempModuleId) {
        // Event Tracker
//        FAWrapper.getInstance().sendFAEvents(me, R.string.CONST_GA_CATEGORY_USERLV, R.string.CONST_GA_ACTION_BTN, R.string.CONST_GA_LABEL_OPEN_LIVECHAT, "<" + moduleId + ">", false);
//        LiveChatHelper.getInstance(me, me).showWebView(me, tempModuleId);
        DialogHelper.createSimpleDialog(this, getString(R.string.contact_us_redirect_message), getString(R.string.btn_ok), (dialog, which) -> {
            Intent intentBrowser = new Intent(Intent.ACTION_VIEW, Uri.parse("https://wa.me/+85260331000"));
            startActivity(intentBrowser);
        }, getString(R.string.btn_cancel));
    }

    protected void initUI2() {

    }

    @Override
    protected void onNewIntent(Intent intent) {
        if (debug) Log.i(this.getClass().toString(), "onNewIntent");
        super.onNewIntent(intent);
        isZh = "zh".equalsIgnoreCase(getResString(R.string.myhkt_lang));
        ClnEnv.updateUILocale(getBaseContext(), ClnEnv.getAppLocale(getBaseContext()));
    }

    @Override
    protected void onResume() {
        super.onResume();
        isActivityActive = true;
        me = this;
        isZh = "zh".equalsIgnoreCase(getResString(R.string.myhkt_lang));

        // change the theme according to the premier status
        setCustomNavBarLeft();
        setCustomNavBarRight();
    }

    @Override
    protected void onPause() {
        isActivityActive = false;
        super.onPause();
    }

    private void setCustomNavBarLeft() {
        ColorFilter filter;
        if (ClnEnv.getSessionPremierFlag() && !ClnEnv.isMyMobFlag()) {
            filter = new PorterDuffColorFilter(Color.parseColor("#FFFFFF"), PorterDuff.Mode.SRC_ATOP);
        } else {
            filter = new PorterDuffColorFilter(Color.parseColor("#217EC0"), PorterDuff.Mode.SRC_ATOP);
        }
        aq.id(R.id.navbar_button_left).getImageView().setColorFilter(filter);
    }

    private void setCustomNavBarRight() {
        Boolean isPause = LiveChatHelper.getInstance(me, me).isPause;
        setLiveChangeIcon(isPause);
    }

    public void setLiveChangeIcon(Boolean isPause) {
        if (isPause) {
            if (aq != null) {
                aq.id(R.id.navbar_button_right).visibility(View.VISIBLE);
                aq.navBarButton(R.id.navbar_button_right, R.drawable.chat);
                ColorFilter filter = new PorterDuffColorFilter(Color.parseColor("#DB9200"), PorterDuff.Mode.MULTIPLY);
                aq.id(R.id.navbar_button_right).getImageView().setColorFilter(filter);
            }
        } else {
            if (aq != null) {
                if (isLiveChatShown) {
                    aq.id(R.id.navbar_button_right).visibility(View.VISIBLE);
                } else {
                    aq.id(R.id.navbar_button_right).visibility(View.INVISIBLE);
                }
                aq.navBarButton(R.id.navbar_button_right, R.drawable.chat);
                aq.id(R.id.navbar_button_right).getImageView().clearColorFilter();
                //Premier: white , Non-Premier : blue
                if (debug) Log.i("TAG", ClnEnv.getSessionPremierFlag() + "");

                ColorFilter filter;
                if (ClnEnv.getSessionPremierFlag() && !ClnEnv.isMyMobFlag()) {
                    filter = new PorterDuffColorFilter(Color.parseColor("#FFFFFF"), PorterDuff.Mode.MULTIPLY);
                } else {
                    filter = new PorterDuffColorFilter(Color.parseColor("#217EC0"), PorterDuff.Mode.MULTIPLY);
                }
                aq.id(R.id.navbar_button_right).getImageView().setColorFilter(filter);
            }
        }
    }

    protected void onStop() {
        super.onStop();
        cleanupUI();
    }

    // Free all references to the UI elements
    protected void cleanupUI() {

    }

    public final void displayDialog(Context cxt, String message) {
        if (isActivityActive) {
            if (alertDialog != null) {
                alertDialog.dismiss();
            }

            AlertDialog.Builder builder = new Builder(cxt);
            builder.setMessage(message);
            builder.setPositiveButton(Utils.getString(cxt, R.string.btn_ok), (dialog, which) -> {
                dialog.dismiss();
                alertDialog = null;
            });

            alertDialog = builder.create();
            alertDialog.show();
        }
    }

    // Login First Dialog
    protected final void loginFirstDialog(final MAINMENU clickItem) {
        if (isActivityActive) {
            Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
            Bundle rbundle = new Bundle();
            rbundle.putSerializable("CLICKBUTTON", clickItem);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            intent.putExtras(rbundle);
            finish();
            startActivity(intent);
            overridePendingTransition(R.anim.right_slide_in, R.anim.left_slide_out);
        }
    }

    public String getResString(int res) {
        return Utils.getString(this, res);
    }

    public void onSuccess(APIsResponse response) {
        if (response != null) {
            if (APIsManager.HELO.equals(response.getActionTy()) && doHelloFromChatMA) {
                if (debug) debugLog("DOHELO", "CHAT_MA dohelo sucess");
                doHelloFromChatMA = false;
                if (!"Y".equalsIgnoreCase(ClnEnv.getChatIsMaintenance())) {
                    if (isActivityActive)
                        LiveChatHelper.getInstance(me, me).showWebView(me, moduleId);
                } else {
                    if (isActivityActive)
                        DialogHelper.createSimpleDialog(me, getString(R.string.LIVECHAT_UNAVAILABLE));
                }
            }
        }
    }

    public void onFail(APIsResponse response) {
        if (response != null) {
            if (APIsManager.HELO.equals(response.getActionTy()) && doHelloFromChatMA) {
                doHelloFromChatMA = false;
                // General Error Message
                if (!"".equals(response.getMessage()) && response.getMessage() != null) {
                    if (isActivityActive)
                        DialogHelper.createSimpleDialog(this, response.getMessage());
                } else {
                    if (isActivityActive)
                        DialogHelper.createSimpleDialog(this, ClnEnv.getRPCErrMsg(this, response.getReply().getCode()));
                }
            }
        }
    }

    public void debugLog(String tag, String msg) {
        if (debug) Log.i(tag, msg);
    }

    @Override
    public void onActivityResult(final int requestCode, final int resultCode, final Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        if (requestCode == LiveChatHelper.mRequestCodeFilePicker) {
            if (resultCode == Activity.RESULT_OK) {
                if (intent != null) {
                    if (LiveChatHelper.mFileUploadCallbackFirst != null) {
                        LiveChatHelper.mFileUploadCallbackFirst.onReceiveValue(intent.getData());
                        LiveChatHelper.mFileUploadCallbackFirst = null;
                    } else if (LiveChatHelper.mFileUploadCallbackSecond != null) {
                        Uri[] dataUris;
                        try {
                            dataUris = new Uri[]{Uri.parse(intent.getDataString())};
                        } catch (Exception e) {
                            dataUris = null;
                        }

                        LiveChatHelper.mFileUploadCallbackSecond.onReceiveValue(dataUris);
                        LiveChatHelper.mFileUploadCallbackSecond = null;
                    }
                }
            } else {
                if (LiveChatHelper.mFileUploadCallbackFirst != null) {
                    LiveChatHelper.mFileUploadCallbackFirst.onReceiveValue(null);
                    LiveChatHelper.mFileUploadCallbackFirst = null;
                } else if (LiveChatHelper.mFileUploadCallbackSecond != null) {
                    LiveChatHelper.mFileUploadCallbackSecond.onReceiveValue(null);
                    LiveChatHelper.mFileUploadCallbackSecond = null;
                }
            }
        }
    }
}
