package com.pccw.myhkt.activity;

import android.content.Context;
import android.content.SharedPreferences;
import androidx.test.espresso.intent.Intents;
import androidx.test.rule.ActivityTestRule;
import androidx.test.rule.GrantPermissionRule;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import com.pccw.myhkt.AccountHelper;
import com.pccw.myhkt.ClnEnv;
import com.pccw.myhkt.R;
import com.pccw.myhkt.base.BaseTest;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static androidx.test.platform.app.InstrumentationRegistry.getInstrumentation;

@Ignore
@RunWith(AndroidJUnit4.class)
public class ChangePasswordMyProfileActivityTest extends BaseTest {
    private Context context;
    SharedPreferences shared;

    @Rule
    public ActivityTestRule<MainMenuActivity> activityTestRule = new ActivityTestRule<>(MainMenuActivity.class);
    @Rule
    public GrantPermissionRule permissionRule = GrantPermissionRule.grant(android.Manifest.permission.WRITE_EXTERNAL_STORAGE);

    @Before
    public void setUp() {
        context = getInstrumentation().getTargetContext();
        shared = context.getSharedPreferences(
                "myhkt",
                Context.MODE_PRIVATE);
        Intents.init();

        loginUser(context, shared,
                AccountHelper.getUATAccount7().getUsername(),
                AccountHelper.getUATAccount7().getPassword(),
                true);

        gotoMainMenuItem(3);
        clickView(R.id.myprofloginid_changepwd);
    }

    @After
    public void tearDown() {
        if(ClnEnv.isLoggedIn()){
            executeLogout(context);
        }
        Intents.release();
    }

    @Test
    public void testTabs() {
        clickString(context, R.string.myhkt_myservicelist_tabname);
        delay(TestTimeouts.DIALOG_TIMEOUTS);
        checkViewIsDisplayed(R.id.myprofservlist_btn_update);
        clickString(context, R.string.myhkt_mycontactinfo_tabname);
        delay(TestTimeouts.API_TIMEOUTS, waitForViewEnabled());
        checkViewIsDisplayed(R.id.myprofcontact_btn_update);
        clickString(context, R.string.myhkt_myloginprofile_tabname);
        delay(TestTimeouts.DIALOG_TIMEOUTS);
        checkViewIsDisplayed(R.id.myprofloginid_btn_update);
    }

    @Test
    public void testLiveChat(){
        checkLiveChat(context,R.string.LIVE_CHAT_DISCLAIMER,
                R.id.myproflogin_changepw_btn_update);
    }

    @Test
    public void testBackBtn(){
        checkBackBtn(MainMenuActivity.class.getName(),3);
    }

    @Test
    public void testDisplayUI(){
        checkStringIsDisplayed(context, R.string.myhkt_myprof_title);
        backBtnExist();
        checkLiveChatBtnIsDisplayed();
        checkStringIsDisplayed(context, R.string.myhkt_myloginprofile_tabname);
        checkStringIsDisplayed(context, R.string.myhkt_myservicelist_tabname);
        checkStringIsDisplayed(context, R.string.myhkt_mycontactinfo_tabname);
        checkStringIsDisplayed(context, R.string.myhkt_myprof_chgpwd);

        checkStringIsDisplayed(context, R.string.myhkt_myprof_curpwd);
        checkStringIsDisplayed(context, R.string.myhkt_myprof_newpwd);
        checkStringIsDisplayed(context, R.string.myhkt_myprof_retpwd);

        checkViewString(R.id.reg_input_layoutet,R.id.myproflogin_changepw_name_et,"");
        checkViewString(R.id.reg_input_layoutet,R.id.myproflogin_changepw_email_et,"");
        checkViewString(R.id.reg_input_layoutet,R.id.myproflogin_changepw_mobnum_et,"");

        checkStringHint(context, R.id.reg_input_layoutet,
                R.id.myproflogin_changepw_email_et, R.string.REGF_PWD_HINT);
    }

    @Test
    public void testMaskPassword(){
        typeTextView(R.id.reg_input_layoutet,R.id.myproflogin_changepw_name_et,"test 123");
        typeTextView(R.id.reg_input_layoutet,R.id.myproflogin_changepw_email_et,"test 123");
        typeTextView(R.id.reg_input_layoutet,R.id.myproflogin_changepw_mobnum_et,"test 123");

        clickView(R.id.reg_input_button_show_pwd,R.id.myproflogin_changepw_name_et);
        clickView(R.id.reg_input_button_show_pwd,R.id.myproflogin_changepw_email_et);
        clickView(R.id.reg_input_button_show_pwd,R.id.myproflogin_changepw_mobnum_et);

        checkViewPasswordVisible(R.id.reg_input_layoutet, R.id.myproflogin_changepw_name_et);
        checkViewPasswordVisible(R.id.reg_input_layoutet, R.id.myproflogin_changepw_email_et);
        checkViewPasswordVisible(R.id.reg_input_layoutet, R.id.myproflogin_changepw_mobnum_et);

        clickView(R.id.reg_input_button_show_pwd,R.id.myproflogin_changepw_name_et);
        clickView(R.id.reg_input_button_show_pwd,R.id.myproflogin_changepw_email_et);
        clickView(R.id.reg_input_button_show_pwd,R.id.myproflogin_changepw_mobnum_et);

        checkViewPasswordHidden(R.id.reg_input_layoutet, R.id.myproflogin_changepw_name_et);
        checkViewPasswordHidden(R.id.reg_input_layoutet, R.id.myproflogin_changepw_email_et);
        checkViewPasswordHidden(R.id.reg_input_layoutet, R.id.myproflogin_changepw_mobnum_et);
    }

    @Test
    public void testCancel(){
        typeTextView(R.id.reg_input_layoutet,R.id.myproflogin_changepw_name_et,"Aa12345");
        typeTextView(R.id.reg_input_layoutet,R.id.myproflogin_changepw_email_et,"Aa123456");
        typeTextView(R.id.reg_input_layoutet,R.id.myproflogin_changepw_mobnum_et,"Aa123456");

        clickView(R.id.myproflogin_changepw_btn_cancel);
        checkViewIsDisplayed(R.id.myprofloginid_btn_update);

        clickView(R.id.myprofloginid_changepwd);
        checkViewIsDisplayed(R.id.myproflogin_changepw_header);

        checkViewString(R.id.reg_input_layoutet,R.id.myproflogin_changepw_name_et,"");
        checkViewString(R.id.reg_input_layoutet,R.id.myproflogin_changepw_email_et,"");
        checkViewString(R.id.reg_input_layoutet,R.id.myproflogin_changepw_mobnum_et,"");
    }

    @Test
    public void testUpdateChangePassword(){
        clearTextView(R.id.reg_input_layoutet,R.id.myproflogin_changepw_name_et);
        clearTextView(R.id.reg_input_layoutet,R.id.myproflogin_changepw_email_et);
        clearTextView(R.id.reg_input_layoutet,R.id.myproflogin_changepw_mobnum_et);

        clickView(R.id.myproflogin_changepw_btn_update);
        delay(TestTimeouts.DIALOG_TIMEOUTS,waitForText(btn_OK));
        checkDialogStringIsDisplayed(context,R.string.PAMM_NAPWD);
        clickOK();

        typeTextView(R.id.reg_input_layoutet,R.id.myproflogin_changepw_email_et,"Aa12345");
        typeTextView(R.id.reg_input_layoutet,R.id.myproflogin_changepw_mobnum_et,"Aa12345");

        clickView(R.id.myproflogin_changepw_btn_update);
        delay(TestTimeouts.DIALOG_TIMEOUTS,waitForText(btn_OK));
        checkDialogStringIsDisplayed(context,R.string.PAMM_IVCURRPWD);
        clickOK();

        typeTextView(R.id.reg_input_layoutet,R.id.myproflogin_changepw_name_et,"Aa12345");
        clearTextView(R.id.reg_input_layoutet,R.id.myproflogin_changepw_mobnum_et);

        clickView(R.id.myproflogin_changepw_btn_update);
        delay(TestTimeouts.DIALOG_TIMEOUTS,waitForText(btn_OK));

        checkDialogStringIsDisplayed(context,R.string.PAMM_IVCFMPWD);
        clickOK();

        typeTextView(R.id.reg_input_layoutet,R.id.myproflogin_changepw_email_et,"aa12345");
        typeTextView(R.id.reg_input_layoutet,R.id.myproflogin_changepw_mobnum_et,"aa12345");

        clickView(R.id.myproflogin_changepw_btn_update);
        delay(TestTimeouts.DIALOG_TIMEOUTS,waitForText(btn_OK));
        checkDialogStringIsDisplayed(context,R.string.REGM_ILPWD);
        clickOK();

        typeTextView(R.id.reg_input_layoutet,R.id.myproflogin_changepw_email_et,"Aa12345");
        typeTextView(R.id.reg_input_layoutet,R.id.myproflogin_changepw_mobnum_et,"aa12345");

        clickView(R.id.myproflogin_changepw_btn_update);
        delay(TestTimeouts.DIALOG_TIMEOUTS,waitForText(btn_OK));

        checkDialogStringIsDisplayed(context,R.string.PAMM_IVCFMPWD);
        clickOK();

        typeTextView(R.id.reg_input_layoutet,R.id.myproflogin_changepw_email_et,"Aa123456");
        typeTextView(R.id.reg_input_layoutet,R.id.myproflogin_changepw_mobnum_et,"Aa123456");

        clickView(R.id.myproflogin_changepw_btn_update);
        delay(TestTimeouts.API_TIMEOUTS,waitForText(btn_OK));
        checkDialogStringIsDisplayed(context,R.string.PAMM_DONE);
        clickOK();

        checkViewIsDisplayed(R.id.myprofloginid_btn_update);
        clickView(R.id.myprofloginid_changepwd);

        typeTextView(R.id.reg_input_layoutet,R.id.myproflogin_changepw_name_et,"Aa123456");
        typeTextView(R.id.reg_input_layoutet,R.id.myproflogin_changepw_email_et,"Aa12345");
        typeTextView(R.id.reg_input_layoutet,R.id.myproflogin_changepw_mobnum_et,"Aa12345");

        clickView(R.id.myproflogin_changepw_btn_update);
        delay(TestTimeouts.API_TIMEOUTS,waitForText(btn_OK));

        checkDialogStringIsDisplayed(context,R.string.PAMM_DONE);
        clickOK();

        clickView(R.id.myprofloginid_changepwd);
        checkViewIsDisplayed(R.id.myproflogin_changepw_header);

        typeTextView(R.id.reg_input_layoutet,R.id.myproflogin_changepw_name_et,"Aa12345");
        typeTextView(R.id.reg_input_layoutet,R.id.myproflogin_changepw_email_et,"Aa12345");
        typeTextView(R.id.reg_input_layoutet,R.id.myproflogin_changepw_mobnum_et,"Aa12345");

        clickView(R.id.myproflogin_changepw_btn_update);
        delay(TestTimeouts.DIALOG_TIMEOUTS);
        checkDialogStringIsDisplayed(context,R.string.PAMM_PWD_NO_CHANGE);
        clickOK();

        typeTextView(R.id.reg_input_layoutet,R.id.myproflogin_changepw_name_et,"Aa12345");
        typeTextView(R.id.reg_input_layoutet,R.id.myproflogin_changepw_email_et,"Aa12345#");
        typeTextView(R.id.reg_input_layoutet,R.id.myproflogin_changepw_mobnum_et,"Aa12345#");

        clickView(R.id.myproflogin_changepw_btn_update);
        delay(TestTimeouts.DIALOG_TIMEOUTS);

        checkDialogStringIsDisplayed(context,R.string.REGM_ILPWD);
        clickOK();
    }
}