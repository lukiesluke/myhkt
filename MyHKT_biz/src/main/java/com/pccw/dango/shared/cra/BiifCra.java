/*
    Crate for Bill Information
    
    NOTE: the Payment Info (Pyif) for LTS only contains the 
          following fields: 
          
          Payment Method (Cash/Autopay/Credit Card)
          Payer Detail (Name + Id Document)
          Credit Card Token
          Credit Card Expiry Date
          
          Other fields should be referenced in the Billing 
          Info (Biif).
    
    Keywords
    --------
    $URL: $
    $Rev: $
    $Date: $
    $Author: $
*/

package com.pccw.dango.shared.cra;

import java.io.Serializable;

import com.pccw.dango.shared.entity.Account;
import com.pccw.dango.shared.entity.Biif;
import com.pccw.dango.shared.entity.SubnRec;
import com.pccw.dango.shared.tool.MyTool;
import com.pccw.dango.shared.tool.RC;
import com.pccw.wheat.shared.tool.MiniRtException;
import com.pccw.wheat.shared.tool.Reply;
import com.pccw.wheat.shared.tool.Tool;

public class BiifCra extends BaseCraEx implements Serializable
{
    private static final long serialVersionUID = 5105734275440834646L;
    
    private String                  iLoginId;           /* Login ID                                      */
    private Account                 iAcct;              /* Account Information                           */
    private SubnRec                 iSubnRec;           /* Subscription Record                           */
    private Biif                    iBiif4Hkt;          /* Billing information for H                     */
    private Biif                    iPyif4Lts;          /* Payment information for H LTS                 */
    private Biif                    iBiif4Csl;          /* Billing information for C                     */
    
    private Biif                    oBiif4Hkt;          /* Billing information for H                     */
    private Biif                    oPyif4Lts;          /* Payment information for H LTS                 */
    private Biif                    oBiif4Csl;          /* Billing information for C                     */
    private String                  oCeksUrl;           /* URL to CEKS                                   */
    
    
    public static final String      BI_LANG_EN              = "EN";
    public static final String      BI_LANG_ZH              = "ZH";
    public static final String      BI_LANG_BI              = "BI";

    public static final String      LTS_BI_LANG_EN          = "E";
    public static final String      LTS_BI_LANG_ZH          = "C";
    public static final String      LTS_BI_LANG_BI          = "X";
    
    public static final String      MOB_BI_LANG_EN          = "ENG";
    public static final String      MOB_BI_LANG_ZH          = "CHN";
    public static final String      MOB_BI_LANG_BI          = "BILN";
    
    public static final String      MOBC_BI_LANG_EN         = "ENG";
    public static final String      MOBC_BI_LANG_ZH         = "CHI";

    /* WARNING, below is a bit confusing */
    public static final String      BILL_MEDIA_E            = "E"; /* Email Bill for Non-LTS            */
    public static final String      BILL_MEDIA_S            = "S"; /* Email Bill for LTS                */
    public static final String      LTS_BILL_MEDIA_P        = "P"; /* Paper Bill for LTS                */
    public static final String      BILL_MEDIA_X = "X"; /* Paper Bill + Email for LTS        */

    public static final String      LTS_PM_CASH             = "M"; /* LTS Payment Method - Cash         */
    public static final String      LTS_PM_CRCARD           = "C"; /* LTS Payment Method - Credit Card  */
    public static final String      LTS_PM_AUTOPAY          = "A"; /* LTS Payment Method - Autopay      */

    public static final String      WAIVE_PAPER_ACCT        = "W"; /* Will Waive PB for LTS             */
    
    public static final int         LTS_ADR_MAXLEN          = 100;
    public static final int         LTS_ADR_MAXTLEN         = 600;
    public static final int         MOB_ADR_MAXLEN          = 100;
    public static final int         MOB_ADR_MAXTLEN         = 500;
    public static final int         PCD_ADR_MAXLEN          = 50;
    public static final int         PCD_ADR_MAXTLEN         = 150;
    public static final int         MAX_EMAIL               = 64;    

    
    public static void main(String rArg[])
    {
        System.out.println(getVer());
        return;
    }
    
    
    public static String getVer()
    {
        return ("$URL: $, $Rev: $");
    }

    
    public BiifCra()
    {
        initAndClear();
    }


    protected void init()
    {
        super.init();
    }


    public void clear()
    {
        super.clear();
        
        clearILoginId();
        clearIAcct();
        clearISubnRec();
        clearIBiif4Hkt();
        clearIPyif4Lts();
        clearIBiif4Csl();
        clearOBiif4Hkt();
        clearOPyif4Lts();
        clearOBiif4Csl();
        clearOCeksUrl();
    }


    public BiifCra copyFrom(BiifCra rSrc)
    {
        super.copyFrom(rSrc);
        
        setILoginId(rSrc.getILoginId());
        setIAcct(rSrc.getIAcct());
        setISubnRec(rSrc.getISubnRec());
        setIBiif4Hkt(rSrc.getIBiif4Hkt().copyMe());
        setIPyif4Lts(rSrc.getIPyif4Lts().copyMe());
        setIBiif4Csl(rSrc.getIBiif4Csl().copyMe());
        setOBiif4Hkt(rSrc.getOBiif4Hkt());
        setOPyif4Lts(rSrc.getOPyif4Lts());
        setOBiif4Csl(rSrc.getOBiif4Csl());
        setOCeksUrl(rSrc.getOCeksUrl());
        
        return (this);
    }


    public BiifCra copyTo(BiifCra rDes)
    {
        rDes.copyFrom(this);
        return (rDes);
    }
    
    
    public BiifCra copyMe()
    {
        BiifCra                     rDes;

        rDes = new BiifCra();
        rDes.copyFrom(this);
        return (rDes);
    }


    public void clearILoginId()
    {
        iLoginId = "";
    }


    public void setILoginId(String rArg)
    {
        iLoginId = rArg;
    }


    public String getILoginId()
    {
        return (iLoginId);
    }


    public void clearIAcct()
    {
        iAcct = new Account();
    }


    public void setIAcct(Account rArg)
    {
        iAcct = rArg;
    }


    public Account getIAcct()
    {
        return (iAcct);
    }


    public void clearISubnRec()
    {
        iSubnRec = new SubnRec();
    }


    public void setISubnRec(SubnRec rArg)
    {
        iSubnRec = rArg;
    }


    public SubnRec getISubnRec()
    {
        return (iSubnRec);
    }

    
    public void clearIBiif4Hkt()
    {
        iBiif4Hkt = new Biif();
    }


    public void setIBiif4Hkt(Biif rArg)
    {
        iBiif4Hkt = rArg;
    }


    public Biif getIBiif4Hkt()
    {
        return (iBiif4Hkt);
    }

    
    public void clearIPyif4Lts()
    {
        iPyif4Lts = new Biif();
    }


    public void setIPyif4Lts(Biif rArg)
    {
        iPyif4Lts = rArg;
    }


    public Biif getIPyif4Lts()
    {
        return (iPyif4Lts);
    }


    public void clearIBiif4Csl()
    {
        iBiif4Csl = new Biif();
    }


    public void setIBiif4Csl(Biif rArg)
    {
        iBiif4Csl = rArg;
    }


    public Biif getIBiif4Csl()
    {
        return (iBiif4Csl);
    }


    public void clearOBiif4Hkt()
    {
        oBiif4Hkt = new Biif();
    }


    public void setOBiif4Hkt(Biif rArg)
    {
        oBiif4Hkt = rArg;
    }


    public Biif getOBiif4Hkt()
    {
        return (oBiif4Hkt);
    }


    public void clearOPyif4Lts()
    {
        oPyif4Lts = new Biif();
    }


    public void setOPyif4Lts(Biif rArg)
    {
        oPyif4Lts = rArg;
    }


    public Biif getOPyif4Lts()
    {
        return (oPyif4Lts);
    }


    public void clearOBiif4Csl()
    {
        oBiif4Csl = new Biif();
    }


    public void setOBiif4Csl(Biif rArg)
    {
        oBiif4Csl = rArg;
    }


    public Biif getOBiif4Csl()
    {
        return (oBiif4Csl);
    }


    public void clearOCeksUrl()
    {
        oCeksUrl = "";
    }


    public void setOCeksUrl(String rArg)
    {
        oCeksUrl = rArg;
    }


    public String getOCeksUrl()
    {
        return (oCeksUrl);
    }
    
    
    public static String convBillLang(String rBillLang)
    {   
        if (rBillLang.equals(LTS_BI_LANG_EN) ||
            rBillLang.equals(MOB_BI_LANG_EN))
        {
            return (BI_LANG_EN);
        }
        else if (rBillLang.equals(LTS_BI_LANG_ZH) ||
                rBillLang.equals(MOB_BI_LANG_ZH))
        {
            return (BI_LANG_ZH);
        }
        else if (rBillLang.equals(LTS_BI_LANG_BI) ||
                rBillLang.equals(MOB_BI_LANG_BI))
        {
            return (BI_LANG_BI);
        }
        
        return (new String());
    }

    
    public static String convBillLang4Lts(String rBillLang)
    {   
        if (rBillLang.equals(BI_LANG_EN))
        {
            return (LTS_BI_LANG_EN);
        }
        else if (rBillLang.equals(BI_LANG_ZH))
        {
            return (LTS_BI_LANG_ZH);
        }
        else if (rBillLang.equals(BI_LANG_BI))
        {
            return (LTS_BI_LANG_BI);
        }
        
        return (new String());
    }


    public static String convBillLang4Mob(String rBillLang)
    {   
        if (rBillLang.equals(BI_LANG_EN))
        {
            return (MOB_BI_LANG_EN);
        }
        else if (rBillLang.equals(BI_LANG_ZH))
        {
            return (MOB_BI_LANG_ZH);
        }
        else if (rBillLang.equals(BI_LANG_BI))
        {
            return (MOB_BI_LANG_BI);
        }
        
        return (new String());
    }
    
    
    public Reply validate()
    {
        Reply                       rRR;
    
        
        /* Billing Address */

        if (getISubnRec().lob.equals(SubnRec.LOB_PCD) ||
            getISubnRec().lob.equals(SubnRec.LOB_TV)  ||
            getISubnRec().lob.equals(SubnRec.LOB_IMS)) {
            
            getIBiif4Hkt().clearBillAdr4();
            getIBiif4Hkt().clearBillAdr5();
            getIBiif4Hkt().clearBillAdr6();
        }

        getIBiif4Hkt().setBillAdr1(getIBiif4Hkt().getBillAdr1().toUpperCase());
        getIBiif4Hkt().setBillAdr2(getIBiif4Hkt().getBillAdr2().toUpperCase());
        getIBiif4Hkt().setBillAdr3(getIBiif4Hkt().getBillAdr3().toUpperCase());
        getIBiif4Hkt().setBillAdr4(getIBiif4Hkt().getBillAdr4().toUpperCase());
        getIBiif4Hkt().setBillAdr5(getIBiif4Hkt().getBillAdr5().toUpperCase());
        getIBiif4Hkt().setBillAdr6(getIBiif4Hkt().getBillAdr6().toUpperCase());
        
        rRR = validAdrChr(getIBiif4Hkt().getBillAdr1(),
                          getIBiif4Hkt().getBillAdr2(),
                          getIBiif4Hkt().getBillAdr3(),
                          getIBiif4Hkt().getBillAdr4(),
                          getIBiif4Hkt().getBillAdr5(),
                          getIBiif4Hkt().getBillAdr6());
       
        if (!rRR.isSucc()) return rRR;


        rRR = validAdrLen(getISubnRec().lob,
                          getIBiif4Hkt().getBillAdr1(),
                          getIBiif4Hkt().getBillAdr2(),
                          getIBiif4Hkt().getBillAdr3(),
                          getIBiif4Hkt().getBillAdr4(),
                          getIBiif4Hkt().getBillAdr5(),
                          getIBiif4Hkt().getBillAdr6());
        
        if (!rRR.isSucc()) return rRR;
        
        
        /* Billing Email */
        
        if (getISubnRec().lob.equals(SubnRec.LOB_LTS)) {
            
            rRR = validBiMed4Lts();
            if (rRR.isSucc()) rRR = validLang();

            if (rRR.isSucc()) {
                if (getIBiif4Hkt().getBillMedia().equals(BILL_MEDIA_S)) {
                    if (rRR.isSucc()) rRR = validEmail();
                    if (rRR.isSucc()) rRR = validSmsNtf();
                }
            }
            
            if (!rRR.isSucc()) return (rRR);
        }
        else if (getISubnRec().lob.equals(SubnRec.LOB_PCD) ||
                 getISubnRec().lob.equals(SubnRec.LOB_TV)  ||
                 getISubnRec().lob.equals(SubnRec.LOB_IMS)) {
            
            if (getIBiif4Hkt().getBillMedia().equals(BILL_MEDIA_E)) {
                rRR = validEmail();
            }
            
            if (!rRR.isSucc()) return (rRR);
        }
        else if (getISubnRec().lob.equals(SubnRec.LOB_MOB) ||
                 getISubnRec().lob.equals(SubnRec.LOB_IOI)) {
            
            if (getIBiif4Hkt().getBillMedia().equals(BILL_MEDIA_E)) {
                rRR = validEmail();
            }
            
            if (rRR.isSucc()) rRR = validLang();
            
            if (!rRR.isSucc()) return (rRR);
        }
        else {
            throw new MiniRtException("Unexpected LOB ("+getISubnRec().lob+")!");
        }
        
        return (Reply.getSucc());
    }
    
    
    public Reply validate4Csl()
    {
        Reply                       rRR;


        /* Billing Address */
        
        if (getIBiif4Csl().isPox()) {
            
            if (Tool.isNil(getIBiif4Csl().getPostalBx())) {
                
                return (new Reply(RC.BIIF_NIL_POX));
            }
            
            rRR = validAdrChr(getIBiif4Csl().getPostalBx(),
                              getIBiif4Csl().getPostalNm(),
                              getIBiif4Csl().getDistrict(),
                              getIBiif4Csl().getArea());
            
            if (!rRR.isSucc()) return rRR;
            
            getIBiif4Csl().setUnit("");
            getIBiif4Csl().setFlat("");
            getIBiif4Csl().setFloor("");
            getIBiif4Csl().setBlock("");
            getIBiif4Csl().setBuilding("");
            getIBiif4Csl().setStreetNm("");
            getIBiif4Csl().setStreetNum("");
            
        }
        else {
            
            if (Tool.isNil(getIBiif4Csl().getUnit())      &&
                Tool.isNil(getIBiif4Csl().getFlat())      &&
                Tool.isNil(getIBiif4Csl().getFloor())     &&
                Tool.isNil(getIBiif4Csl().getBlock())     &&
                Tool.isNil(getIBiif4Csl().getBuilding())  &&
                Tool.isNil(getIBiif4Csl().getSection())   &&
                Tool.isNil(getIBiif4Csl().getStreetNum()) &&
                Tool.isNil(getIBiif4Csl().getStreetNm())  &&
                Tool.isNil(getIBiif4Csl().getDistrict())  &&
                Tool.isNil(getIBiif4Csl().getArea())) {
                    
                return (new Reply(RC.BIIF_NIL_ADR));
            }
            
            rRR = validAdrChr(getIBiif4Csl().getUnit(),
                              getIBiif4Csl().getFlat(),
                              getIBiif4Csl().getFloor(),
                              getIBiif4Csl().getBlock(),
                              getIBiif4Csl().getBuilding(),
                              getIBiif4Csl().getSection(),
                              getIBiif4Csl().getStreetNum(),
                              getIBiif4Csl().getStreetNm(),
                              getIBiif4Csl().getDistrict(),
                              getIBiif4Csl().getArea());
  
            if (!rRR.isSucc()) return rRR;

            
            getIBiif4Csl().setPostalBx("");
            getIBiif4Csl().setPostalNm("");
        }
        
        
        /* Billing Email */
        rRR = validEmail4Csl();
        
        /* Billing Language */
        if (rRR.isSucc()) rRR = validLang4Csl();
        
        return (rRR);
    }

    
    public static Reply validAdrChr(String ... rAdrLine)
    {
        int                         rx, ri, rl;
        
        for (rx=0; rx<rAdrLine.length; rx++) {
            if (!Tool.isASC(rAdrLine[rx])) {
                return (new Reply(RC.BIIF_NA_ADR));
            }
        }
        
        return (Reply.getSucc());
    }
    
    public static Reply validAdrLen(String rLob, String ... rAdrLine)
    {
        int                         rCnt     = 0;
        int                         rMaxLen  = 0;
        int                         rMaxTLen = 0;
        
        if (rLob.equals(SubnRec.LOB_LTS))
        {
            rMaxLen  = LTS_ADR_MAXLEN;
            rMaxTLen = LTS_ADR_MAXTLEN;
        }
        else if (rLob.equals(SubnRec.LOB_MOB) ||
                 rLob.equals(SubnRec.LOB_IOI) ||
                 rLob.equals(SubnRec.WLOB_XMOB)) // added xmob to get pass iv_lob as xmob 
        {
            rMaxLen  = MOB_ADR_MAXLEN;
            rMaxTLen = MOB_ADR_MAXTLEN;
        } 

        else if (rLob.equals(SubnRec.LOB_PCD) ||
                 rLob.equals(SubnRec.LOB_TV)  ||
                 rLob.equals(SubnRec.LOB_IMS))
        {
            rMaxLen  = PCD_ADR_MAXLEN;
            rMaxTLen = PCD_ADR_MAXTLEN;
        } 
        else 
        {
            return (new Reply(RC.IV_LOB));
        }

        
        for (int r1=0; r1 < rAdrLine.length; r1++)
        {
            if (rAdrLine[r1].trim().length() > rMaxLen)
            {
                return (new Reply(RC.BIIF_IL_ADR));
            }
            rCnt = rCnt + rAdrLine[r1].trim().length();
        }
        
        if (rCnt == 0) {
            return (new Reply(RC.BIIF_NIL_ADR));
        }
        
        if (rCnt > rMaxTLen)
        {
            return (new Reply(RC.BIIF_IL_ADR));
        }
        
        return (Reply.getSucc());
    }
    
    
    public Reply validBiMed4Lts()
    {
        if (!getIBiif4Hkt().getBillMedia().equals(BILL_MEDIA_S) && 
            !getIBiif4Hkt().getBillMedia().equals(LTS_BILL_MEDIA_P)) {
            return (new Reply(RC.BIIF_IV_MEDIA));
        }

        return (Reply.getSucc());
    }
    
    
    public Reply validEmail()
    {
        getIBiif4Hkt().setBillEmail(getIBiif4Hkt().getBillEmail().toLowerCase());
        return (validEmailFmt(getIBiif4Hkt().getBillEmail()));
    }

    
    public Reply validEmail4Csl()
    {
        getIBiif4Csl().setBillEmail(getIBiif4Csl().getBillEmail().toLowerCase());
        return (validEmailFmt(getIBiif4Csl().getBillEmail()));
    }

    
    public static Reply validEmailFmt(String rEmail)
    {

        if (rEmail.isEmpty()) {
            return (new Reply(RC.BIIF_NIL_EMAIL));
        }

        if (!Tool.isASC(rEmail)) {
            return (new Reply(RC.BIIF_NA_EMAIL));
        }
        
        if (rEmail.length() > MAX_EMAIL) {
            return (new Reply(RC.BIIF_IL_EMAIL));
        }
        
        if (!MyTool.isVaEmail(rEmail)) {
            return (new Reply(RC.BIIF_IV_EMAIL));
        }
        
        return (Reply.getSucc());
    }
    
    
    public Reply validLang()
    {
        if (!getIBiif4Hkt().getBillLang().equals(BI_LANG_EN) &&
            !getIBiif4Hkt().getBillLang().equals(BI_LANG_ZH) &&
            !getIBiif4Hkt().getBillLang().equals(BI_LANG_BI)) {
            return (new Reply(RC.BIIF_IV_LANG));
        }
        
        return (Reply.getSucc());
    }
    
    
    public Reply validLang4Csl()
    {
        if (!getIBiif4Csl().getBillLang().equals(MOBC_BI_LANG_EN) &&
            !getIBiif4Csl().getBillLang().equals(MOBC_BI_LANG_ZH)) {
            return (new Reply(RC.BIIF_IV_LANG));
        }
        
        return (Reply.getSucc());
    }
    
    
    public Reply validSmsNtf()
    {
        /* SMS Notification could be optional */
        
        getIBiif4Hkt().setSmsNtfn(getIBiif4Hkt().getSmsNtfn().trim());
        if (!Tool.isNil(getIBiif4Hkt().getSmsNtfn())) {
            if (!MyTool.isVaMob(getIBiif4Hkt().getSmsNtfn())) {
                return (new Reply(RC.BIIF_IV_SMSNTF));
            }
        }
        
        return (Reply.getSucc());
    }
}
