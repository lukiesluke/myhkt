package com.pccw.myhkt.activity;

import android.content.Context;
import android.content.SharedPreferences;
import androidx.test.platform.app.InstrumentationRegistry;
import androidx.test.espresso.intent.Intents;
import androidx.test.rule.ActivityTestRule;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import com.pccw.myhkt.AccountHelper;
import com.pccw.myhkt.R;
import com.pccw.myhkt.base.BaseTest;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static androidx.test.platform.app.InstrumentationRegistry.getInstrumentation;
import static androidx.test.espresso.action.ViewActions.click;

@Ignore
@RunWith(AndroidJUnit4.class)
public class ServicePlanPCDTest extends BaseTest {

    private Context context;

    private SharedPreferences shared;
    SharedPreferences.Editor prefEditor;


    @Rule
    public ActivityTestRule<MainMenuActivity> activityTestRule =
            new ActivityTestRule<MainMenuActivity>(MainMenuActivity.class) {
                @Override
                protected void beforeActivityLaunched() {
                    clearSharedPrefs(InstrumentationRegistry.getTargetContext());
                    InstrumentationRegistry.getTargetContext().deleteDatabase("myhkt.db");
                    super.beforeActivityLaunched();
                }
            };

    private void clearSharedPrefs(Context context) {
        SharedPreferences prefs =
                context.getSharedPreferences("myhkt", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.clear();
        editor.commit();
    }

    @Before
    public void setUp() {
        context = getInstrumentation().getTargetContext();
        shared = context.getSharedPreferences(
                "myhkt",
                Context.MODE_PRIVATE);
//        Intents.init();
        clickLeftNavBar();
        findStringContaining(context, R.string.myhkt_LGIF_LOGIN).perform(click());
        loginTestSR1();
    }

    @After
    public void tearDown() {
        executeLogout(context);
    }

    @Test
    public void servicePlanNoServiceDetailTest() {
        clickViewInAdapter(R.id.servicelist_listView, 12);
        delay(10000);
        clickViewInAdapter(R.id.hkt_indicator_gridview1, 1);
        delay(10000);
    }

    @Test
    public void servicePlanHasServiceDetailTest() {
        clickViewInAdapter(R.id.servicelist_listView, 7);
        delay(20000);
        clickViewInAdapter(R.id.hkt_indicator_gridview1, 1);
        delay(10000);
    }

    private void loginTestSR1() {
        loginMyCredentialsUAT(AccountHelper.getUATAccount6().getUsername(), AccountHelper.getUATAccount6().getPassword(), shared);
        delay(3000);
        gotoMainMenuItem(0);
    }
}
