package com.pccw.myhkt.utils;


import static com.pccw.myhkt.utils.Constant.DEVICE_BRAND;
import static com.pccw.myhkt.utils.Constant.DEVICE_MANUFACTURE;
import static com.pccw.myhkt.utils.Constant.DEVICE_MODEL;
import static com.pccw.myhkt.utils.Constant.OS_ENVIRONMENT;
import static com.pccw.myhkt.utils.Constant.USER_TYPE_PRD;
import static com.pccw.myhkt.utils.Constant.USER_TYPE_UAT;

import android.app.Activity;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;

import com.google.firebase.analytics.FirebaseAnalytics;

public class FirebaseSetting {
    private static final String CONTACT_US = "contact_us";
    private static final String UPDATE_INFO = "update_info";
    private static final String ACCOUNT_STATUS = "account_status";
    private static final String ICON_NAME = "icon_name";
    private static final String BILL_STATEMENT = "bill_statement";
    private static final String TAB_PROFILE_NAME = "tab_profile_name";
    private static final String EVENT_MENU_CLICK = "menu_icon_click";
    private static final String EVENT_PROFILE = "profile_event";
    public static final String EVENT_CONTACT_US_WHATSAPP = "Contact_Us_WhatsApp";
    public static final String EVENT_CONTACT_US_E_FORM = "Contact_Us_eForm";
    public static final String EVENT_CONTACT_US = "Contact_Us_";
    private static final String EVENT_UPDATE_INFO = "update_info";
    private static final String EVENT_TERMINATE_ACCOUNT = "Terminate_MyHKT_Account";
    private static final String EVENT_ACCOUNT_REGISTRATION = "account_registration";
    private static final String BILL_DOWNLOAD_EVENT = "bill_statement_download";
    private static final String USER_PROPERTY_ENV = "Environment";
    private static final String ITEM_ACCT_REG = "Account Registration Successful";
    private final FirebaseAnalytics mFirebaseAnalytics;

    public FirebaseSetting(Context context) {
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(context);
        mFirebaseAnalytics.setAnalyticsCollectionEnabled(true);
    }

    public void onScreenView(@NonNull Activity activity, String screenName) {
        mFirebaseAnalytics.setCurrentScreen(activity, screenName, activity.getClass().getSimpleName());
    }

    public void onMenuIconClickEvent(String iconName) {
        Bundle bundle = new Bundle();
        bundle.putString(ICON_NAME, iconName);
        bundle.putString(OS_ENVIRONMENT, Constant.USER_TYPE_OS);
        bundle.putString(DEVICE_MANUFACTURE, Build.MANUFACTURER);
        bundle.putString(DEVICE_BRAND, Build.BRAND);
        bundle.putString(DEVICE_MODEL, Build.MODEL);
        mFirebaseAnalytics.logEvent(EVENT_MENU_CLICK, bundle);
    }

    public void onBillDownload() {
        Bundle bundle = new Bundle();
        bundle.putString(BILL_STATEMENT, "Bill Statement Download");
        bundle.putString(OS_ENVIRONMENT, Constant.USER_TYPE_OS);
        bundle.putString(DEVICE_MANUFACTURE, Build.MANUFACTURER);
        bundle.putString(DEVICE_BRAND, Build.BRAND);
        bundle.putString(DEVICE_MODEL, Build.MODEL);
        mFirebaseAnalytics.logEvent(BILL_DOWNLOAD_EVENT, bundle);
    }

    public void onContactButtonTap(String eventName, String phoneNumber) {
        Bundle bundle = new Bundle();
        bundle.putString(CONTACT_US, "Contact Us - " + phoneNumber);
        bundle.putString(OS_ENVIRONMENT, Constant.USER_TYPE_OS);
        bundle.putString(DEVICE_MANUFACTURE, Build.MANUFACTURER);
        bundle.putString(DEVICE_BRAND, Build.BRAND);
        bundle.putString(DEVICE_MODEL, Build.MODEL);
        mFirebaseAnalytics.logEvent(eventName, bundle);
    }

    public void onUpdateInfoEvent(String context) {
        Bundle bundle = new Bundle();
        bundle.putString(UPDATE_INFO, context);
        mFirebaseAnalytics.logEvent(EVENT_UPDATE_INFO, bundle);
    }

    public void onTerminateAccount(String context) {
        Bundle bundle = new Bundle();
        bundle.putString(UPDATE_INFO, context);
        mFirebaseAnalytics.logEvent(EVENT_TERMINATE_ACCOUNT, bundle);
    }

    public void onAccountRegisterEvent() {
        Bundle bundle = new Bundle();
        bundle.putString(ACCOUNT_STATUS, ITEM_ACCT_REG);
        mFirebaseAnalytics.logEvent(EVENT_ACCOUNT_REGISTRATION, bundle);
    }

    public void onProfileEvent(String tabName) {
        Bundle bundle = new Bundle();
        bundle.putString(TAB_PROFILE_NAME, tabName);
        bundle.putString(OS_ENVIRONMENT, Constant.USER_TYPE_OS);
        bundle.putString(DEVICE_MANUFACTURE, Build.MANUFACTURER);
        bundle.putString(DEVICE_BRAND, Build.BRAND);
        bundle.putString(DEVICE_MODEL, Build.MODEL);
        mFirebaseAnalytics.logEvent(EVENT_PROFILE, bundle);
    }

    public void setScreenName(String className, String screenName) {
        Bundle bundle = new Bundle();
        bundle.putString(FirebaseAnalytics.Param.SCREEN_NAME, screenName);
        bundle.putString(FirebaseAnalytics.Param.SCREEN_CLASS, className);
        mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SCREEN_VIEW, bundle);
    }

    public void setUserEvent(String userType) {
        Bundle bundle = new Bundle();
        if (USER_TYPE_UAT.equalsIgnoreCase(userType)) {
            bundle.putString(USER_PROPERTY_ENV, USER_TYPE_UAT);
        } else if (USER_TYPE_PRD.equalsIgnoreCase(userType)) {
            bundle.putString(USER_PROPERTY_ENV, USER_TYPE_PRD);
        }

        bundle.putString(OS_ENVIRONMENT, Constant.USER_TYPE_OS);
        bundle.putString(DEVICE_MANUFACTURE, Build.MANUFACTURER);
        bundle.putString(DEVICE_BRAND, Build.BRAND);
        bundle.putString(DEVICE_MODEL, Build.MODEL);
        mFirebaseAnalytics.logEvent(USER_PROPERTY_ENV, bundle);
    }

    public void setPRDProperty() {
        mFirebaseAnalytics.setUserProperty(USER_PROPERTY_ENV, USER_TYPE_PRD);
    }

    public void setUATProperty() {
        mFirebaseAnalytics.setUserProperty(USER_PROPERTY_ENV, USER_TYPE_UAT);
    }
}
