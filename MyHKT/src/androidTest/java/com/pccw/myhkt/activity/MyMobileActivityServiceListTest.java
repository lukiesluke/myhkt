package com.pccw.myhkt.activity;

import android.content.Context;
import android.content.SharedPreferences;
import androidx.test.espresso.intent.Intents;
import androidx.test.rule.ActivityTestRule;
import androidx.test.rule.GrantPermissionRule;
import androidx.test.ext.junit.runners.AndroidJUnit4;
import android.view.View;

import com.pccw.myhkt.AccountHelper;
import com.pccw.myhkt.ClnEnv;
import com.pccw.myhkt.R;
import com.pccw.myhkt.base.BaseTest;
import com.pccw.myhkt.mymob.activity.MyMobileActivity;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static androidx.test.platform.app.InstrumentationRegistry.getInstrumentation;

@Ignore
@RunWith(AndroidJUnit4.class)
public class MyMobileActivityServiceListTest extends BaseTest {
    private Context context;
    SharedPreferences shared;

    @Rule
    public ActivityTestRule<MainMenuActivity> activityTestRule = new ActivityTestRule<>(MainMenuActivity.class);
    @Rule
    public GrantPermissionRule permissionRule = GrantPermissionRule.grant(android.Manifest.permission.WRITE_EXTERNAL_STORAGE);

    @Before
    public void setUp(){
        context = getInstrumentation().getTargetContext();
        shared = context.getSharedPreferences(
                "myhkt",
                Context.MODE_PRIVATE);
        Intents.init();
    }

    @After
    public void tearDown() {
        if(ClnEnv.isLoggedIn()){
            executeLogout(context);
        }
        Intents.release();
    }

    @Test
    public void testDisplayUI(){
        loginUAT();
        checkStringIsDisplayed(context,R.string.MYMOB_NAV_TITLE);
        backBtnExist();
        checkLiveChatBtnIsDisplayed();
        checkStringIsDisplayed(context,R.string.myhkt_mymob_desc_list);
        checkStringIsDisplayed(context,R.string.MYMOB_ADD_TITLE);
        checkStringIsDisplayed(context,R.string.MYMOB_LIST_TITLE);

        checkStringIsDisplayed("54730551");

        checkViewIsDisplayed(R.id.mymob_myhkt_list, R.id.adapter_mymob_accountlist_logo,1);

        // TODO: check if Display Name is not visible
        checkStringIsDisplayed(context, R.string.myhkt_btn_alias);
    }

    @Test
    public void testBackBtn(){
        loginUAT();
        int position = 4;
        checkBackBtn(MainMenuActivity.class.getName(),position);
    }

    @Test
    public void testLiveChatBtn(){
        loginUAT();
        checkLiveChat(context, R.string.LIVE_CHAT_DISCLAIMER,
                R.id.mymob_servicelist_add_btn);
    }

    @Test
    public void testSetDisplayButton(){
        loginUAT();
        clickView(R.id.mymob_edit_button);
        // TODO: check if all Display Name is visible
    }

    @Test
    public void testSetDisplaySwipe(){
        loginUAT();
        swipeLeftView(R.id.mymob_myhkt_list,1);
        delay(TestTimeouts.DIALOG_TIMEOUTS);
        checkStringIsDisplayed(context, R.string.MYMOB_BTN_ALIAS);
    }

    @Test
    public void testChangeDisplayName(){
        loginUAT();
        changeDisplayNameBaseTest(context, 1, "", R.id.mymob_myhkt_list,
                R.string.MYMOB_BTN_ALIAS, R.id.adapter_mymob_accountlist_edit,
                R.id.adapter_mymob_accountlist_alias, waitForText(btn_OK));
    }

    @Test
    public void testAddMobileNumberBtn(){
        loginUAT();
        clickView(R.id.mymob_servicelist_add_btn);
        checkViewVisibility(R.id.mymob_navbar, View.VISIBLE);
        clickLeftNavBar();
        checkStringIsDisplayed(context,R.string.MYMOB_LIST_TITLE);
    }

    @Test
    public void testMobileNumDetails(){
        loginUAT();
        clickViewInAdapter(R.id.mymob_myhkt_list,1);
        delay(TestTimeouts.API_TIMEOUTS,waitForText(btn_OK));
        clickOK();
        checkIntendedActivity(ServiceActivity.class.getName());
        clickLeftNavBar();
        checkIntendedActivity(MyMobileActivity.class.getName());
    }

    @Test
    public void testEmptyMobileService(){
        loginUAT2();
        checkStringIsDisplayed(context,R.string.myhkt_FORGOTPWD);
    }

    @Test
    public void testLoggedOut(){
        gotoMainMenuItem(4);
        checkStringIsDisplayed(context,R.string.myhkt_FORGOTPWD);
    }

    public void loginUAT(){
        clickLeftNavBar();
        clickString(context, R.string.myhkt_LGIF_LOGIN);
        loginMyCredentialsUAT(AccountHelper.getUATAccount8().getUsername(),
                AccountHelper.getUATAccount8().getPassword(), shared);
        gotoMainMenuItem(4);
    }

    public void loginUAT2(){
        clickLeftNavBar();
        clickString(context, R.string.myhkt_LGIF_LOGIN);
        loginMyCredentialsUAT(AccountHelper.getUATAccount7().getUsername(),
                AccountHelper.getUATAccount7().getPassword(), shared);
        gotoMainMenuItem(4);
    }

}