package com.pccw.myhkt.activity;

import android.content.Context;
import android.content.SharedPreferences;
import androidx.test.espresso.intent.Intents;
import androidx.test.rule.ActivityTestRule;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import com.pccw.myhkt.AccountHelper;
import com.pccw.myhkt.R;
import com.pccw.myhkt.base.BaseTest;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static androidx.test.platform.app.InstrumentationRegistry.getInstrumentation;
import static androidx.test.espresso.Espresso.onData;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static org.hamcrest.CoreMatchers.anything;

@Ignore
@RunWith(AndroidJUnit4.class)
public class RebootModemLoadingTest extends BaseTest {
    private Context context;
    SharedPreferences shared;


    @Rule
    public ActivityTestRule<MainMenuActivity> activityTestRule = new ActivityTestRule<>(MainMenuActivity.class);

    @Before
    public void setUp(){
        context = getInstrumentation().getTargetContext();
        shared = context.getSharedPreferences(
                "myhkt",
                Context.MODE_PRIVATE);
        Intents.init();
        clickLeftNavBar();
        findStringContaining(context, R.string.myhkt_LGIF_LOGIN).perform(click());
        loginMyCredentialsPRD(AccountHelper.getPRDAccount3().getUsername(),
                AccountHelper.getPRDAccount3().getPassword(), shared);
        gotoMainMenuItem(1);
    }

    @After
    public void tearDown() {
        clickLeftNavBar();
        executeLogout(context);
        Intents.release();
    }

    @Test
    public void testRebootModemNetvigatorLoading() {
        clickViewInAdapter(R.id.servicelist_listView, 1);
        delay(10000);
        clickViewInAdapter(R.id.hkt_indicator_gridview1, 1);
        findStringContaining(context, R.string.reboot_start_btn).perform(click());
        delay(10000);
        findComponentByID(R.id.navbar_leftdraw);
        findStringContaining("yck008");
        checkLiveChatBtnIsDisplayed();
        checkLiveChat(context, R.string.LIVE_CHAT_DISCLAIMER, R.id.navbar_button_left);
        findStringContaining(context, R.string.reboot_loading_description);
        delay(120000);
    }

    @Test
    public void testRebootModemNowTVLoading() {
        clickViewInAdapter(R.id.servicelist_listView, 2);
        delay(10000);
        clickViewInAdapter(R.id.hkt_indicator_gridview1, 1);
        findStringContaining(context, R.string.reboot_start_btn).perform(click());
        delay(10000);
        findComponentByID(R.id.navbar_leftdraw);
        findStringContaining("1111713434");
        checkLiveChatBtnIsDisplayed();
        checkLiveChat(context, R.string.LIVE_CHAT_DISCLAIMER, R.id.navbar_button_left);
        findStringContaining(context, R.string.reboot_loading_description);
        delay(120000);
    }

    @Test
    public void testRebootModemLoadingNetvigatorLiveChat() {
        clickViewInAdapter(R.id.servicelist_listView, 1);
        delay(10000);
        clickViewInAdapter(R.id.hkt_indicator_gridview1, 1);
        findStringContaining(context, R.string.reboot_start_btn).perform(click());
        delay(10000);
        checkLiveChat(context, R.string.LIVE_CHAT_DISCLAIMER, R.id.navbar_button_left);
        delay(120000);
    }

    @Test
    public void testRebootModemLoadingNowTVLiveChat() {
        clickViewInAdapter(R.id.servicelist_listView, 2);
        delay(10000);
        clickViewInAdapter(R.id.hkt_indicator_gridview1, 1);
        findStringContaining(context, R.string.reboot_start_btn).perform(click());
        delay(10000);
        checkLiveChat(context, R.string.LIVE_CHAT_DISCLAIMER, R.id.navbar_button_left);
        delay(120000);
    }

}