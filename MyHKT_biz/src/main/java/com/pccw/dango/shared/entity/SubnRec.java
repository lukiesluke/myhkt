/*
    Subscription Record

    Keywords
    --------
    $URL: $
    $Rev: $
    $Date: $
    $Author: $
 */

package com.pccw.dango.shared.entity;

import java.io.Serializable;

import com.pccw.dango.shared.tool.Msgs;
//import com.pccw.dango.server.table.SUBN;
import com.pccw.dango.shared.tool.MyTool;
import com.pccw.dango.shared.tool.RC;
import com.pccw.wheat.shared.tool.Const;
import com.pccw.wheat.shared.tool.MiniRtException;
import com.pccw.wheat.shared.tool.Reply;
import com.pccw.wheat.shared.tool.Tool;

import android.util.Log;

public class SubnRec implements Serializable
{
	private static final long serialVersionUID = -6958832272528382596L;

	public int                      rid;                /* Record Id                                     */
	public int                      custRid;            /* CUST rid                                      */
	public String                   cusNum;             /* BOM Customer Number                           */
	public String                   acctNum;            /* BOM Account Number                            */
	public String                   srvId;              /* BOM Service Id (Instant Id)                   */
	public String                   srvNum;             /* Service Number of the LOB                     */
	public String                   bsn;                /* BSN                                           */
	public String                   fsa;                /* FSA                                           */
	public String                   lob;                /* LOB                                           */
	public String                   wipCust;            /* WIP Indicator Y/N                             */
	public String                   sipSubr;            /* SIP Indicator Y/N                             */
	public String                   netLoginId;         /* Net Login Id                                  */
	public String                   aloneDialup;        /* Standalone Dialup Y/N                         */
	public String                   eyeGrp;             /* Eye Group                                     */
	public String                   ind2l2b;            /* 2L2B Indicator                                */
	public String                   refFsa2l2b;         /* Reference FSA for 2L2B                        */
	public String                   indPcd;             /* PCD Indicator                                 */
	public String                   indTv;              /* TV Indicator                                  */
	public String                   indEye;             /* Eye Indicator                                 */
	public String                   domainTy;           /* Domain Type: N/S = Netvigator/HK Star         */
	public String                   tos;                /* Type Of Service                               */
	public String                   datCd;              /* DAT Code                                      */
	public String                   tariff;             /* Tarrif                                        */
	public String                   priMob;             /* Primary Mobile Indicator                      */
	public String                   billagt;            /* Billing Agent                                 */
	public String                   systy;              /* System Type                                   */
	public String                   alias;              /* Alias                                         */
	public String                   assoc;              /* Associate                                     */
	public String                   createTs;           /* Record Create TS                              */
	public String                   createPsn;          /* Record Create Person                          */
	public String                   lastupdTs;          /* Last Update TS                                */
	public String                   lastupdPsn;         /* Last Update Person                            */
	public int                      rev;                /* Record Revision                               */

	/* Assign during runtime */
	public String                   instAdr;            /* Installation Address                         */
	public String                   storeTy;            /* Store Type                                   */
	public String                   ivr_pwd;            /* IVR Password                                  */
	public String                   auth_ph;            /* Authenticated Passphrase                      */
	public String                   acct_ty;            /* Account Type                                  */
	public boolean                  inMipMig;           /* Is In MIP Migration                           */ 
	 
	 
	public static final int         L_CUS_NUM      = 8;
	public static final int         L_ACCT_NUM     = 14;
	public static final int         L_SRV_ID       = 16;
	public static final int         L_SRV_NUM      = 32;
	public static final int         L_BSN          = 40;
	public static final int         L_FSA          = 16;
	public static final int         L_LOB          = 6;
	public static final int         L_WIP_CUST     = 1;
	public static final int         L_SIP_SUBR     = 1;
	public static final int         L_NET_LOGIN_ID = 32;
	public static final int         L_ALONE_DIALUP = 1;
	public static final int         L_EYE_GRP      = 16;
	public static final int         L_IND_2L2B     = 1;
	public static final int         L_REF_FSA_2L2B = 8;
	public static final int         L_IND_PCD      = 1;
	public static final int         L_IND_TV       = 1;
	public static final int         L_IND_EYE      = 1;
	public static final int         L_DOMAIN_TY    = 1;
	public static final int         L_TOS          = 4;
	public static final int         L_DAT_CD       = 4;
	public static final int         L_TARIFF       = 1;
	public static final int         L_PRI_MOB      = 1;
	public static final int         L_BILLAGT      = 16;
	public static final int         L_SYSTY        = 4;
	public static final int         L_ALIAS        = 16;
	public static final int         L_ASSOC        = 1;
	public static final int         L_CREATE_TS    = 14;
	public static final int         L_CREATE_PSN   = 40;
	public static final int         L_LASTUPD_TS   = 14;
	public static final int         L_LASTUPD_PSN  = 40;

	/* Should be no longer to have NE */
	public static final String      LOB_LTS         = "LTS";        /* LTS                               */
	public static final String      LOB_PCD         = "PCD";        /* PCD                               */
	public static final String      LOB_IMS         = "IMS";        /* IMS (CommOnly, =PCD)              */
	public static final String      LOB_TV          = "TV";         /* nowTV                             */
	public static final String      LOB_MOB         = "MOB";        /* H-SIM                             */
	public static final String      LOB_IOI         = "IOI";        /* 101 in H-System                   */
	public static final String      LOB_101         = "101";        /* 101 in C-System                   */
	public static final String      LOB_O2F         = "O2F";        /* C-SIM                             */

	/* Wildcard/Meta LOB, NOT stored in DB */ 
	public static final String      WLOB_XMOB       = "XMOB";       /* =MOB+NE (for MyMobile only)       */
	public static final String      WLOB_CSL        = "CSL";        /* =MOB+O2F (for Self-Regn)          */
	public static final String      WLOB_X101       = "X101";       /* =101+IOI (for Self-Regn)          */
	public static final String      WLOB_WIRELESS   = "WIRELESS";   /* =MOB+NE+10+O2F+IOI (4 Proj-D API) */

	public static final String      TOS_MOB         = "3G";         /* only for Mobile                   */
	public static final String      TOS_IMS         = "FSA";        /* only for IMS/TV                   */
	public static final String      TOS_CSL         = "CSL";        /* only for 101/O2F                  */
	public static final String      TOS_LTS_TEL     = "TEL";        /* LTS - DEL                         */
	public static final String      TOS_LTS_MOB     = "MOB";        /* LTS - 0060 on Mobile              */
	public static final String      TOS_LTS_ITS     = "ITS";        /* LTS - Calling Card                */

	public static final String      TOS_LTS_ONC     = "ONC";        /* LTS - One Call                    */
	public static final String      TOS_LTS_ICF     = "ICF";        /* LTS - Int'l C/F                   */
	public static final String      TOS_LTS_EYE     = "EYE";        /* LTS - Eye                         */

	public static final String      TOS_LTS_OCM     = "OCM";        /* LTS - One Communication (CommOnly)*/

	/* Billing System Types */
	public static final String      SYSTY_MOB       = "MOB";
	public static final String      SYSTY_LTS       = "DRG";
	public static final String      SYSTY_IMS       = "IMS";
	public static final String      SYSTY_CSL       = "CSL";

	public static final String      WIP_Y           = "Y";          /* P + W                             */
	public static final String      WIP_P           = "P";          /* In Person                         */ 
	public static final String      WIP_W           = "W";          /* Written                           */

	public static final String      DTY_NETVIGATOR  = "N";          /* netvigator.com                    */
	public static final String      DTY_HKSTAR      = "S";          /* hkstar.com                        */
	public static final String      DTY_NO_DOMAIN   = "P";          /* No Domain (4 Alone TV)            */
	public static final String      DTY_ATWORK      = "W";          /* @work (CommOnly)                  */
	public static final String      DTY_ONLAN       = "L";          /* ONLAN (CommOnly)                  */

	public static final String      TARF_RESIDENTAL = "R";
	public static final String      TARF_BUSINESS   = "B";

	public static final String      DATC_LTS_DEL    = "DEL";        /* LTS - Dir Exch Line               */
	public static final String      DATC_LTS_NCR    = "NCR";        /* LTS - NCR                         */
	public static final String      DATC_LTS_HUNT   = "HUNT";       /* LTS - Hunting                     */
	public static final String      DATC_LTS_CITN   = "CITN";       /* LTS - City Net                    */
	public static final String      DATC_LTS_MOB    = "MOB";        /* LTS - 0060 Mob                    */
	public static final String      DATC_LTS_TEL    = "TEL";        /* LTS - 0060 Fixed                  */
	public static final String      DATC_LTS_ITCC   = "ITCC";       /* LTS - Calling Card                */

	public static final String      PRMB_PRI        = "P";          /* Primary SIM                       */
	public static final String      PRMB_SEC        = "S";          /* Secondary SIM                     */
	public static final String      PRMB_NA         = "";           /* Non-MDP/MUP SIM                   */

	public static final String      DMA_NETVIGATOR  = "@netvigator.com";
	public static final String      DMA_HKSTAR      = "@hkstar.com";
	public static final String      DMA_ATWORK      = "@biznetvigator.com";
	public static final String      DMA_ONLAN       = "@biznetvigator.com";

	public static final String      SRVID_ZOMBIE    = "~ZOMBIE";

	/* Values for Storage Type */
	public static final String      STTY_SN         = "SN";         /* Derived From SUBN                 */
	public static final String      STTY_AD         = "AD";         /* Add-On but in DB                  */
	public static final String      STTY_AO         = "AO";         /* Add-On in Client/Session Only     */

	/* Length of account numbers of various Systems */
    public static final int         IMS_ACCTNUM_LEN = 10;
	public static final int         TV_ACCTNUM_LEN  = 10;
	public static final int         PCD_ACCTNUM_LEN = 10;
	public static final int         CSL_ACCTNUM_LEN = 8;

	/* Values for Account Type */
	public static final String      ATTY_C          = "C"; /* Corporate             */
	public static final String      ATTY_I          = "I"; /* Individual            */


	public static void main(String rArg[])
	{
		System.out.println(getVer());
		return;
	}


	public static String getVer()
	{
		return ("$URL: $, $Rev: $");
	}


	public SubnRec()
	{
		initAndClear();
	}


	final void initAndClear()
	{
		init();
		clear();
	}


	protected void init()
	{
	}


	public void clear()
	{
		/* from Table */
		rid             = 0;
		custRid         = 0;
		cusNum          = "";
		acctNum         = "";
		srvId           = "";
		srvNum          = "";
		bsn             = "";
		fsa             = "";
		lob             = "";
		wipCust         = "";
		sipSubr         = "";
		netLoginId      = "";
		aloneDialup     = "";
		eyeGrp          = "";
		ind2l2b         = "";
		refFsa2l2b      = "";
		indPcd          = "";
		indTv           = "";
		indEye          = "";
		domainTy        = "";
		tos             = "";
		datCd           = "";
		tariff          = "";
		priMob          = "";
		billagt         = "";
		systy           = "";
		alias           = "";
		assoc           = "";
		createTs        = "";
		createPsn       = "";
		lastupdTs       = "";
		lastupdPsn      = "";
		rev             = 0;

		/* Runtime Variables */
        storeTy         = "";
        instAdr         = "";
        ivr_pwd         = "";
        auth_ph         = "";
        acct_ty         = "";
        inMipMig        = false;
	}


	public SubnRec copyFrom(SubnRec rSrc)
	{
		rid             = rSrc.rid;
		custRid         = rSrc.custRid;
		cusNum          = rSrc.cusNum;
		acctNum         = rSrc.acctNum;
		srvId           = rSrc.srvId; 
		srvNum          = rSrc.srvNum;
		bsn             = rSrc.bsn;
		fsa             = rSrc.fsa;
		lob             = rSrc.lob;
		wipCust         = rSrc.wipCust;
		sipSubr         = rSrc.sipSubr;
		netLoginId      = rSrc.netLoginId;
		aloneDialup     = rSrc.aloneDialup;
		eyeGrp          = rSrc.eyeGrp;
		ind2l2b         = rSrc.ind2l2b;
		refFsa2l2b      = rSrc.refFsa2l2b;
		indPcd          = rSrc.indPcd;
		indTv           = rSrc.indTv;
		indEye          = rSrc.indEye;
		domainTy        = rSrc.domainTy;
		tos             = rSrc.tos;
		datCd           = rSrc.datCd;
		tariff          = rSrc.tariff;
		priMob          = rSrc.priMob;
		billagt         = rSrc.billagt;
		systy           = rSrc.systy;
		alias           = rSrc.alias;
		assoc           = rSrc.assoc;
		createTs        = rSrc.createTs;
		createPsn       = rSrc.createPsn;
		lastupdTs       = rSrc.lastupdTs;
		lastupdPsn      = rSrc.lastupdPsn;
		rev             = rSrc.rev;

		storeTy         = rSrc.storeTy;
	    instAdr         = rSrc.instAdr;
	    ivr_pwd         = rSrc.ivr_pwd;
	    auth_ph         = rSrc.auth_ph;
	    acct_ty         = rSrc.acct_ty;
	    inMipMig        = rSrc.inMipMig;

		return (this);
	}


	public SubnRec copyTo(SubnRec rDes)
	{
		rDes.copyFrom(this);
		return (rDes);
	}


	public SubnRec copyMe()
	{
		SubnRec                     rDes;

		rDes = new SubnRec();
		rDes.copyFrom(this);
		return (rDes);
	}


	public void trim()
	{
		cusNum          = cusNum.trim();
		acctNum         = acctNum.trim();
		srvId           = srvId.trim();
		srvNum          = srvNum.trim();
		bsn             = bsn.trim();
		fsa             = fsa.trim();
		lob             = lob.trim();
		wipCust         = wipCust.trim();
		sipSubr         = sipSubr.trim();
		netLoginId      = netLoginId.trim();
		aloneDialup     = aloneDialup.trim();
		eyeGrp          = eyeGrp.trim();
		ind2l2b         = ind2l2b.trim();
		refFsa2l2b      = refFsa2l2b.trim();
		indPcd          = indPcd.trim();
		indTv           = indTv.trim();
		indEye          = indEye.trim();
		domainTy        = domainTy.trim();
		tos             = tos.trim();
		datCd           = datCd.trim();
		tariff          = tariff.trim();
		priMob          = priMob.trim();
		billagt         = billagt.trim();
		systy           = systy.trim();
		alias           = alias.trim();
		assoc           = assoc.trim();
		createTs        = createTs.trim();
		createPsn       = createPsn.trim();
		lastupdTs       = lastupdTs.trim();
		lastupdPsn      = lastupdPsn.trim();

		/* No need to trim ivr_pwd */
		storeTy         = storeTy.trim();
		instAdr         = instAdr.trim();
		auth_ph         = auth_ph.trim();
	    acct_ty         = acct_ty.trim();
	}


	public Reply verifyBaseInput()
	{
		Reply                       rRR;

		trim();

		rRR = verifyLob();
		if (rRR.isSucc()) rRR = verifyAcctNum();
		if (rRR.isSucc()) rRR = verifySrvNum();

		return (rRR);
	}


	public Reply verifySysty()
	{
		/*
            Normally not for Interactive Data Input Check
            Only used for API.
		 */

		 if (!Tool.isInParm(systy, SYSTY_LTS, SYSTY_IMS, SYSTY_MOB, SYSTY_CSL)) {
			 return (new Reply(RC.SUBN_IVSYSTY));
		 }

		return (Reply.getSucc());
	}


	public Reply verifyLob()
	{
		/*
            Normally not for Interactive Data Input Check
            Only used for API.
		 */

		if (!Tool.isInParm(lob, LOB_LTS, LOB_PCD, LOB_IMS, LOB_TV, LOB_MOB, LOB_IOI, LOB_101, LOB_O2F)) { 
			return (new Reply(RC.SUBN_IVLOB));
		}

		return (Reply.getSucc());
	}


	public Reply verifyLob4Reg()
	{
		/* Accept Wild Card - See SignupMdu */

		if (!Tool.isInParm(lob, LOB_LTS, LOB_PCD, LOB_IMS, LOB_TV, WLOB_CSL, WLOB_X101)) {
			return (new Reply(RC.SUBN_IVLOB));
		}

		return (Reply.getSucc());
	}


	public Reply verifyAcctNum()
	{
		/*
            Since CSP-CR2014008, when Registration,
            the Account Number can be Zero - except for NowTV
		 */

		if (!Tool.isASC(acctNum, 0, L_ACCT_NUM)) {
			return (new Reply(RC.SUBN_NLACCTNUM));
		}

		if (acctNum.length() > 0) {
			if (!Tool.isDig(acctNum)) {
				return (new Reply(RC.SUBN_IVACCTNUM));
			}
		}

		return (Reply.getSucc());
	}


	public Reply verifySrvNum()
	{
		if (!Tool.isASC(srvNum, 0, L_SRV_NUM)) {
			return (new Reply(RC.SUBN_NLACCTNUM));
		}

		return (Reply.getSucc());
	}


	public Reply verify4Registration()
	{
		Reply                       rRC;

		rRC = verifyLob4Reg();
		if (!rRC.isSucc()) return (rRC);

		if (lob.equals(LOB_LTS)) {
			if (acctNum.length() != 0) {
				return (new Reply(RC.SUBN_NONEED_ACCTNUM));
			}

			return (verifySrvNumLTS());
		}
		else if (lob.equals(LOB_PCD)) {
			if (acctNum.length() != 0) {
				return (new Reply(RC.SUBN_NONEED_ACCTNUM));
			}

			return (verifySrvNumPCD());
		}
		else if (lob.equals(LOB_IMS)) {
			if (srvNum.length() != 0) {
				return (new Reply(RC.SUBN_NONEED_SRVNUM));
			}
			
			return (verifyAcctNumIMS());
		}
		else if (lob.equals(LOB_TV)) {
			/** Andy Add */
			if (!Tool.isDig(acctNum)) {
				return (new Reply(RC.SUBN_IVACCTNUM));
			}
			
			if (acctNum.length() != TV_ACCTNUM_LEN) {
				return (new Reply(RC.SUBN_IVAN_TV));
			}
			
			if (srvNum.length() != 0) {
				return (new Reply(RC.SUBN_NONEED_SRVNUM));
			}

			return (verifyAcctNumTV());
		}
		else if (lob.equals(WLOB_CSL)) {
			if (acctNum.length() != 0) {
				return (new Reply(RC.SUBN_NONEED_ACCTNUM));
			}

			return (verifySrvNumMOB());
		}
		else if (lob.equals(WLOB_X101)) {
			if (acctNum.length() != 0) {
				return (new Reply(RC.SUBN_NONEED_ACCTNUM));
			}

			return (verifySrvNumMOB());
		}
		else {
			throw new MiniRtException("Unexpected LOB ("+lob+")!"); 
		}
	}


	public Reply verifySrvNumLTS()
	{
		String                      rVar;

		rVar = srvNum;

		if (!Tool.isNotNilDig(rVar)) {
			return (new Reply(RC.SUBN_IVSN_LTS));
		}

		/*
            Since LTS contains FTNS, IDD0060 and Calling Card,
            the validation rule is relaxed to 8 digits only.
		 */

		if (rVar.length() != 8) {
			return (new Reply(RC.SUBN_IVSN_LTS));
		}

		return (Reply.getSucc());
	}


	public Reply verifySrvNumMOB()
	{
		String                      rVar;

		rVar = srvNum;

		if (!Tool.isNotNilDig(rVar)) {
			return (new Reply(RC.SUBN_IVSN_MOB));
		}

		if (!MyTool.isVaMob(rVar)) {
			return (new Reply(RC.SUBN_IVSN_MOB));
		}

		return (Reply.getSucc());
	}


	public Reply verifySrvNumPCD()
	{
		if (srvNum.length() == 0 || srvNum.length() > L_SRV_NUM || !Tool.isAlpNum(srvNum, true, true)) {
			return (new Reply(RC.SUBN_IVSN_PCD));
		}
		
		return (Reply.getSucc());
	}

	
    public Reply verifyAcctNumIMS()
    {
        if (acctNum.length() != IMS_ACCTNUM_LEN) {
            return (new Reply(RC.SUBN_IVAN_IMS));
        }
        
        return (Reply.getSucc());
    }

	public Reply verifyAcctNumTV()
	{
		if (acctNum.length() != TV_ACCTNUM_LEN) {
			return (new Reply(RC.SUBN_IVAN_TV));
		}

		return (Reply.getSucc());
	}


	public Reply verifyAlias()
	{
		alias = alias.trim();

		return verifyAlias(alias);
	}


	public static Reply verifyAlias(String rAlias)
	{
		if (rAlias.length()  > L_ALIAS) {
			return (new Reply(RC.SUBN_ILALIAS));
		}

		return (Reply.getSucc());
	}


	public Reply verifyFsa(boolean rAllowNil)
	{
		/*
            Normally not for Interactive Data Input Check
            Only used for API.
		 */

		 if (!Tool.isASC(fsa, 0, L_FSA)) {
			 return (new Reply(RC.SUBN_NLFSA));
		 }

		 if (!rAllowNil) {
			 if (fsa.length() == 0) {
				 return (new Reply(RC.SUBN_NLFSA));
			 }
		 }                

		 return (Reply.getSucc());
	}


	public boolean isWip()
	{
		return (wipCust.equals(WIP_P) ||
				wipCust.equals(WIP_W) ||
				wipCust.equals(WIP_Y));
	}


	public boolean isSip()
	{
		return (sipSubr.equals(Const.Y));
	}


	public boolean noSelfService()
	{
		return (isWip() || isSip());
	}


	public static int weightOfDisplayLob(String rLob)
	{
		/* Return a Weight of LOB for Display */

		if (rLob.equals(SubnRec.LOB_TV)) {
			 return (50);
		}
		else if (rLob.equals(SubnRec.LOB_PCD)) {
			return (40);
		}
		else if (rLob.equals(SubnRec.LOB_IMS)) {
			return (40);
		}
		else if (rLob.equals(SubnRec.LOB_MOB)) {
			return (20);
		}
		else if (rLob.equals(SubnRec.LOB_IOI)) {
			return (20);
		}
		else if (rLob.equals(SubnRec.LOB_101)) {
			return (20);
		}
		else if (rLob.equals(SubnRec.LOB_O2F)) {
			return (20);
		}
		else if (rLob.equals(SubnRec.LOB_LTS)) {
			return (10);
		}
		else {
			throw new RuntimeException("Unexpected LOB (" + rLob +")!");
		}
	}


	public int weightOfDisplayLob()
	{
		return (weightOfDisplayLob(lob));
	}


	public boolean isAssoc()
	{
		return (assoc.equals(Const.Y));
	}


	public static boolean isZombie(String rSrvId)
	{
		return (rSrvId.equals(SRVID_ZOMBIE));
	}


	public boolean isZombie()
	{
		return (isZombie(srvId));
	}


	public static boolean isAddOn(String rStoreTy)
	{
		return (rStoreTy.equals(STTY_AO) || rStoreTy.equals(STTY_AD));
	}


	public boolean isAddOn()
	{
		/*
            SubnRec is used to hold both SUBN and Add-On.

            Currently (Jan-2015) Add-On are not
            stored in the DB. Only Mobile App will request data
            for Add-On SubscriptionRec.
		 */

		return (isAddOn(storeTy));
	}


	public Account deriveAcct()
	{
		/*
            Derive an Account 
		 */

		Account                     rAcct;

		rAcct = new Account();

		rAcct.setCustRid(custRid);
		rAcct.setCusNum(cusNum);
		rAcct.setAcctNum(acctNum);
		rAcct.setLob(lob);
		rAcct.setSysTy(systy);
		rAcct.setLive(!isZombie());

		return (rAcct);
	}
 
    
//    public String prettySrvNum()
//    {
//        /*
//            touch-up the Service Number for Presentation ONLY
//        */
//        
//        String                      rSrvNum;
//        String                      rSufx;
//        int                         rx, ri, rl;
//        
//        if (lob.equals(LOB_LTS)) {
//            if (tos.equals(TOS_LTS_ITS)) {
//                if ((rl=srvNum.length()) > 4) {
//                    rSufx = srvNum.substring(rl - 4);
//                    rSrvNum = ClnEnv.getBiTxMap().get(Msgs.CALLING_CARD_PFX) + rSufx;
//                    return (rSrvNum);
//                }
//            }
//        }
//        
//        return (srvNum);
//    }
//    
//    
//    public String getIdent(String rZmDesn)
//    {
//        /*
//            Return Identification Description for this SubnRec
//            Identification means Service Number, or, Account Number (if Zombie)
//        */
//        
//        if (!isZombie()) {
//            return (prettySrvNum());
//        }
//        else {
//            if (Tool.isNil(rZmDesn)) {
//                return (acctNum);
//            }
//            else {
//                return (acctNum + rZmDesn);
//            }
//        }
//    }
//    
//    
//    public String getIdent()
//    {
//        String                      rZmDesn;
//        
//        rZmDesn = ClnEnv.getBiTxMap().get(Msgs.ZOMBIE_DESN);
//        return (getIdent(rZmDesn));
//    }
	public boolean isBillByAgent()
    {
        return (billagt.trim().length() != 0);
    }
}
