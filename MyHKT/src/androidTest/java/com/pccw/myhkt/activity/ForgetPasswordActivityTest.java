package com.pccw.myhkt.activity;

import android.content.Context;
import android.content.SharedPreferences;
import androidx.test.espresso.intent.Intents;
import androidx.test.rule.ActivityTestRule;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import com.pccw.myhkt.R;
import com.pccw.myhkt.base.BaseTest;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static androidx.test.platform.app.InstrumentationRegistry.getInstrumentation;

@Ignore
@RunWith(AndroidJUnit4.class)
public class ForgetPasswordActivityTest extends BaseTest {
    private Context context;
    private SharedPreferences shared;
    SharedPreferences.Editor prefEditor;

    private final int ORIGIN_NAVBAR = 0xFFFFFFFF;
    private final int ORIGIN_INQUIRY = 0x0FFFFFFF;

    @Rule
    public ActivityTestRule<ForgetPasswordActivity> activityTestRule = new ActivityTestRule<>(ForgetPasswordActivity.class);

    @Before
    public void setUp() {
        context = getInstrumentation().getTargetContext();
        shared = context.getSharedPreferences(
                "myhkt",
                Context.MODE_PRIVATE);
        Intents.init();
    }

    @After
    public void tearDown() {
        Intents.release();
    }

    @Test
    public void verifyForgetPasswordUI() {
        checkStringIsDisplayed(context,R.string.myhkt_title_forgetpassword);
        checkStringIsDisplayed(context,R.string.FGCF_RECALL);
        checkStringIsDisplayed(context,R.string.myhkt_forgetpassword_loginid);
        checkStringIsDisplayed(context,R.string.myhkt_forgetpassword_password);
        checkStringIsDisplayed(context,R.string.myhkt_forgetpassword_ITO);
        checkStringIsDisplayed(context,R.string.REGF_HKID_NO);
        checkStringIsDisplayed(context,R.string.REGF_PASSPORT_NO);
        checkStringIsDisplayed(context,R.string.REGF_FB_DOC_NUM);
        checkStringIsDisplayed(context,R.string.MYHKT_BTN_CANCEL);
        checkStringIsDisplayed(context,R.string.MYHKT_BTN_CONFIRM);
        checkStringIsDisplayed(context,R.string.MYHKT_BTN_LIVECHAT);
    }

    @Test
    public void testIncorrectFields() {
        insertMyDetail("A123456(7)");
        clickString(context,R.string.MYHKT_BTN_CONFIRM);
        delay(TestTimeouts.API_TIMEOUTS,waitForText(btn_OK));
    }

    @Test
    public void testLoginIDHKIDFillUp() {
        clickString(context,R.string.myhkt_forgetpassword_loginid);
        clickString(context,R.string.REGF_HKID_NO);
        insertMyDetail("A123456(7)");
        clickConfirm();
        delay(TestTimeouts.API_TIMEOUTS, waitForText(btn_OK));
    }

    @Test
    public void testLoginIDPassportFillUp() {
        clickString(context,R.string.myhkt_forgetpassword_loginid);
        clickString(context,R.string.REGF_HKID_NO);
        insertMyDetail("A123456(7)");
        clickConfirm();
        delay(TestTimeouts.API_TIMEOUTS,waitForText(btn_OK));
    }

    @Test
    public void testPasswordHKIDFillUp() {
        clickString(context,R.string.myhkt_forgetpassword_password);
        clickString(context,R.string.REGF_PASSPORT_NO);
        insertMyDetail("A123456(7)");
        clickConfirm();
        delay(TestTimeouts.API_TIMEOUTS,waitForText(btn_OK));
    }

    @Test
    public void testPasswordPassportFillUp() {
        clickString(context,R.string.myhkt_forgetpassword_password);
        clickString(context,R.string.REGF_PASSPORT_NO);
        insertMyDetail("A123456(7)");
        clickConfirm();
        delay(TestTimeouts.API_TIMEOUTS,waitForText(btn_OK));
    }

    @Test
    public void testLiveChatInquiryNavigation() {
        executeLiveChat(ORIGIN_NAVBAR);
        checkStringIsDisplayed(context,R.string.LIVE_CHAT_DISCLAIMER);
    }

    @Test
    public void testLiveChatInquiryButton() {
        executeLiveChat(ORIGIN_INQUIRY);
        checkStringIsDisplayed(context,R.string.LIVE_CHAT_DISCLAIMER);
    }

    @Test
    public void testClearTextButton() {
        insertMyDetail("A123456(7)");
        clickView(R.id.reg_input_buttonclear);
        checkStringIsDisplayed(R.id.reg_input_layoutet, R.id.forgetpassword_input_hkid,"");
    }

    @Test
    public void testBackButton() {
        clickLeftNavBar();
        checkIntendedActivity(LoginActivity.class.getName());
    }


    private void clickConfirm() {
        clickView(R.id.forgetpassword_btn_confirm);
    }

    private void insertMyDetail(String hkidpassport) {
        typeTextView(R.id.reg_input_layoutet,R.id.forgetpassword_input_hkid,hkidpassport);
    }

    private void executeLiveChat(int mode) {
        switch(mode) {
            case ORIGIN_NAVBAR:
                clickLiveChatNavBar();
                break;
            case ORIGIN_INQUIRY:
                clickLiveChatBottom();
                break;
        }
    }

    private void clickLiveChatBottom() {
        clickView(R.id.forgetpassword_btn_livechat);
    }

}