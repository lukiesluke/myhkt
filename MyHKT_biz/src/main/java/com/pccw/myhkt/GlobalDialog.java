package com.pccw.myhkt;

/************************************************************************
 File       : GlobalDialog.java
 Desc       : For everywhere we need to show dialog
 Name       : GlobalDialog
 Created by : Vincent Fung
 Date       : 13/12/2012

 Change History:
 Date       Modified By			Description
 ---------- ----------------	-------------------------------
 05/02/2014 Vincent Fung		- First draft
 18/02/2014 Vincent Fung		- Added onNewIntent to handle rebuild Dialog problem
 26/03/2014 Derek Tsui			- Updated createDialog switch for notification message behavior
 *************************************************************************/

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.WindowManager;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AlertDialog.Builder;

import com.pccw.biz.myhkt.R;
import com.pccw.myhkt.activity.ClearActivity;
import com.pccw.myhkt.activity.MainMenuActivity;
import com.pccw.myhkt.activity.ServiceListActivity;

public class GlobalDialog extends Activity implements DialogInterface.OnCancelListener {
    private boolean debug = false;
    private static GlobalDialog me;
    private AlertDialog alertDialog = null;
    private AlertDialog.Builder builder;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        debug = getResources().getBoolean(R.bool.DEBUG);
        ClnEnv.updateUILocale(getBaseContext(), ClnEnv.getAppLocale(getBaseContext()));
        me = this;
        if (debug) Log.i("test", "dialog create");
        createDialog(me.getIntent().getExtras().getInt("DIALOGTYPE"));
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        if (debug) Log.i("test", "dialog create1");
        me = this;
        createDialog(intent.getExtras().getInt("DIALOGTYPE"));
    }

    protected void createDialog(int id) {
        if (alertDialog != null) {
            alertDialog.dismiss();
        }

        builder = new Builder(me);
        switch (id) {
            case R.string.CONST_DIALOG_LNTT_RESULT:
                builder.setMessage(Utils.getString(me, R.string.MYHKT_LT_MSG_COMPLETED));
                builder.setCancelable(false);
                builder.setPositiveButton(Utils.getString(me, R.string.MYHKT_BTN_LATER), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        finish();
                    }
                });
                builder.setNegativeButton(Utils.getString(me, R.string.btn_ok), new OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        ClnEnv.setPref(me.getApplicationContext(), me.getString(R.string.CONST_PREF_LNTT_NOTICEFLAG), true);
                        dialog.dismiss();
                        /* Intent parentIntent = new Intent(me, SplashActivity.class);
                         * parentIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK); */
                        Intent parentIntent = new Intent(me, MainMenuActivity.class);
                        parentIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                        startActivity(parentIntent);
                        me.overridePendingTransition(0, 0);
                        finish();
                    }
                });
                alertDialog = builder.create();
                alertDialog.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT);
                alertDialog.show();
                break;
            case R.string.CONST_DIALOG_MSG_BILL:
                if (ClnEnv.getPushDataBill() != null && ClnEnv.isLoggedIn()) {
                    if ((!"".equalsIgnoreCase(ClnEnv.getPushDataBill().getLoginId()) && ClnEnv.getPushDataBill().getLoginId().equalsIgnoreCase(ClnEnv.getSessionLoginID()))) {

                        //prepare service num
//					String serviceNum = "";
//					if (ClnEnv.isLoggedIn()) {
//						for (SubnRec rSubnRec : ClnEnv.getAssocSubnRecAry(ClnEnv.getQualSvee().getSubnRecAry())) {
//							if (rSubnRec.acctNum.equalsIgnoreCase(ClnEnv.getPushDataBill().getAcctNum())) {
//								int ltsType = 0;
//								
//			        			//service number for bill dialog TODO
//								if (rSubnRec.lob.equals(SubnRec.LOB_LTS)) {
//									ltsType = Utils.getLtsSrvType(rSubnRec.tos, rSubnRec.eyeGrp, rSubnRec.priMob);
//									if (ltsType == R.string.CONST_LTS_CALLINGCARD) {
//				    					int last4index = (rSubnRec.srvNum.length() - 4) < 0 ? 0 : rSubnRec.srvNum.length() - 4;
//				    					serviceNum = String.format("CARD%s", rSubnRec.srvNum.substring(last4index)) + "\n";
//									} else {
//				    					serviceNum = rSubnRec.srvNum + "\n";
//									}
//								} else {
//			    					serviceNum = rSubnRec.srvNum + "\n";
//								}
//								
//			    				if (rSubnRec.lob.equals(SubnRec.LOB_LTS) || rSubnRec.lob.equals(SubnRec.LOB_MOB) || rSubnRec.lob.equals(SubnRec.LOB_101) || 
//		    						rSubnRec.lob.equals(SubnRec.LOB_IOI) || rSubnRec.lob.equals(SubnRec.LOB_O2F)) {
//			    					serviceNum = getString(R.string.LTTF_LTS_ACCTNUM) + " " + serviceNum;
//			    				} else if (rSubnRec.lob.equals(SubnRec.LOB_TV)) {
//			    					serviceNum = getString(R.string.LTTF_TV_ACCTNUM) + " " + serviceNum;
//			    				} else if (rSubnRec.lob.equals(SubnRec.LOB_PCD)) {
//			    					serviceNum = getString(R.string.LTTF_PCD_ACCTNUM) + " " + serviceNum;
//			    				} 
//							}
//						}
//					}
//					builder.setMessage(serviceNum + ClnEnv.getPushDataBill().getMessage());

                        builder.setMessage(ClnEnv.getPushDataBill().getMessage());
                        builder.setCancelable(false);
                        builder.setPositiveButton(Utils.getString(me, R.string.btn_ok), new OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                ClnEnv.setPref(me.getApplicationContext(), me.getString(R.string.CONST_PREF_BILLMSG_NOTICEFLAG), true);
                                dialog.dismiss();
                                /* Intent parentIntent = new Intent(me, SplashActivity.class);
                                 * parentIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK); */
                                Intent parentIntent = new Intent(me, ServiceListActivity.class);
                                parentIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                                startActivity(parentIntent);
                                me.overridePendingTransition(0, 0);
                                finish();
                            }
                        });
//					builder.setNegativeButton(Utils.getString(me, R.string.MYHKT_BTN_IGNORE), new DialogInterface.OnClickListener() {
//						public void onClick(DialogInterface dialog, int which) {
//							dialog.dismiss();
//							finish();
//						}
//					});
                        alertDialog = builder.create();
                        alertDialog.show();
                    }
                } else {
                    ClnEnv.getPushDataBill().clear();
                    break;
                }
                break;
            case R.string.CONST_DIALOG_MSG_GEN:
                if (debug) Log.d("GLOBAL DIALOG", "I AM HIT");
                if (ClnEnv.getPushDataGen() != null) {
                    builder.setMessage(ClnEnv.getPushDataGen().getMessage());
                    builder.setCancelable(false);
                    builder.setPositiveButton(Utils.getString(me, R.string.btn_ok), new OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            me.overridePendingTransition(0, 0);
                            finish();
                        }
                    });
                    alertDialog = builder.create();
                    alertDialog.show();
                }
                break;
            case R.string.CONST_DIALOG_LNTT_ONSTART:
            case R.string.CONST_DIALOG_LNTT_ONRESET:
                if (id == R.string.CONST_DIALOG_LNTT_ONSTART) {
                    builder.setMessage(Utils.getString(me, R.string.MYHKT_LT_MSG_NORMAL_START_SUCCESS));
                } else if (id == R.string.CONST_DIALOG_LNTT_ONRESET) {
                    builder.setMessage(Utils.getString(me, R.string.MYHKT_LT_MSG_RESET_START_SUCCESS));
                }

                builder.setPositiveButton(Utils.getString(me, R.string.btn_ok), new OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        finish();
                    }
                });
                builder.setOnCancelListener(new Dialog.OnCancelListener() {
                    @Override
                    public void onCancel(DialogInterface dialog) {
                        dialog.dismiss();
                        finish();
                    }
                });
                alertDialog = builder.create();
                alertDialog.show();
                break;
            case R.string.CONST_DIALOG_VC_FORCE_UPDATE:
                OnClickListener onPositiveClickListener = new OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        gotoGooglePlay();
                    }
                };
                OnClickListener onNegativeClickListener = new OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        closeApplication();
                    }
                };
                builder = new Builder(this);
                builder.setMessage(getResString(R.string.version_update_blocked));
                builder.setPositiveButton(getResString(R.string.version_update_update), onPositiveClickListener);
                builder.setNegativeButton(getResString(R.string.version_update_exit), onNegativeClickListener);
                builder.setCancelable(false);
                AlertDialog alertDialog1 = builder.create();
                //alertDialog.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT);
                alertDialog1.show();
                break;
            case R.string.CONST_DIALOG_VC_OPTION_UPDATE:
                OnClickListener onPositiveClickListener1 = new OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        gotoGooglePlay();
                    }
                };
                OnClickListener onNegativeClickListener1 = new OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        ClnEnv.isVCFinish = true;
                        finish();
                    }
                };
                builder = new Builder(this);
                builder.setMessage(getResString(R.string.version_update_optional));
                builder.setPositiveButton(getResString(R.string.version_update_update), onPositiveClickListener1);
                builder.setNegativeButton(getResString(R.string.version_update_exit), onNegativeClickListener1);
                builder.setCancelable(false);
                AlertDialog alertDialog2 = builder.create();
                alertDialog2.show();
                break;
            default:
                // create a default dialog
                alertDialog = builder.create();
                alertDialog.show();
                break;
        }
    }

    @Override
    public void onCancel(DialogInterface arg0) {
        // THIS IS VERY IMPORTANT TO REMOVE THE ACTIVITY WHEN THE DIALOG IS DISMISSED
        // IF NOT ADDED USER SCREEN WILL NOT RECEIVE ANY EVENTS BEFORE USER PRESSES BACK
        finish();
    }

    public String getResString(int res) {
        return Utils.getString(this, res);
    }

    // Goto Google Play
    private final void gotoGooglePlay() {
        ClnEnv.isVCFinish = true;
        ClnEnv.isForceCloseApp = true;
        Intent intent = new Intent(this, ClearActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.putExtra("FLAG", "PLAY");
        startActivity(intent);
        finish();

    }

    public final void closeApplication() {
        ClnEnv.isVCFinish = true;
        ClnEnv.isForceCloseApp = true;
        Intent intent = new Intent(this, ClearActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.putExtra("FLAG", "EXIT");
        startActivity(intent);
        finish();
    }
}
