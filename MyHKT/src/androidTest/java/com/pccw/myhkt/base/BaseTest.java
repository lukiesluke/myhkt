package com.pccw.myhkt.base;

import android.app.ActivityManager;
import android.content.Context;
import android.content.SharedPreferences;
import androidx.test.espresso.FailureHandler;
import androidx.test.espresso.ViewAction;
import androidx.test.espresso.ViewInteraction;
import android.view.View;

import com.pccw.myhkt.ClnEnv;
import com.pccw.myhkt.R;
import com.pccw.myhkt.activity.TestTimeouts;
import com.pccw.myhkt.activity.MainMenuActivity;
import com.pccw.myhkt.util.Constant;

import org.hamcrest.Matcher;

import java.util.List;

import static androidx.test.espresso.Espresso.onData;
import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.swipeLeft;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.RootMatchers.isDialog;
import static androidx.test.espresso.matcher.ViewMatchers.isDescendantOfA;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.CoreMatchers.anything;
import static org.hamcrest.core.AllOf.allOf;

public class BaseTest extends EspressoWrapper {
    /**********START FINAL*********/
    public String btn_OK = "OK";
    public String btn_CANCEL = "CANCEL";

    public void clickOK(){
        clickString(btn_OK);
    }

    public void clickCANCEL(){
        clickString(btn_CANCEL);
    }

    public void loginUser(Context context, SharedPreferences shared, String username, String password, boolean isUAT){
        clickLeftNavBar();
        clickString(context, R.string.myhkt_LGIF_LOGIN);
        if(isUAT){
            loginMyCredentialsUAT(username, password, shared);
        } else{
            loginMyCredentialsPRD(username, password, shared);
        }
    }

    protected void loginMyCredentials(String username, String password) {
        typeTextView(R.id.reg_input_layoutet,R.id.login_input_userid,username);
        typeTextView(R.id.reg_input_layoutet,R.id.login_input_pw,password);

        clickView(R.id.login_login_btn);
        delay(TestTimeouts.API_TIMEOUTS,waitForLogin());
    }

    protected void loginMyCredentials(String username, String password, Runnable callback) {
        typeTextView(R.id.reg_input_layoutet,R.id.login_input_userid,username);
        typeTextView(R.id.reg_input_layoutet,R.id.login_input_pw,password);

        clickView(R.id.login_login_btn);
        delay(TestTimeouts.API_TIMEOUTS,callback);
    }

    protected void changeMode(String mode){
        typeTextView(R.id.reg_input_layoutet,R.id.login_input_userid,"changemode");
        clickView(R.id.login_login_btn);

        delay(TestTimeouts.DIALOG_TIMEOUTS, waitForText("EXTERNAL UAT"));
        clickDialogString(mode);
    }

    protected void loginMyCredentialsUAT(String username, String password) {
        changeMode("EXTERNAL UAT");
        loginMyCredentials(username,password);
    }

    protected void loginMyCredentialsPRD(String username, String password) {
        changeMode("PRD (Pilot)");
        loginMyCredentials(username,password);
    }

    protected void loginMyCredentialsUAT(String username, String password, SharedPreferences sharedPreferences) {
        loginMyCredentialsUAT(username,password);
        if(checkFingerPrintConfirmation(sharedPreferences))
            closeMyFingerprintBecauseIDontNeedIt();
    }

    protected void loginMyCredentialsPRD(String username, String password, SharedPreferences sharedPreferences) {
        loginMyCredentialsPRD(username,password);
        if(checkFingerPrintConfirmation(sharedPreferences))
            closeMyFingerprintBecauseIDontNeedIt();
    }

    public void checkBackBtn(String name, int position){
        clickLeftNavBar();
        checkIntendedActivity(name);
        gotoMainMenuItem(position);
    }

    public void checkLiveChat(Context context, int liveChatDisclaimer,
                              int displayedViewId){
        clickLiveChatNavBar();
        checkDialogStringIsDisplayed(context,liveChatDisclaimer);
        clickCANCEL();
        checkViewIsDisplayed(displayedViewId);
        clickLiveChatNavBar();
        clickOK();
        delay(TestTimeouts.LIVECHAT_TIMEOUTS,waitForLiveChat());
        clickView(R.id.dialog_livechat_close);
        delay(TestTimeouts.DIALOG_TIMEOUTS);
        clickOK();
    }

    protected void clickLeftNavBar() {
        clickView(R.id.navbar_button_left,R.id.navbar_base_layout);
    }

    protected void backBtnExist() {
        checkViewIsDisplayed(R.id.navbar_button_left,R.id.navbar_base_layout);
    }

    protected void gotoMainMenuItem(int position) {
        clickViewInAdapter(R.id.mainmenu_gridlayout,position);
    }

    protected void executeLogout(Context context){
        int max = 5;

        for(int index =0; index < max; index++){
            if(isMainMenuOnForeground(context)){
                break;
            }
            pressBackRoot();
        }
        delay(TestTimeouts.DIALOG_TIMEOUTS);
        clickLeftNavBar();
        clickString(context,R.string.SHLF_LOGOUT);
        clickString(context,R.string.myhkt_btn_yes);
    }

    protected void delay(long milliseconds){
        try {
            Thread.sleep(milliseconds);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    protected void delay(long milliseconds, Runnable callback){
        try {
            for (int i = 0; i < milliseconds/1000; i++) {
                isStop = true;
                callback.run();
                if(isStop == true){
                    break;
                }
                else {
                    Thread.sleep(1000);
                }
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (Exception e){
            e.printStackTrace();
        }
        isStop = false;
    }

    public void scrollToView(int viewId){
        scrollView(viewId);
        delay(TestTimeouts.ACTIVITY_LOAD_TIMEOUTS);
    }

    public void scrollToViewWithDescendant(int viewId, int descendantOfId){
        scrollView(viewId,descendantOfId);
        delay(TestTimeouts.ACTIVITY_LOAD_TIMEOUTS);
    }

    protected void closeMyFingerprintBecauseIDontNeedIt() {
        clickView(R.id.touch_id_button_close);
        clickDialogString(btn_OK);
    }

    protected void clickLiveChatNavBar() {
        clickView(R.id.navbar_button_right,R.id.navbar_base_layout);
    }

    protected void checkLiveChatBtnIsDisplayed() {
        checkViewIsDisplayed(R.id.navbar_button_right,R.id.navbar_base_layout);
    }

    protected void checkLiveChatBtnIsHidden() {
        checkViewIsNotDisplayed(R.id.navbar_button_right,R.id.navbar_base_layout);
    }

    protected boolean checkFingerPrintConfirmation(SharedPreferences shared) {
        return shared.getInt(Constant.TOUCH_ID_WELCOME_TOUCH_ID_COUNTER, 0) < 3;
    }
    public Runnable waitForLogin(){
        return new Runnable(){
            @Override
            public void run()
            {
                isStop = ClnEnv.isLoggedIn();
            }
        };
    }

    public Runnable waitForText(String text){
        return new Runnable(){
            @Override
            public void run(){
                try {
                    checkStringIsDisplayed(text);
                }catch (Exception e){
                    isStop = false;
                }
            }
        };
    }

    public Runnable waitForText(Context context, int stringId){
        return new Runnable(){
            @Override
            public void run(){
                try {
                    checkStringIsDisplayed(context, stringId);
                }catch (Exception e){
                    isStop = false;
                }
            }
        };
    }

    public Runnable waitForViewEnabled(){
        return new Runnable(){
            @Override
            public void run(){
                try {
                    onView(withId(R.id.myprofcontact_mob_et))
                            .withFailureHandler(new FailureHandler() {
                                @Override
                                public void handle(Throwable error, Matcher<View> viewMatcher) {
                                    if (error != null){
                                        isStop = false;
                                    }else{
                                        isStop = true;
                                    }
                                }
                            }).check(matches(checkIfEnabled()));
                }catch (Exception e){
                    isStop = false;
                }
            }
        };
    }

    /**
     *
     * @param context
     * @param index - index of the swiped item
     * @param currentDisplayName - currently displayed name
     * @param swipeListViewId - id of listview
     * @param stringDisplayNameId - id of the text in the button to be clicked
     * @param btnAliasId - id of the button to be clicked in adapter
     * @param tvLabelContainerId - id of the textview of the alias in the adapter
     * @param callback - wait callback
     */
    public void  changeDisplayNameBaseTest(Context context, int index, String currentDisplayName,
                                          int swipeListViewId, int stringDisplayNameId,
                                          int btnAliasId, int tvLabelContainerId, Runnable callback){
        targetAccount(index, swipeLeft(),swipeListViewId);

        delay(TestTimeouts.DIALOG_TIMEOUTS);
        checkStringIsDisplayed(context,stringDisplayNameId);
        onData(anything()).inAdapterView(withId(swipeListViewId))
                .atPosition(index)
                .onChildView(withId(btnAliasId))
                .perform(click());

        checkStringIsDisplayed(context,R.string.MYMOB_PLZ_INPUT_ALIAS);
        clickCANCEL();
        checkStringIsDisplayed(context,R.string.myhkt_btn_alias);

        targetAccount(index, swipeLeft(), swipeListViewId);
        delay(TestTimeouts.DIALOG_TIMEOUTS);
        onData(anything()).inAdapterView(withId(swipeListViewId))
                .atPosition(index)
                .onChildView(withId(btnAliasId))
                .perform(click());

        typeTextView(R.id.et_change_display_name, "test name");

        clickOK();

        delay(TestTimeouts.API_TIMEOUTS, callback); // waiting API response

        checkStringIsDisplayed(context,R.string.RSVM_DONE);

        clickOK();

        onData(anything()).inAdapterView(withId(swipeListViewId))
                .atPosition(index)
                .onChildView(withId(tvLabelContainerId))
                .check(matches(withText("test name")));

        targetAccount(index, swipeLeft(),swipeListViewId);
        delay(TestTimeouts.DIALOG_TIMEOUTS);

        onData(anything()).inAdapterView(withId(swipeListViewId))
                .atPosition(index)
                .onChildView(withId(btnAliasId))
                .perform(click());

        clearTextView(R.id.et_change_display_name);

        typeTextView(R.id.et_change_display_name, currentDisplayName);

        clickOK();
        delay(TestTimeouts.API_TIMEOUTS, callback); // waiting API response
        clickOK();
    }

    public boolean isMainMenuOnForeground(Context context){
        boolean result = false;
        ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningTaskInfo> tasks = am.getRunningTasks(1);
        result = tasks.get(0).topActivity.getClassName().contains(MainMenuActivity.class.getSimpleName());

        return result;
    }

    public Runnable waitForLiveChat(){
        return new Runnable(){
            @Override
            public void run(){
                try {
                    checkViewIsDisplayed(R.id.dialog_livechat_close);
                    isStop = true;
                }catch (Exception e){
                    isStop = false;
                }
            }
        };
    }

    /**********END FINAL*********/

    public void targetAccount(int index, ViewAction perform, int adapterId){
        onData(anything())
                .inAdapterView(withId(adapterId))
                .atPosition(index)
                .perform(perform);
    }

    protected ViewInteraction findStringContaining(String hardText) {
        return onView(withText(hardText));
    }

    protected ViewInteraction findStringContaining(Context context, int resId) {
        return onView(withText(context.getResources().getString(resId)));
    }

    protected ViewInteraction findStringContainingDialog(Context context, int resId) {
        return onView(withText(context.getResources().getString(resId))).inRoot(isDialog())
                .check(matches(isDisplayed()));
    }

    protected ViewInteraction findComponentByID(int resId) {
        return onView(withId(resId));
    }

    protected ViewInteraction findComponentByID(int resId, int descendant) {
        return onView(allOf(withId(resId), isDescendantOfA(withId(descendant))));
    }
}
