package com.pccw.myhkt.activity;

import android.content.Context;
import android.content.SharedPreferences;
import androidx.test.platform.app.InstrumentationRegistry;
import androidx.test.rule.ActivityTestRule;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import com.pccw.myhkt.AccountHelper;
import com.pccw.myhkt.R;
import com.pccw.myhkt.base.BaseTest;

import org.hamcrest.CoreMatchers;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static androidx.test.platform.app.InstrumentationRegistry.getInstrumentation;
import static androidx.test.espresso.Espresso.onData;
import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.clearText;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.scrollTo;
import static androidx.test.espresso.action.ViewActions.swipeLeft;
import static androidx.test.espresso.action.ViewActions.typeText;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.RootMatchers.isDialog;
import static androidx.test.espresso.matcher.ViewMatchers.isAssignableFrom;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.anything;

@Ignore@RunWith(AndroidJUnit4.class)
public class MyLineTestListActivityTest extends BaseTest {

    private Context context;

    private SharedPreferences shared;
    SharedPreferences.Editor prefEditor;


    @Rule
    public ActivityTestRule<MainMenuActivity> activityTestRule =
            new ActivityTestRule<MainMenuActivity>(MainMenuActivity.class) {
                @Override
                protected void beforeActivityLaunched() {
                    clearSharedPrefs(InstrumentationRegistry.getTargetContext());
                    InstrumentationRegistry.getTargetContext().deleteDatabase("myhkt.db");
                    super.beforeActivityLaunched();
                }
            };

    private void clearSharedPrefs(Context context) {
        SharedPreferences prefs =
                context.getSharedPreferences("myhkt", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.clear();
        editor.commit();
    }

    @Before
    public void setUp() {
        context = getInstrumentation().getTargetContext();
        shared = context.getSharedPreferences(
                "myhkt",
                Context.MODE_PRIVATE);
//        Intents.init();
        clickLeftNavBar();
        findStringContaining(context, R.string.myhkt_LGIF_LOGIN).perform(click());
        loginMyCredentialsPRD(AccountHelper.getPRDAccount3().getUsername(), AccountHelper.getPRDAccount3().getPassword(), shared);
        gotoMainMenuItem(1);
    }

    @Test
    public void testMyLineTestListDisplay() {
        checkViewIsDisplayed(R.id.servicelist_btn_alias);
        checkViewIsDisplayed(R.id.servicelist_listView);
    }

    @Test
    public void testMyLineTestListNavigate() {
        for(int i = 0; i < 3; i++) {
            clickViewInAdapter(R.id.servicelist_listView, i);
            delay(TestTimeouts.LIVECHAT_TIMEOUTS);
            clickLeftNavBar();
        }
    }

    @Test
    public void testMyLineTestListSetDisplayName() {
        clickView(R.id.servicelist_btn_alias);
    }

    @Test
    public void testMyLineTestListDisplayName() {
        findComponentByID(R.id.servicelist_btn_alias)
                .perform(click());
        onData(CoreMatchers.anything())
                .inAdapterView(withId(R.id.servicelist_listView))
                .atPosition(0)
                .onChildView(withId(R.id.adapter_servicelist_edit))
                .perform(click());
        findStringContaining("CANCEL").perform(click());
        changeDisplayNameBaseTest(context, 0, "", R.id.servicelist_listView,
                R.string.MYMOB_BTN_ALIAS, R.id.adapter_servicelist_edit,
                R.id.adapter_servicelist_header_alias, waitForText(btn_OK));
    }

    @Test
    public void testMyLineTestListChangeDisplayName() {
        findComponentByID(R.id.servicelist_btn_alias)
                .perform(click());
        onData(CoreMatchers.anything())
                .inAdapterView(withId(R.id.servicelist_listView))
                .atPosition(0)
                .onChildView(withId(R.id.adapter_servicelist_edit))
                .perform(click());
        findStringContaining("OK").perform(click());
        delay(5000);
        findStringContaining("OK").perform(click());
    }

    @After
    public void tearDown() {
        executeLogout(context);
    }
}
