package com.pccw.myhkt.activity;

import java.lang.Thread.UncaughtExceptionHandler;
import java.util.List;

import com.pccw.dango.shared.tool.RC;
import com.pccw.myhkt.APIsManager;
import com.pccw.myhkt.APIsResponse;
import com.pccw.myhkt.ClnEnv;
import com.pccw.myhkt.DialogHelper;
import com.pccw.biz.myhkt.R;
import com.pccw.myhkt.Utils;
import com.pccw.myhkt.adapter.ServiceListViewAdapter.OnServiceListViewAdapterListener;
import com.pccw.myhkt.fragment.IDDCodesNTimesFragment;
import com.pccw.myhkt.fragment.ServListFragment;
import com.pccw.myhkt.fragment.IDDCodesNTimesFragment.OnIDDCodeNtimesListener;
import com.pccw.myhkt.fragment.IDDRateServiceFragment;
import com.pccw.myhkt.fragment.IDDRateServiceFragment.OnIDDRateServiceListener;
import com.pccw.myhkt.fragment.IDDRatesFragment;
import com.pccw.myhkt.fragment.IDDRatesFragment.OnIDDRatesListener;
import com.pccw.myhkt.fragment.ServListFragment.OnServListListener;
import com.pccw.myhkt.lib.ui.AAQuery;
import com.pccw.myhkt.lib.ui.LRTabButton;
import com.pccw.myhkt.model.AcctAgent;
import com.pccw.myhkt.model.HomeButtonItem;
import com.pccw.myhkt.model.IDDCountry;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.LinearLayout.LayoutParams;

/************************************************************************
 * File : IDDActivity.java
 * Desc : IDD Page
 * Name : IDDActivity
 * by 	: Andy Wong
 * Date : 11/01/2016
 * 
 * Change History:
 * Date       Modified By		Description
 * ---------- ----------------	-------------------------------
 * 11/01/2016 Andy Wong 		-First draft
 *************************************************************************/

public class IDDActivity extends BaseActivity implements OnIDDCodeNtimesListener, OnIDDRateServiceListener, OnIDDRatesListener, OnServListListener {

	private String 					TAG = this.getClass().getName();
	private AAQuery 				aq;

	private int 					extralinespace = 0;
	private int 					greylineHeight = 0;
	private boolean 				isRememberPw = true;
	private List<IDDCountry>        countryList;

	private IDDCodesNTimesFragment  iDDCodesNTimesFragment;
	private IDDRatesFragment  		iDDRatesFragment;
	private ServListFragment		servListFragment;
	private FragmentManager 		fragmentManager ;
	private LRTabButton 			btn;
	
	//status
	//True--left False--right 
	private Boolean is0060View = false;
	private String srvNum = "63832232";
	private String custNum = "98166600";
	//View
	private Activity 				me;
	
	//Bundle
	public static String BUNDLEIS0060 = "BUNDLEIS0060";

	@Override
	public final void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);	
		// reload Data
		ClnEnv.updateUILocale(getBaseContext(),	ClnEnv.getAppLocale(getBaseContext()));
		setContentView(R.layout.activity_idd);
		me = this;
		extralinespace = (int) getResources().getDimension(R.dimen.extralinespace);
		basePadding = (int) getResources().getDimension(R.dimen.basePadding);
		is0060View = getIntent().getBooleanExtra(BUNDLEIS0060, false);
	}



	@Override
	protected void onNewIntent(Intent intent) {
		super.onNewIntent(intent);
		ClnEnv.updateUILocale(getBaseContext(),	ClnEnv.getAppLocale(getBaseContext()));
	}

	@Override
	protected final void onStart() {
		super.onStart();
		
		//Screen Tracker
	}

	protected void initUI2() {

		aq = new AAQuery(this);
		fragmentManager  =  getSupportFragmentManager();
		//navbar
		aq.navBarButton(R.id.navbar_button_left, R.drawable.btn_back);
		aq.id(R.id.navbar_button_left).clicked(this, "onClick");
		aq.navBarTitle(R.id.navbar_title, getResources().getString(R.string.MYHKT_IDD_TITLE));

		aq.id(R.id.idd_frame1).backgroundColorId(R.color.white);
		aq.marginpx(R.id.idd_frame1, basePadding, 0, basePadding, 0);
		aq.id(R.id.idd_frame1).getView().setPadding(0, extralinespace, 0, 0);

		btn = (LRTabButton) aq.id(R.id.idd_lrtabbtn).getView();
		btn.initViews(this, getResString(R.string.MYHKT_IDD_BTN_CALL_RATE), getResString(R.string.MYHKT_IDD_BTN_CODE_N_TIME), -1 ,-1, (int)getResources().getDimension(R.dimen.iddlrtabtextsize), (int) getResources().getDimension(R.dimen.lr_button_height), LayoutParams.MATCH_PARENT);
//		btn.initViews(this, getResString(R.string.MYHKT_IDD_BTN_CALL_RATE), getResString(R.string.MYHKT_IDD_BTN_CODE_N_TIME));
		aq.id(R.id.idd_lrtabbtn).height((int)getResources().getDimension(R.dimen.lr_button_height) , false);
		btn.clicked(this, "onClick");
		btn.setBtns(is0060View);
		changeFragment();
   	}

	public void changeFragment(){
		FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
		//Screen Tracker
		if (is0060View) {
//			iDDRatesServiceFragment = new IDDRateServiceFragment();
//			fragmentTransaction.replace(R.id.idd_frame2, iDDRatesServiceFragment);
			servListFragment = new ServListFragment();
			fragmentTransaction.replace(R.id.idd_frame2, servListFragment);
		
			//tracker

		} else {		
			iDDCodesNTimesFragment = new IDDCodesNTimesFragment();
			fragmentTransaction.replace(R.id.idd_frame2, iDDCodesNTimesFragment);
			
			//tracker
		}
		//tracker
		Utils.closeSoftKeyboard(this);
		fragmentTransaction.commit();
	}


	@Override
	public void goToIddRateFragment() {
		FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
		iDDRatesFragment = new IDDRatesFragment();
		fragmentTransaction.setCustomAnimations(R.anim.right_slide_in, R.anim.left_slide_out);
		if (is0060View) {
			fragmentTransaction.replace(R.id.idd_frame2, iDDRatesFragment);
		}
		Utils.closeSoftKeyboard(this);
		fragmentTransaction.commit();
		
	}
	// Android Device Back Button Handling
	public final void onBackPressed() {
		//			clearNotificationData();
		finish();
		overridePendingTransition(R.anim.left_slide_in, R.anim.right_slide_out);
	}

	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.navbar_button_left:
			finish();
			overridePendingTransition(R.anim.left_slide_in, R.anim.right_slide_out);
			break;
		case LRTabButton.leftBTnid:
			is0060View = true;
			//check the login status
			if (!ClnEnv.isLoggedIn()) {
				loginFirstDialog();
			} else {
				changeFragment();
			}
			btn.setBtns(is0060View);
			moduleId = is0060View ? getResources().getString(R.string.MODULE_LTS_IDD) : getResources().getString(R.string.MODULE_LTS_IDD);
			break;
		case LRTabButton.rightBTnid:
			is0060View = false;
			changeFragment();
			btn.setBtns(is0060View);
			moduleId = is0060View ? getResources().getString(R.string.MODULE_LTS_IDD) : getResources().getString(R.string.MODULE_LTS_IDD);
			break;			
		}
	}

	public void loginFirstDialog() {
		DialogHelper.createSimpleDialog(me,getResString(R.string.MYHKT_IDD_LOGIN), getResString(R.string.btn_ok), new DialogInterface.OnClickListener(){

			@Override
			public void onClick(DialogInterface dialog, int which) {
				Intent intent;
				intent = new Intent(me, LoginActivity.class);
				Bundle rbundle = new Bundle();
				
				rbundle.putSerializable("CLICKBUTTON", HomeButtonItem.MAINMENU.IDD);
				intent.putExtras(rbundle);
				startActivity(intent);
				overridePendingTransition(R.anim.right_slide_in, R.anim.left_slide_out);
				finish();						
			}			
		});
		
	}
	
	@Override
	protected final void onResume() {
		super.onResume();
		// Update Locale		
		ClnEnv.updateUILocale(getBaseContext(),	ClnEnv.getAppLocale(getBaseContext()));
		moduleId = is0060View ? getResources().getString(R.string.MODULE_LTS_IDD) : getResources().getString(R.string.MODULE_LTS_IDD);
	}	
	

	@Override
	protected final void onPause() {
		super.onPause();
	}

	@Override
	protected final void onStop() {
		super.onStop();
	}



	public void onSuccess(APIsResponse response) {
		super.onSuccess(response);
		if (response != null) {
		}
	}

	public void onFail(APIsResponse response) {
		super.onFail(response);
		if (response != null) { 

			// General Error Message
			if (!"".equals(response.getMessage()) && response.getMessage() != null) {
				displayDialog(this, response.getMessage());
			} else if (RC.TCSESS_MM.equalsIgnoreCase(response.getReply().getCode())) {
				ivSessDialog();
			} else {
				displayDialog(this, ClnEnv.getRPCErrMsg(this, response.getReply().getCode()));
			}
		}
	}

	@Override
	public List<IDDCountry> getCountryList() {
		return countryList;
	}

	@Override
	public void setCountryList(List<IDDCountry> mCountryList) {
		countryList = mCountryList;		
	}



	@Override
	public String getSrvNum() {
		return acctAgent.getSrvNum();
	}



	@Override
	public String getCustNum() {
		return acctAgent.getCusNum();
	}



	@Override
	public int getType() {
		return ServListFragment.IDDRate;
	}


	private AcctAgent acctAgent;
	@Override
	public void setAcctAgent(AcctAgent mAcctAgent) {
		acctAgent = mAcctAgent; 
	}

	@Override
	public AcctAgent getAcctAgent() {
		return acctAgent;
	}	
}