package com.pccw.myhkt.lib.ui;

import android.content.Context;
import android.os.Build;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.pccw.myhkt.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MyHktImageButton extends ConstraintLayout {

    private final Context mContext;

    @BindView(R.id.iv_btn_action_icon) ImageView btnIcon;
    @BindView(R.id.tv_btn_action_text) TextView btnText;

    public MyHktImageButton(Context context) {
        super(context);
        mContext = context;
    }

    public MyHktImageButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        mContext = context;
    }

    public MyHktImageButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mContext = context;
    }

    public void init(int iconId, String text) {
        inflate(getContext(), R.layout.newhktbutton, this);
        ButterKnife.bind(this);
        btnText.setText(text);
        if(Build.VERSION.SDK_INT > Build.VERSION_CODES.JELLY_BEAN_MR2)
            btnIcon.setImageDrawable(mContext.getDrawable(iconId));
        else
            btnIcon.setImageDrawable(ContextCompat.getDrawable(mContext, iconId));
    }

    public void init(int iconId, int text) {
        inflate(getContext(), R.layout.newhktbutton, this);
        ButterKnife.bind(this);
        btnText.setText(mContext.getResources().getString(text));
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
            btnIcon.setImageDrawable(mContext.getDrawable(iconId));
        else
            btnIcon.setImageDrawable(ContextCompat.getDrawable(mContext, iconId));
    }

    public void setText(String text) {
        btnText.setText(text);
    }

    public void setText(int text) {
        btnText.setText(mContext.getResources().getString(text));
    }

    public void setImageDrawable(int iconId) {
        btnIcon.setImageDrawable(mContext.getDrawable(iconId));
    }

    public void setOnClickListener(View.OnClickListener onClickListener) {
        super.setOnClickListener(onClickListener);
    }

}
