/*
    Default User in Dango
    
    Keywords
    --------
    $URL: svn://10.87.120.207/dango/rel/v16.0/src/com/pccw/dango/shared/entity/OsUsr.java $
    $Rev: 844 $
    $Date: 2016-10-17 11:54:01 +0800 (¶g¤@, 17 ¤Q¤ë 2016) $
    $Author: upd $
*/

package com.pccw.dango.shared.entity;

import java.io.Serializable;


public class OsUsr extends BaseUserEx implements Serializable
{
    private static final long serialVersionUID = 3532348210328423763L;
    
    private String                  usrId;
    

    public static void main(String rArg[])
    {
        System.out.println(getVer());
        return;
    }
    
    
    public static String getVer()
    {
        return ("$URL: svn://10.87.120.207/dango/rel/v16.0/src/com/pccw/dango/shared/entity/OsUsr.java $, $Rev: 844 $");
    }
    
    
    public OsUsr()
    {
        super();
        usrId = ID_UNKN;
    }
    
    
    public OsUsr(String rUsrId)
    {
        super();
        usrId = rUsrId;
    }
    
    
    public String getId()
    {
        return (usrId);
    }
    
    
    public String getType()
    {
        return (TY_OSUR);
    }
    
    
    public String getRole()
    {
        return ("");
    }
    
    
    public String[] getRghtAry()
    {
        return (new String[0]);
    }
    
    
    public boolean isAuth(String rRght)
    {
        return (false);
    }
}
