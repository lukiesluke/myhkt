package com.pccw.myhkt.fragment;

import com.google.gson.Gson;
import com.pccw.dango.shared.cra.ApptCra;
import com.pccw.dango.shared.entity.SubnRec;
import com.pccw.dango.shared.entity.SveeRec;
import com.pccw.myhkt.APIsResponse;
import com.pccw.biz.myhkt.R;
import com.pccw.myhkt.Utils;
import com.pccw.myhkt.fragment.SRCreationFragment.OnLnttListener;
import com.pccw.myhkt.lib.ui.AAQuery;
import com.pccw.myhkt.model.LnttAgent;

import android.os.Bundle;
import androidx.fragment.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;



public class LnttFragment extends BaseServiceFragment implements OnLnttListener{
	private LnttFragment me;
	private AAQuery aq;

	private SveeRec sveeRec = null;
	private FragmentTransaction ft;

	private String TAG = "LnttFragment";
	private Boolean isRsShown = false;

	private LnttRsFragment lnttRsFragment;
	private LnttStartFragment lnttStartFragment;
	private SRCreationFragment srCreationFragment;
	private SRConfirmFragment srConfirmFragment;

	private LnttAgent lnttAgent;
	private ApptCra apptCra = null;

	protected int	activeChildview = R.string.CONST_SELECTEDVIEW_LINETEST;	// Initial default subview


	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		lnttRsFragment = new LnttRsFragment();
		lnttStartFragment = new LnttStartFragment();
		srCreationFragment = new SRCreationFragment();
		srConfirmFragment = new SRConfirmFragment();
	}

	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		me = this;
		ft = getChildFragmentManager().beginTransaction();
		View fragmentLayout = inflater.inflate(R.layout.fragment_myproflogin, container, false);
		return fragmentLayout;
	}

	@Override
	public void onStart() {
		super.onStart();
	}

	@Override
	public void onResume(){
		super.onResume();
	}

	@Override
	public void refresh(){
		super.refresh();		
		if(callback_main.isLnttServiceClearable()) {
			Utils.clearLnttService(getActivity());
			lnttAgent = Utils.getPrefLnttAgent(getActivity());
			callback_main.setLnttAgent(lnttAgent);
			callback_main.setLnttCra(null);
			//		callback_main.setApptCra(null);		
		}
		activeChildview = R.string.CONST_SELECTEDVIEW_LINETEST1;		
		displayChildview(0);
		lnttStartFragment.refresh();
//		refreshData();
	}

	@Override
	public final void refreshData() {
		super.refreshData();
		if (callback_main.getActiveSubview() == R.string.CONST_SELECTEDVIEW_LINETEST || callback_main.getActiveSubview() == R.string.CONST_SELECTEDVIEW_LINETEST1 || callback_main.getActiveSubview() == R.string.CONST_SELECTEDVIEW_LINETEST3) {	
			if (debug) Log.i(TAG, "Z");
			if (activeChildview == R.string.CONST_SELECTEDVIEW_SRCREATION || activeChildview == R.string.CONST_SELECTEDVIEW_SRCREATION_CONFIRM) {
				displayChildview(0);
			} else {
				lnttAgent = callback_main.getLnttAgent();
				callback_main.setApptCra(null);

				Gson gson = new Gson(); 
				if (lnttAgent!=null) {if (debug) Utils.showLog(TAG, gson.toJson(lnttAgent));}

				// Make Sure the Result is not expired and EndTimestamp is not empty
				if (!"".equalsIgnoreCase(lnttAgent.getEndTimestamp()) && Utils.isExpiredLnttResult(me.getActivity(), lnttAgent.getEndTimestamp())) {
					//			Utils.clearLnttService(me.getActivity());
					lnttAgent = new LnttAgent();
					if (debug) Log.i(TAG, "A");
					isRsShown = true;
					activeChildview = R.string.CONST_SELECTEDVIEW_LINETEST3;
					displayChildview(0);
					//			ft.replace(R.id.fragment_myproflogin_fragment, lnttRsFragment).commitAllowingStateLoss();
					return;
				}
				// No Line Test processing || Returned Lntt Error Msg || Line Test Complete but different srvNum
				if ("".equalsIgnoreCase(lnttAgent.getLnttSrvNum()) || (lnttAgent.getLnttSrvNum().equalsIgnoreCase(callback_main.getSubnRec().srvNum) && !"".equalsIgnoreCase(lnttAgent.getResultMsg())) || (!lnttAgent.getLnttSrvNum()
						.equalsIgnoreCase(callback_main.getSubnRec().srvNum) && (!"".equalsIgnoreCase(lnttAgent.getEndTimestamp())))) {
					isRsShown = false;
					if (debug) Log.i(TAG, "B");
					activeChildview = R.string.CONST_SELECTEDVIEW_LINETEST1;
					displayChildview(0);
					//			ft.replace(R.id.fragment_myproflogin_fragment, lnttStartFragment).commitAllowingStateLoss();
				} else if (!(lnttAgent.getLnttSrvNum().equalsIgnoreCase(callback_main.getSubnRec().srvNum) && lnttAgent.getLnttCra().getISubnRec().acctNum.equalsIgnoreCase(callback_main.getSubnRec().acctNum))) {
					//			displayDialog(getResString(R.string.MYHKT_LT_ERR_RUNNING));
					activeChildview = R.string.CONST_SELECTEDVIEW_LINETEST1;
					displayChildview(0);
					//			ft.replace(R.id.fragment_myproflogin_fragment, lnttStartFragment).commitAllowingStateLoss();
				} else if ("".equalsIgnoreCase(lnttAgent.getEndTimestamp())) {
					//			displayDialog(getResString(R.string.MYHKT_LT_MSG_NORMAL_START_SUCCESS));					
//					ft.replace(R.id.fragment_myproflogin_fragment, lnttStartFragment).commitAllowingStateLoss();
					activeChildview = R.string.CONST_SELECTEDVIEW_LINETEST1;
					displayChildview(0);
				} else {
					isRsShown = true;
					if (debug) Log.i(TAG, "C");
					activeChildview = R.string.CONST_SELECTEDVIEW_LINETEST3;
					displayChildview(0);
					//			ft.replace(R.id.fragment_myproflogin_fragment, lnttRsFragment).commitAllowingStateLoss();
				}
				//			Log.i(TAG, "D" + isRsShown);
				if (callback_main.getActiveSubview() == R.string.CONST_SELECTEDVIEW_LINETEST) {
					int lob = callback_main.getLob();
					if (lob == R.string.CONST_LOB_LTS) { callback_livechat.setModuleId(getResString(R.string.MODULE_LTS_LTT)); }
					if (lob == R.string.CONST_LOB_PCD) { callback_livechat.setModuleId(getResString(R.string.MODULE_PCD_LTT)); }
					if (lob == R.string.CONST_LOB_TV) { callback_livechat.setModuleId(getResString(R.string.MODULE_TV_LTT)); }
					switch (activeChildview) {
					case R.string.CONST_SELECTEDVIEW_LINETEST1:
						lnttStartFragment.refreshData();
						break;
					case R.string.CONST_SELECTEDVIEW_LINETEST3:
						lnttRsFragment.refreshData();
						break;
					case R.string.CONST_SELECTEDVIEW_SRCREATION:
						srCreationFragment.refreshData();
						break;
					case R.string.CONST_SELECTEDVIEW_SRCREATION_CONFIRM:
						srConfirmFragment.refreshData();
						break;
					}
				}
			}
		}
	}

	public void openSRCreationFrag(){
		//		SRCreationFragment srCreationFragment = new SRCreationFragment();
		ft = getChildFragmentManager().beginTransaction();
		ft.setCustomAnimations(R.anim.right_slide_in, R.anim.left_slide_out);
		ft.replace(R.id.fragment_myproflogin_fragment, srCreationFragment).commit();
	}

	public final void displayChildview(int type) {
		ft = getChildFragmentManager().beginTransaction();
		if (type == 1) {
			ft.setCustomAnimations(R.anim.right_slide_in, R.anim.left_slide_out);	
		} else if (type == 2){
			ft.setCustomAnimations(R.anim.left_slide_in, R.anim.right_slide_out);
		} else {
			ft.setCustomAnimations(0, 0);
		}

		switch (activeChildview) {
		case R.string.CONST_SELECTEDVIEW_LINETEST1:
			setModuleId();
			ft.replace(R.id.fragment_myproflogin_fragment, lnttStartFragment);					
			break;
		case R.string.CONST_SELECTEDVIEW_LINETEST3:
			setModuleId();
			ft.replace(R.id.fragment_myproflogin_fragment, lnttRsFragment);
			break;
		case R.string.CONST_SELECTEDVIEW_SRCREATION:
			//			srCreationFragment = new SRCreationFragment();
			setModuleId();
			ft.replace(R.id.fragment_myproflogin_fragment, srCreationFragment);
			//					ft.addToBackStack(null);
			break;
		case R.string.CONST_SELECTEDVIEW_SRCREATION_CONFIRM:
			//			srConfirmFragment = new SRConfirmFragment();
			setModuleId();
			ft.replace(R.id.fragment_myproflogin_fragment, srConfirmFragment);
			//					ft.addToBackStack(null);
			break;
		}
		ft.commit();
	}

	protected void setModuleId(){
		int lob = callback_main.getLob();
		switch (activeChildview) {
		case R.string.CONST_SELECTEDVIEW_LINETEST1:
		case R.string.CONST_SELECTEDVIEW_LINETEST3:
			if (lob == R.string.CONST_LOB_LTS) { callback_livechat.setModuleId(getResString(R.string.MODULE_LTS_LTT)); }
			if (lob == R.string.CONST_LOB_PCD) { callback_livechat.setModuleId(getResString(R.string.MODULE_PCD_LTT)); }
			if (lob == R.string.CONST_LOB_TV) { callback_livechat.setModuleId(getResString(R.string.MODULE_TV_LTT)); }
			break;
		case R.string.CONST_SELECTEDVIEW_SRCREATION:			
		case R.string.CONST_SELECTEDVIEW_SRCREATION_CONFIRM:
			if (lob == R.string.CONST_LOB_LTS) { callback_livechat.setModuleId(getResString(R.string.MODULE_LTS_SR)); }
			if (lob == R.string.CONST_LOB_PCD) { callback_livechat.setModuleId(getResString(R.string.MODULE_PCD_SR)); }
			if (lob == R.string.CONST_LOB_TV) { callback_livechat.setModuleId(getResString(R.string.MODULE_TV_SR)); }
			break;
		}
		super.setModuleId();
	}

	@Override
	public void setActiveChildview(int index) {
		activeChildview = index;
	}


	@Override
	public void onSuccess(APIsResponse response) {
	}

	@Override
	public void onFail(APIsResponse response) {

	}

	@Override
	public int getActiveChildview() {
		return activeChildview;
	}

}
