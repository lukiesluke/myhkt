/*
    Class for Bilingual Text Tag
    
    Mirror of the TXTG
    For GWT (client/shared) package access
    
    Keywords
    --------
    $URL: svn://10.87.120.207/dango/rel/v16.0/src/com/pccw/dango/shared/entity/BiTx.java $
    $Rev: 844 $
    $Date: 2016-10-17 11:54:01 +0800 (¶g¤@, 17 ¤Q¤ë 2016) $
    $Author: upd $
*/

package com.pccw.dango.shared.entity;

import java.io.Serializable;

import com.pccw.wheat.shared.tool.MiniRtException;
import com.pccw.wheat.shared.tool.Tool;



public class BiTx implements Serializable
{
    private static final long serialVersionUID = -1941109155002241697L;
    
    protected String                tag;
    protected String                zh;
    protected String                en;

    public static final String      LANG_ZH     = "zh";
    public static final String      LANG_EN     = "en";
    
    
    public static void main(String rArg[])
    {
        System.out.println(getVer());
        return;
    }
    
    
    public static String getVer()
    {
        return ("$URL: svn://10.87.120.207/dango/rel/v16.0/src/com/pccw/dango/shared/entity/BiTx.java $, $Rev: 844 $");
    }
    

    final void initAndClear()
    {
        init();
        clear();
    }
    
    
    protected void init()
    {
    }
    
    
    public void clear()
    {
        clearTag();
        clearZh();
        clearEn();
    }
    
    
    public BiTx copyFrom(BiTx rBT)
    {
        setTag(rBT.getTag());
        setZh(rBT.getZh());
        setEn(rBT.getEn());
        
        return (this);
    }
    
    
    public BiTx copyTo(BiTx rBT)
    {
        rBT.copyFrom(this);
        return (rBT);
    }
    
    
    public BiTx copyMe()
    {
        BiTx                      rDes;
        
        rDes = new BiTx();
        rDes.copyFrom(this);
        
        return (rDes);
    }
    
    
    public void clearTag()
    {
        setTag("");
    }
    
    
    public void setTag(String rTag)
    {
        tag = rTag;
    }
    
    
    public String getTag()
    {
        return (tag);
    }
    
    
    public void clearZh()
    {
        setZh("");
    }
    
    
    public void setZh(String rZh)
    {
        zh = rZh;
    }
    
    
    public String getZh()
    {
        return (zh);
    }

    
    public void clearEn()
    {
        setEn("");
    }
    
    
    public void setEn(String rEn)
    {
        en = rEn;
    }
    
    
    public String getEn()
    {
        return (en);
    }
    
    
    public static boolean isValidLang(String rLang)
    {
        if (!Tool.isNil(rLang)) {
            if (rLang.equals(LANG_EN)) return (true);
            if (rLang.equals(LANG_ZH)) return (true);
        }
        
        return (false);
    }
    
    
    public static void assureValidLang(String rLang)
    {
        if (rLang.equals(LANG_EN)) return;
        if (rLang.equals(LANG_ZH)) return;
        
        throw new MiniRtException("Unexpected Language("+rLang+")!");
    }
    
    
    public static boolean isZh(String rLang)
    {
        assureValidLang(rLang);
        return (rLang.equals(LANG_ZH));
    }
    
    
    public static boolean isEn(String rLang)
    {
        assureValidLang(rLang);
        return (rLang.equals(LANG_EN));
    }
    
    
    public String getText(String rLang)
    {
        assureValidLang(rLang);
        
        if (rLang.equals(LANG_EN)) return (getEn());
        if (rLang.equals(LANG_ZH)) return (getZh());
        
        return (""); /* Fool the Comipler */
    }
}