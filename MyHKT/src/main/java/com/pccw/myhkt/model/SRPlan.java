package com.pccw.myhkt.model;

import java.util.ArrayList;

import android.os.Parcel;
import android.os.Parcelable;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

@Root(name = "SRPlan", strict = false)
public class SRPlan implements Parcelable{

    @Element(name = "serviceType", required = false)
    public String serviceType;

    @Element(name = "engDestName", required = false)
    public String engDestName;

    @Element(name = "engDestNameExt", required = false)
    public String engDestNameExt;

    @Element(name = "chiDestName", required = false)
    public String chiDestName;

    @Element(name = "chiDestNameExt", required = false)
    public String chiDestNameExt;

    public ArrayList<Rate> rateList = new ArrayList<Rate>();

    @Element(name = "rates", required = false)
    public Rates rates;

	public SRPlan() { }

    public Rates getRates() {
        return rates;
    }

    public SRPlan(Parcel in) {
        this.serviceType = in.readString();
        this.engDestName = in.readString();
        this.engDestNameExt = in.readString();
        this.chiDestName = in.readString();
        this.chiDestNameExt = in.readString();
        this.rateList = in.readArrayList(Rate.class.getClassLoader());
    }

    public static final SRPlan.Creator CREATOR = new SRPlan.Creator() {
        public SRPlan createFromParcel(Parcel in) {
            return new SRPlan(in);
        }

        public SRPlan[] newArray(int size) {
            return new SRPlan[size];
        }
    };
    
	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.serviceType);
        dest.writeString(this.engDestName);
        dest.writeString(this.engDestNameExt);
        dest.writeString(this.chiDestName);
        dest.writeString(this.chiDestNameExt);
        dest.writeList(this.rateList);
	}

}
