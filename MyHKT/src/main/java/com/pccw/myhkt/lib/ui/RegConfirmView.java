package com.pccw.myhkt.lib.ui;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.LinearLayout.LayoutParams;

import com.androidquery.AQuery;
import com.pccw.myhkt.R;
import com.pccw.myhkt.Utils;

public class RegConfirmView extends LinearLayout {

	private AQuery aq;

	public RegConfirmView(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
	}

	public RegConfirmView(Context context, AttributeSet attrs) {
		super(context, attrs);
		initViews(context, attrs);
	}

	public RegConfirmView(Context context, AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);
		initViews(context, attrs);
	}

	private void initViews(final Context context, AttributeSet attrs) {
		TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.HomeImageButton, 0, 0);
		aq = new AQuery(this);

		LayoutInflater.from(context).inflate(R.layout.item_reg_confirm, this);
		try {
		

		} finally {
			a.recycle();
		}
	}

	public void initViews(final Context context, int height, String title, String content ) {
		LayoutInflater.from(context).inflate(R.layout.item_reg_confirm, this ,false);
		aq = new AQuery(this);

		aq.id(R.id.reg_confirm_layouttitle).text(title);
		aq.id(R.id.reg_confirm_layoutcontent).text(content);
		// Style Initialize
		aq.id(R.id.reg_confirm_layouttitle).height(height/2, false);
		aq.id(R.id.reg_confirm_layoutcontent).height(height/2, false);
		
	}

}
