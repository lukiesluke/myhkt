package com.pccw.myhkt.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.bumptech.glide.Priority;
import com.pccw.dango.shared.cra.InbxCra;
import com.pccw.dango.shared.entity.IbxMsgRec;
import com.pccw.dango.shared.entity.SubnRec;
import com.pccw.dango.shared.tool.RC;
import com.pccw.myhkt.APIsManager;
import com.pccw.myhkt.APIsResponse;
import com.pccw.myhkt.ClnEnv;
import com.pccw.myhkt.DialogHelper;
import com.pccw.myhkt.FAWrapper;
import com.pccw.myhkt.GlideApp;
import com.pccw.myhkt.LiveChatHelper;
import com.pccw.myhkt.R;
import com.pccw.myhkt.Utils;
import com.pccw.myhkt.dialogs.LOBSelectionDialog;
import com.pccw.myhkt.dialogs.LOBSelectionDialogCompat;
import com.pccw.myhkt.enums.MessageCategory;
import com.pccw.myhkt.lib.ui.AAQuery;
import com.pccw.myhkt.lib.ui.HKTButton;
import com.pccw.myhkt.listeners.OnDownloadAttachmentListener;
import com.pccw.myhkt.model.ImageHolder;
import com.pccw.myhkt.util.Constant;

import java.io.File;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/************************************************************************
 * File : InboxDetailActivity.java
 * Desc : Inbox item details
 * Name : InboxDetailActivity
 * by : Addulmoiz Esmail
 * Date : 20/07/2018
 * Change History:
 * Date       Modified By		Description
 * ---------- ----------------	-------------------------------
 * 20/07/2018 Abdulmoiz Esmail  -First draft
 *************************************************************************/
public class InboxDetailActivity extends BaseActivity implements APIsManager.OnAPIsListener, OnDownloadAttachmentListener {

    private AAQuery aq;
    HKTButton mOpenUrlBtn;
    IbxMsgRec inboxContent;
    LinearLayout deleteMesageButton;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inbox_detail);
        aq.id(R.id.navbar_button_right).clicked(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //login required
                if (LiveChatHelper.isPause) {
                    openLiveChat();
                } else {
                    popupLOBSelection();
                }
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        moduleId = getResString(R.string.MODULE_MY_MESSAGES);
        FAWrapper.getInstance().sendFAScreen(this, R.string.CONST_SCRN_INBOX_DETAILS, false);
    }

    private void popupLOBSelection() {
        ArrayList<ImageHolder> arrayList = new ArrayList<>();
        arrayList.add(new ImageHolder(R.drawable.lob_pcd_plain, SubnRec.LOB_PCD));
        arrayList.add(new ImageHolder(R.drawable.lob_lts_plain, SubnRec.LOB_LTS));
        arrayList.add(new ImageHolder(R.drawable.lob_tv_plain, SubnRec.LOB_TV));
        arrayList.add(new ImageHolder(R.drawable.lob_1010_plain, SubnRec.WLOB_X101));
        arrayList.add(new ImageHolder(R.drawable.lob_csl_plain, SubnRec.WLOB_CSL));
        arrayList.add(new ImageHolder(R.drawable.lob_csp_plain, SubnRec.LOB_CSP));
        spawnLOBDialog(arrayList);
    }

    private void spawnLOBDialog(ArrayList<ImageHolder> arrayList) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
            new LOBSelectionDialog(this, true, new DialogInterface.OnCancelListener() {
                @Override
                public void onCancel(DialogInterface dialog) {
                    Log.d(this.getClass().getName(), "onCancel: No selection was made.");
                }
            }, arrayList).show();
        else new LOBSelectionDialogCompat(this, true, new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                Log.d(this.getClass().getName(), "onCancel: No selection was made.");
            }
        }, arrayList).show();
    }

    @Override
    protected void initUI2() {
        super.initUI2();

        aq = new AAQuery(this);

        //navbar
        aq.navBarButton(R.id.navbar_button_left, R.drawable.btn_back);
        aq.navBarTitle(R.id.navbar_title, getString(R.string.title_inbox));
        aq.id(R.id.navbar_button_left).clicked(this, "onClick");

        int textSize = (int) getResources().getDimension(R.dimen.textsize_xlarge);
        mOpenUrlBtn = (HKTButton) aq.id(R.id.open_web_button).getView();
        deleteMesageButton = (LinearLayout) aq.id(R.id.delete_container).getView();
        mOpenUrlBtn.initViews(this, HKTButton.TYPE_DARK_BLUE_INBOX, getString(R.string.inbox_button_url), textSize, ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        mOpenUrlBtn.setPadding(60, basePadding, 60, basePadding);
        aq.id(R.id.open_web_button).clicked(this, "onClick");


        inboxContent = (IbxMsgRec) getIntent().getSerializableExtra(Constant.INBOX_BUNDLE_CONTENT);
        if (inboxContent != null) {
            String imgUrl = isZh ? inboxContent.pathZh : inboxContent.pathEn;
            if (!(imgUrl != null && !TextUtils.isEmpty(imgUrl.trim()))) {
                // aq.id(R.id.email_text_content).getTextView().setText(isZh ? inboxContent.ctntZh : inboxContent.ctntEn);
                aq.id(R.id.email_text_content).getTextView().setText(isZh ? inboxContent.ctntZh.replace("\r", System.getProperty("line.separator")) : inboxContent.ctntEn.replace("\r", System.getProperty("line.separator")));
                aq.id(R.id.email_text_content).getTextView().setVisibility(View.VISIBLE);
            } else {
                aq.id(R.id.email_text_content).getTextView().setVisibility(View.GONE);
            }
            aq.id(R.id.email_img_category).getImageView().setImageDrawable(getResources().getDrawable(MessageCategory.getMessageCategoryIcon(MessageCategory.fromString(inboxContent.msgCat))));
            aq.id(R.id.email_text_date).getTextView().setText(Utils.toDateString(inboxContent.createTs, InboxListActivity.IBX_DATE_FORMAT, isZh ? "yyyy'年'MMM" : "MMM yyyy", isZh));
            aq.id(R.id.email_text_title).getTextView().setText(isZh ? inboxContent.subjZh : inboxContent.subjEn);
            //aq.id(R.id.email_text_content).getTextView().setText(isZh ? Html.fromHtml(inboxContent.ctntZh) : Html.fromHtml(inboxContent.ctntEn));

            aq.id(R.id.email_text_remaining).getTextView().setText(Utils.toDateString(inboxContent.createTs, InboxListActivity.IBX_DATE_FORMAT, "dd/MM/yyyy"));

            String urlBrowser = isZh ? inboxContent.urlZh.trim() : inboxContent.urlEn.trim();

//            GlideApp.with(this)
//                    .load(imgUrl)
//                    .priority(Priority.HIGH)
//                    .into(aq.id(R.id.email_image_view).getImageView());

            if (!TextUtils.isEmpty(imgUrl)) {
                APIsManager.doDownloadImageAttachment(this.getApplicationContext(), imgUrl, this, false, "");
            }

            if (!TextUtils.isEmpty(urlBrowser)) {
                aq.id(R.id.email_image_view).clicked(this, "onClick");
                mOpenUrlBtn.setVisibility(View.VISIBLE);
            }

            aq.id(R.id.delete_container).clicked(this, "onClick");

            //Call api request mark the email read
            if (!inboxContent.readSts.equalsIgnoreCase("Y")) {
                InbxCra inbxCra = new InbxCra();
                inbxCra.setILoginId(ClnEnv.getLoginId());
                inbxCra.setIIbxMsgRec(inboxContent);
                APIsManager.doUpdateInboxStatus(this, inbxCra);
            }
        }
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.navbar_button_left:
                onBackPressed();
                break;
            case R.id.open_web_button:
            case R.id.email_image_view:
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(isZh ? inboxContent.urlZh : inboxContent.urlEn));
                startActivity(i);
                overridePendingTransition(R.anim.right_slide_in, R.anim.left_slide_out);
                break;
            case R.id.delete_container:
                showDeleteMessagePopup();
                break;
        }
    }

    private void showDeleteMessagePopup() {
        AlertDialog.Builder builder = new AlertDialog.Builder(InboxDetailActivity.this);
        builder.setMessage(this.getString(R.string.INBOX_DELETE_ALERT));

        builder.setPositiveButton(R.string.MYHKT_BTN_CONFIRM, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int id) {
//                ((InboxDetailActivity) activity).finishActivity();
                InbxCra inbxCra = new InbxCra();
                inbxCra.setILoginId(ClnEnv.getLoginId());
                inbxCra.setIIbxMsgRec(inboxContent);
                APIsManager.doDeleteMessageFromInbox(InboxDetailActivity.this, inbxCra);
            }
        });
        builder.setNegativeButton(R.string.btn_cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
            }
        });

        AlertDialog alert = builder.create();
        alert.show();
    }

    public void finishActivity() {
        Intent intent = new Intent();
        intent.putExtra("MSG_ID", inboxContent.rid);
        setResult(Activity.RESULT_OK, intent);
        finish();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        overridePendingTransition(R.anim.left_slide_in, R.anim.right_slide_out);
    }

    @Override
    public void onSuccess(APIsResponse response) {
        super.onSuccess(response);

        //resave the inbox list
        if (response != null) {
            if (response.getActionTy().equals(APIsManager.INBX_UPD_RDSTS) || response.getActionTy().equals(APIsManager.INBX_DEL_MSG)) {
                boolean isDeleteMsgRequest = response.getActionTy().equals(APIsManager.INBX_DEL_MSG);

                InbxCra inbxCra = (InbxCra) response.getCra();
                if (isDeleteMsgRequest)
                    inbxCra.setOIbxMsgRecAry(removeDeletedMessageIfReturned(inbxCra));

                if (inbxCra != null) {
                    ClnEnv.setPref(this.getApplicationContext(), Constant.CONST_INBOX_CRA_RESPONSE, Utils.serialize(inbxCra));

                    //broadcast: inbox is updated
                    sendBroadcast(new Intent().setAction(Constant.ACTION_INBOX_LIST_UPDATED).setPackage(getPackageName()));
                    if (isDeleteMsgRequest) finishActivity();
                }
            }
        }
    }

    private IbxMsgRec[] removeDeletedMessageIfReturned(InbxCra inbxCra) {
        IbxMsgRec[] ibxMsgRecs = inbxCra.getOIbxMsgRecAry();
        List<IbxMsgRec> result = new LinkedList();

        for (IbxMsgRec item : ibxMsgRecs)
            if (item.rid != inboxContent.rid) result.add(item);
        return result.toArray(ibxMsgRecs);
    }

    @Override
    public void onFail(APIsResponse response) {
        super.onFail(response);
        if (response.getActionTy().equals(APIsManager.INBX_DEL_MSG)) {
            // General Error Message
            if (!"".equals(response.getMessage()) && response.getMessage() != null) {
                DialogHelper.createSimpleDialog(InboxDetailActivity.this, response.getMessage());
            } else if (RC.TCSESS_MM.equalsIgnoreCase(response.getReply().getCode())) {
                BaseActivity.ivSessDialog();
            } else {
                DialogHelper.createSimpleDialog(InboxDetailActivity.this, ClnEnv.getRPCErrMsg(InboxDetailActivity.this, response.getReply().getCode()));
            }
        }
    }

    @Override
    public void onSuccessDownload(File file) {
        GlideApp.with(this).load(Uri.fromFile(file)).priority(Priority.HIGH).into(aq.id(R.id.email_image_view).getImageView());
    }

    @Override
    public void onFailedDownload() {
        Toast.makeText(this, "Failed  download attachment", Toast.LENGTH_SHORT).show();
    }
}
