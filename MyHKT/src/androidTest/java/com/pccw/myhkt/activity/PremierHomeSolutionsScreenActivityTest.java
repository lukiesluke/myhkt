package com.pccw.myhkt.activity;

import android.content.Context;
import android.content.SharedPreferences;
import androidx.test.espresso.intent.Intents;
import androidx.test.rule.ActivityTestRule;
import androidx.test.rule.GrantPermissionRule;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import com.pccw.myhkt.AccountHelper;
import com.pccw.myhkt.ClnEnv;
import com.pccw.myhkt.R;
import com.pccw.myhkt.base.BaseTest;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static androidx.test.platform.app.InstrumentationRegistry.getInstrumentation;

@Ignore
@RunWith(AndroidJUnit4.class)
public class PremierHomeSolutionsScreenActivityTest extends BaseTest {
    private Context context;
    SharedPreferences shared;

    @Rule
    public ActivityTestRule<MainMenuActivity> activityTestRule = new ActivityTestRule<>(MainMenuActivity.class);
    @Rule
    public GrantPermissionRule permissionRule = GrantPermissionRule.grant(android.Manifest.permission.WRITE_EXTERNAL_STORAGE);

    @Before
    public void setUp(){
        context = getInstrumentation().getTargetContext();
        shared = context.getSharedPreferences(
                "myhkt",
                Context.MODE_PRIVATE);

        if(ClnEnv.isLoggedIn()){
            executeLogout(context);
        }else{
            Intents.init();
        }

        loginUser(context, shared,
                AccountHelper.getUATAccount6().getUsername(),
                AccountHelper.getUATAccount6().getPassword(),
                true);
        gotoMainMenuItem(5);
    }

    @After
    public void tearDown() {
        if(ClnEnv.isLoggedIn()){
            executeLogout(context);
        }
        Intents.release();
    }

    @Test
    public void testDisplayUI() {
        checkStringIsDisplayed(context, R.string.MYHKT_HOME_BTN_SOLUTION);
        backBtnExist();
        checkLiveChatBtnIsDisplayed();
        checkViewIsDisplayed(R.id.activity_browser_webview);
    }

    @Test
    public void testLiveChat() {
        checkLiveChat(context,R.string.LIVE_CHAT_DISCLAIMER,
                R.id.activity_browser_webview);
    }

    @Test
    public void testBackBtn() {
        checkBackBtn(MainMenuActivity.class.getName(),5);
    }

}