package com.pccw.myhkt.fragment;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;

import com.pccw.dango.shared.cra.PlanMobCra;
import com.pccw.dango.shared.entity.MobUsage;
import com.pccw.dango.shared.entity.SubnRec;
import com.pccw.dango.shared.g3entity.G3BoostUpOffer1DTO;
import com.pccw.dango.shared.g3entity.G3DisplayServiceItemDTO;
import com.pccw.dango.shared.g3entity.G3UsageQuotaDTO;
import com.pccw.myhkt.APIsResponse;
import com.pccw.myhkt.FAWrapper;
import com.pccw.myhkt.R;
import com.pccw.myhkt.Utils;
import com.pccw.myhkt.activity.BaseActivity;
import com.pccw.myhkt.cell.model.ArrowTextCell;
import com.pccw.myhkt.cell.model.BigTextCell;
import com.pccw.myhkt.cell.model.Cell;
import com.pccw.myhkt.cell.model.CircleViewCell;
import com.pccw.myhkt.cell.model.IconTextCell;
import com.pccw.myhkt.cell.model.SingleBtnCell;
import com.pccw.myhkt.cell.model.SmallTextCell;
import com.pccw.myhkt.cell.model.WebViewCell;
import com.pccw.myhkt.cell.view.CellViewAdapter;
import com.pccw.myhkt.enums.HKTDataType;
import com.pccw.myhkt.lib.ui.AAQuery;
import com.pccw.myhkt.model.AcctAgent;
import com.pccw.myhkt.util.Constant;

import java.util.ArrayList;
import java.util.List;

/************************************************************************
File       : UsageDataChild1Fragment.java
Desc       : View for showing csl/1010/o2f Usage/Roaming
			 isMob: true for csl , false for 1010/o2f case
			 isLocal: true for local data, false for roaming
Name       : UsageDataChild1Fragment
Created by : Andy Wong
Date       : 15/03/2016

Change History:
Date       Modified By			Description
---------- ----------------	-------------------------------
15/03/2016 Andy Wong			- First draft
14/06/2017 Abdulmoiz Esmail     - Update showing of remaining data usage, use the KB or MB form depending on the size. 
15/06/2017 Abdulmoiz Esmail		- 59645: Change HSim Normal Sim header as Secondary Sim in Usage
18/06/2018 Abdulmoiz Esmial     - 74137: Global asia plan
26/07/2018 Abdulmoiz Esmail     - 76196: Incorrect display of MUP individual usage and null check of remaining MB
 *************************************************************************/
@SuppressLint("ResourceAsColor")
public class UsageDataChild1Fragment extends BaseServiceFragment {
	private UsageDataChild1Fragment me;
	private View myView;
	private AAQuery	aq;
	private List<Cell> cellList;
	private int col2Width;
	private int col3Width;
	private int btnWidth;

	private Boolean isMob = false;
	private Boolean isCSimPrim = false;
	private Boolean isLocal = true;
	private Boolean isGlobal = false;
	
	private Boolean isMup = false;
	private Boolean isMaster = false;

	private OnUsageDataListener callback;
	private OnUsageListener callback_usage;

	private CellViewAdapter cellViewAdapter;
	private LinearLayout frame;

	private PlanMobCra planMobCra;
	private MobUsage mobUsage;


	List<G3DisplayServiceItemDTO> serviceList;

	//Use in UsageFragment
	public interface OnUsageListener {
		void setPlanMobCra(PlanMobCra mplanMobcra);
		PlanMobCra getPlanMobCra();
		void setSelectedServiceItem(G3DisplayServiceItemDTO g3DisplayServiceItemDTO);
		G3DisplayServiceItemDTO getSelectedServiceItem();
		G3BoostUpOffer1DTO getSelectedG3BoostUpOfferDTO();
		void getSelectedG3BoostUpOfferDTO(G3BoostUpOffer1DTO g3BoostUpOffer1DTO);
		void setActiveChildview(int index);
		int getActiveChildview();
		void displayChildview(Boolean isBack);
		Boolean getIsLocal();
		int getLob();
	}

	public interface OnUsageDataListener {
		PlanMobCra getPlanMobCra();
		void setPlanMobCra(PlanMobCra mplanMobcra);
		List<G3DisplayServiceItemDTO> getLocalServiceList();
		List<G3DisplayServiceItemDTO> getGlobalServiceList();
		List<G3DisplayServiceItemDTO> getRommaServiceList();
		List<G3DisplayServiceItemDTO> getOtherLocalServiceList();
		List<G3DisplayServiceItemDTO> getOtherRommaServiceList();
		AcctAgent getAcctAgent();
	}

	public static UsageDataChild1Fragment onInstance(String region) {
		Bundle bundle = new Bundle();
		bundle.putString(Constant.REGION, region);

		UsageDataChild1Fragment usageDataChild1Fragment = new UsageDataChild1Fragment();
		usageDataChild1Fragment.setArguments(bundle);

		return usageDataChild1Fragment;
	}

	@Override
	public void onAttach(@NonNull Activity activity) {
		super.onAttach(activity);
		try {
			callback = (OnUsageDataListener)getParentFragment();
			callback_usage = (OnUsageListener)getParentFragment().getParentFragment();
		} catch (ClassCastException e) {
			throw new ClassCastException(getParentFragment().toString() + " must implement OnUsageListener");
		}		
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		if (callback_main != null && callback_main.getAcctAgent() != null
				&& callback_main.getAcctAgent().getSubnRec() != null
				&& !TextUtils.isEmpty(callback_main.getAcctAgent().getSubnRec().lob)) {

			if (SubnRec.LOB_MOB.equals(callback_main.getAcctAgent().getSubnRec().lob)
					|| SubnRec.LOB_IOI.equals(callback_main.getAcctAgent().getSubnRec().lob)
					|| SubnRec.WLOB_XMOB.equals(callback_main.getAcctAgent().getSubnRec().lob)
					|| SubnRec.LOB_CSP.equals(callback_main.getAcctAgent().getSubnRec().lob)
					|| SubnRec.WLOB_XCSP.equals(callback_main.getAcctAgent().getSubnRec().lob)) {
				isMob = true;
			}
		}
		//		isMob = true;//derek testing remove after
		isLocal = callback_usage.getIsLocal();
		if (callback.getPlanMobCra() != null && callback.getPlanMobCra().getOMobUsage() != null) {
			G3UsageQuotaDTO g3OtherUsageDTO = callback.getPlanMobCra().getOMobUsage().getG3UsageQuotaDTO();
			if (g3OtherUsageDTO != null) {
				isCSimPrim = "P".equalsIgnoreCase(g3OtherUsageDTO.getPrimaryFlag());
			}
		}

		//check if the instance of the class is global
		Bundle bundle = getArguments();
		if (bundle != null && bundle.getString(Constant.REGION).equalsIgnoreCase("GLOBAL")) {
			isGlobal = true;
		}
	}

	public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		me = this;
		View fragmentLayout = inflater.inflate(R.layout.fragment_usagedata_child1, container, false);
		myView = fragmentLayout;
		initData();
		initUI();
		return fragmentLayout;
	}

	protected void initData() {    		
		super.initData();
		aq = new AAQuery(myView);
		btnWidth = deviceWidth/2 - basePadding; 
		col2Width = (deviceWidth - basePadding *2 )/4;
		col3Width = (deviceWidth - basePadding *2 ) * 2/9;
		planMobCra = callback.getPlanMobCra();
		cellViewAdapter = new CellViewAdapter(me.getActivity());
	}

	protected void initUI() {
		super.initUI();

		if (planMobCra == null) {
			if (isMob) {
				initMobUI();
			} else {
				init1010UI();
			}
		} else {
			if (isMob) {
				setMobUI();
			} else {
				set1010UI();
			}
		}
	}

	protected void initMobUI() {
		aq.id(R.id.fragment_usagedata_layout).backgroundColorId(R.color.white);
		aq.marginpx(R.id.fragment_usagedata_layout, 0, 0, 0, 0);
		frame = (LinearLayout) aq.id(R.id.fragment_usagedata_listview).getView();

		cellList = new ArrayList<Cell>();
		Cell cell;
		int mobusageImg = Utils.theme(R.drawable.icon_mobusage, callback_usage.getLob()); //themed icon
        cell = new Cell(Cell.ICONTEXT, mobusageImg, getResString(determineMobileData()));
        cellList.add(cell);
		cellViewAdapter.setView(frame, cellList);
	}

	protected void init1010UI() {
		if (debug) Log.i(TAG, callback.getAcctAgent().getLob());
		aq.id(R.id.fragment_usagedata_layout).backgroundColorId(R.color.white);
		aq.marginpx(R.id.fragment_usagedata_layout, 0, 0, 0, 0);
		frame = (LinearLayout) aq.id(R.id.fragment_usagedata_listview).getView();

		cellList = new ArrayList<Cell>();
		Cell cell;
		if (SubnRec.LOB_MOB.equalsIgnoreCase(callback.getAcctAgent().getLob()) || SubnRec.LOB_IOI.equalsIgnoreCase(callback.getAcctAgent().getLob())) {
			int mobusageImg = Utils.theme(R.drawable.icon_mobusage, callback_usage.getLob()); //themed icon
            cell = new Cell(Cell.ICONTEXT, mobusageImg, getResString(determineMobileData()));
			cell.setLeftPadding(basePadding);
			cell.setRightPadding(basePadding);
			cellList.add(cell);
		} else if (SubnRec.LOB_O2F.equalsIgnoreCase(callback.getAcctAgent().getLob()) || SubnRec.LOB_101.equalsIgnoreCase(callback.getAcctAgent().getLob())) {
			int mobusageImg = Utils.theme(R.drawable.icon_mobusage, callback_usage.getLob()); //themed icon
			cell = new Cell(Cell.ICONTEXT, mobusageImg, getResString(isLocal ? R.string.csl_usage_mobile_data : R.string.roam_mobile_data));
			cell.setLeftPadding(basePadding);
			cell.setRightPadding(basePadding);
			cellList.add(cell);
		}
		cellViewAdapter.setView(frame, cellList);
	}

	protected void setMobUI() {
		aq.id(R.id.fragment_usagedata_layout).backgroundColorId(R.color.white);
		aq.marginpx(R.id.fragment_usagedata_layout, 0, 0, 0, 0);
		frame = (LinearLayout) aq.id(R.id.fragment_usagedata_listview).getView();

		isMup = planMobCra.isMup();
		isMaster = planMobCra.isMaster();

		//get service list processed by UsageDataFragment
		//check if is fragment is for global to get the global service list
		serviceList = isLocal ? (isGlobal ? callback.getGlobalServiceList() : callback.getLocalServiceList()) : callback.getRommaServiceList();
		cellList = new ArrayList<Cell>();

		List<G3DisplayServiceItemDTO> otherCountries = null;
		G3DisplayServiceItemDTO totalDto = null;
		G3DisplayServiceItemDTO serviceDTO = null;

		if (isLocal) {
			int[] widthText2Col = {col2Width, col2Width};
			int[] widthText3Col = {col3Width, col3Width, col3Width};
			mobUsage = planMobCra.getOMobUsage();

			G3UsageQuotaDTO g3UsageQuotaDTO = mobUsage.getG3UsageQuotaDTO();

			if (isMup) {
				G3DisplayServiceItemDTO mupItem = null;
				String masterMrt = planMobCra.getMasterMrt();
				if (serviceList !=null && serviceList.size() > 0) {
					for (int i=0;i < serviceList.size() ; i++){
						if ("MUP Individual Usage".equals(serviceList.get(i).getServiceType())) {
							mupItem = serviceList.get(i);
							break;
						}
					}
					for (int i=0;i < serviceList.size() ; i++){
						if (!"MUP Individual Usage".equals(serviceList.get(i).getServiceType())) {
							serviceDTO = serviceList.get(i);
							this.addCircleView_MUP(serviceList.get(i), mupItem, masterMrt);			
						}
					}
				}
			} else {
				if (serviceList != null && serviceList.size() > 0) {
					otherCountries = new ArrayList<>();
					for (int i = 0; i < serviceList.size(); i++) {
						if (isGlobal) {
							if (serviceList.get(i).getUsageBar().getShowBar().equals("Y")) {
								totalDto = serviceList.get(i);
								serviceDTO = serviceList.get(i);
								addCircleView(serviceList.get(i));
							} else {
								otherCountries.add(serviceList.get(i));
							}
						} else {
							serviceDTO = serviceList.get(i);
							addCircleView(serviceList.get(i));
						}
					}
				}
			}
			//Local Voice part
			String subTitle ="";
			if (g3UsageQuotaDTO.getAsOfDate() != null) {
				String outStr = "";
				try {
					outStr = Utils.toDateString(g3UsageQuotaDTO.getAsOfDate(), "dd/MM/yyyy", Utils.getString(me.getActivity(), R.string.usage_date_format));
				} catch (Exception e) {
					outStr = g3UsageQuotaDTO.getAsOfDate();
				}
				subTitle = String.format("%s%s", getResString(R.string.as_of), outStr);
			}

			if (!isMup) {
				String[] title = {};
				String[] unitVoice = {};
				String[] minIntra = {};
				String[] minNormal= {};

				title = new String[] {isGlobal ? getResString(R.string.usage_entitled_global): getResString(R.string.usage_individual_entitled),
                        isGlobal ? getResString(R.string.usage_used_global) : getResString(R.string.usage_individual)};

				String unitLabel = isGlobal ? "GB" : getResString(R.string.mins);
				unitVoice = new String[]{"(" + unitLabel + ")", "(" + unitLabel + ")"};

				if(isGlobal) {
					//Display other countries
					displayAsianGlobalOtherCountries(otherCountries, totalDto,
							title, unitVoice, widthText2Col);

				} else {
					String localVoiceIntraEntitlement = convertNum(g3UsageQuotaDTO.getLocalVoiceIntraEntitlement());
					String localVoiceInterEntitlement = convertNum(g3UsageQuotaDTO.getLocalVoiceInterEntitlement());
					if (localVoiceIntraEntitlement.equals(getResString(R.string.usage_unlimited)) || localVoiceInterEntitlement.equals(getResString(R.string.usage_unlimited))) {
						localVoiceIntraEntitlement = getResString(R.string.usage_unlimited);
						localVoiceInterEntitlement = getResString(R.string.usage_unlimited);
					}

					// jarlou
					if (Utils.isFUP(serviceDTO)) {
						showUnlimittedPlan(getResString(R.string.usage_fup_title), getString(R.string.usage_fup));
					} else if (Utils.isQOS(serviceDTO)) {
						showUnlimittedPlan(getResString(R.string.usage_qos_title), getString(R.string.usage_qos));
					}

					minIntra = new String[]{localVoiceIntraEntitlement, convertNum(g3UsageQuotaDTO.getLocalVoiceIntraUsage())};
					minNormal = new String[]{localVoiceInterEntitlement, convertNum(g3UsageQuotaDTO.getLocalVoiceInterUsage())};

					int voicecallImg = Utils.theme(R.drawable.icon_voicecall, callback_usage.getLob()); //themed icon
					IconTextCell cell1 = new IconTextCell(voicecallImg, getResString(R.string.usage_voice_call));
					cellList.add(cell1);

					ArrowTextCell cell2 = new ArrowTextCell(subTitle, title, unitVoice, widthText2Col);
					cell2.setNoteSizeDelta(-4);
					cell2.setContentSizeDelta(-4);
					cellList.add(cell2);
					ArrowTextCell cell3 = new ArrowTextCell(getResString(R.string.usage_normal), minNormal, null, widthText2Col, R.color.cell_bg_grey);
					cell3.setNoteSizeDelta(-4);
					cell3.setContentSizeDelta(-4);
					cell3.setContentColorId(R.color.dark_blue);
					cellList.add(cell3);
					ArrowTextCell cell4 = new ArrowTextCell(getResString(R.string.usage_intra_network), minIntra, null, widthText2Col, R.color.white);
					cell4.setNoteSizeDelta(-4);
					cell4.setContentSizeDelta(-4);
					cell4.setContentColorId(R.color.dark_blue);
					cellList.add(cell4);
				}
			} else {
				String[] title = {};
				String[] unitVoice = {};
				String[] minIntra = {};
				String[] minNormal= {};

				/*Maseter will show one more col: group usage */
				title = isMaster ?	new String[] {getResString(R.string.usage_individual_entitled), getResString(R.string.usage_individual), getResString(R.string.usage_group)}
								  :	new String[] {getResString(R.string.usage_individual_entitled), getResString(R.string.usage_individual)};
				unitVoice = isMaster ? new String[] {"("+getResString(R.string.mins)+")", "("+getResString(R.string.mins)+")", "("+getResString(R.string.mins)+")" }
									  : new String[] {"("+getResString(R.string.mins)+")", "("+getResString(R.string.mins)+")" };

				String localVoiceIntraEntitlement = convertNum(g3UsageQuotaDTO.getLocalVoiceIntraEntitlement());
				String localVoiceInterEntitlement = convertNum(g3UsageQuotaDTO.getLocalVoiceInterEntitlement());
				if (localVoiceIntraEntitlement.equals(getResString(R.string.usage_unlimited)) || localVoiceInterEntitlement.equals(getResString(R.string.usage_unlimited))) {
					localVoiceIntraEntitlement = getResString(R.string.usage_unlimited);
					localVoiceInterEntitlement = getResString(R.string.usage_unlimited);
				}

				minIntra = isMaster ? new String[] {localVoiceIntraEntitlement,convertNum(g3UsageQuotaDTO.getLocalVoiceIntraUsage()), convertNum(g3UsageQuotaDTO.getLocalVoiceIntraUsageGrp())}
									 : new String[] {localVoiceIntraEntitlement,convertNum(g3UsageQuotaDTO.getLocalVoiceIntraUsage())};
				minNormal = isMaster ? new String[] {localVoiceInterEntitlement,convertNum(g3UsageQuotaDTO.getLocalVoiceInterUsage()), convertNum(g3UsageQuotaDTO.getLocalVoiceInterUsageGrp())}
									  :	new String[] {localVoiceInterEntitlement,convertNum(g3UsageQuotaDTO.getLocalVoiceInterUsage())};

				int voicecallImg = Utils.theme(R.drawable.icon_voicecall, callback_usage.getLob()); //themed icon
				IconTextCell cell1 = new IconTextCell(voicecallImg, getResString(R.string.usage_voice_call));
				cellList.add(cell1);

				ArrowTextCell cell2 = new ArrowTextCell(subTitle ,title, unitVoice, isMaster? widthText3Col:widthText2Col);
				cell2.setNoteSizeDelta(-4);
				cell2.setContentSizeDelta(-4);
				cellList.add(cell2);
				ArrowTextCell cell3 = new ArrowTextCell(getResString(R.string.usage_normal), minNormal, null, isMaster? widthText3Col:widthText2Col, R.color.cell_bg_grey);
				cell3.setNoteSizeDelta(-4);
				cell3.setContentSizeDelta(-4);
				cell3.setContentColorId(R.color.dark_blue);
				cellList.add(cell3);
				ArrowTextCell cell4 = new ArrowTextCell(getResString(R.string.usage_intra_network),minIntra , null,isMaster? widthText3Col:widthText2Col,R.color.white);
				cell4.setNoteSizeDelta(-4);
				cell4.setContentSizeDelta(-4);
				cell4.setContentColorId(R.color.dark_blue);
				cellList.add(cell4);
			}
		} else {
			//Roaming part
			if (serviceList != null && serviceList.size() > 0) {
				for (int i = 0; i < serviceList.size(); i++) {
					addCircleView(serviceList.get(i));
				}
			} else {
				Cell cell;
				int mobusageImg = Utils.theme(R.drawable.icon_mobusage, callback_usage.getLob()); //themed icon
                cell = new Cell(Cell.ICONTEXT, mobusageImg, getResString(determineMobileData()));
				cellList.add(cell);

				String messageInfo = SubnRec.LOB_CSP.equals(callback_main.getAcctAgent().getSubnRec().lob)
						|| SubnRec.LOB_MOB.equals(callback_main.getAcctAgent().getSubnRec().lob)
						|| SubnRec.LOB_IOI.equals(callback_main.getAcctAgent().getSubnRec().lob)
						|| SubnRec.LOB_101.equals(callback_main.getAcctAgent().getSubnRec().lob)
						|| SubnRec.WLOB_XCSP.equals(callback_main.getAcctAgent().getSubnRec().lob)
						|| SubnRec.WLOB_XMOB.equals(callback_main.getAcctAgent().getSubnRec().lob) ?
						getResString(R.string.PLNMBM_NOROAMING_USAGE_CSP) : getResString(R.string.hsim_roaming_no_ser_remark);


				SmallTextCell noServiceRemark = new SmallTextCell(messageInfo, "");
				noServiceRemark.setRightPadding(basePadding);
				noServiceRemark.setLeftPadding(basePadding);
				noServiceRemark.setTitleSizeDelta(-2);
				cellList.add(noServiceRemark);

				//enquiry
				WebViewCell webViewCell = new WebViewCell(isZh ? "file:///android_asset/roaming_enquiry_zh.html" : "file:///android_asset/roaming_enquiry_en.html");
				webViewCell.setTopMargin(extralinespace / 3);
				cellList.add(webViewCell);
				SingleBtnCell singleBtnCell = new SingleBtnCell(getResString(R.string.MYHKT_BTN_LIVECHAT), col2Width * 2, new OnClickListener() {

					@Override
					public void onClick(View v) {
						((BaseActivity) me.getActivity()).openLiveChat();
					}
				});
				singleBtnCell.setBgcolorId(R.color.cell_bg_grey);
				singleBtnCell.setDraw(R.drawable.livechat_small);
				singleBtnCell.setLeftMargin(basePadding);
				singleBtnCell.setRightMargin(basePadding);
				singleBtnCell.setLeftPadding(basePadding);
				singleBtnCell.setRightPadding(basePadding);
				cellList.add(singleBtnCell);
			}
		}

		cellViewAdapter.setView(callback_main.getAcctAgent().getSubnRec().lob, frame, cellList);
	}

    private int determineMobileData() {
	    if(isLocal) return R.string.usage_mobile_data;
	    else return R.string.usage_roam_mobile_data;
    }

	protected void set1010UI() {
		aq.id(R.id.fragment_usagedata_layout).backgroundColorId(R.color.white);
		aq.marginpx(R.id.fragment_usagedata_layout, 0, 0, 0, 0);
		frame = (LinearLayout) aq.id(R.id.fragment_usagedata_listview).getView();

		//get service list processed by usagedatafragment		
		serviceList = isLocal ? callback.getLocalServiceList() : callback.getRommaServiceList();
		cellList = new ArrayList<Cell>();

		if (serviceList != null && serviceList.size() > 0) {
			addCircleView(serviceList.get(0));
		}
		cellViewAdapter.setView(frame, cellList);
	}

	public void refreshData() {
		super.refreshData();
		initUI();
	}

	@Override
	public void onResume() {
		super.onResume();
		if (!Utils.getString(getActivity(), R.string.CONST_SCRN_MOBUSAGE).equals(callback_main.getTrackerId()) && callback_usage.getIsLocal()) {
			//Screen Tracker
			FAWrapper.getInstance().sendFAScreen(getActivity(), R.string.CONST_SCRN_MOBUSAGE, false);
			if (debug) Log.i("GATRACKER", "GATRACKER" + Utils.getString(getActivity(), R.string.CONST_SCRN_MOBUSAGE));
		} else if (!Utils.getString(getActivity(), R.string.CONST_SCRN_MOBUSAGE_ROAMING).equals(callback_main.getTrackerId()) && !callback_usage.getIsLocal()) {
			//Screen Tracker
			FAWrapper.getInstance().sendFAScreen(getActivity(), R.string.CONST_SCRN_MOBUSAGE_ROAMING, false);
		}
	}

	public void refresh(){
		super.refresh();
		isLocal = callback_usage.getIsLocal();

		if (!Utils.getString(getActivity(), R.string.CONST_SCRN_MOBUSAGE).equals(callback_main.getTrackerId()) && callback_usage.getIsLocal()) {
			callback_main.setTrackerId(Utils.getString(getActivity(), R.string.CONST_SCRN_MOBUSAGE));
			//Screen Tracker
			FAWrapper.getInstance().sendFAScreen(getActivity(), R.string.CONST_SCRN_MOBUSAGE,
					false);
			if (debug) Log.i("GATRACKER", "GATRACKER" + Utils.getString(getActivity(), R.string.CONST_SCRN_MOBUSAGE));
		} else if (!Utils.getString(getActivity(), R.string.CONST_SCRN_MOBUSAGE_ROAMING).equals(callback_main.getTrackerId()) && !callback_usage.getIsLocal()){
			callback_main.setTrackerId(Utils.getString(getActivity(), R.string.CONST_SCRN_MOBUSAGE_ROAMING));
			//Screen Tracker
			FAWrapper.getInstance().sendFAScreen(getActivity(), R.string.CONST_SCRN_MOBUSAGE_ROAMING,
					false);
		}
		initUI();
	}

	protected void cleanupUI() {
	}

	/**
	 * @param item
	 */
    private void addCircleView(final G3DisplayServiceItemDTO item) {
		//Title
		String title = isZh ? item.getTitleLevel1Chi() : item.getTitleLevel1Eng();
		String subTitle = isZh ? item.getTitleLevel2Chi() : item.getTitleLevel2Eng();
		if (subTitle != null && !subTitle.equals("")) {
			if (callback_usage.getLob() != R.string.CONST_LOB_1010 && callback_usage.getLob() != R.string.CONST_LOB_O2F) { //Csim bypass level2 title
			} else if (isCSimPrim) {
				title = title + " (" + getString(R.string.csl_usage_total) + ")";
			}
		}

		if (Utils.is5G(item)) {
			title = getString(R.string.usage_mobile_data_5g);
		}
        int mobusageImg = Utils.theme(R.drawable.icon_mobusage, callback_usage.getLob()); //themed icon
        IconTextCell titleCell = new IconTextCell(mobusageImg, title);
        titleCell.setLeftPadding(basePadding);
        titleCell.setRightPadding(basePadding);
        cellList.add(titleCell);

        //As of date
        String asOfDate = isZh ? item.getAsOfDateChi() : item.getAsOfDateEng();
        if (asOfDate != null && !asOfDate.equals("")) {
            ArrowTextCell arrowTextCell = new ArrowTextCell(asOfDate, null, null, null);
            arrowTextCell.setRightPadding(basePadding);
            arrowTextCell.setLeftPadding(basePadding);
            arrowTextCell.setArrowShown(true);
            cellList.add(arrowTextCell);
        }

        //ProgressBar
        if ("Y".equalsIgnoreCase(item.getUsageBar().getShowBar())) {
            String unit = "";

            //get usage remaining
            double remainingMB = 0;
            double entitlementMB = 0;
            String remaining = "---";
            String entitlement = "---";
            boolean isRemaining = false;

            remaining = Utils.formattedDataByMBStr(item.getUsageDescription().getRemainingInMB(), HKTDataType.HKTDataTypeRemaining, true);

            // Special handling in KB : added null checking
            String remainingKB = item.getUsageDescription().getRemainingInKB() != null ? item.getUsageDescription().getRemainingInKB() : "";
            if (remainingKB != null && !TextUtils.isEmpty(remainingKB) && Double.parseDouble(remainingKB.replace(",", "")) < 1024) {
                remaining = remainingKB + "KB";
            }

            if (!TextUtils.isEmpty(remaining)) {
                isRemaining = true;
            }

            CircleViewCell circlecell;
            if (isZh ? item.getUsageDescription().getTextChi().contains("100%+") :
                    item.getUsageDescription().getTextEng().contains("100%+")) {
                circlecell = new CircleViewCell(isZh ? "100%+" : "100%+", remaining, 100);
                circlecell.setContentSizeDelta(40);
                circlecell.setContentColorId(R.color.text_gray);
            } else {
                if (isRemaining) {
                    circlecell = new CircleViewCell(isZh ? item.getUsageBar().getBarPercent() + "%" :
                            item.getUsageBar().getBarPercent() + "%", remaining, item.getUsageBar().getBarPercent());
                    circlecell.setContentSizeDelta(40);
                    circlecell.setContentColorId(R.color.text_gray);
                } else {
                    circlecell = new CircleViewCell(isZh ? item.getUsageBar().getBarPercent() + "%" :
                            item.getUsageBar().getBarPercent() + "%", "", item.getUsageBar().getBarPercent());
                    circlecell.setContentSizeDelta(40);
                    circlecell.setContentColorId(R.color.text_gray);
                }
            }

            //set bar color
            if (item.getUsageBar().getBarPercent() < 10) {
                circlecell.setBarColor(R.color.cprogressbar_red);
                circlecell.setTitleColorId(R.color.cprogressbar_red);
            } else if (item.getUsageBar().getBarPercent() >= 10 && item.getUsageBar().getBarPercent() <= 20) {
                circlecell.setBarColor(R.color.cprogressbar_yellow);
                circlecell.setTitleColorId(R.color.cprogressbar_yellow);
            }

            // jarlou
            if (Utils.hasUnlimitedPlan(item) && Utils.hasReachUnlimitedPlan(item)) {
                if (Utils.isFUP(item)) {
                    circlecell = createUnlimittedCircleViewCell(getResString(R.string.usage_fup_use));
                } else if (Utils.isQOS(item)) {
                    circlecell = createUnlimittedCircleViewCell(getResString(R.string.usage_qos_use));
                }
            }

            circlecell.setLeftPadding(extralinespace * 4);
            circlecell.setRightPadding(extralinespace * 4);

            cellList.add(circlecell);
            if (Utils.hasReachUnlimitedPlan(item) && Utils.hasReachUnlimitedPlan(item) && Utils.is5G(item)) {
                cellList.add(createStatement(getString(R.string.usage_5g_use_statement)));
            } else {
                showStatement(item);
            }

            String remainingUsage = isZh ? item.getUsageDescription().getTextChi() : item.getUsageDescription().getTextEng();
            if ("QUOTA-BASED".equals(item.getUsageType())) {
                remainingUsage = "";
            }
            if (!TextUtils.isEmpty(remainingUsage)) {
                SmallTextCell smallTextCell = new SmallTextCell(remainingUsage, "");
                smallTextCell.setTitleGravity(Gravity.CENTER);
                smallTextCell.setBgcolorId(R.color.white);
                cellList.add(smallTextCell);
            }

        } else {
            SmallTextCell smallTextCell = new SmallTextCell(isZh ? item.getUsageDescription().getTextChi() : item.getUsageDescription().getTextEng(), "");
            smallTextCell.setBgcolorId(R.color.cell_bg_grey);
            cellList.add(smallTextCell);
        }
        //Topup btn
        //CSim always shows, HSim shows where allowTopup is Y
        if ("Y".equalsIgnoreCase(item.getAllowTopup()) || !isMob) {
            OnClickListener onClickListener = new OnClickListener() {
                @Override
                public void onClick(View v) {
                    callback_usage.setSelectedServiceItem(item);
                    callback_usage.setActiveChildview(R.string.CONST_SELECTEDFRAG_TOPUP1);
                    callback_usage.displayChildview(false);
                }
            };
            String btnName = getResString(R.string.usage_boost_up1);
            SingleBtnCell singleBtnCell = new SingleBtnCell(btnName, btnWidth, onClickListener);
            int topupImg = Utils.theme(R.drawable.icon_topup, callback_usage.getLob()); //themed icon
            singleBtnCell.setDraw(topupImg);
            cellList.add(singleBtnCell);
        }
    }

	/**
	 * @param item G3DisplayServiceItemDTO
	 * @param mupItem G3DisplayServiceItemDTO
	 * @param masterMrt String
	 */
	private void addCircleView_MUP(final G3DisplayServiceItemDTO item, G3DisplayServiceItemDTO mupItem, String masterMrt) {
		//Title 
		String title = isZh ? item.getTitleLevel1Chi() : item.getTitleLevel1Eng();
		title = title + getResString(R.string.usage_mobile_data_group);
		int mobusageImg = Utils.theme(R.drawable.icon_mobusage, callback_usage.getLob()); //themed icon
		if (Utils.is5G(item)) {
			title = getString(R.string.usage_mobile_data_5g);
		}
		IconTextCell titleCell = new IconTextCell(mobusageImg, title);
		titleCell.setLeftPadding(basePadding);
		titleCell.setRightPadding(basePadding);
		cellList.add(titleCell);

		IconTextCell corrPrimCell = new IconTextCell(R.drawable.icon_mobusage_inv, String.format(getResString(R.string.usage_mobile_data_group_sub), masterMrt));
		corrPrimCell.setLeftPadding(basePadding);
		corrPrimCell.setRightPadding(basePadding);
		corrPrimCell.setBotPadding(basePadding);
		corrPrimCell.setCellHeight(LayoutParams.WRAP_CONTENT);
		corrPrimCell.setTitleSizeDelta(-2);
		cellList.add(corrPrimCell);

		//As of date 
		String asOfDate = isZh ? item.getAsOfDateChi() : item.getAsOfDateEng();
		if (asOfDate != null && !asOfDate.equals("")) {
			ArrowTextCell arrowTextCell = new ArrowTextCell(asOfDate, null, null, null);
			arrowTextCell.setRightPadding(basePadding);
			arrowTextCell.setLeftPadding(basePadding);
			arrowTextCell.setArrowShown(true);
			cellList.add(arrowTextCell);
		}

		//ProgressBar
		if ("Y".equalsIgnoreCase(item.getUsageBar().getShowBar())) {	
			String unit = ""; 
			
			//get usage remaining
			double remainingMB = 0;
			double entitlementMB = 0;
			String remaining = "---";
			String entitlement = "---";
			String individual = "---";
			boolean isRemaining = false;

			remaining = Utils.formattedDataByMBStr(item.getUsageDescription().getRemainingInMB(),
					HKTDataType.HKTDataTypeRemaining, true);

			// Special handling in KB
			String remainingKB = item.getUsageDescription().getRemainingInKB() != null ? item.getUsageDescription().getRemainingInKB() : "";
			if(remainingKB != null && !TextUtils.isEmpty(remainingKB) && Double.parseDouble(remainingKB.replace(",", "")) < 1024) {
				remaining = remainingKB + "KB";
			}

			if(!TextUtils.isEmpty(remaining)) {
				isRemaining = true;
			}

			String individualUsed = mupItem != null && mupItem.getUsageDescription() != null && mupItem.getUsageDescription().getConsumedInMB() != null ? mupItem.getUsageDescription().getConsumedInMB() : "";
			if(individualUsed != null && !TextUtils.isEmpty(individualUsed)) {
                individual = Utils.formattedDataByMBStr(individualUsed,
                        HKTDataType.HKTDataTypeUsed, true);
            } else {
			    individual = "-";
            }

            String entitled = item.getUsageDescription().getInitialInMB() != null ? item.getUsageDescription().getInitialInMB() : "";
			if(entitled != null && !TextUtils.isEmpty(entitled)) {
				entitlement = Utils.formattedDataByMBStr(entitled,
						HKTDataType.HKTDataTypeEntitled, true);
			} else {
				entitlement = "-";
			}

			CircleViewCell circlecell;
			if (isRemaining) {
				circlecell = new CircleViewCell(isZh?  item.getUsageBar().getBarPercent() + "%" : item.getUsageBar().getBarPercent() + "%", remaining, item.getUsageBar().getBarPercent());
			} else {
				circlecell = new CircleViewCell(isZh?  item.getUsageBar().getBarPercent() + "%" : item.getUsageBar().getBarPercent() + "%", "", item.getUsageBar().getBarPercent());
			}

			//set bar color
			if (item.getUsageBar().getBarPercent() < 10) {
				circlecell.setBarColor(R.color.cprogressbar_red);
				circlecell.setTitleColorId(R.color.cprogressbar_red);
			} else if (item.getUsageBar().getBarPercent() >= 10 && item.getUsageBar().getBarPercent() <= 20) {
				circlecell.setBarColor(R.color.cprogressbar_yellow);
				circlecell.setTitleColorId(R.color.cprogressbar_yellow);
			}

			// jarlou
			if(Utils.hasUnlimitedPlan(item) && Utils.hasReachUnlimitedPlan(item)){
				if(Utils.isFUP(item)){
					circlecell = createUnlimittedCircleViewCell(getResString(R.string.usage_fup_use));
				}
				else if(Utils.isQOS(item)){
					circlecell = createUnlimittedCircleViewCell(getResString(R.string.usage_qos_use));
				}
			}

			circlecell.setLeftPadding(extralinespace*4);
			circlecell.setRightPadding(extralinespace*4);

			cellList.add(circlecell);

			// [Bugfix] If remaining InMB exists, reset usageDescription to be Empty String
			String remainingUsage = isZh? item.getUsageDescription().getTextChi() : item.getUsageDescription().getTextEng();
			if (!TextUtils.isEmpty(item.getUsageDescription().getRemainingInMB())) {
				remainingUsage = "";
			}
			if(!TextUtils.isEmpty(remainingUsage)) {
				SmallTextCell smallTextCell = new SmallTextCell(remainingUsage, "");
				smallTextCell.setTitleGravity(Gravity.CENTER);
				smallTextCell.setBgcolorId(R.color.white);
				cellList.add(smallTextCell);
			}

			if (!Utils.hasReachUnlimitedPlan(item)) {
				if (isMaster) {
					SmallTextCell dataDesCell = new SmallTextCell(String.format(getResString(R.string.usage_mobile_data_des_master), entitlement, individual) , "");
					dataDesCell.setTitleGravity(Gravity.CENTER);
					dataDesCell.setTitleSizeDelta(-2);
					cellList.add(dataDesCell);
				}
				else {
					SmallTextCell dataDesCell = new SmallTextCell(String.format(getResString(R.string.usage_mobile_data_des_slave), individual) , "");
					dataDesCell.setTitleGravity(Gravity.CENTER);
					dataDesCell.setTitleSizeDelta(-2);
					cellList.add(dataDesCell);
				}
			}
			if(Utils.hasReachUnlimitedPlan(item) && Utils.hasReachUnlimitedPlan(item) && Utils.is5G(item)){
				cellList.add(createStatement(getString(R.string.usage_5g_use_statement)));
			} else{
				showStatement(item);
			}
			
		} else {
			SmallTextCell smallTextCell = new SmallTextCell(isZh? item.getUsageDescription().getTextChi() : item.getUsageDescription().getTextEng() , "");
			cellList.add(smallTextCell);
		}
		
		//Topup btn
		//CSim always shows, HSim shows where allowTopup is Y 
		if ("Y".equalsIgnoreCase(item.getAllowTopup()) || !isMob) {
			OnClickListener onClickListener = new OnClickListener(){
				@Override
				public void onClick(View v) {
					callback_usage.setSelectedServiceItem(item);		
					callback_usage.setActiveChildview(R.string.CONST_SELECTEDFRAG_TOPUP1);
					callback_usage.displayChildview(false);
				}			
			};
			String btnName = getResString(R.string.usage_boost_up1);
			SingleBtnCell singleBtnCell = new SingleBtnCell(btnName, btnWidth, onClickListener);
			int topupImg = Utils.theme(R.drawable.icon_topup, callback_usage.getLob()); //themed icon
			singleBtnCell.setDraw(topupImg);
			cellList.add(singleBtnCell);		
		}
	}

	private String convertNum(String num) {
		if (num !=null) num = num.trim();
		if (num != null && num.length() != 0){
			if (num.equalsIgnoreCase("UNLIMIT")) {
				return getResString(R.string.usage_unlimited);
			} else {
				return Utils.formatNumber(Utils.truncateStringDot(num)); 
			}
		} else {
			return "-";
		}
	}

	/**
	 * Moiz: 06.18.18
	 * Display play the global other country list
	 * @param otherCountries : Service items usageBar.showBar : N
	 * @param totalDto : Service item usageBar.showBar : Y
	 * @param columnTitles StringAry
	 * @param units StringAry
	 * @param widthText2Col intAry
	 */
	private void displayAsianGlobalOtherCountries(List<G3DisplayServiceItemDTO> otherCountries, G3DisplayServiceItemDTO totalDto,
		String[] columnTitles, String[] units, int[] widthText2Col) {
		String entitledInGB;
		String consumedInGB;
		String sTitle;
		String totalDtoTitle = "";
		List<ArrowTextCell> countryList = new ArrayList<>();

		ArrowTextCell countryItem;
		boolean isOdd = false;

		//Total dto - showBar:Y
		if(totalDto != null) {
			totalDtoTitle = callback_usage.getPlanMobCra().getServerTS();//isZh ? totalDto.getAsOfDateChi() : totalDto.getAsOfDateEng();
			if (totalDtoTitle != null && !TextUtils.isEmpty(totalDtoTitle)) {
				String outStr = "";
				outStr = Utils.toDateString(totalDtoTitle, "yyyyMMddHHmmss", Utils.getString(getActivity(), R.string.asia_plan_date_format));
				totalDtoTitle = String.format("%s%s", getResString(R.string.as_of), outStr);
			}
			sTitle = Utils.getString(getActivity(), R.string.csl_usage_total);
			entitledInGB = Utils.formattedDataByMBStr(totalDto.getUsageDescription().getInitialInMB(),
					HKTDataType.HKTDataTypeEntitled, false);
			consumedInGB = Utils.formattedDataByMBStr(totalDto.getUsageDescription().getConsumedInMB(),
					HKTDataType.HKTDataTypeUsed, false);

			countryItem = new ArrowTextCell(sTitle, new String[]{entitledInGB, consumedInGB}, null, widthText2Col, R.color.cell_bg_grey);
			countryItem.setNoteSizeDelta(-4);
			countryItem.setContentSizeDelta(-4);
			countryItem.setContentColorId(R.color.dark_blue);
			countryList.add(countryItem);
		}

		// show other countries on the list
		if(otherCountries != null && !otherCountries.isEmpty()) {
			for (int i = 0; i < otherCountries.size(); i++) {
				G3DisplayServiceItemDTO dto = otherCountries.get(i);

				if (dto.getUsageBar().getShowBar().equals("N")) {
					sTitle = isZh ? dto.getTitleLevel1Chi() : dto.getTitleLevel1Eng();

					consumedInGB = Utils.formattedDataByMBStr(dto.getUsageDescription().getConsumedInMB(),
							HKTDataType.HKTDataTypeUsed, false);

					//this will define the row color
					if (i % 2 == 1) {
						isOdd = true;
					}

					countryItem = new ArrowTextCell(sTitle, new String[]{consumedInGB}, null, widthText2Col, isOdd ? R.color.cell_bg_grey : R.color.white);
					countryItem.setNoteSizeDelta(-4);
					countryItem.setContentSizeDelta(-4);
					countryItem.setContentColorId(R.color.dark_blue);
					countryList.add(countryItem);
				}
			}
		}

		ArrowTextCell cell2 = new ArrowTextCell(totalDtoTitle, columnTitles, units, widthText2Col);
		cell2.setNoteSizeDelta(-4);
		cell2.setContentSizeDelta(-4);
		cell2.setArrowShown(true);
		cellList.add(cell2);

		if(!countryList.isEmpty()) {
			cellList.addAll(countryList);
		}
	}

	public void showUnlimittedPlan(String title, String content){
		BigTextCell titleFUP = new BigTextCell(title, "");
		titleFUP.setTitleSizeDelta(-1);
		titleFUP.setLeftMargin(basePadding);
		titleFUP.setRightMargin(basePadding);
		titleFUP.setBotMargin(0);
		titleFUP.setTopMargin(0);
		titleFUP.setContentSizeDelta(0);
		cellList.add(titleFUP);

		SmallTextCell contentFUP;
		contentFUP = new SmallTextCell(content, "");
		contentFUP.setTitleSizeDelta(-2);
		contentFUP.setLeftPadding(0);
		contentFUP.setLeftMargin(basePadding);
		contentFUP.setRightPadding(0);
		contentFUP.setRightMargin(basePadding/2);
		cellList.add(contentFUP);
	}

	public void showStatement(G3DisplayServiceItemDTO item){
		if(Utils.hasReachUnlimitedPlan(item)){
			if(Utils.isFUP(item)){
				cellList.add(createStatement(getString(R.string.usage_fup_use_statement)));
			}
			else if(Utils.isQOS(item)){
				cellList.add(createStatement(getString(R.string.usage_qos_use_statement)));
			}
		}else{
			if(Utils.isQOS(item) && isMup){
				cellList.add(createStatement(getString(R.string.usage_qos_not_in_use_mup)));
			}
			else if(Utils.isQOS(item)){
				cellList.add(createStatement(getString(R.string.usage_qos_not_in_use)));
			}
		}
	}

	public SmallTextCell createStatement(String label){
		SmallTextCell dataDesCell;
		dataDesCell = new SmallTextCell(label , "");

		dataDesCell.setTitleGravity(Gravity.CENTER);
		dataDesCell.setTitleSizeDelta(-2);
		dataDesCell.setLeftPadding(0);
		dataDesCell.setLeftMargin(basePadding);
		dataDesCell.setRightPadding(0);
		dataDesCell.setRightMargin(basePadding);
		dataDesCell.setBotMargin(basePadding);
		return dataDesCell;
	}

	public CircleViewCell createUnlimittedCircleViewCell(String inUse){
		CircleViewCell circlecell = new CircleViewCell("", inUse, 100);
		circlecell.setContentSizeDelta(40);
		circlecell.setContentColorId(R.color.text_gray);
		circlecell.setBarColor(R.color.cpogressbar_unlimitted);
		circlecell.setTitleColorId(R.color.cpogressbar_unlimitted);
		return circlecell;
	}

	@Override
	public void onSuccess(APIsResponse response) { }

	@Override
	public void onFail(APIsResponse response) { }

}
