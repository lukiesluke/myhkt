package com.pccw.myhkt.model;

import com.google.gson.annotations.SerializedName;

public class AppConfigCommon {

    private String livechat_consumer_flag;
    private String livechat_premier_flag;
    private String show_fingerprint_banner;

    @SerializedName("banner_url_en")
    private String bannerUrlEn;

    @SerializedName("banner_url_zh")
    private String bannerUrlZh;

    @SerializedName("banner_url_pt_en")
    private String bannerUrlPremierEn;

    @SerializedName("banner_url_pt_zh")
    private String bannerUrlPremierZh;

    @SerializedName("banner_after_url_en")
    private String bannerAfterUrlEn;

    @SerializedName("banner_after_url_zh")
    private String bannerAfterUrlZh;

    public String getLivechat_consumer_flag() {
        return livechat_consumer_flag;
    }

    public void setLivechat_consumer_flag(String livechat_consumer_flag) {
        this.livechat_consumer_flag = livechat_consumer_flag;
    }

    public String getLivechat_premier_flag() {
        return livechat_premier_flag;
    }

    public void setLivechat_premier_flag(String livechat_premier_flag) {
        this.livechat_premier_flag = livechat_premier_flag;
    }

    public String getShow_fingerprint_banner() {
        return show_fingerprint_banner;
    }

    public void setShow_fingerprint_banner(String show_fingerprint_banner) {
        this.show_fingerprint_banner = show_fingerprint_banner;
    }

    public String getBannerUrlEn() {
        return bannerUrlEn;
    }

    public void setBannerUrlEn(String bannerUrlEn) {
        this.bannerUrlEn = bannerUrlEn;
    }

    public String getBannerUrlZh() {
        return bannerUrlZh;
    }

    public void setBannerUrlZh(String bannerUrlZh) {
        this.bannerUrlZh = bannerUrlZh;
    }

    public String getBannerUrlPremierEn() {
        return bannerUrlPremierEn;
    }

    public void setBannerUrlPremierEn(String bannerUrlPremierEn) {
        this.bannerUrlPremierEn = bannerUrlPremierEn;
    }

    public String getBannerUrlPremierZh() {
        return bannerUrlPremierZh;
    }

    public void setBannerUrlPremierZh(String bannerUrlPremierZh) {
        this.bannerUrlPremierZh = bannerUrlPremierZh;
    }

    public String getBannerAfterUrlEn() {
        return bannerAfterUrlEn;
    }

    public void setBannerAfterUrlEn(String bannerAfterUrlEn) {
        this.bannerAfterUrlEn = bannerAfterUrlEn;
    }

    public String getBannerAfterUrlZh() {
        return bannerAfterUrlZh;
    }

    public void setBannerAfterUrlZh(String bannerAfterUrlZh) {
        this.bannerAfterUrlZh = bannerAfterUrlZh;
    }
}
