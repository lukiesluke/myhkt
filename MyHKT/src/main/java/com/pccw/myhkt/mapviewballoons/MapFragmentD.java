package com.pccw.myhkt.mapviewballoons;

/************************************************************************
 File       : MapFragmentD.java
 Desc       : MapView based on Google Map API V2 implementation
 Name       : MapFragmentD
 Created by : Ryan Wong
 Date       : 14/03/2013

 Change History:
 Date       Modified By        	Description
 ---------- ----------------   	-------------------------------
 14/03/2013 Ryan Wong          	1.) First draft
 15/03/2013 Ryan Wong			1.) Added more Exception handling
 26/03/2013 Ryan Wong			- Updated default map location
 05/06/2013 Ryan Wong			- Added another pin style for CS Centers
 20/01/2013 Vincent Fung			- Added Shop Type Smart Living
 11/08/2014 Derek Tsui			- Added shop map images for csl, 1010, hkt
 *************************************************************************/

import java.util.HashMap;
import java.util.Map;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import android.location.LocationListener;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.InfoWindowAdapter;
import com.google.android.gms.maps.GoogleMap.OnInfoWindowClickListener;
import com.google.android.gms.maps.GoogleMap.OnMarkerClickListener;
import com.google.android.gms.maps.GoogleMap.OnMyLocationChangeListener;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.pccw.dango.shared.entity.ShopRec;
import com.pccw.myhkt.ClnEnv;
import com.pccw.myhkt.DialogHelper;
import com.pccw.myhkt.R;
import com.pccw.myhkt.Utils;
import com.pccw.myhkt.util.Constant;
import com.pccw.myhkt.util.RuntimePermissionUtil;

public class MapFragmentD extends Fragment implements OnMapReadyCallback {

    public static final int LOCATION_UPDATE_MIN_DISTANCE = 10;
    public static final int LOCATION_UPDATE_MIN_TIME = 5000;

    private GoogleMap mapView;
    private boolean traceMyLoc = false;
    private boolean runOnFirstFix = false;
    private boolean isZh = false;
    private BitmapDescriptor bitmap, bitmap2, bitmap3, bitmap4, bitmap5, bitmap6, bitmap7;
    private OnMapEventListener mapEventCallback = null;
    private ShopRec[] shopRec = null;
    private Map<Marker, ShopRec> markerLookup = null;
    private Activity thisActivity;

    private LatLng mPOILatLng = null;
    private LocationManager mLocationManager;

    Location mMyLocation = null;

    public MapFragmentD() {
    }

    @Override
    public final void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mLocationManager = (LocationManager) thisActivity.getSystemService(Context.LOCATION_SERVICE);
        traceMyLoc = getArguments().getBoolean("myLoc", false);
        isZh = getArguments().getBoolean("isZh", false);
    }

    @Override
    public final View onCreateView(LayoutInflater mInflater, ViewGroup arg1, Bundle arg2) {

        View fragmentLayout = mInflater.inflate(R.layout.fragment_map, arg1, false);

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);


        return fragmentLayout;
    }

    @Override
    public final void onInflate(Activity arg0, AttributeSet arg1, Bundle arg2) {
        super.onInflate(arg0, arg1, arg2);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        thisActivity = activity;

        // This makes sure that the container activity has implemented
        // the callback interface. If not, it throws an exception
        try {
            mapEventCallback = (OnMapEventListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement OnMapEventListener");
        }
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        thisActivity = getActivity();

        // This makes sure that the container activity has implemented
        // the callback interface. If not, it throws an exception
        try {
            mapEventCallback = (OnMapEventListener) getActivity();
        } catch (ClassCastException e) {
            throw new ClassCastException(getActivity().toString() + " must implement OnMapEventListener");
        }
    }


    @Override
    public final void onResume() {
        super.onResume();
    }

    @SuppressLint("MissingPermission")
    @Override
    public final void onPause() {
        super.onPause();
        if (traceMyLoc) {
            if (RuntimePermissionUtil.isLocationPermissionGranted(getActivity()) && mapView != null) {
                mapView.setMyLocationEnabled(false);
            } else {
                if (ClnEnv.getPref(getActivity(), Constant.LOCATION_PERMISSION_DENIED, false)) {
                    DialogHelper.createSimpleDialog(thisActivity, Utils.getString(thisActivity, R.string.permission_denied_setting_enabled), Utils.getString(thisActivity, R.string.btn_ok));
                } else {
                    RuntimePermissionUtil.requestLocationPermission(getActivity(), Constant.REQUEST_LOCATION_PERMISSION_DISABLED);
                }
            }
        }


    }

    @Override
    public final void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }

    @SuppressLint("MissingPermission")
    public final void movetomyloc() {
        if (traceMyLoc) {

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                boolean isGPSEnabled = mLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
                boolean isNetworkEnabled = mLocationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);


                if (isNetworkEnabled) {
                    mLocationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER,
                            LOCATION_UPDATE_MIN_TIME, LOCATION_UPDATE_MIN_DISTANCE, mLocationListener);
                    if (mLocationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER) != null) {
                        mMyLocation = mLocationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                    }
                }

                if (isGPSEnabled) {
                    mLocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,
                            LOCATION_UPDATE_MIN_TIME, LOCATION_UPDATE_MIN_DISTANCE, mLocationListener);
                    if (mLocationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER) != null) {
                        mMyLocation = mLocationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                    }
                }
            } else {
                if(mapView != null) {
                    mMyLocation = mapView.getMyLocation();
                }
            }
            if (mMyLocation == null) {
                DialogHelper.createSimpleDialog(thisActivity, Utils.getString(thisActivity, R.string.myhkt_shop_mylocturnonGPS), Utils.getString(thisActivity, R.string.btn_ok));
            } else {
                if(mapView != null) {
                    mapView.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(mMyLocation.getLatitude(), mMyLocation.getLongitude()), 17));
                }
            }
        }
    }

    public final void gotoPOI(double mlat, double mlng) {
        try {
            if (mapView != null) {
                setUpMapIfNeeded();
                mapView.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(mlat, mlng), 17));
            } else {
                //if map is not yet loaded in the screen, save the lat lng, then load it in onMapReady callback
                mPOILatLng = new LatLng(mlat, mlng);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private final void setUpMapIfNeeded() {

        try {
            // Do a null check to confirm that we have not already instantiated the map.
            if (mapView == null) {
                runOnFirstFix = false;

                //Move the current loading map pins to loadMapPins() functions for re-usability
                loadMapPins();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void loadMapPins() {
        if (mapView != null) {

            if (traceMyLoc) {
                mapView.setOnMyLocationChangeListener(onMyLocationChangeListener);

                // Set default zoom
                mapView.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(22.285523f, 114.154687f), 15));
            }
            mapView.setInfoWindowAdapter(new ShopInfoWindowAdapter());
            mapView.setOnInfoWindowClickListener(onInfoWindowClickListener);
            mapView.setOnMarkerClickListener(onMarkerClickListener);

            bitmap = BitmapDescriptorFactory.fromResource(R.drawable.pin_people);
            bitmap2 = BitmapDescriptorFactory.fromResource(R.drawable.pin_cs);
            bitmap3 = BitmapDescriptorFactory.fromResource(R.drawable.pin_smartliving);
            bitmap4 = BitmapDescriptorFactory.fromResource(R.drawable.pin_hkt);
            bitmap5 = BitmapDescriptorFactory.fromResource(R.drawable.pin_csl);
            bitmap6 = BitmapDescriptorFactory.fromResource(R.drawable.pin_1010);
            bitmap7 = BitmapDescriptorFactory.fromResource(R.drawable.pin_iot);

            UiSettings uisettings = mapView.getUiSettings();
            uisettings.setMyLocationButtonEnabled(false);

            drawMarker();
        }
    }

    private final void drawMarker() {
        if (shopRec != null) {
            try {
                setUpMapIfNeeded();
            } catch (Exception e) {
                // we cannot set up the Map, should terminate
                getActivity().finish();
            }
            mapView.clear();

            MarkerOptions markerOptions;
            Marker marker;
            markerLookup = new HashMap<Marker, ShopRec>();

            for (ShopRec shop : shopRec) {

                try {
                    markerOptions = new MarkerOptions();
                    markerOptions.draggable(false);
                    markerOptions.position(new LatLng(shop.latitude, shop.longitude));
                    markerOptions.title((isZh) ? shop.name_zh : shop.name_en);
                    markerOptions.snippet((isZh) ? shop.addr_zh : shop.addr_en);
//					if (shop.shop_ty.equalsIgnoreCase(ShopRec.TY_SHOP)) {
//						markerOptions.icon(bitmap);
//					} else if (shop.shop_ty.equalsIgnoreCase(ShopRec.TY_SMARTLIVING)) {
//						markerOptions.icon(bitmap3);
//					} else {
//						markerOptions.icon(bitmap2);
//					}
                    //new icon by shop type
                    if ("S".equalsIgnoreCase(shop.shop_ty)) {
                        markerOptions.icon(bitmap4);
                    } else if ("D".equalsIgnoreCase(shop.shop_ty)) {
                        markerOptions.icon(bitmap5);
                    } else if ("1".equalsIgnoreCase(shop.shop_ty)) {
                        markerOptions.icon(bitmap6);
                    } else if ("L".equalsIgnoreCase(shop.shop_ty)) {
                        markerOptions.icon(bitmap3);
                    } else if ("I".equalsIgnoreCase(shop.shop_ty)) {
                        markerOptions.icon(bitmap7);
                    } else {
                        markerOptions.icon(bitmap2);
                    }
                    marker = mapView.addMarker(markerOptions);

                    markerLookup.put(marker, shop);
                } catch (Exception e) {
                    // igrore that POI
                }
            }
        }
    }

    public final void fetchShopList(ShopRec[] shoparr) {
        shopRec = shoparr;
        if (mapView != null) {
            drawMarker();
        }
    }

    // Listeners
    private final OnMyLocationChangeListener onMyLocationChangeListener = new OnMyLocationChangeListener() {

        @Override
        public final void onMyLocationChange(Location location) {
            if (location != null) {
                mMyLocation = location;
            }
            if (traceMyLoc && !runOnFirstFix) {
                mapView.animateCamera(CameraUpdateFactory.newLatLng(new LatLng(location.getLatitude(), location.getLongitude())));
                mapEventCallback.onFirstFix();
            }
            runOnFirstFix = true;
        }
    };

    private final OnMarkerClickListener onMarkerClickListener = new OnMarkerClickListener() {

        @Override
        public final boolean onMarkerClick(final Marker marker) {
            return !traceMyLoc;
        }
    };

    private final OnInfoWindowClickListener onInfoWindowClickListener = new OnInfoWindowClickListener() {
        @Override
        public final void onInfoWindowClick(Marker marker) {
            mapEventCallback.onShopDetail(markerLookup.get(marker));
        }
    };

    @SuppressLint("MissingPermission")
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mapView = googleMap;

        // Get a reference to the map/GoogleMap object
        try {
//			Log.i("Map Frag", "setup");
            setUpMapIfNeeded();
        } catch (Exception e) {
            // We have problems setting up the map, we cannot proceed
            e.printStackTrace();
            getActivity().finish();
        }

        if (traceMyLoc) {
            if (RuntimePermissionUtil.isLocationPermissionGranted(getActivity())) {
                mapView.setMyLocationEnabled(true);
            } else {
                if (ClnEnv.getPref(getActivity(), Constant.LOCATION_PERMISSION_DENIED, false)) {
                    DialogHelper.createSimpleDialog(thisActivity, Utils.getString(thisActivity, R.string.permission_denied_setting_enabled), Utils.getString(thisActivity, R.string.btn_ok));
                } else {
                    RuntimePermissionUtil.requestLocationPermission(getActivity(), Constant.REQUEST_LOCATION_PERMISSION_ENABLED);
                }
            }
        }

        loadMapPins();
        if (mPOILatLng != null) {
            mapView.animateCamera(CameraUpdateFactory.newLatLngZoom(mPOILatLng, 17));
        }

    }

    // Container Activity must implement this interface
    public interface OnMapEventListener {
        void onFirstFix();
        void onShopDetail(ShopRec shop);
    }

    class ShopInfoWindowAdapter implements InfoWindowAdapter {
        private final View mWindow;
        private final View mContents;

        @SuppressLint("RestrictedApi")
        ShopInfoWindowAdapter() {
            mWindow = getLayoutInflater(getArguments()).inflate(R.layout.balloon_overlay, null);
            mContents = getLayoutInflater(getArguments()).inflate(R.layout.balloon_overlay, null);
        }

        @Override
        public final View getInfoWindow(Marker marker) {
            render(marker, mWindow);
            return mWindow;
        }

        @Override
        public final View getInfoContents(Marker marker) {
            render(marker, mContents);
            return mContents;
        }

        private final void render(Marker marker, View view) {
            String title = marker.getTitle();
            TextView titleUi = view.findViewById(R.id.balloon_shop_title);
            TextView addressUi = view.findViewById(R.id.balloon_shop_address);
            titleUi.setText(title);
            addressUi.setText(marker.getSnippet());
        }
    }

    @SuppressLint("MissingPermission")
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == Constant.REQUEST_LOCATION_PERMISSION_ENABLED || requestCode == Constant.REQUEST_LOCATION_PERMISSION_DISABLED) {
            //Permission is granted
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                switch (requestCode) {
                    case Constant.REQUEST_LOCATION_PERMISSION_ENABLED:
                        mapView.setMyLocationEnabled(true);
                        break;
                    case Constant.REQUEST_LOCATION_PERMISSION_DISABLED:
                        mapView.setMyLocationEnabled(false);
                        break;
                }
            } else {

                DialogHelper.createSimpleDialog(thisActivity, Utils.getString(thisActivity, R.string.permission_denied), Utils.getString(thisActivity, R.string.btn_ok));

                //This will return false if the user tick the Don't ask again checkbox
                if (!RuntimePermissionUtil.shouldShowLocationRequestPermission(thisActivity)) {
                    ClnEnv.setPref(thisActivity, Constant.LOCATION_PERMISSION_DENIED, true);
                }
            }
        }
    }

    private LocationListener mLocationListener = new LocationListener() {
        @Override
        public void onLocationChanged(Location location) {
            if (location != null) {
                if (traceMyLoc && !runOnFirstFix) {
                    mMyLocation = location;
                    mapView.animateCamera(CameraUpdateFactory.newLatLng(new LatLng(location.getLatitude(), location.getLongitude())));
                    mapEventCallback.onFirstFix();
                }
                runOnFirstFix = true;

                mLocationManager.removeUpdates(mLocationListener);
            }


        }

        @Override
        public void onStatusChanged(String s, int i, Bundle bundle) {

        }

        @Override
        public void onProviderEnabled(String s) {

        }

        @Override
        public void onProviderDisabled(String s) {

        }
    };
}