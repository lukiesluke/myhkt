package com.pccw.myhkt.cell.model;

import android.view.View.OnClickListener;

import com.pccw.biz.myhkt.R;

public class DetailBtnCell extends Cell{

	private OnClickListener onRightClickListener = null;
	private OnClickListener onLeftClickListener = null;
	
	public DetailBtnCell(String title, String content, int draw, String[] clickArray){
		super();
		//Default setting
		type = Cell.DETAIL_BTN;
		titleColorId = R.color.black;
		contentColorId = R.color.hkt_textcolor;
		titleSizeDelta = 0;
		contentSizeDelta = -2;

		this.title = title;
		this.content = content;
		this.draw = draw;
		this.clickArray = clickArray;
	}
	public OnClickListener getRightOnClickListener() {
		return onRightClickListener;
	}
	public void setOnRightClickListener(OnClickListener onRightClickListener) {
		this.onRightClickListener = onRightClickListener;
	}
	public OnClickListener getLeftOnClickListener() {
		return onLeftClickListener;
	}
	public void setOnLeftClickListener(OnClickListener onLeftClickListener) {
		this.onLeftClickListener = onLeftClickListener;
	}
}