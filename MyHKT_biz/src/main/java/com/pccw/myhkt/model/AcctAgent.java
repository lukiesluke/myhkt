package com.pccw.myhkt.model;

import java.io.Serializable;

import com.pccw.dango.shared.entity.SubnRec;


public final class AcctAgent implements Serializable{
	private static final long serialVersionUID = -1838549375878228377L;
	
	//MyAccount Key
	private int lobType;
	private String	lob;
	private String	srvNum;
	private String	cusNum;
	private String	acctNum;
	private String	srvId;
	private String	sysTy;
	private String	alias;
	private boolean	live;
	private boolean 	assoc;
	private int		subnRecX;
	private int		acctX;
	private int		ltsType;	// CR2013011
	private SubnRec	subnRec;
	
	//MyMobile key
	private String	password;
	private String	cardType;
	private boolean	isHeader;
	private boolean isMyMob = false;
	private String latest_bill;
	private boolean isSms = false;
	private String 	acctType;	//account type eg:Master "M", Slave "S", Individual "I", Large "L"

	
	public int getLobType() {
		return lobType;
	}
	public void setLobType(int lobType) {
		this.lobType = lobType;
	}
	public String getLob() {
		return lob;
	}
	public void setLob(String lob) {
		this.lob = lob;
	}
	public String getSrvNum() {
		return srvNum;
	}
	public void setSrvNum(String srvNum) {
		this.srvNum = srvNum;
	}
	public String getCusNum() {
		return cusNum;
	}
	public void setCusNum(String cusNum) {
		this.cusNum = cusNum;
	}
	public String getAcctNum() {
		return acctNum;
	}
	public void setAcctNum(String acctNum) {
		this.acctNum = acctNum;
	}
	public String getSrvId() {
		return srvId;
	}
	public void setSrvId(String srvId) {
		this.srvId = srvId;
	}
	public String getSysTy() {
		return sysTy;
	}
	public void setSysTy(String sysTy) {
		this.sysTy = sysTy;
	}
	public String getAlias() {
		return alias;
	}
	public void setAlias(String alias) {
		this.alias = alias;
	}
	public boolean isLive() {
		return live;
	}
	public void setLive(boolean live) {
		this.live = live;
	}
	public int getSubnRecX() {
		return subnRecX;
	}
	public void setSubnRecX(int subnRecX) {
		this.subnRecX = subnRecX;
	}
	public int getAcctX() {
		return acctX;
	}
	public void setAcctX(int acctX) {
		this.acctX = acctX;
	}
	public int getLtsType() {
		return ltsType;
	}
	public void setLtsType(int ltsType) {
		this.ltsType = ltsType;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
	public boolean isAssoc() {
		return assoc;
	}
	public void setAssoc(boolean assoc) {
		this.assoc = assoc;
	}
	
	public SubnRec getSubnRec() {
		return subnRec;
	}
	public void setSubnRec(SubnRec subnRec) {
		this.subnRec = subnRec;
	}
//	public SubscriptionRec getIoSubscriptionRec() {
//		return ioSubscriptionRec;
//	}
//	public void setIoSubscriptionRec(SubscriptionRec ioSubscriptionRec) {
//		this.ioSubscriptionRec = ioSubscriptionRec;
//	}
	

	
	public String getCardType() {
		return cardType;
	}
	public void setCardType(String cardType) {
		this.cardType = cardType;
	}
	public boolean isHeader() {
		return isHeader;
	}
	public void setHeader(boolean isHeader) {
		this.isHeader = isHeader;
	}
	public boolean isMyMob() {
		return isMyMob;
	}
	public void setMyMob(boolean isMyMob) {
		this.isMyMob = isMyMob;
	}
	public String getLatest_bill() {
		return latest_bill;
	}
	public void setLatest_bill(String latest_bill) {
		this.latest_bill = latest_bill;
	}
	public boolean isSms() {
		return isSms;
	}
	public void setSms(boolean isSms) {
		this.isSms = isSms;
	}
	public String getAcctType() {
		return acctType;
	}
	public void setAcctType(String acctType) {
		this.acctType = acctType;
	}
	
	public AcctAgent copyMe() {
		AcctAgent rDes;

		rDes = new AcctAgent();
		rDes.copyFrom(this);

		return (rDes);
	}
	
	public AcctAgent copyFrom(AcctAgent rSrc) {
		lob = rSrc.lob;
		srvNum = rSrc.srvNum;
		cusNum = rSrc.cusNum;
		acctNum = rSrc.acctNum;
		srvId = rSrc.srvId;
		sysTy = rSrc.sysTy;
		alias = rSrc.alias;
		live = rSrc.live;
		subnRecX = rSrc.subnRecX;
		acctX = rSrc.acctX;
		ltsType = rSrc.ltsType;
		subnRec = rSrc.subnRec;
		lobType = rSrc.lobType;
		assoc = rSrc.assoc;
		live = rSrc.live;
		cardType = rSrc.cardType;
		isHeader = rSrc.isHeader;
		isMyMob = rSrc.isMyMob;
		latest_bill = rSrc.latest_bill;
		isSms = rSrc.isSms;
		acctType = rSrc.acctType;
		
		password = rSrc.password;

//		ioSubscriptionRec = rSrc.ioSubscriptionRec;

		return (this);
	}
}
