/*
    Crate for MyMobile (Add-On) Authentication
    
    Keywords
    --------
    $URL: $
    $Rev: $
    $Date: $
    $Author: $
*/


package com.pccw.dango.shared.cra;

import java.io.Serializable;

import com.pccw.dango.shared.entity.SubnRec;
import com.pccw.dango.shared.g3entity.G3GetSubGroupCodeResultDTO;


public class AddOnCra extends BaseCraEx implements Serializable
{
    private static final long serialVersionUID = -5936056215894069380L;
    
    private String                  iLoginId;           /* Login ID                                      */
    private SubnRec                 iSubnRec;           /* Subscription Record                           */
    private boolean                 iSms;               /* SMS                                           */
    
    private SubnRec                 oSubnRec;           /* Subscription Record                           */
    private boolean                 oIndivAcctFlg;      /* Individual Account Flag                       */
    private G3GetSubGroupCodeResultDTO 
                                    oG3GetSubGroupCodeResultDTO;
                                                        /* Sub Group Code                                */

    
    public static void main(String rArg[])
    {
        System.out.println(getVer());
        return;
    }
    
    
    public static String getVer()
    {
        return ("$URL: $, $Rev: $");
    }

    
    public AddOnCra()
    {
        initAndClear();
    }
    
    
    protected void init()
    {
        super.init();
    }
    
    
    public void clear()
    {
        super.clear();

        clearILoginId();
        clearISubnRec();
        clearISms();
        clearOSubnRec();
        clearOIndivAcctFlg();
        clearOG3GetSubGroupCodeResultDTO();
    }
    

    public AddOnCra copyFrom(AddOnCra rSrc)
    {
        super.copyFrom(rSrc);
        
        setILoginId(rSrc.getILoginId());
        setISubnRec(rSrc.getISubnRec());
        setISms(rSrc.getISms());
        setOSubnRec(rSrc.getOSubnRec());
        setOIndivAcctFlg(rSrc.getOIndivAcctFlg());
        setOG3GetSubGroupCodeResultDTO(rSrc.getOG3GetSubGroupCodeResultDTO());

        return (this);
    }
    
    
    public AddOnCra copyTo(AddOnCra rDes)
    {
        rDes.copyFrom(this);
        return (rDes);
    }
    
    
    public AddOnCra copyMe()
    {
        AddOnCra                      rDes;
        
        rDes = new AddOnCra();
        rDes.copyFrom(this);
        return (rDes);
    }

    
    public void clearILoginId()
    {
        setILoginId("");
    }


    public void setILoginId(String rLoginId) 
    {
        iLoginId = rLoginId;
    }
    
    
    public String getILoginId() 
    {
        return iLoginId;
    }

    
    public void clearISubnRec()
    {
        iSubnRec = new SubnRec();
    }


    public void setISubnRec(SubnRec rArg)
    {
        iSubnRec = rArg;
    }


    public SubnRec getISubnRec()
    {
        return (iSubnRec);
    }
    
    
    public void clearISms()
    {
        setISms(false);
    }
    
    
    public void setISms(boolean rISms)
    {
        iSms = rISms;
    }
    
    
    public boolean getISms()
    {
        return (iSms);
    }
    
    
    public void clearOSubnRec()
    {
        oSubnRec = new SubnRec();
    }


    public void setOSubnRec(SubnRec rArg)
    {
        oSubnRec = rArg;
    }


    public SubnRec getOSubnRec()
    {
        return (oSubnRec);
    }
    
    
    public void clearOIndivAcctFlg()
    {
        oIndivAcctFlg = false;
    }
    

    public void setOIndivAcctFlg(boolean rArg) 
    {
        oIndivAcctFlg = rArg;
    }

    
    public boolean getOIndivAcctFlg() 
    {
        return oIndivAcctFlg;
    }

    
    public void clearOG3GetSubGroupCodeResultDTO()
    {
        oG3GetSubGroupCodeResultDTO = new G3GetSubGroupCodeResultDTO();
    }


    public void setOG3GetSubGroupCodeResultDTO(G3GetSubGroupCodeResultDTO rArg)
    {
        oG3GetSubGroupCodeResultDTO = rArg;
    }


    public G3GetSubGroupCodeResultDTO getOG3GetSubGroupCodeResultDTO()
    {
        return (oG3GetSubGroupCodeResultDTO);
    }
}
