package com.pccw.myhkt.fragment;

import static com.pccw.myhkt.Utils.setCharLimit;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.pccw.dango.shared.cra.BiifCra;
import com.pccw.dango.shared.entity.SubnRec;
import com.pccw.dango.shared.tool.MyTool;
import com.pccw.dango.shared.tool.RC;
import com.pccw.myhkt.APIsManager;
import com.pccw.myhkt.APIsResponse;
import com.pccw.myhkt.ClnEnv;
import com.pccw.myhkt.DialogHelper;
import com.pccw.myhkt.FAWrapper;
import com.pccw.myhkt.InterpretRCManager;
import com.pccw.myhkt.R;
import com.pccw.myhkt.Utils;
import com.pccw.myhkt.activity.BaseActivity;
import com.pccw.myhkt.lib.ui.AAQuery;
import com.pccw.myhkt.lib.ui.PopOverInputView;
import com.pccw.myhkt.lib.ui.popover.ActionItem;
import com.pccw.myhkt.lib.ui.popover.QuickAction;
import com.pccw.wheat.shared.tool.Reply;

import java.util.Objects;

public class BillInfoLTSChild1Fragment extends BaseServiceFragment {
    private BillInfoLTSChild1Fragment me;
    private View myView;
    private AAQuery aq;
    private BiifCra biifCra;
    private QuickAction langQuickAction;
    private QuickAction mediaQuickAction;
    private PopOverInputView langPopover;
    private PopOverInputView mediaPopover;
    private int screenWidth;

    @Override
    public void onAttach(@NonNull Activity activity) {
        super.onAttach(activity);
        try {
            callback_main = (OnServiceListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity + " must implement OnServiceListener");
        }

        try {
            callback_livechat = (OnLiveChatListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity + " must implement OnLiveChatListener");
        }
        if (callback_main == null) {
            if (debug) Log.i(TAG, "1callback null");
        }
    }

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        me = this;
        View fragmentLayout = inflater.inflate(R.layout.fragment_billinfolts_child1, container, false);
        myView = fragmentLayout;
        initData();
        return fragmentLayout;
    }

    @Override
    public void onResume() {
        // Screen Tracker
        super.onResume();
        FAWrapper.getInstance().sendFAScreen(getActivity(), R.string.CONST_SCRN_LTSBILLINFO, false);
    }

    protected void initUI() {
        // Screen Tracker
        FAWrapper.getInstance().sendFAScreen(getActivity(), R.string.CONST_SCRN_LTSBILLINFO, false);

        aq = new AAQuery(myView);
        //Header
        aq.id(R.id.billinfo_layout).backgroundColorId(R.color.white);
        int imageRes = Utils.theme(R.drawable.billinfo_icon, callback_main.getLob());
        int rImageRes = R.drawable.ios7_question;
        aq.id(R.id.billinfo_header).getTextView().setCompoundDrawablesWithIntrinsicBounds(imageRes, 0, rImageRes, 0);
        aq.id(R.id.billinfo_header).clicked(this, "onClick");

        aq.padding(R.id.billinfo_header_layout, 0, basePadding, 0, basePadding);

        setLangPopOver();
        setMediaPopOver();

        int charLimit = setCharLimit(callback_main.getLob());
        aq.id(R.id.billinfo_opt2_custname).text(getResString(R.string.BUPLTF_BI_NAME));
        aq.normEditText(R.id.billinfo_opt2_etxt_addr1, "", "", charLimit);
        aq.normEditText(R.id.billinfo_opt2_etxt_addr2, "", "", charLimit);
        aq.normEditText(R.id.billinfo_opt2_etxt_addr3, "", "", charLimit);
        aq.normEditText(R.id.billinfo_opt2_etxt_addr4, "", "", charLimit);
        aq.normEditText(R.id.billinfo_opt2_etxt_addr5, "", "", charLimit);
        aq.normEditText(R.id.billinfo_opt2_etxt_addr6, "", "", charLimit);

        if (callback_main.getLob() == R.string.CONST_LOB_LTS) {
            aq.setCharLimit(R.id.billinfo_opt2_etxt_addr1, 40);
            aq.setCharLimit(R.id.billinfo_opt2_etxt_addr2, 32);
            aq.setCharLimit(R.id.billinfo_opt2_etxt_addr3, 32);
            aq.setCharLimit(R.id.billinfo_opt2_etxt_addr4, 32);
            aq.setCharLimit(R.id.billinfo_opt2_etxt_addr5, 32);
            aq.setCharLimit(R.id.billinfo_opt2_etxt_addr6, 32);
        }

        //disable all editing ui until data is fully loaded
        aq.normEditText(R.id.billinfo_etxt_email, "", "");
        aq.normEditText(R.id.billinfo_etxt_sms, "", "");
        aq.id(R.id.billinfo_popover_lang).enabled(false);
        aq.id(R.id.billinfo_popover_billmedia).enabled(false);
        assert this.getParentFragment() != null;
        ((BillInfoLTSFragment) this.getParentFragment()).hideRightBtn(R.string.CONST_SELECTEDVIEW_BILLINFOLTS_CHILD1);
        disableBillAddUI();
        disableEmailAddUI(true);
        disableSMSUI(true);
    }

    public void setLangPopOver() {
        //LANGUAGE quickaction
        langQuickAction = new QuickAction(getActivity(), screenWidth / 2, 0);
        langPopover = aq.popOverInputView(R.id.billinfo_popover_lang, Utils.getString(me.requireActivity(), R.string.BUPLTF_BILANGEN));
        //Lang Items
        String[] langItems;
        langItems = new String[]{Utils.getString(me.requireActivity(), R.string.BUPLTF_BILANGEN), Utils.getString(me.requireActivity(), R.string.BUPLTF_BILANGZH), Utils.getString(me.requireActivity(), R.string.BUPLTF_BILANGBI)};
        ActionItem[] langItem = new ActionItem[langItems.length];
        for (int i = 0; i < langItems.length; i++) {
            langItem[i] = new ActionItem(i, langItems[i]);
            langQuickAction.addActionItem(langItem[i]);
        }
        langQuickAction.setOnActionItemClickListener((quickAction, pos, actionId) -> {
            ActionItem actionItem = quickAction.getActionItem(pos);
            //						aq.id(R.id.popover_input_layouttxt).text(actionItem.getTitle());
            langPopover.setText(actionItem.getTitle());
        });
        aq.id(R.id.billinfo_popover_lang).clicked(this, "onClick");
    }

    public void setMediaPopOver() {
        //Bill Media quickaction
        mediaQuickAction = new QuickAction(getActivity(), screenWidth / 2, 0);
        mediaPopover = aq.popOverInputView(R.id.billinfo_popover_billmedia, Utils.getString(me.requireActivity(), R.string.BUPLTF_MEDIA_P));

        //Bill Media Items
        String[] mediaItems;
        mediaItems = new String[]{Utils.getString(me.requireActivity(), R.string.BUPLTF_MEDIA_P), Utils.getString(me.requireActivity(), R.string.BUPLTF_MEDIA_E)};

        ActionItem[] mediaItem = new ActionItem[mediaItems.length];
        for (int i = 0; i < mediaItems.length; i++) {
            mediaItem[i] = new ActionItem(i, mediaItems[i]);
            mediaQuickAction.addActionItem(mediaItem[i]);
        }
        mediaQuickAction.setOnActionItemClickListener((quickAction, pos, actionId) -> {
            ActionItem actionItem = quickAction.getActionItem(pos);
            mediaPopover.setText(actionItem.getTitle());

            switch (pos) {
                case 0:
                    changeToPaperAlertDialog();
                    aq.id(R.id.billinfo_etxt_email).text("");
                    aq.id(R.id.billinfo_etxt_sms).text("");
                    disableSMSUI(true);
                    disableEmailAddUI(true);
                    break;
                case 1:
                    enableSMSUI();
                    enableEmailAddrUI();
                    if (biifCra != null && biifCra.getOBiif4Hkt() != null && biifCra.getOBiif4Hkt().getBillMedia() != null && biifCra.getOBiif4Hkt().getBillMedia().equalsIgnoreCase(BiifCra.BILL_MEDIA_S)) {
                        aq.id(R.id.billinfo_etxt_email).text(biifCra.getOBiif4Hkt().getBillEmail());
                        aq.id(R.id.billinfo_etxt_sms).text(biifCra.getOBiif4Hkt().getSmsNtfn());
                    } else {
                        aq.id(R.id.billinfo_etxt_email).text(Objects.requireNonNull(ClnEnv.getQualSvee()).getSveeRec().ctMail);
                        aq.id(R.id.billinfo_etxt_sms).text(ClnEnv.getQualSvee().getSveeRec().ctMob);
                    }
                    break;
            }
        });
        aq.id(R.id.billinfo_popover_billmedia).clicked(this, "onClick");
    }

    @SuppressLint("NonConstantResourceId")
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.billinfo_header:
                displayDialog(Utils.getString(me.requireActivity(), R.string.myhkt_BUPLTF_FB_BI_ADR));
                break;
            case R.id.billinfo_popover_lang:
                langQuickAction.show(v, aq.id(R.id.billinfo_layout).getView());
                langQuickAction.setAnimStyle(QuickAction.ANIM_GROW_FROM_RIGHT);
                break;
            case R.id.billinfo_popover_billmedia:
                mediaQuickAction.show(v, aq.id(R.id.billinfo_layout).getView());
                mediaQuickAction.setAnimStyle(QuickAction.ANIM_GROW_FROM_RIGHT);
                break;
        }
    }

    protected final void refreshData() {
        if (callback_main.getActiveSubview() == R.string.CONST_SELECTEDVIEW_BILLSUMMARY) {
            assert requireParentFragment().getParentFragment() != null;
            if (!((BillFragment) getParentFragment().getParentFragment()).isBillSum) {
                if (((BillInfoLTSFragment) getParentFragment()).activeSubView == R.string.CONST_SELECTEDVIEW_BILLINFOLTS_CHILD1) {
                    super.refreshData();
                    doGetBillInfo();
                }
            }
        }
    }

    public final void refresh() {
        super.refresh();

        doGetBillInfo();
    }

    private void doGetBillInfo() {
        BiifCra biifCra = new BiifCra();
        biifCra.setILoginId(Objects.requireNonNull(ClnEnv.getQualSvee()).getSveeRec().loginId);
        biifCra.setISubnRec(callback_main.getSubnRec().copyMe());
        APIsManager.doGetBillInfo(me, biifCra);
    }

    public final void initBillInfo() {
        aq.id(R.id.billinfo_opt2_custname_txt).text(biifCra.getOBiif4Hkt().getBillNm());
        callback_livechat.setModuleId(getResString(R.string.MODULE_LTS_BILL));
        aq.id(R.id.billinfo_opt2_etxt_addr1).text(biifCra.getOBiif4Hkt().getBillAdr1().replace("\n", "").replace("\r", ""));
        aq.id(R.id.billinfo_opt2_etxt_addr2).text(biifCra.getOBiif4Hkt().getBillAdr2().replace("\n", "").replace("\r", ""));
        aq.id(R.id.billinfo_opt2_etxt_addr3).text(biifCra.getOBiif4Hkt().getBillAdr3().replace("\n", "").replace("\r", ""));
        aq.id(R.id.billinfo_opt2_etxt_addr4).text(biifCra.getOBiif4Hkt().getBillAdr4().replace("\n", "").replace("\r", ""));
        aq.id(R.id.billinfo_opt2_etxt_addr5).text(biifCra.getOBiif4Hkt().getBillAdr5().replace("\n", "").replace("\r", ""));
        aq.id(R.id.billinfo_opt2_etxt_addr6).text(biifCra.getOBiif4Hkt().getBillAdr6().replace("\n", "").replace("\r", ""));

        //bill language
        if (biifCra.getOBiif4Hkt().getBillLang().equalsIgnoreCase(BiifCra.BI_LANG_ZH)) {
            aq.popOverInputView(R.id.billinfo_popover_lang, Utils.getString(me.requireActivity(), R.string.BUPLTF_BILANGZH));
        } else if (biifCra.getOBiif4Hkt().getBillLang().equalsIgnoreCase(BiifCra.BI_LANG_EN)) {
            aq.popOverInputView(R.id.billinfo_popover_lang, Utils.getString(me.requireActivity(), R.string.BUPLTF_BILANGEN));
        } else {
            aq.popOverInputView(R.id.billinfo_popover_lang, Utils.getString(me.requireActivity(), R.string.BUPLTF_BILANGBI));
        }
        aq.id(R.id.billinfo_popover_lang).enabled(true);

        //disable all editing for zombie account
        if (callback_main.IsZombie() || callback_main.isMyMobAcct() || biifCra == null) {
            disableBillAddUI();
            disableEmailAddUI(false);
            disableSMSUI(false);
            aq.id(R.id.billinfo_popover_lang).enabled(false);
            aq.id(R.id.billinfo_popover_billmedia).enabled(false);
            assert getParentFragment() != null;
            ((BillInfoLTSFragment) getParentFragment()).hideRightBtn(R.string.CONST_SELECTEDVIEW_BILLINFOLTS_CHILD1);
        } else {
            if (biifCra.getOBiif4Hkt().getBillMedia().equalsIgnoreCase(BiifCra.BILL_MEDIA_S)) {
                mediaPopover.setText(Utils.getString(me.requireActivity(), R.string.BUPLTF_MEDIA_E));
                mediaPopover.setEnabled(false);
                TextView tvLayoutTxt = mediaPopover.findViewById(R.id.popover_input_layouttxt);
                tvLayoutTxt.setTextColor(requireContext().getResources().getColor(R.color.hkt_txtcolor_grey_disable));
                tvLayoutTxt.setCompoundDrawables(null, null, null, null);
                aq.id(R.id.billinfo_etxt_email).text(biifCra.getOBiif4Hkt().getBillEmail());
                aq.id(R.id.billinfo_etxt_sms).text(biifCra.getOBiif4Hkt().getSmsNtfn());
                enableEmailAddrUI();
                enableSMSUI();
            } else {
                mediaPopover.setText(Utils.getString(me.requireActivity(), R.string.BUPLTF_MEDIA_P));
                mediaPopover.setEnabled(true);
                aq.id(R.id.billinfo_etxt_email).text("");
                aq.id(R.id.billinfo_etxt_sms).text("");
                disableEmailAddUI(true);
                disableSMSUI(true);
            }

            if (callback_main.getLtsType() == R.string.CONST_LTS_ICFS ||
                    callback_main.getLtsType() == R.string.CONST_LTS_ONECALL ||
                    callback_main.getLtsType() == R.string.CONST_LTS_IDD0060 ||
                    callback_main.getLtsType() == R.string.CONST_LTS_CALLINGCARD) {
                disableBillAddUI();
            } else {
                enableBillAddUI();
            }
            aq.id(R.id.billinfo_popover_lang).enabled(true);
            assert getParentFragment() != null;
            ((BillInfoLTSFragment) getParentFragment()).showLeftBtn(R.string.CONST_SELECTEDVIEW_BILLINFOLTS_CHILD1);
            ((BillInfoLTSFragment) getParentFragment()).showRightBtn(R.string.CONST_SELECTEDVIEW_BILLINFOLTS_CHILD1);
        }

    }

    public void disableBillAddUI() {
        aq.id(R.id.billinfo_opt2_etxt_addr1).enabled(false);
        aq.id(R.id.billinfo_opt2_etxt_addr2).enabled(false);
        aq.id(R.id.billinfo_opt2_etxt_addr3).enabled(false);
        aq.id(R.id.billinfo_opt2_etxt_addr4).enabled(false);
        aq.id(R.id.billinfo_opt2_etxt_addr5).enabled(false);
        aq.id(R.id.billinfo_opt2_etxt_addr6).enabled(false);
    }

    public void enableBillAddUI() {
        aq.id(R.id.billinfo_opt2_etxt_addr1).enabled(true);
        aq.id(R.id.billinfo_opt2_etxt_addr2).enabled(true);
        aq.id(R.id.billinfo_opt2_etxt_addr3).enabled(true);
        aq.id(R.id.billinfo_opt2_etxt_addr4).enabled(true);
        aq.id(R.id.billinfo_opt2_etxt_addr5).enabled(true);
        aq.id(R.id.billinfo_opt2_etxt_addr6).enabled(true);
    }

    public void enableEmailAddrUI() {
        aq.id(R.id.billinfo_email_layout).visibility(View.VISIBLE);
        aq.id(R.id.billinfo_etxt_email).enabled(true);
    }

    public void disableEmailAddressET() {
        aq.id(R.id.billinfo_etxt_email).enabled(false);
    }

    public void disableEmailAddUI(Boolean isHidden) {
        aq.id(R.id.billinfo_email_layout).visibility(isHidden ? View.GONE : View.VISIBLE);
        aq.id(R.id.billinfo_etxt_email).enabled(false);
    }

    public void enableSMSUI() {
        aq.id(R.id.billinfo_sms_layout).visibility(View.VISIBLE);
        aq.id(R.id.billinfo_etxt_sms).enabled(true);
    }

    public void disableSMSUI(Boolean isHidden) {
        aq.id(R.id.billinfo_sms_layout).visibility(isHidden ? View.GONE : View.VISIBLE);
        aq.id(R.id.billinfo_etxt_sms).enabled(false);
    }

    public final void updateBillInfo() {
        BiifCra biifCraUpd = biifCra.copyMe();
        biifCraUpd.setILoginId(Objects.requireNonNull(ClnEnv.getQualSvee()).getSveeRec().loginId);
        biifCraUpd.setIPyif4Lts(biifCraUpd.getOPyif4Lts().copyMe());
        biifCraUpd.setIBiif4Hkt(biifCraUpd.getOBiif4Hkt().copyMe());
        biifCraUpd.getIBiif4Hkt().setBillAdr1(aq.id(R.id.billinfo_opt2_etxt_addr1).getText().toString());
        biifCraUpd.getIBiif4Hkt().setBillAdr2(aq.id(R.id.billinfo_opt2_etxt_addr2).getText().toString());
        biifCraUpd.getIBiif4Hkt().setBillAdr3(aq.id(R.id.billinfo_opt2_etxt_addr3).getText().toString());
        biifCraUpd.getIBiif4Hkt().setBillAdr4(aq.id(R.id.billinfo_opt2_etxt_addr4).getText().toString());
        biifCraUpd.getIBiif4Hkt().setBillAdr5(aq.id(R.id.billinfo_opt2_etxt_addr5).getText().toString());
        biifCraUpd.getIBiif4Hkt().setBillAdr6(aq.id(R.id.billinfo_opt2_etxt_addr6).getText().toString());
        biifCraUpd.getIBiif4Hkt().setBillEmail(aq.id(R.id.billinfo_etxt_email).getText().toString());
        biifCraUpd.getIBiif4Hkt().setSmsNtfn(aq.id(R.id.billinfo_etxt_sms).getText().toString());
        biifCraUpd.getIBiif4Hkt().setBillLang(getSelectedLangCode());
        if (mediaPopover.getText().equals(getResString(R.string.BUPLTF_MEDIA_P))) {
            biifCraUpd.getIBiif4Hkt().setBillMedia(BiifCra.LTS_BILL_MEDIA_P);
        } else {
            biifCraUpd.getIBiif4Hkt().setBillMedia(BiifCra.BILL_MEDIA_S);
        }
        Reply reply = BiifCra.validAdrLen(SubnRec.LOB_LTS, biifCraUpd.getIBiif4Hkt().getBillAdr1(), biifCraUpd.getIBiif4Hkt().getBillAdr2(), biifCraUpd.getIBiif4Hkt().getBillAdr3(), biifCraUpd.getIBiif4Hkt().getBillAdr4(), biifCraUpd.getIBiif4Hkt().getBillAdr5(), biifCraUpd.getIBiif4Hkt().getBillAdr6());
        if (reply.isSucc())
            reply = BiifCra.validAdrChr(biifCraUpd.getIBiif4Hkt().getBillAdr1(), biifCraUpd.getIBiif4Hkt().getBillAdr2(), biifCraUpd.getIBiif4Hkt().getBillAdr3(), biifCraUpd.getIBiif4Hkt().getBillAdr4(), biifCraUpd.getIBiif4Hkt().getBillAdr5(), biifCraUpd.getIBiif4Hkt().getBillAdr6());
        if (reply.isSucc() && biifCraUpd.getIBiif4Hkt().getBillMedia().equalsIgnoreCase(BiifCra.BILL_MEDIA_S))
            reply = BiifCra.validEmailFmt(biifCraUpd.getIBiif4Hkt().getBillEmail());
        if (reply.isSucc() && biifCraUpd.getIBiif4Hkt().getBillMedia().equalsIgnoreCase(BiifCra.BILL_MEDIA_S) && !biifCraUpd.getIBiif4Hkt().getSmsNtfn().isEmpty()) {
            if (!MyTool.isVaMob(biifCraUpd.getIBiif4Hkt().getSmsNtfn())) {
                reply = new com.pccw.wheat.shared.tool.Reply(RC.BIIF_IV_SMSNTF);
            }
        }
        if ((biifCra.getOBiif4Hkt().getBillLang().equalsIgnoreCase(BiifCra.BI_LANG_EN) || biifCra.getOBiif4Hkt().getBillLang().equalsIgnoreCase(BiifCra.BI_LANG_ZH))
                && biifCraUpd.getIBiif4Hkt().getBillLang().equalsIgnoreCase(BiifCra.BI_LANG_BI)) {
            displayDialog(Utils.getString(me.requireActivity(), R.string.MYHKT_ERR_INVALID_BILINGUAL));
        } else if (!reply.isSucc()) {
            displayDialog(InterpretRCManager.interpretRC_BinqLtsMdu(getActivity(), reply.getCode()));
        } else {
            confirmUpdateDialog(biifCraUpd);
        }
    }

    private void confirmUpdateDialog(final BiifCra biifCraUpd) {
        DialogInterface.OnClickListener onPosClick = (dialog, which) -> {
            dialog.dismiss();
            APIsManager.doUpdBillinfo(me, biifCraUpd);
        };
        DialogInterface.OnClickListener onNegClick = (dialog, which) -> {
            dialog.dismiss();
            displayDialog(getResString(R.string.CFMM_DISCARD));
            initBillInfo();
        };
        DialogHelper.createTitleDialog(me.getActivity(), getString(R.string.BUPLTM_CFMHDR), getString(R.string.BUPLTM_CFMMSG),
                getString(R.string.CFMF_CFM), onPosClick,
                getString(R.string.CFMF_NOTCFM), onNegClick);
    }

    private void resetLayout() {

        aq.id(R.id.billinfo_opt2_etxt_addr1).text("");
        aq.id(R.id.billinfo_opt2_etxt_addr2).text("");
        aq.id(R.id.billinfo_opt2_etxt_addr3).text("");
        aq.id(R.id.billinfo_opt2_etxt_addr4).text("");
        aq.id(R.id.billinfo_opt2_etxt_addr5).text("");
        langPopover.setText("");

    }

    private String getSelectedLangCode() {
        if (langPopover.getText().equalsIgnoreCase(getResString(R.string.BUPLTF_BILANGZH))) {
            return BiifCra.BI_LANG_ZH;
        } else if (langPopover.getText().equalsIgnoreCase(getResString(R.string.BUPLTF_BILANGEN))) {
            return BiifCra.BI_LANG_EN;
        } else if (langPopover.getText().equalsIgnoreCase(getResString(R.string.BUPLTF_BILANGBI))) {
            return BiifCra.BI_LANG_BI;
        }
        return "";
    }

    // Simple UI Dialog
    protected final void changeToPaperAlertDialog() {
        if (BiifCra.WAIVE_PAPER_ACCT.equalsIgnoreCase(biifCra.getOBiif4Hkt().getWaivePbChrg()) || !BiifCra.BILL_MEDIA_S.equalsIgnoreCase(biifCra.getOBiif4Hkt().getBillMedia())) {
            return;
        }
        AlertDialog.Builder builder = new Builder(getActivity());
        builder.setMessage(Utils.getString(me.requireActivity(), R.string.BUPLTF_MEDIA_P_MSG));
        builder.setPositiveButton(me.getString(R.string.btn_ok), null);
        builder.create().show();
    }

    @Override
    public void onSuccess(APIsResponse response) {
        if (APIsManager.READ_BIIF.equals(response.getActionTy())) {
            biifCra = new BiifCra();
            biifCra = (BiifCra) response.getCra();
            resetLayout();
            initBillInfo();
        } else if (APIsManager.UPD_BIIF.equals(response.getActionTy())) {
            biifCra = new BiifCra();
            biifCra = (BiifCra) response.getCra();
            if (callback_main.getLob() == R.string.CONST_LOB_1010 || callback_main.getLob() == R.string.CONST_LOB_O2F || callback_main.getLob() == R.string.CONST_LOB_MOB || callback_main.getLob() == R.string.CONST_LOB_IOI) {
                DialogHelper.createSimpleDialog(getActivity(), requireActivity().getString(R.string.BUPMBM_UPD_DONE));
            } else if (callback_main.getLob() == R.string.CONST_LOB_LTS) {
                DialogHelper.createSimpleDialog(getActivity(), requireActivity().getString(R.string.BUPLTM_UPD_DONE));
            } else {
                DialogHelper.createSimpleDialog(getActivity(), requireActivity().getString(R.string.BUPIMM_UPD_DONE));
            }
            doGetBillInfo();
        }
    }

    @Override
    public void onFail(APIsResponse response) {
        //		 General Error Message
        if (!"".equals(response.getMessage()) && response.getMessage() != null) {
            DialogHelper.createSimpleDialog(getActivity(), response.getMessage());
        } else if (RC.TCSESS_MM.equalsIgnoreCase(response.getReply().getCode())) {
            BaseActivity.ivSessDialog();
        } else {
            DialogHelper.createSimpleDialog(getActivity(), ClnEnv.getRPCErrMsg(getActivity(), response.getReply().getCode()));
        }

        //Hide the update button when there is error for reading bill
        if (APIsManager.READ_BIIF.equals(response.getActionTy())) {
            biifCra = new BiifCra();
            biifCra = (BiifCra) response.getCra();
            resetLayout();
            initBillInfo();
            assert getParentFragment() != null;
            ((BillInfoLTSFragment) getParentFragment()).hideRightBtn(R.string.CONST_SELECTEDVIEW_BILLINFOLTS_CHILD1);
        } else if (APIsManager.UPD_BIIF.equals(response.getActionTy())) {
            doGetBillInfo();
        }
    }
}
