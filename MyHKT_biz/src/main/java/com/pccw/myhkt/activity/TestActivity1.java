package com.pccw.myhkt.activity;

import android.graphics.Point;
import android.os.Bundle;
import androidx.fragment.app.FragmentActivity;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Toast;

import com.pccw.biz.myhkt.R;
import com.pccw.myhkt.lib.ui.AAQuery;
import com.pccw.myhkt.lib.ui.LRTabButton;

public class TestActivity1 extends FragmentActivity {  
	private boolean 				debug = false;
	AAQuery aq ;
	private int 					extralinespace;
	private int 					gridlayoutPadding = 0;
	private int 					deviceWidth = 0;
	private int 					colWidth = 0;

	@Override  
	public void onCreate(Bundle savedInstanceState)  {  
		super.onCreate(savedInstanceState);  
		debug = getResources().getBoolean(R.bool.DEBUG);
		setContentView(R.layout.activity_test1);  

		Display display = getWindowManager().getDefaultDisplay();
		Point size = new Point();
		display.getSize(size);
		deviceWidth = size.x;
		gridlayoutPadding = (int) getResources().getDimension(R.dimen.mainmenu_gridlayout_padding);
		extralinespace = (int) getResources().getDimension(R.dimen.extralinespace);
		initUI();
		if (debug) Log.i("TestActivity", colWidth+"");        
	}  

	public void initUI(){
		aq = new AAQuery(this);
		LRTabButton btn = (LRTabButton)aq.id(R.id.dq_lrtabbtn).getView();
		btn.initViews(this, "Company", "Person", R.drawable.contact_daycontact, R.drawable.contact_nightcontact);
		btn.setOnLeftClickLisener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				Toast.makeText(v.getContext(), "Testing", Toast.LENGTH_LONG).show();
			}		
			
		});
		
		btn.setOnRightClickLisener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				Toast.makeText(v.getContext(), "Testing1", Toast.LENGTH_LONG).show();
			}		
			
		});
	
	}
}  