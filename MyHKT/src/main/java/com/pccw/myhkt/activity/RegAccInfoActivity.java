package com.pccw.myhkt.activity;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.graphics.Paint;
import android.graphics.Point;
import android.os.Bundle;
import android.text.InputType;
import android.view.Display;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.CompoundButton;
import android.widget.LinearLayout;

import com.pccw.dango.shared.cra.AcMainCra;
import com.pccw.dango.shared.cra.LgiCra;
import com.pccw.dango.shared.entity.CustRec;
import com.pccw.dango.shared.entity.SubnRec;
import com.pccw.dango.shared.entity.SveeRec;
import com.pccw.dango.shared.tool.DirNum;
import com.pccw.dango.shared.tool.RC;
import com.pccw.myhkt.APIsManager;
import com.pccw.myhkt.APIsResponse;
import com.pccw.myhkt.ClnEnv;
import com.pccw.myhkt.DialogHelper;
import com.pccw.myhkt.FAWrapper;
import com.pccw.myhkt.InterpretRCManager;
import com.pccw.myhkt.R;
import com.pccw.myhkt.Utils;
import com.pccw.myhkt.lib.ui.AAQuery;
import com.pccw.myhkt.lib.ui.LRButton;
import com.pccw.myhkt.lib.ui.LRButton.OnLRButtonClickListener;
import com.pccw.myhkt.lib.ui.RegInputItem;
import com.pccw.wheat.shared.tool.Reply;

import java.util.Locale;

/************************************************************************
File       : RecAccInfoActivity.java
Desc       : A follow up registration page for users from other sources
Name       : RecAccInfoActivity
Created by : Derek Tsui
Date       : 15/03/2016

Change History:
Date       Modified By			Description
---------- ----------------	-------------------------------
15/03/2016 Derek Tsui         	- First draft
29/05,2018 Moiz Esmail          - Removed PIC dialog
*************************************************************************/

public class RegAccInfoActivity extends BaseActivity {
	private AAQuery 		aq;
	
	private static LgiCra 	lgicra 			= null;
	private static boolean 	savePw 			= false;
	private static String	loginId			= "";
	private static String 	pwd				= "";
	private static String	errCode			= "";
	
	private boolean							isHKID				= true;
	private boolean							hasPassword			= false;   //has password section
	private boolean							firstSetLang		= true;
	private int								spinnerIndex		= 0;
	private boolean							hasCheckBox 		= false;
	
	private LRButton   	regaccinfo_doctype_lrbtn;
	private LRButton   	regaccinfo_lang_lrbtn;

	private RegInputItem regNewPwd;
	private RegInputItem regConfirmPwd;

	private static CustRec					regCustRec			= null;
	private static SveeRec					sveeRec 			= null;
	
	private String domainURL = APIsManager.PRD_FTP;
	
	public final static String BUNDLE_ACMAIN = "BUNDLE_ACMAIN";
	public final static String BUNDLE_ERRCODE = "BUNDLE_ERRCODE";
	public final static String BUNDLE_SAVEPW = "BUNDLE_SAVEPW";
	public final static String BUNDLE_LOGINID = "BUNDLE_LOGINID";
	public final static String BUNDLE_PWD = "BUNDLE_PWD";
	public final static String BUNDLE_NEWPWD = "BUNDLE_NEWPWD";
	public final static String BUNDLE_LOGINCRA = "BUNDLE_LOGINCRA";
	@Override
	public final void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);	
		initData();

		Bundle bundle = getIntent().getExtras();
		if (bundle != null) {
			savePw = bundle.getBoolean("SAVEPW");
			errCode = bundle.getString("ERRCODE", "");
			loginId = bundle.getString("LOGINID", "");
			pwd = bundle.getString("PWD");
			lgicra = (LgiCra) bundle.getSerializable("LOGINCRA");
		}
		
		domainURL = ClnEnv.getPref(me, me.getString(R.string.CONST_PREF_FTP), APIsManager.PRD_FTP);
		ClnEnv.updateUILocale(getBaseContext(),	ClnEnv.getAppLocale(getBaseContext()));

		try {
			// It is not necessary that RegCustRec is still available
		
			regCustRec = lgicra.getOQualSvee().getCustRec().copyMe();
			sveeRec = lgicra.getOQualSvee().getSveeRec().copyMe();
//			regCustRec = ClnEnv.getQualCust().getRegCustRec().copyMe();
		} catch (Exception e) {
			e.printStackTrace();
			finish();
			overridePendingTransition(R.anim.left_slide_in, R.anim.right_slide_out);
		}
		setContentView(R.layout.activity_regaccinfo);
	}
	
	// Restore the Bundle data when re-built Activity
	protected void onRestoreInstanceState(Bundle savedInstanceState) {
		super.onRestoreInstanceState(savedInstanceState);
		savePw = savedInstanceState.getBoolean("SAVEPW");
		errCode = savedInstanceState.getString("ERRCODE");
		loginId = savedInstanceState.getString("LOGINID");
		pwd = savedInstanceState.getString("PWD");
		lgicra = (LgiCra) savedInstanceState.getSerializable("LOGINCRA");
	}

	// Saved data before Destroy Activity
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putBoolean("SAVEPW", savePw);
		outState.putSerializable("ERRCODE", errCode);
		outState.putString("LOGINID", loginId);
		outState.putString("PWD", pwd);
		outState.putSerializable("LOGINCRA", lgicra);
	}

	private void initData(){
		Display display = getWindowManager().getDefaultDisplay();
		Point size = new Point();
		display.getSize(size);
		me = this;
	}

	@Override
	protected void onNewIntent(Intent intent) {
		super.onNewIntent(intent);
		ClnEnv.updateUILocale(getBaseContext(),	ClnEnv.getAppLocale(getBaseContext()));
	}

	@Override
	protected final void onStart() {
		initData();
		super.onStart();
		//Screen Tracker
		moduleId = getResString(R.string.MODULE_REGN1);
		initRegAccUI();
	}

	@Override
	public void onResume(){
		super.onResume();
		FAWrapper.getInstance().sendFAScreen(me, R.string.CONST_SCRN_REGACCINFO, false);
	}
	
	@Override
	protected final void onPause() {
		super.onPause();
	}

	@Override
	protected final void onStop() {
		super.onStop();
	}
	
	// Android Device Back Button Handling
	public final void onBackPressed() {
		finish();
		overridePendingTransition(R.anim.left_slide_in, R.anim.right_slide_out);
	}
	
	protected void initUI2() {
		aq = new AAQuery(this);
		//navbar
		//navbar style
		aq.navBarBaseLayout(R.id.navbar_base_layout);
		aq.navBarTitle(R.id.navbar_title, getResources().getString(R.string.SFRF_HDR_PNI));
		aq.navBarButton(R.id.navbar_button_left, R.drawable.btn_back);
		aq.navBarButton(R.id.navbar_button_right, R.drawable.livechat_small);
		aq.id(R.id.navbar_button_left).clicked(this, "onClick");
		
		aq.marginpx(R.id.regaccinfo_scrollview, basePadding, 0, basePadding, 0);
		
		aq.id(R.id.regaccinfo_loginid_desc).text(R.string.REGF_LOGIN_ID);
		aq.marginpx(R.id.regaccinfo_loginid_desc, 0, basePadding, 0, 0);
		
		aq.id(R.id.regaccinfo_loginid_txt).text(loginId);
		
		aq.id(R.id.regaccinfo_nickname_txt).text(R.string.PAMF_NICK_NAME);
		aq.marginpx(R.id.regaccinfo_nickname_txt, 0, basePadding, 0, 0);
		aq.normEditText(R.id.regaccinfo_nickname_etxt, "", getResources().getString(R.string.REGF_NICK_NAME_HINT));
		
		aq.id(R.id.regaccinfo_ITO_ATH).text(R.string.SFRF_ITO_ATH);
		
		aq.marginpx(R.id.regaccinfo_ITO_ATH, 0, basePadding, 0, basePadding);
		
		regaccinfo_doctype_lrbtn = (LRButton) aq.id(R.id.regaccinfo_doctype_lrbtn).getView();
		regaccinfo_doctype_lrbtn.initViews(this, getString(R.string.REGF_HKID), getString(R.string.REGF_PASSPORT));
		regaccinfo_doctype_lrbtn.setPadding(extraLineSpace, 0, extraLineSpace, 0);
		OnLRButtonClickListener onDocTypeClick = new OnLRButtonClickListener(){
			@Override
			public void onLRBtnLeftClick() {
				isHKID = true;
			}

			@Override
			public void onLRBtnRightClick() {
				isHKID = false;
			}			
		};
		regaccinfo_doctype_lrbtn.setCallback(onDocTypeClick);
		
//		aq.id(R.id.regaccinfo_docnum_txt).text(R.string.REGF_DOC_NUM);
//		aq.marginpx(R.id.regaccinfo_docnum_txt, 0, basePadding, 0, 0);
		aq.normEditText(R.id.regaccinfo_docnum_etxt, "", getResources().getString(R.string.REGF_FB_DOC_NUM));
		aq.id(R.id.regaccinfo_email_txt).text(R.string.REGF_CT_MAIL);
		aq.marginpx(R.id.regaccinfo_email_txt, 0, basePadding, 0, 0);
		aq.normEditText(R.id.regaccinfo_email_etxt, "", getResources().getString(R.string.myhkt_myprof_email_hint));
		aq.id(R.id.regaccinfo_mobno_txt).text(R.string.REGF_CT_MOB);
		aq.marginpx(R.id.regaccinfo_mobno_txt, 0, basePadding, 0, 0);
		aq.normEditText(R.id.regaccinfo_mobno_etxt, "", getResources().getString(R.string.myhkt_myprof_mobnum_hint));
		aq.id(R.id.regaccinfo_newpwd_txt).text(R.string.REGF_PWD);
		aq.marginpx(R.id.regaccinfo_newpwd_txt, 0, basePadding, 0, 0);

		regNewPwd = (RegInputItem) aq.id(R.id.regaccinfo_newpwd_etxt).getView();
		regNewPwd.initViews(this, "", getString(R.string.REGF_PWD_HINT), "", InputType.TYPE_TEXT_VARIATION_PASSWORD);
		regNewPwd.setMaxLength(16);
		regConfirmPwd = (RegInputItem) aq.id(R.id.regaccinfo_retypepwd_etxt).getView();
		regConfirmPwd.initViews(this, "", getString(R.string.REGF_PWD_HINT), "", InputType.TYPE_TEXT_VARIATION_PASSWORD);
		regConfirmPwd.setMaxLength(16);

		aq.id(R.id.regaccinfo_retypepwd_txt).text(R.string.PAMF_CFM_PWD);
		aq.marginpx(R.id.regaccinfo_retypepwd_txt, 0, basePadding, 0, 0);

		aq.marginpx(R.id.regaccinfo_newpwd_etxt, 0, 0 ,0, 0);
		aq.marginpx(R.id.regaccinfo_retypepwd_etxt, 0, 0 ,0, 0);
		
		//Lang
		aq.marginpx(R.id.regaccinfo_lang_desc, 0, basePadding, 0, 0);
		regaccinfo_lang_lrbtn = (LRButton) aq.id(R.id.regaccinfo_lang_lrbtn).getView();
		regaccinfo_lang_lrbtn.initViews(this, getString(R.string.btn_chi), getString(R.string.btn_eng));
		regaccinfo_lang_lrbtn.setPadding(extraLineSpace, basePadding, extraLineSpace, 0);
		OnLRButtonClickListener onLangClick = new OnLRButtonClickListener(){
			@Override
			public void onLRBtnLeftClick() {
				isZh = true;
				ClnEnv.setPref(getApplicationContext(), getString(R.string.CONST_PREF_LOCALE), getString(R.string.CONST_LOCALE_ZH));
				ClnEnv.updateUILocale(getBaseContext(), ClnEnv.getAppLocale(getBaseContext()));	
				initData();
				refreshLangUI();
				setLiveChangeIcon(false);
			}

			@Override
			public void onLRBtnRightClick() {
				isZh = false;
				ClnEnv.setPref(getApplicationContext(), getString(R.string.CONST_PREF_LOCALE), getString(R.string.CONST_LOCALE_EN));
				ClnEnv.updateUILocale(getBaseContext(), ClnEnv.getAppLocale(getBaseContext()));	
				initData();
				refreshLangUI();
				setLiveChangeIcon(false);
			}			
		};
		regaccinfo_lang_lrbtn.setCallback(onLangClick);
		
		aq.marginpx(R.id.regaccinfo_pics_layout, 0, basePadding, 0, basePadding);
		aq.id(R.id.regaccinfo_pics_layout).visibility(View.GONE);
		aq.id(R.id.regaccinfo_pics_cb).getCheckBox().setButtonDrawable(R.drawable.checkbox);
		aq.id(R.id.regaccinfo_pics_txt).text(R.string.MYHKT_TNC_ACCEPT);
		aq.id(R.id.regaccinfo_pics_txt).getTextView().getPaint().setFlags(Paint.UNDERLINE_TEXT_FLAG);
		aq.id(R.id.regaccinfo_pics_txt).textSize(12f);
		aq.id(R.id.regaccinfo_pics_txt).clicked(this, "onClick");
		
		aq.normTxtBtn(R.id.regaccinfo_submit_btn, getResString(R.string.REGF_NEXT), LinearLayout.LayoutParams.MATCH_PARENT);
		aq.id(R.id.regaccinfo_submit_btn).margin(basePadding/2, basePadding/2, basePadding/2, basePadding/2);
		aq.id(R.id.regaccinfo_submit_btn).clicked(this, "onClick");
	}
	
	private final void refreshLangUI() {
		aq = new AAQuery(this);
		//navbar
		//navbar style
		aq.navBarTitle(R.id.navbar_title, getResources().getString(R.string.SFRF_HDR_PNI));
				
		aq.id(R.id.regaccinfo_loginid_desc).text(R.string.REGF_LOGIN_ID);
				
		aq.id(R.id.regaccinfo_nickname_txt).text(R.string.PAMF_NICK_NAME);
		
		aq.id(R.id.regaccinfo_ITO_ATH).text(R.string.SFRF_ITO_ATH);
		
		
//		aq.id(R.id.regaccinfo_docnum_txt).text(R.string.REGF_DOC_NUM);
		aq.id(R.id.regaccinfo_email_txt).text(R.string.REGF_CT_MAIL);
		aq.id(R.id.regaccinfo_mobno_txt).text(R.string.REGF_CT_MOB);
		
		
		aq.id(R.id.regaccinfo_docnum_etxt).getEditText().setHint(R.string.REGF_FB_DOC_NUM);
		aq.id(R.id.regaccinfo_email_etxt).getEditText().setHint(R.string.myhkt_myprof_email_hint);
		aq.id(R.id.regaccinfo_mobno_etxt).getEditText().setHint(R.string.myhkt_myprof_mobnum_hint);
		//aq.id(R.id.regaccinfo_newpwd_etxt).getEditText().setHint(R.string.REGF_PWD_HINT);
		aq.id(R.id.regaccinfo_nickname_etxt).getEditText().setHint(R.string.REGF_NICK_NAME_HINT);
		regNewPwd = (RegInputItem) aq.id(R.id.regaccinfo_newpwd_etxt).getView();
		regNewPwd.setHint(getString(R.string.REGF_PWD_HINT));
		regConfirmPwd = (RegInputItem) aq.id(R.id.regaccinfo_retypepwd_etxt).getView();
		regConfirmPwd.setHint(getString(R.string.REGF_PWD_HINT));
		
//		regaccinfo_doctype_lrbtn = (LRButton) aq.id(R.id.regaccinfo_doctype_lrbtn).getView();
		regaccinfo_doctype_lrbtn.initViews(this, getString(R.string.REGF_HKID), getString(R.string.REGF_PASSPORT));
//		regaccinfo_doctype_lrbtn.setPadding(extralinespace, 0, extralinespace, 0);
//		OnLRButtonClickListener onDocTypeClick = new OnLRButtonClickListener(){
//			@Override
//			public void onLRBtnLeftClick() {
//			}
//
//			@Override
//			public void onLRBtnRightClick() {
//			}			
//		};
//		regaccinfo_doctype_lrbtn.setCallback(onDocTypeClick);
		
		aq.id(R.id.regaccinfo_newpwd_txt).text(R.string.REGF_PWD);
		aq.id(R.id.regaccinfo_retypepwd_txt).text(R.string.PAMF_CFM_PWD);
		
		aq.id(R.id.regaccinfo_pics_txt).text(R.string.MYHKT_TNC_ACCEPT);
		
		aq.normTxtBtn(R.id.regaccinfo_submit_btn, getResString(R.string.REGF_NEXT), LinearLayout.LayoutParams.MATCH_PARENT);
	}
	
	private final void initRegAccUI() {
		aq = new AAQuery(this);
		if (isZh) {
			regaccinfo_lang_lrbtn.setBtns(true);
		} else {
			regaccinfo_lang_lrbtn.setBtns(false);
		}
		
		//init doctype button
		if (isHKID) {
			regaccinfo_doctype_lrbtn.setBtns(true);
		} else {
			regaccinfo_doctype_lrbtn.setBtns(false);
		}
		
		//init new password section
		hasCheckBox = false;
		setSubmitButton(true);
		if (errCode.equals(RC.FILL_SVEE) || errCode.equals(RC.FILL_SVEE4CLBL)) {
			aq.id(R.id.regaccinfo_pwd_layout).visibility(View.VISIBLE);
			hasPassword = true;
		} else if (errCode.equals(RC.FILL_SVEE4CLAV) || errCode.equals(RC.FILL_SVEE4CLUB) || errCode.equals(RC.FILL_SVEE_NOTNC) || errCode.equals(RC.FILL_SVEE4CLBL_NOTNC)){
			if ("Y".equalsIgnoreCase(lgicra.getOQualSvee().getSveeRec().pwdEff) && "V".equalsIgnoreCase(lgicra.getOQualSvee().getSveeRec().state)) {
				aq.id(R.id.regaccinfo_pwd_layout).visibility(View.GONE);
				hasPassword = false;
			} else {
				aq.id(R.id.regaccinfo_pwd_layout).visibility(View.VISIBLE);
				hasPassword = true;
			}
		}
		
		//please do not remove this comments
//		if (errCode.equals(RC.FILL_SVEE) || errCode.equals(RC.FILL_SVEE4CLBL)) {
//			aq.id(R.id.regaccinfo_pwd_layout).visibility(View.VISIBLE);
//			hasPassword = true;
//			aq.id(R.id.regaccinfo_pics_layout).visibility(View.VISIBLE);
//			
//			hasCheckBox = true;
//			setSubmitButton(false);
//		} else if (errCode.equals(RC.FILL_SVEE4CLAV) || errCode.equals(RC.FILL_SVEE4CLUB) || errCode.equals(RC.FILL_SVEE_NOTNC) || errCode.equals(RC.FILL_SVEE4CLBL_NOTNC)){
//			if ("Y".equalsIgnoreCase(lgicra.getOQualSvee().getSveeRec().pwdEff) && "V".equalsIgnoreCase(lgicra.getOQualSvee().getSveeRec().state)) {
//				aq.id(R.id.regaccinfo_pwd_layout).visibility(View.GONE);
//				hasPassword = false;
//				aq.id(R.id.regaccinfo_pics_layout).visibility(View.GONE);
//				hasCheckBox = false;
//				setSubmitButton(true);
//			} else {
//				aq.id(R.id.regaccinfo_pwd_layout).visibility(View.VISIBLE);
//				hasPassword = true;
//				aq.id(R.id.regaccinfo_pics_layout).visibility(View.GONE);
//				hasCheckBox = false;
//				setSubmitButton(true);
//			}
//		}
		
		// prefill nickname, lang, email and mobile
		if (lgicra != null && lgicra.getOQualSvee().getSveeRec() != null) {
			aq.id(R.id.regaccinfo_nickname_etxt).text(lgicra.getOQualSvee().getSveeRec().nickname);
			aq.id(R.id.regaccinfo_nickname_etxt).getEditText().clearFocus();
			aq.id(R.id.regaccinfo_email_etxt).text(lgicra.getOQualSvee().getSveeRec().ctMail);
			aq.id(R.id.regaccinfo_email_etxt).getEditText().clearFocus();
			aq.id(R.id.regaccinfo_mobno_etxt).text(lgicra.getOQualSvee().getSveeRec().ctMob);
			aq.id(R.id.regaccinfo_mobno_etxt).getEditText().clearFocus();
		}
	
	// Check set default Language once
	if (firstSetLang) {

		// Start: Fault 0004570 Propagate current app language settings
		isZh = !Utils.getString(me, R.string.myhkt_lang).equalsIgnoreCase("en");
		// End: Fault 0004570

		firstSetLang = false;
	}
	
	
//	if (errCode.equals(RC.FILL_SVEE) || errCode.equals(RC.FILL_SVEE4CLBL)) {
		//Checkbox function apply only in RC_FILL_RCUS / RC_FILL_SVEE4CLBL case
		if (hasCheckBox) {
			if (aq.id(R.id.regaccinfo_pics_cb).getCheckBox().isChecked()) {
				setSubmitButton(true);
			} else {
		    	setSubmitButton(false);
		 	}
		} else {
			setSubmitButton(true);
		}

		//submit button toggle on checkbox change
		aq.id(R.id.regaccinfo_pics_cb).getCheckBox().setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
		       @Override
		       public void onCheckedChanged(CompoundButton buttonView,boolean isChecked) {
					if (hasCheckBox) {
						if (aq.id(R.id.regaccinfo_pics_cb).getCheckBox().isChecked()) {
							setSubmitButton(true);
						} else {
					    	setSubmitButton(false);
					 	}
					} else {
						setSubmitButton(true);
					}
		       }
		});
//	}
	}
	
	public void onClick(View v) {
		aq = new AAQuery(this);
		InputMethodManager inputManager;
		switch (v.getId()) {
		case R.id.navbar_button_left:
			finish();
			overridePendingTransition(R.anim.left_slide_in, R.anim.right_slide_out);
			break;
//		case R.id.regaccinfo_nickname_txt:
//			DialogHelper.createSimpleDialog(this, getString(R.string.PAMF_FB_NICK_NAME));
//			break;
//		case R.id.regaccinfo_docnum_txt:
//			DialogHelper.createSimpleDialog(this, getString(R.string.REGF_FB_DOC_NUM));
//			break;
//		case R.id.regaccinfo_email_txt:
//			DialogHelper.createSimpleDialog(this, getString(R.string.REGF_FB_CT_MAIL));
//			break;
//		case R.id.regaccinfo_mobno_txt:
//			DialogHelper.createSimpleDialog(this, getString(R.string.REGF_FB_CT_MOB));
//			break;
//		case R.id.regaccinfo_newpwd_txt:
//			DialogHelper.createSimpleDialog(this, getString(R.string.PAMF_FB_NEW_PWD));
//			break;
		case R.id.regaccinfo_pics_txt:
			Intent intent = new Intent(getApplicationContext(), BrowserActivity.class);
			Bundle bundle = new Bundle();
			bundle.putString(BrowserActivity.INTENTMODULEID, moduleId);
			bundle.putString(BrowserActivity.INTENTLINK, isZh? domainURL + "/mba/html/tnc4preset_zh.htm" : domainURL + "/mba/html/tnc4preset_en.htm");				
			bundle.putString(BrowserActivity.INTENTTITLE, getResString(R.string.myhkt_confirm_ack3));
			intent.putExtras(bundle);
			intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
			startActivity(intent);
			overridePendingTransition(R.anim.right_slide_in, R.anim.left_slide_out);
			break;
		case R.id.regaccinfo_submit_btn:
			// hide the soft keyboard
			Utils.closeSoftKeyboard(this, v);		

			//TODO			
			SveeRec rSveeRec = sveeRec.copyMe();
			rSveeRec.nickname = aq.id(R.id.regaccinfo_nickname_etxt).getText().toString();
			rSveeRec.ctMail = aq.id(R.id.regaccinfo_email_etxt).getText().toString();
			rSveeRec.ctMob = aq.id(R.id.regaccinfo_mobno_etxt).getText().toString();
			if (isZh) {
				rSveeRec.lang = SveeRec.LG_ZH;
			} else {
				rSveeRec.lang = SveeRec.LG_EN;
			}
//			rRegCustRec.sec_qus = spinnerIndex + 1; // Due to verify started at 1
//			rRegCustRec.sec_ans = viewholder.regaccinfo_edit_answer.getText().toString();
//			
//			if (errCode.equals(RC.FILL_SVEE) || errCode.equals(RC.FILL_SVEE4CLBL) ||
//				!((errCode.equals(RC.FILL_SVEE4CLBL) || errCode.equals(RC.FILL_SVEE4CLUB) || errCode.equals(RC.FILL_SVEE_NOTNC) || errCode.equals(RC.FILL_SVEE4CLBL_NOTNC)) && 
//				"Y".equalsIgnoreCase(lgicra.getOQualSvee().getSveeRec().pwdEff) && 
//				"V".equalsIgnoreCase(lgicra.getOQualSvee().getSveeRec().state))) {
//				rSveeRec.pwd = aq.id(R.id.regaccinfo_newpwd_etxt).getText().toString();
//			}			
			
			if (errCode.equals(RC.FILL_SVEE) || errCode.equals(RC.FILL_SVEE4CLBL)) {
				rSveeRec.pwd = regNewPwd.getInput();
			} else if (errCode.equals(RC.FILL_SVEE4CLAV) || errCode.equals(RC.FILL_SVEE4CLUB) || errCode.equals(RC.FILL_SVEE_NOTNC) || errCode.equals(RC.FILL_SVEE4CLBL_NOTNC)){
				if ("Y".equalsIgnoreCase(lgicra.getOQualSvee().getSveeRec().pwdEff) && "V".equalsIgnoreCase(lgicra.getOQualSvee().getSveeRec().state)) {
					// nothing
				} else {
					rSveeRec.pwd = regNewPwd.getInput();
				}
			}
			
			//TODO
			CustRec rRegCustRec =  new CustRec();
//			CustomerRec customerRec = new CustomerRec();
			if (isHKID) {
				rRegCustRec.docTy = CustRec.TY_HKID;
			} else {
				rRegCustRec.docTy = CustRec.TY_PASSPORT;
			}
			rRegCustRec.docNum = aq.id(R.id.regaccinfo_docnum_etxt).getText().toString();
			
			// Start: Fault 0004568 - added doTouchUpDocNum() of the original CSP Logic
			rRegCustRec.docNum = rRegCustRec.docNum.trim().toUpperCase(Locale.US);
			aq.id(R.id.regaccinfo_docnum_etxt).text(rRegCustRec.docNum);
//			// End: Fault 0004568
			
			rSveeRec.trim();

			//For mobile prefix validation
			String mobPfx = ClnEnv.getPref(this, "mobPfx", getResString(R.string.mob_pfx));
			String ltsPfx = ClnEnv.getPref(this, "ltsPfx", "2,3,81,82,83");
			DirNum.getInstance(mobPfx, ltsPfx);

			Reply reply = rSveeRec.verifyNickName(false);
			if (reply.isSucc()) reply = rRegCustRec.verifyBaseInput();
			if (reply.isSucc()) reply = rSveeRec.verifyCtMail();
			if (reply.isSucc()) reply = rSveeRec.verifyCtMob(false);
			if (reply.isSucc()) reply = rSveeRec.verifyLang();
//			if (reply.isSucc()) reply = rSveeRec.verifySecAns();
			if (reply.isSucc() && hasPassword) {
				reply = rSveeRec.verifyPwd();
			}

			//TODO
			if (!reply.isSucc()) {
				DialogHelper.createSimpleDialog(me, InterpretRCManager.interpretRC_RegnMdu(me, reply)); //TODO
//			} else if (!aq.id(R.id.regaccinfo_newpwd_etxt).getText().toString().equals(aq.id(R.id.regaccinfo_retypepwd_etxt).getText().toString())) {
//				DialogHelper.createSimpleDialog(me, getString(R.string.PAMM_IVCFMPWD));
			} else if (!rSveeRec.pwd.equals(regConfirmPwd.getInput()) && hasPassword) {      // cfmPwd = Retype-password
//				// Display error message PAMM_IVCFMPWD
				DialogHelper.createSimpleDialog(me, getString(R.string.PAMM_IVCFMPWD));
			} else if (errCode.equals(RC.FILL_SVEE) || errCode.equals(RC.FILL_SVEE4CLBL)) {
				// append user info
				AcMainCra acMainCra = new AcMainCra();
				acMainCra.setICustRec(rRegCustRec);
				acMainCra.setISveeRec(rSveeRec);
				acMainCra.setISubnRec(new SubnRec());
				acMainCra.setISoGud(lgicra.getOSoGud());
				nextPageDialog(acMainCra);
//				APIsManager.doAppendUserInfo(me, acMainCra);
//				acMainCra.setI
//				me.progressDialogHelper.showProgressDialog(me, me.onstopHandler, false);
//				regnCra = new RegnCra();
//				regnCra.setCustomerRec(customerRec);
//				regnCra.setSubscriptionRec(new SubscriptionRec());
//				regnCra.setRegCustRec(rRegCustRec.copyMe());
//				
//				RegnGrq regnGrq = new RegnGrq();
//				regnGrq.actionTy = AppendUserInfoAsyncTask.actionTy;
//				regnGrq.sessTok = ClnEnv.getSessTok();
//				regnGrq.acptTnc = true;
//				regnGrq.regnCra = regnCra;
//				
//				appendUserInfoAsyncTask = new AppendUserInfoAsyncTask(me.getApplicationContext(), callbackHandler).execute(regnGrq);
			} else {
				//RC_FILL_RCUS4CLAV
				//RC_FILL_SVEE4CLUB
				// append user info
				AcMainCra acMainCra = new AcMainCra();
				acMainCra.setICustRec(rRegCustRec);
				acMainCra.setISveeRec(rSveeRec);
				acMainCra.setISubnRec(new SubnRec());
				acMainCra.setISoGud(lgicra.getOSoGud());
				nextPageDialog(acMainCra);
//				APIsManager.doAppendUserInfo(me, acMainCra);
				
//				me.progressDialogHelper.showProgressDialog(me, me.onstopHandler, false);
//				regnCra = new RegnCra();
//				regnCra.setCustomerRec(customerRec);
//				regnCra.setSubscriptionRec(new SubscriptionRec());
//				regnCra.setRegCustRec(rRegCustRec.copyMe());
//
//				RegnGrq regnGrq = new RegnGrq();
//				regnGrq.actionTy = AppendUserInfoAsyncTask.actionTy;
//				regnGrq.sessTok = ClnEnv.getSessTok();
//				regnGrq.regnCra = regnCra;
//				+
//				appendUserInfoAsyncTask = new AppendUserInfoAsyncTask(me.getApplicationContext(), callbackHandler).execute(regnGrq);
			}
			break;
		}
	}
	
	private void nextPageDialog(final AcMainCra acMainCra) {
		Intent intent;
		Bundle bundle = new Bundle();
		bundle.putSerializable(BUNDLE_ACMAIN, acMainCra);
		bundle.putString(BUNDLE_ERRCODE, errCode);
		bundle.putBoolean(BUNDLE_SAVEPW, savePw);
		bundle.putString(BUNDLE_LOGINID, loginId);
		bundle.putSerializable(BUNDLE_LOGINCRA, lgicra);
		bundle.putString(BUNDLE_PWD, pwd);
		bundle.putString(BUNDLE_NEWPWD, regNewPwd.getInput()/*regNewPwd.getInput()*/);
		intent = new Intent(getApplicationContext(), RegAccInfoConfirmActivity.class);
		intent.putExtras(bundle);
		startActivity(intent);
		overridePendingTransition(R.anim.right_slide_in, R.anim.left_slide_out);
	}
	
	private void setSubmitButton(boolean enabled) {
		if (enabled) {
			aq.id(R.id.regaccinfo_submit_btn).getButton().setEnabled(true);
			aq.id(R.id.regaccinfo_submit_btn).getButton().setAlpha(1.0f);
		} else {
			aq.id(R.id.regaccinfo_submit_btn).getButton().setEnabled(false);
			aq.id(R.id.regaccinfo_submit_btn).getButton().setAlpha(0.2f);
		}
	}

	public void onSuccess(APIsResponse response) {
		super.onSuccess(response);
		if (response != null) {
			if (APIsManager.ECLOSION.equals(response.getActionTy())) {
				AcMainCra mAcMainCra = (AcMainCra) response.getCra();
				//dialog redirect to mainmenu onclick
				ClnEnv.setPref(me.getApplicationContext(), me.getString(R.string.CONST_PREF_SAVELOGINID), true);
				ClnEnv.setPref(me.getApplicationContext(), me.getString(R.string.CONST_PREF_LOGINID), loginId);				
			}
			
			if (errCode.equals(RC.FILL_SVEE) || errCode.equals(RC.FILL_SVEE4CLBL)) {
				ClnEnv.setSessionLoginID(loginId);
				ClnEnv.setSessionPassword(regNewPwd.getInput());
			} else if(errCode.equals(RC.FILL_SVEE4CLAV) || errCode.equals(RC.FILL_SVEE4CLUB) || errCode.equals(RC.FILL_SVEE_NOTNC) || errCode.equals(RC.FILL_SVEE4CLBL_NOTNC)) {
				if ("Y".equalsIgnoreCase(lgicra.getOQualSvee().getSveeRec().pwdEff) && "V".equalsIgnoreCase(lgicra.getOQualSvee().getSveeRec().state)) {
					ClnEnv.setSessionLoginID(loginId);
					ClnEnv.setSessionPassword(pwd);
				} else {
					ClnEnv.setSessionLoginID(loginId);
					ClnEnv.setSessionPassword(regNewPwd.getInput());
				}
			}	
			
			if (savePw) {
				// Saving password implies saving loginid
				if (errCode.equals(RC.FILL_SVEE) || errCode.equals(RC.FILL_SVEE4CLBL)) {
					ClnEnv.setPref(me.getApplicationContext(), me.getString(R.string.CONST_PREF_SAVEPASSWORD), true);
					ClnEnv.setEncPref(me.getApplicationContext(), loginId, me.getString(R.string.CONST_PREF_PASSWORD),regNewPwd.getInput());
				} else if (errCode.equals(RC.FILL_SVEE4CLAV) || errCode.equals(RC.FILL_SVEE4CLUB) || errCode.equals(RC.FILL_SVEE_NOTNC) || errCode.equals(RC.FILL_SVEE4CLBL_NOTNC)){
					if ("Y".equalsIgnoreCase(lgicra.getOQualSvee().getSveeRec().pwdEff) && "V".equalsIgnoreCase(lgicra.getOQualSvee().getSveeRec().state)) {
						ClnEnv.setPref(me.getApplicationContext(), me.getString(R.string.CONST_PREF_SAVEPASSWORD), true);
						ClnEnv.setEncPref(me.getApplicationContext(), loginId, me.getString(R.string.CONST_PREF_PASSWORD), pwd);
					} else {
						ClnEnv.setPref(me.getApplicationContext(), me.getString(R.string.CONST_PREF_SAVEPASSWORD), true);
						ClnEnv.setEncPref(me.getApplicationContext(), loginId, me.getString(R.string.CONST_PREF_PASSWORD), regNewPwd.getInput());
					}
				}
			} else {
				// Not saving password - remove the saved password
				ClnEnv.setPref(me.getApplicationContext(), me.getString(R.string.CONST_PREF_SAVEPASSWORD), false);
				ClnEnv.setEncPref(me.getApplicationContext(), "", me.getString(R.string.CONST_PREF_PASSWORD), "");
			}
			redirectMainMenuDialog(Utils.getString(me, R.string.SFRM_SUCCREG_4FILL));
		}
	}

	public void onFail(APIsResponse response) {
		super.onFail(response);
		if (response != null) {
			if (APIsManager.ECLOSION.equals(response.getActionTy())) {
				AcMainCra mAcMainCra = (AcMainCra) response.getCra();
//				if (mAcMainCra.getReply().isEqual(Reply.RC_IVSESS) || mAcMainCra.getReply().isEqual(Reply.RC_ALT)) {
//					// RC_IVSESS
//					// RC_ALT
//					me.redirectDialog(Utils.getString(me, R.string.DLGM_ABORT_IVSESS));
//				} else {
					// RC_IVDATA
					// RC_NOT_AUTH
					// RC_INACTIVE_RCUS
					// RC_UXSVLTERR
					DialogHelper.createSimpleDialog(me, InterpretRCManager.interpretRC_RegnMdu(me, mAcMainCra.getReply()));
//					me.regnGrq = null;
//				}
			} else {
				// General Error Message
				if (!"".equals(response.getMessage()) && response.getMessage() != null) {
					displayDialog(this, response.getMessage());
				} else if (RC.TCSESS_MM.equalsIgnoreCase(response.getReply().getCode())) {
					ivSessDialog();
				} else {
					DialogHelper.createSimpleDialog(me, InterpretRCManager.interpretRC_RegnMdu(me, response.getReply()));
				}
			}
		}
	}
	
	
	// redirect to login screen if RC_IVSESS
		protected final void redirectMainMenuDialog(String message) {
			AlertDialog.Builder builder = new Builder(me);
			builder.setMessage(message);
			builder.setPositiveButton(Utils.getString(me, R.string.btn_ok), new OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					dialog.dismiss();
					Intent intent = new Intent(me.getApplicationContext(), MainMenuActivity.class);
					intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
					startActivity(intent);
					me.overridePendingTransition(R.anim.left_slide_in, R.anim.right_slide_out);
					me.finish();
				}
			});
			builder.setOnCancelListener(new Dialog.OnCancelListener() {
				public void onCancel(DialogInterface dialog) {
					dialog.dismiss();
					Intent intent = new Intent(me.getApplicationContext(), MainMenuActivity.class);
					intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
					startActivity(intent);
					me.overridePendingTransition(R.anim.left_slide_in, R.anim.right_slide_out);
					me.finish();
				}
			});
			builder.create().show();
		}
}
