package com.pccw.myhkt.activity;

import android.content.Context;
import android.content.SharedPreferences;
import androidx.test.espresso.intent.Intents;
import androidx.test.rule.ActivityTestRule;
import androidx.test.ext.junit.runners.AndroidJUnit4;
import android.util.Log;

import com.pccw.myhkt.AccountHelper;
import com.pccw.myhkt.ClnEnv;
import com.pccw.myhkt.R;
import com.pccw.myhkt.base.BaseTest;
import com.pccw.myhkt.util.Constant;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static androidx.test.platform.app.InstrumentationRegistry.getInstrumentation;

@Ignore
@RunWith(AndroidJUnit4.class)
public class TouchIdWelcomeTest extends BaseTest {
    SharedPreferences shared;
    Context context;

    @Rule
    public ActivityTestRule<LoginActivity> activityTestRule =
            new ActivityTestRule<>(LoginActivity.class);


    @Before
    public void prepare() {
        context = getInstrumentation().getTargetContext();
        shared = context.getSharedPreferences(
                "myhkt",
                Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = shared.edit();
        editor.putInt(Constant.TOUCH_ID_WELCOME_TOUCH_ID_COUNTER, 0);
        editor.commit();

        if(ClnEnv.isLoggedIn()){
            executeLogout(context);
        }

        loginMyCredentialsUAT(AccountHelper.getUATAccount8().getUsername(),
                AccountHelper.getUATAccount8().getPassword());
    }

    @Test
    public void testWelcomeDisplayAfterLogin() {
        checkViewIsDisplayed(R.id.welcome_message);

        checkViewWithDrawable(R.id.iv_fingerprint_auth,R.drawable.fingerprint_top_bg);

        checkViewWithDrawable(R.id.iv_fingerprint,R.drawable.ic_fingerprint_icon_fp);
        
        checkViewWithText(R.id.activate_1, context,R.string.fp_welcome_title_p1);
        
        checkViewWithText(R.id.activate_2, context,R.string.fp_welcome_title_p2);

        checkViewWithText(R.id.activate_3, context,R.string.fp_welcome_title_p3);

        checkViewWithText(R.id.ti_btn_activate_now, context,R.string.fp_welcome_btn_activate);
        
        checkViewWithText(R.id.welcome_message, context,R.string.fp_welcome_description_normal);

        checkViewWithText(R.id.ti_btn_activate_later, context,R.string.fp_welcome_btn_skip);
    }

    @Test
    public void testWelcomeDisplayAfterClickingDashboardBanner() {
        if (checkFingerPrintConfirmation()) {
            clickView(R.id.touch_id_button_close);
            clickOK();
        }

        clickView(R.id.mainmenu_topbanner,R.id.mainmenu_frame);
        
        checkViewIsDisplayed(R.id.welcome_message);

        checkViewWithDrawable(R.id.iv_fingerprint_auth,R.drawable.fingerprint_top_bg);

        checkViewWithDrawable(R.id.iv_fingerprint,R.drawable.ic_fingerprint_icon_fp);

        checkViewWithText(R.id.activate_1, context,R.string.fp_welcome_title_p1);

        checkViewWithText(R.id.activate_2, context,R.string.fp_welcome_title_p2);

        checkViewWithText(R.id.activate_3, context,R.string.fp_welcome_title_p3);

        checkViewWithText(R.id.ti_btn_activate_now, context,R.string.fp_welcome_btn_activate);

        checkViewWithText(R.id.welcome_message, context,R.string.fp_welcome_description_normal);

        checkViewWithText(R.id.ti_btn_activate_later, context,R.string.fp_welcome_btn_later);

        delay(TestTimeouts.ACTIVITY_LOAD_TIMEOUTS);
    }

    @Test
    public void testTouchIdCloseBtn() {

        Intents.init();
        clickView(R.id.touch_id_button_close);
        clickOK();
        checkIntendedActivity(MainMenuActivity.class.getName());

        Intents.release();

    }

    @Test
    public void testActivateLaterBtn(){
        Intents.init();

        clickView(R.id.touch_id_button_close);
        clickOK();

        clickView(R.id.mainmenu_topbanner,R.id.mainmenu_frame);

        clickView(R.id.ti_btn_activate_later);

        checkIntendedActivity(MainMenuActivity.class.getName());

        Intents.release();
    }

    @Test
    public void testActivateNowBtnWithDeviceTouchIDNotActivated() {
        clickView(R.id.ti_btn_activate_now);

        checkDialogStringIsDisplayed("Fingerprint ID has not been enabled at this device now. To use the " +
                "fingerprint authentication at My HKT, please enable Fingerprint " +
                "ID inside the \"Settings\" at your device first.");
        clickOK();

        clickView(R.id.touch_id_button_close);
        clickOK();
    }


    private boolean checkFingerPrintConfirmation() {
        Log.d(this.getClass().getName(), "populateUsernameFromSharedPrefsTest: " +
                shared.getInt(Constant.TOUCH_ID_WELCOME_TOUCH_ID_COUNTER, 0));
        return shared.getInt(Constant.TOUCH_ID_WELCOME_TOUCH_ID_COUNTER, 0) < 3;
    }



    @After
    public void tearDown(){
        if(ClnEnv.isLoggedIn()){
            executeLogout(context);
        }
    }

}
