package com.pccw.myhkt.fragment;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcelable;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.ImageView.ScaleType;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.RelativeLayout;

import com.pccw.dango.shared.entity.SveeRec;
import com.pccw.dango.shared.tool.RC;
import com.pccw.myhkt.APIsManager;
import com.pccw.myhkt.APIsResponse;
import com.pccw.myhkt.ClnEnv;
import com.pccw.myhkt.DialogHelper;
import com.pccw.biz.myhkt.R;
import com.pccw.myhkt.Utils;
import com.pccw.myhkt.activity.BaseActivity;
import com.pccw.myhkt.cell.model.Cell;
import com.pccw.myhkt.cell.model.DetailBtnCell;
import com.pccw.myhkt.cell.model.SmallTextCell;
import com.pccw.myhkt.cell.model.TextIconCell;
import com.pccw.myhkt.cell.view.CellViewAdapter;
import com.pccw.myhkt.fragment.IDDRateServiceFragment.OnIDDRateServiceListener;
import com.pccw.myhkt.fragment.PlanLTSMainFragment.OnPlanLtsListener;
import com.pccw.myhkt.lib.ui.AAQuery;
import com.pccw.myhkt.lib.ui.popover.QuickAction;
import com.pccw.myhkt.model.AcctAgent;
import com.pccw.myhkt.model.Destination;
import com.pccw.myhkt.model.Rate;
import com.pccw.myhkt.model.SRPlan;


/************************************************************************
 * File : IDDRatesFragment.java
 * Desc : 
 * Name : IDDRatesFragment
 * by 	: 
 * Date : 
 * 
 * Change History:
 * Date       Modified By		Description
 * ---------- ----------------	-------------------------------
 * 06/23/2017  Abdulmoiz Esmail	- Moved the checking of language on the onAttach method and on the onCreateView method,
 *  							  checked if the locale  has changed to default English (due to webviews in Android 
 *  							  higher or equal 24), if does switch to Chinese. 
 *************************************************************************/
public class IDDRatesFragment extends BaseFragment{

	private String TAG = "IDDRatesFragment";
	private IDDRatesFragment me;
	private AAQuery aq;

	private SveeRec sveeRec = null;

	// CallBack

	private OnIDDRatesListener callback;

	private Boolean 				isZh;
	private Boolean 				isResultShown;
	private int 					extralinespace;
	private int 					textViewHeight;
	private int 					countryPos = -1;
	private ArrayList<Rate> 		rateList = new ArrayList<Rate>();
	private SRPlan 					srPlan;

	private String[] destAry;
	private int destPos;
	private List<Destination> destList;

	private LinearLayout frame ;
	private ArrayList<Cell> cellList;
	private CellViewAdapter cellViewAdapter;

	private Boolean isFromPlanLts = false;
	private OnPlanLtsListener callback_lts;
	public interface OnIDDRatesListener {
		//		public SRPlan getSRPlan();
		//		public void setSRPlan(SRPlan srPlan);	
        String getSrvNum();
		String getCustNum();

	}


	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);

        isZh = "zh".equalsIgnoreCase(getResString(R.string.myhkt_lang));
		
		if (getParentFragment()!=null && getParentFragment() instanceof PlanLTSFragment) {
			try {
				callback_lts = (OnPlanLtsListener) this.getParentFragment();
				isFromPlanLts = true;
			} catch (ClassCastException e) {
				isFromPlanLts = false;
				if (debug) Log.i(TAG, "not from lts");
			}
		}
		try {
			callback = (OnIDDRatesListener) (isFromPlanLts ? getParentFragment() : activity );
		} catch (ClassCastException e) {
			throw new ClassCastException((isFromPlanLts ? getParentFragment().toString() : activity.toString()) + " must implement OnIDDRatesListener");
		}		
	}

	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		me = this;		
		View fragmentLayout = inflater.inflate(R.layout.fragment_iddrates, container, false);

		//Switch to Zh if the locale changed to En and os is >=24
		if (Build.VERSION.SDK_INT >= 24 && isZh && "en".equalsIgnoreCase(getString(R.string.myhkt_lang))) {
				Utils.switchToZhLocale(getActivity());
		}
		
		
		aq = new AAQuery(fragmentLayout);

		initData();
		if (savedInstanceState != null) {

		}
		initUI();
		return fragmentLayout;
	}

	protected void initData() {
		//		srPlan1 = new SRPlan();
		//		srPlan.chiDestName = "澳洲固網電話";
		isResultShown = false;
		extralinespace = (int) getResources().getDimension(R.dimen.extralinespace);
		basePadding = (int) getResources().getDimension(R.dimen.basePadding);
		textViewHeight = (int) getResources().getDimension(R.dimen.textviewheight);
		cellViewAdapter = new CellViewAdapter(getActivity());
		//		titleWidth = (int) getResources().getDimension(R.dimen.idd_title_width);
		//		
		//		hrsList = new String[24];
		//		for (int i=0; i < 24; i++) {
		//			hrsList[i] = String.format("%02d", i);					
		//		}
		//		minsList = new String[60];
		//		for (int i=0; i < 60; i++) {
		//			minsList[i] =  String.format("%02d", i);					
		//		}
		//		initTimePos();
		//		Display display = this.getActivity().getWindowManager().getDefaultDisplay();
		//		Point size = new Point();
		//		display.getSize(size);
		//		int deviceWidth = size.x;
		//		btnWidth = deviceWidth / 2 ;

	}

	protected void initUI() {	

		//IDD0060 Title
		if (isFromPlanLts) {
			aq.id(R.id.fragment_iddrates_plan_lts_frame).backgroundColorId(R.color.white);
			aq.marginpx(R.id.fragment_iddrates_plan_lts_frame, 0, 0, 0, 0);
			frame = (LinearLayout) aq.id(R.id.fragment_iddrates_plan_lts_frame).getView();
			cellList = new ArrayList<Cell>();

			SmallTextCell smallTextCell = new SmallTextCell(getResString(R.string.myhkt_CALL_RATE), "");
			smallTextCell.setTitleTypeface(Typeface.BOLD);
			smallTextCell.setTitleColorId(R.color.black);
			smallTextCell.setLeftPadding(basePadding);
			smallTextCell.setRightPadding(basePadding);
			cellList.add(smallTextCell);

			TextIconCell textIconCell = new  TextIconCell(R.drawable.logo_fixedline_0060_1, "IDD");
			textIconCell.setLeftPadding(basePadding);
			textIconCell.setRightPadding(basePadding);
			cellList.add(textIconCell);

			cellViewAdapter.setView(frame, cellList);
			OnClickListener iddClick = new OnClickListener(){

				@Override
				public void onClick(View v) {
					callback_lts.setActiveSubView(R.string.CONST_SELECTEDVIEW_LTSPLAN_MAIN1);
					callback_lts.displayChildview(2);
				}
			};

			Drawable rightDrawable = getResources().getDrawable(R.drawable.btn_details);
		
			aq.normText(R.id.fragment_iddrates_detailclick, getResString(R.string.PLNLTF_TPL1), -2);
			aq.padding(R.id.fragment_iddrates_detailclick, basePadding, 0, basePadding , 0);
			aq.id(R.id.fragment_iddrates_detailclick).clicked(iddClick).textColorId(R.color.hkt_textcolor).height(ViewGroup.LayoutParams.WRAP_CONTENT).width(ViewGroup.LayoutParams.WRAP_CONTENT);
			int detailBtnPadding = (int) getResources().getDimension(R.dimen.detail_btn_padding);
			aq.id(R.id.fragment_iddrates_detailclick).getTextView().setCompoundDrawablesWithIntrinsicBounds(null, null, rightDrawable, null);
			aq.id(R.id.fragment_iddrates_detailclick).getTextView().setCompoundDrawablePadding(detailBtnPadding);
		}



		aq.normTextGrey(R.id.fragment_iddrates_header1, getResString(R.string.MYHKT_IDDRATE_HEADER1), -2);
		aq.padding(R.id.fragment_iddrates_header1, basePadding, 0, basePadding, 0);
		aq.gravity(R.id.fragment_iddrates_header1, Gravity.BOTTOM);

		aq.padding(R.id.fragment_iddrates_header2_layout, basePadding, 0, basePadding, 0);
		aq.normText(R.id.fragment_iddrates_header2_layout_txt1, callback.getSrvNum() ,2);
		aq.normTextGrey(R.id.fragment_iddrates_header2_layout_txt2, " " + getResString(R.string.MYHKT_IDDRATE_HEADER2) , -2);

		//Select country
		aq.normTextBlue(R.id.fragment_iddrates_country_layout_name, getResString(R.string.MYHKT_IDDRATE_SEL_COUNTRY));
		aq.normTextBlue(R.id.fragment_iddrates_country_layout_desc, "", -2);
		aq.padding(R.id.fragment_iddrates_country_layout, basePadding, 0, basePadding, 0);
		aq.id(R.id.fragment_iddrates_country_layout).clicked(this, "onClick");
		aq.id(R.id.fragment_iddrates_country_layout_name).height(countryPos <0 ? textViewHeight: textViewHeight*4/7, false);
		aq.id(R.id.fragment_iddrates_country_layout_desc).height(countryPos <0 ? 0: textViewHeight*3/7, false);

		aq.id(R.id.fragment_iddrates_country_layout_arrow).image(R.drawable.btn_arrowdown);
		aq.gravity(R.id.fragment_iddrates_country_layout_arrow , Gravity.CENTER);


		//Result Time header
		aq.id(R.id.fragment_iddrates_result_header_layout).visibility(isResultShown? View.VISIBLE :View.GONE);
		aq.id(R.id.fragment_iddrates_result_header_layout_txt1).image(R.drawable.arrow).height(textViewHeight, false);
		aq.id(R.id.fragment_iddrates_result_header_layout_txt1).getImageView().setScaleType(ScaleType.CENTER);
		aq.normText(R.id.fragment_iddrates_result_header_layout_txt2, getResString(R.string.MYHKT_IDDRATE_ON) + DateFormat.format("dd/MM/yy", Calendar.getInstance()) + getResString(R.string.MYHKT_IDDRATE_DMY));
		aq.normTextGrey(R.id.fragment_iddrates_result_header_layout_txt3, getResString(R.string.MYHKT_IDDRATE_PER_MIN) , -2);
		aq.gravity(R.id.fragment_iddrates_result_header_layout_txt3, Gravity.RIGHT|Gravity.CENTER);
		aq.padding(R.id.fragment_iddrates_result_header_layout, basePadding, 0, basePadding, 0);

		((LinearLayout)aq.id(R.id.fragment_iddrates_result_frame).getView()).removeAllViews();
		if (isResultShown) {
			setResult();
		}


		aq.line(R.id.fragment_iddrates_line1);
		aq.line(R.id.fragment_iddrates_line2);
		//		aq.line(R.id.iddcodesntime_line3);
		//		aq.line(R.id.iddcodesntime_line4);
		//		aq.line(R.id.iddcodesntime_line5);
		aq.marginpx(R.id.fragment_iddrates_line1, basePadding, 0, basePadding, 0);
		aq.marginpx(R.id.fragment_iddrates_line2, basePadding, 0, basePadding, 0);

		//Remark textview
		aq.normTextGrey(R.id.fragment_iddrates_remark_txt, "", -2);
		aq.padding(R.id.fragment_iddrates_remark_txt, basePadding, 0, basePadding, 0);

		//Remark
		aq.id(R.id.fragment_iddrates_result_scrollview).visibility(isResultShown ? View.VISIBLE : View.INVISIBLE);
		aq.id(R.id.fragment_iddrates_remark).visibility(isResultShown ? View.VISIBLE : View.INVISIBLE);
		aq.marginpx(R.id.fragment_iddrates_remark, basePadding, extralinespace, basePadding, extralinespace);
		aq.id(R.id.fragment_iddrates_remark).getWebView().getSettings().setDefaultTextEncodingName("utf-8");
		aq.id(R.id.fragment_iddrates_remark).getWebView().loadUrl(isZh ? "file:///android_asset/idd_rate_remarks_zh.html" : "file:///android_asset/idd_rate_remarks_en.html");
	}

	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.fragment_iddrates_country_layout:
			openPicker();
			break;	
		}
	}

	/**********************************
	 * 
	 * Destination part
	 * 
	 ***********************************/

	QuickAction mQuickAction;
	private final void openPicker(){
		//		ActionItem[] actionItem = new ActionItem[destList.size()];
		//		for (int i=0; i<destList.size(); i++) {
		//			String name = isZh ?destList.get(i).chiDestName :destList.get(i).engDestName;
		//			String ext = isZh ?destList.get(i).chiDestNameExt :destList.get(i).engDestNameExt;
		//			Boolean isExtShown = ext != null && !ext.equals("");
		//			String str = isExtShown ? name +"\n" + ext : name;
		//			actionItem[i] = new ActionItem(i, str);
		//		}
		//
		//		WindowManager wm = (WindowManager) this.getActivity().getSystemService(Context.WINDOW_SERVICE);
		//		int screenWidth 	= wm.getDefaultDisplay().getWidth();
		//		mQuickAction 	= new QuickAction(this.getActivity(), screenWidth/2, 0);
		//		for (int i = 0; i < actionItem.length; i++) {
		//			mQuickAction.addActionItem(actionItem[i]);
		//		}
		//
		//		//setup the action item click listener
		//		mQuickAction.setOnActionItemClickListener(new QuickAction.OnActionItemClickListener() {
		//			@Override
		//			public void onItemClick(QuickAction quickAction, int pos, int actionId) {
		//
		//				setDest(isZh ? destList.get(pos).chiDestName:destList.get(pos).engDestName,
		//						isZh ? destList.get(pos).chiDestNameExt:destList.get(pos).engDestNameExt);
		//				HashMap <String, String> hmSelectedDestDetail = new HashMap<String, String>();
		//				hmSelectedDestDetail.put("dest", destList.get(pos).engDestName);
		//				hmSelectedDestDetail.put("destext",destList.get(pos).engDestNameExt);
		//				hmSelectedDestDetail.put("customernum", me.callback.getCustNum());
		//				hmSelectedDestDetail.put("servicenum", me.callback.getSrvNum());
		//				hmSelectedDestDetail.put("srvtype", "MOB");
		//				APIsManager.doGetIDRateResult(me,hmSelectedDestDetail);
		//
		//			}
		//		});
		//		mQuickAction.show(aq.id(R.id.fragment_iddrates_country_layout).getView(), aq.id(R.id.fragment_iddrates_country_layout).getView());

		destAry = new String[destList.size()+1];
		destAry[0] = getResString(R.string.MYHKT_IDDRATE_SEL_COUNTRY);
		for (int i=0; i<destList.size(); i++) {
			String name = isZh ?destList.get(i).chiDestName :destList.get(i).engDestName;
			String ext = isZh ?destList.get(i).chiDestNameExt :destList.get(i).engDestNameExt;
			Boolean isExtShown = ext != null && !ext.equals("");
			String str = isExtShown ? name +"\n" + ext : name;
			destAry[i+1] = str;
		}


		final QuickAction pickerWheelAction = new QuickAction(this.getActivity(), 0, 0);

		pickerWheelAction.initPickerWheelSingle(destAry, destPos);
		pickerWheelAction.setOnPickerScrollSingleListener(new QuickAction.OnPickerScrollSingleListener() {
			@Override
			public void onPickerScroll(String leftResult, int pos) {
				destPos = pos;
				if (pos != 0) {
					setDest(isZh ? destList.get(pos-1).chiDestName:destList.get(pos-1).engDestName,
							isZh ? destList.get(pos-1).chiDestNameExt:destList.get(pos-1).engDestNameExt);
					HashMap <String, String> hmSelectedDestDetail = new HashMap<String, String>();
					hmSelectedDestDetail.put("dest", destList.get(pos-1).engDestName);
					hmSelectedDestDetail.put("destext",destList.get(pos-1).engDestNameExt);
					hmSelectedDestDetail.put("customernum", callback.getCustNum());
					hmSelectedDestDetail.put("servicenum", callback.getSrvNum());
					hmSelectedDestDetail.put("srvtype", "MOB");
					pickerWheelAction.dismiss();
					APIsManager.doGetIDRateResult(me,hmSelectedDestDetail);
				} else {
					setDest(getResString(R.string.MYHKT_IDDRATE_SEL_COUNTRY), "");
					clearResult();	
				}
			}
		});
		pickerWheelAction.showPicker(aq.id(R.id.fragment_iddrates_country_layout).getView(), aq.id(R.id.fragment_iddrates_country_layout).getView());
	}

	public void setDest(String name, String ext) {
		Boolean isExtShown = ext != null && !ext.equals("");
		if (debug) Log.i(TAG, isExtShown + ext);
		aq.id(R.id.fragment_iddrates_country_layout_name).height(!isExtShown? textViewHeight: textViewHeight*4/7 , false);
		aq.id(R.id.fragment_iddrates_country_layout_desc).height(!isExtShown ? 0: textViewHeight*3/7, false);
		aq.id(R.id.fragment_iddrates_country_layout_name).text(name);
		aq.id(R.id.fragment_iddrates_country_layout_desc).text(ext);
	}
	/**********************************
	 * 
	 * Result part
	 * 
	 ***********************************/
	public void doSearch() {
		//		HashMap<String, String> hmSelectedDestDetail = new HashMap<String, String>();
		//		hmSelectedDestDetail.put("dest", currDestinations[position].engDestName);
		//		hmSelectedDestDetail.put("destext", currDestinations[position].engDestNameExt);
		//		hmSelectedDestDetail.put("customernum", ClnEnv.getLgiCra().getISveeRec().me.callback.getSubscriptionRec().cus_num);
		//		hmSelectedDestDetail.put("servicenum", me.callback.getSubscriptionRec().srv_num);
		//		hmSelectedDestDetail.put("srvtype", "MOB");
		//		APIsManager.doGetIDRateResult(me, hmSelectedDestDetail);
		//		rateList = new ArrayList<Rate>();
		//		Rate rate = new Rate();
		//		rate.timeZone = "Non-peak hours";
		//		rate.timeZoneRemarks = "Monday-Friday 20:00-09:00/nAll day Saturday, Sunday & public holiday";
		//		rate.rate = "6.84";
		//		rateList.add(rate);
		//		Rate rate1 = new Rate();
		//		rate1.timeZone = "Peak hours";
		//		rate1.timeZoneRemarks = "Monday-Friday 09:00-21:00";
		//		rate1.rate = "8.45";
		//		rateList.add(rate1);
		//		setResult();		
	}

	public void clearResult(){
		isResultShown = false; 
		aq.id(R.id.fragment_iddrates_result_header_layout).visibility(isResultShown? View.VISIBLE :View.INVISIBLE);
		aq.id(R.id.fragment_iddrates_result_scrollview).visibility(isResultShown? View.VISIBLE: View.INVISIBLE);
		aq.id(R.id.fragment_iddrates_remark).visibility(isResultShown? View.VISIBLE: View.INVISIBLE);
		((LinearLayout)aq.id(R.id.fragment_iddrates_result_frame).getView()).removeAllViews();
	}
	public void setResult(){

		isResultShown = true; 
		aq.id(R.id.fragment_iddrates_result_header_layout).visibility(isResultShown? View.VISIBLE :View.INVISIBLE);
		aq.id(R.id.fragment_iddrates_result_scrollview).visibility(isResultShown? View.VISIBLE: View.INVISIBLE);
		aq.id(R.id.fragment_iddrates_remark).visibility(isResultShown? View.VISIBLE: View.INVISIBLE);
		((LinearLayout)aq.id(R.id.fragment_iddrates_result_frame).getView()).removeAllViews();
		//		rateList = srPlan.rateList;

		if (rateList !=null && rateList.size()>0){
			for (int rx = 0; rx < rateList.size(); rx++) {
				LayoutInflater layoutInflater = (LayoutInflater) this.getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);  
				RelativeLayout view = (RelativeLayout) layoutInflater.inflate(R.layout.item_srplan, null, false);
				view.setBackgroundColor(this.getResources().getColor(R.color.idd_grey));

				String name = isZh ? rateList.get(rx).chiTimeZone : rateList.get(rx).timeZone ;
				String desc = isZh ? rateList.get(rx).chiTimeZoneRemarks : rateList.get(rx).timeZoneRemarks ;
				String rates = "HK $" + rateList.get(rx).rate;
				AAQuery a = new AAQuery(view);
				a.normTextGrey(R.id.item_srplan_title, name);
				a.normTextBlue(R.id.item_srplan_rate, rates);
				a.normTextGrey(R.id.item_srplan_time, desc);

				((LinearLayout)aq.id(R.id.fragment_iddrates_result_frame).getView()).addView(view);
				LinearLayout.LayoutParams params = (LayoutParams) view.getLayoutParams();
				//				params.topMargin = basePadding;
				view.setLayoutParams(params);
				view.setPadding(basePadding, basePadding/2, extralinespace, extralinespace/2);
			}
		}

		if (!isZh) {
			if (srPlan !=null && srPlan.engDestNameExt != null && srPlan.engDestNameExt.trim().length() != 0) {
				aq.id(R.id.fragment_iddrates_remark_txt).visibility(View.VISIBLE);
				aq.normTextGrey(R.id.fragment_iddrates_remark_txt, "*Remarks: " + srPlan.engDestNameExt , -2);
			} else {
				aq.id(R.id.fragment_iddrates_remark_txt).visibility(View.GONE);
			}
		} else {
			if (srPlan !=null && srPlan.chiDestNameExt != null && srPlan.chiDestNameExt.trim().length() != 0) {
				aq.id(R.id.fragment_iddrates_remark_txt).visibility(View.VISIBLE);
				aq.normTextGrey(R.id.fragment_iddrates_remark_txt, "*備註: " + srPlan.chiDestNameExt , -2);
			} else {
				aq.id(R.id.fragment_iddrates_remark_txt).visibility(View.GONE);
			}

		}

	}


	@Override
	public void onFail(APIsResponse response) {
		if (debug) Log.i(TAG, "Fail");
		// General Error Message
		if (!"".equals(response.getMessage()) && response.getMessage() != null) {
			DialogHelper.createSimpleDialog(getActivity(),  response.getMessage());
		} else if (RC.TCSESS_MM.equalsIgnoreCase(response.getReply().getCode())) {
			BaseActivity.ivSessDialog();
		} else {
			DialogHelper.createSimpleDialog(getActivity(),  ClnEnv.getRPCErrMsg(getActivity(), response.getReply().getCode()));
		}		
	}

	@Override
	public void onStart() {
		super.onStart();
	}

	public void onResume(){
		super.onResume();	
		if (destList == null || destList.size() <=0){
			APIsManager.doGetIDDRateDest(me);	
		}		
	}

	@Override
	public void onSuccess(APIsResponse response) {
		if (debug) Log.i(TAG, "onSuccess");
		if (response != null) {
			if (APIsManager.IDDRATEDEST.equals(response.getActionTy())) {
				destList = (List<Destination>) response.getCra();
			} else if (APIsManager.IDDRATERESULT.equals(response.getActionTy())) {
				srPlan  = (SRPlan) response.getCra();
				rateList = srPlan.rateList;
				setResult();
			}
		}

	}
}
