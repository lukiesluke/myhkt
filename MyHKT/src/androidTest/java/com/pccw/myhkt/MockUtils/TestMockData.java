package com.pccw.myhkt.MockUtils;

import androidx.test.platform.app.InstrumentationRegistry;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.pccw.dango.shared.cra.HeloCra;
import com.pccw.dango.shared.cra.LgiCra;
import com.pccw.dango.shared.entity.CustRec;
import com.pccw.dango.shared.entity.SubnRec;
import com.pccw.dango.shared.entity.SveeRec;
import com.pccw.myhkt.APIsResponse;
import com.pccw.myhkt.JsonHelper;

import java.lang.reflect.Type;

public class TestMockData {

    public static LgiCra getUATLoginSuccessResponse() {
        APIsResponse apIsResponse = new APIsResponse();

        try {
            String json = JsonHelper.getUatMockData(InstrumentationRegistry.getContext());

            Gson gson = new Gson();

            Object obj = gson.fromJson(json, LgiCra.class);

            apIsResponse.setCra(obj);

        } catch (Throwable t) {
            Log.e("My App", t.getLocalizedMessage());
        }

        return  (LgiCra) apIsResponse.getCra();
    }

    public static SveeRec getUATSveeRecObject() {
        APIsResponse apIsResponse = new APIsResponse();

        try {
            String json = JsonHelper.getMockData(InstrumentationRegistry.getContext(),
                    "sveerec_mock_data.json");

            Gson gson = new Gson();

            Object obj = gson.fromJson(json, SveeRec.class);

            apIsResponse.setCra(obj);

        } catch (Throwable t) {
            Log.e("My App", t.getLocalizedMessage());
        }

        return  (SveeRec) apIsResponse.getCra();
    }

    public static SubnRec getUATSubnRecObject() {
        APIsResponse apIsResponse = new APIsResponse();

        try {
            String json = JsonHelper.getMockData(InstrumentationRegistry.getContext(),
                    "subn_mock_data.json");

            Gson gson = new Gson();

            Object obj = gson.fromJson(json, SubnRec.class);

            apIsResponse.setCra(obj);

        } catch (Throwable t) {
            Log.e("My App", t.getLocalizedMessage());
        }

        return  (SubnRec) apIsResponse.getCra();
    }

    public static CustRec getUATCustRecObject() {
        APIsResponse apIsResponse = new APIsResponse();

        try {
            String json = JsonHelper.getMockData(InstrumentationRegistry.getContext(),
                    "cust_mock_data.json");

            Gson gson = new Gson();

            Object obj = gson.fromJson(json, CustRec.class);

            apIsResponse.setCra(obj);

        } catch (Throwable t) {
            Log.e("My App", t.getLocalizedMessage());
        }

        return  (CustRec) apIsResponse.getCra();
    }

    public static HeloCra getHeloCra() {
        String json = (String) MockJsonResponse.LOGIN_UAT_RESPONSE();
        HeloCra heloCra = new HeloCra();
        try {

            Gson gson = new Gson();

            Type collectionType = new TypeToken<HeloCra>() {}.getType();
            heloCra = gson.fromJson(json.toString(), collectionType);



        } catch (Throwable t) {
            Log.e("My App", "Could not parse malformed JSON: \"" + json + "\"");
        }

        return  heloCra;
    }
}
