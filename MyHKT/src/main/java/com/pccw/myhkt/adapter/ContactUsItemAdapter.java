package com.pccw.myhkt.adapter;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.pccw.myhkt.R;
import com.pccw.myhkt.lib.ui.MyHktImageButton;
import com.pccw.myhkt.model.ContactUsItem;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ContactUsItemAdapter extends
        RecyclerView.Adapter<ContactUsItemAdapter.ViewHolder> {

    private final Context context;
    private final ArrayList<ContactUsItem> contactUsItems;

    public ContactUsItemAdapter(Context context) {
        this.context = context;
        this.contactUsItems = new ArrayList<>();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_item_contact_title)
        TextView tvTitle;

        @BindView(R.id.tv_item_contact_sub_title)
        TextView tvSubtitle;

        @BindView(R.id.btn_item_contact_button)
        MyHktImageButton btnContact;

        @BindView(R.id.iv_item_contact_image)
        ImageView ivImage;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        return new ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_contact,
                parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull final ContactUsItemAdapter.ViewHolder holder,
                                 final int position) {
        ContactUsItem contactUsItem = contactUsItems.get(position);
        checkTitleContent(holder, contactUsItem);
        checkSubtitleContent(holder, contactUsItem);
        checkActionContent(holder, contactUsItem);
    }

    @Override
    public int getItemCount() {
        return contactUsItems.size();
    }

    public void addItemEntry(ContactUsItem contactUsItem) {
        this.contactUsItems.add(contactUsItem);
        notifyDataSetChanged();
    }

    public void addItemEntries(ArrayList<ContactUsItem> contactUsItems) {
        this.contactUsItems.clear();
        this.contactUsItems.addAll(contactUsItems);
        notifyDataSetChanged();
    }

    private void checkTitleContent(ContactUsItemAdapter.ViewHolder holder,
                                   ContactUsItem contactUsItem) {
        if (contactUsItem.getTitle() != -1)
            holder.tvTitle.setText(contactUsItem.getTitle());
        else if (!contactUsItem.getHardTitle().isEmpty())
            holder.tvTitle.setText(contactUsItem.getHardTitle());
        else holder.tvTitle.setVisibility(View.GONE);
    }

    private void checkSubtitleContent(ContactUsItemAdapter.ViewHolder holder,
                                      ContactUsItem contactUsItem) {
        if (contactUsItem.getSubtitle() != -1)
            holder.tvSubtitle.setText(contactUsItem.getSubtitle());
        else if (!contactUsItem.getHardSubtitle().isEmpty())
            holder.tvTitle.setText(contactUsItem.getHardSubtitle());
        else
            holder.tvSubtitle.setVisibility(View.GONE);
    }

    private void checkActionContent(ContactUsItemAdapter.ViewHolder holder,
                                    ContactUsItem contactUsItem) {
        if (contactUsItem.getActionText() != -1) {
            holder.btnContact.init(contactUsItem.getIcon(), contactUsItem.getActionText());
            holder.btnContact.setOnClickListener(
                    v -> contactUsItem.getItemClickListener().onItemClick());
        } else {
            holder.btnContact.setVisibility(View.GONE);
            holder.ivImage.setVisibility(View.VISIBLE);
            if (contactUsItem.getIcon() != -1) {
                holder.ivImage.setImageDrawable(ContextCompat.getDrawable(context, contactUsItem.getIcon()));
                holder.ivImage.setOnClickListener(
                        v -> contactUsItem.getItemClickListener().onItemClick());
            } else
                holder.ivImage.setVisibility(View.GONE);
        }
    }
}
