package com.pccw.myhkt.activity;

import android.Manifest;
import android.content.Context;
import androidx.test.rule.ActivityTestRule;
import androidx.test.rule.GrantPermissionRule;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import com.pccw.myhkt.R;
import com.pccw.myhkt.base.BaseTest;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static androidx.test.platform.app.InstrumentationRegistry.getInstrumentation;

@Ignore
@RunWith(AndroidJUnit4.class)
public class ShopTypeListUIActivityTest extends BaseTest {
    private Context context;

    @Rule
    public ActivityTestRule<MainMenuActivity> activityTestRule = new ActivityTestRule<>(
            MainMenuActivity.class);
    @Rule public GrantPermissionRule permissionRule = GrantPermissionRule.grant(
            Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION);

    @Before
    public void setUp(){
        context = getInstrumentation().getTargetContext();
        gotoMainMenuItem(10);
    }

    @Test
    public void testDisplayUI() {
        checkStringIsDisplayed(context, R.string.myhkt_shop_title);
        checkViewIsDisplayed(R.id.navbar_button_left, R.id.navbar_base_layout);
        checkViewIsDisplayed(R.id.activity_browser_webview);
        checkViewIsDisplayed(R.id.shoptype_listview);
        checkViewIsDisplayed(R.id.shoptype_remark);
        checkViewIsDisplayed(R.id.shopregion_googletacs);
        checkViewIsDisplayed(R.id.shopregion_googleprivacy);
    }

    @Test
    public void testMyLocationDisplay() {
        clickViewInAdapter(R.id.shoptype_listview,0);
        checkStringIsDisplayed(context, R.string.myhkt_shop_myloc);
    }

    @Test
    public void testAllShopsDisplay() {
        clickViewInAdapter(R.id.shoptype_listview,1);
        checkStringIsDisplayed(context, R.string.myhkt_shop_allshop);
    }

    @Test
    public void testIoTHKTDisplay() {
        clickViewInAdapter(R.id.shoptype_listview,2);
        checkStringIsDisplayed(context, R.string.myhkt_shop_iot);
    }

    @Test
    public void testHKTShopsDisplay() {
        clickViewInAdapter(R.id.shoptype_listview,3);
        checkStringIsDisplayed(context, R.string.myhkt_shop_hkt);
    }

    @Test
    public void test1O1OCentreDisplay() {
        clickViewInAdapter(R.id.shoptype_listview,4);
        checkStringIsDisplayed(context, R.string.myhkt_shop_1010);
    }

    @Test
    public void testcslShopsDisplay() {
        clickViewInAdapter(R.id.shoptype_listview,5);
        checkStringIsDisplayed(context, R.string.myhkt_shop_csl);
    }

    @Test
    public void testCustomerServiceCenterDisplay() {
        clickViewInAdapter(R.id.shoptype_listview,6);
        checkStringIsDisplayed(context, R.string.myhkt_shop_cs);
    }

    @Test
    public void testSmartLivingStoreShowcaseDisplay() {
        clickViewInAdapter(R.id.shoptype_listview,7);
        checkStringIsDisplayed(context, R.string.myhkt_shop_smart_living);
    }

    @Test
    public void testSMEBusinessSolutionsCounterDisplay() {
        clickViewInAdapter(R.id.shoptype_listview,8);
        checkStringIsDisplayed(context, R.string.myhkt_shop_biz);
    }

}