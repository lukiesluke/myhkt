package com.pccw.myhkt.mymob.fragment;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.os.Bundle;
import android.text.InputFilter;
import android.text.Selection;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;

import com.pccw.dango.shared.cra.AcMainCra;
import com.pccw.dango.shared.cra.AddOnCra;
import com.pccw.dango.shared.entity.SubnRec;
import com.pccw.dango.shared.tool.RC;
import com.pccw.myhkt.APIsManager;
import com.pccw.myhkt.APIsResponse;
import com.pccw.myhkt.ClnEnv;
import com.pccw.myhkt.DialogHelper;
import com.pccw.biz.myhkt.R;
import com.pccw.myhkt.Utils;
import com.pccw.myhkt.activity.BaseActivity;
import com.pccw.myhkt.activity.LoginActivity;
import com.pccw.myhkt.activity.ServiceActivity;
import com.pccw.myhkt.lib.swipelistview.BaseSwipeListViewListener;
import com.pccw.myhkt.lib.swipelistview.SwipeListView;
import com.pccw.myhkt.lib.ui.AAQuery;
import com.pccw.myhkt.lib.ui.HKTButton;
import com.pccw.myhkt.model.AcctAgent;
import com.pccw.myhkt.model.HomeButtonItem.MAINMENU;
import com.pccw.myhkt.mymob.MyMobileAccountHelper;
import com.pccw.myhkt.mymob.adapter.MyMobListViewAdapter;
import com.pccw.myhkt.mymob.adapter.MyMobListViewAdapter.OnMyMobListAdapterListener;
import com.pccw.myhkt.mymob.model.EditCra;

public class MyMobServiceListFragment extends BaseMyMobFragment implements OnMyMobListAdapterListener{
	private MyMobServiceListFragment me;
	private View myView;
	private AAQuery aq;
	
	private MyMobileAccountHelper				myMobileAccountHelper;
	
	private SwipeListView 					mymob_myhkt_list;
	private MyMobListViewAdapter			mobListViewAdapter		= null;

	private boolean							isEditMode			= false;

	private int					CLICK_BTN_ID			= 0;

	private SubnRec							ioSubscriptionRec 		= null;
	private AddOnCra 						addonCra;
	
	private AcctAgent					selectedAcctAgent		= null;

	
	//UI data
		private final int 			colMaxNum = 3;
		private int					colWidth = 0;
	private int 					buttonPadding = 0;
	private int 					extralinespace = 0;
	private int 					greylineHeight = 0;
	private int 					txtColor;
	private int 					textSize;
	
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		me = this;		

		View fragmentLayout = inflater.inflate(R.layout.fragment_mymob_servicelist, container, false);
		myView = fragmentLayout;
		return fragmentLayout;
	}

	@Override
	public void onStart() {
		super.onStart();
		if (APIsManager.AO_ADDON.equals(callback_main.getActionTy())) { // redirect from login
			callback_main.setActiveSubview(R.string.CONST_SELECTEDFRAG_MYMOB_ADDACC);
			callback_main.displaySubview();
			callback_main.setActionTy(null);
		} else if (APIsManager.AO_AUTH.equals(callback_main.getActionTy())) { // redirect from login
			//refill subnrec before retry
//			callback_main.setIoSubscriptionRec((callback_main.getRecallAcctAgent().getSubnRec()));
//			callback_main.setSelectedAcctAgent(callback_main.getRecallAcctAgent());
			
			AcctAgent acctAgent = new AcctAgent();
			acctAgent = callback_main.getRecallAcctAgent();
			me.selectedAcctAgent = acctAgent;
			
			if (debug) Log.d("mymob onclick", acctAgent.getSrvNum() + "/" + acctAgent.getPassword() + "/" + acctAgent.getLob());
			ioSubscriptionRec = new SubnRec();
			ioSubscriptionRec.srvNum = acctAgent.getSrvNum();
			ioSubscriptionRec.ivr_pwd = acctAgent.getPassword();
			ioSubscriptionRec.lob = acctAgent.getLob();
			callback_main.setIoSubscriptionRec(ioSubscriptionRec);
			
			authenticate(); //auto retry from login redirect
			
			callback_main.setAutoLogin(false);
			callback_main.setActionTy(null);
		} else if(callback_main.isEmptyList()) { //normal empty case
			callback_main.setActiveSubview(R.string.CONST_SELECTEDFRAG_MYMOB_ADDACC);
			callback_main.displaySubview();
		}
		callback_main.showLiveChatIcon();
	}
	
	@Override
	public void onPause(){
		super.onPause();
	}
	
	@Override
	public void onResume(){
		super.onResume();
	}
	
	protected void initUI() {
		aq = new AAQuery(myView);
		int padding_screen =  (int) getActivity().getResources().getDimension(R.dimen.padding_screen);
		int basePadding =  (int) getActivity().getResources().getDimension(R.dimen.basePadding);
		
		aq.marginpx(R.id.mymob_servicelist_desc, padding_screen, padding_screen, padding_screen, padding_screen);
		aq.marginpx(R.id.mymob_servicelist_add_btn, padding_screen, padding_screen, padding_screen, padding_screen);
		aq.id(R.id.mymob_servicelist_add_btn).textColorId(R.color.hkt_textcolor_orange);
		aq.id(R.id.mymob_servicelist_add_btn).getTextView().setCompoundDrawablesWithIntrinsicBounds(R.drawable.rightarrow_small, 0, 0, 0);
		aq.id(R.id.mymob_servicelist_add_btn).getTextView().setCompoundDrawablePadding(basePadding);
		aq.id(R.id.mymob_servicelist_add_btn).clicked(this, "onClick");
		
		aq.normTxtBtn(R.id.mymob_edit_button, getResString(R.string.myhkt_btn_alias) , LinearLayout.LayoutParams.MATCH_PARENT , HKTButton.TYPE_ORANGE);
		aq.id(R.id.mymob_edit_button).clicked(this, "onClick");
		
		myMobileAccountHelper = MyMobileAccountHelper.getInstance(getActivity());
		loadList();
		
		//auto login redirect from Add Account success call 
		if (callback_main.isAutoLogin()) {
			callback_main.setAutoLogin(false);
			
			if (callback_main.getAddOnCra() != null) { //auto login from add account action
				////////////////
				AcctAgent acctAgent = new AcctAgent();
				acctAgent = callback_main.getSelectedAcctAgent();
				me.selectedAcctAgent = acctAgent;
				callback_main.setRecallAcctAgent(acctAgent);
				
				// localSessTok = "DSFS3123123";
				if (debug) Log.d("mymob onclick", acctAgent.getSrvNum() + "/" + acctAgent.getPassword() + "/" + acctAgent.getLob());
				
				ioSubscriptionRec = new SubnRec();
				ioSubscriptionRec = callback_main.getAddOnCra().getOSubnRec();
//				ioSubscriptionRec.srvNum = acctAgent.getSrvNum();
//				ioSubscriptionRec.ivr_pwd = acctAgent.getPassword();
//				ioSubscriptionRec.lob = acctAgent.getLob();
				callback_main.setIoSubscriptionRec(ioSubscriptionRec);
				authenticate();
//				APIsManager.forceCloseProgressDialog(getActivity()); //TODO progressdialog fix
				/////////////////
			}
//			else { //auto login from login existing account action
//				AcctAgent acctAgent = new AcctAgent();
//				acctAgent = callback_main.getRecallAcctAgent();
//				me.selectedAcctAgent = acctAgent;
//				
//				if (debug) Log.d("mymob onclick", acctAgent.getSrvNum() + "/" + acctAgent.getPassword() + "/" + acctAgent.getLob());
//				ioSubscriptionRec = new SubnRec();
//				ioSubscriptionRec.srvNum = acctAgent.getSrvNum();
//				ioSubscriptionRec.ivr_pwd = acctAgent.getPassword();
//				ioSubscriptionRec.lob = acctAgent.getLob();
//				callback_main.setIoSubscriptionRec(ioSubscriptionRec);
//				authenticate();
//			}
		}
	}
	
	private void loadList() {
		mymob_myhkt_list = getActivity().findViewById(R.id.mymob_myhkt_list);
//		RelativeLayout mymob_login_layout = (RelativeLayout) aq.id(R.id.mymob_login_layout).getView();

//		if (debug) Log.d("MyMobileActivity", "Load List Form MyAccount");
		try {
//			mobListViewAdapter = new MyMobListViewAdapter(this, parentOnClickId, mymob_myhkt_list);
			mobListViewAdapter = new MyMobListViewAdapter(me, mymob_myhkt_list);
		} catch (Exception e) {
			e.printStackTrace();
		}

		// if (debug) Log.d("MyMobileActivity", mobListViewAdapter.getAccountCount() + " rows in ListView "+mobListViewAdapter.getMyAcctListCount()+"/"+mobListViewAdapter.getMyMobListCount());

		// Set adapter for mymob_listview
		int deviceWidth = getResources().getDisplayMetrics().widthPixels;
		mymob_myhkt_list.setDeviceWidth(deviceWidth);
		mymob_myhkt_list.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
		mymob_myhkt_list.setAdapter(mobListViewAdapter);
//		mymob_myhkt_list.setEmptyView(findViewById(R.id.servicelist_empty));

		mymob_myhkt_list.setSwipeListViewListener( new AccountListSwipeListViewListener());
		mymob_myhkt_list.setSwipeMode(SwipeListView.SWIPE_MODE_LEFT);
		mymob_myhkt_list.setSwipeActionLeft(SwipeListView.SWIPE_ACTION_REVEAL);
		mymob_myhkt_list.setSwipeActionRight(SwipeListView.SWIPE_ACTION_NONE);
		mymob_myhkt_list.setOffsetLeft(deviceWidth * 5 / 6);
		mymob_myhkt_list.setAnimationTime(0);
		mymob_myhkt_list.setSwipeOpenOnLongPress(false);
		
		if (mobListViewAdapter.hasMyMobAcct()) {
			// if MyMob standalone list has records , collapse/gone login laylout
			if (callback_main.isEditMode()) {
//				collapse(mymob_login_layout);  //TODO stay in servicelist page
			} else {
//				mymob_login_layout.setVisibility(View.GONE); //no use 
			}
		}
		// No records in the MyMob standalone list ,hide expand/show login layout
		else {
			if (callback_main.isEditMode()) {
//				expand(mymob_login_layout);  //go to login page
			} else {
//				mymob_login_layout.setVisibility(View.VISIBLE); //no use
			}
			
//			EditText mymob_mobilenum = (EditText) aq.id(R.id.mymob_mobilenum).getView();
//			mymob_mobilenum.setEnabled(true);
//			mymob_mobilenum.setCursorVisible(true);
//			mymob_mobilenum.setFocusable(true);
//			mymob_mobilenum.setFocusableInTouchMode(true);
			
			
//			DigitsKeyListener digitsKeyListener = DigitsKeyListener.getInstance();
//			viewholder.mobile_num.setKeyListener(digitsKeyListener);

		}
		cleanupLoginLayout();
		// If has record in list , show menu button and arrow button
//		TextView mymob_login_header = (TextView) aq.id(R.id.mymob_login_header).getView();
		if (mobListViewAdapter.getAccountCount() > 1) {
			aq.id(R.id.mymob_edit_button).getButton().setVisibility(View.VISIBLE);
//			mymob_login_header.setClickable(true);
			// Change Button to down arrow
//			mymob_login_header.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.btn_arrowup, 0);
		}
		// No records in list ,hide menu button and arrow button
		else {
			aq.id(R.id.mymob_edit_button).getButton().setVisibility(View.GONE);
//			mymob_login_header.setClickable(false);
//			mymob_login_header.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
		}
		callback_main.setEditMode(false);
	}
	
	public void onClick(View v) {
		RelativeLayout mymob_login_layout = (RelativeLayout) aq.id(R.id.mymob_login_layout).getView();
		switch (v.getId()) {
		case R.id.mymob_edit_button:
			mymob_myhkt_list = getActivity().findViewById(R.id.mymob_myhkt_list);
			if (mymob_myhkt_list != null && mymob_myhkt_list.getVisibility() == View.VISIBLE) {
				if (mymob_myhkt_list.isOpened()) {
					mymob_myhkt_list.closeOpenedItems();
				} else {
					for (int i = mymob_myhkt_list.getFirstVisiblePosition(); i <= mymob_myhkt_list.getLastVisiblePosition(); i++) {
						if (mobListViewAdapter.getItemViewType(i) == 0) {
							mymob_myhkt_list.openAnimate(i);
						}
					}
				}
			}
			break;
		case R.id.mymob_edit_cancel_button:
			InputMethodManager inputManager;
//			isEditMode = false;
			callback_main.setEditMode(false);
			// hide the soft keyboard
			try {
				inputManager = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
				inputManager.hideSoftInputFromWindow(mymob_login_layout.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
				inputManager = null;
			} catch (Exception e) {
				// fail to hide the keyboard
			}
			aq.id(R.id.mymob_login_header).getTextView().performClick();
			break;
		case R.id.mymob_edit_remove_button:
			removeAccount();
			break;
		case R.id.mymob_forget_pw_txt:
			DialogHelper.createSimpleDialog(getActivity(), getString(R.string.MYMOB_HINT_PASSWORD));
			break;
		case R.id.mymob_servicelist_add_btn:
			callback_main.setActiveSubview(R.string.CONST_SELECTEDFRAG_MYMOB_ADDACC);
			callback_main.displaySubview();
			break;
		}
	}
	
	private void loginAndSave() {
		// Set srv_num and pwd from input
		ioSubscriptionRec = new SubnRec();
		ioSubscriptionRec.srvNum = aq.id(R.id.mymob_mobilenum).getText().toString();
		ioSubscriptionRec.ivr_pwd = aq.id(R.id.mymob_password).getText().toString();
		// Do verify first
//		if (localSessTok == null) {
//			startDoHelo();
//		} else {
//			startDoVerifyMyMobByAreaAsyncTask(localSessTok);
//		}
		doVerify();
	}
	
	private boolean isMyAccountRecExist(String mobileNum) {
		if (ClnEnv.isLoggedIn()) {
			if (ClnEnv.getLgiCra().getOQualSvee().getSubnRecAry().length != 0){
				SubnRec[] subnRec = ClnEnv.getLgiCra().getOQualSvee().getSubnRecAry();
				for (int i = 0; i < subnRec.length; i++) {
					if ((subnRec[i].lob.equalsIgnoreCase(SubnRec.LOB_101) ||
							subnRec[i].lob.equalsIgnoreCase(SubnRec.LOB_IOI) ||
							subnRec[i].lob.equalsIgnoreCase(SubnRec.LOB_MOB) ||
							subnRec[i].lob.equalsIgnoreCase(SubnRec.LOB_O2F)) &&
							subnRec[i].srvNum.equalsIgnoreCase(aq.id(R.id.mymob_mobilenum).getText().toString())) {
						DialogHelper.createSimpleDialog(getActivity(), getString(R.string.MYMOB_RC_MM_SUBN_FND));
						return true;
					}
				}
			}
		}
		return false;
	}
	
	private void removeAccount() {
		aq = new AAQuery(myView);
//		isEditMode = true;
		callback_main.setEditMode(true);
		
		final String mob_num = aq.id(R.id.mymob_mobilenum).getText().toString().trim();
		
		OnClickListener onPositiveClickListener =  new AlertDialog.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				if (debug) Log.d("MyMobileActivity", "Delete mob num:" + mob_num);
				// Delete Record in Database
				myMobileAccountHelper = MyMobileAccountHelper.getInstance(getActivity());
				myMobileAccountHelper.delete(mob_num);
				loadList();
			}
		};
		DialogHelper.createSimpleDialog(getActivity(), getString(R.string.myhkt_delete_account_confirm), getString(R.string.btn_confirm), onPositiveClickListener, getString(R.string.MYHKT_BTN_CANCEL));
	}
	
	private final void doVerify() {
//		AuthGrq authgrq;
//		progressDialogHelper.showProgressDialog(me, onstopHandler, true, ProgressDialogHelper.dologin);
//		authgrq = new AuthGrq();
//		authgrq.actionTy = DoVerifyMyMobAsyncTask.actionTy;
//		authgrq.sessTok = sessTok;
//		authgrq.authCra = new AuthCra();
//		authgrq.authCra.setIoSubscriptionRec(getIoSubscriptionRec());
//		authgrq.authCra.setISms(isEditMode ? false:true);
//		
//		if (ClnEnv.isLoggedIn()) {
//			authgrq.authCra.setILoginId(ClnEnv.getSessionLoginID());
//		} else {
//			authgrq.authCra.setILoginId("");
//		}
//		doVerifyMyMobAsyncTask = new DoVerifyMyMobAsyncTask(getApplicationContext(), callbackHandler).execute(authgrq);
		
		////
		addonCra = new AddOnCra();
		addonCra.setISubnRec(callback_main.getIoSubscriptionRec());
//		addonCra.setISms(isEditMode ? false:true);
		addonCra.setISms(!callback_main.isEditMode());
		addonCra.setILoginId(ClnEnv.isLoggedIn() ? ClnEnv.getSessionLoginID() : "");
		APIsManager.doVerify(me, addonCra);
		////
	}
	
	public class AccountListSwipeListViewListener extends BaseSwipeListViewListener {
		@Override
		public void onClickFrontView(int position, View view) {
			super.onClickFrontView(position, view);
			Intent intent = null;
			Bundle bundle = new Bundle();
			// pass button click
//			parentOnClickId = R.id.adapter_mymob_accountlist_front;
//			bundle.putInt("CLICKBUTTON", parentOnClickId);
			// pass whether MyMobile entrance
			if (debug) Log.d("", "pos:" + position);
			// If selected position is a item (ItemViewType = 0,item ;ItemViewType = 1, header)
			if (mobListViewAdapter.getItemViewType(position) == MyMobListViewAdapter.TYPE_ITEM) {
				// If selected item is from MyAccount
				if (mobListViewAdapter.isItemAssocSubnAry(position)) {
					//Click MyHKT Account Event Tracker
					
					// We pass different object according to the source of the array element
//					bundle.putBoolean("ISASSOCARY", true);
//					bundle.putSerializable("SUBNREC", (SubnRec) mobListViewAdapter.getItem(position));
					
					AcctAgent agent = (AcctAgent)mobListViewAdapter.getItem(position);
					if(agent.getSubnRec().inMipMig){
						new AlertDialog.Builder(getActivity())
			              .setMessage(getResString(R.string.M_CSIM_IN_MIG))
			              .setCancelable(false)
			              .setPositiveButton(getResString(R.string.btn_ok), null).create().show();
						return;
					}
					bundle.putSerializable("ACCTAGENT", agent);

					intent = new Intent(getActivity().getApplicationContext(), ServiceActivity.class);

					if (intent != null) {
						intent.putExtras(bundle);
						startActivity(intent);
						getActivity().overridePendingTransition(R.anim.right_slide_in, R.anim.left_slide_out);
					}
				} else {
					//Click Other Mobile Account Event Tracker
					
					AcctAgent acctAgent = (AcctAgent) mobListViewAdapter.getItem(position);
					selectedAcctAgent = acctAgent.copyMe();
					callback_main.setSelectedAcctAgent(selectedAcctAgent);
					// MyMob
					// if password edittext is empty during login prompt dialog
					if (mobListViewAdapter.getAcctAgentList()[position].getPassword().isEmpty()) {
						pwdRequestDialog(acctAgent);
						return;
					}
					
					// localSessTok = "DSFS3123123";
					if (debug) Log.d("mymob onclick", acctAgent.getSrvNum() + "/" + acctAgent.getPassword() + "/" + acctAgent.getLob());
					ioSubscriptionRec = new SubnRec();
					ioSubscriptionRec.srvNum = acctAgent.getSrvNum();
					ioSubscriptionRec.ivr_pwd = acctAgent.getPassword();
					ioSubscriptionRec.lob = acctAgent.getLob();
					callback_main.setIoSubscriptionRec(ioSubscriptionRec);
					authenticate();
				}
			}
		}

		@Override
		public void onClickBackView(int position) {
			SwipeListView mymob_myhkt_list = getActivity().findViewById(R.id.mymob_myhkt_list);
			mymob_myhkt_list.closeOpenedItems();
		}
	}
	
	private void cleanupLoginLayout() {
//		aq.id(R.id.mymob_displayname).getEditText().getText().clear();
//		aq.id(R.id.mymob_mobilenum).getEditText().getText().clear();
//		aq.id(R.id.mymob_password).getEditText().getText().clear();
//		savepassword.setChecked(false);
//		aq.id(R.id.mymob_login_header).text(getResString(R.string.MYMOB_ADD_TITLE));
//		aq.id(R.id.mymob_edit_loginbar).getView().setVisibility(View.GONE);
//		aq.id(R.id.mymob_login_button).getButton().setVisibility(View.VISIBLE);
	}
	
	private void authenticate() {
		AddOnCra authAddonCra = new AddOnCra();
//		authAddonCra = addoncra;
		authAddonCra.setISms(false);
		authAddonCra.setILoginId(ClnEnv.isLoggedIn() ? ClnEnv.getSessionLoginID() : "");
		
		if (callback_main.getIoSubscriptionRec() == null) callback_main.setIoSubscriptionRec(callback_main.getRecallAcctAgent().getSubnRec());

		authAddonCra.setISubnRec(callback_main.getIoSubscriptionRec());
		

		callback_main.setAddOnCra(authAddonCra);
		APIsManager.doAuthen(me, authAddonCra);
	}
	
	
	public void updateAlias(String alias, SubnRec subnRec) {
		// doSvcAso
		SubnRec[] subnRecAry = new SubnRec[ClnEnv.getQualSvee().getSubnRecAry().length];
		SubnRec selectedSubnRec = new SubnRec();
		for (int i = 0; i < ClnEnv.getQualSvee().getSubnRecAry().length; i++) {
			subnRecAry[i] = ClnEnv.getQualSvee().getSubnRecAry()[i].copyMe();
			if (Utils.matchSubnRec(subnRecAry[i], subnRec)) {
				subnRecAry[i].alias = alias;
				selectedSubnRec = subnRecAry[i];
			}
		}
		
		AcMainCra acMainCra = new AcMainCra();
		acMainCra.setIChgPwd(false);
		acMainCra.getISveeRec().pwd = ClnEnv.getSessionPassword();
		acMainCra.setISubnRecAry(subnRecAry);
		acMainCra.setISveeRec(ClnEnv.getQualSvee().getSveeRec().copyMe());
		acMainCra.setILoginId(ClnEnv.getQualSvee().getSveeRec().loginId);
		acMainCra.setISubnRec(selectedSubnRec);
		
		APIsManager.doSvcAso(null, me, acMainCra);
	}
	
	//refresh listview to display new changes
	private void refreshList() {
		try {
			mymob_myhkt_list = getActivity().findViewById(R.id.mymob_myhkt_list);
			try {
				mobListViewAdapter = new MyMobListViewAdapter(this, mymob_myhkt_list);
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			// Set adapter for mymob_listview
			int deviceWidth = getResources().getDisplayMetrics().widthPixels;
			mymob_myhkt_list.setDeviceWidth(deviceWidth);
			mymob_myhkt_list.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
			mymob_myhkt_list.setAdapter(mobListViewAdapter);
//			mymob_myhkt_list.setEmptyView(findViewById(R.id.servicelist_empty));

			mymob_myhkt_list.setSwipeListViewListener( new AccountListSwipeListViewListener());
			mymob_myhkt_list.setSwipeMode(SwipeListView.SWIPE_MODE_LEFT);
			mymob_myhkt_list.setSwipeActionLeft(SwipeListView.SWIPE_ACTION_REVEAL);
			mymob_myhkt_list.setSwipeActionRight(SwipeListView.SWIPE_ACTION_NONE);
			mymob_myhkt_list.setOffsetLeft(deviceWidth * 5 / 6);
			mymob_myhkt_list.setAnimationTime(0);
			mymob_myhkt_list.setSwipeOpenOnLongPress(false);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private void prepareAcctAgent(AddOnCra addoncra) {
		if (addoncra == null) {
		    selectedAcctAgent = new AcctAgent();
			selectedAcctAgent.setAlias(aq.id(R.id.mymob_displayname).getText().toString());
			selectedAcctAgent.setMyMob(true);
			selectedAcctAgent.setSrvNum(aq.id(R.id.mymob_mobilenum).getText().toString());
			selectedAcctAgent.setPassword(aq.id(R.id.mymob_password).getText().toString());
			selectedAcctAgent.setLatest_bill("");
			selectedAcctAgent.setSms(false);
			
			//default cardtype "POSTPAID"
			selectedAcctAgent.setCardType(getResString(R.string.CONST_POSTPAID));
		} else {
		    selectedAcctAgent = new AcctAgent();
			selectedAcctAgent.setAcctNum(addoncra.getOSubnRec().acctNum);
			selectedAcctAgent.setAlias(aq.id(R.id.mymob_displayname).getText().toString());
			selectedAcctAgent.setMyMob(true);
			selectedAcctAgent.setSrvNum(aq.id(R.id.mymob_mobilenum).getText().toString());
			selectedAcctAgent.setPassword(aq.id(R.id.mymob_password).getText().toString());
			selectedAcctAgent.setLob(addoncra.getOSubnRec().lob);
			selectedAcctAgent.setLatest_bill("");
			selectedAcctAgent.setSms(false);
			
			//default cardtype "POSTPAID"
			selectedAcctAgent.setCardType(getResString(R.string.CONST_POSTPAID));
			
		}
		callback_main.setSelectedAcctAgent(selectedAcctAgent);
	}
	
	// mymob login password request dialog
	private void pwdRequestDialog(final AcctAgent acctAgent) {
		// login accept tnc dialog
		final LinearLayout loginLayout = (LinearLayout) getActivity().getLayoutInflater().inflate(R.layout.dialog_cb_etxt, null);
		AlertDialog.Builder builder = new Builder(getActivity());
		final CheckBox dialog_cb_remember = loginLayout.findViewById(R.id.dialog_cb_remember);
		final EditText dialog_etxt_password = loginLayout.findViewById(R.id.dialog_etxt_password);
		
		builder.setView(loginLayout);
		builder.setNegativeButton(getResString(R.string.btn_ok), new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				// hide keyboard
				try {
					InputMethodManager inputManager;
					inputManager = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
					inputManager.hideSoftInputFromWindow(loginLayout.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
					inputManager = null;
				} catch (Exception e) {
					// fail to hide the keyboard
				}

				// set flag and store password if save password checked
				callback_main.setPwdDlgIsSavePwd(dialog_cb_remember.isChecked());
				if (callback_main.isPwdDlgIsSavePwd()) {
					callback_main.setPwdDlgPwd(ClnEnv.getEncString(getActivity(), acctAgent.getSrvNum(), dialog_etxt_password.getText().toString()));
				}
//				if (me.myMobileAccountHelper.isRecordExists(acctAgent.getSrvNum())) {
//					me.myMobileAccountHelper.updateAccountInfo(acctAgent.getAlias(), acctAgent.getSrvNum(), encPassword, "");
//				}
			
				// login without saving password
				ioSubscriptionRec = new SubnRec();
				ioSubscriptionRec.srvNum = acctAgent.getSrvNum();
				ioSubscriptionRec.ivr_pwd = dialog_etxt_password.getText().toString();
				ioSubscriptionRec.lob = acctAgent.getLob();
				callback_main.setIoSubscriptionRec(ioSubscriptionRec);
				
				selectedAcctAgent.setPassword(dialog_etxt_password.getText().toString());
				authenticate();
				return;
			}
		});
		builder.setPositiveButton(getResString(R.string.btn_cancel), new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				// hide keyboard
				try {
					InputMethodManager inputManager;
					inputManager = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
					inputManager.hideSoftInputFromWindow(loginLayout.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
					inputManager = null;
				} catch (Exception e) {
					// fail to hide the keyboard
				}
				return;
			}
		});
		builder.show();
	}
	

	@Override
	public void displayAliasDialog(String message, final int position) {
		// closeOpenedListItems
		if (mymob_myhkt_list != null) {
			if (mymob_myhkt_list.isOpened()) {
				mymob_myhkt_list.closeOpenedItems();
			}
		}
		
		if (alertDialog != null) {
			alertDialog.dismiss();
		}
		
		AlertDialog.Builder builder = new Builder(getActivity());
		builder.setMessage(message);

		// Set an EditText view to get user input
		final EditText input = new EditText(getActivity());
		input.setFilters(new InputFilter[] { new InputFilter.LengthFilter(getResources().getInteger(R.integer.CONST_MAX_ALIAS)) });
		input.setSingleLine();
		input.setText(mobListViewAdapter.getAlias(position).toString());
		// edittext cursor reposition
		Selection.setSelection(input.getText(), input.length());
		builder.setView(input);

		builder.setNegativeButton(getResString(R.string.btn_ok), new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int whichButton) {
				// hide keyboard
		        View view = getActivity().getCurrentFocus();
		        if (view != null) {
		            InputMethodManager inputManager = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
		            inputManager.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
		        }
				
				String value = input.getText().toString();
				SubnRec subnRec = (SubnRec) mobListViewAdapter.getSubnRecItem(position);
				updateAlias(value, subnRec);
			}
		});

		builder.setPositiveButton(getResString(R.string.MYHKT_BTN_CANCEL), new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int whichButton) {
				// hide keyboard
		        View view = getActivity().getCurrentFocus();
		        if (view != null) {
		            InputMethodManager inputManager = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
		            inputManager.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
		        }
		        
			}
		});
		
		

		alertDialog = builder.create();
		alertDialog.show();
	}

	@Override
	public void editAccountInfo(String alias, String mob_num, String password,
			int pos) {
		aq = new AAQuery(myView);
		SwipeListView mymob_myhkt_list = getActivity().findViewById(R.id.mymob_myhkt_list);
		RelativeLayout mymob_login_layout = (RelativeLayout) aq.id(R.id.mymob_login_layout).getView();
		// closeOpenedListItems
		if (mymob_myhkt_list != null) {
			if (mymob_myhkt_list.isOpened()) {
				mymob_myhkt_list.closeOpenedItems();
			}
		}
		
		// Edit mode, set mymob_edit_loginbar visible
//		isEditMode = true;
		callback_main.setEditMode(true);
//		expand(mymob_login_layout); //no longer using , go to add account frag
		
		//pass edit data to add acc frag for editing
		EditCra editcra = new EditCra();
		editcra.setAlias(alias);
		editcra.setMob_num(mob_num);
		editcra.setPassword(password);
		editcra.setPos(pos);
		
		callback_main.setEditCra(editcra);
		
		//go to add acc frag
		callback_main.setActiveSubview(R.string.CONST_SELECTEDFRAG_MYMOB_ADDACC);
		callback_main.displaySubview();
		
		//no longer use, will be using them in add acc frag, remove these code later
//		// Change Button to up arrow
//		aq.id(R.id.mymob_edit_loginbar).getView();
//		aq.id(R.id.mymob_login_header).getTextView().setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.btn_arrowup, 0);
//		aq.id(R.id.mymob_edit_loginbar).getView().setVisibility(View.VISIBLE);
//		aq.id(R.id.mymob_displayname).text(alias);
//		aq.id(R.id.mymob_displayname).getEditText().requestFocus();
//		aq.id(R.id.mymob_mobilenum).getEditText().setEnabled(false);
//		aq.id(R.id.mymob_mobilenum).getEditText().setCursorVisible(false);
//		aq.id(R.id.mymob_mobilenum).getEditText().setFocusable(false);
//		aq.id(R.id.mymob_mobilenum).getEditText().setFocusableInTouchMode(false);
//		aq.id(R.id.mymob_mobilenum).getEditText().setText(mob_num);
//		aq.id(R.id.mymob_login_header).text(R.string.MYMOB_EDIT_TITLE);
//		aq.id(R.id.mymob_password).text(password);
//
//		if (!password.equals("")) {
//			aq.switchButton(R.id.mymob_remember_pw_sb).setChecked(true);
////			viewholder.savepassword.setChecked(true);
//		} else {
//			aq.switchButton(R.id.mymob_remember_pw_sb).setChecked(false);
////			viewholder.savepassword.setChecked(false);
//		}
	}
	
	@Override
	public void onSuccess(APIsResponse response) {
		Intent intent;
		Bundle bundle;
		if (APIsManager.AO_AUTH.equals(response.getActionTy())) {
			AddOnCra authAddonCra = (AddOnCra) response.getCra();
			ClnEnv.setMyMobFlag(true);
			
			String lob = SubnRec.WLOB_XMOB.equalsIgnoreCase(authAddonCra.getOSubnRec().lob) ? SubnRec.LOB_MOB : authAddonCra.getOSubnRec().lob;
			int lobType = Utils.getLobType(lob); 
			SubnRec subnRec = new SubnRec();
			subnRec = authAddonCra.getOSubnRec().copyMe();
			subnRec.ivr_pwd = authAddonCra.getISubnRec().ivr_pwd;

//			subnRec.lob = lob;
			me.selectedAcctAgent.setSubnRec(subnRec);
			me.selectedAcctAgent.setAcctNum(subnRec.acctNum);
			me.selectedAcctAgent.setSrvNum(subnRec.srvNum);
			me.selectedAcctAgent.setLob(lob);
			me.selectedAcctAgent.setLobType(lobType);
			me.selectedAcctAgent.setLive(true);
//			me.selectedAcctAgent.setAcctType(subnRec.acct_ty); //for "T" "V" account type handling
			
			if (callback_main.isPwdDlgIsSavePwd()) {
				// remember password and login
				if (me.myMobileAccountHelper.isRecordExists(me.selectedAcctAgent.getSrvNum())) {
					me.myMobileAccountHelper.updateAccountInfo(me.selectedAcctAgent.getAlias(), me.selectedAcctAgent.getSrvNum(),callback_main.getPwdDlgPwd(), "");
				}
			}
			
			
			bundle = new Bundle();
			bundle.putBoolean("ISMYMOBACCT", true);
			bundle.putSerializable("SUBNREC", subnRec);
			bundle.putSerializable("ACCTAGENT", me.selectedAcctAgent);								

			intent = new Intent(getActivity().getApplicationContext(), ServiceActivity.class);
			intent.putExtras(bundle);
			me.startActivity(intent);
//			getActivity().overridePendingTransition(R.anim.right_slide_in, R.anim.left_slide_out);
			
		} else if (APIsManager.SVCASO.equals(response.getActionTy())) {
			AcMainCra acMainCra = (AcMainCra) response.getCra();
			
			DialogHelper.createSimpleDialog(getActivity(), getString(R.string.RSVM_DONE));
			ClnEnv.getQualSvee().setSveeRec(acMainCra.getOSveeRec());
			ClnEnv.getQualSvee().setSubnRecAry(acMainCra.getOSubnRecAry());
			
			// Reset check RC_ALT error time
//			me.errALT2nd = false;
			refreshList();
		}
	}

	@Override
	public void onFail(APIsResponse response) {
		// General Error Message
		if (!"".equals(response.getMessage()) && response.getMessage() != null) {
			DialogHelper.createSimpleDialog(getActivity(), response.getMessage());
		} else if (RC.TCSESS_MM.equalsIgnoreCase(response.getReply().getCode())) {
			BaseActivity.ivSessDialog();

		} else {
			if (response.getReply().getCode().equals(RC.AO_INDIV_H) || response.getReply().getCode().equals(RC.AO_INDIV_C)) {
//				me.isSavePwd = (isRememberPw) ? true : false; //TODO test here
					callback_main.setSavePwd(false);
					//redirect to login and come back here for add acc retry
					DialogHelper.createSimpleDialog(getActivity(), getString(R.string.MYMOB_RC_MM_H_INDIV), getString(R.string.btn_ok), 
							new OnClickListener() {
								@Override
								public void onClick(DialogInterface dialog, int which) {
									Bundle	rbundle;
									Intent intent;
									dialog.dismiss();
									ClnEnv.clear(getActivity());
									intent = new Intent(getActivity().getApplicationContext(), LoginActivity.class);
									intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
									rbundle = new Bundle();
									rbundle.putString("ACTIONTY", APIsManager.AO_AUTH);
									rbundle.putSerializable("CLICKBUTTON", MAINMENU.MYMOB);
									rbundle.putBoolean("ISSAVEPWD", callback_main.isSavePwd());
									rbundle.putBoolean("ISMYMOBLOGIN", true);
//									prepareAcctAgent(callback_main.getAddOnCra());
									callback_main.getSelectedAcctAgent().setSubnRec(callback_main.getIoSubscriptionRec());
									rbundle.putSerializable("SELECTACCTAGENT", callback_main.getSelectedAcctAgent());
					//				asyncId = null;
									intent.putExtras(rbundle);

									startActivity(intent);
									getActivity().overridePendingTransition(R.anim.left_slide_in, R.anim.right_slide_out);
									getActivity().finish();
								}
							});
				
			} else {
				DialogHelper.createSimpleDialog(getActivity(), ClnEnv.getRPCErrMsg(getActivity(), response.getReply().getCode()));
			}
		}
	}

}
