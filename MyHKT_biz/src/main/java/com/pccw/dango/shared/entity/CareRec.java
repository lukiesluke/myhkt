/*
    CARE Record
    
    Keywords
    --------
    $URL: $
    $Rev: $
    $Date: $
    $Author: $
*/

package com.pccw.dango.shared.entity;

import java.io.Serializable;

import com.pccw.dango.shared.tool.MyTool;
import com.pccw.dango.shared.tool.RC;
import com.pccw.wheat.shared.tool.Alma;
import com.pccw.wheat.shared.tool.MiniRtException;
import com.pccw.wheat.shared.tool.Reply;
import com.pccw.wheat.shared.tool.Tool;

public class CareRec implements Serializable
{
    private static final long serialVersionUID = -10921470812156310L;
    
    public int                      custRid;            /* CUST rid                                      */
    public String                   care;               /* CARE subscription indicator                   */
    public String                   bipt;               /* Bill Protector Subscription Indicator         */
    public int                      hitCnt;             /* Visit Count (before subscribed)               */
    public String                   custNm;             /* Customer Name                                 */ 
    public String                   ageGrp;             /* Age Group (obsoleted)                         */
    public String                   nickNm;             /* Nick Name                                     */
    public String                   formNum;            /* Form Number                                   */
    public String                   chnlCode;           /* Channel Code                                  */
    public String                   ctMail;             /* Contact Email                                 */
    public String                   ctPhone;            /* Contact Phone                                 */
    public String                   dob;                /* Date of Birth                                 */
    public String                   opt4Dm;             /* Opt In/Out for DM                             */
    public String                   lang;               /* Language                                      */
    public int                      nuLtsSubn;          /* Nu of LTS Subscription                        */
    public int                      nuMobSubn;          /* Nu of MOB Subscription                        */
    public int                      nuImsSubn;          /* Nu of IMS Subscription                        */
    public int                      nuTvSubn;           /* Nu of TV  Subscription                        */
    public String                   careUpTs;           /* CARE Update TS                                */
    public String                   biptUpTs;           /* Bill Protector Update TS                      */
    public String                   visitTs;            /* Last Visit TS                                 */
    public String                   createTs;           /* Record Create TS                              */
    public String                   createPsn;          /* Record Create Person                          */
    public String                   lastupdTs;          /* Last Update TS                                */
    public String                   lastupdPsn;         /* Last Update Person                            */
    public int                      rev;                /* Record Revision                               */

    public static final int         L_CARE        = 1;
    public static final int         L_BIPT        = 1;
    public static final int         L_CUST_NM     = 128;
    public static final int         L_AGE_GRP     = 1;
    public static final int         L_NICK_NM     = 20;
    public static final int         L_FORM_NUM    = 20;
    public static final int         L_CHNL_CODE   = 40; /* Refer to CARE.java                            */ 
    public static final int         L_CT_MAIL     = 40;
    public static final int         L_CT_PHONE    = 20;
    public static final int         L_DOB         = 8;
    public static final int         L_OPT4_DM     = 1;
    public static final int         L_LANG        = 2;
    public static final int         L_CARE_UPTS   = 14;
    public static final int         L_BIPT_UPTS   = 14;
    public static final int         L_VISIT_TS    = 14;
    public static final int         L_CREATE_TS   = 14;
    public static final int         L_CREATE_PSN  = 40;
    public static final int         L_LASTUPD_TS  = 14;
    public static final int         L_LASTUPD_PSN = 40;

    /* Applicable to 'care' and 'bipt' fields */
    public static final String      STS_NOT_REG         = "N";
    public static final String      STS_OPT_OUT         = "O";
    public static final String      STS_OPT_OUT_IG      = "G";
    public static final String      STS_OPT_IN          = "I";

    public static final int         MAX_SUBN            = 99;
    
    
    public static void main(String rArg[])
    {
        System.out.println(getVer());
        return;
    }
    
    
    public static String getVer()
    {
        return ("$URL: $, $Rev: $");
    }


    public CareRec()
    {
        initAndClear();
    }

    
    final void initAndClear()
    {
        init();
        clear();
    }

    
    protected void init()
    {
    }


    public void clear()
    {
        custRid         = 0;
        care            = "";
        bipt            = "";
        hitCnt          = 0;
        custNm          = "";
        ageGrp          = "";
        nickNm          = "";
        formNum         = "";
        chnlCode        = "";
        ctMail          = "";
        ctPhone         = "";
        dob             = "";
        opt4Dm          = "";
        lang            = "";
        nuLtsSubn       = 0;
        nuMobSubn       = 0;
        nuImsSubn       = 0;
        nuTvSubn        = 0;
        careUpTs        = "";
        biptUpTs        = "";
        visitTs         = "";
        createTs        = "";
        createPsn       = "";
        lastupdTs       = "";
        lastupdPsn      = "";
        rev             = 0;
    }


    public CareRec copyFrom(CareRec rSrc)
    {
        custRid         = rSrc.custRid;
        care            = rSrc.care;
        bipt            = rSrc.bipt;
        hitCnt          = rSrc.hitCnt;
        custNm          = rSrc.custNm;
        ageGrp          = rSrc.ageGrp;
        nickNm          = rSrc.nickNm;
        formNum         = rSrc.formNum;
        chnlCode        = rSrc.chnlCode;
        ctMail          = rSrc.ctMail;
        ctPhone         = rSrc.ctPhone;
        dob             = rSrc.dob;
        opt4Dm          = rSrc.opt4Dm;
        lang            = rSrc.lang;
        nuLtsSubn       = rSrc.nuLtsSubn;
        nuMobSubn       = rSrc.nuMobSubn;
        nuImsSubn       = rSrc.nuImsSubn;
        nuTvSubn        = rSrc.nuTvSubn;
        careUpTs        = rSrc.careUpTs;
        biptUpTs        = rSrc.biptUpTs;
        visitTs         = rSrc.visitTs;
        createTs        = rSrc.createTs;
        createPsn       = rSrc.createPsn;
        lastupdTs       = rSrc.lastupdTs;
        lastupdPsn      = rSrc.lastupdPsn;
        rev             = rSrc.rev;

        return (this);
    }


    public CareRec copyTo(CareRec rDes)
    {
        rDes.copyFrom(this);
        return (rDes);
    }
    
    
    public CareRec copyMe()
    {
        CareRec                     rDes;

        rDes = new CareRec();
        rDes.copyFrom(this);
        return (rDes);
    }
    
    
    public Reply verifyOptInds()
    {
        Reply                       rRR;
        
        rRR = verifyCare();
        if (rRR.isSucc()) rRR = verifyBipt();

        return (rRR);
    }
    
    
    public Reply verifyAll()
    {
        /*
        	Should only for Opt-In to BIPT
        */
    
        Reply                       rRR;
        
        rRR = verifyCare();
        if (rRR.isSucc()) rRR = verifyBipt();
        if (rRR.isSucc()) rRR = verifyCustNm();
        if (rRR.isSucc()) rRR = verifyAgeGrp();
        if (rRR.isSucc()) rRR = verifyNickNm();
        if (rRR.isSucc()) rRR = verifyFormNum();
        if (rRR.isSucc()) rRR = verifyChnlCode();
        if (rRR.isSucc()) rRR = verifyCtMail();
        if (rRR.isSucc()) rRR = verifyCtPhone();
        if (rRR.isSucc()) rRR = verifyDob();
        if (rRR.isSucc()) rRR = verifyOpt4Dm();
        if (rRR.isSucc()) rRR = verifyLang();
        if (rRR.isSucc()) rRR = verifyNuLtsSubn();
        if (rRR.isSucc()) rRR = verifyNuMobSubn();
        if (rRR.isSucc()) rRR = verifyNuImsSubn();
        if (rRR.isSucc()) rRR = verifyNuTvSubn();
        
        return rRR;
    }
    
    
    public Reply verify4Pending()
    {
        /*
            Should only for Pending
        */
        
        Reply                       rRR;
        
        rRR = verifyVisitTs();
        
        return (rRR);
    }
    
    
    public Reply verifyCare()
    {
        if ((care.length() > L_CARE) ) {
            return (new Reply(RC.CARE_ILCARE));
        }

        if (!Tool.isNil(care)) {
            if (!care.equals(STS_NOT_REG) &&
                !care.equals(STS_OPT_OUT) &&
                !care.equals(STS_OPT_OUT_IG) &&
                !care.equals(STS_OPT_IN))
            return (new Reply(RC.CARE_IVCARE));
        }
        
        return (Reply.getSucc());
    }
    
    
    public Reply verifyBipt()
    {
        if ((bipt.length() > L_BIPT) ) {
            return (new Reply(RC.CARE_ILBIPT));
        }

        if (!Tool.isNil(bipt)) {
            if (!bipt.equals(STS_NOT_REG) &&
                !bipt.equals(STS_OPT_OUT) &&
                !bipt.equals(STS_OPT_OUT_IG) &&
                !bipt.equals(STS_OPT_IN))
            return (new Reply(RC.CARE_IVBIPT));
        }
        
        return (Reply.getSucc());
    }
    
    
    public Reply verifyCustNm()
    {
    	if (!Tool.isASC(custNm, 0, L_CUST_NM)) {
            return (new Reply(RC.CARE_ILCUNM));
        }

        return (Reply.getSucc());
    }
    
    
    public boolean isAllOptIn()
    {
        return (care.equals(CareRec.STS_OPT_IN) && bipt.equals(CareRec.STS_OPT_IN)); 
    }
    
  
    public Reply verifyAgeGrp()
    {
        if ((ageGrp.length() > L_AGE_GRP) ) {
            /* this is obsolete, and should not be filled by System/User */
            throw new MiniRtException("Unexpected, AgeGrp too long!");
        }
        
        return (Reply.getSucc());
    }
    
    
    public Reply verifyNickNm()
    {
        if (nickNm.length() > L_NICK_NM) {
            return (new Reply(RC.CARE_ILNKNM));
        }
        
        return (Reply.getSucc());
    }
    
    
    public Reply verifyFormNum()
    {
        if (!Tool.isASC(formNum, 0, L_FORM_NUM)) {
            return (new Reply(RC.CARE_ILFORM));
        }
        
        return (Reply.getSucc());
    }
    
    
    public Reply verifyChnlCode()
    {
        if (!Tool.isASC(chnlCode, 0, L_CHNL_CODE)) {
            return (new Reply(RC.CARE_ILCHNL));
        }
        
        return (Reply.getSucc());
    }
    
    
    public Reply verifyCtMail()
    {
        String                      rLower;
        
        if (!Tool.isASC(ctMail, SveeRec.MIN_LOGIN_ID_LEN, L_CT_MAIL)) {
            return (new Reply(RC.CARE_ILCTMA));
        }
        
        rLower = ctMail.toLowerCase();

        if (!rLower.equals(ctMail)) {
            return (new Reply(RC.CARE_ICCTMA));
        }

        if (!MyTool.isVaEmail(ctMail)) {
            return (new Reply(RC.CARE_IVCTMA));
        }

        return (Reply.getSucc());
    }
    
    
    public Reply verifyCtPhone()
    {
        if (!Tool.isASC(ctPhone, 8, L_CT_PHONE)) {
            return (new Reply(RC.CARE_ILCTPH));
        }

        return (Reply.getSucc());
    }
    
    
    public Reply verifyDob()
    {
        /* not all Customers provide DOB, hence, Nil DOB is allowed */
        
        if (!Tool.isNil(dob)) {
            if (dob.length() != L_DOB) {
                return (new Reply(RC.CARE_IVDOB));
            }
            
            if (Tool.isNil(Alma.initAlmaWithLowTS(dob))) {
                return (new Reply(RC.CARE_IVDOB));
            }
        }

        return (Reply.getSucc());
    }
    
    
    public Reply verifyOpt4Dm()
    {
        if (!Tool.isInParm(opt4Dm, STS_NOT_REG, STS_OPT_IN, STS_OPT_OUT)) {
            return (new Reply(RC.CARE_IVOPT4DM));
        }
        
        return (Reply.getSucc());
    }
    
    
    public Reply verifyLang()
    {
        if (!Tool.isInParm(lang, SveeRec.LG_EN, SveeRec.LG_ZH)) {
            return (new Reply(RC.CARE_IVLANG));
        }
        
        return (Reply.getSucc());
    }
    
    
    public Reply verifyNuLtsSubn()
    {
        if (nuLtsSubn < 0 || nuLtsSubn > MAX_SUBN) {
            return (new Reply(RC.CARE_IVLTSN));
        }
        
        return (Reply.getSucc());
    }
    
    
    public Reply verifyNuMobSubn()
    {
        if (nuMobSubn < 0 || nuMobSubn > MAX_SUBN) {
            return (new Reply(RC.CARE_IVMOBN));
        }
        
        return (Reply.getSucc());
    }
    
    
    public Reply verifyNuImsSubn()
    {
        if (nuImsSubn < 0 || nuImsSubn > MAX_SUBN) {
            return (new Reply(RC.CARE_IVIMSN));
        }
        
        return (Reply.getSucc());
    }
    
    
    public Reply verifyNuTvSubn()
    {
        if (nuTvSubn < 0 || nuTvSubn > MAX_SUBN) {
            return (new Reply(RC.CARE_IVTVN));
        }
        
        return (Reply.getSucc());
    }
    
    
    public Reply verifyVisitTs()
    {
        if (!Alma.isValid(visitTs)) {
            return (new Reply(RC.CARE_IVVITS));
        }
        
        return (Reply.getSucc());
    }
}
