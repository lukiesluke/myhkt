package com.pccw.myhkt.utils;

import android.content.Context;
import android.util.Log;

import com.pccw.biz.myhkt.R;
import com.pccw.myhkt.APIsManager;
import com.pccw.myhkt.ClnEnv;

import org.apache.http.util.ByteArrayBuffer;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.StandardCharsets;
import java.util.Date;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class DownloadService {
    private final String PATH;
    private final String HOST;
    private static final String IMAGE_DIR_EN = "images/en/";
    private static final String IMAGE_DIR_ZH = "images/zh/";
    private static final String FILE_EXT = ".png";
    public static final String APP_CONFIG = "app_config.json";
    private final String endpoint = "/mba/biz/data/" + APP_CONFIG;
    private String livechat_consumer_flag = "";
    private String livechat_premier_flag = "";
    private String banner_url_en = "";
    private String banner_url_zh = "";
    private static DownloadService downloadService = null;
    private final DownloadServiceListener downloadServiceListener;
    private Context context;

    public DownloadService(Context context, DownloadServiceListener downloadServiceListener) {
        this.context = context;
        PATH = context.getResources().getString(R.string.PATH);
        HOST = ClnEnv.getPref(context, context.getString(R.string.CONST_PREF_FTP), APIsManager.PRD_FTP);
        this.downloadServiceListener = downloadServiceListener;
    }

    public static synchronized DownloadService getInstance(Context context, DownloadServiceListener downloadServiceListener) {
        if (downloadService == null) {
            downloadService = new DownloadService(context, downloadServiceListener);
        }
        return downloadService;
    }

    public interface DownloadServiceListener {
        void onDownloadAppConfig();

        void onDownloadSuccess(String filename);
    }

    public void downloadAppConfig() {
        ExecutorService executor = Executors.newSingleThreadExecutor();
        executor.execute(() -> {
            ClnEnv.setAppConfigDownloaded(false);
            if ("OK".equalsIgnoreCase(DownloadFromUrl(PATH, HOST + endpoint, APP_CONFIG))) {
                ClnEnv.setAppConfigDownloaded(true);
            }
        });
    }

    public void downloadImageBanner() {
        ExecutorService executor = Executors.newSingleThreadExecutor();
        executor.execute(() -> {
            try {
                File file = new File(PATH + APP_CONFIG);
                if (!file.exists()) {
                    if (downloadServiceListener != null) {
                        downloadServiceListener.onDownloadAppConfig();
                    }
                    return;
                }

                JSONObject obj = new JSONObject(loadJson(APP_CONFIG)); // get json file from directory
                //create image directories if they are not exist
                File imgDir = new File(PATH + IMAGE_DIR_EN);
                File zhImgDir = new File(PATH + IMAGE_DIR_ZH);

                if (!imgDir.exists()) imgDir.mkdirs();
                if (!zhImgDir.exists()) zhImgDir.mkdirs();

                JSONArray image_list = obj.getJSONArray("image_list");
                for (int i = 0; i < image_list.length(); i++) {
                    JSONObject imageDetail = image_list.getJSONObject(i);
                    String fileName = imageDetail.getString("key");

                    Log.d("lwg", "image_list.length(): " + image_list.length() + " ++: " + i);
                    Log.d("lwg", "PATH + IMAGE_DIR_EN: " + PATH + IMAGE_DIR_EN);
                    Log.d("lwg", "fileName: " + fileName);

                    Log.d("lwg", "HOST: " + HOST + "/mba/biz/" + imageDetail.getString("en_img"));
                    Log.d("lwg", "HOST: " + HOST + "/mba/biz/" + imageDetail.getString("zh_img"));

                    Log.d("lwg", "imageDetail.getString(\"en_img\"): " + imageDetail.getString("en_img"));
                    Log.d("lwg", "imageDetail.getString(\"zh_img\"): " + imageDetail.getString("zh_img"));
                    // Download Images
                    DownloadFromUrl(PATH + IMAGE_DIR_EN, HOST + "/mba/biz/" + imageDetail.getString("en_img"), fileName + FILE_EXT);
                    DownloadFromUrl(PATH + IMAGE_DIR_ZH, HOST + "/mba/biz/" + imageDetail.getString("zh_img"), fileName + FILE_EXT);
                }

                JSONObject common_config = obj.getJSONObject("common_config");
                livechat_consumer_flag = common_config.getString("livechat_consumer_flag");
                livechat_premier_flag = common_config.getString("livechat_premier_flag");
                banner_url_en = common_config.getString("banner_url_en");
                banner_url_zh = common_config.getString("banner_url_zh");

                ClnEnv.setPref(context.getApplicationContext(), "livechat_consumer_flag", "Y".equalsIgnoreCase(livechat_consumer_flag));
                ClnEnv.setPref(context.getApplicationContext(), "livechat_premier_flag", "Y".equalsIgnoreCase(livechat_premier_flag));
                ClnEnv.setPref(context.getApplicationContext(), "banner_url_en", banner_url_en);
                ClnEnv.setPref(context.getApplicationContext(), "banner_url_zh", banner_url_zh);
            } catch (JSONException e) {
                throw new RuntimeException(e);
            }
        });
    }

    // Downloader method
    public String DownloadFromUrl(String storagePath, String fileURL, String fileName) {
        String status = "OK";
        try {
            URL url = new URL(fileURL);
            File file = new File(storagePath + fileName);
            boolean doDownload = false;
            HttpURLConnection httpCon = (HttpURLConnection) url.openConnection();
            long lastModifiedFile = httpCon.getLastModified();
            Log.e("lwg", "lastModifiedFileName: " + "fileName >> " + fileName + " : " + lastModifiedFile);

            Date serverFileLastModified = new Date(lastModifiedFile);
            if (lastModifiedFile == 0) {
                Log.e("lwg", "No last-modified information.");
                status = "FAILED";
            } else {
                Log.e("lwg", "serverFileLastModified: " + serverFileLastModified);
                if (file.exists()) {
                    Date localFileLastModified = new Date(file.lastModified());
                    Log.e("lwg", "localFileLastModified: " + localFileLastModified);
                    if (localFileLastModified.compareTo(serverFileLastModified) != 0) {
                        doDownload = true;
                    } else {
                        Log.e("lwg", "File :" + fileName + " Same file.");
                    }
                } else {
                    doDownload = true;
                }
            }

            if (doDownload) {
                URLConnection urlConnection = url.openConnection();
                InputStream is = urlConnection.getInputStream();
                BufferedInputStream bis = new BufferedInputStream(is);
                ByteArrayBuffer baf = new ByteArrayBuffer(50);
                int current;
                while ((current = bis.read()) != -1) {
                    baf.append((byte) current);
                }
                FileOutputStream fos = new FileOutputStream(file, false);
                fos.write(baf.toByteArray());
                fos.close();

                Log.d("lwg", "Downloaded file " + fileName + ": from ULR :" + url);
                downloadServiceListener.onDownloadSuccess(fileName);
            }

            return status;
        } catch (IOException e) {
            status = "FAILED";
            return status;
        }
    }

    public String loadJson(String fileName) {
        File file = new File(PATH + fileName);
        String json;
        InputStream in;
        try {
            in = new BufferedInputStream(new FileInputStream(file));
            int size = in.available();
            byte[] buffer = new byte[size];

            in.read(buffer);
            in.close();
            json = new String(buffer, StandardCharsets.UTF_8);
        } catch (IOException e) {
            return null;
        }
        return json;
    }
}
