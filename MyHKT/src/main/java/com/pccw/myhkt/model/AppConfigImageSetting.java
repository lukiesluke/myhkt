package com.pccw.myhkt.model;

public class AppConfigImageSetting extends AppConfigImage {

    private String urlFileNameZh = "";
    private String urlFileNameEn = "";
    private String localFilePathZh = "";
    private String localFilePathEn = "";
    private String localFileName = "";

    // FileName save on the local device
    public String getLocalFileName() {
        return localFileName = getKey().concat(".png");
    }

    /*
            Replace the file value "en_img" : "images/banner_20240531_en.png" from app_config.json
            getEnImg.replace("images/", "") get only the fileName;
            */
    public String getUrlFileNameZh() {
        return urlFileNameZh = getZhImg().replace("images/", "");
    }

    /*
    Set file path to save on the local device storage for zh
    */
    public String getLocalFilePathZh() {
        return localFilePathZh = "/images/zh/";
    }

    /*
    Replace the file value "en_img" : "images/banner_20240531_en.png" from app_config.json
    getEnImg.replace("images/", "") get only the fileName;
    */
    public String getUrlFileNameEn() {
        return urlFileNameEn = getEnImg().replace("images/", "");
    }

    /*
    Set file path to save on the local device storage for en
    */
    public String getLocalFilePathEn() {
        return localFilePathEn = "/images/en/";
    }
}
