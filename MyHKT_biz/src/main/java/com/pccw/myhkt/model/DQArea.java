package com.pccw.myhkt.model;

import java.io.Serializable;

public class DQArea implements Serializable {
	/**
	 * 
	 */
	private static final long	serialVersionUID	= 2954026099270406726L;
	public String				ename;
	public String				cname;
	public String				value;
	public DQDistrict[]			districts;

	public DQArea() {
		ename = new String();
		cname = new String();
		value = new String();
		districts = new DQDistrict[0];
	}

	public String getEname() {
		return ename;
	}

	public void setEname(String ename) {
		this.ename = ename.trim();
	}

	public String getCname() {
		return cname;
	}

	public void setCname(String cname) {
		this.cname = cname.trim();
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value.trim();
	}

	public DQDistrict[] getDistricts() {
		return districts;
	}

	public void setDistricts(DQDistrict[] districts) {
		this.districts = districts;
	}
}