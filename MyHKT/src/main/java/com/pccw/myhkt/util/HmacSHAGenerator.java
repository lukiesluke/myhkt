package com.pccw.myhkt.util;


import android.os.Build;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URISyntaxException;
import java.nio.charset.Charset;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

import kotlin.text.Charsets;



public class HmacSHAGenerator {
    // This variable stores the binary key, which is computed from the string
    // (Base64) key
    private byte[] key;
//    private String charsetName = "UTF-8";

    public HmacSHAGenerator(String keyString) throws IOException {
//        this.key = keyString.getBytes(charsetName);
        this.key = getBytesUtf8(keyString);
    }

    public static byte[] getBytesUtf8(final String string) {
        return getBytes(string, Charsets.UTF_8);
    }

    private static byte[] getBytes(final String string, final Charset charset) {
        if (string == null) {
            return null;
        }
        return string.getBytes(charset);
    }

    public String getSignature(String path)
            throws NoSuchAlgorithmException, InvalidKeyException, UnsupportedEncodingException, URISyntaxException {

        // Retrieve the proper URL components to sign
        String resource = path;

        // Get an HMAC-SHA512 signing key from the raw key bytes
        SecretKeySpec sha512Key = new SecretKeySpec(key, "HmacSHA512");

        // Get an HMAC-SHA512 Mac instance and initialize it with the HMAC-SHA512 key
        Mac mac = Mac.getInstance("HmacSHA512");
        mac.init(sha512Key);

        // compute the binary signature for the request
        byte[] sigBytes = mac.doFinal(resource.getBytes());

        // base 64 encode the binary signature
//      String signature = Base64.encodeBase64String(sigBytes);
//        String signature = Base64.encodeBase64URLSafeString(sigBytes);
//		signature = URLEncoder.encode(signature, "UTF-8");
//
//        Base64 base64 = new Base64(0, (byte[])null, true);
        //String signature = base64.encodeToString(sigBytes);
        // String signature = Base64.getEncoder().encodeToString(sigBytes);
        String signature = "";
        if(Build.VERSION.SDK_INT <= Build.VERSION_CODES.N_MR1) {
            signature = android.util.Base64.encodeToString(sigBytes, android.util.Base64.URL_SAFE ^ android.util.Base64.NO_WRAP);
        } else {
            signature = Base64.getUrlEncoder().encodeToString(sigBytes);
        }
        return signature;
    }
}
