package com.pccw.myhkt.activity;

import android.content.Context;
import android.content.SharedPreferences;
import androidx.test.espresso.intent.Intents;
import androidx.test.rule.ActivityTestRule;
import androidx.test.rule.GrantPermissionRule;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import com.pccw.myhkt.AccountHelper;
import com.pccw.myhkt.R;
import com.pccw.myhkt.base.BaseTest;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static androidx.test.platform.app.InstrumentationRegistry.getInstrumentation;
import static androidx.test.espresso.Espresso.onData;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.matcher.RootMatchers.isDialog;
import static androidx.test.espresso.matcher.RootMatchers.withDecorView;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static org.hamcrest.CoreMatchers.anything;

@Ignore
@RunWith(AndroidJUnit4.class)
public class RebootModemStartTest extends BaseTest {
    private Context context;
    SharedPreferences shared;


    @Rule
    public ActivityTestRule<MainMenuActivity> activityTestRule = new ActivityTestRule<>(MainMenuActivity.class);
    @Rule
    public GrantPermissionRule permissionRule = GrantPermissionRule.grant(android.Manifest.permission.WRITE_EXTERNAL_STORAGE);

    @Before
    public void setUp(){
        context = getInstrumentation().getTargetContext();
        shared = context.getSharedPreferences(
                "myhkt",
                Context.MODE_PRIVATE);
        Intents.init();
        clickLeftNavBar();
        findStringContaining(context, R.string.myhkt_LGIF_LOGIN).perform(click());
        loginMyCredentialsPRD(AccountHelper.getPRDAccount3().getUsername(),
                AccountHelper.getPRDAccount3().getPassword(), shared);
        gotoMainMenuItem(1);
    }

    @After
    public void tearDown() {
        clickLeftNavBar();
        executeLogout(context);
        Intents.release();
    }

    @Test
    public void testRebootModemNetvigatorDisplayUI() {
        clickViewInAdapter(R.id.servicelist_listView, 1);
        delay(10000);
        clickViewInAdapter(R.id.hkt_indicator_gridview1, 1);
        findStringContaining("yck008");
        findStringContaining(context, R.string.reboot_description_pcd);
        findStringContaining(context, R.string.reboot_start_btn);
        checkLiveChatBtnIsDisplayed();
    }

    @Test
    public void testRebootModemNowTVDisplayUI() {
        clickViewInAdapter(R.id.servicelist_listView, 2);
        delay(10000);
        clickViewInAdapter(R.id.hkt_indicator_gridview1, 1);
        findStringContaining("1111713434");
        findStringContaining(context, R.string.reboot_description_tv);
        findStringContaining(context, R.string.reboot_start_btn);
        checkLiveChatBtnIsDisplayed();
    }

    @Test
    public void testRebootModemNetvigatorLiveChat() {
        clickViewInAdapter(R.id.servicelist_listView, 1);
        delay(10000);
        clickViewInAdapter(R.id.hkt_indicator_gridview1, 1);
        checkLiveChat(context, R.string.LIVE_CHAT_DISCLAIMER, R.id.navbar_button_left);
    }

    @Test
    public void testRebootModemNowTVLiveChat() {
        clickViewInAdapter(R.id.servicelist_listView, 2);
        delay(10000);
        clickViewInAdapter(R.id.hkt_indicator_gridview1, 1);
        checkLiveChat(context, R.string.LIVE_CHAT_DISCLAIMER, R.id.navbar_button_left);
    }

    @Test
    public void testRebootModemStartToReboot() {
        clickViewInAdapter(R.id.servicelist_listView, 1);
        delay(10000);
        clickViewInAdapter(R.id.hkt_indicator_gridview1, 1);
//        findComponentByID(R.id.linetest_st_btn)..perform(click());
        findStringContaining(context, R.string.reboot_start_btn).perform(click());
        delay(10000);
        delay(120000);
    }

}