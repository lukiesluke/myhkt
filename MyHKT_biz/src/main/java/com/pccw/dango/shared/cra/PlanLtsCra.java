/*
    Crate for PLan (LTS)
    
    Keywords
    --------
    $URL: $
    $Rev: $
    $Date: $
    $Author: $
*/

package com.pccw.dango.shared.cra;

import java.io.Serializable;

import com.pccw.dango.shared.entity.SrvPlan;
import com.pccw.dango.shared.entity.SubnRec;

public class PlanLtsCra extends BaseCraEx implements Serializable
{
    private static final long serialVersionUID = 4710037647397132657L;
    
    private String                  iLoginId;           /* Login ID                                      */
    private SubnRec                 iSubnRec;           /* Subscription Record                           */
    
    private SrvPlan[]               oTermPlanAry;       /* Array of Term Plan                            */
    private SrvPlan[]               oCallPlanAry;       /* Array of Call Plan                            */
    private String                  oIAdr;              /* IA                                            */


    public static void main(String rArg[])
    {
        System.out.println(getVer());
        return;
    }
    
    
    public static String getVer()
    {
        return ("$URL: $, $Rev: $");
    }

    
    public PlanLtsCra()
    {
        initAndClear();
    }


    protected void init()
    {
        super.init();
    }


    public void clear()
    {
        super.clear();
        
        clearILoginId();
        clearISubnRec();
        clearOTermPlanAry();
        clearOCallPlanAry();
        clearOIAdr();
    }


    public PlanLtsCra copyFrom(PlanLtsCra rSrc)
    {
        super.copyFrom(rSrc);
        
        setILoginId(rSrc.getILoginId());
        setISubnRec(rSrc.getISubnRec());
        setOTermPlanAry(rSrc.getOTermPlanAry());
        setOCallPlanAry(rSrc.getOCallPlanAry());
        setOIAdr(rSrc.getOIAdr());

        return (this);
    }


    public PlanLtsCra copyTo(PlanLtsCra rDes)
    {
        rDes.copyFrom(this);
        return (rDes);
    }
    
    
    public PlanLtsCra copyMe()
    {
        PlanLtsCra                  rDes;

        rDes = new PlanLtsCra();
        rDes.copyFrom(this);
        return (rDes);
    }


    public void clearILoginId()
    {
        iLoginId = "";
    }


    public void setILoginId(String rArg)
    {
        iLoginId = rArg;
    }


    public String getILoginId()
    {
        return (iLoginId);
    }


    public void clearISubnRec()
    {
        iSubnRec = new SubnRec();
    }


    public void setISubnRec(SubnRec rArg)
    {
        iSubnRec = rArg;
    }


    public SubnRec getISubnRec()
    {
        return (iSubnRec);
    }


    public void clearOTermPlanAry()
    {
        oTermPlanAry = new SrvPlan[0];
    }


    public void setOTermPlanAry(SrvPlan[] rArg)
    {
        oTermPlanAry = rArg;
    }


    public SrvPlan[] getOTermPlanAry()
    {
        return (oTermPlanAry);
    }


    public void clearOCallPlanAry()
    {
        oCallPlanAry = new SrvPlan[0];
    }


    public void setOCallPlanAry(SrvPlan[] rArg)
    {
        oCallPlanAry = rArg;
    }


    public SrvPlan[] getOCallPlanAry()
    {
        return (oCallPlanAry);
    }


    public void clearOIAdr()
    {
        oIAdr = "";
    }


    public void setOIAdr(String rArg)
    {
        oIAdr = rArg;
    }


    public String getOIAdr()
    {
        return (oIAdr);
    }
}
