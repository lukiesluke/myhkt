package com.pccw.myhkt.activity;

import android.content.Context;
import android.content.SharedPreferences;
import androidx.test.platform.app.InstrumentationRegistry;
import androidx.test.espresso.intent.Intents;
import androidx.test.rule.ActivityTestRule;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import com.pccw.myhkt.AccountHelper;
import com.pccw.myhkt.R;
import com.pccw.myhkt.base.BaseTest;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static androidx.test.platform.app.InstrumentationRegistry.getInstrumentation;
import static androidx.test.espresso.action.ViewActions.click;

@Ignore
@RunWith(AndroidJUnit4.class)
public class ServicePlanMOBTest extends BaseTest {

    private Context context;

    private SharedPreferences shared;
    SharedPreferences.Editor prefEditor;


    @Rule
    public ActivityTestRule<MainMenuActivity> activityTestRule =
            new ActivityTestRule<MainMenuActivity>(MainMenuActivity.class) {
                @Override
                protected void beforeActivityLaunched() {
                    clearSharedPrefs(InstrumentationRegistry.getTargetContext());
                    InstrumentationRegistry.getTargetContext().deleteDatabase("myhkt.db");
                    super.beforeActivityLaunched();
                }
            };

    private void clearSharedPrefs(Context context) {
        SharedPreferences prefs =
                context.getSharedPreferences("myhkt", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.clear();
        editor.commit();
    }

    @Before
    public void setUp() {
        context = getInstrumentation().getTargetContext();
        shared = context.getSharedPreferences(
                "myhkt",
                Context.MODE_PRIVATE);
//        Intents.init();
        clickLeftNavBar();
        findStringContaining(context, R.string.myhkt_LGIF_LOGIN).perform(click());
        loginGreatChina2();
        clickViewInAdapter(R.id.servicelist_listView, 2);
        delay(10000);
        clickViewInAdapter(R.id.hkt_indicator_gridview1, 1);
        delay(15000);
    }

    @After
    public void tearDown() {
        executeLogout(context);
//        Intents.release();
    }

    @Test
    public void servicePlanSwipeLeftNextTest() {
        swipeLeftView(R.id.fragment_usagedata_layout);
    }

    @Test
    public void servicePlanTopUpTest() {
//        clickView(R.id.table_cell_btn);
        clickString(context, R.string.usage_boost_up1);
        delay(20000);
    }

    private void loginGreatChina2() {
        loginMyCredentialsPRD(AccountHelper.getPRDAccount1().getUsername(), AccountHelper.getPRDAccount1().getPassword(), shared);
        delay(3000);
        gotoMainMenuItem(0);
    }
}
