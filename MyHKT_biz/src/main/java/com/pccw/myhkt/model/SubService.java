package com.pccw.myhkt.model;

import com.pccw.dango.shared.entity.SubnRec;
import com.pccw.biz.myhkt.R;

/**
 * 
 * 14/12/2017	Abdulmoiz Esmail	-Updated the getResId in line with Commercial App
 *
 */
public class SubService {

	public SubService(int imageRes, String lob) {
		super();
		this.imageRes = imageRes;
		this.lob = lob;
	}
	public int getImageRes() {
		return imageRes;
	}
	public void setImageRes(int imageRes) {
		this.imageRes = imageRes;
	}
	public String getLob() {
		return lob;
	}
	public void setLob(String lob) {
		this.lob = lob;
	}
	private int imageRes;
	private String lob;
	
	public static final int getResId(String lob) {
		
		if (lob.equals(SubnRec.LOB_LTS)) { return R.drawable.lob_lts_tel;}
		if (lob.equals(SubnRec.LOB_IMS)) { return R.drawable.lob_ims;}
		if (lob.equals(SubnRec.LOB_PCD)) { return R.drawable.lob_pcd;}
		if (lob.equals(SubnRec.LOB_LTS+SubnRec.TOS_LTS_OCM)) { return R.drawable.lob_lts_ocm;}
		if (lob.equals(SubnRec.LOB_TV)) { return R.drawable.lob_tv_plain;}
		return -1;
	}
	
	public static final String getName(String lob) {
//		if (lob.equals(SubnRec.LOB_NE)) { return "NE";}
//		if (lob.equals(SubnRec.LOB_LTS)) { return R.drawable.logo_fixedline;}
//		if (lob.equals(SubnRec.LOB_TV)) { return R.drawable.logo_now;}
//		if (lob.equals(SubnRec.LOB_101)) { return R.drawable.logo_1010;}
//		if (lob.equals(SubnRec.LOB_MOB)) { return R.drawable.logo_csl;}
//		return -1;
		return lob;
	}
	
	
}
