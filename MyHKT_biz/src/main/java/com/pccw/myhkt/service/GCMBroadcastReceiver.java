package com.pccw.myhkt.service;

/************************************************************************
File       : GCMBroadcastReceiver.java
Desc       : Google Cloud Message Receiver for Push Notification
Name       : GCMIntentService
Created by : Derek Tsui
Date       : 11/03/2014

Change History:
Date       Modified By			Description
---------- ----------------	-------------------------------
11/03/2014 Derek Tsui			-From Google API
*************************************************************************/

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import androidx.legacy.content.WakefulBroadcastReceiver;

import com.pccw.myhkt.service.GCMIntentService;

public class GCMBroadcastReceiver extends WakefulBroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        // Explicitly specify that GcmIntentService will handle the intent.
        ComponentName comp = new ComponentName(context.getPackageName(),
                GCMIntentService.class.getName());
        // Start the service, keeping the device awake while it is launching.
        startWakefulService(context, (intent.setComponent(comp)));
        setResultCode(Activity.RESULT_OK);
    }
}
