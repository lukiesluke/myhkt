package com.pccw.myhkt.fragment;

import android.app.Activity;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.NonNull;

import com.pccw.dango.shared.cra.AcMainCra;
import com.pccw.dango.shared.entity.SveeRec;
import com.pccw.dango.shared.tool.RC;
import com.pccw.myhkt.APIsManager;
import com.pccw.myhkt.APIsResponse;
import com.pccw.myhkt.ClnEnv;
import com.pccw.myhkt.DialogHelper;
import com.pccw.myhkt.InterpretRCManager;
import com.pccw.myhkt.R;
import com.pccw.myhkt.Utils;
import com.pccw.myhkt.activity.BaseActivity;
import com.pccw.myhkt.fragment.MyProfLoginMainFragment.OnMyProfLoginListener;
import com.pccw.myhkt.lib.ui.AAQuery;
import com.pccw.myhkt.lib.ui.RegInputItem;
import com.pccw.myhkt.util.Constant;
import com.pccw.wheat.shared.tool.Reply;


public class ChangePwdFragment extends BaseFragment {
    private ChangePwdFragment me;
    private AAQuery aq;
    private boolean isReinitialize = false;

    private SveeRec sveeRec = null;

    // CallBack
    private OnMyProfLoginListener callback;

    private RegInputItem inputCurrentPw;
    private RegInputItem inputNewPw;
    private RegInputItem inputNewRePw;

    private Button cancelBtn;
    private Button updateBtn;

    private String currentPwInput;
    private String newPwInput;
    private String newRePwInput;

    private String TAG = "ChangePwdFragment";

    @Override
    public void onAttach(@NonNull Activity activity) {
        super.onAttach(activity);
        try {
            callback = (OnMyProfLoginListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement OnMyProfLoginListener");
        }
    }

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        me = this;
        View fragmentLayout = inflater.inflate(R.layout.fragment_myproflogin_changepw, container, false);
        initData();

        return fragmentLayout;
    }

    protected void initData() {
        sveeRec = callback.getSveeRec();
        super.initData();
    }

    protected void initUI() {
        super.initUI();
        if (isReinitialize) {

            currentPwInput = sveeRec != null ? sveeRec.nickname : "";
            newPwInput = sveeRec != null ? sveeRec.ctMail : "";
            newRePwInput = sveeRec != null ? sveeRec.ctMob : "";

            aq = new AAQuery(getActivity());
            //scrollview
            aq.marginpx(R.id.myproflogin_changepw_scrollView, basePadding, 0, basePadding, 0);

            //header
            aq.id(R.id.myproflogin_changepw_header).margin(0, 0, 0, 0);
            aq.id(R.id.myproflogin_changepw_header).getTextView().setText(getResources().getString(R.string.myhkt_myprof_chgpwd));
            aq.id(R.id.myproflogin_changepw_header).getTextView().setTypeface(Typeface.MONOSPACE, Typeface.BOLD);
            aq.id(R.id.myproflogin_changepw_header).textSize(getResources().getInteger(R.integer.textsize_default_int));

            //edit text
            inputCurrentPw = (RegInputItem) aq.id(R.id.myproflogin_changepw_name_et).getView();
            inputCurrentPw.initViews(getActivity(), getResources().getString(R.string.myhkt_myprof_curpwd), "", "", InputType.TYPE_TEXT_VARIATION_PASSWORD);
            inputCurrentPw.setPadding(0, 0, 0, 10);
            inputCurrentPw.setMaxLength(16);

            inputNewPw = (RegInputItem) aq.id(R.id.myproflogin_changepw_email_et).getView();
            inputNewPw.initViews(getActivity(), getResources().getString(R.string.myhkt_myprof_newpwd), getResources().getString(R.string.REGF_PWD_HINT), "", InputType.TYPE_TEXT_VARIATION_PASSWORD);
            inputNewPw.setPadding(0, 0, 0, 10);
            inputNewPw.setMaxLength(16);

            inputNewRePw = (RegInputItem) aq.id(R.id.myproflogin_changepw_mobnum_et).getView();
            inputNewRePw.initViews(getActivity(), getResources().getString(R.string.myhkt_myprof_retpwd), "", "", InputType.TYPE_TEXT_VARIATION_PASSWORD);
            inputNewRePw.setPadding(0, 0, 0, 10);
            inputNewRePw.setMaxLength(16);

            //button layout
            aq.norm2TxtBtns(R.id.myproflogin_changepw_btn_cancel, R.id.myproflogin_changepw_btn_update, getResString(R.string.MYHKT_BTN_CANCEL), getResString(R.string.MYHKT_BTN_UPDATE));
            aq.id(R.id.myproflogin_changepw_btn_update).clicked(this, "onClick");
            aq.id(R.id.myproflogin_changepw_btn_cancel).clicked(this, "onClick");
        }

//		prepareAccData();
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.myproflogin_changepw_btn_cancel:
                ((MyProfLoginMainFragment) getParentFragment()).openLoginidSubFrag();
                break;
            case R.id.myproflogin_changepw_btn_update:
                Utils.closeSoftKeyboard(getActivity(), v);

                currentPwInput = inputCurrentPw.getInput();
                newPwInput = inputNewPw.getInput();
                newRePwInput = inputNewRePw.getInput();

                AcMainCra acMainCra = new AcMainCra();

//			SveeRec mSveeRec = ClnEnv.getQualSvee().getSveeRec().copyMe();
                SveeRec mSveeRec = sveeRec.copyMe();
//			acMainCra.setISveeRec(mSveeRec);
                acMainCra.setILoginId(ClnEnv.getQualSvee().getSveeRec().loginId);
                acMainCra.setIChgPwd(true);
                mSveeRec.pwd = newPwInput;
                mSveeRec.lang = ClnEnv.getAppLocale(getActivity().getBaseContext());
                acMainCra.setISveeRec(mSveeRec);
                acMainCra.setIOrigPwd(currentPwInput);

                SveeRec currPwCheck = sveeRec.copyMe(); //for current password checking
                currPwCheck.pwd = currentPwInput;
                Reply reply = currPwCheck.verifyPwd(); //verify current pwd
                if (newPwInput.length() < 6 || newPwInput.length() > 16) {
                    // JARLOU: Previous error code PAMM_IVCURRPWD
                    DialogHelper.createSimpleDialog(getActivity(), getString(R.string.PAMM_NAPWD));
                    return;
                }
                reply = mSveeRec.verifyPwd(); //verify new pwd
                if (!reply.isSucc()) {
                    DialogHelper.createSimpleDialog(getActivity(), InterpretRCManager.interpretRC_ChangePassword(getActivity(), reply));
                    return;
                }

                //Check if the retype pwd = new pwd
                if (!newPwInput.equals(newRePwInput)) {
                    // Display error message PAMM_IVCFMPWD
                    displayDialog(getResString(R.string.PAMM_IVCFMPWD));
                    return;
                }
                acMainCra.setISveeRec(mSveeRec);
                APIsManager.doChangePwd(me, acMainCra);
                break;
        }
    }

    @Override
    public void onSuccess(APIsResponse response) {
        displayDialog(getResString(R.string.PAMM_DONE));
        AcMainCra acMainCra = (AcMainCra) response.getCra();
        //store new Svee
        ClnEnv.getQualSvee().setSveeRec(acMainCra.getOSveeRec());
        callback.setSveeRec(acMainCra.getOSveeRec());
        //update session password
        //Moiz: 21/11/17 : clear the session password after change password

        ClnEnv.setSessionPassword(newPwInput);

        //Moiz: 21/11/17 Save password after reset if the Touch Id login is activated.
        if (Build.VERSION.SDK_INT >= 23 && Utils.isTouchIDLoginActivated(getActivity())) {
            //If the login user (Temp User) is equal to the Touch Id login id, update both temp and default touch Id password
            if (Utils.isTouchIdUserEqualsToLoggedInUser(getActivity())) {
                Utils.saveTempUserCredentials(getActivity(), sveeRec.loginId, inputNewPw.getInput());
                Utils.saveTempUserCredentialsAsTouchIdDefault(getActivity());
            } else {
                //If the Touch Id is not current login user (Temp User), update the default Touch User's password
                String touchIdLoginId = ClnEnv.getPref(getActivity(), Constant.TOUCH_ID_USER_ID_DEFAULT, "");
                if (sveeRec.loginId.equals(touchIdLoginId)) {
                    ClnEnv.setEncPref(getActivity(), touchIdLoginId, Constant.TOUCH_ID_PWD_DEFAULT, newPwInput);
                }
            }
        } else {
            //rewrite saved password
            Utils.saveTempUserCredentials(getActivity(), sveeRec.loginId, newPwInput);
            ClnEnv.setEncPref(getActivity().getApplicationContext(), sveeRec.loginId, getActivity().getString(R.string.CONST_PREF_PASSWORD), newPwInput);
        }
        ((MyProfLoginMainFragment) getParentFragment()).openLoginidSubFrag();
    }

    @Override
    public void onFail(APIsResponse response) {
        // General Error Message
        if (!"".equals(response.getMessage()) && response.getMessage() != null) {
            DialogHelper.createSimpleDialog(getActivity(), response.getMessage());
        } else if (RC.TCSESS_MM.equalsIgnoreCase(response.getReply().getCode())) {
            BaseActivity.ivSessDialog();
        } else {
            DialogHelper.createSimpleDialog(getActivity(), InterpretRCManager.interpretRC_AcMainMdu(getActivity(), response.getReply()));
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        isReinitialize = false;
    }

    @Override
    public void onStop() {
        super.onStop();
        isReinitialize = false;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        isReinitialize = true;
    }
}
