package com.pccw.myhkt.activity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.pccw.myhkt.ClnEnv;
import com.pccw.myhkt.FAWrapper;
import com.pccw.myhkt.R;
import com.pccw.myhkt.lib.ui.AAQuery;
import com.pccw.myhkt.lib.ui.HKTButton;
import com.pccw.myhkt.util.Constant;

/************************************************************************
 * File : TouchIdVerificationFailedActivity.java
 * Desc : Touch Id Login Activation Complete is the page shown when Touch ID login is failed
 * Name : TouchIdVerificationFailedActivity
 * by : Abdulmoiz Esmail
 * Date : 02/11/2017
 *
 * Change History:
 * Date       Modified By		Description
 * ---------- ----------------	-------------------------------
 * 02/11/2017 Abdulmoiz Esmail  - First Draft
 *************************************************************************/
public class TouchIdVerificationFailedActivity extends Activity {

    HKTButton mRetryButton;
    Button mLiveChatButton;
    HKTButton mOkBUtton; 
    ImageView mFailIcon;
    TextView mTitle;
    TextView mMessage;
    TextView mLockMessageLiveChat;
    private boolean retry;


    private AAQuery aq;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_touch_id_verification_failed);

        aq = new AAQuery(this);

        mRetryButton = (HKTButton) aq.id(R.id.btn_retry).getView();
        mRetryButton.initViews(this, getResources().getString(R.string.fp_failure_btn_retry), LayoutParams.MATCH_PARENT);

        mLiveChatButton = (Button) aq.id(R.id.btn_live_chat).getView();
        //mLiveChatButton.initViews(this, getResources().getString(R.string.touch_id_lock_mode_live_chat), LayoutParams.MATCH_PARENT);

        mOkBUtton = (HKTButton) aq.id(R.id.btn_ok).getView();
        mOkBUtton.initViews(this, getResources().getString(R.string.fp_failure_lock_btn_confirm), LayoutParams.WRAP_CONTENT);
        aq.padding(R.id.btn_ok, 180, 0, 180, 0);

        mLockMessageLiveChat = (TextView) findViewById(R.id.live_chat_message);
        mMessage = (TextView) findViewById(R.id.message);


        mFailIcon = (ImageView) findViewById(R.id.image_view_fail_icon);
        mTitle = (TextView) findViewById(R.id.title);
        Typeface face = Typeface.createFromAsset(getAssets(),
                "Roboto-Light.ttf");
        mTitle.setTypeface(face);

        ClnEnv.setPref(this, Constant.TOUCH_ID_FAILURE_COUNT, 0); //Clear failure count
        
        this.retry = getIntent().getBooleanExtra(Constant.TOUCH_ID_RETRY_COUNT, false);//ClnEnv.getPref(this, Constant.TOUCH_ID_RETRY_COUNT, 0);

        if(!this.retry) {
            mRetryButton.setVisibility(View.VISIBLE);
            mLiveChatButton.setVisibility(View.GONE);
            mOkBUtton.setVisibility(View.GONE);
            FAWrapper.getInstance().sendFAScreen(this, R.string.CONST_SCRN_FP_FAILURE, false);;
        } else {
            mTitle.setText(getString(R.string.fp_failure_lock_top_title));
            mMessage.setText(getString(R.string.fp_failure_lock_title));
            mLockMessageLiveChat.setVisibility(View.VISIBLE);
            mFailIcon.setImageDrawable(getResources().getDrawable(R.drawable.ic_fingerprint_icon_locked));
            mRetryButton.setVisibility(View.GONE);
            mLiveChatButton.setVisibility(View.GONE);
            FAWrapper.getInstance().sendFAScreen(this, R.string.CONST_SCRN_FP_LOCK, false);
        }


    }
    


    public void onRetryButtonClicked(View view) {
    	
    	Intent intent = new Intent();
        intent.putExtra(Constant.IS_TO_SHOW_LIVE_CHAT, false);
        setResult(Activity.RESULT_OK, intent);
        finish();
        overridePendingTransition(R.anim.left_slide_in, R.anim.right_slide_out);
    }

    public void onOkyButtonClicked(View view) {
        setResult(Activity.RESULT_CANCELED);
        finish();
        overridePendingTransition(R.anim.left_slide_in, R.anim.right_slide_out);
    }

    public void onLiveChatButtonClicked(View view) {

        Intent intent = new Intent();
        intent.putExtra(Constant.IS_TO_SHOW_LIVE_CHAT, true);
        setResult(Activity.RESULT_OK, intent);
        finish();
        overridePendingTransition(R.anim.left_slide_in, R.anim.right_slide_out);
    }

    @Override
    public void onResume() {
        super.onResume();
        if(!this.retry) {
            FAWrapper.getInstance().sendFAScreen(this, R.string.CONST_SCRN_FP_FAILURE, false);;
        } else {
            FAWrapper.getInstance().sendFAScreen(this, R.string.CONST_SCRN_FP_LOCK, false);
        }
    }
    
    @Override
    public void onBackPressed() {
    	finish();
        overridePendingTransition(R.anim.left_slide_in, R.anim.right_slide_out);
    	super.onBackPressed();
    }
}
