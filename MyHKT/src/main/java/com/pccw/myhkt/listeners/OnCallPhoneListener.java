package com.pccw.myhkt.listeners;

public interface OnCallPhoneListener {
    void onCallPhoneNumber(String phoneNum);
}
