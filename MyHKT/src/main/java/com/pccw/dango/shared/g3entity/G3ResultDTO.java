package com.pccw.dango.shared.g3entity;

import java.io.Serializable;


public class G3ResultDTO implements Serializable 
{
    private static final long serialVersionUID = -3482671528773314783L;
    
    private String resultRefCode;
    private String errMsg;
    private String errCode;
    
	public String getResultRefCode() {
		return resultRefCode;
	}
	public void setResultRefCode(String resultRefCode) {
		this.resultRefCode = resultRefCode;
	}
	public String getErrMsg() {
		return errMsg;
	}
	public void setErrMsg(String errMsg) {
		this.errMsg = errMsg;
	}
	public String getErrCode() {
		return errCode;
	}
	public void setErrCode(String errCode) {
		this.errCode = errCode;
	}

    
}
