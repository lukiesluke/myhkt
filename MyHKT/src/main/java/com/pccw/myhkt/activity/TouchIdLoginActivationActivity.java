package com.pccw.myhkt.activity;

import android.content.Intent;
import android.os.Bundle;
import android.text.InputType;
import android.view.View;
import android.view.ViewGroup.LayoutParams;

import com.pccw.dango.shared.cra.LgiCra;
import com.pccw.dango.shared.entity.SveeRec;
import com.pccw.myhkt.APIsManager;
import com.pccw.myhkt.APIsResponse;
import com.pccw.myhkt.APIsManager.OnAPIsListener;
import com.pccw.myhkt.ClnEnv;
import com.pccw.myhkt.DialogHelper;
import com.pccw.myhkt.FAWrapper;
import com.pccw.myhkt.R;
import com.pccw.myhkt.Utils;
import com.pccw.myhkt.lib.ui.AAQuery;
import com.pccw.myhkt.lib.ui.HKTButton;
import com.pccw.myhkt.lib.ui.RegInputItem;
import com.pccw.myhkt.touchId.OnTouchIdActivityListener;
import com.pccw.myhkt.touchId.TouchIDClickListener;
import com.pccw.myhkt.util.Constant;

import java.security.KeyStore;

import javax.crypto.KeyGenerator;

/************************************************************************
 * File : TouchIdLoginActivationActivity.java 
 * Desc : Touch Id Login Activation contains the activation of Touch Id for logged in user 
 * Name : TouchIdLoginActivationActivity
 * by : Abdulmoiz Esmail 
 * Date : 19/10/2017
 * 
 * Change History:
 * Date       Modified By		Description
 * ---------- ----------------	-------------------------------
 * 19/10/2017 Abdulmoiz Esmail  - First Draft
 * 04/12/2017 Abdulmoiz Esmail  - Update tapping T&C, update link
 *************************************************************************/

public class TouchIdLoginActivationActivity extends BaseActivity implements OnTouchIdActivityListener, OnAPIsListener{

    public static final String TAG = TouchIdLoginActivationActivity.class.getSimpleName();

    private static final String KEY_NAME_NOT_INVALIDATED = "key_not_invalidated";
    
    private String domainURL = APIsManager.PRD_FTP;

    private AAQuery aq;

    private KeyStore mKeyStore;
    private KeyGenerator mKeyGenerator;

    private HKTButton mActivateButton;
    private RegInputItem regInputPw;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_touch_id_login_activation);
        
        isLiveChatShown = false;
        
        domainURL = ClnEnv.getPref(me, me.getString(R.string.CONST_PREF_FTP), APIsManager.PRD_FTP);
        ClnEnv.setPref(this, Constant.TOUCH_ID_FAILURE_COUNT, 0); //Clear failure count
    }
    
    @Override
    protected void onStart() {
    	super.onStart();
    }
    
    @Override
    protected void onPostResume() {
    	// TODO Auto-generated method stub
    	super.onPostResume();
    	
    	moduleId = getResources().getString(R.string.MODULE_LGIF);	
    }

    @Override
    protected void initUI2() {
        super.initUI2();

        aq = new AAQuery(this);

        //navbar
        aq.navBarTitle(R.id.navbar_title, getResources().getString(R.string.fp_activation_title));
        aq.navBarButton(R.id.navbar_button_left, R.drawable.btn_back);
        aq.navBarButton(R.id.navbar_button_right, R.drawable.chat);

        aq.id(R.id.navbar_button_left).clicked(this, "onClick");
        
        mActivateButton = (HKTButton) aq.id(R.id.btn_show_touch_id).getView();
        mActivateButton.initViews(this, getResources().getString(R.string.fp_activation_activate), LayoutParams.MATCH_PARENT);

        
        regInputPw = (RegInputItem) aq.id(R.id.login_input_pw).getView();
        regInputPw.initViews(this, getResources().getString(R.string.fp_activation_input_pwd), "", "", InputType.TYPE_TEXT_VARIATION_PASSWORD);
        regInputPw.setPadding(0, 0, 0, 0);
        regInputPw.setMaxLength(16);

        mActivateButton.setOnClickListener(new TouchIDClickListener(this));

    }

    @Override
    protected void onResume() {
        super.onResume();

        if (Utils.isTouchIDEnrolledByUser(this)) {
            if (Utils.isKeyguardSecured(this)) {
                mActivateButton.setEnabled(true);
            } else {

                mActivateButton.setEnabled(false);
                //Secure Lock is not enabaled
            }
        } else {
            DialogHelper.createSimpleDialog(this, getString(R.string.fp_welcome_not_enrolled_alert_msg));
            mActivateButton.setEnabled(false);
        }
        //Screen Tracker
        FAWrapper.getInstance().sendFAScreen(this, R.string.CONST_SCRN_FP_ACTIVATION, false);
    }

    /**
     * Proceed to completed page
     */
    @Override
    public void onTouchIdSuccess() {
    	
    	 SveeRec sveeRec = new SveeRec();
         sveeRec.loginId = ClnEnv.getPref(getApplicationContext(), Constant.TOUCH_ID_TEMP_USER_ID, "");
         
         LgiCra lgiCra = new LgiCra();
         lgiCra.setISveeRec(sveeRec);
         //		APIsManager.doLogin(this, lgiCra, isRememberPw);
         APIsManager.activateUserFingerPrint(this, lgiCra);

        

    }

    @Override
    public void onTouchIdFailed(int failCount) {
    	if(ClnEnv.getPref(getApplicationContext(), Constant.TOUCH_ID_FAILURE_COUNT, 0) == 3) {
        	showRetryActivity();
        }
    }

    @Override
    public void onEnterPassword() {
    	
//        mPasswordField.setFocusable(true);

    }
    
    @Override
    public void onFingerprintLockout(String error) {
    	//showRetryActivity();
    	Intent intent = new Intent(this, TouchIdVerificationFailedActivity.class);
		intent.putExtra(Constant.TOUCH_ID_RETRY_COUNT, true);
		intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivityForResult(intent, Constant.REQUEST_TOUCH_ID_VERIFICATION_FAILED);
        overridePendingTransition(R.anim.right_slide_in, R.anim.left_slide_out);
    }

    @Override
    public void onFingerprintError(int messId) {
    	// Do nothing
    	// Dialog is dismissed
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == Constant.REQUEST_TOUCH_ID_COMPLETE_PAGE) {
        	Intent reusltIntent = new Intent();
        	setResult(RESULT_OK, reusltIntent);
            finish();
            overridePendingTransition(R.anim.left_slide_in, R.anim.right_slide_out);
        }
    }

    public String getInputPassword() {
    	return regInputPw.getInput();
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.navbar_button_left:
            	setResult(RESULT_CANCELED);
                finish();
                overridePendingTransition(R.anim.left_slide_in, R.anim.right_slide_out);
                break;
        }
    }
    
    @Override
    public void onBackPressed() {
    	setResult(RESULT_CANCELED);
    	finish();
        overridePendingTransition(R.anim.left_slide_in, R.anim.right_slide_out);
    	super.onBackPressed();
    }
    
    public void onShowTnC(View view) {
    	
    	Intent intent = new Intent(getApplicationContext(), BrowserActivity.class);
		Bundle bundle = new Bundle();
		bundle.putBoolean(BrowserActivity.ISTOSHOWLIVECHAT, false);
		bundle.putString(BrowserActivity.INTENTLINK, isZh? domainURL + "/mba/html//fingerprint_tnc_zh.html" : domainURL + "/mba/html//fingerprint_tnc_en.html");				
		bundle.putString(BrowserActivity.INTENTTITLE, getResString(R.string.myhkt_confirm_ack2));
		intent.putExtras(bundle);
		intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
		startActivity(intent);
		overridePendingTransition(R.anim.right_slide_in, R.anim.left_slide_out);
    	
    }
    
    
    //API Call Listener
    @Override
    public void onSuccess(APIsResponse response) {
    	// TODO Auto-generated method stub
    	super.onSuccess(response);
    	if (response != null && APIsManager.API_FINGERPRCFG.equals(response.getActionTy())) {
    		
    		// Event Tracker
            FAWrapper.getInstance().sendFAEvents(me, R.string.CONST_GA_CATEGORY_USERLV,
                    R.string.CONST_GA_ACTION_BTN, R.string.CONST_GA_LABEL_FP_ACTIVATION,
                    false);

            //Clear Touch ID failures and retries
            ClnEnv.setPref(this, Constant.TOUCH_ID_FAILURE_COUNT, 0);
            //ClnEnv.setPref(this, Constant.TOUCH_ID_RETRY_COUNT, 0);
            
            //Set the Remember Me to false to disable auto login
            ClnEnv.setPref(this, getString(R.string.CONST_PREF_SAVEPASSWORD), false);
            ClnEnv.setPref(this, getString(R.string.CONST_PREF_SAVELOGINID), false);

            //Save Touch Login Enabled
            Utils.setTouchIDLoginActivated(this, true);
            
            //Save Touch ID Login Enabled to true as the default value
            Utils.setTouchIdLoginEnabled(this, true);

            //Save the temporary username and password as the default user name and password
            Utils.saveTempUserCredentialsAsTouchIdDefault(this);
    		
    		Intent intent = new Intent(this, TouchIdLoginActivationCompletedActivity.class);
            startActivityForResult(intent, Constant.REQUEST_TOUCH_ID_COMPLETE_PAGE);
            overridePendingTransition(R.anim.right_slide_in, R.anim.left_slide_out);
    	}
    }
    
    @Override
    public void onFail(APIsResponse response) {
    	// TODO Auto-generated method stub
    	super.onFail(response);
    	if (response != null && APIsManager.API_FINGERPRCFG.equals(response.getActionTy())) {
    		displayDialog(this, ClnEnv.getRPCErrMsg(this, response.getReply().getCode()));
    	}
    }
    
    public void showRetryActivity() {
        Intent intent = new Intent(this, TouchIdVerificationFailedActivity.class);
        startActivityForResult(intent, Constant.REQUEST_TOUCH_ID_VERIFICATION_FAILED);
        overridePendingTransition(R.anim.right_slide_in, R.anim.left_slide_out);
    }
}
