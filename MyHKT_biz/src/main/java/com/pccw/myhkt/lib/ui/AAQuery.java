package com.pccw.myhkt.lib.ui;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.text.InputFilter;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.RelativeSizeSpan;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.androidquery.AQuery;
import com.kyleduo.switchbutton.SwitchButton;
import com.pccw.biz.myhkt.R;
import com.pccw.myhkt.ClnEnv;
import com.pccw.myhkt.Utils;
import com.pccw.myhkt.ViewUtils;

public class AAQuery extends AQuery {

	private static int DEFAULT_TITLE_SIZE = 14;
	private static final int DEFAULT_TITLE_TXT_COLOR = R.color.hkt_navbarblue;
	private float density;
	public AAQuery(Context context) {
		super(context);
		DisplayMetrics metrics = new DisplayMetrics();
		WindowManager wm = (WindowManager)context.getSystemService(Context.WINDOW_SERVICE);
		wm.getDefaultDisplay().getMetrics(metrics);
		density = metrics.density;
		DEFAULT_TITLE_SIZE = context.getResources().getInteger(R.integer.textsize_default_int);
	}

	public AAQuery(Activity act, View root) {
		super(act, root);
		DisplayMetrics metrics = new DisplayMetrics();
		WindowManager wm = (WindowManager)act.getSystemService(Context.WINDOW_SERVICE);
		wm.getDefaultDisplay().getMetrics(metrics);
		density = metrics.density;
		DEFAULT_TITLE_SIZE = act.getResources().getInteger(R.integer.textsize_default_int);

	}

	public AAQuery(Activity act) {
		super(act);
		DisplayMetrics metrics = new DisplayMetrics();
		WindowManager wm = (WindowManager)act.getSystemService(Context.WINDOW_SERVICE);
		wm.getDefaultDisplay().getMetrics(metrics);
		density = metrics.density;
		DEFAULT_TITLE_SIZE = act.getResources().getInteger(R.integer.textsize_default_int);
	}

	public AAQuery(View view) {
		super(view);
		DisplayMetrics metrics = new DisplayMetrics();
		WindowManager wm = (WindowManager)view.getContext().getSystemService(Context.WINDOW_SERVICE);
		wm.getDefaultDisplay().getMetrics(metrics);
		density = metrics.density;
		DEFAULT_TITLE_SIZE = view.getContext().getResources().getInteger(R.integer.textsize_default_int);
	}

	// Styled NavBar
	public void navBarBaseLayout(int id) {
		super.id(id).backgroundColorId(ClnEnv.getSessionPremierFlag() && !ClnEnv.isMyMobFlag() ? R.color.hkt_headingpremier : R.color.white);
		int px = Utils.dpToPx(getContext().getResources().getInteger(R.integer.padding_screen));
		super.id(id).getView().setPadding(0, px, 0, px);
	}

	public void navBarButton(int id, int imageId) {
		int padding = Utils.dpToPx(getContext().getResources().getInteger(R.integer.padding_screen));
		int btnHeight = 0;
		if (imageId != R.drawable.btn_slidemenu) {
			btnHeight = getContext().getResources().getDimensionPixelOffset(R.dimen.button_height) - padding;	
		} else {
			btnHeight = (int) (getContext().getResources().getDimensionPixelOffset(R.dimen.button_height) - padding *1.5);
		}
		
		int refH = ViewUtils.heightOfScaleToFitHeight(super.getContext(), imageId, (btnHeight));
		Bitmap bm = ViewUtils.scaleToFitHeight(super.getContext(), imageId, (btnHeight));
		super.id(id).height(btnHeight, false);		
		super.id(id).width(Math.max(btnHeight, refH), false);
		

		super.id(id).image(bm);
		super.id(id).getImageView().setScaleType(ScaleType.CENTER);
	
		LinearLayout.LayoutParams params = (LayoutParams) super.id(id).getView().getLayoutParams();
//		params.gravity = Gravity.CENTER_VERTICAL;
		if (imageId == R.drawable.chat) {
			params.rightMargin = padding;
		} else if (imageId == R.drawable.btn_slidemenu) {
			params.leftMargin = padding;
		} else if (imageId == R.drawable.btn_back) {
			params.leftMargin = padding;
		}
		//Premier: white , Non-Premier : blue
		
		if (id == R.id.navbar_button_left) {
			super.id(id).getImageView().clearColorFilter();
			if (ClnEnv.getSessionPremierFlag() && !ClnEnv.isMyMobFlag()) {
				ColorFilter filter = new PorterDuffColorFilter(Color.parseColor("#FFFFFF"), PorterDuff.Mode.SRC_IN);				
				super.id(id).getImageView().setColorFilter(filter);
			}
		}
//		ColorFilter filter = new PorterDuffColorFilter(Color.parseColor("#FFFFFF"), PorterDuff.Mode.DST_IN);				
//		super.id(id).getImageView().setColorFilter(filter);
		super.id(id).getView().setLayoutParams(params);
	}

	public void navBarTitle(int id, String text) {
		super.id(id).getTextView().setGravity(Gravity.CENTER);
		super.id(id).getTextView().setMaxLines(1);
		super.id(id).text(text);
//		super.id(id).textSize(getContext().getResources().getInteger(R.integer.textsize_default_int)); //TODO
		super.id(id).textSize(DEFAULT_TITLE_SIZE);
		super.id(id).textColorId(ClnEnv.getSessionPremierFlag() && !ClnEnv.isMyMobFlag() ? R.color.white : DEFAULT_TITLE_TXT_COLOR);
//		Typeface type = Typeface.createFromAsset(getContext().getAssets(),"fonts/mingliu.ttc"); 
//		super.id(id).getTextView().setTypeface(type);
		//change theme for MyMob
		if (ClnEnv.isMyMobFlag()) {
			int colorId = R.color.hkt_edittextbgorange;
			if (ClnEnv.isIs101Flag()) {
				colorId = R.color.black;
			}
			super.id(id).textColorId(colorId);
		}
	}
	
	public void navBarTitle(int id, String text, int colorId) {
		super.id(id).getTextView().setGravity(Gravity.CENTER);
		super.id(id).getTextView().setMaxLines(1);
		super.id(id).text(text);
		super.id(id).textSize(DEFAULT_TITLE_SIZE);
		super.id(id).textColorId(colorId);


	}

	public void navBarTitle(int id, int leftId, int rightId, int leftRes, int rightRes, String text) {
		super.id(id).getTextView().setGravity(Gravity.CENTER);
		super.id(id).getTextView().setMaxLines(1);
		super.id(id).text(text + "  ");
		super.id(id).textSize(DEFAULT_TITLE_SIZE);
		super.id(id).textColorId(DEFAULT_TITLE_TXT_COLOR);
		if(leftId > 0)
			super.id(leftId).getImageView().setImageResource(leftRes);
		if(rightId > 0)
			super.id(rightId).getImageView().setImageResource(rightRes);
		
		//change theme for MyMob
		if (ClnEnv.isMyMobFlag()) {
			int colorId = R.color.hkt_edittextbgorange;
			if (ClnEnv.isIs101Flag()) {
				colorId = R.color.black;
			}
			super.id(id).textColorId(colorId);
		}
	}

	public void navBarDrawable(int id, int resId) {
		super.id(id).image(resId);
	}
	// Styled ImageView - Line
	public void line(int... id) {
		int extralinespace = (int) getContext().getResources().getDimension(R.dimen.extralinespace);
		line(extralinespace, "", id);
	}
	
	public void line(int padding, String useless, int... id) {
		for (int i = 0; i < id.length; i++) {
			LinearLayout.LayoutParams lineParams =(LayoutParams) super.id(id[i]).getImageView().getLayoutParams();
			lineParams.width = LayoutParams.MATCH_PARENT ;
			lineParams.height = (int) getContext().getResources().getDimension(R.dimen.greyline_height);
			super.id(id[i]).getImageView().setLayoutParams(lineParams);
			super.id(id[i]).getImageView().setImageResource(R.drawable.greyline);	
			super.id(id[i]).getImageView().setPadding(padding, 0, padding, 0);
		}
	}
	public void lineRelativeLayout(int... id) {
		int extralinespace = (int) getContext().getResources().getDimension(R.dimen.extralinespace);
		for (int i = 0; i < id.length; i++) {
			RelativeLayout.LayoutParams lineParams =(RelativeLayout.LayoutParams) super.id(id[i]).getImageView().getLayoutParams();
			lineParams.width = LayoutParams.MATCH_PARENT ;
			lineParams.height = (int) getContext().getResources().getDimension(R.dimen.greyline_height);
			super.id(id[i]).getImageView().setLayoutParams(lineParams);
			super.id(id[i]).getImageView().setImageResource(R.drawable.greyline);	
			super.id(id[i]).getImageView().setPadding(extralinespace, 0, extralinespace, 0);
		}
	}
	public void line(int id){
		ViewGroup.LayoutParams lineParams = super.id(id).getImageView().getLayoutParams();
		lineParams.width = LayoutParams.MATCH_PARENT ;
		lineParams.height = (int) getContext().getResources().getDimension(R.dimen.greyline_height);
		super.id(id).getImageView().setLayoutParams(lineParams);
		super.id(id).getImageView().setImageResource(R.drawable.greyline);
	}

	// Styled TextView

	/**
	 * 
	 * 	
	 * Default
	 * left right padding :	extralinespace
	 * height  			  	textviewheight
	 * textsize 			bodytextsize
	 * Gravity	 			left center
	 * 
	 * @param id
	 * @param text
	 * 
	 */
	public void normText(int id, String text) {
		int extralinespace = (int) getContext().getResources().getDimension(R.dimen.extralinespace);
		super.id(id).text(text).height((int) getContext().getResources().getDimension(R.dimen.textviewheight), false);
		super.id(id).getTextView().setTextSize(TypedValue.COMPLEX_UNIT_PX, this.getContext().getResources().getDimension(R.dimen.bodytextsize));
		gravity(id, Gravity.LEFT| Gravity.CENTER_VERTICAL);		
	}

	public void normText(int id, String text, int deltaSP) {
		normText(id, text);
		super.id(id).getTextView().setTextSize(TypedValue.COMPLEX_UNIT_PX, this.getContext().getResources().getDimension(R.dimen.bodytextsize) +deltaSP * density);
	}
	public void normTextGrey(int id, String text) {	
		normText(id, text);
		super.id(id).text(text).textColorId(R.color.hkt_txtcolor_grey);
	}
	public void normTextGrey(int id, String text, int deltaSP) {		
		normText(id, text, deltaSP);
		super.id(id).text(text).textColorId(R.color.hkt_txtcolor_grey);
	}
	public void normTextBlue(int id, String text) {		
		normText(id, text);
		super.id(id).text(text).textColorId(R.color.hkt_textcolor);
	}
	public void normTextBlue(int id, String text, int deltaSP) {		
		normText(id, text, deltaSP);
		super.id(id).text(text).textColorId(R.color.hkt_textcolor);
	}

	// Styled PopOverView
	//default
	public PopOverInputView popOverInputView(int id, String text) {
		int padding_screen = (int) getContext().getResources().getDimension(R.dimen.padding_screen);
		int regInputHeight = (int) getContext().getResources().getDimension(R.dimen.reg_input_height);
		PopOverInputView popOverInputView = (PopOverInputView) super.id(id).getView();
		popOverInputView.initViews(getContext(), regInputHeight, text);
		popOverInputView.setPadding(0, 0, 0, 0);
		return popOverInputView;
	}

	// Styled Spinner
	public void spinText(int id, String text) {
		spinText(id, text, 0, false);
	}
	
	// Styled Spinner
		public void spinText(int id, String text, Boolean isArrowUp) {
			spinText(id, text, 0, isArrowUp);
		}

	// Styled Spinner
	public void spinText(int id, String text, int deltaSP, Boolean isArrowUp) {
		int edittextpadding = (int) getContext().getResources().getDimension(R.dimen.edittextpadding);
		
		super.id(id).text(text).textColorId(R.color.hkt_textcolor).height((int) getContext().getResources().getDimension(R.dimen.reg_input_height)/2, false);
		//change theme for MyMob
//		if (ClnEnv.isMyMobFlag()) {
//				super.id(id).text(text).background(R.drawable.hkt_edittextorange_bg);
//			if (ClnEnv.isIs101Flag()) {
//				super.id(id).text(text).background(R.drawable.hkt_edittextblack_bg);
//			}
//		} else {
//			super.id(id).text(text).background(R.drawable.hkt_edittextblue_bg);
//		}
		//		super.id(id).getSpinner()..setTextSize(TypedValue.COMPLEX_UNIT_PX, this.getContext().getResources().getDimension(R.dimen.bodytextsize));
		//		
		Drawable drawable = getContext().getResources().getDrawable(isArrowUp ? R.drawable.btn_arrowup : R.drawable.btn_arrowdown);
		drawable.setBounds(0, 0, drawable.getMinimumWidth(), drawable.getMinimumHeight());
		
		if (ClnEnv.isMyMobFlag()) {
			super.id(id).text(text).background(R.drawable.hkt_edittextorange_bg);
			ColorFilter filter = new PorterDuffColorFilter(Color.parseColor("#F9A619"), PorterDuff.Mode.MULTIPLY); //hkt_textcolor_orange
			drawable.setColorFilter(filter);
			
			//change theme for MyMob
			if (ClnEnv.isIs101Flag()) {
				super.id(id).text(text).background(R.drawable.hkt_edittextblack_bg);
				filter = new PorterDuffColorFilter(Color.parseColor("#000000"), PorterDuff.Mode.MULTIPLY); //black
				drawable.setColorFilter(filter);
			} else {
				super.id(id).text(text).background(R.drawable.hkt_edittextblue_bg);
			}
		}
		
		super.id(id).getTextView().setCompoundDrawables(null, null, drawable, null);
		super.id(id).getTextView().setTextSize(TypedValue.COMPLEX_UNIT_PX, this.getContext().getResources().getDimension(R.dimen.bodytextsize) +deltaSP * density);
		super.id(id).getTextView().setPadding(edittextpadding, 0, edittextpadding, 0);
		super.id(id).getTextView().setGravity(Gravity.LEFT|Gravity.CENTER_VERTICAL);
		super.id(id).textColorId(R.color.hkt_buttonblue);
	}

	/***********************************
	 * 
	 * EditText
	 * 
	 *********************************/
	// Styled EditText
	public EditText normEditText(int id, String text, String hint, int charLimit) {
		int extralinespace = (int) getContext().getResources().getDimension(R.dimen.extralinespace);

		InputFilter[] filterArray = new InputFilter[1];
		filterArray[0] = new InputFilter.LengthFilter(charLimit);

		//change theme for MyMob
		if (ClnEnv.isMyMobFlag()) {
				super.id(id).text(text).background(R.drawable.hkt_edittextorange_bg).height((int)getContext().getResources().getDimension(R.dimen.edittext_height), false);
			if (ClnEnv.isIs101Flag()) {
				super.id(id).text(text).background(R.drawable.hkt_edittextblack_bg).height((int)getContext().getResources().getDimension(R.dimen.edittext_height), false);
			}
		} else {
			super.id(id).text(text).background(R.drawable.hkt_edittextwhite_bg).height((int)getContext().getResources().getDimension(R.dimen.edittext_height), false);
		}
		super.id(id).getEditText().setSingleLine();
		super.id(id).getEditText().setHint(hint);
		super.id(id).getEditText().setTextSize(TypedValue.COMPLEX_UNIT_PX, this.getContext().getResources().getDimension(R.dimen.bodytextsize));
		super.id(id).getEditText().setPadding(extralinespace/2, 0, extralinespace/2, 0);
		super.id(id).getEditText().setFilters(filterArray);
		return super.id(id).getEditText();
	}

	public EditText normEditText(int id, String text, String hint) {
		return normEditText( id,  text,  hint,  100);
	}

	public EditText mymobEditText(int id, String text, String hint) {
		int extralinespace = (int) getContext().getResources().getDimension(R.dimen.extralinespace);
		super.id(id).text(text).background(R.drawable.hkt_edittextorange_bg).height((int)getContext().getResources().getDimension(R.dimen.edittext_height), false);
		super.id(id).getEditText().setHint(hint);
		super.id(id).getEditText().setTextSize(TypedValue.COMPLEX_UNIT_PX, this.getContext().getResources().getDimension(R.dimen.bodytextsize));
		super.id(id).getEditText().setPadding(extralinespace/2, 0, extralinespace/2, 0);
		
		//shrink hint's size
		SpannableString span = new SpannableString(hint);
		span.setSpan(new RelativeSizeSpan(0.9f), 0, hint.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
		super.id(id).getEditText().setHint(span);

		return super.id(id).getEditText();
	}

	public void maxLength(int id, int max) {
		InputFilter[] fileter = new InputFilter[1] ;
		fileter[0] = 	new InputFilter.LengthFilter(max);
		super.id(id).getEditText().setFilters(fileter);
	}

	/***************************************
	 * 
	 * Switch Button
	 * 
	 * 
	 **************************************/
	public SwitchButton switchButton (int id) {
		com.kyleduo.switchbutton.SwitchButton switchButton = (com.kyleduo.switchbutton.SwitchButton) super.id(id).getView();
		switchButton.setAnimationDuration(300);
		switchButton.setBackDrawableRes(R.drawable.ios_back_drawable);
		switchButton.setBackMeasureRatio((float) 1.4);
		switchButton.setThumbDrawableRes(R.drawable.ios_thumb);
		switchButton.setThumbMargin(-Utils.dpToPx(5), -Utils.dpToPx(5)/2, -Utils.dpToPx(5), -Utils.dpToPx(8));
		RelativeLayout.LayoutParams swparams = (RelativeLayout.LayoutParams) switchButton.getLayoutParams();
		swparams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
		swparams.addRule(RelativeLayout.CENTER_VERTICAL);
		switchButton.setLayoutParams(swparams);	
		return switchButton;
	}


	// Styled Button	
	public void norm2TxtBtns(int leftRes, int RightRes, String leftText, String reightText) {
		int basePadding = (int) getContext().getResources().getDimension(R.dimen.basePadding);
		HKTButton lBtn = (HKTButton) super.id(leftRes).getView();
		lBtn.setText(leftText);
		LinearLayout.LayoutParams params_l = (LayoutParams) lBtn.getLayoutParams();
		params_l.rightMargin = basePadding;
		params_l.width = LinearLayout.LayoutParams.MATCH_PARENT;
		lBtn.setLayoutParams(params_l);
		layoutWeight(leftRes,1);
		HKTButton rBtn = (HKTButton) super.id(RightRes).getView();
		rBtn.setText(reightText);
		LinearLayout.LayoutParams params_r = (LayoutParams) lBtn.getLayoutParams();
		params_r.leftMargin = basePadding;
		params_r.width = LinearLayout.LayoutParams.MATCH_PARENT;
		layoutWeight(RightRes,1);
		rBtn.setLayoutParams(params_r);
		//change theme for MyMob
		if (ClnEnv.isMyMobFlag()) {
			int btnType = HKTButton.TYPE_ORANGE;
			if (ClnEnv.isIs101Flag()) {
				btnType = HKTButton.TYPE_BLACK;
			}
			lBtn.setType(btnType);
			rBtn.setType(btnType);
		}
	}
	
	//norm2Txt with btnType
	public void norm2TxtBtns(int leftRes, int RightRes, String leftText, String reightText, int btnType) {
		HKTButton lBtn = (HKTButton) super.id(leftRes).getView();
		lBtn.setText(leftText);
		lBtn.setType(btnType);
		LinearLayout.LayoutParams params_l = (LayoutParams) lBtn.getLayoutParams();
		params_l.rightMargin = Utils.dpToPx(getContext().getResources().getInteger(R.integer.padding_screen));
		params_l.width = LinearLayout.LayoutParams.MATCH_PARENT;
		lBtn.setLayoutParams(params_l);
		layoutWeight(leftRes,1);
		HKTButton rBtn = (HKTButton) super.id(RightRes).getView();
		rBtn.setText(reightText);
		rBtn.setType(btnType);
		LinearLayout.LayoutParams params_r = (LayoutParams) lBtn.getLayoutParams();
		params_r.leftMargin = Utils.dpToPx(getContext().getResources().getInteger(R.integer.padding_screen));
		params_r.width = LinearLayout.LayoutParams.MATCH_PARENT;
		layoutWeight(RightRes,1);
		rBtn.setLayoutParams(params_r);
	}

	public HKTButton normTxtBtn(int res, String text ,int width) {		
		HKTButton hktBtn = (HKTButton) super.id(res).getView();
		hktBtn.initViews(getContext(), text, width);
		//		hktBtn.initViews(getContext(), text);
		//change theme for MyMob
		if (ClnEnv.isMyMobFlag()) {
			int btnType = HKTButton.TYPE_ORANGE;
			if (ClnEnv.isIs101Flag()) {
				btnType = HKTButton.TYPE_BLACK;
			}
			hktBtn.setType(btnType);
		}
		return hktBtn;
	}
	public HKTButton normTxtBtn(int deltaSP,int res ,String text ,int width) {		
		HKTButton hktBtn = (HKTButton) super.id(res).getView();
		hktBtn.initViews(getContext(), text, width);
		hktBtn.setTextSize(TypedValue.COMPLEX_UNIT_PX, this.getContext().getResources().getDimension(R.dimen.bodytextsize) +deltaSP * density);
		//		hktBtn.initViews(getContext(), text);
		//change theme for MyMob
		if (ClnEnv.isMyMobFlag()) {
			int btnType = HKTButton.TYPE_ORANGE;
			if (ClnEnv.isIs101Flag()) {
				btnType = HKTButton.TYPE_BLACK;
			}
			hktBtn.setType(btnType);
		}
		return hktBtn;
	}
	
	


	public HKTButton normTxtBtn(int res, String text ,int width , int btnType) {		
		HKTButton hktBtn = (HKTButton) super.id(res).getView();
		hktBtn.initViews(getContext(),btnType, text, width);
		//		hktBtn.initViews(getContext(), text);
		return hktBtn;
	}


	//TODO
	// Style RegInputItem
	public RegInputItem regInputItem(int id, String text,String hint, String input,int inputType, int maxlength) {
		int extralinespace = (int) getContext().getResources().getDimension(R.dimen.extralinespace);
		int basePadding = (int) getContext().getResources().getDimension(R.dimen.basePadding);
		int regInputHeight = (int) getContext().getResources().getDimension(R.dimen.reg_input_height);
		RegInputItem regInputItem = (RegInputItem) super.id(id).getView();
		regInputItem.initViews(getContext(), regInputHeight, text, hint, input, inputType);
		regInputItem.setPadding(basePadding, 0, basePadding, extralinespace);
		regInputItem.setMaxLength(maxlength);
		return regInputItem;
	}

	// Style RegInputItem
	/* Default inputtype*/
	public RegInputItem regInputItem(int id, String text,String hint, String input, int maxlength) {
		int extralinespace = (int) getContext().getResources().getDimension(R.dimen.extralinespace);
		int basePadding = (int) getContext().getResources().getDimension(R.dimen.basePadding);
		int regInputHeight = (int) getContext().getResources().getDimension(R.dimen.reg_input_height);
		RegInputItem regInputItem = (RegInputItem) super.id(id).getView();
		regInputItem.initViews(getContext(), regInputHeight, text, hint, input);
		regInputItem.setPadding(basePadding, 0, basePadding, extralinespace);
		regInputItem.setMaxLength(maxlength);
		return regInputItem;
	}

	// Not Max Lenghth
	public RegInputItem regInputItem(int id, String text,String hint, String input) {
		int extralinespace = (int) getContext().getResources().getDimension(R.dimen.extralinespace);
		int basePadding = (int) getContext().getResources().getDimension(R.dimen.basePadding);
		int regInputHeight = (int) getContext().getResources().getDimension(R.dimen.reg_input_height);
		RegInputItem regInputItem = (RegInputItem) super.id(id).getView();
		regInputItem.initViews(getContext(), regInputHeight, text, hint, input);
		regInputItem.setPadding(basePadding, 0, basePadding, extralinespace);
		return regInputItem;
	}

	public void layoutWeight(int id, float weight) {
		View view  = super.id(id).getView();

		if (view.getLayoutParams() instanceof LinearLayout.LayoutParams) {
			LinearLayout.LayoutParams layoutParams = (LayoutParams) view.getLayoutParams();
			layoutParams.weight = weight;
			view.setLayoutParams(layoutParams);
		}		
	}

	public Drawable scaleDrawable(int res, int height, int width) {
		Drawable dr = getContext().getResources().getDrawable(res);
		Bitmap bitmap = ((BitmapDrawable) dr).getBitmap();
		Drawable d = new BitmapDrawable(getContext().getResources(), Bitmap.createScaledBitmap(bitmap, width, height, true));

		return d;
	}

	//set margin
	public void marginpx(int id, int left, int top, int right, int bottom) {
		View view = super.id(id).getView();
		if (view.getLayoutParams() instanceof LinearLayout.LayoutParams) {
			LinearLayout.LayoutParams params = (LinearLayout.LayoutParams)view.getLayoutParams();
			params.leftMargin = left;
			params.topMargin = top;
			params.rightMargin = right;
			params.bottomMargin = bottom;
			view.setLayoutParams(params);
			params = null;

		} else if (view.getLayoutParams() instanceof RelativeLayout.LayoutParams) {
			RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams)view.getLayoutParams();
			params.leftMargin = left;
			params.topMargin = top;
			params.rightMargin = right;
			params.bottomMargin = bottom;
			view.setLayoutParams(params);
			params = null;			
		}		
		view = null;		
	}

	public void gravity(int id, int gravity) {
		View view = super.id(id).getView();
		if (view !=null) {
			if (view instanceof TextView){
				((TextView)view).setGravity(gravity);
			}			
			if (view instanceof EditText){
				((EditText)view).setGravity(gravity);
			}
			if (view instanceof Button){
				((Button)view).setGravity(gravity);
			}
			if (view instanceof LinearLayout){
				((LinearLayout)view).setGravity(gravity);
			}
			if (view instanceof RelativeLayout){
				((RelativeLayout)super.getView()).setGravity(gravity);
			}
		}
	}

	public void padding(int id, int left, int top, int right, int bottom) {
		View view = super.id(id).getView();
		if (view !=null) {
			if (view instanceof TextView){
				view.setPadding (left, top, right, bottom);
			}			
			if (view instanceof EditText){
				view.setPadding (left, top, right, bottom);
			}
			if (view instanceof Button){
				view.setPadding (left, top, right, bottom);
			}
			if (view instanceof LinearLayout){
				view.setPadding (left, top, right, bottom);
			}
			if (view instanceof RelativeLayout){
				super.getView().setPadding (left, top, right, bottom);
			}
			if (view instanceof ImageView){
				super.getView().setPadding (left, top, right, bottom);
			}
		}
	}
	
	public static final int getDefaultTextSize() {
		return DEFAULT_TITLE_SIZE;
	}
}