package com.pccw.myhkt.activity;

import android.content.Context;
import android.content.Intent;
import androidx.test.espresso.intent.Intents;
import androidx.test.rule.ActivityTestRule;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import com.pccw.myhkt.MockUtils.TestMockData;
import com.pccw.myhkt.R;
import com.pccw.myhkt.base.BaseTest;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static androidx.test.platform.app.InstrumentationRegistry.getInstrumentation;

@Ignore
@RunWith(AndroidJUnit4.class)
public class RegAccInfoActivityTest extends BaseTest{
    private Context context;

    @Rule
    public ActivityTestRule<RegAccInfoActivity> activityTestRule = new ActivityTestRule<>(RegAccInfoActivity.class, false, false);


    @Before
    public void setActivity() {
        Intent myIntent = new Intent();
        myIntent.putExtra("LOGINID", "");
        myIntent.putExtra("PWD", "a12345");
        myIntent.putExtra("SAVEPW", "");
        myIntent.putExtra("ERRCODE", "RC_FILL_SVEE");
        myIntent.putExtra("LOGINCRA", TestMockData.getUATLoginSuccessResponse());
        activityTestRule.launchActivity(myIntent);
        context = getInstrumentation().getTargetContext();
    }

    @Test
    public void testUIViews() {
        clickView(R.id.lrbutton_right,R.id.regaccinfo_lang_lrbtn);

        checkViewIsDisplayed(R.id.regaccinfo_loginid_desc);

        checkViewIsDisplayed(R.id.regaccinfo_nickname_txt);

        checkViewIsDisplayed(R.id.regaccinfo_nickname_etxt);

        checkViewIsDisplayed(R.id.regaccinfo_ITO_ATH);

        checkViewIsDisplayed(R.id.regaccinfo_doctype_lrbtn);

        checkViewIsDisplayed(R.id.regaccinfo_docnum_txt);

        checkViewIsDisplayed(R.id.regaccinfo_email_txt);

        checkViewIsDisplayed(R.id.regaccinfo_mobno_txt);

        scrollToView(R.id.regaccinfo_retypepwd_etxt);

        checkViewIsDisplayed(R.id.regaccinfo_mobno_etxt);

        checkViewIsDisplayed(R.id.regaccinfo_lang_desc);

        checkViewIsDisplayed(R.id.regaccinfo_lang_lrbtn);

        checkViewIsDisplayed(R.id.regaccinfo_newpwd_txt);

        checkViewIsDisplayed(R.id.regaccinfo_retypepwd_txt);

        checkViewIsDisplayed(R.id.regaccinfo_retypepwd_etxt);

        checkViewIsDisplayed(R.id.regaccinfo_submit_btn);

        checkViewIsDisplayed(R.id.regaccinfo_pwd_layout);
    }

    @Test
    public void verifyInputHints() {
        clickView(R.id.lrbutton_right,R.id.regaccinfo_lang_lrbtn);

        checkStringHint(R.id.regaccinfo_docnum_etxt,"e.g. A123456(7)");

        checkStringHint(R.id.regaccinfo_mobno_etxt,"Valid mobile no. with 8 digits");

        checkStringHint(R.id.regaccinfo_email_etxt,"Valid email address with 8-40 characters");

        checkStringHint(R.id.regaccinfo_nickname_etxt,"Maximum 20 characters");

    }

    @Test
    public void testTextDisplayedFromResponse() {

        checkViewWithText(R.id.regaccinfo_email_etxt,"greenteaworld1981@yahoo.com.hk");

        checkViewWithText(R.id.regaccinfo_mobno_etxt,"61313256");

        checkViewWithText(R.id.regaccinfo_nickname_etxt,"pccwuser");

        checkViewIsDisplayed(R.id.regaccinfo_loginid_desc);

    }

    @Test
    public void testNoHKIDInput() {
        clickView(R.id.lrbutton_right,R.id.regaccinfo_lang_lrbtn);

        clickView(R.id.regaccinfo_submit_btn);

        checkStringIsDisplayed("HKID no./ Passport no is invalid. Please enter a valid HKID no. or Passport no.");
    }

    @Test
    public void testValidHKIDInput() {
        clickView(R.id.lrbutton_right,R.id.regaccinfo_lang_lrbtn);

        Intents.init();

        typeTextView(R.id.regaccinfo_docnum_etxt,"B209128(9)");

        scrollToView(R.id.regaccinfo_retypepwd_etxt);

        typeTextView(R.id.reg_input_layoutet, R.id.regaccinfo_newpwd_etxt,"Aa12345");

        typeTextView(R.id.reg_input_layoutet, R.id.regaccinfo_retypepwd_etxt,"Aa12345");

        clickView(R.id.regaccinfo_submit_btn);

        checkIntendedActivity(RegAccInfoConfirmActivity.class.getName());

        Intents.release();

    }

    @Test
    public void testChangeLanguage() {
        scrollToView(R.id.regaccinfo_retypepwd_etxt);

        clickView(R.id.lrbutton_right,R.id.regaccinfo_lang_lrbtn);

        checkViewWithText(R.id.regaccinfo_loginid_desc, context, R.string.REGF_LOGIN_ID);

        checkViewWithText(R.id.regaccinfo_nickname_txt, context, R.string.PAMF_NICK_NAME);

        checkViewWithText(R.id.regaccinfo_ITO_ATH, context, R.string.SFRF_ITO_ATH);

        checkViewWithText(R.id.regaccinfo_email_txt, context, R.string.REGF_CT_MAIL);

        checkViewWithText(R.id.regaccinfo_mobno_txt, context, R.string.REGF_CT_MOB);

    }

}