package com.pccw.myhkt.activity;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.pccw.biz.myhkt.R;
import com.pccw.dango.shared.cra.AcMainCra;
import com.pccw.dango.shared.entity.BcifRec;
import com.pccw.dango.shared.entity.CustRec;
import com.pccw.dango.shared.entity.SubnRec;
import com.pccw.dango.shared.entity.SveeRec;
import com.pccw.dango.shared.tool.RC;
import com.pccw.myhkt.APIsManager;
import com.pccw.myhkt.APIsResponse;
import com.pccw.myhkt.ClnEnv;
import com.pccw.myhkt.DialogHelper;
import com.pccw.myhkt.Utils;
import com.pccw.myhkt.lib.ui.AAQuery;
import com.pccw.myhkt.lib.ui.HKTButton;
import com.pccw.myhkt.lib.ui.RegConfirmView;
import com.pccw.myhkt.lib.ui.RegImageButton;
import com.pccw.myhkt.model.SubService;
import com.pccw.wheat.shared.tool.Reply;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/************************************************************************
 * File : RegConfirmActivity.java 
 * Desc : Reg Confirm 
 * Name : RegConfirmActivity
 * by : Andy Wong 
 * Date : 30/10/2015
 *
 * Change History:
 * Date       Modified By		Description
 * ---------- ----------------	-------------------------------
 * 30/10/2015 Andy Wong 		-First draft
 * 12/12/2017 Moiz Esmail		-Added the iBcifRec the registration
 *************************************************************************/
public class RegConfirmActivity extends BaseActivity {
    // Common Components
    private boolean debug = false;
    private static RegConfirmActivity me;
    private String TAG = this.getClass().getName();
    private AQuery aq;
    private final int colMaxNum = 3;
    private int deviceWidth = 0;
    private int colWidth = 0;
    private int buttonPadding = 0;
    private int extralinespace = 0;
    private int regConfirmViewHeight = 0;
    private int greylineHeight = 0;
    private int basePadding = 0;
    private List<RegConfirmItem> regConfirmItemList;
    private RegImageButton regImageButon;
    private TextView textViewTc, textViewPri;

    private Button canceBtn;
    private Button confirmBtn;
    private HashMap<String, String> serviceMap;

    private CustRec custRec;
    private SveeRec sveeRec;
    private SubnRec subnRec;
    private BcifRec bcifRecCompInfo;
    private Boolean isHKID;
    private String domainURL = APIsManager.PRD_FTP;
    private static final String SCREEN_NAME = "Registration Confirm Screen";

    @Override
    public final void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        me = this;
        debug = getResources().getBoolean(R.bool.DEBUG);

        custRec = (CustRec) getIntent().getExtras().getSerializable(RegBasicActivity.BUNDLE_CUSTREC);
        sveeRec = (SveeRec) getIntent().getExtras().getSerializable(RegBasicActivity.BUNDLE_SVEEREC);
        subnRec = (SubnRec) getIntent().getExtras().getSerializable(RegBasicActivity.BUNDLE_SUBNREC);
        bcifRecCompInfo = (BcifRec) getIntent().getExtras().getSerializable(RegBasicActivity.BUNDLE_COMPANY_INFO_REC);
        ClnEnv.updateUILocale(getBaseContext(), ClnEnv.getAppLocale(getBaseContext()));

        domainURL = ClnEnv.getPref(me, me.getString(R.string.CONST_PREF_FTP), APIsManager.PRD_FTP);
        setContentView(R.layout.activity_regconfirm);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        ClnEnv.updateUILocale(getBaseContext(), ClnEnv.getAppLocale(getBaseContext()));
    }

    @Override
    protected final void onStart() {
        initData();
        super.onStart();

        setClickListener();
    }

    @Override
    protected final void onResume() {
        super.onResume();
        moduleId = getResources().getString(R.string.MODULE_REG);
        firebaseSetting.onScreenView(RegConfirmActivity.this, SCREEN_NAME);

        // Update Locale
        ClnEnv.updateUILocale(getBaseContext(), ClnEnv.getAppLocale(getBaseContext()));
    }

    private void initData() {
        //init the service map
        serviceMap = new HashMap<String, String>();
        serviceMap.put(SubnRec.LOB_LTS, Utils.getString(this, R.string.myhkt_lts_fixedline));
        serviceMap.put(SubnRec.LOB_IMS, Utils.getString(this, R.string.comm_lob_ims));
        serviceMap.put(SubnRec.LOB_PCD, Utils.getString(this, R.string.SHLM_LB_LOB_PCD));
        serviceMap.put(SubnRec.LOB_LTS + SubnRec.TOS_LTS_OCM, Utils.getString(this, R.string.comm_lob_lts_o));
        serviceMap.put(SubnRec.LOB_TV, Utils.getString(this, R.string.myhkt_cu_tvTitle));

        //* init the regconfirm list
        regConfirmItemList = new ArrayList<RegConfirmItem>();

        if (subnRec.lob.equals(SubnRec.LOB_TV) || subnRec.lob.equals(SubnRec.LOB_IMS)) {
            /* For TV and IMS(Bus Netvigator), make sure srv_num is empty and acct_num has value*/
            regConfirmItemList.add(new RegConfirmItem(R.id.regconfirm_view_service, getResString(R.string.myhkt_confirm_head1), serviceMap.get(subnRec.lob) + "/" + subnRec.acctNum));
        } else {
            regConfirmItemList.add(new RegConfirmItem(R.id.regconfirm_view_service, getResString(R.string.myhkt_confirm_head1), serviceMap.get(subnRec.lob + subnRec.tos) + "/" + subnRec.srvNum));
        }

        regConfirmItemList.add(new RegConfirmItem(R.id.regconfirm_view_hkid, getResString(R.string.comm_reg_confirm_hkbr), custRec.docNum));
        regConfirmItemList.add(new RegConfirmItem(R.id.regconfirm_view_loginid, getResString(R.string.myhkt_confirm_loginid), sveeRec.loginId));
        regConfirmItemList.add(new RegConfirmItem(R.id.regconfirm_view_nickname, getResString(R.string.myhkt_confirm_nickname), sveeRec.nickname));
        regConfirmItemList.add(new RegConfirmItem(R.id.regconfirm_view_mobile_number_reset, getResString(R.string.myhkt_confirm_reset), sveeRec.ctMob));

        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        deviceWidth = size.x;
        colWidth = (deviceWidth - (int) getResources().getDimension(R.dimen.extralinespace) * 2) / colMaxNum;
        buttonPadding = (int) getResources().getDimension(R.dimen.reg_logo_padding);
        extralinespace = (int) getResources().getDimension(R.dimen.extralinespace);
        basePadding = (int) getResources().getDimension(R.dimen.basePadding);
        regConfirmViewHeight = (int) getResources().getDimension(R.dimen.reg_confirm_view_height);
        greylineHeight = (int) getResources().getDimension(R.dimen.greyline_height);

    }


    protected final void initUI2() {
        aq = new AQuery(this);
        AAQuery aaq = new AAQuery(this);
        aaq.navBarButton(R.id.navbar_button_left, R.drawable.btn_back);
        aaq.navBarTitle(R.id.navbar_title, getResString(R.string.myhkt_reg_title));

        aq.id(R.id.navbar_button_left).clicked(this, "onBackPressed");
        aq.id(R.id.regconfirm_heading).text(getResString(R.string.myhkt_confirm_head2)).height(40);
        aq.id(R.id.regconfirm_heading).getTextView().setPadding(basePadding, extralinespace, basePadding, 0);

        regImageButon = (RegImageButton) aq.id(R.id.regconfirm_icon).getView();
        regImageButon.initViews(this, SubService.getResId(subnRec.lob + subnRec.tos), "", 0, 0, regConfirmViewHeight);
        regImageButon.setPadding(basePadding, extralinespace / 2, basePadding, extralinespace / 2);
        regImageButon.setBg(R.drawable.logo_container_gray);

        setLine(R.id.regconfirm_line1);
        setLine(R.id.regconfirm_line2);
        setLine(R.id.regconfirm_line3);
        setLine(R.id.regconfirm_line4);
        setLine(R.id.regconfirm_line5);


        //init info list
        for (int itemCount = 0; itemCount < regConfirmItemList.size(); itemCount++) {

            RegConfirmView regConfirmView = (RegConfirmView) aq.id(regConfirmItemList.get(itemCount).res).getView();
            regConfirmView.initViews(this, regConfirmViewHeight, regConfirmItemList.get(itemCount).title, regConfirmItemList.get(itemCount).content);
            regConfirmView.setPadding(itemCount == 0 ? 0 : basePadding, 0, basePadding, 0);

        }

        aq.id(R.id.regconfirm_txt_agree).text(getResString(R.string.myhkt_confirm_ack1)).textColorId(R.color.hkt_ios_textcolor);
        aq.id(R.id.regconfirm_txt_tc).text(getResString(R.string.myhkt_confirm_ack2)).clicked(this, "onClick");
        aq.id(R.id.regconfirm_txt_pri).text(getResString(R.string.myhkt_confirm_ack3)).clicked(this, "onClick");

        textViewTc = aq.id(R.id.regconfirm_txt_tc).getTextView();
        textViewTc.setPaintFlags(textViewTc.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        textViewTc.setPadding(0, basePadding, 0, basePadding);
        textViewTc.setTextColor(this.getResources().getColor(R.color.hkt_ios_textcolor));

        textViewPri = aq.id(R.id.regconfirm_txt_pri).getTextView();
        textViewPri.setPaintFlags(textViewTc.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        textViewPri.setPadding(0, basePadding, 0, basePadding);
        textViewPri.setTextColor(this.getResources().getColor(R.color.hkt_ios_textcolor));

        aq.id(R.id.regconfirm_relative_layout).getView().setPadding(basePadding, extralinespace, basePadding, 0);


        canceBtn = (HKTButton) aq.id(R.id.regconfirm_btn_cancel).getView();
        canceBtn.setText(getResString(R.string.myhkt_confirm_cancel));
        LinearLayout.LayoutParams params_cancel = (LayoutParams) canceBtn.getLayoutParams();
        params_cancel.width = ViewGroup.LayoutParams.MATCH_PARENT;
        params_cancel.rightMargin = buttonPadding;
        canceBtn.setLayoutParams(params_cancel);
        confirmBtn = (HKTButton) aq.id(R.id.regconfirm_btn_confirm).getView();
        confirmBtn.setText(getResString(R.string.myhkt_confirm_submit));
        LinearLayout.LayoutParams params_confirm = (LayoutParams) confirmBtn.getLayoutParams();
        params_confirm.leftMargin = buttonPadding;
        params_confirm.width = ViewGroup.LayoutParams.MATCH_PARENT;
        confirmBtn.setLayoutParams(params_confirm);
        confirmBtn.setEnabled(false);
        confirmBtn.setAlpha(0.2f);
        aq.id(R.id.regconfirm_checkbox).getCheckBox().setOnCheckedChangeListener(new OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (!isChecked) {
                    confirmBtn.setEnabled(false);
                    int m_color = Color.argb(200, 255, 255, 255);
                    confirmBtn.setAlpha(0.2f);
                } else {
                    confirmBtn.setEnabled(true);
                    confirmBtn.setAlpha(1.0f);
                }
            }

        });

    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.regconfirm_btn_confirm:
                if (aq.id(R.id.regconfirm_checkbox).isChecked()) {

                    Toast.makeText(v.getContext(), "Reg Success", Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(getApplicationContext(), MainMenuActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    startActivity(intent);
                    overridePendingTransition(R.anim.left_slide_in, R.anim.right_slide_out);
                    finish();
                } else {
                    DialogHelper.createSimpleDialog(RegConfirmActivity.this, getResString(R.string.myhkt_confirm_tick));
                }
                break;
            case R.id.regconfirm_txt_tc: {
                Intent intent = new Intent(getApplicationContext(), BrowserActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString(BrowserActivity.INTENTMODULEID, moduleId);
                bundle.putString(BrowserActivity.INTENTLINK, isZh ? domainURL + "/mba/html/tnc_zh.htm" : domainURL + "/mba/html/tnc_en.htm");
                bundle.putString(BrowserActivity.INTENTTITLE, getResString(R.string.myhkt_confirm_ack2));
                intent.putExtras(bundle);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                startActivity(intent);
                overridePendingTransition(R.anim.right_slide_in, R.anim.left_slide_out);
                break;
            }
            case R.id.regconfirm_txt_pri: {
                Intent intent = new Intent(getApplicationContext(), BrowserActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString(BrowserActivity.INTENTMODULEID, moduleId);
                bundle.putString(BrowserActivity.INTENTLINK, isZh ? domainURL + "/mba/html/privacy_policy_zh.htm" : domainURL + "/mba/html/privacy_policy_en.htm");
                bundle.putString(BrowserActivity.INTENTTITLE, getResString(R.string.myhkt_confirm_ack3));
                intent.putExtras(bundle);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                startActivity(intent);
                overridePendingTransition(R.anim.right_slide_in, R.anim.left_slide_out);
                break;
            }
        }
    }

    //Set onClickListener for the views
    public void setClickListener() {
        confirmBtn.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                if (aq.id(R.id.regconfirm_checkbox).isChecked()) {
                    doRegister();
                } else {
                    DialogHelper.createSimpleDialog(RegConfirmActivity.this, getResString(R.string.myhkt_confirm_tick));
                }
            }
        });

        canceBtn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    public void setLine(int resId) {
        aq.id(resId).image(R.drawable.greyline).height(greylineHeight, false);
    }

    // Android Device Back Button Handling
    public final void onBackPressed() {
        //
        //		Intent intent = new Intent(getApplicationContext(), RegBasicActivity.class);
        //		intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        //		startActivity(intent);
        finish();
        overridePendingTransition(R.anim.left_slide_in, R.anim.right_slide_out);
    }

    private final void doRegister() {
        AcMainCra acMainCra = new AcMainCra();
        acMainCra.setISveeRec(sveeRec);
        acMainCra.setICustRec(custRec);
        acMainCra.setIBcifRec(bcifRecCompInfo);
        SubnRec newSRec = subnRec.copyMe();
        if (newSRec.lob == SubnRec.LOB_MOB || newSRec.lob == SubnRec.LOB_IOI) {
            newSRec.lob = SubnRec.WLOB_CSL;
        }
        if (newSRec.lob == SubnRec.LOB_101) {
            newSRec.lob = SubnRec.WLOB_X101;
        }
        acMainCra.setISubnRec(newSRec);
        APIsManager.doRegister(this, acMainCra, false);
    }


    public void debugLog(String tag, String msg) {
        if (debug) Log.i(tag, msg);
    }


    // Translate Reply getCode() to screen-specific string
    private final static String interpretRC_RegnMdu(Reply rRC) {
        if (isReqSpecMsg4DocTy()) {

            if (rRC.getCode().equals(RC.NO_CUSTOMER)) {
                return Utils.getString(me, R.string.REGM_NO_CUSTOMER_NOPP);
            } else if (rRC.getCode().equals(RC.NO_SUBN)) {
                return Utils.getString(me, R.string.REGM_NO_SUBN_NOPP);
            }
        }

        if (rRC.getCode().equals(RC.INACTIVE_CUST)) {
            return Utils.getString(me, R.string.REGM_INACTIVE_CUST);
        }
        if (rRC.getCode().equals(RC.CUS_ALDY_REG)) {
            return Utils.getString(me, R.string.REGM_CUS_ALDY_REG);
        }

//		if (rRC.getCode().equals(RC.RCUS_NALOGINID)) { return Utils.getString(me, R.string.REGM_NALOGINID); }

        if (rRC.getCode().equals(RC.RCUS_IVLOGINID)) {
            return Utils.getString(me, R.string.REGM_IVLOGINID);
        }

        if (rRC.getCode().equals(RC.RCUS_ILLOGINID)) {
            return Utils.getString(me, R.string.REGM_ILLOGINID);
        }

//		if (rRC.getCode().equals(RC.RCUS_RESV_LOGIN)) { return Utils.getString(me, R.string.REGM_RESV_LOGIN); }

//		if (rRC.getCode().equals(RC.RCUS_NAPWD)) { return Utils.getString(me, R.string.REGM_NAPWD); }

        if (rRC.getCode().equals(RC.SVEE_NLPWD)) {
            return Utils.getString(me, R.string.REGM_ILPWD);
        }

        if (rRC.getCode().equals(RC.SVEE_IVPWDCBN)) {
            return Utils.getString(me, R.string.REGM_IVPWDCBN);
        }

        if (rRC.getCode().equals(RC.SVEE_ILNICKNAME)) {
            return Utils.getString(me, R.string.REGM_ILNICKNAME);
        }

        if (rRC.getCode().equals(RC.SVEE_RESV_NCKNM)) {
            return Utils.getString(me, R.string.REGM_RESV_NCKNM);
        }

//		if (rRC.getCode().equals(RC.RCUS_NACTMAIL)) { return Utils.getString(me, R.string.REGM_NACTMAIL); }

        if (rRC.getCode().equals(RC.SVEE_NLCTMAIL)) {
            return Utils.getString(me, R.string.REGM_ILCTMAIL);
        }

        if (rRC.getCode().equals(RC.SVEE_IVCTMAIL)) {
            return Utils.getString(me, R.string.REGM_IVCTMAIL);
        }

        if (rRC.getCode().equals(RC.SVEE_IVCTMOB)) {
            return Utils.getString(me, R.string.REGM_IVMOB);
        }

        if (rRC.getCode().equals(RC.SVEE_IVMOBALRT)) {
            return Utils.getString(me, R.string.REGM_IVMOBALRT);
        }

        if (rRC.getCode().equals(RC.SVEE_IVLANG)) {
            return Utils.getString(me, R.string.REGM_IVLANG);
        }

        if (rRC.getCode().equals(RC.SVEE_IVSECQUS)) {
            return Utils.getString(me, R.string.REGM_IVSECQUS);
        }

        if (rRC.getCode().equals(RC.SVEE_IVSECANS)) {
            return Utils.getString(me, R.string.REGM_IVSECANS);
        }

        if (rRC.getCode().equals(RC.SVEE_NLSECANS)) {
            return Utils.getString(me, R.string.REGM_ILSECANS);
        }

        if (rRC.getCode().equals(RC.CUST_IVDOCTY)) {
            return Utils.getString(me, R.string.REGM_IVDOCTY);
        }

        if (rRC.getCode().equals(RC.CUST_ILDOCNUM)) {
            return Utils.getString(me, R.string.REGM_ILDOCNUM);
        }

        if (rRC.getCode().equals(RC.SUBN_IVLOB)) {
            return Utils.getString(me, R.string.REGM_IVLOB);
        }

        if (rRC.getCode().equals(RC.SUBN_NLACCTNUM)) {
            return Utils.getString(me, R.string.REGM_ILACCTNUM);
        }

        if (rRC.getCode().equals(RC.SUBN_IVACCTNUM)) {
            return Utils.getString(me, R.string.REGM_IVACCTNUM);
        }

        if (rRC.getCode().equals(RC.SUBN_IVSN_LTS)) {
            return Utils.getString(me, R.string.REGM_IVAN_LTS);
        }

        if (rRC.getCode().equals(RC.SUBN_IVSN_MOB)) {
            return Utils.getString(me, R.string.REGM_IVAN_MOB);
        }

//		if (rRC.getCode().equals(RC.SUBN_IVAN_101)) { return Utils.getString(me, R.string.REGM_IVAN_101); }

//		if (rRC.getCode().equals(RC.SUBN_IVAN_NE)) { return Utils.getString(me, R.string.REGM_IVAN_NE); }

        if (rRC.getCode().equals(RC.SUBN_IVSN_PCD)) {
            return Utils.getString(me, R.string.REGM_IVAN_PCD);
        }

        if (rRC.getCode().equals(RC.SUBN_IVAN_TV)) {
            return Utils.getString(me, R.string.REGM_IVAN_TV);
        }

        if (rRC.getCode().equals(RC.SUBN_ILSRVNUM)) {
//			if (me.equals(SubnRec.LOB_LTS)) {
//				return Utils.getString(me, R.string.REGM_ILSRVNUM_LTS);
//			} else if (me.lob.equals(SubnRec.LOB_MOB)) {
//				return Utils.getString(me, R.string.REGM_ILSRVNUM_MOB);
//			} else if (me.lob.equals(SubnRec.LOB_NE)) {
//				return Utils.getString(me, R.string.REGM_ILSRVNUM_MOB);
//			} else if (me.lob.equals(SubnRec.LOB_PCD)) {
//				return Utils.getString(me, R.string.REGM_ILSRVNUM_PCD);
//			} else if (me.lob.equals(SubnRec.LOB_TV)) {
//				return Utils.getString(me, R.string.REGM_NO_SUBN);
//			} else {
            return Utils.getString(me, R.string.REGM_ILSRVNUM);
//			}
        }

        if (rRC.getCode().equals(RC.SUBN_IVSN_LTS)) {
            return Utils.getString(me, R.string.REGM_IVSN_LTS);
        }

        if (rRC.getCode().equals(RC.SUBN_IVSN_MOB)) {
            return Utils.getString(me, R.string.REGM_IVSN_MOB);
        }

        if (rRC.getCode().equals(RC.NO_CUSTOMER)) {
            return Utils.getString(me, R.string.REGM_NO_CUSTOMER);
        }

        if (rRC.getCode().equals(RC.NO_SUBN)) {
            return Utils.getString(me, R.string.REGM_NO_SUBN);
        }

        if (rRC.getCode().equals(RC.CUS_ALDY_REG)) {
            return Utils.getString(me, R.string.REGM_CUS_ALDY_REG);
        }

        if (rRC.getCode().equals(RC.LOGIN_ID_EXIST)) {
            return Utils.getString(me, R.string.REGM_LOGIN_ID_EXIST);
        }

//		if (rRC.getCode().equals(RC.CTMAIL_EXIST)) { return Utils.getString(me, R.string.REGM_CTMAIL_EXIST); }

//		if (rRC.getCode().equals(RC.NO_CUSTOMER_NOPP)) { return Utils.getString(me, R.string.REGM_NO_CUSTOMER_NOPP); }

//		if (rRC.getCode().equals(RC.NO_SUBN_NOPP)) { return Utils.getString(me, R.string.REGM_NO_SUBN_NOPP); }

        // Undefined error mapping
        return ClnEnv.getRPCErrMsg(me, rRC.getCode());
    }

    private final static boolean isReqSpecMsg4DocTy() {
        /* TODO - Remove when Not Necessary */
        /* Special Handling for 101 and O2F */

        String rLob;
        if (me.subnRec == null || me.custRec == null) {
            return false;
        }

        rLob = me.subnRec.lob;
        if (rLob.equals(SubnRec.LOB_101)) {
            return me.custRec.docTy == CustRec.TY_PASSPORT;
        } else if (rLob.equals(SubnRec.LOB_O2F)) {
            return me.custRec.docTy == CustRec.TY_PASSPORT;
        } else if (rLob.equals(SubnRec.LOB_MOB) || rLob.equals(SubnRec.LOB_IOI)) {
            if (me.subnRec.acctNum.trim().length() == SubnRec.CSL_ACCTNUM_LEN) {
                return me.custRec.docTy == CustRec.TY_PASSPORT;
            }
        }

        return (false);
    }

    public void onSuccess(APIsResponse response) {
        super.onSuccess(response);
        if (response != null) {
            if (APIsManager.HELO.equals(response.getActionTy())) {
                debugLog(TAG, "doHelo complete!!");
            } else if (APIsManager.SIGNUP.equals(response.getActionTy())) {
                DialogHelper.createRedirectActivationDialog(this, sveeRec.loginId, sveeRec.pwd);
            }

        }

    }

    public void onFail(APIsResponse response) {
        super.onFail(response);
        if (response != null) {
            if (APIsManager.SIGNUP.equals(response.getActionTy())) {
                // TODO handle difference errorCode
            }

            // General Error Message
            if (!"".equals(response.getMessage()) && response.getMessage() != null) {
                displayDialog(this, response.getMessage());
            } else if (RC.TCSESS_MM.equalsIgnoreCase(response.getReply().getCode())) {
                ivSessDialog();
            } else {
                displayDialog(this, interpretRC_RegnMdu(response.getReply()));
            }
        }
    }

    //TODO
    public class RegConfirmItem {
        String title;
        String content;
        int res;

        RegConfirmItem(int res, String title, String content) {
            this.res = res;
            this.title = title;
            this.content = content;
        }

    }
}