package com.pccw.myhkt.activity;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.graphics.Point;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AlertDialog.Builder;

import com.google.gson.Gson;
import com.pccw.biz.myhkt.R;
import com.pccw.dango.shared.cra.LgiCra;
import com.pccw.dango.shared.entity.BillList;
import com.pccw.dango.shared.entity.GnrlAppt;
import com.pccw.dango.shared.entity.SrvReq;
import com.pccw.dango.shared.entity.SubnRec;
import com.pccw.dango.shared.entity.SveeRec;
import com.pccw.dango.shared.tool.RC;
import com.pccw.myhkt.APIsManager;
import com.pccw.myhkt.APIsManager.OnAPIsListener;
import com.pccw.myhkt.APIsResponse;
import com.pccw.myhkt.ClnEnv;
import com.pccw.myhkt.DialogHelper;
import com.pccw.myhkt.SaveAccountHelper;
import com.pccw.myhkt.Utils;
import com.pccw.myhkt.lib.ui.AAQuery;
import com.pccw.myhkt.lib.ui.HKTButton;
import com.pccw.myhkt.lib.ui.RegInputItem;
import com.pccw.myhkt.model.AcctAgent;
import com.pccw.myhkt.model.HomeButtonItem.MAINMENU;
import com.pccw.myhkt.mymob.activity.MyMobileActivity;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/************************************************************************
 * File : LoginActivity.java
 * Desc : Login Page
 * Name : LoginActivity
 * by 	: Andy Wong
 * Date : 16/11/2015
 * <p>
 * Change History:
 * Date       Modified By		Description
 * ---------- ----------------	-------------------------------
 * 16/11/2015 Andy Wong 		-First draft
 *************************************************************************/

public class LoginActivity extends BaseActivity implements OnAPIsListener {
    private static final String SCREEN_NAME = "Login Screen";
    private boolean debug = false;
    private String TAG = this.getClass().getName();
    private AAQuery aq;
    private final int colMaxNum = 3;
    private int deviceWidth = 0;
    private int colWidth = 0;
    private int extralinespace = 0;
    private int buttonPadding = 0;
    private int greylineHeight = 0;
    private boolean isRememberPw = true;
    private String userid;
    //View
    private Activity me;
    private HKTButton loginBtn;
    private HKTButton regBtn;
    private TextView textViewRememberPw;
    private com.kyleduo.switchbutton.SwitchButton switchButton;
    private LinearLayout forgetPwLayout;
    private RelativeLayout rememberPwLayout;
    private RegInputItem regInputUserID;
    private RegInputItem regInputPw;

    private MAINMENU parentOnClickId = MAINMENU.NULL;
    private SaveAccountHelper saveAccountHelper;            // SQLite database for a new bill checking

    //MyMobile redirection
    private AcctAgent recallAcctAgent; //AcctAgent that came from MyMobActivity for Redirect
    private String recallActionTy; // asyncId that came from MyMobActivity for redirect
    private boolean recallIsSavePwd; // savepassword checkbox setting that came from MyMobActivity for redirect
    private boolean recallIsLogin; // redirect from service list login page

    @Override
    public final void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        debug = getResources().getBoolean(R.bool.DEBUG);
        isLiveChatShown = false;

        initData();
        // reload Data
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            parentOnClickId = (MAINMENU) bundle.getSerializable("CLICKBUTTON");
            if (parentOnClickId == null) parentOnClickId = MAINMENU.NULL;

            recallAcctAgent = (AcctAgent) bundle.getSerializable("SELECTACCTAGENT");
            recallActionTy = bundle.getString("ACTIONTY");
            recallIsSavePwd = bundle.getBoolean("ISSAVEPWD");
            recallIsLogin = bundle.getBoolean("ISMYMOBLOGIN");
        }

        ClnEnv.updateUILocale(getBaseContext(), ClnEnv.getAppLocale(getBaseContext()));

        //init MyMob flag if come from MyMob
        ClnEnv.setMyMobFlag(false);
        setContentView(R.layout.activity_login);
    }

    private void initData() {
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        deviceWidth = size.x;
        extralinespace = (int) getResources().getDimension(R.dimen.extralinespace);
        buttonPadding = (int) getResources().getDimension(R.dimen.reg_logo_padding);
        greylineHeight = (int) getResources().getDimension(R.dimen.greyline_height);
        userid = "";
        me = this;
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        ClnEnv.updateUILocale(getBaseContext(), ClnEnv.getAppLocale(getBaseContext()));
    }

    @Override
    protected final void onResume() {
        super.onResume();
        // Update Locale
        moduleId = getResources().getString(R.string.MODULE_LGIF);
        ClnEnv.updateUILocale(getBaseContext(), ClnEnv.getAppLocale(getBaseContext()));
        firebaseSetting.onScreenView(LoginActivity.this, SCREEN_NAME);
    }

    protected void initUI2() {

        aq = new AAQuery(this);

        //navbar
        aq.navBarButton(R.id.navbar_button_left, R.drawable.btn_back);
        aq.navBarTitle(R.id.navbar_title, getResources().getString(R.string.myhkt_login_title));
        aq.id(R.id.navbar_button_left).clicked(this, "onClick");

        //UAT switch
        if (APIsManager.UAT_domain.equals(ClnEnv.getPref(me, getString(R.string.CONST_PREF_DOMAIN), APIsManager.PRD_domain))) {
            aq.id(R.id.login_uat_switch_txt).visibility(View.VISIBLE);
        } else {
            aq.id(R.id.login_uat_switch_txt).visibility(View.INVISIBLE);
        }


        aq.id(R.id.login_container).getView().setPadding(buttonPadding, 0, buttonPadding, buttonPadding * 2);

        aq.normText(R.id.login_intro, getResString(R.string.myhkt_login_registerintro));
        aq.id(R.id.login_intro).textColorId(R.color.hkt_reg_grey);

        aq.id(R.id.login_intro).height(LinearLayout.LayoutParams.WRAP_CONTENT, false).getTextView().setPadding(0, buttonPadding, 0, buttonPadding);

        String password = "";
        if (ClnEnv.getPref(getApplicationContext(), getString(R.string.CONST_PREF_SAVELOGINID), false) || ClnEnv.getPref(getApplicationContext(), getString(R.string.CONST_PREF_SAVEPASSWORD), false)) {
            userid = ClnEnv.getPref(getApplicationContext(), getString(R.string.CONST_PREF_LOGINID), "");
        }
        if (ClnEnv.getPref(getApplicationContext(), getString(R.string.CONST_PREF_SAVEPASSWORD), false)) {
            password = ClnEnv.getEncPref(getApplicationContext(), ClnEnv.getPref(getApplicationContext(), getString(R.string.CONST_PREF_LOGINID), ""), getString(R.string.CONST_PREF_PASSWORD), "");
            isRememberPw = true;
        } else {
            isRememberPw = false;
        }


        regInputUserID = (RegInputItem) aq.id(R.id.login_input_userid).getView();
        regInputUserID.initViews(this, getResString(R.string.myhkt_login_userid_txt), getResources().getString(R.string.myhkt_login_userid_hint), userid);
        regInputUserID.setPadding(0, 0, 0, buttonPadding);

        regInputPw = (RegInputItem) aq.id(R.id.login_input_pw).getView();
        regInputPw.initViews(this, getResources().getString(R.string.myhkt_login_pw_txt), "", password, InputType.TYPE_TEXT_VARIATION_PASSWORD);
        regInputPw.setPadding(0, 0, 0, buttonPadding);
        regInputPw.setMaxLength(16);

        forgetPwLayout = (LinearLayout) aq.id(R.id.login_forget_pw_layout).getView();
        forgetPwLayout.setOrientation(LinearLayout.HORIZONTAL);
        forgetPwLayout.setGravity(Gravity.RIGHT | Gravity.CENTER_VERTICAL);
        aq.id(R.id.login_forget_pw_layout).clicked(this, "onClick");

        aq.normTextGrey(R.id.login_forget_pw_txt, getResString(R.string.myhkt_LGIF_FORGOT));
        aq.id(R.id.login_forget_pw_txt).height(LinearLayout.LayoutParams.WRAP_CONTENT, false).getTextView().setPadding(0, buttonPadding, buttonPadding / 3, buttonPadding);
        aq.id(R.id.login_forget_pw_txt).textColorId(R.color.hkt_buttonblue);

        aq.id(R.id.login_forget_pw_iv).image(R.drawable.btn_details);

        aq.line(R.id.login_line1, R.id.login_line2);
        aq.padding(R.id.login_line1, 0, 0, 0, 0);
        aq.padding(R.id.login_line2, 0, 0, 0, 0);

        rememberPwLayout = (RelativeLayout) aq.id(R.id.login_remember_pw_layout).getView();
        rememberPwLayout.setGravity(Gravity.RIGHT | Gravity.CENTER_VERTICAL);

        textViewRememberPw = aq.id(R.id.login_remember_pw_txt).getTextView();
        aq.normTextGrey(R.id.login_remember_pw_txt, getResString(R.string.myhkt_login_remember_pw_txt));
        aq.id(R.id.login_remember_pw_txt).height(LinearLayout.LayoutParams.WRAP_CONTENT, false);
        textViewRememberPw.setPadding(0, buttonPadding, extralinespace, buttonPadding);
        textViewRememberPw.setGravity(Gravity.CENTER_VERTICAL);

        switchButton = aq.switchButton(R.id.login_remember_pw_sb);
        switchButton.setChecked(isRememberPw);
        switchButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                isRememberPw = isChecked;
            }
        });

        //init Btns
        loginBtn = (HKTButton) aq.id(R.id.login_login_btn).getView();
        loginBtn.initViews(this, getResources().getString(R.string.myhkt_login_login_txt), LayoutParams.MATCH_PARENT);
        aq.marginpx(R.id.login_login_btn, 0, buttonPadding * 2, 0, buttonPadding);
        aq.id(R.id.login_login_btn).clicked(this, "onClick");

        regBtn = (HKTButton) aq.id(R.id.login_reg_btn).getView();
        regBtn.initViews(this, HKTButton.TYPE_WHITE_BLUE, getResources().getString(R.string.myhkt_login_reg_txt), LayoutParams.MATCH_PARENT);
        aq.id(R.id.login_reg_btn).clicked(this, "onClick");

    }

    // Android Device Back Button Handling
    public final void onBackPressed() {
        //			clearNotificationData();
        Intent intent = new Intent(me.getApplicationContext(), MainMenuActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        me.startActivity(intent);
        overridePendingTransition(R.anim.left_slide_in, R.anim.right_slide_out);
        finish();
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.navbar_button_left:
                onBackPressed();
                break;
            case R.id.login_login_btn:
                //UAT PRD switch for UAT only, DO NOT USE AT POST RELEASE VERSION
                if (getResources().getBoolean(R.bool.UATPRDSWITCH) && "changemode".equals(regInputUserID.getInput())) {
                    DialogInterface.OnClickListener onPositiveClickListener = new DialogInterface.OnClickListener() {
                        Bundle rbundle;

                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            ClnEnv.setPref(me, getString(R.string.CONST_PREF_DOMAIN), APIsManager.UAT_domain);
                            ClnEnv.setPref(me, getString(R.string.CONST_PREF_SHOPHOST), APIsManager.UAT_shop_host);
                            ClnEnv.setPref(me, getString(R.string.CONST_PREF_SALT), APIsManager.UAT_salt);
                            ClnEnv.setPref(me, getString(R.string.CONST_PREF_FTP), APIsManager.UAT_FTP);
                            ClnEnv.setPref(me, getString(R.string.CONST_PREF_LIVECHAT), APIsManager.UAT_livechat);
                            ClnEnv.setPref(me, getString(R.string.CONST_PREF_TRACKER1), APIsManager.UAT_tracker1);
                            ClnEnv.setPref(me, getString(R.string.CONST_PREF_TRACKER2), APIsManager.UAT_tracker2);
                            ClnEnv.setPref(me, getString(R.string.CONST_PREF_MM_TRACKER1), APIsManager.UAT_mm_tracker1);
                            ClnEnv.setPref(me, getString(R.string.CONST_PREF_MM_TRACKER2), APIsManager.UAT_mm_tracker2);

                            aq.id(R.id.login_uat_switch_txt).visibility(View.VISIBLE);
                        }
                    };
                    DialogInterface.OnClickListener onNegativeClickListener = new DialogInterface.OnClickListener() {
                        Bundle rbundle;

                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            ClnEnv.setPref(me, getString(R.string.CONST_PREF_DOMAIN), APIsManager.PRD_domain);
                            ClnEnv.setPref(me, getString(R.string.CONST_PREF_SHOPHOST), APIsManager.PRD_shop_host);
                            ClnEnv.setPref(me, getString(R.string.CONST_PREF_SALT), APIsManager.PRD_salt);
                            ClnEnv.setPref(me, getString(R.string.CONST_PREF_FTP), APIsManager.PRD_FTP);
                            ClnEnv.setPref(me, getString(R.string.CONST_PREF_LIVECHAT), APIsManager.PRD_livechat);
                            ClnEnv.setPref(me, getString(R.string.CONST_PREF_TRACKER1), APIsManager.PRD_tracker1);
                            ClnEnv.setPref(me, getString(R.string.CONST_PREF_TRACKER2), APIsManager.PRD_tracker2);
                            ClnEnv.setPref(me, getString(R.string.CONST_PREF_MM_TRACKER1), APIsManager.PRD_mm_tracker1);
                            ClnEnv.setPref(me, getString(R.string.CONST_PREF_MM_TRACKER2), APIsManager.PRD_mm_tracker2);
                            aq.id(R.id.login_uat_switch_txt).visibility(View.INVISIBLE);
                        }
                    };
                    DialogHelper.createSimpleDialog(me, "Select host", "External UAT", onPositiveClickListener, "PRD (Pilot)", onNegativeClickListener);
                    break;
                }

                if ("".equals(regInputUserID.getInput())) {
                    displayDialog(this, Utils.getString(this, R.string.LGIM_IVLOGIN_ID));
                } else if ("".equals(regInputPw.getInput())) {
                    displayDialog(this, Utils.getString(this, R.string.LGIM_LOGIN_FAIL));
                } else {
                    doLogin();
                    // Enable Live Chat Disclaimer
                    Utils.setLiveChatDisclaimerFlag(true);
                }
                break;
            case R.id.login_reg_btn:
                Intent intent;
                intent = new Intent(me.getApplicationContext(), RegBasicActivity.class);
                startActivity(intent);
                overridePendingTransition(R.anim.right_slide_in, R.anim.left_slide_out);
                break;
            case R.id.login_forget_pw_layout:
                Intent intent_forgetPassword;
                intent_forgetPassword = new Intent(me.getApplicationContext(), ForgetPasswordActivity.class);
                startActivity(intent_forgetPassword);
                overridePendingTransition(R.anim.right_slide_in, R.anim.left_slide_out);
                break;
        }
    }

    private final void doLogin() {
        SveeRec sveeRec = new SveeRec();
        sveeRec.loginId = regInputUserID.getInput();
        sveeRec.pwd = regInputPw.getInput();
        LgiCra lgiCra = new LgiCra();
        lgiCra.setISveeRec(sveeRec);
        //		APIsManager.doLogin(this, lgiCra, isRememberPw);
        APIsManager.doLoginTest(this, lgiCra, isRememberPw);
    }

    @Override
    protected final void onPause() {
        super.onPause();
        debugLog(TAG, "onPause...");
        userid = regInputUserID.getInput();
    }

    public void onSuccess(APIsResponse response) {
        super.onSuccess(response);
        if (response != null) {
            if (APIsManager.HELO.equals(response.getActionTy())) {
                debugLog(TAG, "doHelo complete!!");
            } else if (APIsManager.LGI.equals(response.getActionTy())) {
                LgiCra lgiCra = (LgiCra) response.getCra();
                Gson gson = new Gson();
                String result = gson.toJson(lgiCra);
                if (debug) Utils.showLog(TAG, result);
//				Log.d(TAG, result.substring(0, 3000));
//				Log.d(TAG, result.substring(3001, 6000));
//				Log.d(TAG, result.substring(6001, result.length()));

//				if(lgiCra.getOQualSvee() != null && lgiCra.getOQualSvee().getCustRec() != null &&
//						!lgiCra.getOQualSvee().getCustRec().phylum.equals(CustRec.PH_COMM)) {
//					displayDialog(this, getString(R.string.login_phylum_invalid));  		//Show error message if it's not Commercial Account
//				} else {

                ClnEnv.setLgiCra(lgiCra);
                // Keeping the username and password in memory
                ClnEnv.setSessionLoginID(regInputUserID.getInput());
                ClnEnv.setSessionPassword(regInputPw.getInput());
                ClnEnv.setSessionSavePw(isRememberPw);
                // Save Account Type
                ClnEnv.setPref(this, getString(R.string.CONST_PREF_PREMIER_FLAG), ClnEnv.getQualSvee().getCustRec().isPremier());
                ClnEnv.setSessionPremierFlag(ClnEnv.getQualSvee().getCustRec().isPremier());

                ClnEnv.setPref(this, getString(R.string.CONST_PREF_SAVELOGINID), true);
                ClnEnv.setPref(this, getString(R.string.CONST_PREF_LOGINID), regInputUserID.getInput());

                if (isRememberPw) {
                    // Saving password implies saving loginid
                    ClnEnv.setPref(me.getApplicationContext(), me.getString(R.string.CONST_PREF_SAVEPASSWORD), true);
                    ClnEnv.setEncPref(me.getApplicationContext(), regInputUserID.getInput(), me.getString(R.string.CONST_PREF_PASSWORD), regInputPw.getInput());
                } else {
                    // Not saving password - remove the saved password
                    ClnEnv.setPref(me.getApplicationContext(), me.getString(R.string.CONST_PREF_SAVEPASSWORD), false);
                    ClnEnv.setEncPref(me.getApplicationContext(), "", me.getString(R.string.CONST_PREF_PASSWORD), "");
                }

                // New Bill Indicator Process
                saveAccountHelper = SaveAccountHelper.getInstance(me);
                // Removed all records if last bill date is one year or earlier than current Date
                saveAccountHelper.RemoveRecordForInitial(lgiCra.getServerTS());
                // make sure the BillAry Not null and empty Array
                if (lgiCra.getOBillListAry() != null) {
                    for (BillList binqcra : lgiCra.getOBillListAry()) {
                        if (binqcra.getOBillAry() != null && binqcra.getOBillAry().length > 0) {
                            try {
                                // Check Database only if the bill date returned
                                if (!"".equalsIgnoreCase(binqcra.getOBillAry()[0].getInvDate())) {
                                    // Get lastBilldate from database
                                    String lastBillDate = saveAccountHelper.getDateByLoginIDAndAcctNum(ClnEnv.getSessionLoginID(), binqcra.getIAcct().getAcctNum());
                                    if (lastBillDate == null) {
                                        saveAccountHelper.insert(ClnEnv.getSessionLoginID(), binqcra.getIAcct().getAcctNum());
                                    } else if (CompareLastBillDate(lastBillDate, binqcra.getOBillAry()[0].getInvDate())) {
                                        saveAccountHelper.updateFlag(ClnEnv.getSessionLoginID(), binqcra.getIAcct().getAcctNum());
                                    }
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }


                // Appointment Indicator Process
                // Appt Alert Icon
                ClnEnv.setPref(me.getApplicationContext(), me.getString(R.string.CONST_PREF_APPTIND_FLAG), false);
                if (lgiCra.getOGnrlApptAry() != null) {
                    for (int i = 0; i < lgiCra.getOGnrlApptAry().length; i++) {
                        GnrlAppt gnrlAppt = lgiCra.getOGnrlApptAry()[i];
                        if (Utils.CompareDateAdd3(me, gnrlAppt.getApptStDT(), lgiCra.getServerTS(), getString(R.string.input_datetime_format))) {
                            // Have an appointment
                            ClnEnv.setPref(me.getApplicationContext(), me.getString(R.string.CONST_PREF_APPTIND_FLAG), true);
                            break;
                        }
                    }
                }

                if (lgiCra.getOSrvReqAry() != null) {
                    for (int i = 0; i < lgiCra.getOSrvReqAry().length; i++) {
                        SrvReq srvReq = lgiCra.getOSrvReqAry()[i];
                        if (Utils.CompareDateAdd3(me, srvReq.getApptTS().getApptDate(), lgiCra.getServerTS(), getString(R.string.input_date_format))) {
                            // Have an appointment
                            ClnEnv.setPref(me.getApplicationContext(), me.getString(R.string.CONST_PREF_APPTIND_FLAG), true);
                            break;
                        }
                    }
                }

                // SR Array
                if (lgiCra.getOSrvReqAry() != null) {
                    for (int i = 0; i < lgiCra.getOSrvReqAry().length; i++) {
                        SrvReq srvReq = lgiCra.getOSrvReqAry()[i];
                    }
                }

                RedirectTargetScreen();

                int rl = ClnEnv.getQualSvee().getSubnRecAry().length;
                int rx, rlz;
                //live accounts
                for (rx = 0; rx < rl; rx++) {

                    SubnRec subnRec = ClnEnv.getQualSvee().getSubnRecAry()[rx];
                    if (debug) Log.i("SubnRec", subnRec.lob);
                }

                //zombie accounts
                rlz = ClnEnv.getQualSvee().getZmSubnRecAry().length;
                for (rx = 0; rx < rlz; rx++) {
                    SubnRec subnRec = ClnEnv.getQualSvee().getZmSubnRecAry()[rx];
                    if (debug) Log.i("XSubnRec", subnRec.lob);
                }
//				}
            }
        }
    }

    public void onFail(APIsResponse response) {
        super.onFail(response);
        Intent intent;
        if (response != null) {
            if (APIsManager.LGI.equals(response.getActionTy())) {
                try {
                    LgiCra lgiCra = (LgiCra) response.getCra();
                    if (lgiCra.getReply().getCode().equals(RC.FILL_SVEE)
                            || lgiCra.getReply().getCode().equals(RC.FILL_SVEE4CLBL)
                            || lgiCra.getReply().getCode().equals(RC.FILL_SVEE4CLAV)
                            || lgiCra.getReply().getCode().equals(RC.FILL_SVEE4CLUB)
                            || lgiCra.getReply().getCode().equals(RC.FILL_SVEE_NOTNC)
                            || lgiCra.getReply().getCode().equals(RC.FILL_SVEE4CLBL_NOTNC)) {
                        // RC_FILL_SVEE
                        // Keeping the username and password in memory
                        theClubRedirectDialog(lgiCra, regInputUserID.getInput(), regInputPw.getInput(), isRememberPw, lgiCra.getReply().getCode());
                    } else if (lgiCra.getReply().getCode().equals(RC.ACTV_SVEE)) {
                        DialogHelper.createRedirectActivationDialog(this, regInputUserID.getInput(), regInputPw.getInput());
                    } else if (lgiCra.getReply().getCode().equals(RC.RESET_SVEE) || lgiCra.getReply().getCode().equals(RC.INITPWD_SVEE)) {
                        // RC_RESET_RCUS
                        Bundle rbundle = new Bundle();
                        rbundle.putString("RESETPWD", regInputPw.getInput());
                        rbundle.putBoolean("SAVEPWD", isRememberPw);
                        rbundle.putSerializable("SVEEREC", lgiCra.getOQualSvee().getSveeRec());
                        rbundle.putString("ERRCODE", lgiCra.getReply().getCode());
                        rbundle.putSerializable("LOGINCRA", lgiCra);
                        intent = new Intent(me.getApplicationContext(), ResetPwdActivity.class);
                        intent.putExtras(rbundle);
                        me.startActivity(intent);
                        me.overridePendingTransition(R.anim.right_slide_in, R.anim.left_slide_out);
                    } else {
                        // General Error Message
                        if (!"".equals(response.getMessage()) && response.getMessage() != null) {
                            displayDialog(this, response.getMessage());
                        } else if (RC.TCSESS_MM.equalsIgnoreCase(response.getReply().getCode())) {
                            BaseActivity.ivSessDialog();
                        } else {
                            displayDialog(this, ClnEnv.getRPCErrMsg(this, response.getReply().getCode()));
                        }
                    }
                } catch (Exception e) { // to catch class cast exception when login receive 101.
                    // General Error Message
                    if (!"".equals(response.getMessage()) && response.getMessage() != null) {
                        displayDialog(this, response.getMessage());
                    } else if (RC.TCSESS_MM.equalsIgnoreCase(response.getReply().getCode())) {
                        BaseActivity.ivSessDialog();
                    } else {
                        displayDialog(this, ClnEnv.getRPCErrMsg(this, response.getReply().getCode()));
                    }
                }
            } else {
                // General Error Message
                if (!"".equals(response.getMessage()) && response.getMessage() != null) {
                    displayDialog(this, response.getMessage());
                } else if (RC.TCSESS_MM.equalsIgnoreCase(response.getReply().getCode())) {
                    ivSessDialog();
                } else {
                    displayDialog(this, ClnEnv.getRPCErrMsg(this, response.getReply().getCode()));
                }
            }
        }
    }

    private final boolean CompareLastBillDate(String lastBilldate, String newlastBilldate) {
        SimpleDateFormat inputdf = new SimpleDateFormat("yyyyMMdd", Locale.US);
        Date lastBillDate, newlastBillDate;
        try {
            lastBillDate = inputdf.parse(lastBilldate);
            newlastBillDate = inputdf.parse(newlastBilldate);

            if (lastBillDate.compareTo(newlastBillDate) < 0) {
                return true;
            }
        } catch (Exception e) {
            // fail to convert date
            e.printStackTrace();
        }
        return false;
    }

    // Redirect to Activation
    protected final void redirectDialog(String message, final LgiCra logincra) {
        AlertDialog.Builder builder = new Builder(LoginActivity.this);
        builder.setMessage(message);
        builder.setPositiveButton(Utils.getString(me, R.string.btn_ok), new OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                Intent intent = new Intent(getApplicationContext(), ActivationActivity.class);
                // Loading required information to Activation
                Bundle bundle = new Bundle();
                bundle.putString("LOGINID", regInputUserID.getInput());
                bundle.putString("PWD", regInputPw.getInput());
                intent.putExtras(bundle);
                startActivity(intent);
                overridePendingTransition(R.anim.right_slide_in, R.anim.left_slide_out);
                finish();
            }
        });
        builder.setOnCancelListener(new Dialog.OnCancelListener() {
            public void onCancel(DialogInterface dialog) {
                dialog.dismiss();
                Intent intent = new Intent(getApplicationContext(), ActivationActivity.class);
                // Loading required information to Activation
                Bundle bundle = new Bundle();
                bundle.putString("LOGINID", regInputUserID.getInput());
                bundle.putString("PWD", regInputPw.getInput());
                intent.putExtras(bundle);
                startActivity(intent);
                overridePendingTransition(R.anim.right_slide_in, R.anim.left_slide_out);
                finish();
            }
        });
        builder.create().show();
    }

    private final void RedirectTargetScreen() {
        Intent intent;
        Bundle bundle;
        // forward to the required screen
        switch (parentOnClickId) {
            //			case R.id.adapter_mymob_accountlist_front: //MyMob List Item Click
            //				intent = new Intent(me.getApplicationContext(), MyMobileActivity.class);
            //				me.startActivity(intent);
            //				me.overridePendingTransition(R.anim.right_slide_in, R.anim.left_slide_out);
            //				break;
            case MYMOB:
                if (debug) Log.i("REDIRECTTARGETSCREEN", "REDIRECTTARGETSCREEN");
                intent = new Intent(me.getApplicationContext(), MyMobileActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                if (recallAcctAgent != null) {
                    bundle = new Bundle();
                    bundle.putSerializable("SELECTACCTAGENT", recallAcctAgent);
                    bundle.putString("ACTIONTY", recallActionTy);
                    bundle.putBoolean("ISSAVEPWD", recallIsSavePwd);
                    bundle.putBoolean("ISMYMOBLOGIN", recallIsLogin);
                    intent.putExtras(bundle);
                }
                me.startActivity(intent);
                me.overridePendingTransition(R.anim.right_slide_in, R.anim.left_slide_out);
                finish();
                break;
            case MYACCT:
                intent = new Intent(me.getApplicationContext(), ServiceListActivity.class);
                me.startActivity(intent);
                me.overridePendingTransition(R.anim.right_slide_in, R.anim.left_slide_out);
                finish();
                break;
            case MYAPPT:
                //				intent = new Intent(me.getApplicationContext(), AppointmentListActivity.class);
                //				me.startActivity(intent);
                //				me.overridePendingTransition(R.anim.right_slide_in, R.anim.left_slide_out);
                break;
            case MYPROFILE:
                intent = new Intent(me.getApplicationContext(), MyProfileActivity.class);
                me.startActivity(intent);
                me.overridePendingTransition(R.anim.right_slide_in, R.anim.left_slide_out);
                finish();
                break;
            //			case R.id.helphktpremier_button_livechat:
            //			case R.id.helpconsumer_button_livechat:
            //				Utils.setCurrentModule(Utils.getString(me, R.string.MODULE_CONTACT_US));
            //				intent = new Intent(me.getApplicationContext(), LiveChatActivity.class);
            //				me.startActivity(intent);
            //				me.overridePendingTransition(R.anim.right_slide_in, R.anim.left_slide_out);
            //				break;
            //			case R.id.mainmenu_button_livechat:
            //				Utils.setCurrentModule(Utils.getString(me, R.string.MODULE_HOME));
            //				intent = new Intent(me.getApplicationContext(), LiveChatActivity.class);
            //				me.startActivity(intent);
            //				me.overridePendingTransition(R.anim.right_slide_in, R.anim.left_slide_out);
            //				break;
            case DIRECTINQ:
                //				intent = new Intent(me.getApplicationContext(), DirectoryInquiryActivity.class);
                //				me.startActivity(intent);
                //				me.overridePendingTransition(R.anim.right_slide_in, R.anim.left_slide_out);
                break;
            case IDD:
                intent = new Intent(me.getApplicationContext(), IDDActivity.class);
                bundle = new Bundle();
                bundle.putBoolean(IDDActivity.BUNDLEIS0060, true);
                intent.putExtras(bundle);
                me.startActivity(intent);
                me.overridePendingTransition(R.anim.right_slide_in, R.anim.left_slide_out);
                finish();
                break;
            case CONTACTUS:
                intent = new Intent(me.getApplicationContext(), ContactUsActivity.class);
                bundle = new Bundle();
                intent.putExtras(bundle);
                me.startActivity(intent);
                me.overridePendingTransition(R.anim.right_slide_in, R.anim.left_slide_out);
                finish();
                break;
            //			case R.id.plantv_fib_btn:
            //				Utils.setCurrentModule(Utils.getString(me, R.string.MODULE_TV_PONCHK));
            //				intent = new Intent(me.getApplicationContext(), LiveChatActivity.class);
            //				me.startActivity(intent);
            //				me.overridePendingTransition(R.anim.right_slide_in, R.anim.left_slide_out);
            //				break;
            //			case R.id.planpcd_fib_btn:
            //				Utils.setCurrentModule(Utils.getString(me, R.string.MODULE_PCD_PONCHK));
            //				intent = new Intent(me.getApplicationContext(), LiveChatActivity.class);
            //				me.startActivity(intent);
            //				me.overridePendingTransition(R.anim.right_slide_in, R.anim.left_slide_out);
            //				break;
            //			case R.id.mymobile_navbar:
            //				intent = new Intent(me.getApplicationContext(), MyMobileActivity.class);
            //				intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            //				if (recallAcctAgent != null) {
            //					bundle = new Bundle();
            //					bundle.putSerializable("SELECTACCTAGENT", recallAcctAgent);
            //					bundle.putSerializable("ASYNCID", recallAsyncId);
            //					bundle.putBoolean("ISSAVEPWD", recallIsSavePwd);
            //					intent.putExtras(bundle);
            //				}
            //				me.startActivity(intent);
            //				me.overridePendingTransition(R.anim.right_slide_in, R.anim.left_slide_out);
            //				break;
            case SETTING:
                intent = new Intent(me.getApplicationContext(), SettingsActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                me.startActivity(intent);
                me.overridePendingTransition(R.anim.left_slide_in, R.anim.right_slide_out);
                finish();
                break;

            default:
                intent = new Intent(me.getApplicationContext(), MainMenuActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                me.startActivity(intent);
                me.overridePendingTransition(R.anim.left_slide_in, R.anim.right_slide_out);
                finish();
                break;
        }
        me.finish();
    }


    protected final void theClubRedirectDialog(LgiCra lgicra, String loginId, String pwd, boolean savepw, String errcode) {
        Intent intent;
        Bundle bundle;
        intent = new Intent(getApplicationContext(), RegAccInfoActivity.class);
        bundle = new Bundle();
        bundle.putString("LOGINID", loginId);
        bundle.putString("PWD", pwd);
        bundle.putBoolean("SAVEPW", savepw);
        bundle.putString("ERRCODE", errcode);
        bundle.putSerializable("LOGINCRA", lgicra);
        intent.putExtras(bundle);
        startActivity(intent);
        overridePendingTransition(R.anim.right_slide_in, R.anim.left_slide_out);
    }
}