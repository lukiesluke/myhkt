package com.pccw.myhkt.service;

/************************************************************************
File       : LineTestIntentService.java
Desc       : Background Service For LineTest
Name       : LineTestIntentService
Created by : Vincent Fung
Date       : 12/02/2014

Change History:
Date       Modified By			Description
---------- ----------------	-------------------------------
12/02/2014  Vincent Fung		- First draft 
13/02/2014  Vincent Fung		1.) Created IntentService for background lineTest
								2.) Allowed to Normal LineTest and Reset Modem
								3.) Added Notification to foreground or push Notification to mobile device
24/02/2014 Vincent Fung			- Code cleanup
 *************************************************************************/

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import android.app.ActivityManager;
import android.app.ActivityManager.RunningTaskInfo;
import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.BitmapDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import androidx.core.app.NotificationCompat;
import android.util.Log;

import com.google.gson.Gson;
import com.pccw.dango.shared.cra.LnttCra;
import com.pccw.dango.shared.entity.SubnRec;
import com.pccw.dango.shared.tool.RC;
import com.pccw.myhkt.APIsManager;
import com.pccw.myhkt.APIsManager.OnAPIsListener;
import com.pccw.myhkt.APIsResponse;
import com.pccw.myhkt.ClnEnv;
import com.pccw.myhkt.GlobalDialog;
import com.pccw.biz.myhkt.R;
import com.pccw.myhkt.Utils;
import com.pccw.myhkt.activity.BaseActivity;
import com.pccw.myhkt.activity.ServiceListActivity;
import com.pccw.myhkt.activity.SplashActivity;
import com.pccw.myhkt.model.LnttAgent;
import com.pccw.myhkt.model.PushData;

public class LineTestIntentService extends IntentService implements OnAPIsListener {
	private boolean										debug					= false;

	//	private static AsyncTask<LnttGrq, Void, LnttCra>	doSubmitLnttAsyncTask	= null;
	//	private static AsyncTask<LnttGrq, Void, LnttCra>	doLnttResetAsyncTask	= null;
	private static LineTestIntentService				me;
	public static int									lnttRtryCnt;
	private Timer										timer					= new Timer();
	private static LnttAgent							lnttAgent				= null;

	private String										errorMsg				= "";
	private String TAG = "Lntt";
	public LineTestIntentService() {
		super("LineTestIntentService");
	}

	@Override
	public void onCreate() {
		// The service is being created
		debug = getResources().getBoolean(R.bool.DEBUG);
		me = this;	
	}

	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}

	@Override
	protected void onHandleIntent(Intent intent) {
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		Bundle lnttBundle = intent.getExtras();
		if (lnttBundle.getSerializable("LNTTAGENT") != null) {
			if (debug) Log.i(TAG, "Service start1");
			lnttAgent = (LnttAgent) lnttBundle.getSerializable("LNTTAGENT");
			if ("".equalsIgnoreCase(Utils.getPrefLnttAgent(me).getLnttSrvNum())) {
				if (debug) Log.i(TAG, "Service start2" + lnttAgent.getLtrsRecStatus());
				lnttRtryCnt = 0;
				// Saved Service Number for identifying in ServiceList
				ClnEnv.setPref(me.getApplicationContext(), me.getString(R.string.CONST_PREF_LNTT_AGENT), Utils.serialize(lnttAgent));
				if ("T".equalsIgnoreCase(lnttAgent.getLtrsRecStatus())) {
					doResetModem();
				} else {
					doSubmitLnttLoop();
				}
			} else {
				if (debug) Log.i(TAG, "Service start3");
				// Stop Service if the srvNum is not Empty String
				// means Service restarted in unknown case [i.e. user force to kill the apps when line Test is running]
				me.stopSelf();
				if (debug) Log.i(TAG, "Service start3.1");
				Utils.clearLnttService(me);
			}
		} else {
			if (debug) Log.i(TAG, "Service start4");
			me.stopSelf();
			Utils.clearLnttService(me);
		}
		return Service.START_REDELIVER_INTENT;
	}

	public final void doLineTestSchedule(com.pccw.dango.shared.cra.LnttCra lnttcra) {
		// clear timer to make sure one task allowed
		timer.cancel();
		timer.purge();
		timer = new Timer();

		try {
			timer.schedule(new TimerTask() {
				@Override
				public void run() {
					doSubmitLnttLoop();
				} 
			}, 30000);
		} catch (Exception e) {
			// fail to sleep
			lnttRtryCnt = 6;
			// Asynctask completed. Cleanup myself
			//			doSubmitLnttAsyncTask = null;
			errorMsg = Utils.getString(me, R.string.LTTM_TIMEOUT);
			lnttAgent.setResultMsg(errorMsg);
			lnttAgent.setLnttCra(lnttcra);

			stopLnttService(errorMsg);
		}
	}

	private final void doResetModemSchedule(LnttCra lnttcra) {
		// clear previous line test reset Record
		/* lnttAgent.getLnttCra().setLtrsRec(new LtrsRec()); */
		// clear timer to make sure one task allowed
		timer.cancel();
		timer.purge();
		timer = new Timer();

		try {
			timer.schedule(new TimerTask() {
				@Override
				public void run() {
					doResetModem();
				}
			}, 30000);
		} catch (Exception e) {
			// fail to sleep
			lnttRtryCnt = 6;
			// Asynctask completed. Cleanup myself
			errorMsg = Utils.getString(me, R.string.LTTM_TIMEOUT);
			lnttAgent.setResultMsg(errorMsg);
			lnttAgent.setLnttCra(lnttcra);

			stopLnttService(errorMsg);
		}
	}

	private final void doSubmitLnttLoop() {
		if (debug) Log.i(TAG, "doSubmitLnttLoop");
		if (lnttAgent != null) {
			LnttCra lnttCra = lnttAgent.getLnttCra();
			APIsManager.doSubmitLntt(this, lnttCra);			

		} else {
			// may be stop at MainMenu
			lnttAgent = new LnttAgent();
			stopLnttService(Utils.getString(me, R.string.MYHKT_LT_ERR_EXPIRED));
		}
	}

	private final void doResetModem() {
		if (debug) Log.i(TAG, "doResetModem");
		if (lnttAgent != null) {
			if (debug) Log.i(TAG, "doResetModem1");
			LnttCra lnttCra = lnttAgent.getLnttCra();
			lnttCra.setILtrsRec(lnttCra.getOLtrsRec());
			lnttCra.setOLtrsRec(null);
			APIsManager.doResetModem(this, lnttCra);
		} else {
			lnttAgent = new LnttAgent();
			stopLnttService(Utils.getString(me, R.string.MYHKT_LT_ERR_EXPIRED));
		}
	}

	@Override
	public void onSuccess(APIsResponse response) {
		LnttCra mLnttCra = null;
		if (debug) Log.i(TAG, "Success");
	
		if (response != null) {
			if (APIsManager.LNTT.equals(response.getActionTy())) {
				mLnttCra = (LnttCra) response.getCra();
				if (debug) Utils.showLog(TAG, new Gson().toJson(mLnttCra));
				lnttAgent.setLnttCra(mLnttCra.copyMe());
				me.errorMsg = "";
				lnttAgent.setResultMsg(me.errorMsg);
				me.stopLnttService(me.errorMsg);
			} else if (APIsManager.RTRT.equals(response.getActionTy())) {
				mLnttCra = (LnttCra) response.getCra();
				lnttAgent.setLnttCra(mLnttCra.copyMe());
				me.errorMsg = "";
				lnttAgent.setResultMsg(me.errorMsg);
				me.stopLnttService(me.errorMsg);
			}
		}
	}

	@Override
	public void onFail(APIsResponse response) {
		LnttCra mLnttCra = null;
		if (debug) Log.i(TAG, "Fail");
		if (response != null) {
			if (debug) Log.i(TAG, "Fail1" + response.getActionTy() + response.getMessage());
			if (!"".equals(response.getMessage()) && response.getMessage() != null) {
				lnttAgent.setResultMsg(response.getMessage());
				//Pass to MainMenu to show error msg
				ClnEnv.setPref(me.getApplicationContext(), me.getString(R.string.CONST_PREF_LNTT_AGENT), Utils.serialize(lnttAgent));
				me.stopLnttService(response.getMessage());
				//				displayDialog(this, response.getMessage());
			} else if (RC.TCSESS_MM.equalsIgnoreCase(response.getReply().getCode())) {
				BaseActivity.ivSessDialog();
				Utils.clearLnttService(me.getApplicationContext());
			} else if (APIsManager.LNTT.equals(response.getActionTy())) {
				mLnttCra = (LnttCra) response.getCra();
				lnttAgent.setLnttCra(mLnttCra.copyMe());
				if (mLnttCra.getReply().isBusy() || mLnttCra.getReply().getCode().equals(RC.USRA_LT_PREP)){

					// RC_USRA_LT_PREP
					if (lnttRtryCnt < 6 && lnttAgent != null) {
						lnttRtryCnt++;

						// Update LtrsRec
						ClnEnv.setPref(me.getApplicationContext(), me.getString(R.string.CONST_PREF_LNTT_AGENT), Utils.serialize(lnttAgent));
						// me.lnttCra.setLtrsRec(lnttcra.getLtrsRec());
						// sleep 30 seconds and retry doSubmitLntt();
						me.doLineTestSchedule(mLnttCra);
					} else {
						// Out of 6 times retry
						me.errorMsg = Utils.getString(me, R.string.LTTM_TIMEOUT);
						lnttAgent.setResultMsg(me.errorMsg);
						me.stopLnttService(me.errorMsg);
					}
				} else {
					if (mLnttCra.getReply().getCode().equals(RC.GWTGUD_MM) || mLnttCra.getReply().getCode().equals(RC.ALT)) {
						// GWTGUD_MM
						// ALT
						me.errorMsg = ClnEnv.getRPCErrMsg(me, RC.GWTGUD_MM);
					} else if (mLnttCra.getReply().getCode().equals(RC.USRA_LT_ERR) || mLnttCra.getReply().getCode().equals(RC.USRA_LT_UXSTS)) {
						// USRA_LT_ERR
						// USRA_LT_UXSTS
						me.errorMsg = Utils.getString(me, R.string.LTTM_ERR);
					} else {
						// RC_IVDATA
						// RC_NOT_AUTH
						// RC_UXSVLTERR
						me.errorMsg = Utils.getString(me, R.string.MYHKT_LT_ERR_EXPIRED);
					}

					lnttAgent.setResultMsg(me.errorMsg);
					me.stopLnttService(me.errorMsg);					
				}
			} else if (APIsManager.RTRT.equals(response.getActionTy())) {
				if (debug) Log.i(TAG, "Fail2");
				Gson gson = new Gson();
				if (debug) Log.i(TAG, "Fail2"+gson.toJson(response.getCra()));
				mLnttCra = (LnttCra) response.getCra();
				lnttAgent.setLnttCra(mLnttCra.copyMe());
				if (debug) Log.i(TAG, "Fail3" + mLnttCra.getReply().getCode());
				if (mLnttCra.getReply().isBusy() || mLnttCra.getReply().getCode().equals(RC.USRA_LT_PREP)){

					// RC_USRA_LT_PREP
					if (lnttRtryCnt < 6 && lnttAgent != null) {
						lnttRtryCnt++;

						// Update LtrsRec
						ClnEnv.setPref(me.getApplicationContext(), me.getString(R.string.CONST_PREF_LNTT_AGENT), Utils.serialize(lnttAgent));
						// sleep 30 seconds and retry doResetModem();
						me.doResetModemSchedule(mLnttCra);
					} else {

						// Out of 6 times retry
						me.errorMsg = Utils.getString(me, R.string.LTTM_TIMEOUT);
						lnttAgent.setResultMsg(me.errorMsg);
						me.stopLnttService(me.errorMsg);
					}
				} else {
					if (mLnttCra.getReply().getCode().equals(RC.GWTGUD_MM) || mLnttCra.getReply().getCode().equals(RC.ALT)) {
						// GWTGUD_MM
						// ALT
						me.errorMsg = ClnEnv.getRPCErrMsg(me, RC.GWTGUD_MM);
					} else if (mLnttCra.getReply().getCode().equals(RC.USRA_LT_ERR) || mLnttCra.getReply().getCode().equals(RC.USRA_LT_UXSTS)) {
						// USRA_LT_ERR
						// USRA_LT_UXSTS
						me.errorMsg = Utils.getString(me, R.string.LTTM_ERR);
					} else {
						// RC_IVDATA
						// RC_NOT_AUTH
						// RC_UXSVLTERR
						me.errorMsg = Utils.getString(me, R.string.MYHKT_LT_ERR_EXPIRED);
					}

					lnttAgent.setResultMsg(me.errorMsg);
					me.stopLnttService(me.errorMsg);		
				}
			} else {
				if (!"".equals(response.getMessage()) && response.getMessage() != null) {
					lnttAgent.setResultMsg(response.getMessage());
					me.stopLnttService(response.getMessage());
					//					displayDialog(this, response.getMessage());
				} else if (RC.TCSESS_MM.equalsIgnoreCase(response.getReply().getCode())) {
					BaseActivity.ivSessDialog();
				} else {
					lnttAgent.setResultMsg(me.errorMsg);
					me.stopLnttService(me.errorMsg);
					//					displayDialog(this, ClnEnv.getRPCErrMsg(this, response.getReply().getCode()));
				}
			}
		}
	}



	@Override
	public void onLowMemory() {
	}

	@Override
	public void onDestroy() {
		// clear all Timer schedule to avoid restart line test
		timer.cancel();
		timer.purge();
		timer = new Timer();

	}
	//
	private final void stopLnttService(String message) {
		// Only show LineTest Complete Message
		generateNotification(me.getApplicationContext(), Utils.getString(me, R.string.MYHKT_LT_MSG_COMPLETED));

		//Use line test for CGM general
		//		Intent intent = new Intent();
		//		Bundle bundle = new Bundle();		
		//		bundle.putString("message", "Your PCCW-HKT mobile service general");
		//		bundle.putString("loginId", "Your PCCW-HKT mobile service general");
		//		bundle.putString("acctNum", "Your PCCW-HKT mobile service general");
		//		bundle.putString("type", "Your PCCW-HKT mobile service general");
		//		intent.putExtras(bundle);
		//		GCMIntentService.generateNotificationGeneral(me.getApplicationContext(), intent);

		//Use line test for CGM Bill
		//		Intent intent = new Intent();
		//		Bundle bundle = new Bundle();		
		//		bundle.putString("message", "Your PCCW-HKT mobile service general");
		//		bundle.putString("loginId", "asdfg12345@pccw.com");
		//		bundle.putString("acctNum", lnttAgent.getLnttCra().getISubnRec().acctNum);
		////		bundle.putString("acctNum", "Your PCCW-HKT mobile service general");
		//		bundle.putString("type", "Your PCCW-HKT mobile service general");
		//		intent.putExtras(bundle);
		//		generateNotificationBill(me.getApplicationContext(), intent);
		me.stopSelf();
	}

	//	// Copied Code from MyAccount Notification Alert
	private void generateNotification(Context context, String message) {
		if (debug) Log.i(TAG, "generateNotification");
		long when = System.currentTimeMillis();
		SimpleDateFormat dateFormat = new SimpleDateFormat(Utils.getString(me, R.string.input_datetime_format));
		// Save all data and EndTimestamp before stopService
		lnttAgent.setEndTimestamp(dateFormat.format(new Date(when)));

		ClnEnv.setPref(context, context.getString(R.string.CONST_PREF_LNTT_AGENT), Utils.serialize(lnttAgent));
		String title = Utils.getString(me.getApplicationContext(), R.string.app_name);
		if (lnttAgent.getResultMsg() ==null){		

			// SubscriptionRec Validation
			Boolean isValidSubnRec = false;
			if (ClnEnv.isLoggedIn()) {
				for (SubnRec rSubnRec : ClnEnv.getAssocSubnRecAry(ClnEnv.getQualSvee().getSubnRecAry())) {
					if (rSubnRec.srvNum.equalsIgnoreCase(lnttAgent.getLnttSrvNum()) && rSubnRec.acctNum.equalsIgnoreCase(lnttAgent.getLnttCra().getISubnRec().acctNum)) {
						isValidSubnRec = true;
					}
				}
			}

			if (!isValidSubnRec) {
				Utils.clearLnttService(me);
				return;
			}
			// Validation End
		}
		Intent notificationIntent = null;
		ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
		List<RunningTaskInfo> tasks = am.getRunningTasks(1);
		if (!tasks.isEmpty()) {
			ComponentName topActivity = tasks.get(0).topActivity;
			if (!topActivity.getPackageName().equals(context.getPackageName())) {
				// MyHKT is not running in foreground
				notificationIntent = new Intent(context, SplashActivity.class);
				// This Boolean is used for turning on redirection from Splash > MainMenu > ServiceList > TargetService
				notificationIntent.putExtra("LTRS_READNOW", true);
			} else {				
				Intent intent = new Intent();  
				intent.setAction("LineTest");		        
				sendBroadcast(intent);
				// MyHKT is running in foreground and directly open the ServiceActivity
				me.redirectDialog(context, R.string.CONST_DIALOG_LNTT_RESULT);
			}
		}

		// Only notification when the App is inactive
		if (notificationIntent != null) {

			PendingIntent intent = PendingIntent.getActivity(context, 0, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);

			NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context);
			mBuilder.setSmallIcon(R.drawable.icon_notification);
			// For Android 3.0 and 3.1 may be crashed by setLargeIcon
			if (Build.VERSION.SDK_INT >= 14) mBuilder.setLargeIcon(((BitmapDrawable) context.getResources().getDrawable(R.drawable.hktappicon)).getBitmap());
			mBuilder.setWhen(when);
			mBuilder.setContentTitle(title);
			mBuilder.setTicker(message);
			mBuilder.setContentText(message);
			mBuilder.setAutoCancel(true);
			mBuilder.setDefaults(Notification.DEFAULT_ALL);
			mBuilder.setContentIntent(intent);
			Notification notification = mBuilder.build();
			notification = new NotificationCompat.BigTextStyle(mBuilder).bigText(message).build();

			NotificationManager mNotificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

			mNotificationManager.notify(context.getResources().getInteger(R.integer.CONST_NOTIFICATION_ID_LNTT), notification);
		}
	}

	// redirect to LineTest Target Activity
	protected final void redirectDialog(Context context, int dialogType) {
		Intent dialogIntent = new Intent(context, GlobalDialog.class);
		dialogIntent.putExtra("DIALOGTYPE", dialogType);
		dialogIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
		context.startActivity(dialogIntent);
	}

	private static void generateNotificationBill(Context context, Intent msgIntent) {
		if (me.debug) Log.d("GEN BILL", "STARTED");
		//Notification Intent identifier
		String pass = "BILL";
		Bundle bundle = msgIntent.getExtras();
		PushData pushData = new PushData();
		if (bundle.getString("message") != null) pushData.setMessage(bundle.getString("message"));
		if (bundle.getString("loginId") != null) pushData.setLoginId(bundle.getString("loginId"));
		if (bundle.getString("acctNum") != null) pushData.setAcctNum(bundle.getString("acctNum"));
		if (bundle.getString("type") != null) pushData.setType(bundle.getString("type"));

		if (me.debug) Log.d("GEN BILL", "Push data received, pushData message = " + pushData.getMessage());
		long when = System.currentTimeMillis();
		// Save all data before stopService
		String title = Utils.getString(me.getApplicationContext(), R.string.app_name);

		Intent notificationIntent = null;
		ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
		List<RunningTaskInfo> tasks = am.getRunningTasks(1);
		if (!tasks.isEmpty()) {
			ComponentName topActivity = tasks.get(0).topActivity;
			if (!topActivity.getPackageName().equals(context.getPackageName())) {
				// MyHKT is not running in foreground
				notificationIntent = new Intent(context, SplashActivity.class);
				// specify notification intent by setData()
				//notificationIntent.setData(new Uri.Builder().scheme("data").appendQueryParameter("text", pass).build());
				notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
				notificationIntent.putExtra("BILLMSG_READNOW", true);
				notificationIntent.putExtra("SERIAL_BILLMSG", pushData);
				if (me.debug) Log.d("GEN BILL", "notificationIntent prepared");
				if (me.debug) Log.d("GEN BILL", "notificationIntent prepared, pushData message = " + pushData.getMessage());
			} else {
				// MyHKT is running in foreground and directly open the ServiceActivity
				ClnEnv.setPushDataBill(pushData);
				try {
					if (ClnEnv.getPushDataBill() != null) {
						String loginID = ClnEnv.getPushDataBill().getLoginId();
						if ((!"".equalsIgnoreCase(loginID) && !loginID.equalsIgnoreCase(ClnEnv.getSessionLoginID())) || "".equalsIgnoreCase(ClnEnv.getSessionLoginID())) {
							Utils.clearBillMsgService(me);
						} else if (ClnEnv.isLoggedIn()) {
							// Check subscriptionRec is valid in current ServiceList
							Boolean isValidSubnRec = false;
							for (SubnRec rSubnRec : ClnEnv.getAssocSubnRecAry(ClnEnv.getQualSvee().getSubnRecAry())) {
								if (rSubnRec.acctNum.equalsIgnoreCase(ClnEnv.getPushDataBill().getAcctNum())) {
									isValidSubnRec = true;
								}
							}
							if (isValidSubnRec) {
								me.redirectDialog(context, R.string.CONST_DIALOG_MSG_BILL);
							} else {
								Utils.clearBillMsgService(me);
							}
						}
					}
				} catch (Exception e) {
					//Prevent throw null pointer exception for BillPushData
					ClnEnv.setPref(me.getApplicationContext(), me.getString(R.string.CONST_PREF_BILLMSG_NOTICEFLAG), false);
					return;
				}
			}
		}

		// Only notification when the App is inactive
		if (notificationIntent != null) {
			PendingIntent intent = PendingIntent.getActivity(context, Integer.parseInt(Utils.randomString(5)), notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);

			NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context);
			mBuilder.setSmallIcon(R.drawable.icon_notification);
			// For Android 3.0 and 3.1 may be crashed by setLargeIcon
			if (Build.VERSION.SDK_INT >= 14) mBuilder.setLargeIcon(((BitmapDrawable) context.getResources().getDrawable(R.drawable.hktappicon)).getBitmap());
			mBuilder.setWhen(when);
			mBuilder.setContentTitle(title);
			mBuilder.setTicker(pushData.getMessage());
			mBuilder.setContentText(pushData.getMessage());
			mBuilder.setAutoCancel(true);
			mBuilder.setDefaults(Notification.DEFAULT_ALL);
			mBuilder.setContentIntent(intent);
			Notification notification = mBuilder.build();
			notification = new NotificationCompat.BigTextStyle(mBuilder).bigText(pushData.getMessage()).build();

			NotificationManager mNotificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

			//mNotificationManager.notify(context.getResources().getInteger(R.integer.CONST_NOTIFICATION_ID_BILL), notification);
			mNotificationManager.notify(Integer.parseInt(Utils.randomString(5)), notification);
		}
	}

}