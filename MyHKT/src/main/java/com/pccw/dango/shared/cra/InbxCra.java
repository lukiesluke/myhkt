/*
    Crate for Inbox
    
    Keywords
    --------
    $URL: $
    $Rev: $
    $Date: $
    $Author: $
*/

package com.pccw.dango.shared.cra;

import java.io.Serializable;

import com.pccw.dango.shared.entity.IbxMsgRec;
import com.pccw.wheat.shared.tool.Tool;

public class InbxCra extends BaseCraEx implements Serializable
{
    private static final long serialVersionUID = 3355049023584790858L;
    
    private String                  iLoginId;           /* Login ID                                      */
    private IbxMsgRec               iIbxMsgRec;         /* Inbox Message Record                          */
    private boolean                 iUnrdCnt;           /* Get Unread Count?                             */
    
    private int                     oUnrdCnt;           /* Unread Count                                  */
    private IbxMsgRec[]             oIbxMsgRecAry;      /* Array of Inbox Message                        */


    public static void main(String rArg[])
    {
        System.out.println(getVer());
        return;
    }
    
    
    public static String getVer()
    {
        return ("$URL:  $, $Rev: $");
    }

    
    public InbxCra()
    {
        initAndClear();
    }


    protected void init()
    {
        super.init();
    }


    public void clear()
    {
        super.clear();
        
        clearILoginId();
        clearIIbxMsgRec();
        clearIUnrdCnt();
        clearOUnrdCnt();
        clearOIbxMsgRecAry();
    }


    public InbxCra copyFrom(InbxCra rSrc)
    {
        super.copyFrom(rSrc);
        
        setILoginId(rSrc.getILoginId());
        setIIbxMsgRec(rSrc.getIIbxMsgRec());
        setIUnrdCnt(rSrc.isIUnrdCnt());
        setOUnrdCnt(rSrc.getOUnrdCnt());
        setOIbxMsgRecAry(rSrc.getOIbxMsgRecAry());

        return (this);
    }


    public InbxCra copyTo(InbxCra rDes)
    {
        rDes.copyFrom(this);
        return (rDes);
    }
    
    
    public InbxCra copyMe()
    {
        InbxCra                  rDes;

        rDes = new InbxCra();
        rDes.copyFrom(this);
        return (rDes);
    }


    public void clearILoginId()
    {
        iLoginId = "";
    }


    public void setILoginId(String rArg)
    {
        iLoginId = rArg;
    }


    public String getILoginId()
    {
        return (iLoginId);
    }


    public void clearIIbxMsgRec()
    {
        iIbxMsgRec = new IbxMsgRec();
    }


    public void setIIbxMsgRec(IbxMsgRec rArg)
    {
        iIbxMsgRec = rArg;
    }


    public IbxMsgRec getIIbxMsgRec()
    {
        return (iIbxMsgRec);
    }

    
    public void clearIUnrdCnt()
    {
        iUnrdCnt = false;
    }


    public void setIUnrdCnt(boolean rArg)
    {
        iUnrdCnt = rArg;
    }


    public boolean isIUnrdCnt()
    {
        return (iUnrdCnt);
    }

    
    public void clearOUnrdCnt()
    {
        oUnrdCnt = 0;
    }


    public void setOUnrdCnt(int rArg)
    {
        oUnrdCnt = rArg;
    }


    public int getOUnrdCnt()
    {
        return (oUnrdCnt);
    }


    public void clearOIbxMsgRecAry()
    {
        oIbxMsgRecAry = new IbxMsgRec[0];
    }


    public void setOIbxMsgRecAry(IbxMsgRec[] rArg)
    {
        oIbxMsgRecAry = rArg;
    }


    public IbxMsgRec[] getOIbxMsgRecAry()
    {
        return (oIbxMsgRecAry);
    }
}
