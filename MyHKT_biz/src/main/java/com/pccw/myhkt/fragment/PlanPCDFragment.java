package com.pccw.myhkt.fragment;

import java.lang.Thread.UncaughtExceptionHandler;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import com.google.gson.Gson;
import com.pccw.dango.shared.cra.PlanImsCra;
import com.pccw.dango.shared.entity.SrvPlan;
import com.pccw.dango.shared.tool.RC;
import com.pccw.myhkt.APIsManager;
import com.pccw.myhkt.APIsResponse;
import com.pccw.myhkt.ClnEnv;
import com.pccw.myhkt.DialogHelper;
import com.pccw.biz.myhkt.R;
import com.pccw.myhkt.Tool;
import com.pccw.myhkt.Utils;
import com.pccw.myhkt.activity.BaseActivity;
import com.pccw.myhkt.cell.model.ArrowTextCell;
import com.pccw.myhkt.cell.model.BigTextCell;
import com.pccw.myhkt.cell.model.Cell;
import com.pccw.myhkt.cell.model.SmallTextCell;
import com.pccw.myhkt.cell.model.WebViewCell;
import com.pccw.myhkt.cell.view.CellViewAdapter;
import com.pccw.myhkt.lib.ui.AAQuery;
import com.pccw.myhkt.lib.ui.ScalableImageView;

import android.app.Activity;
import android.graphics.Point;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

/************************************************************************
 * File : UsageFragment.java
 * Desc : Usage Page handle mob and 1010 case
 * Name : UsageFragment
 * by 	: Andy Wong
 * Date : 25/1/2015
 * 
 * Change History:
 * Date       Modified By		Description
 * ---------- ----------------	-------------------------------
 * 25/1/2015  Andy Wong 		-First draft
 * 15/6/2017  Abdulmoiz Esmail  -59316: PCD subscription under csl, setModuleId for PCD Plan & PCD Bill.
 *************************************************************************/

public class PlanPCDFragment extends BaseServiceFragment {
	private PlanPCDFragment me;
	private AAQuery aq;

	private static final String TAG = "PlanPCDFragment";
	private List<Cell> cellList;
	private CellViewAdapter	cellViewAdapter;
	private LinearLayout frame;

	private int padding_twocol;
	private int colWidth;
	private int sColWidth;

	private String[]	servicePlanCols;
	private String[] 	monthlyFeeCol;
	private String[] 	endDateCol;
	private String[] 	endDateNoteCol;
	
	private SmallTextCell webViewCellRmk;
	private SmallTextCell webViewCellVas;
	private SmallTextCell webViewCellInc;
	private SmallTextCell webViewCellCmt;
	private PlanImsCra	planIMSCra;

	//	private int[] widthArray;
	private int[] sWidthArray;
	private int[] singleWidthArray;

	private Cell cellLine;

	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		me = this;
		View fragmentLayout = inflater.inflate(R.layout.fragment_plan_pcd, container, false);
		aq = new AAQuery(fragmentLayout);
		initData();
		return fragmentLayout;
	}

	@Override
	public void onStart() {
		super.onStart();
		//Screen Tracker
	}

	protected void initData(){
		super.initData();
		padding_twocol = (int) getResources().getDimension(R.dimen.padding_twocol);
		colWidth = (deviceWidth - extralinespace *2 ) /4;
		sColWidth = (deviceWidth - extralinespace *2 ) * 2/9;
		cellViewAdapter = new CellViewAdapter(me.getActivity());
		frame = (LinearLayout) aq.id(R.id.fragment_plan_pcd_tp_frame).getView();
	}

	protected void initUI(){
		aq.id(R.id.fragment_plan_pcd_tp_sv).backgroundColorId(R.color.white);
		aq.marginpx(R.id.fragment_plan_pcd_tp_sv, 0, 0, 0, 0);

		cellList = new ArrayList<Cell>();
		sWidthArray = new int[3];
		sWidthArray[0] =  ViewGroup.LayoutParams.WRAP_CONTENT;
		sWidthArray[1] = sColWidth - extralinespace;
		sWidthArray[2] = sColWidth * 3/2;
		singleWidthArray = new int[1];
		singleWidthArray[0] = ViewGroup.LayoutParams.WRAP_CONTENT;

		cellLine = new Cell(Cell.LINE);
		cellLine.setTopPadding(basePadding);
		cellLine.setLeftPadding(basePadding);
		cellLine.setRightPadding(basePadding);
		cellLine.setBottompadding(basePadding);

		//Service Plan Details
		monthlyFeeCol = new String[1];
		monthlyFeeCol[0] = getResString(R.string.PLNIMF_VAS_AMT);
		endDateCol = new String[1];
		endDateCol[0] = getResString(R.string.PLNIMF_INC_ENDT);
		endDateNoteCol = new String[1];
		endDateNoteCol[0] = getResString(R.string.PLNIMF_INC_ENDT_NOTE);

		BigTextCell bigTextCell = new BigTextCell(getResString(R.string.PLNIMF_SRV_PL_DTL), ""); 
		bigTextCell.setTopMargin(basePadding);
		bigTextCell.setBotMargin(basePadding);
		bigTextCell.setRightMargin(basePadding);
		bigTextCell.setLeftMargin(basePadding);
		cellList.add(bigTextCell);
		
		//Header
		//		ArrowTextCell col3CellHeader1 = new ArrowTextCell("", monthlyFeeCol, null, singleWidthArray,R.color.white);
		//		cellList.add(col3CellHeader1);
		ArrowTextCell col3CellHeader2 = new ArrowTextCell(getResString(R.string.PLNIMF_MTHLY_FEE), new String[] {""} , null, singleWidthArray, R.color.cell_bg_grey);
		col3CellHeader2.setTopPadding(padding_twocol);
		col3CellHeader2.setTitleColorId(R.color.hkt_txtcolor_grey);
		col3CellHeader2.setContentColorId(R.color.hkt_textcolor);
		cellList.add(col3CellHeader2);

		//Service Plan Detail Remark
		webViewCellRmk = new SmallTextCell(getString(R.string.PCD_HIDE_PLAN), "");
		webViewCellRmk.setLeftPadding(basePadding);
		webViewCellRmk.setTitleSizeDelta(-2);
		webViewCellRmk.setTitleColorId(R.color.hkt_txtcolor_grey);
		cellList.add(webViewCellRmk);
		
		cellList.add(cellLine);
		
		//VAS
		ArrowTextCell col3CellVas = new ArrowTextCell(getResString(R.string.PLNIMF_VAS), null, null, singleWidthArray,R.color.white);
		col3CellVas.setLeftPadding(basePadding);
		col3CellVas.setArrowShown(true);
		col3CellVas.setTitleSizeDelta(0);
		cellList.add(col3CellVas);

		
		webViewCellVas = new SmallTextCell(getResString(R.string.PLNIMM_NOVAS), "");
		webViewCellVas.setTopMargin(basePadding);
		webViewCellVas.setLeftPadding(basePadding);
		webViewCellVas.setRightPadding(basePadding);
		webViewCellVas.setBgColorId(R.color.cell_bg_grey);
		webViewCellVas.setTitleSizeDelta(-2);
		cellList.add(webViewCellVas);
		cellList.add(cellLine);

		//Incentive
		ArrowTextCell col3CellInc = new ArrowTextCell(getResString(R.string.PLNIMF_INC), null, null, singleWidthArray,R.color.white);
		col3CellInc.setLeftPadding(basePadding);
		col3CellInc.setArrowShown(true);
		col3CellInc.setTitleSizeDelta(0);
		cellList.add(col3CellInc);
		webViewCellInc = new SmallTextCell(getResString(R.string.PLNIMM_NOINC), "");
		webViewCellInc.setTopMargin(basePadding);
		webViewCellInc.setLeftPadding(basePadding);
		webViewCellInc.setRightPadding(basePadding);
		webViewCellInc.setTitleSizeDelta(-2);
		webViewCellInc.setBgColorId(R.color.cell_bg_grey);
		cellList.add(webViewCellInc);

		cellList.add(cellLine);

		//Commitment
		ArrowTextCell smallCellCmmt = new ArrowTextCell(getResString(R.string.PLNIMF_CMMT), null, null, sWidthArray,R.color.white);
		smallCellCmmt.setLeftPadding(basePadding);
		smallCellCmmt.setArrowShown(true);
		smallCellCmmt.setTitleSizeDelta(0);
		cellList.add(smallCellCmmt);

		webViewCellCmt = new SmallTextCell(getResString(R.string.PLNIMM_NOCMT), "");
		webViewCellCmt.setTopMargin(basePadding);
		webViewCellCmt.setLeftPadding(basePadding);
		webViewCellCmt.setRightPadding(basePadding);
		webViewCellCmt.setTitleSizeDelta(-2);
		webViewCellCmt.setBgColorId(R.color.cell_bg_grey);
		cellList.add(webViewCellCmt);

		cellViewAdapter.setView(frame, cellList);		
	}

	
	//Refresh the data only when it is on current page
	public void refreshData(){	
		super.refreshData();
		if(callback_main.getCurrentServicePage() == 1) {
			setModuleId();
			PlanImsCra mPlanIMSCra = new PlanImsCra(); 
			mPlanIMSCra.setILoginId(ClnEnv.getLoginId());
			mPlanIMSCra.setISubnRec(callback_main.getSubnRec());
			APIsManager.doGetPlanIMS(this, mPlanIMSCra);		
		}	
	}

	protected void setModuleId(){
		//IF LOB is PCD and isBillByAgent is true use MODULE_PCD_BILL_4MOBCS otherwise MODULE_PCD_PLAN
		if(callback_main.getSubnRec().lob.equalsIgnoreCase("PCD") && callback_main.getSubnRec().isBillByAgent()){
			callback_livechat.setModuleId(getResString(R.string.MODULE_PCD_PLAN_4MOBCS));
		} else {
			callback_livechat.setModuleId(getResString(R.string.MODULE_PCD_PLAN));
		}
		  
		super.setModuleId();
	}
	public void refresh(){	
		super.refresh();
		setModuleId();
		PlanImsCra mPlanIMSCra = new PlanImsCra(); 
		mPlanIMSCra.setILoginId(ClnEnv.getLoginId());
		mPlanIMSCra.setISubnRec(callback_main.getSubnRec());
		APIsManager.doGetPlanIMS(this, mPlanIMSCra);		
	}

	public void updateUI(){
		cellList = new ArrayList<Cell>();

		//Check PON coverage, show PON coverage image if available
		if (planIMSCra.getOPonCvg()) {
			aq.id(R.id.fragment_planpcd_banner).visibility(View.VISIBLE);
			//			aq.id(R.id.banner_fib_btn).clicked(this, "onClick");
		}

		//Service Plan Details
		BigTextCell bigTextCell = new BigTextCell(getResString(R.string.PLNIMF_SRV_PL_DTL), ""); 
		bigTextCell.setTopMargin(basePadding);
		bigTextCell.setBotMargin(basePadding);
		bigTextCell.setRightMargin(basePadding);
		bigTextCell.setLeftMargin(basePadding);
		cellList.add(bigTextCell);
		//		ArrowTextCell col3Cell1 = new ArrowTextCell("", monthlyFeeCol, null, singleWidthArray,R.color.white);
		//		col3Cell1.setContentSizeDelta(-4);
		//		cellList.add(col3Cell1);
		String[] fee = {Utils.convertStringToPrice1dp(planIMSCra.getOMthFee().getFee())};

		ArrowTextCell col3Cell2 = new ArrowTextCell(getResString(R.string.PLNIMF_MTHLY_FEE), fee, null, singleWidthArray,R.color.cell_bg_grey);
		col3Cell2.setTitleSizeDelta(-2);
		col3Cell2.setContentSizeDelta(-4);
		col3Cell2.setTitleColorId(R.color.hkt_txtcolor_grey);
		col3Cell2.setContentColorId(R.color.hkt_textcolor);
		cellList.add(col3Cell2);		
		
		//Check cmt end date
		//Remark shown when :
		//1. there is no service plan commitment end date
		//2. end date < calendar date
		//3. end date > calendar date and end date < calendar date + 6 mths
		SrvPlan[] srvPlansCmt = planIMSCra.getOCommAry();
		Boolean isRemarkShown = true;
		if (srvPlansCmt.length > 0) {
			for (int i=0; i <srvPlansCmt.length; i++) {
				if (!srvPlansCmt[i].getDesnEn().startsWith("Vas")) {
					String cmtEndDate = Tool.formatDate(srvPlansCmt[i].getEnDt() ,"dd/MM/yy");	
					Calendar c = Calendar.getInstance();
					Date currentDate = c.getTime();
					c.add(Calendar.MONTH, 6);
					Date after6mthsDate = c.getTime();
					try {
						Date endDate;
						DateFormat df = new SimpleDateFormat("dd/MM/yy"); 	
						endDate = df.parse(cmtEndDate);
						//End Date >= calendar date AND < calendar date + 6 mths,
						if (endDate.before(after6mthsDate) && !endDate.before(currentDate) ) {
							isRemarkShown = true;
						} else //end date < calendar date
                            isRemarkShown = endDate.before(currentDate);
					} catch (ParseException e) {						
						e.printStackTrace();
						isRemarkShown = true;
					}	
					break;
				}
			}
		} else { //if no commitment
			isRemarkShown = true;	
		}
		
		if (!isRemarkShown) {
			if (cellList.contains(webViewCellRmk)) {
				cellList.remove(webViewCellRmk);
			}
		} else {
			if (!cellList.contains(webViewCellRmk)) {
				cellList.add(webViewCellRmk);
			}
		}
		cellList.add(cellLine);
		
		//VAS		
		SrvPlan[] srvPlansVas = planIMSCra.getOVasAry();		
		if (srvPlansVas.length > 0) {
			ArrowTextCell col3CellVas = new ArrowTextCell(getResString(R.string.PLNIMF_VAS), monthlyFeeCol, null,  singleWidthArray,R.color.white);
			col3CellVas.setArrowShown(true);
			col3CellVas.setTitleSizeDelta(0);
			col3CellVas.setContentSizeDelta(-4);
			cellList.add(col3CellVas);			

			for (int i=0; i <srvPlansVas.length; i++) {
				String[] planFee = {Utils.convertStringToPrice1dp(srvPlansVas[i].getFee())};
				ArrowTextCell col3CellVasRow = new ArrowTextCell(isZh ?srvPlansVas[i].getDesnZh():srvPlansVas[i].getDesnEn(), planFee, null, singleWidthArray, i%2==0 ?R.color.cell_bg_grey : R.color.white);

				col3CellVasRow.setTitleColorId(R.color.hkt_txtcolor_grey);
				col3CellVasRow.setTitleSizeDelta(-2);
				col3CellVasRow.setContentSizeDelta(-4);
				col3CellVasRow.setContentColorId(R.color.hkt_textcolor);
				cellList.add(col3CellVasRow);
			}			
		} else {
			ArrowTextCell col3CellVas = new ArrowTextCell(getResString(R.string.PLNIMF_VAS), null, null, new int[0],R.color.white);
			col3CellVas.setArrowShown(true);
			col3CellVas.setTitleSizeDelta(0);
			col3CellVas.setContentSizeDelta(-4);
			cellList.add(col3CellVas);


			//			WebViewCell webViewCell = new WebViewCell(isZh ? "file:///android_asset/plan_novas_zh.html" : "file:///android_asset/plan_novas_en.html");
			//			webViewCell.setTopMargin(extralinespace);
			cellList.add(webViewCellVas);
		}

		cellList.add(cellLine);

		//Incentive		
		SrvPlan[] srvPlansInc = planIMSCra.getOIncAry();
		if (srvPlansInc.length > 0) {
			ArrowTextCell col3CellInc = new ArrowTextCell(getResString(R.string.PLNIMF_INC), endDateCol,endDateNoteCol, singleWidthArray,R.color.white);
			col3CellInc.setArrowShown(true);
			col3CellInc.setTitleSizeDelta(0);
			col3CellInc.setContentSizeDelta(-4);
			col3CellInc.setNoteSizeDelta(-6);
			cellList.add(col3CellInc);			

			for (int i=0; i <srvPlansInc.length; i++) {
				String[] date = {Tool.formatDate(srvPlansInc[i].getEnDt(), "dd/MM/yy")};
				ArrowTextCell col3CellRow = new ArrowTextCell(srvPlansInc[i].getDesnEn(), date, null, singleWidthArray, i%2==0 ?R.color.cell_bg_grey : R.color.white);
				col3CellRow.setTitleColorId(R.color.hkt_txtcolor_grey);		
				col3CellRow.setTitleSizeDelta(-2);
				col3CellRow.setContentSizeDelta(-4);		
				col3CellRow.setContentColorId(R.color.hkt_textcolor);
				cellList.add(col3CellRow);
			}			
		} else {
			ArrowTextCell col3CellInc = new ArrowTextCell(getResString(R.string.PLNIMF_INC), null, null, new int[0],R.color.white);
			col3CellInc.setArrowShown(true);
			col3CellInc.setTitleSizeDelta(0);
			col3CellInc.setContentSizeDelta(-4);
			cellList.add(col3CellInc);

			//			WebViewCell webViewCell = new WebViewCell(isZh ? "file:///android_asset/plan_noinc_zh.html" : "file:///android_asset/plan_noinc_en.html");
			//			webViewCell.setTopMargin(extralinespace);
			cellList.add(webViewCellInc);
		}
		cellList.add(cellLine);


		//Commitment Details		
		srvPlansCmt = planIMSCra.getOCommAry();
		String[] commtCol = {getResString(R.string.PLNIMF_CMMT_PRD), getResString(R.string.PLNIMF_CMMT_ENDT1), getResString(R.string.PLNIMF_CMMT_REMN)};
		String[] commtNoteCol = { getResString(R.string.PLNIMF_INC_ENDT_NOTE1),getResString(R.string.PLNIMF_INC_ENDT_NOTE), getResString(R.string.PLNIMF_INC_ENDT_NOTE1)};		
		if (srvPlansCmt.length > 0) {
			ArrowTextCell col3CellCmt = new ArrowTextCell(getResString(R.string.PLNIMF_CMMT), commtCol, commtNoteCol, sWidthArray,R.color.white);
			col3CellCmt.setArrowShown(true);
			col3CellCmt.setTitleSizeDelta(0);
			col3CellCmt.setContentSizeDelta(-4);
			col3CellCmt.setNoteSizeDelta(-6);
			cellList.add(col3CellCmt);			

			for (int i=0; i <srvPlansCmt.length; i++) {
				//Description
				SpannableString content;
				String desc;
				if (srvPlansCmt[i].getDesnEn().startsWith("Vas")) {
					content = new SpannableString(Utils.getString(me.getActivity(), R.string.PLNIMF_CMMT_VAS));
				} else {
					content = new SpannableString(Utils.getString(me.getActivity(), R.string.PLNIMF_CMMT_SP));
				}
				content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
				desc = content.toString();

				//Commitment Period
				String prd;
				if (!srvPlansCmt[i].getSrvCmmtPrd().equals("")) {
					prd = String.format("%s %s",srvPlansCmt[i].getSrvCmmtPrd(), getResString(R.string.DLGM_MONTH));
				} else {
					prd ="";
				}

				// Remaining month of Commitment
				String remm;
				if (!srvPlansCmt[i].getSrvCmmtRmn().equals("")) {
					remm = String.format("%s %s",srvPlansCmt[i].getSrvCmmtRmn(), getResString(R.string.DLGM_MONTH));
				} else {
					remm ="";
				}

				//End date
				String endDate;
				endDate = Tool.formatDate(srvPlansCmt[i].getEnDt() ,"dd/MM/yy");				
				if (isExpireWithin6mths(endDate)) {
					endDate = "-";
					remm = "-";
				}
				
				String[] data = {prd, endDate, remm};
				ArrowTextCell col3CellRow = new ArrowTextCell(desc, data, null, sWidthArray, i%2==0 ?R.color.cell_bg_grey : R.color.white);
				col3CellRow.setTitleColorId(R.color.hkt_txtcolor_grey);	
				col3CellRow.setTitleSizeDelta(-2);
				col3CellRow.setContentSizeDelta(-4);
				col3CellRow.setContentColorId(R.color.hkt_textcolor);
				cellList.add(col3CellRow);
			}			
		} else {
			ArrowTextCell col3CellCmt = new ArrowTextCell(getResString(R.string.PLNIMF_CMMT), null, null, new int[0],R.color.white);
			col3CellCmt.setArrowShown(true);
			col3CellCmt.setTitleSizeDelta(0);
			col3CellCmt.setContentSizeDelta(-4);
			cellList.add(col3CellCmt);			

			//			WebViewCell webViewCell = new WebViewCell(isZh ? "file:///android_asset/plan_nocmt_zh.html" : "file:///android_asset/plan_nocmt_en.html");
			//			webViewCell.setTopMargin(extralinespace);
			cellList.add(webViewCellCmt);
		}
		cellList.add(cellLine);

		cellViewAdapter.setView(frame, cellList);	
	}
	
	public Boolean isExpireWithin6mths(String date) {
		Calendar c = Calendar.getInstance();
		Date currentDate = c.getTime();
		c.add(Calendar.MONTH, 6);
		Date after6mthsDate = c.getTime();
		Log.i(TAG, currentDate.toString());
		Log.i(TAG, after6mthsDate.toString());
		
		try {
			Date endDate;
			DateFormat df = new SimpleDateFormat("dd/MM/yy"); 	
			endDate = df.parse(date);
			Log.i(TAG, endDate.toString());
			//End Date >= calendar date AND < calendar date + 6 mths,
            return endDate.before(after6mthsDate) && !endDate.before(currentDate);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}	
	}


	protected void cleanupUI() {

	}

	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.banner_fib_img:
			openLiveChat(Utils.getString(me.getActivity(), R.string.MODULE_PCD_PONCHK));	
			break;
		}
	}

	@Override
	public void initPonBanner() {
		RelativeLayout rl = (RelativeLayout) aq.id(R.id.banner_pon_layout).getView();
		ScalableImageView ivFib = rl.findViewById(R.id.banner_fib_img);
		ivFib.setOnClickListener(this);
	}

	protected void openLiveChat(String tempModuled) {
		if(callback_livechat != null) {
			callback_livechat.openLiveChat(tempModuled);
		}
	}

	@Override
	public void onSuccess(APIsResponse response) {
		if (APIsManager.PLAN_IMS.equals(response.getActionTy())) {
			planIMSCra = (PlanImsCra) response.getCra();
			updateUI();
		}
	}

	@Override
	public void onFail(APIsResponse response) {
		if (response != null) {
			if (APIsManager.PLAN_IMS.equals(response.getActionTy())) {
				// General Error Message
				if (!"".equals(response.getMessage()) && response.getMessage() != null) {
					DialogHelper.createSimpleDialog(me.getActivity(), response.getMessage());
				} else if (RC.TCSESS_MM.equalsIgnoreCase(response.getReply().getCode())) {
					BaseActivity.ivSessDialog();
				} else {
					DialogHelper.createSimpleDialog(me.getActivity(), ClnEnv.getRPCErrMsg(this.getActivity(), response.getReply().getCode()));
				}
				//				planIMSCra =  ClnEnv.getJSONObject(this.getActivity(), PlanImsCra.class, "json/plan_pcd.json");
				//				updateUI();
			}
		}
	}
}

