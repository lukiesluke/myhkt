package com.pccw.myhkt.lib.ui;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff.Mode;
import android.graphics.PorterDuffXfermode;
import android.graphics.RadialGradient;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

public class UpCircleShadowView extends View{

	private int viewHeight = 0;
	private int viewWidth = 0;
	private Paint shadowP = new Paint();
	private Paint clearPaint = new Paint();

	public UpCircleShadowView(Context context)  {  
		super(context);  
		setBackgroundColor(Color.TRANSPARENT);
		clearPaint.setColor(Color.TRANSPARENT); 
		setLayerType(View.LAYER_TYPE_SOFTWARE, null);
	} 

	public UpCircleShadowView(Context context, AttributeSet attrs) {
		super(context, attrs);
		setBackgroundColor(Color.TRANSPARENT);
		clearPaint.setColor(Color.TRANSPARENT); 
		setLayerType(View.LAYER_TYPE_SOFTWARE, null);
	}

	public UpCircleShadowView(Context context, AttributeSet attrs,	int defStyleAttr) {
		super(context, attrs, defStyleAttr);
		setBackgroundColor(Color.TRANSPARENT);
		clearPaint.setColor(Color.TRANSPARENT); 
		setLayerType(View.LAYER_TYPE_SOFTWARE, null);
		// TODO Auto-generated constructor stub
	}


	private void setupPaints() {
		int w = viewWidth/2;
		int colors [] = {0x00FFFFFF,0x00FFFFFF,0x88000000 };
		float[] stops = {0, ((float)(w-15))/(float)w, 1f};
		RadialGradient gradient = new RadialGradient(w, w, w,colors,stops, android.graphics.Shader.TileMode.CLAMP);
		shadowP = new Paint();
		shadowP.setDither(true);
		shadowP.setShader(gradient);
		shadowP.setAntiAlias(true);		

		clearPaint.setColor(Color.TRANSPARENT); 
		clearPaint.setXfermode(new PorterDuffXfermode(Mode.CLEAR));
		clearPaint.setAntiAlias(true);
	}
	
    @Override
    protected void onSizeChanged(int newWidth, int newHeight, int oldWidth, int oldHeight) {
        super.onSizeChanged(newWidth, newHeight, oldWidth, oldHeight);
        viewWidth = newWidth;
        viewHeight = newHeight;
//        setupBounds();
        setupPaints();
        invalidate();
    }

	@Override  
	protected void onDraw(Canvas canvas)  { 		
		super.onDraw(canvas);		
		canvas.drawCircle( viewWidth/2,  viewWidth/2, viewWidth/2, shadowP); 
		canvas.drawCircle( viewWidth/2,  viewWidth/2 + 15, viewWidth/2, clearPaint);  
		}  
} 	

