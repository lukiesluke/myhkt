package com.pccw.myhkt.util

import android.content.ContentValues
import android.content.Context
import android.net.Uri
import android.provider.MediaStore
import android.util.Log
import java.io.InputStream
import java.util.*

/**
 *  Created by tommyli on 3/10/2018
 */

class FileUtil {
    companion object {
        private var contentTypeToExtensionMapper = HashMap<String, String>().apply {
            put("text/plain", "txt")
            put("text/html", "htm")
            put("text/html", "html")
            put("text/html", "php")
            put("text/css", "css")
            put("application/javascript", "js")
            put("application/json", "json")
            put("application/xml", "xml")
            put("application/x-shockwave-flash", "swf")
            put("video/x-flv", "flv")
            put("image/png", "png")
            put("image/jpeg", "jpe")
            put("image/jpeg", "jpeg")
            put("image/jpeg", "jpg")
            put("image/gif", "gif")
            put("image/bmp", "bmp")
            put("image/vnd.microsoft.icon", "ico")
            put("image/tiff", "tiff")
            put("image/tiff", "tif")
            put("image/svg+xml", "svg")
            put("image/svg+xml", "svgz")
            put("application/zip", "zip")
            put("application/x-rar-compressed", "rar")
            put("application/x-msdownload", "exe")
            put("application/x-msdownload", "msi")
            put("application/vnd.ms-cab-compressed", "cab")
            put("audio/mpeg", "mp3")
            put("video/quicktime", "qt")
            put("video/quicktime", "mov")
            put("application/pdf", "pdf")
            put("image/vnd.adobe.photoshop", "psd")
            put("application/postscript", "ai")
            put("application/postscript", "eps")
            put("application/postscript", "ps")
            put("application/msword", "doc")
            put("application/rtf", "rtf")
            put("application/vnd.ms-excel", "xls")
            put("application/vnd.ms-powerpoint", "ppt")
            put("application/vnd.oasis.opendocument.text", "odt")
            put("application/vnd.oasis.opendocument.spreadsheet", "ods")
        }.toMap()

        fun randomFileName(mediaType: String) = "File_".plus(Calendar.getInstance().timeInMillis)
            .plus(".")
            .plus(contentTypeToExtensionMapper[mediaType])

        fun writeFromInputStream(context: Context, fileName: String, contentMineType: String, inputStream: InputStream): Uri {

            val contentResolver = context.contentResolver
            val contentValues = ContentValues().apply {
                put(MediaStore.MediaColumns.DISPLAY_NAME, fileName)
                put(MediaStore.MediaColumns.MIME_TYPE, contentMineType)
            }

            val uri = contentResolver.insert(MediaStore.Downloads.EXTERNAL_CONTENT_URI, contentValues)
            val outputStream = contentResolver.openOutputStream(uri!!)

            try {
                inputStream.copyTo(outputStream!!)
                outputStream.close()
            } catch (_: Exception) {
            }
            return uri
        }

        fun getFilePathFromURI(context: Context, uri: Uri): String? {
            var filePath: String? = null
            val cursor = context.contentResolver.query(uri, null, null, null, null)
            if (cursor != null) {
                if (cursor.moveToFirst()) {
                    val columnIndex = cursor.getColumnIndex(MediaStore.Images.Media.DATA)
                    if (columnIndex != -1) {
                        filePath = cursor.getString(columnIndex)
                        Log.d("lwg", "fileName path: $filePath")
                    }
                }
                cursor.close()
            }
            return filePath
        }
    }
}