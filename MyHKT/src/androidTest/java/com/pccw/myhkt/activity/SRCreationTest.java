package com.pccw.myhkt.activity;

import android.content.Context;
import android.content.SharedPreferences;
import androidx.test.espresso.intent.Intents;
import androidx.test.rule.ActivityTestRule;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import com.pccw.myhkt.AccountHelper;
import com.pccw.myhkt.R;
import com.pccw.myhkt.base.BaseTest;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static androidx.test.platform.app.InstrumentationRegistry.getInstrumentation;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.matcher.RootMatchers.isDialog;

@Ignore
@RunWith(AndroidJUnit4.class)
public class SRCreationTest extends BaseTest {
    private Context context;
    SharedPreferences shared;


    @Rule
    public ActivityTestRule<MainMenuActivity> activityTestRule = new ActivityTestRule<>(MainMenuActivity.class);

    @Before
    public void setUp(){
        context = getInstrumentation().getTargetContext();
        shared = context.getSharedPreferences(
                "myhkt",
                Context.MODE_PRIVATE);
        Intents.init();
        clickLeftNavBar();
        clickString(context, R.string.myhkt_LGIF_LOGIN);
        loginMyCredentialsUAT(AccountHelper.getUATAccount9().getUsername(),
                AccountHelper.getUATAccount9().getPassword(), shared);
        gotoMainMenuItem(1);
        clickViewInAdapter(R.id.servicelist_listView, 1);
        delay(10000);
        clickString(context, R.string.LTTF_TEST);
        clickDialogString("OK");
        delay(60000);
        clickDialogString("OK");
        clickString(context, R.string.MYHKT_LT_BTN_REPORT);
        delay(10000);
    }

    @After
    public void tearDown() {
        executeLogout(context);
        Intents.release();
    }

    @Test
    public void testSRCreationDisplayUI() {
        checkStringIsDisplayed(context, R.string.MYHKT_SR_HDR_CREATE);
        checkStringIsDisplayed(context, R.string.MYHKT_SR_LBL_PCD_ACC);
        checkStringIsDisplayed(context, R.string.MYHKT_SR_TITLE_CONTACT);
        checkStringIsDisplayed(context, R.string.MYHKT_SR_LBL_TITLE);
        checkStringIsDisplayed(context, R.string.MYHKT_SR_LBL_NAME);
        checkStringIsDisplayed(context, R.string.MYHKT_SR_LBL_TEL);
        checkStringIsDisplayed(context, R.string.MYHKT_SR_LBL_IS_ENGLISH);
        checkStringIsDisplayed(context, R.string.btn_confirm);
        checkStringIsDisplayed(context, R.string.btn_cancel);
        clickLeftNavBar();
    }

    @Test
    public void testSRCreationConfirmClickAllFields() {
        typeTextView(R.id.srcreation_etxt_contperson, R.id.srcreation_etxtlayout1, "UAT Sr 3");
        typeTextView(R.id.srcreation_etxt_contnum, R.id.srcreation_etxtlayout2, "87654321");
        clickString(context, R.string.btn_confirm);
        clickDialogString("OK");
        delay(10000);
        clickString(context, R.string.btn_ok);
        delay(10000);
        clickDialogString("OK");
        clickLeftNavBar();
        clickLeftNavBar();
        gotoMainMenuItem(2);
        delay(5000);
        clickViewInAdapter(R.id.myapptlist_listview, 0);
        delay(5000);
        clickView(R.id.myapptupd_btn_cancel);
        clickDialogString("YES");
        delay(5000);
        clickDialogString("OK");
    }

    @Test
    public void testSRCreationConfirmClickInvalidTelephoneNumber() {
        typeTextView(R.id.srcreation_etxt_contperson, R.id.srcreation_etxtlayout1, "UAT Sr 3");
        typeTextView(R.id.srcreation_etxt_contnum, R.id.srcreation_etxtlayout2, "12345678");
        clickString(context, R.string.btn_confirm);
        clickDialogString("OK");
        clickLeftNavBar();
    }

    @Test
    public void testSRCreationConfirmClickContactNameOnly() {
        typeTextView(R.id.srcreation_etxt_contperson, R.id.srcreation_etxtlayout1, "UAT Sr 3");
        clickString(context, R.string.btn_confirm);
        clickDialogString("OK");
        clickLeftNavBar();
    }

    @Test
    public void testSRCreationConfirmClickContactNumberOnly() {
        typeTextView(R.id.srcreation_etxt_contnum, R.id.srcreation_etxtlayout2,"56785678");
        clickString(context, R.string.btn_confirm);
        clickDialogString("OK");
        clickLeftNavBar();
    }

    @Test
    public void testSRCreationCancelClick() {
        clickString(context, R.string.btn_cancel);
        clickLeftNavBar();
    }

}