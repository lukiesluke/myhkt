package com.pccw.myhkt.service;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.simplexml.SimpleXmlConverterFactory;

public class APIClient {
    private static Retrofit retrofitMyHKT = null;
    private static Retrofit retrofit = null;
    private static Retrofit retrofitPDF = null;
    private static final String BASE_URL = "http://shop.pccwmobile.com/IDD0060WS/rs/idd0060/";
    private static final String BASE_URL_PDF = "https://customerservice.pccw.com/myhkt/";

    // When switch to PRD mode or UAT mode, API client will set to null.
    // To update the base URL of retrofitMyHKT
    public static void resetAPIClientMyHKT() {
        retrofitMyHKT = null;
    }

    public static Retrofit getRetrofitPDFInstance() {
        if (retrofitPDF == null) {
            retrofitPDF = new retrofit2.Retrofit.Builder()
                    .baseUrl(BASE_URL_PDF)
                    .client(new OkHttpClient())
                    .build();
        }
        return retrofitPDF;
    }

    public static Retrofit getAppConfigInstance(String url) {
        if (retrofitMyHKT == null) {
            retrofitMyHKT = new retrofit2.Retrofit.Builder()
                    .baseUrl(url)
                    .client(new OkHttpClient())
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofitMyHKT;
    }

    public static Retrofit getRetrofitInstance() {
        if (retrofit == null) {
            retrofit = new retrofit2.Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .client(new OkHttpClient())
                    .addConverterFactory(SimpleXmlConverterFactory.create())
                    .build();
        }
        return retrofit;
    }
}
