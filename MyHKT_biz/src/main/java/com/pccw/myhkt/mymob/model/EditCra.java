package com.pccw.myhkt.mymob.model;

import java.io.Serializable;

public class EditCra implements Serializable{
	private static final long serialVersionUID = 2002403121479209086L;
	private int pos;
	private String	alias;
	private String	mob_num;
	private String	password;
	
	public int getPos() {
		return pos;
	}
	public void setPos(int pos) {
		this.pos = pos;
	}
	public String getAlias() {
		return alias;
	}
	public void setAlias(String alias) {
		this.alias = alias;
	}
	public String getMob_num() {
		return mob_num;
	}
	public void setMob_num(String mob_num) {
		this.mob_num = mob_num;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
}
