package com.pccw.myhkt.cell.model;

import com.pccw.biz.myhkt.R;

public class ArrowTextCell extends Cell{

	// Bundle name
	
	private int[] widthArray;
	private String[] noteArray;
	private int noteSizeDelta; 
	
	//ture warpcontent 
	//false normtext height	
	private Boolean titleWrapContent;

	public ArrowTextCell(String title, String[] textArray ,String[] noteArray, int[] widthArray, boolean istaller, int cellheight) { //for VAS fragment handling
		this(title, textArray, noteArray, widthArray, R.color.white);
		
		cellHeight = cellheight;
		isTaller = istaller;
	}
	
	public ArrowTextCell(String title, String[] textArray ,String[] noteArray, int[] widthArray) {
		this(title, textArray, noteArray, widthArray, R.color.white);
	}
	
	public ArrowTextCell(String title, String[] textArray ,String[] noteArray, int[] widthArray, int bgColorId) {
		super();
		//Default setting
		type = Cell.ARROWTEXT;
		titleColorId = R.color.black;
		contentColorId = R.color.hkt_txtcolor_grey;
		
		titleSizeDelta = -2;
		contentSizeDelta = -2;
		noteSizeDelta = -2;	
		titleWrapContent = true; // -1 mean warp content
		isArrowShown = false;
		
//		cellHeight = (int) context.getResources().getDimension(R.dimen.textviewheight);

		this.title = title;
		this.textArray = textArray;
		this.noteArray = noteArray;
		this.widthArray = widthArray;
		this.bgColorId = bgColorId;
	}
	
	public ArrowTextCell(String title, String[] textArray ,String[] noteArray, int[] widthArray, int bgColorId, int contentColorId) {
		super();
		//Default setting
		type = Cell.ARROWTEXT;
		titleColorId = R.color.black;
		this.contentColorId = contentColorId;
		
		titleSizeDelta = -2;
		contentSizeDelta = -2;
		noteSizeDelta = -2;	
		titleWrapContent = true; // -1 mean warp content
		isArrowShown = false;

		this.title = title;
		this.textArray = textArray;
		this.noteArray = noteArray;
		this.widthArray = widthArray;
		this.bgColorId = bgColorId;
	}

	
	public int[] getWidthArray() {
		return widthArray;
	}

	public void setWidthArray(int[] widthArray) {
		this.widthArray = widthArray;
	}

	public String[] getNoteArray() {
		return noteArray;
	}

	public void setNoteArray(String[] noteArray) {
		this.noteArray = noteArray;
	}
	public int getNoteSizeDelta() {
		return noteSizeDelta;
	}
	public void setNoteSizeDelta(int noteSizeDelta) {
		this.noteSizeDelta = noteSizeDelta;
	}
	public Boolean getTitleWrapContent() {
		return titleWrapContent;
	}

	/**
	 * ture set the height of title text view to be warpcontent 
	 * <br>false set the height of title text view to be norm text height 	
	 */
	public void setTitleWrapContent(Boolean titleWrapContent) {
		this.titleWrapContent = titleWrapContent;
	}

}