/*
    Class for Directory Inquiry - page information
    
    Keywords
    --------
    $URL: $
    $Rev: $
    $Date: $
    $Author: $
*/



package com.pccw.dango.shared.entity;

import java.io.Serializable;

public class DQPageInfo implements Serializable 
{
    private static final long serialVersionUID = 1374985469265119652L;
    
    public String                   lr;
    public String                   rn;
    public String                   en;
    public String                   mc;
    public String                   msg;


    public static void main(String rArg[])
    {
        System.out.println(getVer());
        return;
    }
    
    
    public static String getVer()
    {
        return ("$URL: $, $Rev: $");
    }
    
    
    public DQPageInfo()
    {
        initAndClear();
    }
    
    
    final void initAndClear()
    {
        init();
        clear();
    }
    
    
    protected void init()
    {
    }
    
    
    public void clear()
    {
        lr  = "";
        rn  = "";
        en  = "";
        mc  = "";
        msg = "";
    }
    
    
    public DQPageInfo copyFrom(DQPageInfo rSrc)
    {
        lr  = rSrc.lr;
        rn  = rSrc.rn;
        en  = rSrc.en;
        mc  = rSrc.mc;
        msg = rSrc.msg;
    
        return (this);
    }
    
    
    public DQPageInfo copyTo(DQPageInfo rDes)
    {
        rDes.copyFrom(this);
        return (rDes);
    }
    
    
    public DQPageInfo copyMe()
    {
        DQPageInfo             rDes;
        
        rDes = new DQPageInfo();
        rDes.copyFrom(this);
        
        return (rDes);
    }
}
