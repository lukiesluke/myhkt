package com.pccw.myhkt.activity;

import static com.pccw.myhkt.util.Constant.KEY_NAME_LOB_TYPE;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.graphics.Point;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.widget.TextViewCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;
import androidx.viewpager.widget.ViewPager.OnPageChangeListener;

import com.google.gson.Gson;
import com.pccw.dango.shared.cra.ApptCra;
import com.pccw.dango.shared.cra.LnttCra;
import com.pccw.dango.shared.entity.Account;
import com.pccw.dango.shared.entity.SubnRec;
import com.pccw.myhkt.APIsManager.OnAPIsListener;
import com.pccw.myhkt.APIsResponse;
import com.pccw.myhkt.ClnEnv;
import com.pccw.myhkt.DialogHelper;
import com.pccw.myhkt.FAWrapper;
import com.pccw.myhkt.GlobalDialog;
import com.pccw.myhkt.R;
import com.pccw.myhkt.Utils;
import com.pccw.myhkt.fragment.BaseServiceFragment.OnLiveChatListener;
import com.pccw.myhkt.fragment.BaseServiceFragment.OnServiceListener;
import com.pccw.myhkt.fragment.BillFragment;
import com.pccw.myhkt.fragment.LnttFragment;
import com.pccw.myhkt.fragment.LnttRebootModemFragment;
import com.pccw.myhkt.fragment.PlanLTSFragment;
import com.pccw.myhkt.fragment.PlanPCDFragment;
import com.pccw.myhkt.fragment.PlanTVFragment;
import com.pccw.myhkt.fragment.UsageFragment;
import com.pccw.myhkt.fragment.VasFragment;
import com.pccw.myhkt.lib.ui.AAQuery;
import com.pccw.myhkt.lib.ui.HKTIndicator;
import com.pccw.myhkt.model.AcctAgent;
import com.pccw.myhkt.model.LnttAgent;
import com.pccw.myhkt.model.TabItem;
import com.pccw.myhkt.mymob.model.MyMobAcctAgent;
import com.pccw.myhkt.util.Constant;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/************************************************************************
 * File : ServiceActivity.java
 * Desc : Service Main Page
 * Name : ServiceActivity
 * by 	: Andy Wong
 * Date : 16/11/2015
 * Change History:
 * Date       Modified By		Description
 * ---------- ----------------	-------------------------------
 * 16/11/2015 Andy Wong 		-First draft
 * 26/01/2016 Derek Tsui		-core functions update
 * 04/01/2018 Abdulmoiz Esmail  -Added onRequestPermissionsResult for file storage and broadcast it to BillSumFragment
 *************************************************************************/

public class ServiceActivity extends BaseActivity implements OnAPIsListener, OnServiceListener, OnLiveChatListener {
    private boolean debug = false;
    private final String TAG = "lwg";
    private AAQuery aq;
    private int deviceWidth = 0;
    private int colWidth = 0;
    private int tabPos = 0;
    private int activeSubView;
    private final String navBarTitle = "";
    private List<TabItem> tabItemList = new ArrayList<>();
    private HKTIndicator hktindicator;
    private ViewPager viewPager;

    //View
    private Activity me;
    private ServicePagerAdapter servicePagerAdapter;

    // Tab Fragments
    private BillFragment billFragment;
    private UsageFragment usageFragment;
    private UsageFragment roamingFragment;
    private PlanTVFragment planTVFragment;
    private PlanPCDFragment planPCDFragment;
    private PlanLTSFragment planLtsFragment;
    private LnttFragment lnttFragment;
    private VasFragment vasFragment;
    private LnttRebootModemFragment rebootModemFragment;

    protected String trackerId = "";
    protected SubnRec assocSubnRec;
    protected Account account = null;
    protected AcctAgent acctAgent = null; //MyMobList AcctAgent
    protected MyMobAcctAgent myMobAcctAgent = null; //Original AcctAgent

    protected int lob;
    protected String lobString;
    protected int ltsType;

    // Line Test
    protected LnttCra lnttCra = null;
    protected ApptCra apptCra = null;
    protected LnttAgent lnttAgent = null;
    protected boolean showLnttResult = false; //not in use
    protected boolean isLnttServiceClearable = true;
    // Zombie account
    protected boolean isZombie = false;
    protected boolean isMyMobAcct = false;

    private boolean isToRebootModemAPI = false;
    private boolean isMyLineTest = false;

    @Override
    public final void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        debug = getResources().getBoolean(R.bool.DEBUG);
        if (debug) Log.i(TAG, "ServiceActivity:ONCREATE");

        isMyLineTest = getIntent().getBooleanExtra(Constant.KEY_IS_MY_LINE_TEST, false) || Utils.ISMYLIENTEST;

        //Init Fragment
        lnttFragment = new LnttFragment();
        planLtsFragment = new PlanLTSFragment();
        usageFragment = new UsageFragment();
        Bundle bundle_usage = new Bundle();
        bundle_usage.putBoolean("islocal", true);
        usageFragment.setArguments(bundle_usage);
        roamingFragment = new UsageFragment();
        Bundle bundle1 = new Bundle();
        bundle1.putBoolean("islocal", false);
        roamingFragment.setArguments(bundle1);

        ClnEnv.updateUILocale(getBaseContext(), ClnEnv.getAppLocale(getBaseContext()));
        Bundle bundle = this.getIntent().getExtras();
        if (bundle != null) {
            isMyMobAcct = bundle.getBoolean("ISMYMOBACCT", false);
        }

        // We need to ensure the user is still logged on
        if (!ClnEnv.isLoggedIn() && !isMyMobAcct) {
            finish();
            overridePendingTransition(R.anim.left_slide_in, R.anim.right_slide_out);
            return;
        }

        try {
            acctAgent = (AcctAgent) Objects.requireNonNull(bundle).getSerializable("ACCTAGENT");
            if (bundle.containsKey("SUBNREC")) { //if redirected, not select from the list (line test result or bill message)
                assocSubnRec = (SubnRec) bundle.getSerializable("SUBNREC");
            } else {
                assocSubnRec = acctAgent.getSubnRec();
            }

            if (debug) Log.i(TAG, "AcctAgent AccNum: : " + acctAgent.getAcctNum());
            if (debug) Log.i(TAG, "assocSubnRec AccNum: " + assocSubnRec.acctNum);

            isZombie = !acctAgent.isLive();
            lob = getLob(assocSubnRec);
            lobString = getLobString(assocSubnRec);

            //set flag for 1010 theme in MyMob
            ClnEnv.setIs101Flag(is1010(lobString));
            if (lob == R.string.CONST_LOB_LTS) {
                ltsType = Utils.getLtsSrvType(assocSubnRec.tos, assocSubnRec.eyeGrp, assocSubnRec.priMob);
            }

            //Check for the case that should not clear the lnttService
            if (debug) Log.i(TAG, "1LnttAngent" + new Gson().toJson(Utils.getPrefLnttAgent(this)));

            lnttAgent = Utils.getPrefLnttAgent(this);
            String lnttAgentStr = ClnEnv.getPref(this, this.getString(R.string.CONST_PREF_LNTT_AGENT), "");
            //Line test is running
            if (lnttAgent != null && lnttAgent.getEndTimestamp() != null && !lnttAgentStr.isEmpty() && "".equalsIgnoreCase(lnttAgent.getEndTimestamp())) {
                this.setIsLnttServiceClearable(false);
            }

            //Should not clear LnntAgent is not belong to this service
            if (lnttAgent != null && !lnttAgentStr.isEmpty() && !(lnttAgent.getLnttSrvNum().equalsIgnoreCase(assocSubnRec.srvNum) && assocSubnRec.acctNum.equalsIgnoreCase(lnttAgent.getLnttCra().getISubnRec().acctNum))) {
                this.setIsLnttServiceClearable(false);
            }

            //If LineTest returned Result, activeSubview == R.string.CONST_SELECTEDVIEW_TVLINETEST3;
            //Assume Expiry checking at ServiceListActivity
            if (lob == R.string.CONST_LOB_LTS || lob == R.string.CONST_LOB_PCD || lob == R.string.CONST_LOB_TV) {
                lnttAgent = Utils.getPrefLnttAgent(this);
                if (lnttAgent.getLnttSrvNum().equalsIgnoreCase(assocSubnRec.srvNum) && assocSubnRec.acctNum.equalsIgnoreCase(lnttAgent.getLnttCra().getISubnRec().acctNum)) {
                    if (!"".equalsIgnoreCase(lnttAgent.getResultMsg())) {
                        // If LineTest is imcompleted, Server returned Error/ Expired Result. Then redirected to Start LineTest and show Error Dialog
                        showLnttResult = false;
                        activeSubView = R.string.CONST_SELECTEDVIEW_LINETEST;
                        tabPos = 0;
                    } else if (lnttAgent != null && lnttAgent.getEndTimestamp() != null && !"".equalsIgnoreCase(lnttAgent.getEndTimestamp())) {
                        showLnttResult = true;
                        activeSubView = R.string.CONST_SELECTEDVIEW_LINETEST;
                        tabPos = 0;
                    }
                    // Reset the Notification flag to be false
                    ClnEnv.setPref(getApplicationContext(), getString(R.string.CONST_PREF_LNTT_NOTICEFLAG), false);
                }
            }

            //display usage first if its open from MyMobile view
            if (ClnEnv.isMyMobFlag()) {
                if (lob == R.string.CONST_LOB_1010 || lob == R.string.CONST_LOB_O2F) {
                    tabPos = 1;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            // App state unknown.
            // the safest way to handle this situation is to return to the caller.
            finish();
            overridePendingTransition(R.anim.left_slide_in, R.anim.right_slide_out);
        }

        initData();
        setContentView(R.layout.activity_service);
        setStatusBarColor();

        //Register broadcast receiver
        IntentFilter filter = new IntentFilter();
        filter.addAction(Constant.ACTION_LINE_TEST_SUCCESS);
        lineTestBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (intent.getAction() != null && intent.getAction().equals(Constant.ACTION_LINE_TEST_SUCCESS)) {
                    updateLineTestFragment();
                }
            }
        };
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
            registerReceiver(lineTestBroadcastReceiver, filter, Context.RECEIVER_NOT_EXPORTED);
        } else {
            registerReceiver(lineTestBroadcastReceiver, filter);
        }
    }

    protected void onRestart() {
        if (debug) Log.i(TAG, "onReStart" + callOnStart);
        Gson gson = new Gson();
        if (lnttAgent != null) {
            if (debug) Utils.showLog(TAG, gson.toJson(lnttAgent));
        }
        super.onRestart();
        if (debug) Log.i(TAG, "onReStart1" + callOnStart);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        ClnEnv.updateUILocale(getBaseContext(), ClnEnv.getAppLocale(getBaseContext()));
    }

    @Override
    protected final void onStart() {
        super.onStart();
        // If there is Bill Message, activeSubView == R.string.CONST_SELECTEDVIEW_LTSBILL;
        // dialog with bill message will be displayed
        if (ClnEnv.getPushDataBill() != null) {
            if (ClnEnv.getPushDataBill().getAcctNum().equalsIgnoreCase(assocSubnRec.acctNum)) {

                //prepare custom message for bill dialog
                String fullMsg = "";
                String serviceNum = "";
                String acctNum = getString(R.string.MYHKT_PN_MSG_BILL_ACCNUM) + " ";
                String overdueAmt = getString(R.string.MYHKT_PN_MSG_BILL_AMT) + " ";
                String remark = getString(R.string.MYHKT_PN_MSG_BILL);

                //acct number
                if (assocSubnRec.acctNum != null && !assocSubnRec.acctNum.isEmpty()) {
                    acctNum = acctNum + assocSubnRec.acctNum + "\n";
                    fullMsg = fullMsg + acctNum;
                }

                //service number
                int ltsType = 0;
                if (assocSubnRec.lob.equals(SubnRec.LOB_LTS)) {
                    ltsType = Utils.getLtsSrvType(assocSubnRec.tos, assocSubnRec.eyeGrp, assocSubnRec.priMob);
                    if (ltsType == R.string.CONST_LTS_CALLINGCARD) {
                        int last4index = (assocSubnRec.srvNum.length() - 4) < 0 ? 0 : assocSubnRec.srvNum.length() - 4;
                        serviceNum = String.format("CARD%s", assocSubnRec.srvNum.substring(last4index)) + "\n";
                    } else {
                        serviceNum = assocSubnRec.srvNum + "\n";
                    }
                } else {
                    serviceNum = assocSubnRec.srvNum + "\n";
                }

                switch (assocSubnRec.lob) {
                    case SubnRec.LOB_MOB:
                    case SubnRec.LOB_101:
                    case SubnRec.LOB_IOI:
                    case SubnRec.LOB_O2F:
                    case SubnRec.LOB_VOBB:
                        fullMsg = fullMsg + getString(R.string.LTTF_MOB_ACCTNUM) + " " + serviceNum;
                        break;
                    case SubnRec.LOB_LTS:
                        fullMsg = fullMsg + getString(R.string.LTTF_LTS_ACCTNUM) + " " + serviceNum;
                        break;
                    case SubnRec.LOB_PCD:
                        fullMsg = fullMsg + getString(R.string.LTTF_PCD_ACCTNUM) + " " + serviceNum;
                        break;
                }

                //overdue amt
                if (ClnEnv.getPushDataBill().getMessage().contains("$")) {
                    String str = ClnEnv.getPushDataBill().getMessage();
                    String[] parts = str.split("\\$");
                    String part2 = parts[1];
                    String amt = part2.substring(0, part2.indexOf("."));
                    String amtDec = part2.substring(part2.indexOf("."), part2.indexOf(".") + 3);
                    String amount = "$" + amt + amtDec;
                    fullMsg = fullMsg + overdueAmt + amount + "\n";
                }

                //remark
                fullMsg = fullMsg + "\n" + remark;
                DialogHelper.createSimpleDialog(this, fullMsg);
            }
            // Reset the Notification flag to be false
            ClnEnv.getPushDataBill().clear();
            ClnEnv.setPref(getApplicationContext(), getString(R.string.CONST_PREF_BILLMSG_NOTICEFLAG), false);
        }
    }

    @Override
    protected final void onResume() {
        if (debug) Log.i(TAG, "SERVICE ACTIVITY:onResume");
        super.onResume();
        //Screen Tracker
        switch (lob) {
            case R.string.CONST_LOB_MOB:
            case R.string.CONST_LOB_IOI:
            case R.string.CONST_LOB_VOBB:
                //tracker
                FAWrapper.getInstance().sendFAScreen(me, R.string.CONST_SCRN_SERVICEMOB, false);
                break;
            case R.string.CONST_LOB_1010:
            case R.string.CONST_LOB_O2F:
                break;
            case R.string.CONST_LOB_PCD:
                //tracker
                FAWrapper.getInstance().sendFAScreen(me, R.string.CONST_SCRN_SERVICEPCD, false);
                break;
            case R.string.CONST_LOB_TV:
                //tracker
                FAWrapper.getInstance().sendFAScreen(me, R.string.CONST_SCRN_SERVICETV, false);
                break;
            case R.string.CONST_LOB_LTS:
                //tracker
                FAWrapper.getInstance().sendFAScreen(me, R.string.CONST_SCRN_SERVICELTS, false);
                break;
        }
        // Update Locale
        ClnEnv.updateUILocale(getBaseContext(), ClnEnv.getAppLocale(getBaseContext()));
    }

    @Override
    protected final void onDestroy() {
        super.onDestroy();
        ClnEnv.setIs101Flag(false);
        //unrigester broadcast receiver
        try {
            if (lineTestBroadcastReceiver != null) {
                unregisterReceiver(lineTestBroadcastReceiver);
                lineTestBroadcastReceiver = null;
            }
        } catch (IllegalArgumentException ex) {
            ex.printStackTrace();
            // add crashlytics
        }
    }

    // Restore the Bundle data when re-built Activity
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        lob = savedInstanceState.getInt("LOB", lob);
        ClnEnv.onRestoreInstanceState(savedInstanceState);
        super.onRestoreInstanceState(savedInstanceState);
    }

    // Saved data before Destroy Activity
    protected void onSaveInstanceState(Bundle outState) {
        outState.putBoolean("ISZOMBIE", isZombie);
        super.onSaveInstanceState(ClnEnv.onSaveInstanceState(outState));
    }

    //----------------------------------------------------------------------------------------------
    //									View OnClick Methods
    //----------------------------------------------------------------------------------------------
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.navbar_button_left:
                onBackPressed();
                break;
            case R.id.navbar_button_right:
                //livechat
                break;
        }
    }

    // Android Device Back Button Handling
    public final void onBackPressed() {
        if (lnttFragment != null && lnttFragment.isVisible()) {
            ClnEnv.setPref(me.getApplicationContext(), me.getString(R.string.CONST_PREF_LNTT_NOTICEFLAG), false);
        }
        closeActivity();
    }

    public void closeActivity() {
        finish();
        overridePendingTransition(R.anim.left_slide_in, R.anim.right_slide_out);
    }

    //----------------------------------------------------------------------------------------------
    //									UI and Data Initialization Methods
    //----------------------------------------------------------------------------------------------
    public void setStatusBarColor() {
        //Status Bar Color to black if lob is 1010
        if (is1010(lobString) && getIntent().getBooleanExtra(Constant.MY_MOBILE_PARENT_ACTIVITY, false)) {
            Window window = getWindow();
            window.getDecorView().setFitsSystemWindows(false);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(R.color.black));
        } else if (SubnRec.LOB_CSP.equalsIgnoreCase(lobString) && ClnEnv.isMyMobFlag() || SubnRec.WLOB_XCSP.equalsIgnoreCase(lobString) && ClnEnv.isMyMobFlag()) {
            Window window = getWindow();
            window.getDecorView().setFitsSystemWindows(false);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(R.color.mymob_club_sim));
        }
    }

    private void initData() {
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        deviceWidth = size.x;
        basePadding = (int) getResources().getDimension(R.dimen.basePadding);
        me = this;

        tabItemList = new ArrayList<>();
        TabItem tabItemBillSum = new TabItem(R.drawable.bill_n_summary, getResString(R.string.myhkt_title_billAndSummary), 0);
        tabItemBillSum.setPageConst(R.string.CONST_SELECTEDVIEW_BILLSUMMARY);
        TabItem tabItemUsagePlan = new TabItem(R.drawable.serviceplan, getResString(R.string.myhkt_title_plan), 1);
        TabItem tabItemMobUsage = new TabItem(R.drawable.usage_n_plan, getResString(R.string.myhkt_title_usageAndPlan), 1);
        tabItemMobUsage.setPageConst(R.string.CONST_SELECTEDVIEW_MOBUSAGE);
        TabItem tabItemMobRoaming = new TabItem(R.drawable.roaming_and_idd, getResString(R.string.myhkt_title_roaming), 2);
        tabItemMobRoaming.setPageConst(R.string.CONST_SELECTEDVIEW_MOBROAMING);
        TabItem tabItemMobVas = new TabItem(R.drawable.optional_and_vas, getResString(R.string.myhkt_title_vas), 3);
        tabItemMobVas.setPageConst(R.string.CONST_SELECTEDFRAG_VAS);

        //Technical Tabs (0 and 1) indexes
        TabItem tabItemLntt = new TabItem(R.drawable.linetest, getResString(R.string.myhkt_linetest), 0);
        tabItemLntt.setPageConst(R.string.CONST_SELECTEDVIEW_LINETEST);
        TabItem tabItemLnttRebootModem = new TabItem(R.drawable.icon_reboot, getResString(R.string.myhkt_reboot_tab), 1);
        tabItemLnttRebootModem.setPageConst(R.string.CONST_SELECTEDVIEW_LINETEST_REBOOT_MODEM);

        //Add the bill summary page at index 0 for Non Technical Accounts
        if (!isMyLineTest) {
            tabItemList.add(tabItemBillSum);
        }
        switch (lob) {
            case R.string.CONST_LOB_MOB:
            case R.string.CONST_LOB_IOI:
            case R.string.CONST_LOB_CSP:
            case R.string.CONST_LOB_VOBB:
                if (!isZombie) {
                    tabItemList.add(tabItemMobUsage);
                    tabItemList.add(tabItemMobRoaming);
                    tabItemList.add(tabItemMobVas);
                }
                break;
            case R.string.CONST_LOB_1010:
            case R.string.CONST_LOB_O2F:
                if (!isZombie) {
                    tabItemMobUsage.setPageConst(R.string.CONST_SELECTEDVIEW_1010USAGE);
                    tabItemList.add(tabItemMobUsage);
                    tabItemMobRoaming.setPageConst(R.string.CONST_SELECTEDVIEW_1010ROAMING);
                    tabItemList.add(tabItemMobRoaming);
                }
                break;
            case R.string.CONST_LOB_PCD:
                if (!isZombie) {
                    if (!isMyLineTest) {
                        tabItemUsagePlan.setPageConst(R.string.CONST_SELECTEDVIEW_PCDPLAN);
                        tabItemList.add(tabItemUsagePlan);
                    } else {
                        tabItemList.add(tabItemLntt);
                        tabItemList.add(tabItemLnttRebootModem);
                    }
                }
                break;
            case R.string.CONST_LOB_TV:
                if (!isZombie) {
                    if (!isMyLineTest) {
                        tabItemUsagePlan.setPageConst(R.string.CONST_SELECTEDVIEW_TVPLAN);
                        tabItemList.add(tabItemUsagePlan);
                    } else {
                        tabItemList.add(tabItemLntt);
                        tabItemList.add(tabItemLnttRebootModem);
                    }
                }
                break;
            case R.string.CONST_LOB_LTS:
                if (!isZombie) {
                    if (!isMyLineTest) {
                        tabItemUsagePlan.setPageConst(R.string.CONST_SELECTEDVIEW_LTSPLAN);
                        tabItemList.add(tabItemUsagePlan);
                    }
                    if (!(ltsType == R.string.CONST_LTS_ONECALL || ltsType == R.string.CONST_LTS_ICFS || ltsType == R.string.CONST_LTS_CALLINGCARD || ltsType == R.string.CONST_LTS_IDD0060) && isMyLineTest) {
                        tabItemList.add(tabItemLntt);
                    }
                }
                break;
        }
        //Only bill sum page can be shown on Zombie accout
        int maxTabSize = 4;
        for (int i = 1; i <= maxTabSize; i++) {
            if (tabItemList.size() < maxTabSize) {
                tabItemList.add(new TabItem(-1, null, i));
            }
        }

        colWidth = (deviceWidth - basePadding * 2) / tabItemList.size();
        servicePagerAdapter = new ServicePagerAdapter(getSupportFragmentManager());
    }

    protected void initUI2() {
        aq = new AAQuery(this);

        //navbar
        updateGuidelineServiceTitle();
        Utils.setNavigationBarTitle_service(this, aq, lob, ltsType, isZombie, assocSubnRec.acctNum, assocSubnRec.srvNum);
        aq.id(R.id.navbar_title).gone();

        aq.navBarBaseLayout(R.id.navbar_base_layout);
        aq.navBarButton(R.id.navbar_button_left, R.drawable.btn_back);
        aq.navBarButton(R.id.navbar_button_right, R.drawable.livechat_small);
        aq.id(R.id.navbar_button_left).clicked(this, "onClick");

        activeSubView = tabItemList.get(tabPos).pageConst;
        viewPager = (ViewPager) aq.id(R.id.activity_myprofile_view).getView();
        if (viewPager.getAdapter() == null) {
            if (debug) Log.i(TAG, "onStartSet");
            viewPager.setAdapter(servicePagerAdapter);
            if (debug) Log.i(TAG, "onStartEnd");
            hktindicator = (HKTIndicator) aq.id(R.id.activity_myprofile_indictaor).getView();
            aq.id(R.id.activity_myprofile_indictaor).height(Math.max((int) getResources().getDimension(R.dimen.indicator_height), colWidth), false);
            if (ClnEnv.isMyMobFlag()) {   // MyMob's MyAccount Login default displaying "Usage & Plan" Screen
                if (lob == R.string.CONST_LOB_1010) {
                    activeSubView = R.string.CONST_SELECTEDVIEW_1010USAGE;
                    setModuleId(getResString(R.string.MODULE_101_MM_PLAN));
                } else if (lob == R.string.CONST_LOB_O2F) {
                    activeSubView = R.string.CONST_SELECTEDVIEW_1010USAGE;
                    setModuleId(getResString(R.string.MODULE_O2F_MM_PLAN));
                } else if (lob == R.string.CONST_LOB_MOB) {
                    activeSubView = R.string.CONST_SELECTEDVIEW_MOBUSAGE;
                    setModuleId(getResString(R.string.MODULE_MOB_MM_PLAN));
                } else if (lob == R.string.CONST_LOB_IOI) {
                    activeSubView = R.string.CONST_SELECTEDVIEW_MOBUSAGE;
                    setModuleId(getResString(R.string.MODULE_IOI_MM_PLAN));
                } else if (lob == R.string.CONST_LOB_CSP) {
                    activeSubView = R.string.CONST_SELECTEDVIEW_1010USAGE;
                    setModuleId(getResString(R.string.MODULE_CLUBSIM_MM_PLAN));
                }
                tabPos = 1;
            }
            hktindicator.setViewPager(viewPager, tabPos);
            hktindicator.initView(this, tabItemList, (int) getResources().getDimension(R.dimen.smalltext1size), getResources().getColor(R.color.hkt_textcolor), Color.TRANSPARENT, colWidth);
            hktindicator.setOnPageChangeListener(new OnPageChangeListener() {

                @Override
                public void onPageScrollStateChanged(int arg0) {
                }

                @Override
                public void onPageScrolled(int arg0, float arg1, int arg2) {
                }

                @Override
                public void onPageSelected(int position) {
                    tabPos = position;
                    activeSubView = tabItemList.get(position).pageConst;
                    if (debug) Log.i(TAG, activeSubView + "/" + position);
                    Utils.closeSoftKeyboard(me);
                    fragmentRefresh(activeSubView);
                }
            });
        }
    }

    private void updateGuidelineServiceTitle() {
        float density = Utils.getDensity(this.getApplicationContext());
        if (density <= 1.0f && isZombie) {
            TextView serviceTitle = (TextView) aq.id(R.id.navbar_service_title).getTextView();
            ConstraintLayout.LayoutParams lp = (ConstraintLayout.LayoutParams)
                    serviceTitle.getLayoutParams();

            lp.constrainedWidth = false;

            TextViewCompat.setAutoSizeTextTypeWithDefaults(serviceTitle, TextViewCompat.AUTO_SIZE_TEXT_TYPE_NONE);
            serviceTitle.setLayoutParams(lp);
        }
    }

    //----------------------------------------------------------------------------------------------
    //									API Listeners
    //----------------------------------------------------------------------------------------------
    public void onSuccess(APIsResponse response) {
        super.onSuccess(response);
    }

    public void onFail(APIsResponse response) {
        super.onFail(response);
    }

    //----------------------------------------------------------------------------------------------
    //									Private Util Methods and Local Class
    //----------------------------------------------------------------------------------------------
    // View Pager Class
    private class ServicePagerAdapter extends FragmentPagerAdapter {

        public ServicePagerAdapter(FragmentManager fragmentManager) {
            super(fragmentManager);
        }

        @Override
        public int getCount() {
            int pageCount = 0;
            for (TabItem tabItem : tabItemList) {
                if (tabItem.text != null && !tabItem.text.isEmpty()) {
                    pageCount++;
                }
            }
            return pageCount;
        }

        // Returns the fragment to display for that page
        @Override
        public Fragment getItem(int position) {
            Bundle bundleMobile = new Bundle();
            bundleMobile.putBoolean(Constant.MY_MOBILE_PARENT_ACTIVITY,
                    getIntent().getBooleanExtra(Constant.MY_MOBILE_PARENT_ACTIVITY, false));
            bundleMobile.putBoolean(Constant.MY_MOBILE_IS_1010, is1010(lobString));
            bundleMobile.putString(Constant.MY_MOBILE_LOB_STRING, lobString);
            switch (tabItemList.get(position).pageConst) {
                case R.string.CONST_SELECTEDVIEW_BILLSUMMARY:
                    billFragment = new BillFragment();
                    billFragment.setArguments(bundleMobile);
                    return billFragment;
                case R.string.CONST_SELECTEDVIEW_PCDPLAN:
                    planPCDFragment = new PlanPCDFragment();
                    return planPCDFragment;
                case R.string.CONST_SELECTEDVIEW_TVPLAN:
                    planTVFragment = new PlanTVFragment();
                    return planTVFragment;
                case R.string.CONST_SELECTEDVIEW_LTSPLAN:

                    return planLtsFragment;
                case R.string.CONST_SELECTEDVIEW_LINETEST:
                case R.string.CONST_SELECTEDVIEW_LINETEST1:
                case R.string.CONST_SELECTEDVIEW_LINETEST3:

                    return lnttFragment;
                case R.string.CONST_SELECTEDVIEW_MOBUSAGE:
                case R.string.CONST_SELECTEDVIEW_1010USAGE:

                    return usageFragment;
                case R.string.CONST_SELECTEDVIEW_MOBROAMING:
                case R.string.CONST_SELECTEDVIEW_1010ROAMING:

                    return roamingFragment;
                case R.string.CONST_SELECTEDFRAG_VAS:
                    vasFragment = new VasFragment();
                    Bundle bundle = new Bundle();
                    bundle.putInt(KEY_NAME_LOB_TYPE, lob);
                    vasFragment.setArguments(bundle);
                    return vasFragment;

                case R.string.CONST_SELECTEDVIEW_LINETEST_REBOOT_MODEM:
                    lnttFragment.resetLineTestStartPageIfNeeded(); //fix for: CR0021508
                    rebootModemFragment = new LnttRebootModemFragment();
                    return rebootModemFragment;
                default:
                    return null;
            }
        }
    }

    public void fragmentRefresh(int activeSubView) {
        switch (activeSubView) {
            case R.string.CONST_SELECTEDVIEW_BILLSUMMARY:
                if (billFragment != null) {
                    planLtsFragment.detachIDDFragment();
                    billFragment.refresh();
                }
                break;
            //Plan
            case R.string.CONST_SELECTEDVIEW_PCDPLAN:
                if (planPCDFragment != null) {
                    planPCDFragment.refresh();
                }
                break;
            case R.string.CONST_SELECTEDVIEW_TVPLAN:
                if (planTVFragment != null) {
                    planTVFragment.refreshData();
                }
                break;
            case R.string.CONST_SELECTEDVIEW_LTSPLAN:
                if (planLtsFragment != null) {
                    planLtsFragment.refresh();
                }
                break;
            case R.string.CONST_SELECTEDVIEW_LINETEST:
            case R.string.CONST_SELECTEDVIEW_LINETEST1:
            case R.string.CONST_SELECTEDVIEW_LINETEST3:
                if (lnttFragment != null) {
                    if (isToShowLNTTResultSuccess()) {
                        lnttFragment.refreshData();
                    } else {
                        lnttFragment.refresh();
                    }
                }
                break;
            case R.string.CONST_SELECTEDVIEW_MOBUSAGE:
            case R.string.CONST_SELECTEDVIEW_1010USAGE:
                if (lob == R.string.CONST_LOB_MOB) {
                    moduleId = getString(R.string.MODULE_MOB_PLAN);
                } else if (lob == R.string.CONST_LOB_IOI) {
                    moduleId = getString(R.string.MODULE_IOI_PLAN);
                } else if (lob == R.string.CONST_LOB_1010) {
                    moduleId = getString(R.string.MODULE_101_PLAN);
                } else if (lob == R.string.CONST_LOB_O2F) {
                    moduleId = getString(R.string.MODULE_O2F_PLAN);
                } else if (lob == R.string.CONST_LOB_VOBB) {
                    moduleId = getString(R.string.MODULE_VOBB_PLAN);
                }
                if (usageFragment != null) {
                    usageFragment.refresh();
                }
                break;
            case R.string.CONST_SELECTEDVIEW_MOBROAMING:
            case R.string.CONST_SELECTEDVIEW_1010ROAMING:
                if (roamingFragment != null) {
                    roamingFragment.refresh();
                }
                break;
            case R.string.CONST_SELECTEDFRAG_VAS:
                if (vasFragment != null) {
                    vasFragment.refreshData();
                }
                break;
            case R.string.CONST_SELECTEDVIEW_LINETEST_REBOOT_MODEM:
                lnttFragment.resetLineTestStartPageIfNeeded(); //fix for: CR0021508
                if (rebootModemFragment != null) {
                    if (isToRebootModemAPI) {
                        rebootModemFragment.callRebootModemAPI();
                    } else {
                        rebootModemFragment.restartUI();
                    }
                }
                isToRebootModemAPI = false;
            default:
        }
    }

    private int getLob(SubnRec subnRec) {
        if (subnRec == null) {
            return 0;
        }
        if (SubnRec.LOB_LTS.equalsIgnoreCase(subnRec.lob)) {
            return R.string.CONST_LOB_LTS;
        } else if (SubnRec.LOB_MOB.equalsIgnoreCase(subnRec.lob)) {
            return R.string.CONST_LOB_MOB;
        } else if (SubnRec.LOB_IOI.equalsIgnoreCase(subnRec.lob)) {
            return R.string.CONST_LOB_IOI;
        } else if (SubnRec.LOB_PCD.equalsIgnoreCase(subnRec.lob)) {
            return R.string.CONST_LOB_PCD;
        } else if (SubnRec.LOB_TV.equalsIgnoreCase(subnRec.lob)) {
            return R.string.CONST_LOB_TV;
        } else if (SubnRec.LOB_101.equalsIgnoreCase(subnRec.lob)) {
            return R.string.CONST_LOB_1010;
        } else if (SubnRec.LOB_O2F.equalsIgnoreCase(subnRec.lob)) {
            return R.string.CONST_LOB_O2F;
        } else if (SubnRec.WLOB_XMOB.equalsIgnoreCase(subnRec.lob)) {
            return R.string.CONST_LOB_MOB;
        } else if (SubnRec.LOB_CSP.equalsIgnoreCase(subnRec.lob) || SubnRec.WLOB_XCSP.equalsIgnoreCase(subnRec.lob)) {
            return R.string.CONST_LOB_CSP;
        } else if (SubnRec.LOB_VOBB.equalsIgnoreCase(subnRec.lob)) {
            return R.string.CONST_LOB_VOBB;
        } else {
            return 0;
        }
    }

    private boolean is1010(String lob) {
        return lob.equals(SubnRec.LOB_101) || lob.equals(SubnRec.LOB_IOI) || lob.equals(SubnRec.WLOB_X101);
    }

    private String getLobString(SubnRec subnRec) {
        if (SubnRec.LOB_LTS.equalsIgnoreCase(subnRec.lob)) {
            return SubnRec.LOB_LTS;
        } else if (SubnRec.LOB_MOB.equalsIgnoreCase(subnRec.lob)) {
            return SubnRec.LOB_MOB;
        } else if (SubnRec.WLOB_XMOB.equalsIgnoreCase(subnRec.lob)) {
            return SubnRec.WLOB_XMOB;
        } else if (SubnRec.LOB_IOI.equalsIgnoreCase(subnRec.lob)) {
            return SubnRec.LOB_IOI;
        } else if (SubnRec.LOB_PCD.equalsIgnoreCase(subnRec.lob)) {
            return SubnRec.LOB_PCD;
        } else if (SubnRec.LOB_TV.equalsIgnoreCase(subnRec.lob)) {
            return SubnRec.LOB_TV;
        } else if (SubnRec.LOB_101.equalsIgnoreCase(subnRec.lob)) {
            return SubnRec.LOB_101;
        } else if (SubnRec.LOB_O2F.equalsIgnoreCase(subnRec.lob)) {
            return SubnRec.LOB_O2F;
        } else if (SubnRec.LOB_CSP.equalsIgnoreCase(subnRec.lob)) {
            return SubnRec.LOB_CSP;
        } else if (SubnRec.WLOB_XCSP.equalsIgnoreCase(subnRec.lob)) {
            return SubnRec.WLOB_XCSP;
        } else if (SubnRec.LOB_VOBB.equalsIgnoreCase(subnRec.lob)) {
            return SubnRec.LOB_VOBB;
        }
        return "";
    }

    //----------------------------------------------------------------------------------------------
    //										OnServiceListener
    //						Location: com.pccw.myhkt.fragment.BaseServiceFragment
    //----------------------------------------------------------------------------------------------
    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    public SubnRec getSubnRec() {
        return assocSubnRec;
    }

    public void setSubscriptionRec(SubnRec subnRec) {
    }

    public void setActiveSubview(int index) {
        activeSubView = index;
    }

    public int getActiveSubview() {
        return activeSubView;
    }

    public void displaySubview() {
    }

    public int getLob() {
        return lob;
    }

    @Override
    public int getLtsType() {
        if (debug) Log.i(TAG, "getType" + ltsType);
        return ltsType;
    }

    public String getLobString() {
        return lobString;
    }

    public boolean IsZombie() {
        return isZombie;
    }

    public boolean CompareLastBillDate(String lastBilldate, String newlastBilldate) {
        return false;
    }

    public boolean isMyMobAcct() {
        return isMyMobAcct;
    }

    public AcctAgent getAcctAgent() {
        return acctAgent;
    }

    @Override
    public MyMobAcctAgent getMyMobAcctAgent() {
        return myMobAcctAgent;
    }

    @Override
    public int getCurrentServicePage() {
        return tabPos;
    }

    @Override
    public ApptCra getApptCra() {
        return apptCra;
    }

    @Override
    public void setApptCra(ApptCra mApptCra) {
        apptCra = mApptCra;
    }

    @Override
    public LnttAgent getLnttAgent() {
        return lnttAgent;
    }

    @Override
    public void setLnttAgent(LnttAgent mLnttAgent) {
        lnttAgent = mLnttAgent;
    }

    @Override
    public LnttCra getLnttCra() {
        return lnttCra;
    }

    @Override
    public void setLnttCra(LnttCra mLnttCra) {
        this.lnttCra = mLnttCra;
    }

    @Override
    public void openLiveChat() {
        super.openLiveChat();
    }

    @Override
    public String getModuleId() {
        return moduleId;
    }

    @Override
    public String getNavTitle() {
        return navBarTitle;
    }

    @Override
    public boolean isLnttServiceClearable() {
        if (debug) Log.i(TAG, "isLnttService Clearable = " + isLnttServiceClearable);
        return isLnttServiceClearable;
    }

    @Override
    public void setIsLnttServiceClearable(Boolean mIsLnttServiceClearable) {
        this.isLnttServiceClearable = mIsLnttServiceClearable;
        if (debug) Log.i(TAG, "Set isLnttService Clearable = " + mIsLnttServiceClearable);
        if (debug) Log.i(TAG, "LnttAngent" + new Gson().toJson(Utils.getPrefLnttAgent(this)));
    }

    @Override
    public String getTrackerId() {
        return trackerId;
    }

    @Override
    public void setTrackerId(String mtrackerId) {
        trackerId = mtrackerId;
    }

    @Override
    public void onShowModemTab(boolean isToAutoReboot) {
        //Show modem tab
        isToRebootModemAPI = isToAutoReboot;
        tabPos = 1;
        activeSubView = R.string.CONST_SELECTEDVIEW_LINETEST_REBOOT_MODEM;
        hktindicator.onPageSelected(1);
        viewPager.setCurrentItem(1, true);
    }

    //----------------------------------------------------------------------------------------------
    //									LINE TEST METHODS
    //----------------------------------------------------------------------------------------------
    BroadcastReceiver lineTestBroadcastReceiver = null;
    private void updateLineTestFragment() {
        //Check for the case that should not clear the lnttService
        lnttAgent = Utils.getPrefLnttAgent(this);
        String lnttAgentStr = ClnEnv.getPref(this, this.getString(R.string.CONST_PREF_LNTT_AGENT), "");
        //Line test is running
        if (lnttAgent != null && lnttAgent.getEndTimestamp() != null && !lnttAgentStr.isEmpty() && "".equalsIgnoreCase(lnttAgent.getEndTimestamp())) {
            this.setIsLnttServiceClearable(false);
        }

        //Should not clear LnntAgent is not belong to this service
        if (lnttAgent != null && !lnttAgentStr.isEmpty() && !(lnttAgent.getLnttSrvNum().equalsIgnoreCase(assocSubnRec.srvNum) && assocSubnRec.acctNum.equalsIgnoreCase(lnttAgent.getLnttCra().getISubnRec().acctNum))) {
            this.setIsLnttServiceClearable(false);
        }
        //If LineTest returned Result, activeSubview == R.string.CONST_SELECTEDVIEW_TVLINETEST3;
        //Assume Expiry checking at ServiceListActivity

        lnttAgent = Utils.getPrefLnttAgent(this);
        if (lnttAgent.getLnttSrvNum().equalsIgnoreCase(assocSubnRec.srvNum) && assocSubnRec.acctNum.equalsIgnoreCase(lnttAgent.getLnttCra().getISubnRec().acctNum)) {
            //If line test is displayed, refresh the fragment, otherwise show dialog
            if (activeSubView == R.string.CONST_SELECTEDVIEW_LINETEST ||
                    activeSubView == R.string.CONST_SELECTEDVIEW_LINETEST1 ||
                    activeSubView == R.string.CONST_SELECTEDVIEW_LINETEST3) {

                if (!"".equalsIgnoreCase(lnttAgent.getResultMsg())) {
                    showLnttErrorMessageDialog();
                }

                lnttFragment.refreshData();
                // Reset the Notification flag to be false
                ClnEnv.setPref(getApplicationContext(), getString(R.string.CONST_PREF_LNTT_NOTICEFLAG), false);
            } else {
                if (!"".equalsIgnoreCase(lnttAgent.getResultMsg())) {
                    showLnttErrorMessageDialog();
                } else {
                    //show success message, with OK (show line test result fragment directly)
                    // and Later button to dismiss
                    showLnttSuccessMessageDialog();
                }
            }
        } else {
            //Show the OK, Later dialog to show the Service Account details
            redirectDialog(me, R.string.CONST_DIALOG_LNTT_RESULT);
        }
    }

    private void showLnttErrorMessageDialog() {
        DialogInterface.OnClickListener onPositiveClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //clear the error message since it is already shown
                lnttAgent.setResultMsg("");
                ClnEnv.setPref(ServiceActivity.this, getResources().getString(R.string.CONST_PREF_LNTT_AGENT), Utils.serialize(lnttAgent));
            }
        };
        DialogHelper.createSimpleDialog(this, lnttAgent.getResultMsg(), getResString(R.string.btn_ok), onPositiveClickListener);
    }

    private void showLnttSuccessMessageDialog() {
        DialogInterface.OnClickListener onPositiveClickListener = new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                if (lnttFragment != null && lnttFragment.isVisible()) {
                    ClnEnv.setPref(me.getApplicationContext(), me.getString(R.string.CONST_PREF_LNTT_NOTICEFLAG), true);
                }
                dialog.dismiss();
            }
        };
        DialogInterface.OnClickListener onNegativeClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                ClnEnv.setPref(me.getApplicationContext(), me.getString(R.string.CONST_PREF_LNTT_NOTICEFLAG), true);
                //navigate to line test
                tabPos = 0;
                activeSubView = R.string.CONST_SELECTEDVIEW_LINETEST;
                hktindicator.onPageSelected(0);
                viewPager.setCurrentItem(0, true);
            }
        };
        if (lnttFragment != null && lnttFragment.isVisible()) {
            //instead of calling GlobaDialog : CONST_DIALOG_LNTT_RESULT, display dialog immediately
            DialogHelper.createSimpleDialog(this, Utils.getString(me, R.string.MYHKT_LT_MSG_COMPLETED),
                    Utils.getString(me, R.string.btn_ok), onNegativeClickListener, Utils.getString(me, R.string.MYHKT_BTN_LATER), onPositiveClickListener);
        } else {
            redirectDialog(me, R.string.CONST_DIALOG_LNTT_RESULT);
        }
    }

    public boolean isToShowLNTTResultSuccess() {
        return lnttAgent != null && lnttAgent.getLnttSrvNum().equalsIgnoreCase(assocSubnRec.srvNum) &&
                assocSubnRec.acctNum.equalsIgnoreCase(lnttAgent.getLnttCra().getISubnRec().acctNum) &&
                ClnEnv.getPref(getApplicationContext(), getString(R.string.CONST_PREF_LNTT_NOTICEFLAG), false);
    }

    protected final void redirectDialog(Context context, int dialogType) {
        Intent dialogIntent = new Intent(context, GlobalDialog.class);
        dialogIntent.putExtra("DIALOGTYPE", dialogType);
        dialogIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        context.startActivity(dialogIntent);
    }
}
