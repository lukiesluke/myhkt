package com.pccw.myhkt.listeners;

import android.graphics.Bitmap;

import java.io.File;

public interface OnDownloadAttachmentListener {
    void onSuccessDownload(File file);
    void onFailedDownload();
}
