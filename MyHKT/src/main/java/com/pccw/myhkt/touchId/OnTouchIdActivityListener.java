package com.pccw.myhkt.touchId;

/**
 * Created by AMoiz Esmail on 03/11/2017.
 */

public interface OnTouchIdActivityListener {

    void onTouchIdSuccess();
    void onTouchIdFailed(int failCount);
    void onEnterPassword();
    void onFingerprintLockout(String error);
    void onFingerprintError(int messId);
}
