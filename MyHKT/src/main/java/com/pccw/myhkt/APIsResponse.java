package com.pccw.myhkt;

import java.io.Serializable;

import com.pccw.wheat.shared.rpc.BaseCra;

public class APIsResponse extends BaseCra implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2257542614965654192L;
	private String actionTy;
	private String message;
	private Object cra;

	public APIsResponse() {
		initAndClear();
	}

	protected void init() {
		super.init();
	}

	public void clear() {
		super.clear();

		clearActionTy();
		clearMessage();
		clearCra();
	}

	public String getActionTy() {
		return actionTy;
	}

	public void setActionTy(String actionTy) {
		this.actionTy = actionTy;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Object getCra() {
		return cra;
	}

	public void setCra(Object cra) {
		this.cra = cra;
	}
	
	public void clearActionTy() {
		setActionTy("");
	}

	public void clearMessage() {
		setMessage("");
	}

	public void clearCra() {
		setCra(new Object());
	}
}
