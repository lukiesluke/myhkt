/*
    Class for an BOM Customer 
    
    Keywords
    --------
    $URL: $
    $Rev: $
    $Date: $
    $Author: $
*/

package com.pccw.dango.shared.entity;

import java.io.Serializable;


public class BomCust implements Serializable
{
    private static final long serialVersionUID = -7269962570537451915L;
    
    private String                  lob;
    private String                  cusNum;
    private String                  sysTy;
    private String                  acctNum;
    
    
    public static void main(String rArg[])
    {
        System.out.println(getVer());
        return;
    }
    
    
    public static String getVer()
    {
        return ("$URL: $, $Rev: $");
    }
    
    
    public BomCust()
    {
        initAndClear();
    }
    
    
    public BomCust(String rLob, String rCusNum, String rSysTy, String rAcctNum)
    {
        this();
        
        setLob(rLob);
        setCusNum(rCusNum);
        setSysTy(rSysTy);
        setAcctNum(rAcctNum);
    }
    
    
    final void initAndClear()
    {
        init();
        clear();
    }
    
    
    protected void init()
    {
    }
    
    
    public void clear()
    {
        clearLob();
        clearCusNum();
        clearSysTy();
        clearAcctNum();
    }
    
    
    public BomCust copyFrom(BomCust rSrc)
    {
        setLob(rSrc.getLob());
        setCusNum(rSrc.getCusNum());
        setSysTy(rSrc.getSysTy());
        setAcctNum(rSrc.getAcctNum());
        
        return (this);
    }
    
    
    public BomCust copyTo(BomCust rDes)
    {
        rDes.copyFrom(this);
        return (rDes);
    }
    
    
    public BomCust copyMe()
    {
        BomCust             rDes;
        
        rDes = new BomCust();
        rDes.copyFrom(this);
        
        return (rDes);
    }
    
    
    public void clearLob()
    {
        setLob("");
    }
    
    
    public void setLob(String rLob)
    {
        lob = rLob;
    }
    
    
    public String getLob()
    {
        return (lob);
    }
    
    
    public void clearCusNum()
    {
        setCusNum("");
    }
    
    
    public void setCusNum(String rCusNum)
    {
        cusNum = rCusNum;
    }
    
    
    public String getCusNum()
    {
        return (cusNum);
    }
    
    
    public void clearAcctNum()
    {
        setAcctNum("");
    }
    
    
    public void setAcctNum(String rAcctNum)
    {
        acctNum = rAcctNum;
    }

    
    public String getAcctNum()
    {
        return (acctNum);
    }
    
    
    public void clearSysTy()
    {
        setSysTy("");
    }
    
    
    public void setSysTy(String rSysTy)
    {
        sysTy = rSysTy;
    }
    
    
    public String getSysTy()
    {
        return (sysTy);
    }
    
    
    public boolean isSame(BomCust rBomCust)
    {
        return (getSysTy().equals(rBomCust.getSysTy()) && 
                getCusNum().equals(rBomCust.getCusNum()));
    }
}
